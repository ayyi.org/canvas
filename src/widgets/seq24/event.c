//----------------------------------------------------------------------------
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------
#include "config.h"
#include "gui_types.h"
#include "seq24/event.h"


unsigned char
midi_event__get_note(midi_event* event)
{
    return event->data[0];
}


gboolean
midi_event__is_note_on(midi_event* event)
{
    return (event->status == EVENT_NOTE_ON); 
}


gboolean
midi_event__is_note_off(midi_event* event)
{
    return (event->status == EVENT_NOTE_OFF);
}


gboolean
event__is_linked(midi_event* event)
{
	return false;
}

