/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "agl/text/renderer.h"

/*
 *   Actor drawing the background and text label
 */

#ifdef DEBUG
static bool      gl_track_verify                   (TrackActor*);
#endif

static void      gl_part_set_state                 (AGlActor*);
static bool      gl_part_paint                     (AGlActor*);
static void      gl_part_remove                    (PartActor*);
static AGlActor* gl_part_add_audio_to_wf_canvas    (PartActor*);
static void      arr_gl_draw_midi_contents         (PartActor*);
static void      gl_set_colour_from_part_bg        (AMPart*);

static AGlActor* gl_part_new  (AMPart*);
static void      gl_part_free (AGlActor*);

static AGlActorClass part_class = {0, "Part", (AGlActorNew*)gl_part_new, gl_part_free};


static AGlActor*
gl_part_new (AMPart* part)
{
	void gl_part_init (AGlActor* actor)
	{
		PartActor* pa = (PartActor*)actor;
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = arrange->canvas->gl;
		AMPart* part = pa->part;

		if (c->wfc && PART_IS_AUDIO(part) && arrange->view_options[SHOW_PART_CONTENTS].value) {
			AGlActor* wf = gl_part_add_audio_to_wf_canvas(pa);
			agl_actor__add_child(actor, wf);

			if (part->pool_item) {

				void _glpart_on_peakdata_available (Waveform* waveform, gpointer _pa)
				{
					PF2;
					PartActor* pa = _pa;
					Arrange* arrange = ((AGlActor*)pa)->root->user_data;
					g_return_if_fail(windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE));
					GlCanvas* c = arrange->canvas->gl;
					if (arr_part_is_in_viewport(arrange, pa->part)) {
						// this is the correct actor to invalidate
						agl_actor__invalidate((AGlActor*)pa->wf_actor);
						// but for now we need to invalidate the track because the parts are not properly parented. TODO
						agl_actor__invalidate(c->actors[ACTOR_TYPE_TRACKS]);
					}
				}

				void _glpart_on_hires_available (Waveform* waveform, int block, gpointer pa)
				{
					_glpart_on_peakdata_available(waveform, pa);
				}

				g_signal_connect (part->pool_item, "peakdata-ready", (GCallback)_glpart_on_peakdata_available, pa);
				g_signal_connect (part->pool_item, "hires-ready", (GCallback)_glpart_on_hires_available, pa);
			}
		}
	}

	void gl_part_set_size (AGlActor* actor)
	{
		// Currently we attempt to render the whole part. This fails for very large parts (perhaps ~300bars)

		PartActor* pa = (PartActor*)actor;
		g_return_if_fail(pa);
		AMPart* part = pa->part;
		Arrange* arrange = actor->root->user_data;

		float x = arr_gl_pos2px_(arrange, &pa->part->start);

		actor->region = (AGlfRegion){
			.x1 = x,
			.x2 = x + MAX(1, arr_gl_pos2px_(arrange, &part->length)), // minimun width of 1px
			.y2 = agl_actor__height(actor->parent)
		};

		if (pa->wf_actor) {
			((AGlActor*)pa->wf_actor)->region = actor->region;

			if (x < actor->parent->cache.position.x) {
				// recalcuate cache dimensions
				actor->parent->set_size(actor->parent);
			}
		}

		agl_actor__invalidate(actor);
	}

	return (AGlActor*)agl_actor__new(PartActor,
		.actor = {
			.class = &part_class,
			.name = g_strdup(part->name),
			.program = (AGlShader*)&part_shader,
			.init = gl_part_init,
			.set_size = gl_part_set_size,
			.set_state = gl_part_set_state,
			.paint = gl_part_paint,
		},
		.part = part
	);
}


static void
gl_part_free (AGlActor* actor)
{
	PartActor* pa = (PartActor*)actor;
	Arrange* arrange = actor->root->user_data;

	AMPoolItem* pi = pa->part->pool_item;
	if (pi) {
		signal_handler_disconnect_data(pi, pa, 2, "glpart");
	}

	if (!g_hash_table_remove(CGL->parts, pa->part))
		pwarn("unable to remove part from hashtable");
}


void
gl_part_move (AGlActor* actor)
{
	/*
	 *  After a move the part stays the same so does not need to be redrawn
	 *  but the parent does need to be redrawn.
	 *
	 *  agl_actor__invalidate() will clear the cache, but agl_actor__set_size() is needed to recalculate the cache dimensions
	 */
	agl_actor__set_size(actor->parent);
}


/*
 *  Move part to a different track
 */
void
gl_part_change_track (AGlActor* actor)
{
	PartActor* pa = (PartActor*)actor;
	Arrange* arrange = actor->root->user_data;
	AMPart* part = pa->part;

	gl_part_remove(pa);

	arr_gl_part_new(arrange, part);

#ifdef DEBUG
	TrackActor* ta = (TrackActor*)actor->parent;
	g_assert(gl_track_verify(ta));
#endif
}


static void
gl_part_set_state (AGlActor* actor)
{
	PartActor* pa = (PartActor*)actor;
	AMPart* part = pa->part;
	Arrange* arrange = actor->root->user_data;

	part_shader.uniform.colour = song->palette[part->bg_colour];
	part_shader.uniform.selected = arr_part_is_selected(arrange, part);
	part_shader.shader.uniforms[PART_SHADER_WIDTH].value[0] = agl_actor__width(actor);
	part_shader.shader.uniforms[PART_SHADER_HEIGHT].value[0] = agl_actor__height(actor);
}


/*
 *  Paint the part background (the waveform is drawn in a child actor)
 */
static bool
gl_part_paint (AGlActor* actor)
{
	double height = agl_actor__height(actor);
	double width = agl_actor__width(actor);

	PartActor* pa = (PartActor*)actor;
	AMPart* part = pa->part;

	if(actor->children){
		AGlActor* child = actor->children->data;
		AGlfRegion* r = &child->region;
		r->x1 = 0;
	}

#ifdef USE_FBO
	double z = 0.0;
#else
	double z = am_track_list_position(song->tracks, part->track) * -50.0;
#endif

	if(agl->use_shaders){
		agl_translate (&part_shader.shader, -PART_TRANSITION_BORDER, -PART_TRANSITION_BORDER);
		agl_rect(0., 0., width + 2 * PART_TRANSITION_BORDER, height + 2 * PART_TRANSITION_BORDER);
	}else{
		glTranslatef(0.0, 0.0, z);

		int bg_width = 256;

		double tpx = 1.0;
		if(width < bg_width) tpx = width / bg_width;
		// if part is large, only a fraction of the Part is textured.
		// So we need to draw 2 objects, one textured, one not.
		agl_enable(AGL_ENABLE_BLEND);
		gl_set_colour_from_part_bg(part);
		agl_textured_rect(bg_textures[TEXTURE_BG], 0, 0, width, height, &(AGlQuad){0.0, 0.0, tpx, 1.0});

		if(width > bg_width){
			// draw untextured rectangle.
		}
	}

	switch(part->ident.type){
		case AYYI_OBJECT_AUDIO_PART:
			break;
		case AYYI_OBJECT_MIDI_PART:
			arr_gl_draw_midi_contents(pa);
			break;
		default:
			pwarn("bad AMPart type: %i", part->ident.type);
			break;
	}

	{
		if(actor->parent){
			agl_push_clip(0., 0., width - 4., height);

			if(width > 8.0 && height > 10.0){
				agl_print(
					2,
					(int)height - 11,
					am_track_list_position(song->tracks, part->track) * -5. * 0.001/* TODO fbo needs more z-depth */,
					part->fg_is_set ? part->fg_colour : song->palette[1],
					"%s", part->name
				);
			}

			agl_pop_clip();
		}
	}

	return true;
}


static AGlActor*
gl_part_add_audio_to_wf_canvas (PartActor* pa)
{
	AMPart* part = pa->part;
	g_return_val_if_fail(PART_IS_AUDIO(part), NULL);
	g_return_val_if_fail(!pa->wf_actor, NULL);

	AGlActor* actor = (AGlActor*)pa;
	Arrange* arrange = actor->root->user_data;
	GlCanvas* c = arrange->canvas->gl;

	pa->wf_actor = wf_context_add_new_actor(c->wfc, (Waveform*)part->pool_item);
	wf_actor_set_region(pa->wf_actor, &(WfSampleRegion){part->region_start, part->ayyi->length});

	uint32_t fg = agl->use_shaders
		? (part->fg_colour & 0xffffff00) + 156                         // set alpha to 65% - TODO we are overriding a user_setting here?
		: (colour_grey_out_rgba(part->fg_colour) & 0xffffff00) + 0xff; // force opaque due to unresolved blending issues TODO

	wf_actor_set_colour(pa->wf_actor, fg);
	g_free(((AGlActor*)pa->wf_actor)->name);
	((AGlActor*)pa->wf_actor)->name = g_strdup(part->name);

	dbg(2, "%s", part->name);

	return (AGlActor*)pa->wf_actor;
}


static void
gl_part_remove (PartActor* pa)
{
	AGlActor* actor = (AGlActor*)pa;
	TrackActor* ta = (TrackActor*)actor->parent;

	agl_actor__remove_child((AGlActor*)ta, actor);
}


static inline void
arr_gl_draw_note (ArrTrk* at, AMPart* part, MidiNote* note, uint32_t colour, double part_z)
{
	Arrange* arrange = at->arrange;

	double y = arr_track_note_pos_y(at, note);
	if (y > -1.) {
		((AGlUniformUnion*)&agl->shaders.plain->uniforms[PLAIN_COLOUR])->value.i[0] = colour;
		agl_use_program((AGlShader*)agl->shaders.plain);

		agl_rect_((AGlRect){
			.x = arr_samples2px(arrange, note->start),
			.y = arr_track_note_pos_y(at, note),
			.w = MAX(arr_samples2px(arrange, note->length), ARR_MIN_NOTE_DISPLAY_LENGTH),
			.h = ((ArrTrackMidi*)at)->note_height
		});
	}
}


static inline void
arr_gl_draw_note_velocity_bar (ArrTrk* at, AMPart* part, MidiNote* note, uint32_t colour)
{
	#define velocity_bar_width 6

	ArrTrackMidi* atm = (ArrTrackMidi*)at;
	Arrange* arrange = at->arrange;
	int chart_height = atm->velocity_chart_height;
	int track_height = at->height * vzoom(arrange);

	if(track_height < 32) return;

	// border
	if (agl->use_shaders) {
		SET_PLAIN_COLOUR(agl->shaders.plain, 0x000000ff);
		agl_use_program((AGlShader*)agl->shaders.plain);
	}
	agl_rect_((AGlRect){
		.x = arr_samples2px(arrange, note->start),
		.y = track_height - 1,
		.w = velocity_bar_width,
		.h = - (chart_height * note->velocity) / 128
	});

	// main bar
	if (agl->use_shaders) {
		SET_PLAIN_COLOUR(agl->shaders.plain, colour);
		agl_use_program((AGlShader*)agl->shaders.plain);
	}
	agl_rect_((AGlRect){
		.x = arr_samples2px(arrange, note->start),
		.y = track_height - 1,
		.w = velocity_bar_width - 1,
		.h = - (chart_height * note->velocity) / 128
	});
}


static void
arr_gl_draw_midi_contents (PartActor* pa)
{
	AMPart* part = pa->part;
	AMTrack* track = part->track;
	ArrTrk* atr = ((TrackActor*)((AGlActor*)pa)->parent)->atr;
	MidiPart* mpart = (MidiPart*)part;

	double z = am_track_list_position(song->tracks, track) * -50.0;

	// draw midi notes
	MidiNote* note = NULL;
	int safety = 0;
	while((note = am_midi_part__get_next_visible(part, note))){
		arr_gl_draw_note(atr, part, note, 0xff0000ff, z);
		arr_gl_draw_note_velocity_bar(atr, part, note, 0x0ffffff);
		if(safety++ > 512){ perr("bailing out!"); break; }
	}
	if(!safety) dbg(1, "no events");

	GList* l = mpart->note_selection;
	for(;l;l=l->next){
		MidiNote* note = l->data;
		arr_gl_draw_note(atr, part, note, 0xffffffff, z);
	}
}


static void
gl_set_colour_from_part_bg (AMPart* part)
{
	float r, g, b;
	am_palette_get_float(song->palette, part->bg_colour, &r, &g, &b, 0xff);
	glColor4f(r, g, b, 1.0);
}
