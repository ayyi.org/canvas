/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2008-2011 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * Modified by the GTK+ Team and others 1997-2000.
 */

#ifndef __mixer_hbox_h__
#define __mixer_hbox_h__

#include <gdk/gdk.h>
#ifdef USE_AYYI_CONTAINER
#include "ayyi_box.h"
#else
#include <gtk/gtkbox.h>
#endif

#ifdef USE_AYYI_CONTAINER
#define NS_TYPE_BOX AYYI_TYPE_BOX
#define NS_BOX(A) AYYI_BOX(A)
#define NsBoxChild AyyiBoxChild
#define NsBox AyyiBox
#define NS_CONTAINER AYYI_CONTAINER
#else
#define NS_TYPE_BOX GTK_TYPE_BOX
#define NS_BOX(A) GTK_BOX(A)
#define NsBoxChild GtkBoxChild
#define NsBox GtkBox
#define NS_CONTAINER GTK_CONTAINER
#endif
#define MIN_STRIP_WIDTH 10

G_BEGIN_DECLS

#define MIXER_TYPE_HBOX            (mixer_hbox_get_type ())
#define MIXER_HBOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MIXER_TYPE_HBOX, MixerHBox))
#define MIXER_HBOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MIXER_TYPE_HBOX, MixerHBoxClass))
#define MIXER_IS_HBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MIXER_TYPE_HBOX))
#define MIXER_IS_HBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MIXER_TYPE_HBOX))
#define MIXER_HBOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MIXER_TYPE_HBOX, MixerHBoxClass))


typedef struct _MixerHBox       MixerHBox;
typedef struct _MixerHBoxClass  MixerHBoxClass;

struct _MixerHBox
{
#ifdef USE_AYYI_CONTAINER
  AyyiBox    box;
#else
  GtkBox     box;
#endif
  GdkWindow* window;

  GtkWidget* scrollwin;   // external scroll window. must be set by users.

  guint8     button;      // button currently pressed or 0 if none.
  //GPtrArray* strips;      // array of Strip*. They are _not_ in any particular order.
  int        strip_width; // unmagnified width of each strip. Strips are magnified individually and per window.
  double     min_zoom;    // constrained by the window size and number of visible channels.
  double     zoom;        // this is _defined_ as the value that is shown by the mixer zoombar.

  //void       (*on_zoom_changed)(double);
};

struct _MixerHBoxClass
{
#ifdef USE_AYYI_CONTAINER
  AyyiBoxClass parent_class;
#else
  GtkBoxClass parent_class;
#endif

  gboolean (*move_handle) (MixerHBox*, GtkScrollType);
};


GType      mixer_hbox_get_type            (void) G_GNUC_CONST;
GtkWidget* mixer_hbox_new                 (gint spacing);
void       mixer_hbox_add_strip           (MixerHBox*, AyyiMixerStrip*);
void       mixer_hbox_set_zoom            (MixerHBox*, double zoom);
int        mixer_hbox_get_strip_pos       (MixerHBox*, AyyiMixerStrip*);
void       mixer_hbox_on_selection_change (GtkWidget*);


G_END_DECLS

#endif /* __mixer_hbox_h__ */
