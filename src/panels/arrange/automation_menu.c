/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2010-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "support.h"
#include "panels/arrange.h"
#include "window.statusbar.h"
#include "arrange/automation.h"
#include "automation_menu.h"

static void automation_on_menu_show      (GtkWidget*, Arrange*);
static void automation_menu_hide_control (GtkWidget*, Arrange*);
static void automation_point_add         (GtkWidget*, Arrange*);
static void automation_point_remove      (GtkWidget*, Arrange*);


static MenuDef _menu_def[] = {
    {"Add point",    G_CALLBACK(automation_point_add),         "zoom_in",  true},
    {"Delete point", G_CALLBACK(automation_point_remove),      "zoom_out", true},
    {"Hide",         G_CALLBACK(automation_menu_hide_control), "cross",    true},
};


static GtkWidget*
automation_menu_new (Arrange* arrange)
{
	GtkWidget* menu = arrange->automation.menu = gtk_menu_new();

	g_signal_connect(G_OBJECT(menu), "show", G_CALLBACK(automation_on_menu_show), arrange);

	return add_menu_items_from_defn(menu, _menu_def, G_N_ELEMENTS(_menu_def), arrange);
}

static Ptd mouse;

void
automation_menu_popup (Arrange* arrange, GdkEvent* event, Ptd _mouse)
{
	mouse = _mouse;

	if (!arrange->automation.menu) {
		automation_menu_new (arrange);
	}
	gtk_menu_popup(GTK_MENU(arrange->automation.menu), NULL, NULL, NULL, NULL, event->button.button, (guint32)(event->button.time));
}


static void
automation_on_menu_show (GtkWidget* menu, Arrange* arrange)
{
	PF;

	AyyiPanel* panel = (AyyiPanel*)arrange;
	if (!arrange_verify(panel)) return;

	// Store the mouse coords before the menu selection is made.
	gdk_window_get_pointer(((GtkWidget*)panel->window)->window, &arrange->mouse.x, &arrange->mouse.y, NULL);
}


static void
automation_menu_hide_control (GtkWidget* menu_item, Arrange* arrange)
{
	PF;

	ArrAutoPath* aap = automation_current_path(arrange->automation.sel.track);
	aap->hidden = true;

	arrange->automation.sel.subpath = -1;
	arrange->automation.sel.type = -1;

	arrange->canvas->on_auto_ctl_hide(arrange, aap);
}


static void
automation_point_add (GtkWidget* menu_item, Arrange* arrange)
{
	// TODO currently we split the current segment in half without knowing the mouse position - x is ok, y not done.
	PF;

	int ins_pos = arrange->automation.sel.subpath;
	g_return_if_fail(ins_pos > -1);

	int auto_type = arrange->automation.sel.type;
	ArrAutoPath* aap = automation_current_path(arrange->automation.sel.track);
	Curve* curve = aap->curve;
	BPath* bpath = (BPath*)curve->path;

#if 0
	int end = automation_bpath_get_end(bpath);
	if (!end){ pwarn ("bpath has zero length"); }
#endif

	curve_ins(curve, ins_pos);
	bpath = (BPath*)curve->path;

	BPath* bp  = &bpath[ins_pos];
	BPath* bp1 = &bpath[ins_pos - 1];
	BPath* bp2 = &bpath[ins_pos + 1];

	int x = mouse.x;
	float y = mouse.y - arrange->automation.sel.track->y * vzoom(arrange);
	dbg (0, "x=%i y=%0.f", x, y);

	x = x / hzoom(arrange);
	if (x < bp1->x3 || x > bp2->x3) pwarn ("x coord out of range. x=%i, should be %.2f (%i) < x < %.2f (%i)", x, bp1->x3, ins_pos - 1, bp2->x3, ins_pos + 1);

	bp->x3 = arr_px2samples(arrange, x);
	bp->y3 = y;
#ifdef DEBUG
	am_automation_print_path(curve);
#endif

	if (auto_type >= 2) pwarn("CHECK! autotype>=2");

	arrange->automation.sel.subpath = ins_pos;

  	arrange->canvas->on_auto_pt_add(arrange, aap, curve, ins_pos);

	typedef struct {Arrange* arrange; Curve* curve; int ins_pos;} C;

	void done (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;
		shell__statusbar_print(((AyyiPanel*)c->arrange)->window, 0, "automation point added");
	}

	am_automation_point_add(curve, ins_pos, done, AYYI_NEW(C,
		.arrange = arrange,
		.curve = curve,
		.ins_pos = ins_pos
	));
}


static void
automation_point_remove (GtkWidget* menu_item, Arrange* arrange)
{
	PF;

	int del_pos = arrange->automation.sel.subpath;
	if (!del_pos) { perr ("subpath not set"); return; }
	ArrAutoPath* aap = automation_current_path(track_list__track_by_index(arrange->priv->tracks, arrange->automation.sel.tnum));
	Curve* curve = aap->curve;

	if (!curve_get_length(curve)) { pwarn ("bpath has zero length"); return; }

	typedef struct
	{
		Arrange* arrange;
		Curve*   curve;
		int      del_pos;
	} C;

	void done (AyyiIdent id, GError** error, gpointer _c)
	{
		// this is only needed while a signal is not emitted from server for automation point remove

		C* c = _c;
		Curve* curve = c->curve;
		Arrange* arrange = c->arrange;

		int end = curve_get_length(curve) - 1;
		int new_selection = (c->del_pos == end) ? c->del_pos - 1 : c->del_pos;
		int auto_type = arrange->automation.sel.type;

		arrange->automation.sel.subpath = new_selection;

  		arrange->canvas->on_auto_pt_remove(arrange, arrange->automation.sel.track->auto_paths->pdata[auto_type], curve, new_selection);

		g_free(c);
	}

	am_automation_point_remove(curve, del_pos, done, AYYI_NEW(C,
		.arrange = arrange,
		.curve = curve,
		.del_pos = del_pos
	));
}
