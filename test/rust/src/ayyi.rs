#![allow(non_camel_case_types)]
#![allow(dead_code)]

use std;
use glib;
use glib_sys;
use libc;
use std::cell::RefCell;
use std::mem::transmute;
use ::std::os::raw::c_int;

pub type guint32 = ::std::os::raw::c_uint;
pub type gchar = ::std::os::raw::c_char;
pub type gfloat = f32;
pub type gpointer = *mut libc::c_void;
pub type GQuark = guint32;
pub type gint = ::std::os::raw::c_int;
pub type guint = ::std::os::raw::c_uint;
pub type gboolean = gint;
pub type gsize = ::std::os::raw::c_ulong;

#[repr(C)]
#[derive(Copy, Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct __BindgenBitfieldUnit<Storage, Align>
where
	Storage: AsRef<[u8]> + AsMut<[u8]>,
{
	storage: Storage,
	align: [Align; 0],
}

pub type Error = _GError;

#[repr(C)]
#[derive(Debug , Copy, Clone)]
pub struct _GError {
	pub domain : GQuark,
	pub code: gint,
	pub message : * mut gchar,
}

#[test]
fn bindgen_test_layout__GError () {
	assert_eq ! ( :: std :: mem :: size_of :: < _GError > ( ) , 16usize , concat ! ( "Size of: " , stringify ! ( _GError ) ) ) ;
	assert_eq ! ( :: std :: mem :: align_of :: < _GError > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _GError ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GError > ( ) ) ) . domain as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _GError ) , "::" , stringify ! ( domain ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GError > ( ) ) ) . code as * const _ as usize } , 4usize , concat ! ( "Offset of field: " , stringify ! ( _GError ) , "::" , stringify ! ( code ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GError > ( ) ) ) . message as * const _ as usize } , 8usize , concat ! ( "Offset of field: " , stringify ! ( _GError ) , "::" , stringify ! ( message ) ) );
}

extern "C" {
	#[link_name = "\u{1}song"]
	pub static mut song : * mut ClientController;
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _log {
	pub print_to_ui : ::std::option::Option <unsafe extern "C" fn (arg1: * const ::std::os::raw::c_char, arg2: c_int)>,
	pub to_stdout : gboolean,
	pub txtbuf : * mut GtkTextBuffer,
	//pub tag_green : * mut GtkTextTag,
	//pub tag_red : * mut GtkTextTag,
	//pub tag_orange : * mut GtkTextTag,
	//pub tag_yellow : * mut GtkTextTag
	pub _tag_green : * mut gchar,
	pub _tag_red : * mut gchar,
	pub _tag_orange : * mut gchar,
	pub _tag_yellow : * mut gchar,
}

pub type AyyiHandler2 = unsafe extern "C" fn (arg1: * mut Error, arg2: gpointer);

pub type GType = gsize;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GData {
	_unused : [ u8; 0 ],
}
pub type GData = _GData;

pub type GTypeClass = _GTypeClass;
/// GTypeClass:
///
/// An opaque structure used as the base of all classes.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GTypeClass {
	pub g_type : GType,
}

pub type GTypeInstance = _GTypeInstance;
/// GTypeInstance:
///
/// An opaque structure used as the base of all type instances.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GTypeInstance {
	pub g_class: * mut GTypeClass,
}

pub type GObject = _GObject;
/// GObject:
///
/// All the fields in the GObject structure are private
/// to the #GObject implementation and should never be accessed directly.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GObject {
	pub g_type_instance: GTypeInstance,
	pub ref_count: guint,
	pub qdata: * mut GData,
}

pub type AMIter = _am_iter;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _am_iter {
	pub i:      ::std::os::raw::c_int,
	pub ptr:    * mut::std::os::raw::c_void,
	pub list:   * mut glib_sys::GList,
	pub array:  * mut glib_sys::GPtrArray,
}
impl Default for _am_iter {
    fn default() -> _am_iter {
        unsafe { std::mem::zeroed() }
    }
}
#[test]
fn bindgen_test_layout__am_iter ( ) { assert_eq ! ( :: std :: mem :: size_of :: < _am_iter > ( ) , 32usize , concat ! ( "Size of: " , stringify ! ( _am_iter ) ) );
	assert_eq ! ( :: std :: mem :: align_of :: < _am_iter > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _am_iter ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _am_iter > ( ) ) ) . i as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _am_iter ) , "::" , stringify ! ( i)));
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _am_iter > ( ) ) ) . ptr as * const _ as usize } , 8usize , concat ! ( "Offset of field: " , stringify ! ( _am_iter ) , "::" , stringify ! ( ptr)));
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _am_iter > ( ) ) ) . list as * const _ as usize } , 16usize , concat ! ( "Offset of field: " , stringify ! ( _am_iter ) , "::" , stringify ! ( list)));
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _am_iter > ( ) ) ) . array as * const _ as usize } , 24usize , concat ! ( "Offset of field: " , stringify ! ( _am_iter ) , "::" , stringify ! ( array)));
}

pub type ModelCollection = _ModelCollection;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _ModelCollectionPrivate {
	_unused : [ u8 ; 0 ],
}

pub type ModelCollectionPrivate = _ModelCollectionPrivate;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _ModelCollection {
	pub parent_instance: GObject,
	pub priv_ : * mut ModelCollectionPrivate,
	pub selection : * mut glib_sys::GList,
	pub pending : * mut glib_sys::GList,
	pub user_data : * mut ::std::os::raw::c_void,
}

#[test]
fn bindgen_test_layout__ModelCollection () {
	assert_eq ! ( :: std :: mem :: size_of :: < _ModelCollection > ( ) , 56usize , concat ! ( "Size of: " , stringify ! ( _ModelCollection ) ) ) ;
	assert_eq ! ( :: std :: mem :: align_of :: < _ModelCollection > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _ModelCollection ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ModelCollection > ( ) ) ) . parent_instance as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _ModelCollection ) , "::" , stringify ! ( parent_instance ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ModelCollection > ( ) ) ) . priv_ as * const _ as usize } , 24usize , concat ! ( "Offset of field: " , stringify ! ( _ModelCollection ) , "::" , stringify ! ( priv_ ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ModelCollection > ( ) ) ) . selection as * const _ as usize } , 32usize , concat ! ( "Offset of field: " , stringify ! ( _ModelCollection ) , "::" , stringify ! ( selection ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ModelCollection > ( ) ) ) . pending as * const _ as usize } , 40usize , concat ! ( "Offset of field: " , stringify ! ( _ModelCollection ) , "::" , stringify ! ( pending ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ModelCollection > ( ) ) ) . user_data as * const _ as usize } , 48usize , concat ! ( "Offset of field: " , stringify ! ( _ModelCollection ) , "::" , stringify ! ( user_data ) ) ) ;
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union pthread_mutex_t {
	pub __data : __pthread_mutex_s,
	pub __size : [ :: std :: os :: raw :: c_char ; 40usize ],
	pub __align : :: std :: os :: raw :: c_long,
	_bindgen_union_align : [ u64 ; 5usize ],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct __pthread_mutex_s {
	pub __lock : :: std :: os :: raw :: c_int,
	pub __count : :: std :: os :: raw :: c_uint,
	pub __owner : :: std :: os :: raw :: c_int,
	pub __nusers : :: std :: os :: raw :: c_uint,
	pub __kind : :: std :: os :: raw :: c_int,
	pub __spins : :: std :: os :: raw :: c_short,
	pub __elision : :: std :: os :: raw :: c_short,
	pub __list : __pthread_list_t,
}

pub type __pthread_list_t = __pthread_internal_list;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct __pthread_internal_list {
	pub __prev : * mut __pthread_internal_list,
	pub __next : * mut __pthread_internal_list,
}

pub type AyyiIdx = ::std::os::raw::c_int;
pub type AyyiObjType = u32;

pub type AyyiSongPos = _AyyiSongPos;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AyyiSongPos {
	pub beat : :: std :: os :: raw :: c_int,
	pub sub : i16 , pub mu : i16
}

pub type GPos = _GPos;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GPos {
	pub beat : c_int,
	pub sub : :: std :: os :: raw :: c_ushort,
	pub tick : :: std :: os :: raw :: c_ushort,
}

pub type AyyiIdent = _AyyiIdent;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AyyiIdent {
	pub type_ : AyyiObjType,
	pub idx : AyyiIdx,
}

pub type AMPlugin = _plug;
#[repr(C)]
#[derive(Copy, Clone)]
pub struct _plug {
	pub name : [ :: std :: os :: raw :: c_char; 64usize ],
	pub type_ : c_int,
	pub shm_num : c_int,
}

pub const AMTRACKTYPE_TRK_TYPE_NULL : AMTrackType = 0;
pub const AMTRACKTYPE_TRK_TYPE_AUDIO : AMTrackType = 1;
pub const AMTRACKTYPE_TRK_TYPE_MIDI : AMTrackType = 2;
pub const AMTRACKTYPE_TRK_TYPE_BUS : AMTrackType = 3;
pub type AMTrackType = u32;
pub type TrackNum = c_int;

pub const PATHCODE_MOVETO : Pathcode = 0 ;
pub const PATHCODE_MOVETO_OPEN : Pathcode = 1 ;
pub const PATHCODE_CURVETO : Pathcode = 2;
pub const PATHCODE_LINETO : Pathcode = 3 ;
pub const PATHCODE_BP_END : Pathcode = 4;
pub type Pathcode = u32;

pub type BPath = _bpath;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _bpath {
	pub code : Pathcode,
	pub x1 : f64,
	pub y1 : f64,
	pub x2 : f64,
	pub y2 : f64,
	pub x3 : f64,
	pub y3 : f64,
}

pub type Curve = _curve;
#[repr(C)]
#[derive(Copy, Clone)]
pub struct _curve {
	pub path : * mut BPath,
	pub size : usize,
	pub len : usize,
	pub item_size : usize,
	pub grow_size : usize,
	pub name : * mut :: std :: os :: raw :: c_char,
	pub track : * mut AMTrack,
	pub plugin : * mut AMPlugin,
	pub auto_type : c_int,
	pub g_path : * mut :: std :: os :: raw :: c_void,
	pub mutex : pthread_mutex_t
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AMTrack__track_curves {
	pub vol : * mut Curve,
	pub pan : * mut Curve,
	pub _bitfield_1 : __BindgenBitfieldUnit < [ u8 ; 1usize ] , u8 >,
	pub __bindgen_padding_0 : [ u8 ; 7usize ],
}

pub type AMTrack = _AMTrack;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AMTrack {
	pub ident : AyyiIdent,
	pub type_ : AMTrackType,
	pub track_num : TrackNum,
	pub name : * mut :: std :: os :: raw :: c_char,
	pub visible : gboolean,
	pub user_notes : * mut glib_sys::GString,
	//pub meteradj : * mut GtkObject,
	pub bezier : _AMTrack__track_curves,
	pub controls : * mut glib_sys::GList,
}

pub type AMTrackList = _AMTrackList;
#[repr(C)]
#[derive(Copy , Clone)]
pub struct _AMTrackList {
	pub parent_instance : ModelCollection,
	pub priv_ : * mut AMTrackListPrivate,
	pub track : [ * mut AMTrack ; 48usize ],
	pub last : gint ,
}

pub type AMTrackListPrivate = _AMTrackListPrivate;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AMTrackListPrivate {
	_unused : [ u8 ; 0 ],
}

impl From<*mut _AMTrackList> for ModelCollection {
	fn from(a: * mut _AMTrackList) -> Self {
		unsafe {
			(*a).parent_instance
		}
	}
}

pub type AMObject = _AMObject;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AMObject {
	pub vals: * mut _AMObject__am_val,
	pub set : ::std::option::Option <unsafe extern "C" fn (arg1: * mut AMObject , property: c_int, arg2: AMVal, arg3: AyyiHandler2, arg4: gpointer)>,
	pub user_data : gpointer,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct _AMObject__am_val {
	pub val : AMVal,
	pub type_ : :: std :: os :: raw :: c_int,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union AMVal {
	pub i : c_int,
	pub f : f32,
	pub c : * mut :: std :: os :: raw :: c_char,
	pub pos : GPos,
	pub sp : AyyiSongPos,
	_bindgen_union_align : u64,
}

pub type QMap = _q_map;
#[repr(C)]
#[derive(Copy, Clone)]
pub struct _q_map {
	pub name : [ ::std::os::raw::c_char; 64usize ],
	pub size : c_int,
	pub pos : [ GPos; 33usize ],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _ClientControllerPrivate {
	_unused : [ u8; 0 ],
}
pub type ClientControllerPrivate = _ClientControllerPrivate;

pub type ClientController = _ClientController;
#[repr(C)]
#[derive(Copy, Clone)]
pub struct _ClientController {
	pub parent_instance : GObject,
	pub priv_ : * mut ClientControllerPrivate,
	pub loaded : gboolean,
	pub loc : [ AMObject ; 10usize ],
	pub end_auto : gboolean,
	pub sample_rate : gint,
	pub beats2bar : gfloat,
	pub audiodir : * mut gchar,
	pub peaksdir : * mut gchar,
	//pub tempo_adj : * mut GtkObject,
	pub tempo_adj : * mut gchar,
	pub q_settings : [ QMap; 5usize ],
	//pub snap_mode : AMSnapType,
	pub snap_mode : i32,
	pub tracks : * mut AMTrackList,
	//pub pool : * mut AMList,
	//pub channels : * mut AMArray,
	//pub parts : * mut AMList,
	//pub map_parts : * mut AMList,
	pub input_list : * mut glib_sys::GList,
	pub input_str : * mut gchar,
	pub bus_list : * mut glib_sys::GList,
	pub bus_strs : * mut gchar,
	pub plugins : * mut glib_sys::GList,
	pub palette : [ u32; 64usize ],
	pub periodic : guint,
	pub work_queue : * mut glib_sys::GList
}

pub type GtkTextBuffer = _GtkTextBuffer;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GtkTextTagTable {
	pub parent_instance : GObject,
	pub hash : * mut glib_sys::GHashTable,
	pub anonymous : * mut glib_sys::GSList,
	pub anon_count : gint,
	pub buffers : * mut glib_sys::GSList,
}

#[test]
fn bindgen_test_layout__GtkTextTagTable () {
	assert_eq ! ( :: std :: mem :: size_of :: < _GtkTextTagTable > ( ) , 56usize , concat ! ( "Size of: " , stringify ! ( _GtkTextTagTable ) ) ) ;
	assert_eq ! ( :: std :: mem :: align_of :: < _GtkTextTagTable > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _GtkTextTagTable ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GtkTextTagTable > ( ) ) ) . parent_instance as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _GtkTextTagTable ) , "::" , stringify ! ( parent_instance ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GtkTextTagTable > ( ) ) ) . hash as * const _ as usize } , 24usize , concat ! ( "Offset of field: " , stringify ! ( _GtkTextTagTable ) , "::" , stringify ! ( hash ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GtkTextTagTable > ( ) ) ) . anonymous as * const _ as usize } , 32usize , concat ! ( "Offset of field: " , stringify ! ( _GtkTextTagTable ) , "::" , stringify ! ( anonymous ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GtkTextTagTable > ( ) ) ) . anon_count as * const _ as usize } , 40usize , concat ! ( "Offset of field: " , stringify ! ( _GtkTextTagTable ) , "::" , stringify ! ( anon_count ) ) ) ;
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _GtkTextTagTable > ( ) ) ) . buffers as * const _ as usize } , 48usize , concat ! ( "Offset of field: " , stringify ! ( _GtkTextTagTable ) , "::" , stringify ! ( buffers ) ) );
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GtkTextLogAttrCache {
	_unused : [ u8; 0 ],
}
pub type GtkTextLogAttrCache = _GtkTextLogAttrCache;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _GtkTextBuffer {
	pub parent_instance : GObject,
	//pub tag_table : * mut GtkTextTagTable,
	//pub btree : * mut GtkTextBTree,
	pub clipboard_contents_buffers : * mut glib_sys::GSList,
	pub selection_clipboards : * mut glib_sys::GSList,
	pub log_attr_cache : * mut GtkTextLogAttrCache,
	pub user_action_count : guint,
	pub _bitfield_1 : __BindgenBitfieldUnit <[ u8; 1usize ], u8>,
	pub __bindgen_padding_0 : [ u8; 3usize ],
}

#[repr(C)]
#[derive(Debug , Copy , Clone)]
pub struct _AyyiClientPrivate {
	_unused : [ u8; 0 ],
}
pub type AyyiClientPrivate = _AyyiClientPrivate;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _AyyiServicePrivate {
	_unused : [ u8; 0 ],
}
pub type AyyiServicePrivate = _AyyiServicePrivate;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _ayyi_client {
	pub service : * mut AyyiService,
	pub services : * mut * mut AyyiService,
	pub got_shm : c_int,
	pub offline : gboolean,
	pub signal_mask : c_int,
	pub log : _log,
	pub launch_info : * mut glib_sys::GList,
	pub priv_ : * mut AyyiClientPrivate,
}
pub type AyyiClient = _ayyi_client;

#[test]
fn bindgen_test_layout__ayyi_client(){
	assert_eq ! ( :: std :: mem :: size_of :: < _ayyi_client > ( ) , 104usize , concat ! ( "Size of: " , stringify ! ( _ayyi_client ) ) );
	assert_eq ! ( :: std :: mem :: align_of :: < _ayyi_client > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _ayyi_client ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ).service as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( service ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ).services as * const _ as usize } , 8usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( services ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . got_shm as * const _ as usize } , 16usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( got_shm ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . offline as * const _ as usize } , 20usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( offline ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . signal_mask as * const _ as usize } , 24usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( signal_mask ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . log as * const _ as usize } , 32usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( log ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . launch_info as * const _ as usize } , 88usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( launch_info ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _ayyi_client > ( ) ) ) . priv_ as * const _ as usize } , 96usize , concat ! ( "Offset of field: " , stringify ! ( _ayyi_client ) , "::" , stringify ! ( priv_ ) ) );
}

pub type AyyiService = _service;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct _service {
	pub service : * mut ::std::os::raw::c_char,
	pub path : * mut ::std::os::raw::c_char,
	pub interface : * mut ::std::os::raw::c_char,
	pub ping : ::std::option::Option <unsafe extern "C" fn (arg1: ::std::option::Option <unsafe extern "C" fn (arg1: * const ::std::os::raw::c_char)>)>,
	pub segs : * mut glib_sys::GList,
	pub priv_ : * mut AyyiServicePrivate,
}

#[test]
fn bindgen_test_layout__service () {
	assert_eq ! ( :: std :: mem :: size_of :: < _service > ( ) , 48usize , concat ! ( "Size of: " , stringify ! ( _service ) ) );
	assert_eq ! ( :: std :: mem :: align_of :: < _service > ( ) , 8usize , concat ! ( "Alignment of " , stringify ! ( _service ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . service as * const _ as usize } , 0usize , concat ! ( "Offset of field: " , stringify ! ( _service ) , "::" , stringify ! ( service ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . path as * const _ as usize }, 8usize , concat ! ( "Offset of field: " , stringify ! ( _service ), "::" , stringify ! ( path ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . interface as * const _ as usize } , 16usize , concat ! ( "Offset of field: " , stringify ! ( _service ) , "::" , stringify ! ( interface ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . ping as * const _ as usize } , 24usize , concat ! ( "Offset of field: " , stringify ! ( _service ) , "::" , stringify ! ( ping ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . segs as * const _ as usize } , 32usize , concat ! ( "Offset of field: " , stringify ! ( _service ) , "::" , stringify ! ( segs ) ) );
	assert_eq ! ( unsafe { & ( * ( :: std :: ptr :: null :: < _service > ( ) ) ) . priv_ as * const _ as usize } , 40usize , concat ! ( "Offset of field: " , stringify ! ( _service ) , "::" , stringify ! ( priv_ ) ) );
}

extern "C" {
	pub fn ayyi_client_init () -> * mut AyyiClient;
    pub fn am__init(arg1: *mut *mut AyyiService) -> *mut AyyiClient;
	pub fn model_collection_iter_init (arg1: * mut ModelCollection, arg2: * mut AMIter);
	pub fn model_collection_iter_next (arg1: * mut ModelCollection, arg2: * mut AMIter) -> * mut::std::os::raw::c_void;
}
extern "C" { pub fn am__connect_all (arg1: AyyiHandler2, arg2: gpointer); }


#[cfg_attr(feature = "cargo-clippy", allow(transmute_ptr_to_ref))]
unsafe extern "C" fn trampoline(e: * mut Error, func: gpointer) {

    let func: &RefCell<Box<FnMut(* mut Error) + 'static>> = transmute(func);

	// this is where the callback fn is actuallly run
    (&mut *func.borrow_mut())(e);
}

fn into_raw<F: FnMut(* mut glib::Error)> (func: F) -> gpointer {
    let func: Box<RefCell<Box<FnMut(* mut glib::Error)>>> = Box::new(RefCell::new(Box::new(func)));
    Box::into_raw(func) as gpointer
}

pub fn connect_all<F>(func: F)
where F: FnMut(* mut glib::Error) {
    unsafe {
        am__connect_all(Some(trampoline).unwrap(), into_raw(func))
    }
}

