/**
 * @file webkit.c WebKit browser module for Liferea
 *
 * Copyright (C) 2007 Lars Lindner <lars.lindner@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <webkit/webkit.h>

typedef struct LifereaHtmlView HtmlView;

#include <gdl/gdl.h>
#include <debug/debug.h>
#include "html/html_plugin.h"
#include "html/ui_htmlview.h"
#define HANDLED TRUE
#define NOT_HANDLED FALSE

#if 0
static gboolean webkit_on_navigation_requested (WebKitWebView*, WebKitWebFrame*, WebKitNetworkRequest*, WebKitWebNavigationAction*, WebKitWebPolicyDecision*, gpointer);
static void     webkit_on_resource_requested   (WebKitWebView*, WebKitWebFrame*, WebKitWebResource*, WebKitNetworkRequest*, WebKitNetworkResponse*, gpointer);
#endif

static void webkit_init   () {}
static void webkit_deinit () {}

static void
webkit_write_html (GtkWidget* scrollpane, const gchar* string, guint length, const gchar* base, const gchar* mime_type)
{
	GtkWidget* htmlwidget = gtk_bin_get_child (GTK_BIN (scrollpane));

	mime_type =
		g_ascii_strcasecmp (mime_type, "application/xhtml+xml") == 0
		? "application/xhtml"
		: mime_type;
	dbg(2, "mime_type=%s base=%s length=%i visible=%i width=%i,%i height=%i", mime_type, base, length, GTK_WIDGET_VISIBLE(htmlwidget), scrollpane->allocation.width, htmlwidget->allocation.width, htmlwidget->allocation.height);
	webkit_web_view_load_string (WEBKIT_WEB_VIEW (htmlwidget), string, mime_type, "UTF-8", base);
}

static void
webkit_load_commited(WebKitWebView* web_view, WebKitWebFrame* web_frame, gpointer user_data)
{
	//a page-load has just started.
}

static void
webkit_title_changed(WebKitWebView* web_view, WebKitWebFrame* web_frame, const gchar* title, gpointer data)
{
	PF;
	//ui_tabs_set_title(GTK_WIDGET(page), title);
}

static void
webkit_progress_changed(WebKitWebView* page, gint progress, gpointer data)
{
	dbg(2, "progress=%i", progress);
}

static void
webkit_on_link_hover (WebKitWebView* page, const gchar* title, const gchar* url, gpointer data)
{
	//this gets called for mouseovers, amongst other things...

	HtmlView* htmlview = g_object_get_data (G_OBJECT (page), "htmlview");
	gchar* selectedURL = g_object_get_data (G_OBJECT (page), "selectedURL");
	g_free (selectedURL);
		
	if (url) {
		selectedURL = g_strdup (url);

		/* overwrite or clear last status line text */
	} else {
		selectedURL = NULL;
	}
	htmlview_on_url (htmlview, selectedURL);
	
	g_object_set_data (G_OBJECT(page), "selectedURL", selectedURL);
}

static gboolean
webkit_on_button_press(GtkWidget* widget, GdkEventButton* event, gpointer data)
{
	//clicks are propogated, as we need them to get to webkit, but we set a gdl flag to prevent redocking operations.
	PF;
	AyyiPanel* panel = (AyyiPanel*)data;
	//g_return_val_if_fail(AYYI_IS_PANEL(panel), NOT_HANDLED);

	if (event->button == 1) {
		GdlDockMaster* master = GDL_DOCK_MASTER(GDL_DOCK_OBJECT(panel)->master);
		if (master) {
			switch (event->type) {
				case GDK_BUTTON_PRESS:
					dbg(2, "setting lock...");
					master->redock_lock = TRUE;
					break;
				case GDK_BUTTON_RELEASE:
					dbg(2, "clearing lock...");
					master->redock_lock = FALSE;
					break;
				default:
					break;
			}
		}
	}
	return NOT_HANDLED;
}

static GtkWidget*
webkit_new (HtmlView* htmlview, AyyiPanel* panel, gboolean forceInternalBrowsing) 
{
	PF;
	
	GtkWidget* scrollpane = gtk_scrolled_window_new(NULL, NULL);

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollpane), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollpane), GTK_SHADOW_IN);
	
	/* create html widget and pack it into the scrolled window */
	GtkWidget* widget = webkit_web_view_new();
	WebKitWebView* web_view = WEBKIT_WEB_VIEW (widget);
	
	gtk_container_add (GTK_CONTAINER (scrollpane), GTK_WIDGET (web_view));

	g_object_set_data (G_OBJECT (web_view), "htmlview", htmlview);
	g_object_set_data (G_OBJECT (web_view), "internal_browsing", GINT_TO_POINTER (forceInternalBrowsing));

	g_signal_connect (web_view, "load-committed", G_CALLBACK (webkit_load_commited), web_view);
	g_signal_connect (web_view, "title-changed", G_CALLBACK (webkit_title_changed), web_view);
	g_signal_connect (web_view, "load-progress-changed", G_CALLBACK (webkit_progress_changed), web_view);
	g_signal_connect (web_view, "hovering-over-link", G_CALLBACK (webkit_on_link_hover), web_view);
	g_signal_connect (G_OBJECT(web_view), "button-press-event", G_CALLBACK (webkit_on_button_press), panel);
	g_signal_connect (G_OBJECT(web_view), "button-release-event", G_CALLBACK (webkit_on_button_press), panel);
#if 0
	g_signal_connect (web_view, "navigation-policy-decision-requested", G_CALLBACK (webkit_on_navigation_requested), web_view);
	g_signal_connect (web_view, "resource-request-starting", G_CALLBACK (webkit_on_resource_requested), web_view);
#endif

	gtk_widget_show (widget);
	return scrollpane;
}

#if 0
static gboolean
webkit_on_navigation_requested(WebKitWebView* web_view, WebKitWebFrame* frame, WebKitNetworkRequest* request, WebKitWebNavigationAction* navigation_action, WebKitWebPolicyDecision* policy_decision, gpointer user_data)
{
	const gchar* uri = webkit_network_request_get_uri(request);
	dbg(0, "uri=%s", uri);

	return false; // use default behaviour
}

static void
webkit_on_resource_requested(WebKitWebView* web_view, WebKitWebFrame* web_frame, WebKitWebResource* resource, WebKitNetworkRequest* request, WebKitNetworkResponse* response, gpointer user_data)
{
	const gchar* uri = webkit_web_resource_get_uri(resource);
	dbg(0, "uri=%s", uri);
}
#endif

static void
webkit_launch_url (GtkWidget* scrollpane, const gchar* url)
{
	dbg(2, "url=%s", url);
#ifdef LATER
	//copy local file for preprocessing as workaround for webkit base url issues.
	if(strstr(url, "file://")){
		const guchar* from = url + 7;
		const guchar* to = g_strconcat("/tmp/ayyi/", basename(from), NULL); //FIXME tmp should be config option.
		char* copy;
		dbg(2, "%s --> %s", from, to);
		if((copy = file_copy(from, to))){
			pwarn("%s", copy);
			g_free(copy);
		}
		g_free((guchar*)to);
	}
#endif

	webkit_web_view_open (WEBKIT_WEB_VIEW (gtk_bin_get_child (GTK_BIN (scrollpane))), url);
}

static void
webkit_change_zoom_level (GtkWidget *scrollpane, gfloat zoomLevel)
{
	dbg(0, "FIXME!");
}

static gfloat
webkit_get_zoom_level (GtkWidget *scrollpane)
{
	dbg(0, "FIXME!");
	return 1.0;
}

static gboolean
webkit_scroll_pagedown (GtkWidget *scrollpane)
{
	return NOT_HANDLED;
}

static struct htmlviewPlugin webkitInfo = {
	.api_version    = HTMLVIEW_PLUGIN_API_VERSION,
	.name           = "WebKit",
	.priority       = 100,
	.externalCss    = FALSE,
	.plugin_init    = webkit_init,
	.plugin_deinit  = webkit_deinit,
	.create         = webkit_new,
	.write          = webkit_write_html,
	.launch         = webkit_launch_url,
	.zoomLevelGet   = webkit_get_zoom_level,
	.zoomLevelSet   = webkit_change_zoom_level,
	.scrollPagedown = webkit_scroll_pagedown,
	.setProxy       = NULL,
	.setOffLine     = NULL
};

static struct htmlplugin pi = {
	PLUGIN_API_VERSION,
	"WebKit Rendering Plugin",
	PLUGIN_TYPE_HTML_RENDERER,
	&webkitInfo
};

DECLARE_PLUGIN(pi);
DECLARE_HTMLVIEW_PLUGIN(webkitInfo);
