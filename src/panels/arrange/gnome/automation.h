/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __g_automation_h__
#define __g_automation_h__

#include <model/automation.h>

void  g_automation_init               ();
void  g_automation_uninit             ();
void  automation_ctlpts_init          (Arrange*);
void  automation_track_draw           (Arrange*, ArrTrk*);
void  g_automation_track_destroy      (Arrange*, ArrTrk*);
void  g_automation_curves_undraw      (Arrange*);
void  automation_view_on_size_change  (Arrange*);
void  automation_on_curves_change     (Arrange*);
void  g_automation_curve_draw         (Arrange*, Curve*);
void  automation_curve_draw_fast      (Arrange*, Curve*);
void  g_automation_on_new_curve       (Arrange*, Curve*);

void  automation_view_toggle          (Arrange*);
void  automation_show                 (Arrange*);
void  automation_hide                 (Arrange*);

void  g_automation_view_vol           (ArrTrk*);
bool  g_automation_view_pan           (ArrTrk*);
bool  g_automation_show_control       (GtkWidget*, gpointer);

void  arr_k_next_node                 (GtkAccelGroup*, Arrange*);

int   automation_view__get_segment    (Arrange*, BPath*, double mousex, int* seg);
void  automation_view__select_segment (Arrange*, ArrAutoPath*, int seg);

#endif
