/*
  This file is part of the Ayyi Project. http://ayyi.org
  Copyright (C) 2007-2011 Tim Orford
 */
#include <config.h>
#include <glib.h>
#include <clutter/clutter.h>

#ifndef _HAVE_CLUTTER_PART_H
#define _HAVE_CLUTTER_PART_H


G_BEGIN_DECLS

#define CLUTTER_TYPE_PART            clutter_part_get_type()
#define CLUTTER_PART(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CLUTTER_TYPE_PART, ClutterPart))
#define CLUTTER_PARTCLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), CLUTTER_TYPE_PART, ClutterPartClass))
#define CLUTTER_IS_PART(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CLUTTER_TYPE_PART))
#define CLUTTER_IS_PART_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CLUTTER_TYPE_PART))
#define CLUTTER_PART_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CLUTTER_TYPE_PART, ClutterPartClass))

typedef struct _ClutterPart        ClutterPart;
typedef struct _ClutterPartClass   ClutterPartClass;
typedef struct _ClutterPartPrivate ClutterPartPrivate;

struct _ClutterPart
{
	ClutterGroup         parent;

	Arrange*             arrange;
	AMPart*              part;
	ClutterRectangle*    border;
	ClutterActor*        waveform;

	Ptf                  target_position;

	ClutterPartPrivate*  priv;
};

struct _ClutterPartClass 
{
	/*< private >*/
	ClutterGroupClass parent_class;

	void (*_clutter_part_1) (void);
	void (*_clutter_part_2) (void);
	void (*_clutter_part_3) (void);
	void (*_clutter_part_4) (void);
}; 

GType clutter_part_get_type (void) G_GNUC_CONST;

ClutterActor* clutter_part_new    (Arrange*, AMPart*);
void          clutter_part_redraw (ClutterPart*, AMChangeType);
void          clutter_part_resize (ClutterPart*, float, float);
void          clutter_part_select (ClutterPart*, gboolean);


G_END_DECLS

#endif
