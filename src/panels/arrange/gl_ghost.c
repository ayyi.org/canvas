/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

static AGl* agl = NULL;


AGlActor*
ghost_actor (gpointer _)
{
	agl = agl_get_instance();

	void ghost_set_state (AGlActor* actor)
	{
		PLAIN_COLOUR2(agl->shaders.plain) = 0x9999993f;
	}

	bool ghost_draw (AGlActor* actor)
	{
		agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));

		return true;
	}

	void ghost_init (AGlActor* actor)
	{
	}

	AGlActor* actor = agl_actor__new (AGlActor,
		.name = g_strdup("Ghost"),
		.program = agl->shaders.plain,
		.init = ghost_init,
		.set_state = ghost_set_state,
		.paint = ghost_draw,
		.region = {0, 0, 1, 1}
	);

	return (AGlActor*)actor;
}

