uniform vec2 modelview;
uniform vec2 translate;

attribute vec2 position;

varying vec2 MCposition;

void main () 
{
   MCposition = position.xy;
   gl_Position = vec4(vec2(1., -1.) * (position + translate) / modelview - vec2(1.0, -1.0), 1.0, 1.0);
}
