/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __filemanager_c__
#include "global.h"
#include <file_manager/file_manager.h>
#include "support.h"
#include "pool_model.h"
#include "toolbar.h"
#include "../shortcuts.h"
#include "settings.h"
#include "panels/filemanager.h"

extern SMConfig* config;
extern void print_icon_list();
GList* themes = NULL;

static GObject* fm_construct   (GType, guint n_construct_properties, GObjectConstructParam*);
static void     fm_on_ready    (AyyiPanel*);
static void**   fm_get_config  (AyyiPanel*);
static void     fm_add_to_pool (GtkMenuItem*, gpointer);
static void     fm_set_path    (AyyiFilePanel*, const char*);

static GList*   icon_theme_menu      ();
static void     icon_theme_set_theme (const char*);

G_DEFINE_TYPE (AyyiFilePanel, ayyi_file_panel, AYYI_TYPE_PANEL)

static ConfigParam* params[] = {
	&(ConfigParam){"path", G_TYPE_STRING, {.c=(gpointer)fm_set_path}, {.c=""}, {.c=""}, {.c=""}},
	NULL
};


AMAccel fm_keys[] = {
	{"Add to Pool", {{(char)'y',       0}, {0, 0}}, fm_add_to_pool, NULL},
};


static void
ayyi_file_panel_class_init (AyyiFilePanelClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS(klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	panel->name           = "Filemanager";
	panel->accel_group    = gtk_accel_group_new();
	panel->action_group   = shortcuts_add_group("File manager");
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->config         = (ConfigParam**)params;
	panel->default_size.x = 280;
	panel->default_size.y = 350;
	panel->on_ready       = fm_on_ready;
	panel->get_config     = fm_get_config;

	g_object_class->constructor = fm_construct;
}


static GObject*
fm_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if (_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_file_panel_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_FILE_PANEL);

	return g_object;
}


static void
ayyi_file_panel_init (AyyiFilePanel* object)
{
}


static void
fm_on_ready (AyyiPanel* panel)
{
	AyyiFilePanel* self = (AyyiFilePanel*)panel;
	AyyiPanelClass* panel_class = AYYI_PANEL_GET_CLASS(panel);

	GList* widgets = gtk_container_get_children((GtkContainer*)panel->vbox);
	bool done = g_list_length(widgets) > 1;
	g_list_free(widgets);
	if (done) return;

#if 0
	AyyiFilemanager* fm = file_manager__get();
	ayyi_libfilemanager_set_icon_theme (fm, config->icon_theme ? config->icon_theme : "Mist");
	GtkWidget* file_view = view_details_new(fm);
	/*
	GdkColor fg = {0, 0xffff, 0, 0};
	GdkColor bg = {0, 0, 0, 0};
	view_details_set_alt_colours(file_view, &bg, &fg);
	*/
	fm__change_to(fm, g_get_home_dir(), NULL);
	//panel_pack(VIEW_DETAILS(file_view)->scroll_win, EXPAND_TRUE);
#else
	GtkWidget* filer = file_manager__new_window(g_get_home_dir());
	AyyiFilemanager* fm = file_manager__get();
	ayyi_filemanager_set_icon_theme (fm, config->icon_theme ? config->icon_theme : "Mist");
#endif
	panel_pack(filer, EXPAND_TRUE);

	config_load_window_instance(panel);

	// context menu
	GimpActionGroup* action_group = panel_class->action_group;
	make_accels(panel_class->accel_group, action_group, fm_keys, sizeof(AMAccel)/sizeof(fm_keys[0]), self);
	int k = 0;
	GList* actions = gtk_action_group_list_actions(GTK_ACTION_GROUP(action_group));
	for (GList* a=actions;a;a=a->next) {
		GtkAction* action = a->data;
		AMAccel* key = &fm_keys[k++];

		// TODO a closure was already created for this action in make accels.
		GClosure* closure = g_cclosure_new(G_CALLBACK(key->callback), self, NULL);
		g_signal_connect_closure(G_OBJECT(action), "activate", closure, FALSE);

		fm__add_menu_item(action);
	}
	g_list_free(actions);

	if (!themes)
		icon_theme_menu();
	if (themes) {
		GtkWidget* menu = fm->menu;
		GtkWidget* theme_menu = gtk_image_menu_item_new_with_label("Icon Themes");
		GtkIconSet* set = gtk_style_lookup_icon_set(gtk_widget_get_style(menu), GTK_STOCK_PROPERTIES);
		GdkPixbuf* pixbuf = gtk_icon_set_render_icon(set, gtk_widget_get_style(menu), GTK_TEXT_DIR_LTR, GTK_STATE_NORMAL, GTK_ICON_SIZE_MENU, menu, NULL);
		GtkWidget* ico = gtk_image_new_from_pixbuf(pixbuf);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(theme_menu), ico);

		gtk_menu_item_set_submenu(GTK_MENU_ITEM(theme_menu), themes->data);

		void on_theme_select (GtkMenuItem* widget, gpointer user_data)
		{
			GtkLabel* label = GTK_LABEL(gtk_bin_get_child((GtkBin*)widget));
			config->icon_theme = (char*)gtk_label_get_text(label);
		}

		GList* td = gtk_container_get_children(themes->data);
		GList* l = td;
		for (;l;l=l->next) {
			GtkMenuItem* item = l->data;
			g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(on_theme_select), NULL);
		}
		g_list_free(td);

		fm__add_submenu(theme_menu);
	}
}


static void**
fm_get_config (AyyiPanel* panel)
{
	AyyiFilemanager* fm = file_manager__get();

	void** config = g_malloc0(sizeof(void*) * G_N_ELEMENTS(params));
	config[0] = &fm->real_path;

	return config;
}


static void
fm_add_to_pool (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
	GList* selected = fm__selected_items(file_manager__get());
	if(selected){
		for(GList* l=selected;l;l=l->next){
			char* file = l->data;
			pool_import_file(file, NULL);
			g_free(file);
		}
		g_list_free(selected);
	}
}


static void
fm_set_path (AyyiFilePanel* f, const char* path)
{
	AyyiFilemanager* fm = file_manager__get();
	if (fm) {
		fm__change_to(fm, path, NULL);
	}
}


extern char theme_name[];
extern GtkIconTheme* icon_theme;


static void
get_theme_names (GPtrArray* names)
{
	void add_themes_from_dir (GPtrArray* names, const char* dir)
	{
		if (access(dir, F_OK) != 0)	return;

		GPtrArray* list = list_dir((guchar*)dir);
		g_return_if_fail(list != NULL);

		for (int i = 0; i < list->len; i++){
			char* index_path = g_build_filename(dir, list->pdata[i], "index.theme", NULL);
			
			if (access(index_path, F_OK) == 0) {
				g_ptr_array_add(names, list->pdata[i]);
			}
			else g_free(list->pdata[i]);

			g_free(index_path);
		}

		g_ptr_array_free(list, TRUE);
	}

	gint n_dirs = 0;
	gchar** theme_dirs = NULL;
	gtk_icon_theme_get_search_path(icon_theme, &theme_dirs, &n_dirs); // dir list is derived from XDG_DATA_DIRS
	int i; for (i = 0; i < n_dirs; i++) add_themes_from_dir(names, theme_dirs[i]);
	g_strfreev(theme_dirs);

	g_ptr_array_sort(names, strcmp2);
}


gboolean
check_default_theme (gpointer data)
{
	// The default gtk icon theme "hi-color" does not contain any mimetype icons.

	static char* names[] = {"audio-x-wav", "audio-x-generic", "gnome-mime-audio"};

	if (!theme_name[0]) {
		GtkIconInfo* info;
		int i = 0;
		while (i++ < G_N_ELEMENTS(names) && !(info = gtk_icon_theme_lookup_icon(icon_theme, names[i], ICON_HEIGHT, 0)));
		if (info) {
			gtk_icon_info_free(info);
		} else {
			warnprintf("default icon theme appears not to contain audio mime-type icons\n");

			// TODO use a random fallback theme
		}
	}
	return G_SOURCE_REMOVE;
}


static void
icon_theme_set_theme (const char* name)
{
	mime_type_clear();

	dbg(1, "setting theme: %s.", theme_name);

	if(name && name[0]){
		if(!*theme_name) icon_theme = gtk_icon_theme_new(); // the old icon theme cannot be updated
		g_strlcpy(theme_name, name, 64);
		gtk_icon_theme_set_custom_theme(icon_theme, theme_name);
	}

	if(strlen(theme_name))
														#if 0        // also done in libfilemanager?
			gtk_icon_theme_set_custom_theme(icon_theme, theme_name);
														#endif
														;
	else
		g_idle_add(check_default_theme, NULL);

#if 0 // test is disabled as it is not reliable
	while (1)
	{
		GtkIconInfo* info = gtk_icon_theme_lookup_icon(icon_theme, "mime-application:postscript", ICON_HEIGHT, 0);
		if (!info)
		{
			//dbg(1, "looking up test icon...");
			info = gtk_icon_theme_lookup_icon(icon_theme, "gnome-mime-application-postscript", ICON_HEIGHT, 0);
		}
		if (info)
		{
			dbg(0, "got test icon ok. Using theme '%s'", theme_name);
			//print_icon_list();
			g_object_unref(info);
			return;
		}

		if (strcmp(theme_name, DEFAULT_THEME) == 0) break;

		warnprintf("Icon theme '%s' does not contain MIME icons. Using default theme instead.\n", theme_name);
		
		strcpy(theme_name, DEFAULT_THEME);
	}
#endif

	gtk_icon_theme_rescan_if_needed(icon_theme);
}


/*
 *  Build a menu list of available themes.
 */
static GList*
icon_theme_menu ()
{
	if (_debug_) {
		gint n_elements;
		gchar** path[64];
		gtk_icon_theme_get_search_path(icon_theme, path, &n_elements);
		int i;
		for(i=0;i<n_elements;i++){
			dbg(2, "icon_theme_path=%s", path[0][i]);
		}
		g_strfreev(*path);
	}

	void icon_theme_on_select (GtkMenuItem* menuitem, gpointer user_data)
	{
		g_return_if_fail(menuitem);

		const gchar* name = g_object_get_data(G_OBJECT(menuitem), "theme");
		dbg(1, "theme=%s", name);

	#if 0
	#ifdef DEBUG
		if(_debug_) print_icon_list();
	#endif
	#endif
		icon_theme_set_theme(name);
		//application_emit_icon_theme_changed(app, name);
	}


	GtkWidget* menu = gtk_menu_new();

	GPtrArray* names = g_ptr_array_new();
	get_theme_names(names);

	for (int i = 0; i < names->len; i++) {
		char* name = names->pdata[i];
		dbg(2, "name=%s", name);

		GtkWidget* item = gtk_menu_item_new_with_label(name);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);

		g_object_set_data(G_OBJECT(item), "theme", g_strdup(name)); // make sure this string is free'd when menu is updated.

		g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(icon_theme_on_select), NULL);

		g_free(name);
	}
	gtk_widget_show_all(menu);

	g_ptr_array_free(names, TRUE);

	return themes = g_list_append(NULL, menu);
}
