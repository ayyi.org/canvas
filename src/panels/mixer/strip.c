/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include <math.h>
#include "model/channel.h"
#include "model/song.h"
#include "model/plugin.h"
#include "model/track_list.h"
#include "widgets/svgtogglebutton.h"
#include "widgets/rotary.h"
#include "icon.h"
#include "support.h"
#include "style.h"
#include "src/meter.h"
#include "io.h"
#include "widgets/fader_label.h"
#include "widgets/editablelabelbutton.h"
#include "panels/mixer.h"
#include "panels/mixer/insert.h"

#define PAN_RANGE 20.0
#define SMALL_ICON_SIZE 12
#define COMBO_GRID
#ifdef COMBO_GRID
  #include "gtkcombogrid.h"
#endif

extern SMConfig*  config;
extern GtkWidget* pressed_widget;
extern void       mixer_menu__update           (MixerWin*);

extern GType      ayyi_inspector_win_get_type  ();

#include "fader_bay.c"

static void       ayyi_mixer_strip_finalize    (GObject*);
static GtkWidget* _strip__add_widgets          (AyyiPanel*, Strip*, MixerOptions*);
static GtkWidget* strip__input_selector_new    (AyyiMixerStrip*);
static GtkWidget* strip__output_selector_new   (AyyiMixerStrip*);
static void       strip__on_realise            (GtkWidget*, gpointer);
static void       strip__on_allocate           (GtkWidget*, GtkAllocation*);
static bool       strip__on_clicked            (GtkWidget*, GdkEventButton*, gpointer);
static bool       strip__on_label_clicked      (GtkWidget*, GdkEventButton*, gpointer);
static void       strip__on_pan_value_changed  (GtkWidget*, AMChannel*);
static bool       strip__on_aux_widget_value   (GtkRange*, GtkScrollType, gdouble, AyyiMixerStrip*);
static int        aux_num_lookup               (Observable*, AyyiMixerStrip*);

static gint       strip__drag_received         (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer user_data);

static void       strip__on_input_toggled      (GtkCellRendererToggle*, gchar*, GtkComboGrid*);
/*static */void   strip__on_output_toggled     (GtkCellRendererToggle*, gchar*, GtkComboGrid*);
#if 0
static void       inserts_print                (Strip*);
#endif

static void       bmute_on_press               (GtkWidget*, AyyiMixerStrip*);
static void       bsolo_on_press               (GtkWidget*, AyyiMixerStrip*);
static void       brec_on_press                (GtkWidget*, AyyiMixerStrip*);
static void       bsdef_on_press               (GtkWidget*, GdkEventButton*, gpointer);

struct _AyyiMixerStripPrivate {
	GtkWidget*   wmute;
	GtkWidget*   wsolo;
	GtkWidget*   wsdef;
	GtkWidget*   wrec;

	GtkWidget*        spacer2;        //TEMP! spacer below insert widgets.
	GtkWidget*        spacer3;        //TEMP! spacer widget below sends.

	GtkWidget*   rsiz_handle;    // event box for channel resizing.
};

#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

G_DEFINE_TYPE_WITH_PRIVATE (AyyiMixerStrip, ayyi_mixer_strip, GTK_TYPE_FIXED)

enum  {
	AYYI_MIXER_STRIP_DUMMY_PROPERTY
};


AyyiMixerStrip*
ayyi_mixer_strip_construct (GType object_type)
{
#ifdef HAVE_GLIB_2_54
	AyyiMixerStrip* self = (AyyiMixerStrip*)g_object_new_with_properties(object_type, 0, NULL, NULL);
#else
	AyyiMixerStrip* self = g_object_newv (object_type, 0, NULL);
#endif

#if 0
	void strip_finalize_notify(gpointer _, GObject* was)
	{
		dbg(0, "*** strip finalize. strip=%p", was);
	}
	g_object_weak_ref((GObject*)self, strip_finalize_notify, NULL);
#endif

	return self;
}


AyyiMixerStrip*
ayyi_mixer_strip_new ()
{
	return ayyi_mixer_strip_construct (AYYI_TYPE_MIXER_STRIP);
}


static void
ayyi_mixer_strip_class_init (AyyiMixerStripClass * klass)
{
	ayyi_mixer_strip_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->finalize = ayyi_mixer_strip_finalize;

	GtkWidgetClass* widget = GTK_WIDGET_CLASS(klass);
	widget->size_allocate = strip__on_allocate;

	klass->add_widgets = _strip__add_widgets;

	g_signal_new("channel_changed", AYYI_TYPE_MIXER_STRIP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
ayyi_mixer_strip_init (AyyiMixerStrip* s)
{
	s->priv = ayyi_mixer_strip_get_instance_private(s);
	s->colour = -1;
	s->hmag = 1.0;
}


static void
ayyi_mixer_strip_finalize (GObject* obj)
{
	Strip* strip = (Strip*)obj;

	if(strip->cha_name) g_string_free(strip->cha_name, true);
	if(strip->inserts) g_ptr_array_free(strip->inserts, true);

	G_OBJECT_CLASS (ayyi_mixer_strip_parent_class)->finalize (obj);
}


//------------------------------------------------------------------


Strip*
strip__new (AMChannel* channel)
{
	Strip* strip = ayyi_mixer_strip_new();
	strip->channel = channel;
	return strip;
}


void
strip__destroy (AyyiMixerStrip* s)
{
	// free memory and clean up pointers.
	// -should be able to handle a corrupted channel (eg when creation failed halfway through).
	// Meters are freed in meter.c via the strip->destroy signal

	GtkWidget* widget = (GtkWidget*)s;

	AMChannel* prev_channel = s->channel;
	strip__set_channel(s, NULL);
	fader_set_channel (s->fdr, NULL, prev_channel);

	s->e = NULL;
	if(GTK_IS_WIDGET(s)) gtk_widget_destroy(widget);
}


void
strip__set_channel (AyyiMixerStrip* s, const AMChannel* ch)
{
	g_return_if_fail(s);

	if(ch == s->channel) return;

	if(!ch){
		gtk_widget_set_sensitive(s->e, false);

		for(int c=0;c<2;c++){
			if(s->meter.widget[c])
				meter_destroy(s, c);
		}
	}

	AMChannel* prev = s->channel;
	s->channel = (AMChannel*)ch;

	bool new_meter = false;

	if(ch){
		gtk_widget_set_sensitive(s->e, true);
		fader_set_channel(s->fdr, s->channel, prev);

		for(int c=0;c<2;c++){
			if(c < ch->nchans){
				if(!s->meter.widget[c]){
					GtkWidget* vumeter = meter_new(s, c);
					if(vumeter){
						new_meter = true;
						gtk_box_pack_start(GTK_BOX(s->fdr_bay), vumeter, EXPAND_TRUE, FILL_TRUE, 0);
						gtk_box_reorder_child(GTK_BOX(s->fdr_bay), vumeter, c);
					}
				}
			}else{
				if(s->meter.widget[c]){
					meter_destroy(s, c);
				}
			}
		}
	}

	if(!new_meter) g_signal_emit_by_name(s, "channel-changed");
}


bool
strip__is_hidden (AyyiMixerStrip* strip)
{
	if(strip->channel->type == AM_CHAN_MASTER) return FALSE; //TODO doesnt have a event box?
	g_return_val_if_fail(strip, false);
	g_return_val_if_fail(strip->e, false);
	dbg(2, "ch=%i", strip->strip_num);

	return !GTK_WIDGET_VISIBLE(strip->e);
}


bool
strip__is_selected (AyyiMixerStrip* strip)
{
	if(!AYYI_IS_MIXER_WIN(strip->panel)) return false;

	GList* l = ((MixerWin*)strip->panel)->selectionlist;
	for(;l;l=l->next){
		int strip_num = GPOINTER_TO_INT(l->data);
		if(strip_num == strip->strip_num) return TRUE;
	}
	return FALSE;
}


/*
 *  Apply the SelectionColour. The strip border is drawn by the container, not by the strip itself.
 */
void
strip__set_selected (AyyiMixerStrip* strip)
{
	if(strip->label){
		GtkStyle* style = gtk_style_copy(gtk_widget_get_style(strip->label));
		GdkColor color1 = style->bg[GTK_STATE_SELECTED];
		style->bg[GTK_STATE_NORMAL] = color1;
		gtk_widget_set_style (strip->label, style);
		g_object_unref(style);
	}
}


void
strip__unselect (AyyiMixerStrip* strip)
{
	g_return_if_fail(strip);

	if(strip->label){
		gtk_widget_set_style(strip->label, NULL);
	}
}


void
strip__set_pan (AyyiMixerStrip* s, double val)
{
	GtkWidget* widget = s->pan_fdr;
	g_return_if_fail(widget);

	if(s->pan_fdr != pressed_widget) {
		gtk_adjustment_set_value(GTK_ADJUSTMENT(s->pan_adj), val);
	}

	gtk_widget_show(s->pan_fdr);
}


void
strip__set_mute (AyyiMixerStrip* s, bool mute)
{
	AyyiMixerStripPrivate* p = s->priv;
	g_return_if_fail(p->wmute);

	svg_toggle_button_set_active ((SvgToggleButton*)p->wmute, mute);
}


void
strip__set_solo (AyyiMixerStrip* s, bool solo)
{
	AyyiMixerStripPrivate* priv = s->priv;
	g_return_if_fail(priv->wsolo);

	svg_toggle_button_set_active ((SvgToggleButton*)priv->wsolo, solo);
}


void
strip__set_armed (AyyiMixerStrip* s, bool armed)
{
	AyyiMixerStripPrivate* priv = s->priv;
	g_return_if_fail(priv->wsolo);

	svg_toggle_button_set_active ((SvgToggleButton*)priv->wrec, armed);
}


#define STRIP_PACK(A) \
  gtk_box_pack_start (GTK_BOX(s->box), A, EXPAND_FALSE, FALSE, NO_PADDING);


GtkWidget*
strip__add_widgets (AyyiPanel* panel, AyyiMixerStrip* strip, MixerOptions* options)
{
	return AYYI_MIXER_STRIP_GET_CLASS(strip)->add_widgets(panel, strip, options);
}


static GtkWidget*
_strip__add_widgets (AyyiPanel* panel, AyyiMixerStrip* s, MixerOptions* options)
{
	/*

	fixed
	+--eventbox
	   +--vbox
	      +--insel
	      +--outsel
	      |
	      +--insert0  combobox
	      +--insert1  combobox
	      +--etc
	      |
	      +--fader    hbox
	      +--label

	*/
	AyyiMixerStripPrivate* priv = s->priv;
	ASSERT_CH_NUM(s->strip_num);
	dbg (2, "ch_num=%i", s->strip_num);
	g_return_val_if_fail(((AyyiSongService*)ayyi.service)->amixer, NULL);
	g_return_val_if_fail(!s->channel || s->channel->type == AM_CHAN_INPUT, NULL);

	s->panel = panel;
	s->show_aux_values = TRUE;
	s->use_rotaries_for_auxs = options->rotary_aux;

	s->box = gtk_vbox_new(FALSE, 0);

	// event box for catching events, and colouring
	s->e = gtk_event_box_new ();
	g_signal_connect((gpointer)s->e, "button-press-event", G_CALLBACK(strip__on_clicked), s);
	g_signal_connect((gpointer)s->e, "button-release-event", G_CALLBACK(strip__on_clicked), s);

	gtk_fixed_put((GtkFixed*)s, s->e, MIXER_HBOX_SELECTION_BORDER, MIXER_HBOX_SELECTION_BORDER);

	spacer_new(GTK_BOX(s->box), 7);

	int i;
#ifdef HAVE_GTK_2_6
	s->insel  = strip__input_selector_new (s);
	s->outsel = strip__output_selector_new(s);

#else //gtk version < 2.6
	//old style combo box's:

	// input select
	GtkWidget* insel = gtk_combo_new();
	gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(insel)->entry), "My String.");
	char instr[64];
	snprintf(instr, 64, "in: in%02i", ch_num);
	GList* in_list = g_list_append (NULL, instr);
	gtk_combo_set_popdown_strings (GTK_COMBO(insel), in_list);
	//list can be freed now.
	STRIP_PACK(insel);

	// output select (gtk<2.4)
	pwarn ("using deprecated gtk widgets. Routing changes are not supported :-(");
	GtkWidget* outsel = gtk_combo_new();
	GtkEntry* entry = GTK_ENTRY(GTK_COMBO(outsel)->entry);
	gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(outsel)->entry), "Select Output");
	GList* out_list = NULL;
	out_list = g_list_append(out_list, "out: mixbus");
	out_list = g_list_append(out_list, "out: bus1");
	gtk_combo_set_popdown_strings(GTK_COMBO(outsel), out_list);
	// list can be freed now.
	STRIP_PACK(outsel);
	g_signal_connect((gpointer)entry, "changed", G_CALLBACK(outsel_on_select), (gpointer)ch_num);

	// temp spacer
	spacer_new(GTK_BOX(s->box), 15);

	ch->insel = insel;
	ch->outsel = outsel;

#endif // end have_gtk_2.6

	// inserts
	insert_new(s);

	// temp spacer
	priv->spacer2 = spacer_new(GTK_BOX(s->box), 5);

	//-------------------------------------------------------------

	// sends:
	// -these need:
	//    1-adjustment                                       done
	//    2-on/off                                           not complete
	//    3-src select pre/post/fade                         not complete
	//    4-bus_selection                                    done
	//    5-pans (if stereo)                                 not done
	//
	AyyiChannel* ayyi_chan = s->channel ? ayyi_mixer__channel_at(s->channel->ident.idx) : NULL;

	int send_count = 0;
	MixerWin* mixer = (G_OBJECT_TYPE(s->panel) == AYYI_TYPE_MIXER_WIN) ? (MixerWin*)s->panel : NULL;
	if(ayyi_chan){
		if((send_count = options->sendcount)){
			dbg (2, "window->type=%s send_count=%i", AYYI_PANEL_GET_CLASS(s->panel)->name, send_count);

			for (i=0;i<send_count;i++) {
				AyyiAux* aux = ayyi_chan ? ayyi_mixer__aux_get(ayyi_chan, i) : NULL;
				if(aux) dbg(0, "aux %i: bus=%i", i, aux->bus_num);

				Observable* level = s->channel->aux_level[i];
				if(level){
					if (s->use_rotaries_for_auxs) {
						s->wsend[i] = (GtkWidget*)gtk_knob_new(level, (aux != NULL));

						GTK_DIAL(s->wsend[i])->on_right_click = aux_menu__popup;
						//gtk_knob_set_active(s->wsend[i], (aux != NULL));
					} else {
//						s->wsend[i] = gtk_hscale_new(adj);

#if 0
						s_scale [i] = gtk_volume_button_new();
						gtk_scale_button_set_adjustment((GtkScaleButton*)s_scale[i], GTK_ADJUSTMENT(s_adj[i]));
#endif
					}

					g_signal_connect((gpointer)s->wsend[i], "change-value", G_CALLBACK(strip__on_aux_widget_value), (gpointer)s);

					// cannot make this insensitive because then the context menu doesnt work.
					if(0 && !aux) gtk_widget_set_state(s->wsend[i], GTK_STATE_INSENSITIVE);
					STRIP_PACK(s->wsend[i]);
				}
			}

			// extra padding on top and bottom aux
			if(s->use_rotaries_for_auxs) gtk_knob_set_padding(s->wsend[0], 5, -1);

			priv->spacer3 = spacer_new(GTK_BOX(s->box), 5);
		}
	}

	// pan
	// -this probably should be custom composite widget. We need:
	//      -to override the text display. Show "L", "R" when at endstops.
	//      -a CTL mouse drag with finer resolution.
	//      -gravity around centre point to easily centre the control.
	{
		int pval = 0;
		GtkObject* pan_adj = s->pan_adj = gtk_adjustment_new(pval, 
			-PAN_RANGE / 2, PAN_RANGE / 2, // min, max.
			PAN_RANGE / 100,               // step increment
			PAN_RANGE / 10,                // page increment
			0                              // page size
		);
		GtkWidget* pan_scale = gtk_hscale_new(GTK_ADJUSTMENT(pan_adj));
		s->pan_fdr = pan_scale;

		// set pan background colour
		GtkWidget* pan_bg = gtk_event_box_new();

		GtkWidget* pan_align = gtk_alignment_new(1.0, 1.0, 1.0, 1.0);
		gtk_alignment_set_padding(GTK_ALIGNMENT(pan_align), 2, 0, 0, 0); //top padding
		gtk_container_add(GTK_CONTAINER(pan_align), pan_scale);
		gtk_container_add(GTK_CONTAINER(pan_bg), pan_align);
		STRIP_PACK(pan_bg);
		if(!s->channel) gtk_widget_set_sensitive(pan_scale, false);

		g_signal_connect((gpointer)pan_scale, "value-changed", G_CALLBACK(strip__on_pan_value_changed), s->channel);
		g_signal_connect((gpointer)pan_scale, "button-press-event", G_CALLBACK(fader_buttonpress), s->channel);
		g_signal_connect((gpointer)pan_scale, "button-release-event", G_CALLBACK(fader_buttonrelease), s->channel);

		priv->spacer3 = spacer_new(GTK_BOX(s->box), 5);
	}

	// primary button controls

	GtkWidget* hbox2 = gtk_hbox_new(FALSE, 0);
	STRIP_PACK(hbox2);

	// solo defeat
	priv->wsdef = svg_toggle_button_new_full ("sdef-off", "sdef-on", SMALL_ICON_SIZE);
	gtk_box_pack_start(GTK_BOX(hbox2), priv->wsdef, true, true, 0);
	g_signal_connect((gpointer)priv->wsdef, "button-press-event", G_CALLBACK(bsdef_on_press), s);

	// rec
	priv->wrec = svg_toggle_button_new_full ("record", "record-on", SMALL_ICON_SIZE);
	gtk_box_pack_start(GTK_BOX(hbox2), priv->wrec, true, true, 0);
	g_signal_connect((gpointer)priv->wrec, "clicked", G_CALLBACK(brec_on_press), s);

	// solo
	STRIP_PACK(priv->wsolo = svg_toggle_button_new_full("solo-off", "solo-on", 16));
	g_signal_connect((gpointer)priv->wsolo, "clicked", G_CALLBACK(bsolo_on_press), s);

	// mute
	STRIP_PACK(priv->wmute = svg_toggle_button_new_full("mute-off", "mute-on", 16));
	g_signal_connect((gpointer)priv->wmute, "clicked", G_CALLBACK(bmute_on_press), s);

	spacer_new(GTK_BOX(s->box), 5);

	// fader
	if((s->fdr_bay = fader_bay_new(s))){
		gtk_box_pack_start(GTK_BOX(s->box), s->fdr_bay, TRUE, TRUE, 0);
	}

	spacer_new(GTK_BOX(s->box), 5);

	// label
	{
		s->cha_name = g_string_new("");

		s->label = (GtkWidget*)editable_label_button_new(s->cha_name->str);
		gtk_alignment_set((GtkAlignment*)gtk_bin_get_child(GTK_BIN(s->label)), 0.5, 0.0, 0.0, 0.0);
		STRIP_PACK(s->label);
		if(mixer) gtk_size_group_add_widget(mixer->strip_label_sizegroup, s->label);
		g_signal_connect(s->label, "button-release-event", G_CALLBACK(strip__on_label_clicked), s);
	}

#if 0 // GtkTextView not used due to incorrect mouseover pointer.
	{
		//hidden editable text box:
		GtkWidget* view1 = gtk_text_view_new ();
		GtkTextBuffer* txt_buf1 = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view1));
		gtk_text_buffer_set_text(txt_buf1, s->cha_name->str, -1);
		//gtk_text_view_set_justification(GTK_TEXT_VIEW(view1), GTK_JUSTIFY_CENTER);//?? doesnt work?
		gtk_text_view_set_editable(GTK_TEXT_VIEW(view1), FALSE);//to stop cursor flashing. Override on click.
		gtk_text_view_set_cursor_visible((GtkTextView*)view1, false);
		//gtk_widget_set_no_show_all(view1, true);
		gtk_box_pack_start (GTK_BOX(s->box), view1, FALSE, TRUE, 2);
	}
#endif

	//gtk_container_set_border_width (GTK_CONTAINER (vbox), 5); //make space *outside* the vbox.
	gtk_container_add (GTK_CONTAINER(s->e), s->box);
	//gtk_container_set_border_width (GTK_CONTAINER (x), 5); //make space *outside* the vbox.
	gtk_widget_set_size_request(s->box, 20, -1);

	// set up complete channel as a drop target
	gtk_drag_dest_set(s->e, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
	g_signal_connect (G_OBJECT(s->e), "drag-drop", G_CALLBACK(mixer__drag_drop), NULL);
	g_signal_connect (G_OBJECT(s->e), "drag-data-received", G_CALLBACK(strip__drag_received), s);
	g_signal_connect (G_OBJECT(s->e), "realize", G_CALLBACK(strip__on_realise), s);

	strip__update(s);

	return (GtkWidget*)s;
}


bool
strip__update (AyyiMixerStrip* s)
{
	// Ensure an existing gui channel strip is a true reflection of the channel data.

	g_return_val_if_fail(s, false);
	if(!s->channel){
		gtk_widget_set_sensitive(s->e, false);
		return false;
	}
	gtk_widget_set_sensitive(s->e, true);

	g_return_val_if_fail(s->channel->type == AM_CHAN_INPUT, false);

	dbg (2, "ch_num=%i ...", s->strip_num);


	AyyiIdx core_index = s->channel->ident.idx;
	if(!ayyi_track__index_ok(core_index)){ perr ("bad core_index: %i", core_index); return FALSE; }

	strip__input_selector_update(s);
	strip__output_selector_update(s);

	for (int i=0;i<s->inserts->len;i++) strip__update_pluginbox(s, i);
	strip__update_insert_size(s);

	bool   has_pan = false;
	bool   mute  = am_channel__is_muted    (s->channel);
	double level = am_channel__fader_level (s->channel);
	double pan   = am_channel__pan_value   (s->channel, &has_pan);
	dbg(2, "has_pan=%i", has_pan);

	fader__set_value(s->fdr, level);
	strip__set_mute(s, mute);
	if(has_pan) strip__set_pan(s, pan); else gtk_widget_hide(s->pan_fdr);
	strip__set_solo(s, am_channel__is_solod(s->channel));

	strip__label_update(s);

	return true;
}


void
strip__show (AyyiMixerStrip* strip)
{
	g_return_if_fail(strip);
	AyyiMixerStripPrivate* s = strip->priv;

	if(strip->e) gtk_widget_show(strip->e);
	if(s->rsiz_handle) gtk_widget_show(s->rsiz_handle);
}


void
strip__hide (AyyiMixerStrip* strip)
{
	g_return_if_fail(strip);
	AyyiMixerStripPrivate* s = strip->priv;

	if(mixer__n_visible_strips((MixerWin*)strip->panel) <= 1) return; // dont hide last strip

	if(strip->e) gtk_widget_hide(strip->e);
	if(s->rsiz_handle) gtk_widget_hide(s->rsiz_handle);
}


void
strip__output_selector_update (AyyiMixerStrip* s)
{
	dbg(2, "ch=%i %s", s->channel ? s->channel->ident.idx : -1, strip__get_ch_name(s));
#ifdef COMBO_GRID
	if(!s->outsel) return;

	//update combobox text
	char text[64];
	am_channel__get_output_string(s->channel, text);
	gtk_combo_grid_set_text(GTK_COMBO_GRID(s->outsel), text);

	//---------------------------------

	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(s->channel->ident.idx);
	if(!ayyi_trk) return;

	// now iterate over the list ayyi_trk->output_routing...
#ifdef DEBUG
	AyyiList* routing = ayyi_list__first(ayyi_trk->output_routing);
	while((routing = ayyi_list__next(routing))){
		AyyiConnection* connection = ayyi_song__connection_at(routing->id);
		dbg(2, "  connection_id=%i '%s'", routing->id, connection->name);
	}
#endif

	//---------------------------------

	//if the dropdown menu is opened, it needs to be updated too:
	int changed = s->strip_num;
	int open    = output.channel ? output.channel->ident.idx : -1;
	if(changed == open){
		AMChannel* channel = g_ptr_array_index(song->channels->array, open);
		output_menu_update(NULL, channel);
	}

 #else
	static int size = 0;
	GtkComboBox* combo = GTK_COMBO_BOX(strip->outsel);
	//GtkTreeModel* model = gtk_combo_box_get_model(combo);

	//gtk_list_store_clear (model); //segfault!

	// this isnt reliably clearing the list
	int i; for(i=0;i<size;i++){
		gtk_combo_box_remove_text(combo, 0);
	}
	size = 0;

#warning using strip number as track index!
	gtk_combo_box_append_text(combo, am_track__get_output_name(track[strip->strip_num]));
	size++;

	gtk_combo_box_append_text(combo, ""); //poor mans separator.
	size++;

	AyyiConnection* shared = NULL;
	while((shared = engine_output_next(shared))){
		gtk_combo_box_append_text(combo, shared->name);
		size++;
	}

	gtk_combo_box_set_active(combo, 0);
#endif //COMBO_GRID
}


void
strip__input_selector_update (AyyiMixerStrip* s)
{
#ifdef COMBO_GRID
	//update combobox text
	char text[64];
	am_channel__get_input_string(s->channel, text);
	gtk_combo_grid_set_text(GTK_COMBO_GRID(s->insel), text);

	//if the dropdown menu is opened, it needs to be updated too:
	AMChannel* changed = s->channel;
	AMChannel* open = input.channel;
	if(changed == open){
		input_menu_update(NULL, open);
	}

#else
	static int size = 0;
	GtkComboBox* combo = GTK_COMBO_BOX(s->insel);

	int i; for(i=0;i<size;i++) gtk_combo_box_remove_text(combo, 0);
	size = 0;

	gtk_combo_box_append_text(combo, am_track_get_input_name(strip->strip_num));
	size++;
	gtk_combo_box_set_active(combo, 0);

	gtk_combo_box_append_text(combo, ""); //poor mans separator.
	size++;

	AyyiConnection* shared = NULL;
	while((shared = engine_input_next(shared))){
		gtk_combo_box_append_text(combo, shared->name);
		size++;
	}
#endif
}

static bool
insert_is_empty (StripInsert* insert)
{
	return (gtk_combo_box_get_active(GTK_COMBO_BOX(insert->combobox)) < 1);
}


void
strip__update_insert_size (AyyiMixerStrip* s)
{
	// If the strip doesnt have a free insert slot, one is added.

#ifdef HAVE_GTK_2_6
	if(G_OBJECT_TYPE(s->panel) == AYYI_TYPE_MIXER_WIN){
		bool full = FALSE;
		if(!s->inserts) return;
		int insert_count = s->inserts->len;
		StripInsert* last_insert = g_ptr_array_index(s->inserts, insert_count - 1);
		dbg (2, "ch_num=%i insert_count=%i last_insert=%p", s->strip_num, insert_count, last_insert);
		if(!last_insert){ perr ("last_insert"); return; }
		if(!last_insert->combobox){ perr ("no combox for insert_slot=%i", insert_count-1); return; }
		if(!insert_is_empty(last_insert)) full = TRUE;
		if(full){
			dbg (0, "adding new insert slot...");
			insert_new(s);
		}
	}
#endif
}


#if 0
static void
inserts_print (Strip* s)
{
	dbg (0, "arraysize=%i", s->inserts->len);

	int i; for(i=0;i<s->inserts->len;i++){
		StripInsert* insert = g_ptr_array_index(s->inserts, i);
		printf("  insert=%p combobox=%p\n", insert, insert->combobox);
	}
}
#endif


static GtkWidget*
strip__input_selector_new (AyyiMixerStrip* s)
{
#ifdef COMBO_GRID
	// unlike Arrange tracks, trees are not shared - each widget has its own dropdown treeview.

	GtkWidget* selector = gtk_combo_grid_new();
	GtkComboGrid* grid = GTK_COMBO_GRID(selector);

	gtk_combo_grid_set_model(grid, GTK_TREE_MODEL(input.treestore));
	gtk_combo_grid_set_headers_visible(grid, FALSE);
	gtk_combo_grid_set_height(grid, 500);
	if(s->channel){
		gtk_combo_grid_set_model_updater(grid, input_menu_update, s->channel);
	}
	gtk_combo_grid_set_text_column(grid, IO_COL_NAME);

	GtkEntry* entry = GTK_ENTRY(gtk_combo_grid_get_entry((GtkComboGrid*)selector));
	GtkEntryCompletion* completion = gtk_entry_completion_new();
	gtk_entry_completion_set_model(completion, GTK_TREE_MODEL(input.treestore));
	gtk_entry_completion_set_text_column(completion, IO_COL_NAME);
	gtk_entry_completion_set_popup_set_width(completion, FALSE);
	gtk_entry_set_completion(entry, completion);

	//checkbox column
	GtkCellRenderer* renderer = gtk_cell_renderer_toggle_new();
	GtkTreeViewColumn* col = gtk_tree_view_column_new_with_attributes("Checkbox", renderer, "active", COL_CHECKBOX, /*"activatable", TRUE,*/ NULL);
	g_signal_connect(renderer, "toggled", (GCallback)strip__on_input_toggled, grid);
	gtk_combo_grid_append_column(grid, col);

	//input name column:
	GtkTreeViewColumn* col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Name");
	GtkCellRenderer* renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", IO_COL_NAME);
	gtk_tree_view_column_set_sort_column_id(col1, IO_COL_NAME);
	gtk_combo_grid_append_column(grid, col1);

	// device name
	col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Device");
	renderer1 = gtk_cell_renderer_text_new();
	//gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", COL_DEVICE);
	gtk_combo_grid_append_column(grid, col1);

	// port 1 column
	GtkTreeViewColumn* col2 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col2, "Port 1");
	GtkCellRenderer* renderer2 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col2, renderer2, TRUE);
	gtk_tree_view_column_add_attribute(col2, renderer2, "text", COL_PORT1);
	gtk_combo_grid_append_column(grid, col2);

	// port 2 column
	GtkTreeViewColumn* col3 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col3, "Port 2");
	GtkCellRenderer* renderer3 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col3, renderer3, TRUE);
	gtk_tree_view_column_add_attribute(col3, renderer3, "text", COL_PORT2);
	gtk_combo_grid_append_column(grid, col3);
#else

	GtkWidget* selector = gtk_combo_box_entry_new_text();
	GtkComboBox* combo = GTK_COMBO_BOX(selector);

	gtk_combo_box_append_text(combo, "input");
	gtk_combo_box_set_active(combo, 0);
#endif // combogrid

	gtk_widget_show(selector);
	gtk_box_pack_start(GTK_BOX(s->box), selector, FALSE, FALSE, 0);

	return selector;
}


static GtkWidget*
strip__output_selector_new (AyyiMixerStrip* s)
{
#ifdef HAVE_GTK_2_6
	#ifdef COMBO_GRID
	//unlike Arrange tracks, trees are not shared - each widget has its own dropdown treeview, though the model is shared.

	GtkWidget* selector = gtk_combo_grid_new();
	GtkComboGrid* grid = GTK_COMBO_GRID(selector);

	//g_signal_connect(G_OBJECT((GTK_COMBO_GRID(selector))->button), "toggled", G_CALLBACK(combogrid_on_toggled), GINT_TO_POINTER(strip->channel->ident.idx));
	gtk_combo_grid_set_model(grid, GTK_TREE_MODEL(output.treestore));
	gtk_combo_grid_set_headers_visible(grid, FALSE);
	gtk_combo_grid_set_height(grid, 500);
	if(s->channel){
		gtk_combo_grid_set_model_updater(grid, output_menu_update, s->channel);
	}
	gtk_combo_grid_set_text_column(grid, IO_COL_NAME);

	// the completion is too narrow.
	GtkEntry* entry = GTK_ENTRY(gtk_combo_grid_get_entry((GtkComboGrid*)selector));
	GtkEntryCompletion* completion = gtk_entry_completion_new();
	gtk_entry_completion_set_model(completion, GTK_TREE_MODEL(output.treestore));
	gtk_entry_completion_set_text_column(completion, IO_COL_NAME);
	gtk_entry_completion_set_popup_set_width(completion, FALSE);
	gtk_entry_set_completion(entry, completion);

	// TODO the completion implements GtkCellLayout - can we use use any of these functions?
#if 0
	gtk_cell_layout_get_cells(GTK_CELL_LAYOUT(completion));
#endif

	// checkbox column
	GtkCellRenderer* renderer = gtk_cell_renderer_toggle_new();
	GtkTreeViewColumn* col = gtk_tree_view_column_new_with_attributes("Checkbox", renderer, "active", COL_CHECKBOX, /*"activatable", TRUE,*/ NULL);
	g_signal_connect(renderer, "toggled", (GCallback)strip__on_output_toggled, grid);
	gtk_combo_grid_append_column(grid, col);

	// output name column
	GtkTreeViewColumn* col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Name");
	GtkCellRenderer* renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", IO_COL_NAME);
	gtk_tree_view_column_set_sort_column_id(col1, IO_COL_NAME);

	gtk_combo_grid_append_column(grid, col1);

	// device name
	col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Device");
	renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", COL_DEVICE);
	gtk_combo_grid_append_column(grid, col1);

	// port 1 column
	GtkTreeViewColumn* col2 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col2, "Port 1");
	GtkCellRenderer* renderer2 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col2, renderer2, TRUE);
	gtk_tree_view_column_add_attribute(col2, renderer2, "text", COL_PORT1);
	gtk_combo_grid_append_column(grid, col2);

	// port 2 column
	GtkTreeViewColumn* col3 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col3, "Port 2");
	GtkCellRenderer* renderer3 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col3, renderer3, TRUE);
	gtk_tree_view_column_add_attribute(col3, renderer3, "text", COL_PORT2);
	gtk_combo_grid_append_column(grid, col3);
	#else
	GtkWidget* selector = gtk_combo_box_entry_new_text();
	GtkComboBox* combo = GTK_COMBO_BOX(selector);

	gtk_combo_box_append_text(combo, "output");
	gtk_combo_box_set_active(combo, 0);
	#endif

	gtk_widget_show(selector);
	gtk_box_pack_start(GTK_BOX(s->box), selector, FALSE, FALSE, 0);

	return selector;
#endif //gtk 2.6
}


char*
strip__get_ch_name (AyyiMixerStrip* s)
{
	g_return_val_if_fail(s->channel, NULL);
	AyyiChannel* track = ayyi_mixer_container_get_quiet(&((AyyiSongService*)ayyi.service)->amixer->tracks, s->channel->ident.idx);
	if(!track) return NULL;

	// update the local copy just in case
	if(s->cha_name) s->cha_name = g_string_assign(s->cha_name, track->name);

	return track->name;
}


/*
 *  Apply the palette colour #num to the mixer strip.
 *
 *  Currently colour is applied to all elements, whether they have a colour or not.
 */
void
strip__set_colour (AyyiMixerStrip* strip, int num)
{
	g_return_if_fail(strip);
	GtkWidget* vbox = strip->box;

	strip->colour = num;

	// lookup the colour in the palette
	GdkColor colour;
	am_palette_lookup(song->palette, &colour, num);

	gtk_widget_modify_bg(vbox, GTK_STATE_NORMAL, &colour);

	// colour needs to be applied to ALL strip elements

	// method 1: iterate through widgets:
	// assume for the moment that "*strip" is infact the solodef button!!!

	// get parent
	GtkWidget* parent = gtk_widget_get_ancestor(strip->box, GTK_TYPE_EVENT_BOX); //the ch strip event box.
	gtk_widget_modify_bg(parent, GTK_STATE_NORMAL, &colour);
	// find button children
	GList* child_list = gtk_container_get_children(GTK_CONTAINER(parent));

	int j;
	GtkWidget* child = NULL, *child2;
	for(j=0;j<g_list_length(child_list);j++){
		child = g_list_nth_data(child_list, j);
	}

	child_list = gtk_container_get_children(GTK_CONTAINER(child));
	for(j=0;j<g_list_length(child_list);j++){
		child = g_list_nth_data(child_list, j);
		gtk_widget_modify_bg(child, GTK_STATE_NORMAL, &colour);

		if (!strcmp(gtk_widget_get_name(child), "GtkHBox")){
			GList* child_list2 = gtk_container_get_children(GTK_CONTAINER(child));
			int k;
			for(k=0;k<g_list_length(child_list2);k++){
				child2 = g_list_nth_data(child_list2, k);
				gtk_widget_modify_bg(child2, GTK_STATE_NORMAL, &colour);
			}
		}
	}
}


int
strip__get_colour (AyyiMixerStrip* strip)
{
	return strip->colour;
}


static void
strip__on_realise (GtkWidget* widget, gpointer user_data)
{
	// Set pan background colour

	GtkWidget *align, *pan_bg;
	Strip* s = user_data;
	g_return_if_fail(s);

	// pan widget container order: bg->align->scale
	GtkWidget* pan_fdr = s->pan_fdr;
	if((align = gtk_widget_get_parent(pan_fdr))){
		if((pan_bg = gtk_widget_get_parent(align))){
			GtkStyle* style = gtk_style_copy(gtk_widget_get_style(pan_bg));
			colour_get_frame(&style->bg[GTK_STATE_NORMAL]);
			gtk_widget_set_style(pan_bg, style);
			g_object_unref(style);
			return;
		}
	}
	perr ("get pan box failed");
}


static void
strip__on_allocate (GtkWidget* widget, GtkAllocation* allocation)
{
	((GtkWidgetClass*)ayyi_mixer_strip_parent_class)->size_allocate(widget, allocation);

	AyyiMixerStrip* s = (AyyiMixerStrip*)widget;
	gtk_widget_set_size_request(s->e, allocation->width - MIXER_HBOX_SELECTION_BORDER, MAX(0, allocation->height - MIXER_HBOX_SELECTION_BORDER));
}


static bool
strip__on_clicked (GtkWidget* widget, GdkEventButton* event, gpointer _strip)
{
	// Handles mouse clicks on mixer channel strips.
	// Currently connected to both press and release, which can lead to some undesirable behaviour.

	PF;

	g_return_val_if_fail(_strip, false);
	Strip* ms = _strip;
	g_return_val_if_fail(ms->panel, FALSE);
	bool handled = FALSE;

	ayyi_panel_on_focus(ms->panel);

	bool inspector = (G_OBJECT_TYPE(ms->panel) == ayyi_inspector_win_get_type());

	if(event->button == 3){

		// get the menu widget from the window struct
		GList* sel_list = NULL;
		GtkWidget* contextmenu;
		if(G_OBJECT_TYPE(ms->panel) == ayyi_mixer_win_get_type()){
			MixerWin* mixer = (MixerWin*)ms->panel;
			contextmenu = mixer->contextmenu->menu;
			sel_list = mixer->selectionlist;
		} else if(inspector){
			printf("  inspector! nothing to do.\n");
			return handled;
		} else { perr("unknown window type."); return handled; }


		// if no channels are selected, we 'select' the one just clicked
		dbg (2, "checking selection list...");
		if(!sel_list) mixer__selection_replace((MixerWin*)ms->panel, ms);

		if(contextmenu){
			// open pop-up menu
			mixer_menu__update((MixerWin*)ms->panel);
			gtk_menu_popup(GTK_MENU(contextmenu), NULL, NULL, NULL, NULL, event->button, event->time);
		}
		return handled = TRUE;
	}

	if(event->type == GDK_BUTTON_RELEASE && !inspector){
		MixerWin* mixer = (MixerWin*)ms->panel;
		GList* list = ((MixerWin*)ms->panel)->selectionlist;

		if (event->state & GDK_CONTROL_MASK){
			// add or subtract a single channel, depending on whether it is alrady selected
			dbg (0, "CONTROL");
			int c = g_list_index(mixer->selectionlist, GINT_TO_POINTER(ms->strip_num));
			if(c == -1){
				// not already in the list - add it
				mixer__selection_add(mixer, ms);
			} else {
				// already in the list - subtract it
				mixer__selection_subtract(mixer, ms);
			}
		}

		else if (event->state & GDK_SHIFT_MASK){
			// we need to add every channel between the currently clicked, and the previously clicked.

			// assume previously selected track is the last one in the list
			int prev = GPOINTER_TO_INT(g_list_nth_data(list, g_list_length(list) -1));

			int first, last;
			if(prev < ms->strip_num){ first = prev; last = ms->strip_num;}
			else { first = ms->strip_num; last = prev; }

			int c;
			for(c=first;c<last+1;c++){
				// add if not already in the list
				if(g_list_index(list, GINT_TO_POINTER(c)) == -1) mixer__selection_add(mixer, mixer->strips[c]);
			}

		} else {
			mixer__selection_replace(mixer, ms);
			dbg(0, "focussable=%i", gtk_widget_get_can_focus(ms->label));
			gtk_widget_grab_focus(ms->fdr);
		}
		handled = TRUE;
	}
	// not entirely sure about this - are there any occasions when we want to propogate?
	// -currently docking dnd is not possible, but we would have to modify mixer_hbox to be more picky
	//  as it currently treats all click it recieves as resize events.
	else if (event->type == GDK_BUTTON_PRESS) handled = TRUE;

	// if not handled, it is passed on to the mixer window hbox background where we handle drag ops.
	return handled;
}


/*
 *  Upon single clicking, if ch is already selected, the label should go into edit mode.
 *  (selection is already handled via the channel event box)
 */
static bool
strip__on_label_clicked (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
	PF;
	Strip* strip = user_data;
	g_return_val_if_fail(strip, false);

	if(strip__is_selected(strip)){
		// this doesnt work because selection happens before we get here.
		// TODO check again now that double event connect has been modified.
	}
	return NOT_HANDLED;
}


static gint
fader_buttonpress (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
	PF;
	pressed_widget = widget;

	return NOT_HANDLED;
}


static gint
fader_buttonrelease (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
	PF;
	pressed_widget = NULL;

	return NOT_HANDLED;
}


static double
pan_value_normalise (double val)
{
	// input is the raw value from the widget.
	// output should be in the range -1.0 to 1.0
	return val / 10.0;
}


static void 
strip__on_pan_value_changed (GtkWidget* widget, AMChannel* ch)
{
	// Called when the user has moved the pan control.

	double val = gtk_range_get_value(GTK_RANGE(widget));

	if (widget != pressed_widget) return; //only send a msg if mouse button is down

	dbg (1, "pressed. sending msg...");

	// send the pan value directly. It is not mapped into gain values for the individual outputs.
	ayyi_channel__set_float(ch->ident.idx, AYYI_PAN, pan_value_normalise(val));
}


static bool
strip__on_aux_widget_value (GtkRange* range, GtkScrollType scroll, gdouble value, AyyiMixerStrip* s)
{
	// Called by the rotary widget change-value signal following a user action.

	g_assert(s->use_rotaries_for_auxs);

	Observable* val = s->use_rotaries_for_auxs ? GTK_DIAL(range)->value : NULL/*gtk_range_get_adjustment(range)*/;
	int aux_num = aux_num_lookup(val, s);
	dbg(1, "aux_num=%i adj=%.2fdB val=%.2fdB gain=%.4f", aux_num, val->value.f, value, am_db2gain(value));
	if(aux_num < 0) return NOT_HANDLED;

	am_channel__set_aux_level(s->channel, aux_num, value, NULL, NULL);

	return NOT_HANDLED;
}


static int
aux_num_lookup (Observable* adj, AyyiMixerStrip* s)
{
	int i; for(i=0;i<AYYI_AUX_PER_CHANNEL;i++){
		dbg(2, "i=%i al=%p", i, s->channel->aux_level[i]);
		if(!s->channel->aux_level[i]) continue;
		Observable* adj1 = s->channel->aux_level[i];
		if(!adj1) return -1;
		if(adj1 == adj) return i;
	}
	return -1;
}


static int
strip__get_free_plugin_slot (Strip* strip)
{
	return 0;
}


static gint
strip__drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	// possible data types:
	//   1- a palette number from the colour window.

	if(!data || data->length < 1){ pwarn("no data!"); return -1; }

	dbg(2, "%s", data->data);

	if(info == GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING)) printf(" type=string.\n");

	Strip* s = (Strip*)user_data;

	GList* list = uri_list_to_glist((gchar*)data->data);
#ifdef DEBUG
	int i = 0;
#endif
	for(; list; list = list->next){
		char* u = list->data;
		dbg (0, "%i: %s\n", i, u);

		gchar* method_string;
		vfs_get_method_string(u, &method_string);
		if(!strcmp(method_string, "colour")){

			// we need the palette number only, which is passed directly.
			int palette_num = atoi((char*)data->data + 7);
			if(!palette_num){ perr ("cannot parse data!"); return -1; }
			palette_num--; // restore zero indexing.

			strip__set_colour(s, palette_num);
		}
		else if(!strcmp(method_string, "plugin")){
			int plugin_num = atoi((char*)data->data + 7);
			if(!plugin_num){ perr ("cannot parse data!"); return -1; }
			plugin_num--; // restore zero indexing.

			//shm_vst_info* plugin = plugin_info_pod(plugin_num);
			int plugin_slot = strip__get_free_plugin_slot(s);
			struct _plugin_shared* plugin = ayyi_plugin_at(plugin_slot);
			char* plugin_name = plugin->name; //fst_info.name;
			am_plugin__change(s->channel, plugin_slot, plugin_name, NULL, NULL);
		}
		else{
			dbg(0, "unhandled dnd method: %s. Passing on to window...", method_string);
			ayyi_panel_drag_received(widget, drag_context, x, y, data, info, time, user_data);
		}
	}
	uri_list_free(list);

	return FALSE;
}


#ifdef COMBO_GRID
static void
strip__on_input_toggled (GtkCellRendererToggle* cell, gchar* path_string, GtkComboGrid* grid)
{
	PF;
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(input.treestore), &iter, path_string)){
		AyyiIdx c;
		gtk_tree_model_get(GTK_TREE_MODEL(input.treestore), &iter, COL_INDEX, &c, -1);
		AyyiConnection* connection = ayyi_song__connection_at(c);

		am_track__set_input(song->tracks->track[input.channel->ident.idx], connection);
		// no way to explicitly close the popup - activate the row will do it...
	}
}


/*static */void
strip__on_output_toggled (GtkCellRendererToggle* cell, gchar* path_string, GtkComboGrid* grid)
{
	PF;
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(output.treestore), &iter, path_string)){
		AyyiIdx connection;
		gtk_tree_model_get(GTK_TREE_MODEL(output.treestore), &iter, COL_INDEX, &connection, -1);
		AyyiConnection* shared = ayyi_song__connection_at(connection);

		am_track__set_output(song->tracks->track[output.channel->ident.idx], shared, NULL, NULL);
		// no way to explicitly close the popup - activate the row will do it...
	}
}
#endif


bool
strip_check (Strip* strip)
{
	return true;
}


void
strip__show_sends (AyyiMixerStrip* strip, bool show)
{
	int i; for(i=0;i<MAX_SENDS;i++){
		GtkWidget* send = strip->wsend[i];
		if(send) show_widget_if(send, show);
	}
	show_widget_if(strip->priv->spacer3, show);
}


bool
strip__sends_visible (AyyiMixerStrip* s)
{
	GtkWidget* send1 = s->wsend[0];
	if(!send1) return FALSE;
	return GTK_WIDGET_VISIBLE(send1);
}


void
strip__set_label (AyyiMixerStrip* s, char* text)
{
	g_return_if_fail(s);
	g_return_if_fail(text);

	gtk_label_set_text(((EditableLabelButton*)s->label)->label, text);
}


void
strip__label_update (AyyiMixerStrip* strip)
{
	// Get the channel label from core.

	char* ch_name;
	if((ch_name = strip__get_ch_name(strip))) gtk_label_set_text(((EditableLabelButton*)strip->label)->label, ch_name);
}


void
strip__auxs_update (AyyiMixerStrip* strip)
{
	if(!strip || !strip->use_rotaries_for_auxs) return;

	AyyiChannel* ayyi_chan = ayyi_mixer__channel_at(strip->channel->ident.idx);
	if(ayyi_chan){
		int a; for(a=0;a<AYYI_AUX_PER_CHANNEL;a++){
			if(strip->wsend[a]){
				AyyiAux* aux = ayyi_mixer__aux_get(ayyi_chan, a);
				gtk_knob_set_active(strip->wsend[a], (aux != NULL));
			}
		}
	}
}


void
strip__update_pluginbox (AyyiMixerStrip* s, int slot)
{
	AMChannel* channel  = s->channel;

	g_return_if_fail(slot < s->inserts->len);

	AyyiChannel* ac = ayyi_mixer__channel_at_quiet(channel->ident.idx);
	if(ac){
		AyyiIdx plugin_num = ac->plugin[slot].idx;
		dbg (2, "win=%i ch=%i slot=%i plugin=%i active=%i", ayyi_panel_get_instance_num(s->panel), channel->ident.idx, slot, plugin_num, ac->plugin[slot].active);
		if(plugin_num > -1 && plugin_num < g_list_length(song->plugins) + INSERT_BOX_LIST_OFFSET){
			StripInsert* insert = g_ptr_array_index(s->inserts, slot);
			#ifdef OLD
			int list_index = track->insert_active[slot] ? plugin_num + INSERT_BOX_LIST_OFFSET : 0;
			if(insert->changed_handler) g_signal_handler_block(insert->combobox, insert->changed_handler);
			gtk_combo_box_set_active(GTK_COMBO_BOX(insert->combobox), list_index);
			if(insert->changed_handler) g_signal_handler_unblock(insert->combobox, insert->changed_handler);

			snprintf(insert->previous, 255, gtk_combo_box_get_active_text(GTK_COMBO_BOX(insert->combobox)));
			#else
			pluginbox_set_selection(s, insert);
			#endif
		}
	}
}


static void 
bmute_on_press (GtkWidget* widget, AyyiMixerStrip* s)
{
	am_channel__set_mute(s->channel, (am_channel__is_muted(s->channel) + 1) % 2);
}


static void
bsolo_on_press (GtkWidget* widget, AyyiMixerStrip* s)
{
	am_channel__set_solo(s->channel, !am_channel__is_solod(s->channel));
}


static void
brec_on_press (GtkWidget* widget, AyyiMixerStrip* s)
{
	ayyi_channel__set_bool(s->channel->ident.idx, AYYI_ARM, !am_channel__is_armed(s->channel));
}


static void
bsdef_on_press (GtkWidget* widget, GdkEventButton* event, gpointer _strip)
{
	if(event->type != GDK_BUTTON_PRESS) return;

	if(event->button == 1){
		// left mouse button pressed. Toggle the solo-defeat
		AyyiMixerStrip* s = (AyyiMixerStrip*)_strip;
		int val = (am_channel__is_sdef(s->channel) + 1) % 2;
		ayyi_channel__set_bool(s->channel->ident.idx, AYYI_SDEF, val);
	}
}


void
strip__show_insert (Strip* strip, int insert, bool show)
{
	if(show){
		gtk_widget_show(((StripInsert*)g_ptr_array_index(strip->inserts, insert))->combobox);

		gtk_widget_show(strip->priv->spacer2);
	}else{
		insert_hide(g_ptr_array_index(strip->inserts, insert));
		gtk_widget_hide(strip->priv->spacer2);
	}
}
