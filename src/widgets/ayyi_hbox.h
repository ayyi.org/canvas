/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/.
 */

#ifndef __AYYI_HBOX_H__
#define __AYYI_HBOX_H__

#include <gdk/gdk.h>
#include <gtk/gtkbox.h>


G_BEGIN_DECLS

#define AYYI_TYPE_HBOX            (ayyi_hbox_get_type ())
#define AYYI_HBOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_HBOX, AyyiHBox))
#define AYYI_HBOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_HBOX, AyyiHBoxClass))
#define AYYI_IS_HBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_HBOX))
#define AYYI_IS_HBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_HBOX))
#define AYYI_HBOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_HBOX, AyyiHBoxClass))


typedef struct _AyyiHBox	    AyyiHBox;
typedef struct _AyyiHBoxClass  AyyiHBoxClass;

struct _AyyiHBox
{
  GtkBox box;
};

struct _AyyiHBoxClass
{
  GtkBoxClass parent_class;
};


GType	   ayyi_hbox_get_type (void) G_GNUC_CONST;
GtkWidget* ayyi_hbox_new	  (gboolean homogeneous, gint spacing);


G_END_DECLS

#endif
