/*
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*  based on Polygon item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Author: Federico Mena <federico@nuclecu.unam.mx>
 *         Rusty Conover <rconover@bangtail.net>
 */

#include "global.h"
#include <math.h>
#include <sys/time.h>
#include <gtk/gtk.h>
#include "libart_lgpl/art_vpath.h"
#include <libart_lgpl/art_svp.h>
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_svp_vpath_stroke.h"
#include <libart_lgpl/art_bpath.h>
#include <libart_lgpl/art_vpath_bpath.h>
#include <libart_lgpl/art_vpath_dash.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libart_lgpl/art_svp_intersect.h>
#include <libgnomecanvas/libgnomecanvas.h>

#include "model/time.h"
#include "model/am_part.h"
#include "windows.h"
#include "song.h"
#include "arrange.h"
#include "support.h"
#include "map_item.h"


#define NUM_STATIC_POINTS 256	/* Number of static points to use to avoid allocating arrays */

#define HAS_ALPHA_TRUE 1

#define CORNER_SIZE 0.0 //5.0

#define GNOME_CANVAS_MAP_HEIGHT(A) A->pts->coords[5]

#define MAX_PART_WIDTH 128000  // is there a 16bit limit on gnomecanvas size?
#define MAX_X_POS 128000

extern SMConfig* config;

enum {
	PROP_0,
	PROP_POINTS,
	//PROP_TRACK,
	PROP_GPART,
	PROP_ARRANGE,
	PROP_X,
	PROP_Y,
	PROP_LEFT,
	PROP_RIGHT,
	PROP_LABEL,
	//PROP_FG_COLOUR,
};

/* Private part of the GnomeCanvasPixbuf structure */
typedef struct {
	GdkPixbuf*       pixbuf;

	double           width;
	double           height;

	double           x;                   // translations
	double           y;

	guint x_in_pixels : 1;
	guint y_in_pixels : 1;

	/* Whether the pixbuf has changed */
	guint need_pixbuf_update : 1;

    /* Anchor */
    GtkAnchorType anchor;

	int old_width; //used to detect if part dimensions have changed.
	int old_height;
} PartPrivate;


static void gnome_canvas_map_class_init  (GnomeCanvasMapClass*);
static void gnome_canvas_map_init        (GnomeCanvasMap*);
static void gnome_canvas_map_destroy     (GtkObject*);
static void gnome_canvas_map_set_property(GObject*, guint param_id, const GValue*, GParamSpec*);
static void gnome_canvas_map_get_property(GObject*, guint param_id, GValue*, GParamSpec*);
static void gnome_canvas_map_realize     (GnomeCanvasItem*);
static void gnome_canvas_map_render      (GnomeCanvasItem*, GnomeCanvasBuf*);
//static void gnome_canvas_map_draw      (GnomeCanvasItem *item, GdkDrawable *drawable, int x, int y, int width, int height);
static void invalidate_pixbuf            (GnomeCanvasMap*);
static void map_make_pixbuf              (GnomeCanvasItem*);
static void gnome_canvas_map_update      (GnomeCanvasItem*, double *affine, ArtSVP *clip_path, int flags);
#ifdef NEVER
static void gnome_canvas_map_debug       (GnomeCanvasMap*);
#endif
static void set_y                        (GnomeCanvasMap*, double y);
static bool map_item_is_visible          (GnomeCanvasItem*);

static GnomeCanvasItemClass *parent_class;


GType
gnome_canvas_map_get_type(void)
{
	static GType map_type;

	if(!map_type){
		static const GTypeInfo object_info = {
			sizeof (GnomeCanvasMapClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_canvas_map_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,                                       /* class_data */
			sizeof(GnomeCanvasMap),
			0,                                          /* n_preallocs */
			(GInstanceInitFunc)gnome_canvas_map_init,
			NULL                                        /* value_table */
		};

		map_type = g_type_register_static(GNOME_TYPE_CANVAS_SHAPE, "GnomeCanvasMap", &object_info, 0);
	}
	return map_type;
}

static void
gnome_canvas_map_class_init(GnomeCanvasMapClass *class)
{
	GObjectClass *gobject_class;
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	gobject_class = (GObjectClass *) class;
	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = g_type_class_peek_parent(class);

	gobject_class->set_property = gnome_canvas_map_set_property;
	gobject_class->get_property = gnome_canvas_map_get_property;

	g_object_class_install_property(gobject_class, PROP_POINTS,
                 g_param_spec_boxed("points", NULL, NULL, GNOME_TYPE_CANVAS_POINTS,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	/*
	g_object_class_install_property(gobject_class, PROP_TRACK,
                 g_param_spec_int("track", NULL, NULL, 0, G_MAXINT, 0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	*/
	g_object_class_install_property(gobject_class, PROP_GPART,
                 g_param_spec_pointer("gpart", NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_ARRANGE,
                 g_param_spec_pointer("arrange", NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_X,
                 g_param_spec_double("x", NULL, NULL, -10000.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_Y,
                 g_param_spec_double("y", NULL, NULL, -10000.0, 100000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_RIGHT,
                 g_param_spec_double("right", NULL, NULL, 0.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_LEFT,
                 g_param_spec_double("left", NULL, NULL, 0.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_LABEL,
                 g_param_spec_string("label", NULL, NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	object_class->destroy = gnome_canvas_map_destroy;

	item_class->realize= gnome_canvas_map_realize;
	item_class->render = gnome_canvas_map_render;
	item_class->update = gnome_canvas_map_update;
}

static void
gnome_canvas_map_init(GnomeCanvasMap* part)
{
	part->path_def              = NULL;
	part->gpart                 = NULL;
	part->transient_offset_left = 0.0;
	part->pts                   = gnome_canvas_points_new(5);
	part->tiles                 = NULL;
	part->min_height            = 10;
	part->arrange               = NULL;

    int i; for(i=0;i<10;i++) part->pts->coords[i] = 0;

	//pixbuf stuff:
    PartPrivate* pixbuf_priv = g_new0(PartPrivate, 1);
	part->private = pixbuf_priv;
    //priv->anchor = GTK_ANCHOR_NW;
	pixbuf_priv->pixbuf = NULL;

	GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(part);
	shape->priv->outline_rgba = config->part_outline_colour;

	shape->priv->width = PART_OUTLINE_WIDTH;
	shape->priv->width_pixels = 1;
}

static void
gnome_canvas_map_destroy(GtkObject *object)
{
	GnomeCanvasMap* part;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_CANVAS_MAP (object));

	part = GNOME_CANVAS_MAP(object);

	/* remember, destroy can be run multiple times! */

	if(part->path_def) gnome_canvas_path_def_unref(part->path_def);
	part->path_def = NULL;

	if(part->pts) gnome_canvas_points_unref(part->pts);
	part->pts = NULL;

	if (GTK_OBJECT_CLASS (parent_class)->destroy) (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
pixbuf_transparent_corner(GdkPixbuf* pixbuf)
{
  // make the top left corner of the pixbuf transparent.

  g_return_if_fail(pixbuf);
  if(gdk_pixbuf_get_width(pixbuf) < 4) return;

  guchar* pixels = gdk_pixbuf_get_pixels   (pixbuf);
  int rowstride  = gdk_pixbuf_get_rowstride(pixbuf);
  int corner_width = CORNER_SIZE;

  int i,x;
  int row = 0;
  for(x=corner_width;x>=0;x--){
    for(i=0;i<x;i++) pixels[i*4 + 3 + row*rowstride] = 0;
    row++;
  }
}


double
map_get_min_left(GnomeCanvasMap *part)
{
  //return the canvas position corresponding to the beginning of the Part's region.

  return 0;
}


double
map_get_max_right(GnomeCanvasMap *part)
{
  //return the canvas position corresponding to the end of the Part's file source.

  return 100000;
}


static void
map_set_points(GnomeCanvasMap* part, GnomeCanvasPoints* points)
{
  GnomeCanvasItem* item = GNOME_CANVAS_ITEM(part);
  gboolean dimensions_changed = FALSE;

  if(points->coords[3] > MAX_PART_HEIGHT){ perr ("bad coords. height too big! %.2f", points->coords[3]); return; }

  if(part->path_def) gnome_canvas_path_def_unref(part->path_def);

  if(!points){
    part->path_def = gnome_canvas_path_def_new();
    gnome_canvas_shape_set_path_def(GNOME_CANVAS_SHAPE(part), part->path_def);
    return;
  }

  // make points integer so the item aligns with the pixbuf
  int i;
  for(i=0;i<points->num_points;i++){
    points->coords[i] = floor(points->coords[i]);
  }

  //enforce minimum height:
  float top = points->coords[1];
  points->coords[5] = MAX(points->coords[5], top + part->min_height);
  points->coords[7] = MAX(points->coords[7], top + part->min_height);

  int old_width  = ((PartPrivate*)part->private)->old_width;
  int old_height = ((PartPrivate*)part->private)->old_height;

  //optimise the path def to the number of points
  part->path_def = gnome_canvas_path_def_new_sized(points->num_points+1);

#if 0
	/* No need for explicit duplicate, as closepaths does it for us (Lauris) */
	/* See if we need to duplicate the first point */
	duplicate = ((points->coords[0] != points->coords[2 * points->num_points - 2])
		     || (points->coords[1] != points->coords[2 * points->num_points - 1]));
#endif
	
  if(points->coords[0]>MAX_X_POS) errprintf("GnomeCanvasMap::%s(): max x position exceeded. (x=%.1f)\n", __func__, points->coords[0]);

  //--------

  //set the coords into the path_def:

  gnome_canvas_path_def_moveto(part->path_def, points->coords[0], points->coords[1]);

  for(i=1;i<points->num_points;i++){
    if(points->coords[(i*2)] > MAX_PART_WIDTH){
      points->coords[(i*2)] = MAX_PART_WIDTH;//FIXME
      errprintf("GnomeCanvasMap::%s(): max width exceeded. (x2=%.1f %.1f)\n", __func__, points->coords[2], points->coords[4]);
    }
    gnome_canvas_path_def_lineto(part->path_def, points->coords[i * 2], points->coords[(i * 2) + 1]);
  }

  gnome_canvas_path_def_closepath(part->path_def);

  gnome_canvas_shape_set_path_def(GNOME_CANVAS_SHAPE(part), part->path_def);

  int new_width  = item->x2 - item->x1 + 0.5;
  int new_height = item->y2 - item->y1 + 0.5;
  if((new_width != old_width) || (new_height != old_height)) dimensions_changed  = TRUE;
  //if(dimensions_changed) printf("%s(): dimensions changed! new_height=%i old_height=%i\n", __func__, new_height, old_height);
  ((PartPrivate*)part->private)->old_width  = new_width;
  ((PartPrivate*)part->private)->old_height = new_height;

  if(dimensions_changed) invalidate_pixbuf(part);
}


static void
map_set_gpart(GnomeCanvasMap* part, AMPart* gpart)
{
  // set the gui data struct associated with the canvas item.
  // -this should only be done when the item is first created.

  // -this is currently being used as an initialiser, for want of anywhere better to put it (why not in _init?).

  g_return_if_fail(part);
  g_return_if_fail(gpart);
  part->gpart = gpart;

  GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(part);
  shape->priv->fill_rgba = song->palette[gpart->bg_colour]; //we need to set something for this so that the item is opaque to mouse clicks.
  shape->priv->fill_set = 1;

  ((PartPrivate*)part->private)->need_pixbuf_update = TRUE;

  set_y(part, 1.0);
}


static void
set_x(GnomeCanvasMap* part, double x)
{
  // set a new start position for the part.

  GnomeCanvasPoints *pts = part->pts;
  double w = arr_spos2px(part->arrange, &part->gpart->length);
  pts->coords[0] = x+CORNER_SIZE;
  pts->coords[2] = x+w;
  pts->coords[4] = x+w;
  pts->coords[6] = x;
  pts->coords[8] = x;
  map_set_points(part, pts);
}


static void
set_y(GnomeCanvasMap *part, double y)
{
  GnomeCanvasPoints *pts = part->pts;
  double h = 12.0;
  pts->coords[1] = y;
  pts->coords[3] = y;
  pts->coords[5] = y+h;
  pts->coords[7] = y+h;
  pts->coords[9] = y+CORNER_SIZE;
  map_set_points(part, pts);
}


double
gnome_canvas_map_get_x(GnomeCanvasMap *part)
{
  /*
  get the x position from the path_def.
  */

  GnomeCanvasPathDef *path_def = part->path_def; //or does the path_def belong to the parent struct?
  ArtBpath* bpath = gnome_canvas_path_def_first_bpath(path_def);
  //printf("gnome_canvas_map_get_x(): x1=%.1f x2=%.1f x3=%.1f\n", bpath->x1, bpath->x2, bpath->x3);
  //printf("gnome_canvas_map_get_x(): x3=%.1f\n", bpath->x3);

  return bpath->x3; // -5.0? 
}


static void
set_left(GnomeCanvasMap *part, double left)
{
  //this is used during transitory resize operations.
  //-the left of the part is set without affecting the right.
  //-it doesnt access core.

  printf("gnomeCanvasMap::set_left(): left=%.1f.\n", left);

  GnomeCanvasPoints *pts = part->pts;
  left = MAX(left, map_get_min_left(part));

  //if(x > 100000){errprintf("set_right(): x too big (%.0f).\n", x); return;}

  double previous_left = pts->coords[6];
  part->transient_offset_left += left - previous_left;

  pts->coords[0] = left + CORNER_SIZE;
  pts->coords[6] = left;
  pts->coords[8] = left;

  map_set_points(part, pts);
}


static void
set_right(GnomeCanvasMap *part, double right)
{
  printf("gnomeCanvasMap::set_right(): right=%.1f px.\n", right);

  //this is used in resize operations, so dont get the width from core.
  //@param right is in pixels.

  right = MIN(right, map_get_max_right(part));

  GnomeCanvasPoints *pts = part->pts;

  pts->coords[2] = right;
  pts->coords[4] = right;
  map_set_points(part, pts);
}


double
gnome_canvas_map_get_right(GnomeCanvasMap *part)
{
  GnomeCanvasPoints *pts = part->pts;
  return pts->coords[2];
}


static void
set_label(GnomeCanvasMap *part, char* label)
{
  PF;
  invalidate_pixbuf(part);
}


static void
gnome_canvas_map_set_property(GObject *object, guint param_id, const GValue *value, GParamSpec *pspec)
{
	GnomeCanvasPoints *points;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GNOME_IS_CANVAS_MAP(object));

	GnomeCanvasItem *item = GNOME_CANVAS_ITEM(object);
	GnomeCanvasMap *part = GNOME_CANVAS_MAP(object);

	switch (param_id) {
	case PROP_POINTS:
		points = g_value_get_boxed(value);
		map_set_points(part, points);
		gnome_canvas_item_request_update(item);
		break;
	case PROP_GPART:
		{
			AMPart* gpart = g_value_get_pointer(value);
			map_set_gpart(part, gpart);
			gnome_canvas_item_request_update(item);
		}
		break;
	case PROP_ARRANGE:
		part->arrange = g_value_get_pointer(value);
		break;
    case PROP_X:
        set_x(part, g_value_get_double(value));
        gnome_canvas_item_request_update(item);
		break;
    case PROP_Y:
		pwarn("PROP_Y");
        set_y(part, g_value_get_double(value));
        gnome_canvas_item_request_update(item);
		break;
    case PROP_LEFT:
        set_left(part, g_value_get_double(value));
        gnome_canvas_item_request_update(item);
		break;
    case PROP_RIGHT:
        set_right(part, g_value_get_double(value));
        gnome_canvas_item_request_update(item);
		break;
    case PROP_LABEL:
        set_label(part, g_value_dup_string(value));
        gnome_canvas_item_request_update(item);
		break;
 	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnome_canvas_map_get_property(GObject *object, guint param_id, GValue *value, GParamSpec *pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(GNOME_IS_CANVAS_MAP (object));

	//GnomeCanvasMap* poly = GNOME_CANVAS_MAP(object);

	switch(param_id){
		case PROP_POINTS:
			break;
		case PROP_X:
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
	}
}


static void
gnome_canvas_map_realize(GnomeCanvasItem *item)
{
	//GnomeCanvasMap *part = GNOME_CANVAS_MAP(item);

	if (parent_class->realize)
		(* parent_class->realize) (item);

	if (!item->canvas->aa) {
		printf("gnome_canvas_map_realize(): FIXME gdk mode.\n");
		/*
		gcbp_ensure_gdk (shape);

		g_assert(item->canvas->layout.bin_window != NULL);

		shape->priv->gdk->fill_gc = gdk_gc_new (item->canvas->layout.bin_window);
		shape->priv->gdk->outline_gc = gdk_gc_new (item->canvas->layout.bin_window);
		*/
	}
}


static void
gnome_canvas_map_render(GnomeCanvasItem *item, GnomeCanvasBuf *buf)
{
  //buf is not the whole canvas, its a tile of usually 256 x 64.

  //item->x1 etc is the canvas item *bounding box* - it is slightly bigger than the specified size of the Part.

  //printf("gnome_canvas_map_render(): buf: x=%i->%i y=%i->%i. item: y=%.1f->%.1f\n", buf->rect.x0, buf->rect.x1, buf->rect.y0, buf->rect.y1, item->y1, item->y2);

  GnomeCanvasMap *part = GNOME_CANVAS_MAP(item);
  GnomeCanvasShape *shape = GNOME_CANVAS_SHAPE(item);
  //gnome_canvas_map_debug(part);

  if(shape->priv->fill_svp != NULL)
		gnome_canvas_render_svp (buf,
			shape->priv->fill_svp,
			shape->priv->fill_rgba);

  if(shape->priv->outline_svp != NULL)
		gnome_canvas_render_svp (buf,
			shape->priv->outline_svp,
			shape->priv->outline_rgba);

//#if 0
  //pixbuf stuff:

  if(!part->pixbuf) map_make_pixbuf(item);
  if(!part->pixbuf) return; //part too small?

  //FIXME the difference between the bounding box and start of the pixbuf is not the same as priv->width.
  //int outline_width = (int)shape->priv->width;
  int outline_width = PART_OUTLINE_WIDTH;

  int buf_top    = buf->rect.y0;
  int buf_bottom = buf->rect.y1;
  int buf_left   = buf->rect.x0;
  int buf_right  = buf->rect.x1;
  int part_top    = item->y1 + outline_width;
  int part_bottom = item->y2 - outline_width;
  int part_left   = item->x1 + outline_width;
  int part_right  = item->x2 - outline_width;

  GdkPixbuf* dest_pixbuf = gdk_pixbuf_new_from_data(buf->buf,
                            GDK_COLORSPACE_RGB,
                            HAS_ALPHA_FALSE, //HAS_ALPHA_TRUE,   crashes if you set TRUE
                            8,
                            buf->rect.x1 - buf->rect.x0,
                            buf->rect.y1 - buf->rect.y0,
                            buf->buf_rowstride,
                            NULL, NULL);

  char debug_x[64];
  char debug_y[64];

#define USE_GDK_COMPOSITE 1
#ifdef USE_GDK_COMPOSITE
  int x0=0, y0=0, x1=0, y1=0;    //the rect area in the tile we are rendering into.
  //int part_width  = gdk_pixbuf_get_width(part->pixbuf); 
  double offset_x, offset_y;
  gboolean draw = TRUE;

  if(buf->rect.y0 > item->y1){   //we are not rendering the top of the Part.
    if(buf_top < part_bottom){
      snprintf(debug_y, 64, "y-mid");
      y0 = 0;
      y1 = part_bottom - buf_top;// gdk_pixbuf_get_height(part->pixbuf) - buf->rect.y0;
    } else draw = FALSE;
  }else{
    snprintf(debug_y, 64, "y-1st");
    y0 = part_top - buf_top;     //the distance of the part top from the start of this tile.

    y1 = MIN(part_bottom,        //complate part: y1 is part bottom, ie item->y2.
             buf_bottom);        //partial part:  y1 is start of next block.

  }
  offset_y = part_top - buf_top;

  if(buf->rect.x0 > item->x1){   //we are not rendering the lhs of the Part.
    if(buf_left < part_right){   //we are off the end of the Part.
      snprintf(debug_x, 64, "x-2nd");

      x0 = 0;
      x1 = MIN(part_right - buf_left, buf_right - buf_left);

      if(x0 > x1){ 
        //warnprintf("gnome_canvas_map_render(): x0 > x1 : x1=%i x0=%i. Not drawing.\n", x1, x0);
        draw = FALSE;
      }
    } else draw = FALSE;
  }else{
    snprintf(debug_x, 64, "x-1st");
    x0 = part_left - buf_left;
    x1 = MIN(part_right - buf_left, buf_right - buf_left);
      if(x0 > x1){ 
        //warnprintf("gnome_canvas_map_render(): x0 > x1 : x1=%i x0=%i. Not drawing.\n", x1, x0);
        draw = FALSE;
      }
  }
  offset_x = part_left - buf_left - part->transient_offset_left;


  //bounds checking:

  //occasionally the bounds are out by a small amount but it doesnt seem to have any adverse
  //effects so i have disabled the warning messages.

  //if(x1 < 0 || x0 < 0 ) warnprintf("gnome_canvas_map_render(): <0: x1=%i x0=%i\n", x1, x0);
  //if(x0 < 0) warnprintf("gnome_canvas_map_render(): %s x < 0.\n", debug_x);
  x0 = MAX (x0, 0);
  //if(x0 > (buf_right - buf_left)) warnprintf("gnome_canvas_map_render(): %s x0 > buf_width.\n", debug_x);
  x0 = MIN (x0, buf_right - buf_left);
  y0 = MAX (y0, 0);
  y0 = MIN (y0, buf_bottom - buf_top);

  //if(x1 < 0) warnprintf("gnome_canvas_map_render(): %s x1 < 0.\n", debug_x);
  x1 = MAX (x1, 0);
  //if(x1 > (buf_right - buf_left)) warnprintf("gnome_canvas_map_render(): %s x1 > buf_width.\n", debug_x);
  x1 = MIN (x1, buf_right - buf_left);
  y1 = MAX (y1, 0);
  y1 = MIN (y1, buf->rect.y1 - buf->rect.y0);

  //int render_height = MIN(gdk_pixbuf_get_height(part->pixbuf), buf->rect.y1 - buf->rect.y0 - y0);

  //if(x0==0 && ((x1 - x0) > part_width)) warnprintf("gnome_canvas_map_render(): too wide.\n");
 
  //printf("gnome_canvas_map_render(): %i: %s %s y0=%i y1=%i offset_y=%.1f\n", pnum, debug_x, debug_y, y0, y1, offset_y);
  if(draw){
    gdk_pixbuf_composite(part->pixbuf,
                   dest_pixbuf,
                   x0, y0,                    //int dest x,y
                   x1 - x0, y1 - y0,          //dest w,h
                   offset_x, offset_y,        //double offset (translation)
                   1.0, 1.0,                  //double scale
                   GDK_INTERP_NEAREST,
                   255);
  }
#endif

#ifdef USE_GDK_COPY
  //looks like gdk_pixbuf_copy_area requires both pixbufs to have the same transparency options
  //which is not the case for us.
  int src_x=0, src_y=0;
  int width=0, height=0;
  int dest_x=0, dest_y=0;
  int src_height  = gdk_pixbuf_get_height(part->pixbuf);
  int dest_width  = gdk_pixbuf_get_width (dest_pixbuf);
  int dest_height = gdk_pixbuf_get_height(dest_pixbuf);
  gboolean draw = TRUE;
  printf(" canvas has alpha: %i\n", gdk_pixbuf_get_has_alpha(dest_pixbuf));

  if(buf_top > item->y1){ //we are not rendering the top of the Part.
    if(buf_top < part_bottom){ //we are off the bottom.
      snprintf(debug_y, 64, "y-mid");
      src_y = buf_top - part_top;           //distance of buffer start from part start
      height = MIN(buf_bottom - buf_top,       //distance from buffer start to buffer end
                   part_bottom - buf_top);     //or distance from buffer start to part end.
      printf(" a: height: buf_bottom =%i - buf_top=%i\n", buf_bottom, buf_top);
      printf(" b: height: part_bottom=%i - buf_top=%i\n", part_bottom, buf_top);
      dest_y = 0;
    } else draw = FALSE;
  }else{
    snprintf(debug_y, 64, "y-1st");
    src_y = 0;
    height = MIN(buf_bottom - part_top, //distance from part start to buffer end.
                 part_bottom - part_top);    //or distance to part end.
      printf(" a: height: buf_bottom =%i - part_top=%i\n", buf_bottom, part_top);
    dest_y = part_top - buf_top; //distance of the part top from the start of this tile.
  }
  //offset_y = item->y1 - buf->rect.y0 + outline_width;
  if(buf->rect.y1 > item->y2){//part bottom.
    //y1 -= (outline_width+5);
  }


  if(buf->rect.x0 > item->x1){               //we are not rendering the lhs of the Part.
    if(buf_left < part_right){               //we are off the end of the Part.
      snprintf(debug_x, 64, "x-2nd");
      src_x = buf_left - part_left;          //distance of buffer start from part start
      width = MIN(buf_right - buf_left,      //distance from buffer start to buffer end
                  part_right - buf_left);    //or distance from buffer start to part end.
      dest_x = part_left - buf_left;
    } else draw = FALSE;
  }else{
    snprintf(debug_x, 64, "x-1st");
    src_x = 0;
    width = MIN(buf->rect.x1 - part_left,   //distance from part start to buffer end.
                 part_right - part_left);   //or distance to part end.
    dest_x = part_left - buf_left;
  }
  //if(buf->rect.x1 > item->x2) x1 -= (outline_width+10);//PART_BORDER;

  if(src_y < 0) errprintf("  1a: src_y=%i\n", src_y);
  if(src_y + height > gdk_pixbuf_get_height(part->pixbuf)) errprintf("  %s src_y=%i height=%i src_height=%i dest_height=%i\n", debug_y, src_y, height, src_height, dest_height);
  if(height < 1) warnprintf("  %s height=%i\n", debug_y, height);

  printf("  src_y=%i height=%i src_height=%i\n", src_y, height, gdk_pixbuf_get_height(part->pixbuf));

  //int render_height = MIN(gdk_pixbuf_get_height(part->pixbuf), buf->rect.y1 - buf->rect.y0 - y0);

  //if(x0==0 && ((x1 - x0) > render_width)) warnprintf("%s(): too wide.\n", __func__);

  //check the src area isnt bigger than its dimensions: (src_x + width <= src_pixbuf->width)
  //if(offset_x + (x1 - x0) > render_width) warnprintf("%s(): specified src area is too wide.\n", __func__);

  if((width > 0) && (height > 0) && (draw)){
  gdk_pixbuf_copy_area(part->pixbuf,       //const GdkPixbuf *src_pixbuf,
                       src_x, src_y,       //int src_x,src_y
                       width, height,      //int width,height
                       dest_pixbuf,
                       dest_x,             //int dest_x
                       dest_y);            //int dest_y
  }
#endif

	g_object_unref(dest_pixbuf);
}


static void
blit_label(GnomeCanvasMap* part)
{
  //put the cached label pixbuf onto the Part canvas pixbuf:
  gboolean hide=FALSE;
  GdkPixbuf* cache = part->label_pixbuf;
  GdkPixbuf* dest_pixbuf = part->pixbuf;
  #define PART_LABEL_LEFT_OFFSET 0
  int part_width  = gdk_pixbuf_get_width (part->pixbuf) - PART_LABEL_LEFT_OFFSET;
  int part_height = gdk_pixbuf_get_height(part->pixbuf);
  int width  = MIN(gdk_pixbuf_get_width (part->label_pixbuf), part_width);
  int height = MIN(gdk_pixbuf_get_height(part->label_pixbuf), part_height);

  if(!hide){
    gdk_pixbuf_copy_area(cache,                 //const GdkPixbuf *src_pixbuf,
                         0,0,                   //int src_x,src_y
                         width,height,          //int width,height
                         dest_pixbuf,
                         PART_LABEL_LEFT_OFFSET,//int dest_x
                         part_height - height); //int dest_y
  }
}


static bool
map_item_is_visible (GnomeCanvasItem* item)
{
  //return TRUE if an item is both with the scroll-region, and not hidden.

  GnomeCanvasMap* part = GNOME_CANVAS_MAP(item);
  Arrange* arrange = part->arrange;

  double x1, y1, x2, y2;
  gnome_canvas_item_get_bounds(item, &x1, &y1, &x2, &y2);

  int cx1, cy1, cx2, cy2;
  arrange->canvas->get_scroll_offsets(arrange, &cx1, &cy1);
  Size viewport = arr_canvas_viewport_size(arrange);
  cy2 = cy1 + viewport.height;
  cx2 = cx1 + viewport.width;

  //printf("canvas_item_is_visible(): visible region: %i -> %i item: %i -> %i.\n", cy1,cy2,(int)y1,(int)y2);

  if(y1 > cy2){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off bottom.
  if(y2 < cy1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off top.
  if(x1 > cx2){ /*printf("canvas_item_is_visible(): no. off right.\n");*/ return FALSE; }//off right.
  if(x2 < cx1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off left.

  return TRUE;
}


static void
invalidate_pixbuf(GnomeCanvasMap* part)
{
  //indicate that pixbuf is invalid:
  if(part->pixbuf) g_object_unref(part->pixbuf);
  part->pixbuf = NULL;

  if(part->tiles) g_array_free(part->tiles, FREE_SEGMENTS);
  part->tiles = NULL;
}


#include "arrange/gnome/part_item.h"
static void
map_make_pixbuf(GnomeCanvasItem *item)
{
  dbg(2, "...");

  GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(item);
  GnomeCanvasMap* part  = GNOME_CANVAS_MAP(item);

  double outline_width = shape->priv->width;
  int w     = item->x2 - item->x1 - outline_width*2; 
  int h     = item->y2 - item->y1 - outline_width*2;
  //int old_w = part->pixbuf ? gdk_pixbuf_get_width(part->pixbuf) : 0;   //this doesnt work anymore cos we have deleted the pixbuf.
  //int old_h = part->pixbuf ? gdk_pixbuf_get_height(part->pixbuf) : 0;

  if(w < 1){ /*errprintf("GnomeCanvasMap::%s(): bad dimensions (%ix%i) x1=%i x2=%i\n", __func__, w, h, item->x1, item->x2);*/ return; } //not an error - just small.
  if(h < 1){
    errprintf("GnomeCanvasMap::%s(): bad dimensions (%ix%i) x1=%i x2=%i\n", __func__, w, h, item->x1, item->x2); 
    //return;
    h=1; //it behaves better when there is a pixbuf....
  }

  //FIXME dont make a new pixbuf during transient ops if pixbuf is big enough.
  //printf("gnomeCanvasMap::%s(): w=%i oldw=%i\n", __func__, w, old_w);
  if(((PartPrivate*)part->private)->need_pixbuf_update || !part->pixbuf){
      struct timeval time_start;
      gettimeofday(&time_start, NULL);

      if(_debug_ > 1){
        double mem = w * h * 4 / (1024*1024);
        printf("gnomeCanvasMap::%s(): new pixbuf: %ix%i memory=%.3fMB '%s'.\n", __func__, w,h, mem, part->gpart->name);
      }
      if(part->pixbuf) g_object_unref(part->pixbuf);
      GdkPixbuf* pixbuf = part->pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE,//HAS_ALPHA_TRUE,
										 BITS_PER_PIXEL,
										 w,h);
      uint32_t bg_color = song->palette[part->gpart->bg_colour];
      gdk_pixbuf_fill(pixbuf, bg_color);

      //test line:
      /*
      struct _ArtDRect pts = {0.0,0.0,30.0,30.0};
      GdkColor colour_fg = {0xffff,0,0,0};
      pixbuf_draw_line(pixbuf, &pts, 2.0, &colour_fg);
      */

      gnome_canvas_part_text2pic((GnomeCanvasPart*)part); //no not here!
      blit_label(part);

      pixbuf_transparent_corner(pixbuf);

      ((PartPrivate*)part->private)->need_pixbuf_update = FALSE;

      //report_time(&time_start);
  }
}


static void
gnome_canvas_map_update(GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags)
{
  GnomeCanvasMap* part = GNOME_CANVAS_MAP(item);
  if(GNOME_CANVAS_MAP_HEIGHT(part) > MAX_PART_HEIGHT){ perr ("part too big!"); return; }

  //common part
  if(parent_class->update) (* parent_class->update) (item, affine, clip_path, flags);

  //pixbuf:

  if(map_item_is_visible(item)){      //TODO now we're using blocks, we should move the visible check into map_make_pixbuf() ?
    if(!part->pixbuf) map_make_pixbuf(item); //only make a new pixbuf if the old one has been invalidated (deleted).
  }else{
    //printf("gnome_canvas_map_update(): not visible. ignoring...\n");
  }
}


void
gnome_canvas_map_set_selected(GnomeCanvasItem *item, gboolean selected)
{
  if(selected){
    gnome_canvas_item_set(item, "outline_color_rgba", config->part_outline_colour_selected, NULL);
  }else{
    gnome_canvas_item_set(item, "outline_color_rgba", config->part_outline_colour, NULL);
  }
}


#ifdef NEVER
static void
gnome_canvas_map_debug(GnomeCanvasMap *map)
{
  GnomeCanvasPoints* pts = map->pts;
  printf("%s(): x1=%.2f x2=%.2f y1=%.2f y2=%.2f\n", __func__, pts->coords[6], pts->coords[2], pts->coords[1], pts->coords[5]);
}
#endif

