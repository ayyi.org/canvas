/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi Project. http://www.ayyi.org           |
* | copyright (C) 2012-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include <glib-object.h>
#include "icon.h"
#include "ayyi/ayyi_utils.h"
#include "svgtogglebutton.h"

struct _SvgToggleButtonPrivate
{
	gchar*     icon1;
	gchar*     icon2;
	GtkImage*  image;
};

#define SVG_TOGGLE_BUTTON_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_SVG_TOGGLE_BUTTON, SvgToggleButtonPrivate))
G_DEFINE_TYPE_WITH_PRIVATE (SvgToggleButton, svg_toggle_button, GTK_TYPE_TOGGLE_BUTTON)
enum  {
	SVG_TOGGLE_BUTTON_DUMMY_PROPERTY
};
static void     svg_toggle_button_clicked                   (GtkButton*);
//static gboolean svg_toggle_button_expose_event              (GtkWidget*, GdkEventExpose*);
//static gboolean svg_toggle_button_real_button_press_event   (GtkWidget*, GdkEventButton*);
//static gboolean svg_toggle_button_real_button_release_event (GtkWidget*, GdkEventButton*);
//static gboolean svg_toggle_button_real_motion_notify_event  (GtkWidget*, GdkEventMotion*);
static void     svg_toggle_button_finalize                  (GObject*);

#ifdef OVERRIDE_DEFAULT_PAINT //TODO
static void __gtk_button_paint (GtkButton* button, const GdkRectangle* area, GtkStateType state_type, GtkShadowType shadow_type, const gchar* main_detail);
#endif


static void
svg_toggle_button_class_init (SvgToggleButtonClass* klass)
{
	svg_toggle_button_parent_class = g_type_class_peek_parent (klass);

	GtkButtonClass* button_class = (GtkButtonClass*) klass;

	//GTK_WIDGET_CLASS (klass)->expose_event         = svg_toggle_button_expose_event;
	//GTK_WIDGET_CLASS (klass)->button_press_event   = svg_toggle_button_real_button_press_event;
	//GTK_WIDGET_CLASS (klass)->button_release_event = svg_toggle_button_real_button_release_event;
	//GTK_WIDGET_CLASS (klass)->motion_notify_event  = svg_toggle_button_real_motion_notify_event;
	button_class->clicked                          = svg_toggle_button_clicked;
	G_OBJECT_CLASS   (klass)->finalize             = svg_toggle_button_finalize;
}


static void
svg_toggle_button_init (SvgToggleButton* self)
{
	self->priv = svg_toggle_button_get_instance_private(self);
}


SvgToggleButton*
svg_toggle_button_construct (GType object_type)
{
	return (SvgToggleButton*) g_object_new (object_type, NULL);
}


GtkWidget*
svg_toggle_button_new (const char* icon)
{
	SvgToggleButton* self = svg_toggle_button_construct (TYPE_SVG_TOGGLE_BUTTON);

	SvgToggleButtonPrivate* _btn = self->priv;

	_btn->icon1 = (gchar*)icon;

	GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, icon, 16, 0, NULL);

	_btn->image = pixbuf
		? (GtkImage*)gtk_image_new_from_pixbuf(pixbuf)
		: (GtkImage*)gtk_image_new_from_stock(icon, GTK_ICON_SIZE_MENU);

	gtk_container_add(GTK_CONTAINER(self), (GtkWidget*)_btn->image);

	self->size = 16;

	return (GtkWidget*)self;
}


static SvgToggleButton*
svg_toggle_button_new_from_path (const char* svgpath)
{
	SvgToggleButton* self = svg_toggle_button_construct (TYPE_SVG_TOGGLE_BUTTON);

	g_return_val_if_fail(svgpath, self);

	SvgToggleButtonPrivate* _btn = self->priv;

	_btn->icon1 = (char*)svgpath;
	_btn->image = (GtkImage*)gtk_image_new_from_pixbuf(NULL);
	gtk_container_add(GTK_CONTAINER(self), (GtkWidget*)_btn->image);

	svg_toggle_button_set_size(self, 16);

	return self;
}


GtkWidget*
svg_toggle_button_new_full (const char* icon1, const char* icon2, int size)
{
	SvgToggleButton* self = svg_toggle_button_new_from_path (icon1);
	self->priv->icon2 = (char*)icon2;

	svg_toggle_button_set_size(self, size);

	return (GtkWidget*)self;
}


static void
svg_toggle_button_finalize (GObject* obj)
{
	G_OBJECT_CLASS (svg_toggle_button_parent_class)->finalize (obj);
}


static void
set_icon (SvgToggleButton* self, const char* icon)
{
	SvgToggleButtonPrivate* _btn = self->priv;

	GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, icon, self->size, 0, NULL);
	if(pixbuf){
		gtk_image_set_from_pixbuf(_btn->image, pixbuf);
	}else{
		int size = self->size;
		GtkIconSize s = size > 96 ? ICON_SIZE_96
			: size > 64 ? ICON_SIZE_64
			: size > 48 ? ICON_SIZE_48
			: size > 32 ? ICON_SIZE_32
			: GTK_ICON_SIZE_MENU;
		gtk_image_set_from_stock (_btn->image, _btn->icon1, s);
	}
}


void
svg_toggle_button_set_size (SvgToggleButton* self, int size)
{
	SvgToggleButtonPrivate* _btn = self->priv;

	size = (size / 4) * 4;

	if(size == self->size) return;

	self->size = size;

	if(_btn->icon1){
		set_icon(self, _btn->icon1);
	}
}


void
svg_toggle_button_set_active (SvgToggleButton* self, bool active)
{
	// like gtk_toggle_button_set_active but without emitting a signal

	// can use gtk_action_block_activate ?
	// GtkToggleButton implements GtkActivatableIface

	SvgToggleButtonPrivate* _btn = self->priv;

	if(active == ((GtkToggleButton*)self)->active) return;

	((GtkToggleButton*)self)->active = active;

	// change to the 'active' icon if there is one
	if(_btn->icon2){
		set_icon(self, active ? _btn->icon2 : _btn->icon1);
	}
	gtk_widget_set_state ((GtkWidget*)self, active ? GTK_STATE_ACTIVE : GTK_STATE_NORMAL);
	gtk_widget_queue_draw((GtkWidget*)self);
}


#ifdef OVERRIDE_DEFAULT_PAINT
static gboolean
svg_toggle_button_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	if (GTK_WIDGET_DRAWABLE (widget)) {
		GtkWidget* child = GTK_BIN (widget)->child;
		GtkButton* button = GTK_BUTTON (widget);

		GtkStateType state_type = GTK_WIDGET_STATE (widget);

		GtkShadowType shadow_type = button->depressed ? GTK_SHADOW_IN : GTK_SHADOW_OUT;

		__gtk_button_paint (button, &event->area, state_type, shadow_type, "togglebutton");

		if (child) gtk_container_propagate_expose (GTK_CONTAINER (widget), child, event);
	}
	return false;
}
#endif


static void
svg_toggle_button_clicked (GtkButton* button)
{
	PF;
	GtkToggleButton* toggle_button = GTK_TOGGLE_BUTTON (button);

	svg_toggle_button_set_active((SvgToggleButton*)button, !toggle_button->active);

	gtk_toggle_button_toggled (toggle_button);

	g_object_notify (G_OBJECT (toggle_button), "active");

	if(((SvgToggleButton*)button)->on && toggle_button->active){
		gtk_action_activate(((SvgToggleButton*)button)->on);
	}

	// There is nothing to chain up to;
	// we have re-implemented gtktogglebutton, and gtkbutton does not have a clicked fn (it connects to the signal instead).
}


#if 0
static gboolean
svg_toggle_button_real_button_press_event (GtkWidget* base, GdkEventButton* event)
{
	gboolean result = FALSE;
	//SvgToggleButton* self = (SvgToggleButton*) base;
	return result;
}


static gboolean
svg_toggle_button_real_button_release_event (GtkWidget* base, GdkEventButton* event)
{
	gboolean result = FALSE;
	//SvgToggleButton* self = (SvgToggleButton*) base;
	return result;
}


static gboolean
svg_toggle_button_real_motion_notify_event (GtkWidget* base, GdkEventMotion* event)
{
	gboolean result = FALSE;
	//SvgToggleButton* self = (SvgToggleButton*) base;
	return result;
}
#endif


#ifdef OVERRIDE_DEFAULT_PAINT
//tidied up copy of private function from gtkbutton.c - useful for debugging
static void
__gtk_button_paint (GtkButton* button, const GdkRectangle* area, GtkStateType state_type, GtkShadowType shadow_type, const gchar* main_detail)
{
	GtkBorder default_border;
	GtkBorder default_outside_border;
	gint focus_width;
	gint focus_pad;
	//if(state_type == GTK_STATE_ACTIVE) dbg(0, "state=active"); else dbg(0, "state=%i", state_type);

	if (GTK_WIDGET_DRAWABLE (button)) {
		GtkWidget* widget = GTK_WIDGET (button);
		gint border_width = GTK_CONTAINER (widget)->border_width;

		gboolean interior_focus = false;
		//gtk_button_get_props (button, &default_border, &default_outside_border, NULL, &interior_focus);
		gtk_widget_style_get (GTK_WIDGET (widget), "focus-line-width", &focus_width, "focus-padding", &focus_pad, NULL); 

		gint x = widget->allocation.x + border_width;
		gint y = widget->allocation.y + border_width;
		gint width = widget->allocation.width - border_width * 2;
		gint height = widget->allocation.height - border_width * 2;

		if (GTK_WIDGET_HAS_DEFAULT (widget) && GTK_BUTTON (widget)->relief == GTK_RELIEF_NORMAL) {
			gtk_paint_box (widget->style, widget->window, GTK_STATE_NORMAL, GTK_SHADOW_IN, area, widget, "buttondefault", x, y, width, height);

			x += default_border.left;
			y += default_border.top;
			width -= default_border.left + default_border.right;
			height -= default_border.top + default_border.bottom;
		}
		else if (GTK_WIDGET_CAN_DEFAULT (widget)) {
			x += default_outside_border.left;
			y += default_outside_border.top;
			width -= default_outside_border.left + default_outside_border.right;
			height -= default_outside_border.top + default_outside_border.bottom;
		}

		if (!interior_focus && GTK_WIDGET_HAS_FOCUS (widget)) {
			x += focus_width + focus_pad;
			y += focus_width + focus_pad;
			width -= 2 * (focus_width + focus_pad);
			height -= 2 * (focus_width + focus_pad);
		}

		if (button->relief != GTK_RELIEF_NONE || button->depressed || GTK_WIDGET_STATE(widget) == GTK_STATE_PRELIGHT)
			gtk_paint_box (widget->style, widget->window, state_type, shadow_type, area, widget, "button", x, y, width, height);

		if (GTK_WIDGET_HAS_FOCUS (widget)) {
			gint child_displacement_x;
			gint child_displacement_y;
			gboolean displace_focus;

			gtk_widget_style_get (GTK_WIDGET (widget),
				"child-displacement-y", &child_displacement_y,
				"child-displacement-x", &child_displacement_x,
				"displace-focus", &displace_focus,
				NULL);

			if (interior_focus) {
				x += widget->style->xthickness + focus_pad;
				y += widget->style->ythickness + focus_pad;
				width -= 2 * (widget->style->xthickness + focus_pad);
				height -=  2 * (widget->style->ythickness + focus_pad);
			} else {
				x -= focus_width + focus_pad;
				y -= focus_width + focus_pad;
				width += 2 * (focus_width + focus_pad);
				height += 2 * (focus_width + focus_pad);
			}

			if (button->depressed && displace_focus) {
				x += child_displacement_x;
				y += child_displacement_y;
			}

			gtk_paint_focus (widget->style, widget->window, GTK_WIDGET_STATE (widget), area, widget, "button", x, y, width, height);
		}
	}
}
#endif
