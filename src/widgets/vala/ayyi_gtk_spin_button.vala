/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
 
using GLib;
using Gtk;
 
public class Ayyi.GtkSpinButton : Gtk.SpinButton {

	Gtk.Adjustment adj;

	public GtkSpinButton(Gtk.Adjustment _adj)
	{
		adj = new Gtk.Adjustment(0.0, 0.0, 100.0, 1.0, 10.0, 10.0);
		adjustment = adj;
	}

	public override void realize ()
	{
	}
}

