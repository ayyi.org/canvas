/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

static GdkPixbuf* blank_icon = NULL;


static GtkTreeModel*
am_channel_create_plugin_model ()
{
	// TODO this model can probably be shared by all inserts on all channels..

	GtkTreeIter iter;

	GtkTreeStore* store = gtk_tree_store_new (3, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_BOOLEAN);

	gtk_tree_store_append (store, &iter, NULL);
	gtk_tree_store_set (store, &iter,
                        0, blank_icon,
                        1, NO_PLUGIN,
                        2, FALSE,
                        -1);
	gtk_tree_store_append (store, &iter, NULL);
	gtk_tree_store_set (store, &iter,
                        0, blank_icon,
                        1, "Bypass",
                        2, FALSE,
                        -1);
	gtk_tree_store_append (store, &iter, NULL);
	gtk_tree_store_set (store, &iter,
                        0, blank_icon,
                        1, "Edit",
                        2, FALSE,
                        -1);
	gtk_tree_store_append (store, &iter, NULL);
	gtk_tree_store_set (store, &iter,
                        1, "Separator",
                        2, FALSE,
                        -1);

	for(GList* l=song->plugins;l;l=l->next){
		AMPlugin* plug = l->data;

		gtk_tree_store_append (store, &iter, NULL);
		gtk_tree_store_set (store, &iter,
			0, am_plugin__get_icon(plug),
			1, plug->name,
			2, FALSE,
			-1
		);

		AyyiPlugin* plugin_shared = ayyi_plugin_at(plug->shm_num);
		if(plugin_shared){
			dbg (1, "category=%s", plugin_shared->category); //category = VST, etc
		}
	}

	return GTK_TREE_MODEL (store);
}


static void
channels_connect ()
{
	blank_icon = blank_icon ? blank_icon : gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, 16, 16);
	gdk_pixbuf_fill(blank_icon, 0);

	void song__on_channel_add (GObject* _song, AMChannel* channel, gpointer _)
	{
		if(channel->user_data){
			pwarn("user_data already set");
			return;
		}

		channel->user_data = AYYI_NEW(ChannelUserData,
			.plugin_list = am_channel_create_plugin_model()
		);
	}
	g_signal_connect(song->channels, "add", G_CALLBACK(song__on_channel_add), NULL);

	void song__on_channel_delete (GObject* _song, AMChannel* channel, gpointer _)
	{
		if(channel->user_data){
			if(channel->user_data)
				g_object_unref0(((ChannelUserData*)channel->user_data)->plugin_list);
			g_free0(channel->user_data);
		}
	}
	g_signal_connect(song->channels, "delete", G_CALLBACK(song__on_channel_delete), NULL);
}


static void
channels_on_song_load ()
{
	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	while((channel = channel_next(&iter))){
		am_channels__emit("add", channel);
	}
}

