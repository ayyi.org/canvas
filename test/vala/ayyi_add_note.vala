using GLib;
using Gdk;
using Ayyi;

[CCode(cheader_filename = "ayyi_client.h")]

public class Ayyi.AddNoteClient : Ayyi.SimpleClient
{
	private AyyiMidiRegion* part = null;
	public static int n_notes;

	public static int main(string[] args)
	{
		if(!(app = new AddNoteClient()).run(((AddNoteClient*)app)->on_shm)) return 1; else return 0;
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		GLib.List<AyyiMidiRegion*> l = AyyiSong.get_midi_part_list();
		print("n_parts=%u\n", l.length());
		if(l.length() > 0){
			part = l.first().data;
			Ayyi.Ident id = {0, 0};
			have_part(id, null);
		}else{
			char* name = AyyiSong.container_next_name(&((SongService*)ayyi.service)->song->midi_regions, "Midi Part");
			AyyiIdx src_region = 0;
			SongPos pos = {1, 0, 0};
			GPos len = {4, 0, 0};
			AyyiMidiTrack* track = AyyiSong.midi_track_next(null);
			uint colour = 0;
			uint inset = 0;
			AM.Song.add_part(MediaType.AUDIO, src_region, (Model.PoolItem*)null, track->shm_idx, &pos, AM.pos2mu(&len), colour, name, inset, have_part);
		}
	}

	public static void have_part(Ayyi.Ident obj, Error* error)
	{
		GPos start_ = {0, 1, 0};
		GPos length_ = {0, 1, 0};

		//MidiNote* note = MidiNote(); //created on stack! how to create on heap? using static member as temporary solution.
		//MidiNote note2 = MidiNote(){note=60, velocity=100, start=(int)AM.pos2samples(&start), length = (int)AM.pos2samples(&length_)};

		//note vala doesnt like creating structs in heap, we had to create a c function to do this
		MidiNote* note = midi_note_new();
		note->velocity = 100;
		note->start = AM.pos2samples(&start_); //FIXME (is relative to song or region?)
		note->length = (int)AM.pos2samples(&length_);

		AyyiMidiRegion* p = ((AddNoteClient*)app)->part;
		print("part=%s\n", (string)p->name);
		AM.Part* part = AM.Song.get_part_by_id(p->id);

		int pitch = 60;
		uint start = 0;
		MidiNote* n = null;
		while((bool)(n = (MidiNote*)AyyiSong.container_next_item(&p->events, n))){
			pitch = int.max(pitch, note->note);
			start = uint.max(start, note->start);
		}
		note->note = pitch + 1;
		note->start = start + 1;

		n_notes = AyyiSong.container_count_items(&p->events);

		//handler3: why not make it target=false? anything using it?
		AM.Song.midi_note_add(part, note, add_done);
	}

	public static void add_done(Ident ident, GLib.Error* error)
	{
		AyyiMidiRegion* p = ((AddNoteClient*)app)->part;
		int n_notes2 = AyyiSong.container_count_items(&p->events);
		if(n_notes2 == n_notes + 1){
			log_print(LogType.OK, "done.");
		}else{
			log_print(LogType.FAIL, "**** failed! n_notes: %i --> %i\n", n_notes, n_notes2);
		}

		app.quit();
	}
}


