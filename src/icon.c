/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "global.h"
#define RSVG_DISABLE_DEPRECATION_WARNINGS
#include <librsvg/rsvg.h>

#include "arrange.h"
#include "support.h"
#include "icon.h"

#include "../pixmaps/cursor-openhand.xbm"
#include "../pixmaps/cursor-openhand_mask.xbm"
#include "../pixmaps/cursor-closedhand.xbm"
#include "../pixmaps/cursor-closedhand_mask.xbm"
#include "pixmaps/scissors.xbm"
//#include "pixmaps/cursor-zoom-mask.xbm"
//#include "pixmaps/cursor-zoom.xbm"
#include "pixmaps/zoom_in.xbm"
#include "pixmaps/zoom_in_mask.xbm"

extern SMConfig* config;

/*
 *  This icon theme is in addition to the main icon theme and contains
 *  the application's custom icons.
 */
GtkIconTheme* theme = NULL;

#if 0
static GHashTable* icons = NULL;
static GHashTable* icons2 = NULL;
#endif

#if 0
static GdkPixbuf*       svg_pixbuf_surface_new (const char* filename, int size);
static cairo_surface_t* svg_surface_new  (const char* filename, int size);
static cairo_surface_t* get_icon_surface (const char* name, int size);
#endif


void
icons_init ()
{
#if 0
#ifdef WITH_VALGRIND
	icons = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, g_object_unref);
	icons2 = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, (GDestroyNotify)cairo_surface_destroy);
#else
	icons = g_hash_table_new(g_str_hash, g_str_equal);
	icons2 = g_hash_table_new(g_str_hash, g_str_equal);
#endif
#endif

	if(!icon_theme) icon_theme = gtk_icon_theme_new();
	const char* path[] = {config->svgdir};
	gtk_icon_theme_set_search_path(icon_theme, path, 1);
	gtk_icon_theme_set_custom_theme(icon_theme, "theme");
}


#ifdef WITH_VALGRIND
void
icons_uninit ()
{
#if 0
	g_hash_table_unref(icons);
	g_hash_table_unref(icons2);
#endif
}
#endif


/*
 *  'name' - the svg file name without the svg extension.
 *
 *  The icon cache only holds icons with size of 16px.
 *
 *  Caller must unref the pixbuf when finished with it.
 */
#if 0
GdkPixbuf*
get_icon (const char* name, int size)
{
	dbg(2, "name=%s", name);

	GdkPixbuf* pixbuf = (size == 16) ? g_hash_table_lookup(icons, name) : NULL;
	if(pixbuf){
		g_object_ref(pixbuf);
	}else{
		if(size == 16) dbg(3, "icon cache miss. '%s'", name);
		gchar* filename = g_strdup_printf("%s.svg", name);
		if((pixbuf = svg_pixbuf_surface_new(filename, size)) && (size == 16)){
			g_hash_table_insert(icons, (char*)name, pixbuf);
			g_object_ref(pixbuf);
		}
		g_free(filename);
	}
	return pixbuf;
}
#endif


#if 0
static cairo_surface_t*
get_icon_surface (const char* name, int size)
{
	dbg(2, "name=%s", name);

	cairo_surface_t* surface = (size == 16) ? g_hash_table_lookup(icons, name) : NULL;
	if(surface){
		g_object_ref(surface);
	}else{
		if(size == 16) dbg(3, "icon cache miss. '%s'", name);
		gchar* filename = g_strdup_printf("%s.svg", name);
		if((surface = svg_surface_new(filename, size)) && (size == 16)){
			g_hash_table_insert(icons2, (char*)name, surface);
		}
		g_free(filename);
	}
	return surface;
}
#endif


/*
 *  Create a new GtkImage widget from an svg file.
 */
#if 0
GtkWidget*
svg_image_new (const char* svgpath)
{
	gchar* path = g_strdup_printf("%s/%s.svg", config->svgdir, svgpath);

	GdkPixbuf* pixbuf = get_icon(svgpath, 16);
	if(!pixbuf){
		pwarn("svg load failed '%s'.", path);
		if(!g_file_test(path, G_FILE_TEST_EXISTS)){
			pwarn("file not found: '%s'", path);
		}
	}
	g_free(path);

	return gtk_image_new_from_pixbuf(pixbuf);
}
#endif


/*
 *  The cairo surface must be free'd after use.
 */
#if 0
static GdkPixbuf*
svg_pixbuf_surface_new (const char* filename, int size)
{
	GdkPixbuf* pixbuf = NULL;
	char* path = g_strdup_printf("%s/%s", config->svgdir, filename);

	GError* error = NULL;
	RsvgHandle* handle = rsvg_handle_new_from_file(path, &error);
	if(error){
		pwarn("%s", error->message);
		g_error_free(error);
		goto out;
	}

	/*
	 *  CAIRO_FORMAT_RGB24 "Upper 8 bits unused"
	 *  CAIRO_FORMAT_ARGB32 "Each pixel is a 32-bit quantity, with alpha in the upper 8 bits, then red, then
	 *                      green, then blue. The 32-bit quantities are stored native-endian. Pre-multiplied 
	 *                      alpha is used. (That is, 50% transparent red is 0x80800000, not 0x80ff0000.)"
	 */
	cairo_surface_t* surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, size, size);
	cairo_t* cairo = cairo_create(surface);

	cairo_set_source_rgb (cairo, 1, 1, 1);
	cairo_rectangle(cairo, 0, 0, size, size);

	cairo_save (cairo);
	float scale = ((float)size) / 200.;
	cairo_scale(cairo, scale, scale);

#ifdef HAVE_RSVG_2_48
	const char* css = "";
	if(!rsvg_handle_set_stylesheet (handle, (guint8*)css, strlen(css), &error)){
		pwarn("%s", error->message);
		g_error_free(error);
		goto out;
	}
#endif

	if(!rsvg_handle_render_cairo(handle, cairo)){
		pwarn("render failed");
	}

	pixbuf = gdk_pixbuf_new_from_data (
		cairo_image_surface_get_data(surface),
		GDK_COLORSPACE_RGB,
		HAS_ALPHA_TRUE,
		8,
		size, size,
		cairo_image_surface_get_stride(surface),
		NULL,
		NULL
	);

	cairo_destroy(cairo);
	g_object_unref(handle);

  out:
	g_free(path);

	return pixbuf;
}
#endif


#if 0
static GdkPixbuf*
svg_pixbuf_new (const char* filename, int size)
{
	char* path = g_strdup_printf("%s/%s", config->svgdir, filename);

	GdkPixbuf* pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_TRUE, 8, size, size);
	//pixbuf_clear_rect(pixbuf, &(GdkRectangle){0, 0, size, size});

	GError* error = NULL;
	RsvgHandle* handle = rsvg_handle_new_from_file(path, &error);
	if(error){
		pwarn("%s", error->message);
		g_error_free(error);
		goto out;
	}

	cairo_surface_t* surface = cairo_image_surface_create_for_data(gdk_pixbuf_get_pixels(pixbuf), CAIRO_FORMAT_ARGB32, size, size, gdk_pixbuf_get_rowstride(pixbuf));
	cairo_t* cairo = cairo_create(surface);
	cairo_set_source_rgb (cairo, 1,1,1);
	cairo_rectangle(cairo, 0, 0, size, size);
	cairo_save (cairo);

	float scale = (float)size / 200;
	cairo_scale(cairo, scale, scale);

	if(!rsvg_handle_render_cairo(handle, cairo)){
		pwarn("render failed");
	}
	cairo_surface_flush (surface);

	cairo_surface_destroy(surface);
	cairo_destroy(cairo);
	g_object_unref(handle);

	if(!pixbuf) pwarn("svg load failed: %s", path);
  out:
	g_free(path);

	return pixbuf;
}
#endif


#if 0
static cairo_surface_t*
svg_surface_new (const char* filename, int size)
{
	cairo_surface_t* surface = NULL;
	char* path = g_strdup_printf("%s/%s", config->svgdir, filename);

	GError* error = NULL;
	RsvgHandle* handle = rsvg_handle_new_from_file(path, &error);
	if(error){
		pwarn("%s", error->message);
		g_error_free(error);
		goto out;
	}

	/*
	 *  CAIRO_FORMAT_RGB24 "Upper 8 bits unused"
	 *  CAIRO_FORMAT_ARGB32 "Each pixel is a 32-bit quantity, with alpha in the upper 8 bits, then red, then green, then blue. The 32-bit quantities are stored native-endian. Pre-multiplied alpha is used. (That is, 50% transparent red is 0x80800000, not 0x80ff0000.)"
	 */
	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, size, size);
	cairo_t* cairo = cairo_create(surface);

	cairo_set_source_rgb (cairo, 1, 1, 1);
	cairo_rectangle(cairo, 0, 0, size, size);

	cairo_save (cairo);
	float scale = ((float)size) / 200.;
	cairo_scale(cairo, scale, scale);

#ifdef HAVE_RSVG_2_48
	const char* css = "";
	if(!rsvg_handle_set_stylesheet (handle, (guint8*)css, strlen(css), &error)){
		pwarn("%s", error->message);
		g_error_free(error);
		goto out;
	}
#endif

	if(!rsvg_handle_render_cairo(handle, cairo)){
		pwarn("render failed");
	}

	cairo_destroy(cairo);
	g_object_unref(handle);

  out:
	g_free(path);

	return surface;
}
#endif


#if 0
static void
create_bitmap_and_mask_from_xpm (GdkBitmap** bitmap, GdkBitmap** mask, gchar** xpm)
{
	int height, width, colors;
	char pixmap_buffer [(32 * 32)/8];
	char mask_buffer [(32 * 32)/8];
	int x, y, pix;
	int transparent_color, black_color;

	sscanf (xpm [0], "%d %d %d %d", &height, &width, &colors, &pix);

	g_assert (height == 32);
	g_assert (width == 32);
	g_assert (colors == 3);

	transparent_color = ' ';
	black_color = '.';

	for (y = 0; y < 32; y++) {
		for (x = 0; x < 32;) {
			char value = 0, maskv = 0;

			for (pix = 0; pix < 8; pix++, x++) {
				if (xpm [4+y][x] != transparent_color) {
					maskv |= 1 << pix;

					if (xpm [4+y][x] != black_color) {
						value |= 1 << pix;
					}
				}
			}

			pixmap_buffer [(y * 4 + x/8) - 1] = value;
			mask_buffer [(y * 4 + x/8) - 1] = maskv;
		}
	}

	*bitmap = gdk_bitmap_create_from_data (NULL, pixmap_buffer, 32, 32);
	*mask = gdk_bitmap_create_from_data (NULL, mask_buffer, 32, 32);
}
#endif


void
create_cursors (void)
{
	GdkBitmap* bitmap;
	GdkBitmap* mask;
	GdkColor white = {0, 0xffff, 0xffff, 0xffff};
	GdkColor black = {0, 0x0000, 0x0000, 0x0000};

	app->cursors[CURSOR_MOVE] = gdk_cursor_new (GDK_FLEUR);
	app->cursors[CURSOR_PENCIL] = gdk_cursor_new (GDK_PENCIL);
	app->cursors[CURSOR_CROSSHAIR] = gdk_cursor_new (GDK_CROSSHAIR);
	//TODO try this one too:
	//sweep_cursors[CURSOR_CROSSHAIR] = gdk_cursor_new (GDK_XTERM);
	//app->cursors[CURSOR_MAG] = gdk_cursor_new (GDK_CIRCLE);
	app->cursors[CURSOR_HAND1] = gdk_cursor_new (GDK_HAND1);
	app->cursors[CURSOR_LEFT_PTR] = gdk_cursor_new (GDK_LEFT_PTR);
	app->cursors[CURSOR_WATCH] = gdk_cursor_new (GDK_WATCH);
	app->cursors[CURSOR_H_DOUBLE_ARROW] = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
	app->cursors[CURSOR_V_DOUBLE_ARROW] = gdk_cursor_new (GDK_SB_V_DOUBLE_ARROW);

	//create_bitmap_and_mask_from_xpm (&bitmap, &mask, horiz_xpm);
	//sweep_cursors[SWEEP_CURSOR_HORIZ] = gdk_cursor_new_from_pixmap (bitmap, mask, &white, &black, 8, 8);

	//create_bitmap_and_mask_from_xpm (&bitmap, &mask, horiz_plus_xpm);
	//sweep_cursors[SWEEP_CURSOR_HORIZ_PLUS] = gdk_cursor_new_from_pixmap (bitmap, mask, &white, &black, 8, 8);

	//create_bitmap_and_mask_from_xpm (&bitmap, &mask, horiz_minus_xpm);
	//sweep_cursors[SWEEP_CURSOR_HORIZ_MINUS] = gdk_cursor_new_from_pixmap (bitmap, mask, &white, &black, 8, 8);


	bitmap = gdk_bitmap_create_from_data (NULL, (const gchar *) scissors_bits, scissors_width, scissors_height);
	app->cursors[CURSOR_SCISSORS] = gdk_cursor_new_from_pixmap (bitmap, bitmap, &black, &white, cursor_openhand_x_hot, cursor_openhand_y_hot);

	void new_cursor(int i, const gchar* bits, const gchar* mask_bits, int width, int height, int x_hot, int y_hot)
	{
		GdkBitmap* bitmap = gdk_bitmap_create_from_data (NULL, (const gchar *)bits, width, height);
		GdkBitmap* mask = gdk_bitmap_create_from_data (NULL, (const gchar *)mask_bits, width, height);
		app->cursors[i] = gdk_cursor_new_from_pixmap (bitmap, mask, &black, &white, x_hot, y_hot);
	}

	//gimp zoom - too big?
	//new_cursor(CURSOR_ZOOM_IN, (const gchar*)cursor_zoom_bits, (const gchar*)cursor_zoom_mask_bits, cursor_zoom_width, cursor_zoom_height, cursor_zoom_x_hot, cursor_zoom_y_hot);

	new_cursor(CURSOR_ZOOM_IN, (const gchar*)zoom_in_bits, (const gchar*)zoom_in_mask_bits, zoom_in_width, zoom_in_height, zoom_in_x_hot, zoom_in_y_hot);

	new_cursor(CURSOR_HAND_OPEN, (const gchar*)cursor_openhand_bits, (const gchar*)cursor_openhand_mask_bits, cursor_openhand_mask_width, cursor_openhand_height, cursor_openhand_x_hot, cursor_openhand_y_hot);

	bitmap = gdk_bitmap_create_from_data (NULL, (const gchar *) cursor_closedhand_bits, cursor_closedhand_width, cursor_closedhand_height);
	mask = gdk_bitmap_create_from_data (NULL, (const gchar *) cursor_closedhand_mask_bits, cursor_closedhand_mask_width, cursor_closedhand_mask_height);
	app->cursors[CURSOR_HAND_CLOSE] = gdk_cursor_new_from_pixmap (bitmap, mask, &black, &white, cursor_closedhand_x_hot, cursor_closedhand_y_hot);
}


Icon icons[] = {
	{"record"},
	{"record-on"},
	{"solo"},
	{"solo-on"},
	{"mute"},
	{"mute-on"},
	{"record"},
	{"tr-rew"},
	{"tr-stop"},
	{"tr-play"},
	{"tr-ff"},
	{"refresh"},
};

void
create_textures ()
{
	GLuint textures[ICON_MAX] = {0,};

	glGenTextures(ICON_MAX, textures);

	void create_icon (int i, int size)
	{
		Icon* icon = &icons[i];
		icon->texture = textures[i];
		icon->size = size;
		GdkPixbuf* pixbuf = gtk_icon_theme_load_icon (icon_theme, icon->name, size, 0, NULL);

		if(pixbuf){
			glBindTexture   (GL_TEXTURE_2D, textures[i]);
			glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D    (GL_TEXTURE_2D, 0, GL_RGBA, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), 0, GL_RGBA, GL_UNSIGNED_BYTE, gdk_pixbuf_get_pixels(pixbuf));

			g_object_unref(pixbuf);
		}
	}

	int i = 0;
	for(;i<ICON_TR_REC_32;i++)
		create_icon(i, 16);
	for(;i<ICON_MAX;i++)
		create_icon(i, 32);
}


#if 0
void
create_icon_texture (const char* name, int texture_id)
{
	GdkPixbuf* pixbuf = gtk_icon_theme_load_icon (icon_theme, name, 16, 0, NULL);

	if(pixbuf){
		glBindTexture   (GL_TEXTURE_2D, texture_id);
		glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D    (GL_TEXTURE_2D, 0, GL_RGBA, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), 0, GL_RGBA, GL_UNSIGNED_BYTE, gdk_pixbuf_get_pixels(pixbuf));

		g_object_unref(pixbuf);
	}
}
#endif
