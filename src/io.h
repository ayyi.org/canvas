/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __io_h__
#define __io_h__

enum
{
	COL_INDEX = 0,
	COL_ICON,
	COL_CHECKBOX,
	IO_COL_NAME,
	COL_DEVICE,
	COL_PORT1,
	COL_PORT2,
	IO_NUM_COLS
};

typedef struct
{
   // global model
   GtkTreeStore* treestore;
   AMChannel*    channel;    // used to indicate active outputs for current focus.

   // view widgets used only by (arrange) tracks
   GtkWidget*    popup;
   GtkWidget*    treeview;
   AyyiPanel*    parent;
} TreePopup;

GtkWidget* output_menu_init   ();
void       input_menu_update  (AyyiPanel*, AMChannel*);
void       output_menu_update (AyyiPanel*, const AMChannel*);
void       output_menu_popup  ();

#ifndef __io_c__
extern TreePopup input;
extern TreePopup output;
#endif

#endif
