/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <gmodule.h>
#include "debug/debug.h"
#include "ayyi/ayyi_client.h"
#include "utils/demo/demo.h"

#undef RUN_DEMO

#ifdef RUN_DEMO
static char*
find_plugin (const char* name)
{
	char* path = 0;
	char* cwd = g_get_current_dir();

	char* paths[] = {
		g_build_filename(PACKAGE_LIB_DIR, NULL),
		g_strdup_printf("%s/%s/%s.so", cwd, "utils/demo/.libs", name),
		g_strdup_printf("%s/%s/%s.so", cwd, "../utils/demo/.libs", name)
	};

	for (int i=0;i<G_N_ELEMENTS(paths);i++) {
		if(g_file_test(paths[i], G_FILE_TEST_EXISTS)){
			path = g_strdup(paths[i]);
			break;
		}
	}

	for (int i=0;i<G_N_ELEMENTS(paths);i++) {
		g_free(paths[i]);
	}
	g_free(cwd);

	return path;
}


static gboolean
load_demo (gpointer _)
{
	typedef	AyyiPluginPtr (*infoFunc)();
	#define PLUGIN_API_VERSION 1

	char* filepath = find_plugin("libdemo");
	GModule* handle = g_module_open(filepath, G_MODULE_BIND_LOCAL);
	g_free(filepath);

	if (!handle) {
		pwarn("cannot open %s (%s)!", filepath, g_module_error());
		return G_SOURCE_REMOVE;
	}

	infoFunc plugin_get_info;
	if (g_module_symbol(handle, "plugin_get_info", (void*)&plugin_get_info)) {
		AyyiPluginPtr plugin = NULL;
		if ((plugin = (*plugin_get_info)())) {
			if (PLUGIN_API_VERSION != plugin->api_version) {
				dbg(0, "API version mismatch: \"%s\" (%s, type=%d) has version %d should be %d", plugin->name, filepath, plugin->type, plugin->api_version, PLUGIN_API_VERSION);
			}

			demoPluginPtr demo = plugin->symbols;
			if (demo) {
				demo->run(plugin);
			}
		} else {
			g_module_close(handle);
		}
	}

	return G_SOURCE_REMOVE;
}
#endif


void
run_plugins ()
{
#ifdef RUN_DEMO
	g_idle_add(load_demo, NULL);
#endif
}
