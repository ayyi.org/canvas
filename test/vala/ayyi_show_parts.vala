/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

static bool sort = false;
const OptionEntry[] entries = 
{
	{ "sort", 's', 0, OptionArg.NONE, ref sort, "order the part list by the given column", "position" },
	{ null }
};

public class Ayyi.ShowpartsClient : Ayyi.SimpleClient
{
	public static int main (string[] args)
	{
		//_debug_ = 2;

		OptionContext context = new OptionContext("Ayyi Show Parts");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}
		if(sort){
			print("sorting by position...\n");
		}

		app = new ShowpartsClient();
		return (int)app.run(((ShowpartsClient*)app)->on_shm);
	}

	private void on_shm ()
	{
		int n_parts = 0;

		print("--------------------------------------------------------------------\n");
		print("audio parts:\n");

		if(sort){
			GLib.List<AM.Part*>* ll = song->parts->list.copy();
			ll->sort((GLib.CompareFunc)sort_by_position);
			if(ll->length() != 0){
				print("  idx      %13s pos     len flg                 name             playlist\n", "");
				for(;ll!=null;ll=ll->next){
					AM.Part* part = ll->data;
					BaseRegion* region = part->ayyi;
					print_region((AyyiRegion*)region);
				}
			}
		}else{

			if((bool)ayyi_song__audio_region_next(null)){
				print("  idx      %13s pos     len flg                   id                 name             playlist\n", "");
				char bbst[64];
				AyyiRegion* part = null;
				while((bool)(part = ayyi_song__audio_region_next(part))){
					uint64 samples = part->position;
					samples2bbst(samples, bbst);
					Playlist* playlist = (part->playlist > -1) ? AyyiSong.playlist_at(part->playlist) : null;
					print("  %3i %13s %8u %7u %3i %20Lu %20s %20s\n", part->shm_idx, (string)bbst, part->position, part->length, part->flags, part->id, (string)part->name, (bool)playlist ? (string)playlist->name : "");
					//if(part->flags & deleted) print("deleted\n");
					n_parts++;
				}
			}
			else print("\tno audio parts\n");
		}

		//pending parts are now private so we are unable to show them
		/*
		GLib.List<AM.Part>* ll = song->parts_pending;
		if((bool)ll->length()){
			print("pending parts: %u\n", ll->length());
			for(;ll!=null;ll=ll->next){
				print("pending %p", (void*)ll->data);
			}
		}
		*/

		print("--------------------------------------------------------------------\n");
		print("midi parts:\n");
		if((bool)ayyi_song__midi_region_next(null)){
			print("  idx %20s %13s %8s %7s %3s                name              playlist\n", "id", "pos", "pos", "len", "flg");
			char bbst[64];
			AyyiMidiRegion* midi_part = null;
			while((bool)(midi_part = ayyi_song__midi_region_next(midi_part))){
				uint64 samples = midi_part->position;
				samples2bbst(samples, bbst);
				Playlist* playlist = (midi_part->playlist > -1) ? AyyiSong.playlist_at(midi_part->playlist) : null;
				print("  %3i %20Lu %13s %8u %7u %3i %20s %20s\n", midi_part->shm_idx, midi_part->id, (string)bbst, midi_part->position, midi_part->length, midi_part->flags, (string)midi_part->name, (bool)playlist ? (string)playlist->name : "");
				n_parts++;
			}
		}
		else print("\tno midi parts\n");

		print("--------------------------------------------------------------------\n");

		print("total parts: %i\n", n_parts);

		quit();
	}

	void print_region (AyyiRegion* part)
	{
		char bbst[64];
		uint64 samples = part->position;
		samples2bbst(samples, bbst);
		Playlist* playlist = AyyiSong.playlist_at(part->playlist);
		print("  %3i %13s %8u %7u %3i %20s %20s\n", part->shm_idx, (string)bbst, part->position, part->length, part->flags, (string)part->name, (string)playlist->name);
	}

	static int sort_by_position (AM.Part* a, AM.Part* b)
	{
		if(a == null || b == null) return 0;
		return Ayyi.pos_is_after(&a->start, &b->start) ? 1 : -1;
	}
}


