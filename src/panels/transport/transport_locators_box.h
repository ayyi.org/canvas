/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __TRANSPORT_LOCATORS_BOX_H__
#define __TRANSPORT_LOCATORS_BOX_H__

#include <gdk/gdk.h>
#include <gtk/gtkbox.h>

G_BEGIN_DECLS

#define TYPE_TRANSPORT_LOCATORS_BOX            (transport_locators_box_get_type ())
#define TRANSPORT_LOCATORS_BOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TRANSPORT_LOCATORS_BOX, TransportLocatorsBox))
#define TRANSPORT_LOCATORS_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TRANSPORT_LOCATORS_BOX, TransportLocatorsBoxClass))
#define IS_TRANSPORT_LOCATORS_BOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TRANSPORT_LOCATORS_BOX))
#define IS_TRANSPORT_LOCATORS_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TRANSPORT_LOCATORS_BOX))
#define TRANSPORT_LOCATORS_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TRANSPORT_LOCATORS_BOX, TransportLocatorsBoxClass))


typedef struct _TransportLocatorsBox      TransportLocatorsBox;
typedef struct _TransportLocatorsBoxClass TransportLocatorsBoxClass;

struct _TransportLocatorsBox
{
	GtkBox           box;
	GtkWidget*       locators_bg;
	GtkWidget*       loc_box[5];
	GtkWidget*       text_view[5];
	GtkWidget*       label[5];
	GtkWidget*       label_bg[5];

	void (* redraw) (TransportLocatorsBox*);

	// private
	char             text[5][16]; // used for comparison only
	gboolean         first_realise;
};

struct _TransportLocatorsBoxClass
{
	GtkBoxClass parent_class;
};


GType      transport_locators_box_get_type        (void) G_GNUC_CONST;
GtkWidget* transport_locators_box_new             (TransportWin*);
void       transport_locators_box_redraw          (TransportLocatorsBox*);
void       transport_locators_box_on_style_change (TransportLocatorsBox*);
void       transport_locators_box_set_text        (GtkWidget*, int loc_num, const char* text);
void       transport_locators_box_update_n_visible(GtkWidget*);

void       transport_locators_box_make_widgets    (TransportLocatorsBox*);

G_END_DECLS

#endif
