/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __window_h__
#define __window_h__

#include "config.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "model/observable.h"

typedef struct _AyyiWindow     AyyiWindow;
typedef struct _Statusbar      Statusbar;

typedef struct _AyyiPanel      AyyiPanel;
typedef struct _AyyiPanelClass AyyiPanelClass;
typedef GType                  PanelType;
typedef struct _GdlDock        GdlDock;

G_BEGIN_DECLS

#define AYYI_TYPE_WINDOW            (ayyi_window_get_type ())
#define AYYI_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_WINDOW, AyyiWindow))
#define AYYI_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_WINDOW, AyyiWindowClass))
#define AYYI_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_WINDOW))
#define AYYI_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_WINDOW))
#define AYYI_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_WINDOW, AyyiWindowClass))

typedef struct _AyyiWindowClass   AyyiWindowClass;

typedef void   (*AyyiWindowFn)    (AyyiWindow*, gpointer);

#include "menu.h"

struct _AyyiWindow {
    GtkWindow          parent_instance;
    GtkWidget*         vbox;
    GdlDock*           dock;

    Statusbar*         statusbar;

    struct {
        GtkWidget*     recent_files[4/*MAX_RECENT_SONGS*/];
        GtkWidget*     history_item;
        GtkWidget*     history;
        GtkWidget*     file;
        GtkWidget*     window;             // dynamically generated Window menu on the main menubar.
    }                  menu;
    struct {
        struct {
            GtkWidget* undo;
        }              edit;
        EditMenu       canvas_type[4];     // MAX_CANVAS_TYPES
    }                  menus;

    GtkWidget*         context_menu;

    ShellMenus*        shell_menus;

    AyyiPanel*         focussed_panel;

    int                current_width;      // used to check for changes.
};

struct _AyyiWindowClass {
    GtkWindowClass   parent_class;
    GdlDock*         master_dock;
    ArrayObservable* instances;
};

G_DEFINE_AUTOPTR_CLEANUP_FUNC           (AyyiWindow, g_object_unref)

GType       ayyi_window_get_type        () G_GNUC_CONST;
AyyiWindow* ayyi_window_new             ();
AyyiWindow* ayyi_window_construct       (GType);

AyyiWindow* window__new_floating        (AyyiPanelClass*);

void        window__destroy_panel       (AyyiWindow*, AyyiPanel*);
void        window__add_panel_from_menu (AyyiWindow*, AyyiPanelClass*);
bool        window__has_panel_type      (AyyiWindow*, PanelType);
GList*      window__get_dock_items      (AyyiWindow*);

extern Observable* windows_;

G_END_DECLS

#endif
