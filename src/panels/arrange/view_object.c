/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define __gl_actor_c__
#define __gl_canvas_priv__

#include "global.h"
#include <stdio.h>
#include <string.h>
#include <GL/gl.h>
#include <gtk/gtk.h>
#include "agl/utils.h"
#include "agl/ext.h"
#include "waveform/utils.h"
#include "support.h"
#include "arrange.h"
#include "arrange/gl_canvas.h"
#include "arrange/view_object.h"
#include "window.statusbar.h"

typedef struct
{
	guint  timer;
	int    speed;
	int    time;  // counts the number of consequtive window scrolls in order to set the speed.
	AGliPt xy;
} ScrollOp;

static ScrollOp scroll = {0, 1};


void
view_object_init (ViewObjectClass* voclass, ViewObject* vo, ViewObject prototype)
{
	void view_object_abort_change(ViewObject* obj)
	{
		obj->model->vals[0].val = obj->orig_val;
	}

	*vo = prototype;

	voclass->abort = view_object_abort_change;
	vo->class = voclass;
}


void
view_object_box (ViewObject* vo, int line_width)
{
	agl_box(line_width, vo->region.x1, vo->region.y1, vo->region.x2 - vo->region.x1, vo->region.y2 - vo->region.y1);
}


static ViewObject*
actor_pick_generic (AGlActor* _actor, AGliPt xy)
{
	// xy is relative to the top/left of the actor.

	VOActor* actor = (VOActor*)_actor;

	bool region_match (iRegion* r, int x, int y) // dupe
	{
		//dbg(0, "%i (%i %i) %i (%i %i)", xy.x, r->x1, r->x2, xy.y, r->y1, r->y2);
		return x > r->x1 && x < r->x2 && y >= r->y1 && y < r->y2;
	}

	ViewObject* object = NULL;

	for (int i=0;i<actor->n_objs;i++) {
		if(region_match(&actor->objs[i].region, xy.x, xy.y)){
			object = &actor->objs[i];
			break;
		}
	}
	return object;
}


#define SCROLL_MASK 5

/*
 *  Event handler for actors where all the features are encapsulated by ViewObjects.
 */
bool
actor__on_viewobject_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	bool handled = NOT_HANDLED;
	static ViewObject* object = NULL;
	static AGliPt offset;

	ActorPick pick = ((VOActor*)actor)->pick ? ((VOActor*)actor)->pick : actor_pick_generic;

	void viewobject_stop_timer (AGlActor* actor)
	{
		if(scroll.timer){
			scroll = (ScrollOp){.timer = (g_source_remove(scroll.timer), 0), .speed = 1, .time  = 0};
		}
	}

	void viewobject_start_mouse_timer (AGlActor* actor, AGliPt xy)
	{
		bool viewobject_mouse_timeout (gpointer data)
		{
			bool stop = G_SOURCE_REMOVE;
			AGlActor* actor = data;
			Arrange* arrange = actor->root->user_data;
			//int strength = 0;

			Pti right = {scroll.xy.x - agl_actor__width(actor)};

			if (scroll.xy.x < SCROLL_MASK){
				// scroll left
				call(arrange->canvas->scroll_left, arrange, scroll.speed/* / SCROLL_MULTIPLIER*/);
				stop = G_SOURCE_CONTINUE;
				//strength = -scroll.xy.x + SCROLL_EDGE_WIDTH + 1;
			} else if (right.x > -SCROLL_MASK) {
				// scroll right
				AyyiSongPos new_right; arr_gl_px2songpos(arrange, &new_right, scroll.xy.x + scroll.speed);
				if(ayyi_pos_is_after(&new_right, &am_object_val(&song->loc[AM_LOC_END]).sp)){
					dbg(0, "at end. will extend song...");
					ayyi_pos_sub(&new_right, &(AyyiSongPos){0, 0, 1});
					am_song__set_end(new_right.beat + 1);
				}else{
					arrange->canvas->scroll_right(arrange, scroll.speed/* / SCROLL_MULTIPLIER*/);
				}
				stop = G_SOURCE_CONTINUE;
				//strength = right.x + SCROLL_EDGE_WIDTH;
			}
			else {
				scroll.speed = 0;
			}

			/*
			int target_speed = (strength  + 2) / 4;
			int difference = target_speed - scroll.speed;
			*/

			/*
			int dx = (difference > 0) ? MAX(difference / 50, +1) // speeding up
			       : (difference < 0) ? MIN(difference / 10, -1) // slowing down
			       : 0;
			int speed = scroll.speed + dx;
			*/

			#if 0
			//if(dont_stop) scroll_op->speed = MIN((int)(scroll_op->speed + 1 * (strength/8)), MAX_SCROLL_SPEED);
			if(dont_stop) scroll_op->speed = MIN(speed, MAX_SCROLL_SPEED);
			else          scroll_op->timer = 0;
			dbg(2, "scrollspeed=%i strength=%i", scroll_op->speed, strength);
			#endif

			if(stop == G_SOURCE_REMOVE) viewobject_stop_timer(actor);
			return stop;
		}

		//Pti origin; //position that scrolling is done relative to.
		if(!scroll.timer){
			scroll.timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)viewobject_mouse_timeout, actor);
		}
		scroll.xy = xy;
	}

	switch (event->type){
		case GDK_BUTTON_PRESS:
			if(event->button.button == 1){
				dbg (1, "BUTTON_PRESS");
				if(object) pwarn("BUTTON_PRESS: previous object not cleared.");

				if((object = pick(actor, xy))){
					offset = xy;
					offset.x -= object->region.x1;
					offset.y -= object->region.y1;
					agl_actor__grab(actor);
				}
			}
			break;
		case GDK_MOTION_NOTIFY:
			if(object){
				object->class->xy_to_val(object, xy, offset);

				if ((xy.x < SCROLL_MASK) || (xy.x > agl_actor__width(actor) - SCROLL_MASK)) {
					viewobject_start_mouse_timer(actor, xy);
				}else{
					viewobject_stop_timer(actor);
				}

				switch(object->model->vals[0].type){
					case G_TYPE_AYYI_SONG_POS:
						; char bbst[16];
						shell__statusbar_print(((AyyiPanel*)actor->root->user_data)->window, 2, ayyi_pos2bbss(&am_object_val(object->model).sp, bbst));
						break;
					case G_TYPE_INT:
						break;
				}

				gtk_widget_queue_draw(actor->root->gl.gdk.widget);
				handled = true;
			}else{
				int cursor = CURSOR_NORMAL;
				ViewObject* obj;
				if((obj = pick(actor, xy))){
					cursor = obj->class->cursor;
					if(obj->hover.text) shell__statusbar_print(((AyyiPanel*)actor->root->user_data)->window, 2, obj->hover.text);
					handled = true;
				}
				set_cursor(actor->root->gl.gdk.widget->window, cursor);
			}
			break;
		case GDK_2BUTTON_PRESS:
			if(object){
				object = NULL;
			}
			break;
		case GDK_BUTTON_RELEASE:
			viewobject_stop_timer(actor);
			if(object){
				if(true){
					object->class->xy_to_val(object, xy, offset);
					//object->class->change_done(object, NULL, &pos, NULL, NULL);
					if(object->model->set) object->model->set(object->model, 0, am_object_val(object->model), NULL, NULL);
				}else{
					abort_change(object);
				}
				handled = true;
			}
			object = NULL;
			break;
		default:
			break;
	}
	return handled;
}
