/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "gl_button.h"

extern Icon icons[];

enum {
    BG = 0,
    RIPPLE_RADIUS
};

#define BG_OPACITY(B) (*button->animatables[BG]->val.f)
#define RIPPLE(B) (*button->animatables[RIPPLE_RADIUS]->val.f)

struct {
	AGliPt pt;
} GlButtonPress;

static void button_free     (AGlActor*);
static bool button_on_event (AGlActor*, GdkEvent*, AGliPt);
AGlActor*   gl_button       (int icon, ButtonAction, ButtonGetState, gpointer);

static AGlActorClass button_class = {0, "Button", (AGlActorNew*)gl_button, button_free};


AGlActor*
gl_button (int icon, ButtonAction action, ButtonGetState get_state, gpointer user_data)
{
	bool gl_button_paint (AGlActor* actor)
	{
		ButtonActor* button = (ButtonActor*)actor;

		bool state = button->get_state ? button->get_state(actor, button->user_data) : false;

		AGlRect r = {
			.w = agl_actor__width(actor),
			.h = agl_actor__height(actor)
		};

		// background
		if(RIPPLE(button) > 0.0){
			float mix = state ? RIPPLE(button) / 64.0 : 1.0 - RIPPLE(button) / 64.0;
			SET_PLAIN_COLOUR (agl->shaders.plain, colour_mix_rgba(button->bg_colour, get_style_bg_color_rgba(GTK_STATE_SELECTED), mix));
		}else if(state){
			SET_PLAIN_COLOUR (agl->shaders.plain, get_style_bg_color_rgba(GTK_STATE_SELECTED));
		}else{
			SET_PLAIN_COLOUR (agl->shaders.plain, button->bg_colour);
		}

		// hover background
		if(BG_OPACITY(button) > 0.0){ // dont get events if disabled, so no need to check state (TODO turn off hover when disabling).
			float alpha = BG_OPACITY(button);
#if 0
			uint32_t fg = _get_style_bg_color_rgba(actor->root->widget, GTK_STATE_PRELIGHT);
#else
			uint32_t fg = colour_lighter_rgba(((AGlUniformUnion*)&agl->shaders.plain->uniforms[PLAIN_COLOUR])->value.i[0], 16);
#endif
			SET_PLAIN_COLOUR (agl->shaders.plain, button->bg_colour ? colour_mix_rgba(PLAIN_COLOUR2(agl->shaders.plain), fg, alpha) : (fg & 0xffffff00) + (uint32_t)(alpha * 0xff));
		}

		if(!(RIPPLE(button) > 0.0)){
			agl_use_program((AGlShader*)agl->shaders.plain);
			agl_rect_(r);

		}else{
			// click ripple
			CIRCLE_COLOUR() = colour_lighter_rgba(((AGlUniformUnion*)&agl->shaders.plain->uniforms[PLAIN_COLOUR])->value.i[0], 32 - RIPPLE(button) / 2);
			CIRCLE_BG_COLOUR() = PLAIN_COLOUR2 (agl->shaders.plain);
			circle_shader.uniform.centre = GlButtonPress.pt;
			circle_shader.uniform.radius = RIPPLE(button);
			agl_use_program((AGlShader*)&circle_shader);
			agl_rect_(r);
		}

		// icon
		agl->shaders.texture->uniform.fg_colour = 0xffffff00 + (agl_actor__is_disabled(actor) ? 0x77 : 0xff);
		agl_use_program ((AGlShader*)agl->shaders.texture);

		agl_textured_rect(icons[button->icon + (state ? 1 : 0)].texture, r.x, r.y + r.h / 2.0 - 8.0, 16.0, 16.0, NULL);

		return true;
	}

	bool gl_button_paint_gl1 (AGlActor* actor)
	{
		ButtonActor* button = (ButtonActor*)actor;

		bool state = button->get_state ? button->get_state(actor, button->user_data) : false;

		AGlRect r = {
			.w = agl_actor__width(actor),
			.h = agl_actor__height(actor->parent)
		};

		agl_enable(0/* !AGL_ENABLE_TEXTURE_2D*/);
		if(state){
			//agl_colour_rbga(get_style_bg_color_rgba(GTK_STATE_SELECTED));
			agl_rect_(r);
		}
		if(!button->disabled && agl_actor__is_hovered(actor)){
			glColor4f(1.0, 1.0, 1.0, 0.1);
			agl_rect_(r);
		}

		glColor4f(1.0, 1.0, 1.0, 1.0);
		agl_textured_rect(bg_textures[button->icon + (state ? 1 : 0)], r.x, r.y + r.h / 2.0 - 8.0, 16.0, 16.0, NULL);

		return true;
	}

	void button_set_size (AGlActor* actor)
	{
		actor->region.y2 = actor->parent->region.y2 - actor->parent->region.y1;
	}

	void button_init (AGlActor* actor)
	{
		if(!circle_shader.shader.program) agl_create_program(&circle_shader.shader);

		actor->paint = agl->use_shaders ? gl_button_paint : gl_button_paint_gl1;
	}

	ButtonActor* button = agl_actor__new (ButtonActor,
		.actor = {
			.class    = &button_class,
			.program  = agl->shaders.plain,
			.init     = button_init,
			.paint    = agl_actor__null_painter,
			.set_size = button_set_size,
			.on_event = button_on_event,
		},
		.icon      = icon,
		.action    = action,
		.get_state = get_state,
		.user_data = user_data,
	);

	button->animatables[BG] = AGL_NEW(WfAnimatable,
		.val.f        = &button->bg_opacity,
		.start_val.f  = 0.0,
		.target_val.f = 0.0,
		.type         = WF_FLOAT
	);

	button->animatables[RIPPLE_RADIUS] = AGL_NEW(WfAnimatable,
		.val.f        = &button->ripple_radius,
		.start_val.f  = 0.0,
		.target_val.f = 0.0,
		.type         = WF_FLOAT
	);

	return (AGlActor*)button;
}


static void
button_free (AGlActor* actor)
{
	g_free(((ButtonActor*)actor)->animatables[0]);
	g_free(((ButtonActor*)actor)->animatables[1]);
	g_free(actor);
}


static bool
button_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	ButtonActor* button = (ButtonActor*)actor;

	if(button->disabled) return NOT_HANDLED;

	void animation_done (WfAnimation* animation, gpointer user_data)
	{
	}

	void ripple_done (WfAnimation* animation, gpointer user_data)
	{
		ButtonActor* button = user_data;
		RIPPLE(button) = 0.0;
	}

	switch (event->type){
		case GDK_ENTER_NOTIFY:
			dbg (1, "ENTER_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_H_DOUBLE_ARROW);

			button->animatables[BG]->target_val.f = 1.0;
			agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[BG]), animation_done, NULL);
			return AGL_HANDLED;
		case GDK_LEAVE_NOTIFY:
			dbg (1, "LEAVE_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_NORMAL);

			button->animatables[BG]->target_val.f = 0.0;
			agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[BG]), animation_done, NULL);
			return AGL_HANDLED;
		case GDK_BUTTON_PRESS:
			return AGL_HANDLED;
		case GDK_BUTTON_RELEASE:
			dbg(1, "BUTTON_RELEASE");
			if(event->button.button == 1){
				call(button->action, actor, button->user_data);

				GlButtonPress.pt = xy;

				button->ripple_radius = 0.001;
				button->animatables[RIPPLE_RADIUS]->target_val.f = 64.0;
				agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[RIPPLE_RADIUS]), ripple_done, button);

				return AGL_HANDLED;
			}
			break;
		default:
			break;
	}
	return AGL_NOT_HANDLED;
}


void
gl_button_set_sensitive (AGlActor* actor, gboolean sensitive)
{
#ifdef AGL_DEBUG_ACTOR
	dbg(0, "%s: %i", actor->name, sensitive);
#endif
	((ButtonActor*)actor)->disabled = !sensitive;
}


#if 0
static uint32_t
_get_style_bg_color_rgba (GtkWidget* widget, GtkStateType state)
{
	GtkStyle* style = gtk_widget_get_style(widget);

	GdkColor bg = style->bg[state];
	bg.red   = MIN((1 << 16) - 1, bg.red);
	bg.green = MIN((1 << 16) - 1, bg.green);
	bg.blue  = MIN((1 << 16) - 1, bg.blue);

	return color_gdk_to_rgba(&bg);
}
#endif

