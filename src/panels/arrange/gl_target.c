/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "gl_target.h"

// TODO use arr_get_selection_size
// cop->target->width = arr_get_selection_size(op->arrange, op->arrange->part_selection, &cop->target->n_tracks, NULL, NULL);

AGlActor*
target_actor (gpointer _)
{
	void target_set_state (AGlActor* actor)
	{
		SET_PLAIN_COLOUR (agl->shaders.plain, 0x00ff003f);
	}

	bool target_draw (AGlActor* actor)
	{
		Target* target = (Target*)actor;
		Arrange* arrange = actor->root->user_data;
		ArrTrk* track = target->track;

		g_return_val_if_fail(target->track, false);

		int length = arr_gl_pos2px_(arrange, &target->part->length);
		int top = track->y * vzoom(arrange);
		int height = track->height * vzoom(arrange);
		int bottom = top + track->height * vzoom(arrange);
		int w = 6;
		int l = 16;

		if (song->snap_mode) {
			// horizontal lines
			if (l > length / 2 - 2) {
				agl_rect(0,          top,        length, w); // top
				agl_rect(0,          bottom - w, length, w); // bottom
			} else {
				agl_rect(0,          top,        l,      w); // top left
				agl_rect(length - l, top,        l,      w); // top right
				agl_rect(0,          bottom - w, l,      w); // bottom left
				agl_rect(length - l, bottom - w, l,      w); // bottom right
			}

			// vertical lines
			if (l > height / 2 - 2) {
				agl_rect(0,          top + w,    w,      height - 2 * w); // left
				agl_rect(length - w, top + w,    w,      height - 2 * w); // right
			} else {
				agl_rect(0,          top + w,    w,      l - w); // top left
				agl_rect(0,          bottom - l, w,      l - w); // bottom left
				agl_rect(length - w, top + w,    w,      l - w); // top right
				agl_rect(length - w, bottom - l, w,      l - w); // bottom right
			}
		}

		SET_PLAIN_COLOUR (agl->shaders.plain, 0xffffff20);
		agl->shaders.plain->set_uniforms_(agl->shaders.plain);

		// guide lines
		agl_rect(0, -actor->region.y1 - arrange->canvas->vtab[1], 1, 2048);
		agl_rect(-4096, top + height - 1, 8192, 1);

		// box in rulerbar
		agl_rect(0, -actor->region.y1 - arrange->canvas->vtab[1], length, arrange->canvas->vtab[1] - 1);

		return true;
	}

	void target_init (AGlActor* actor)
	{
	}

	Target* actor = agl_actor__new (Target,
		.actor = {
			.name = g_strdup("Target"),
			.program = (AGlShader*)agl->shaders.plain,
			.init = target_init,
			.set_state = target_set_state,
			.paint = target_draw,
			.region = {0, 0, 1, 1}
		},
	);

	return (AGlActor*)actor;
}

