/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "mixer/fader.h"

static gint fader_buttonpress   (GtkWidget*, GdkEventButton*, gpointer);
static gint fader_buttonrelease (GtkWidget*, GdkEventButton*, gpointer);


/*
 *  Create widgets for the fader bay, including metering.
 */
GtkWidget*
fader_bay_new (AyyiMixerStrip* strip)
{
	GtkWidget* fdr_hbox = gtk_hbox_new(FALSE, 0);

	if(strip->channel){
		for(int n=0;n<strip->channel->nchans;n++){
			GtkWidget* vumeter = meter_new(strip, n);
			if(vumeter)
#if 0 // centred
				gtk_box_pack_start(GTK_BOX(fdr_hbox), vumeter, EXPAND_TRUE, FILL_TRUE, 2);
#else // left aligned
				gtk_box_pack_start(GTK_BOX(fdr_hbox), vumeter, false, false, 2);
#endif
		}
	}

	strip->fdr = fader_new (GTK_ADJUSTMENT(gtk_adjustment_new(0.0, -100.0, 6.0, 0.1, 1.0, 1.0)));
	gtk_scale_set_draw_value(GTK_SCALE(strip->fdr), FALSE);
	gtk_range_set_inverted (GTK_RANGE(strip->fdr), TRUE);

	gtk_box_pack_start(GTK_BOX(fdr_hbox), strip->fdr, FALSE, FILL_TRUE, 0); // gtk faders dont scale.

#if 0 // fader label needs more work
	GtkWidget* label = (GtkWidget*)fader_label_new();
	gtk_box_pack_start(GTK_BOX(fdr_hbox), label,  TRUE,  FILL_TRUE, 0);
#endif

	fader_set_channel(strip->fdr, strip->channel, NULL);
	g_signal_connect((gpointer)strip->fdr, "button-press-event", G_CALLBACK(fader_buttonpress), GINT_TO_POINTER(strip->strip_num));
	g_signal_connect((gpointer)strip->fdr, "button-release-event", G_CALLBACK(fader_buttonrelease), GINT_TO_POINTER(strip->strip_num));

	return fdr_hbox;
}


