/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <gmodule.h>

#define APPLICATION_SERVICE_NAME "org.ayyi.exampleplugin.ApplicationService"
#define DBUS_APP_PATH            "/org/ayyi/exampleplugin/Exampleplugin"
#define DBUS_INTERFACE           "org.ayyi.exampleplugin.Application"

const char* exampleplugin_get_hello(AyyiPluginPtr);

#define PLUGIN_TYPE_A 1
#define PLUGIN_API_VERSION 1

typedef struct ExamplePlugin {
	guint       api_version;
	char*       name;
	const char* (*get_hello)();
} *examplePluginPtr;


#define DECLARE_PLUGIN(plugin) \
	G_MODULE_EXPORT AyyiPluginPtr plugin_get_info() { \
		return &plugin; \
	}

#define DECLARE_EXAMPLE_PLUGIN(plugin) \
	G_MODULE_EXPORT examplePluginPtr example_plugin_get_info() { \
		return &plugin; \
	}

