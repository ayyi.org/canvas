using GLib;
using Gdk;
using Ayyi;

static int track_idx = 0;
static string[] tracks = null;
const OptionEntry[] entries = 
{
	//{ "track", 't', 0, OptionArg.INT, ref track_idx, "index of track to delete (use ayyi_show_tracks to get list of tracks)", "N" },
	{ ""/*G_OPTION_REMAINING*/, 0, 0, OptionArg.FILENAME_ARRAY, ref tracks, null, "TRACK" },
	{ null }
};

public class Ayyi.DeleteAudioTrackClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		track_idx = -1;

		OptionContext context = new OptionContext("Ayyi Delete Track");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}
		if(tracks == null){
			print("track not specified\n");
			app = new DeleteAudioTrackClient();
			app.run(((DeleteAudioTrackClient*)app)->on_shm_help);
			return 1;
		}
		track_idx = tracks[0].to_int();

		if(track_idx < 0){
			print ("track not specified\n");
			app = new DeleteAudioTrackClient();
			app.run(((DeleteAudioTrackClient*)app)->on_shm_help);
			return 1;
		}
		print("deleting track %i ...\n", track_idx);

		app = new DeleteAudioTrackClient();

		if(!app.run(((DeleteAudioTrackClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		AyyiTrack* track = AyyiSong.get_audio_track(track_idx);

		if(track == null){
			print("track not found\n");
			print_track_numbers();
			abort();
			return;
		}

		print("deleting: track %i: %s\n", track_idx, (string)track->name);

		AyyiSong.delete_item(&((SongService*)client->services[0])->song->audio_tracks, (Ayyi.Item*)track, (ident, error) => {
			log_print(LogType.OK, "done.");
			quit();
		});
	}

	private void on_shm_help()
	{
		print_track_numbers();
		quit();
	}

	private void print_track_numbers()
	{
		print("available tracks:\n");

		AyyiTrack* trk = null;
		while((bool)(trk = AyyiSong.audio_track_next(trk))){
			print("  %2i: %s\n", trk->shm_idx, (string)trk->name);
		}
		print("\n");
	}

	public void abort()
	{
		log_print(LogType.FAIL, "aborted.");
		quit();
	}
}


