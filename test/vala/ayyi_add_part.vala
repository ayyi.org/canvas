using GLib;
using Gdk;
using Ayyi;

[CCode(cheader_filename = "ayyi_client.h")]

public class Ayyi.AddpartClient : Ayyi.SimpleClient
{
	public static int main (string[] args)
	{
		app = new AddpartClient();

		if(!app.run(((AddpartClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm ()
	{
		string _name = "New Part";
		char* name = AyyiSong.container_next_name(&((SongService*)ayyi.service)->song->audio_regions, _name);

		AM.List<Model.PoolItem*> pool = song->pool;
		if(!(bool)pool.length()){ print("no files\n"); app.quit(); return; }
		Model.PoolItem* pool_item = pool.first();

		SongPos pos = {0, 0, 0};
		uint64 len = 10000;
		AyyiTrack* track = AyyiSong.audio_track_next(null);
		if(AyyiSong.track_is_master(track)){
			track = AyyiSong.audio_track_next(track);
		}
		print("track=%s", (string)track->name);
		AM.Song.add_part(MediaType.AUDIO, REGION_FULL, pool_item, track->shm_idx, &pos, len, 0, name, 0, on_added);
	}

	public static void on_added (Ident ident, GLib.Error* error)
	{
		log_print(LogType.OK, "part added");
		app.quit();
	}
}


