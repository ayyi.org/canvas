<div style="float:right"><a href="http://www.ayyi.org">homepage</a></div>

AyyiGtk
=======
![Alt text](file:pics/sm2.png)

*Welcome to AyyiGtk*

<b>Ayyi</b> is a proof-of-concept demonstrating the possibilities of client-server audio production.

<b>AyyiGtk</b> is the main example application for the system. It uses the <a href="http://ardour.org">libardour</a>-based client <b>Ardourd</b> to provide simple multitrack
audio arrange functionality.

Developers
----------

<a href="developers.html"><COLOUR2>developers</COLOUR2></a>

Users
-----

The gui can be used to arrange and playback blocks of pre-existing audio. It uses the ardour session format.
The same sessions can be used either in Seismix or in Ardour4.

These pages describe the operation of each Seismix window type:<br>
<a href="arrange.html"><font color="#ff6600">arrange</font></a><br>
<a href="mixer.html"><font color="#ff6600">mixer</font></a><br>
<a href="list.html"><COLOUR2>list edit</font></a><br>
<a href="event.html"><COLOUR2>event edit</font></a><br>
<a href="pool.html"><COLOUR2>pool</font></a><br>
<a href="plugin.html"><COLOUR2>plugin</font></a><br>
<a href="shortcuts.html"><COLOUR2>shortcuts</font></a><br>
<a href="inspector.html"><COLOUR2>inspector</font></a>

<a href="windows.html"><font color="#ff6600">windows</font></a><br/>
<a href="global_shortcuts.html"><font color="#ff6600">global shortcuts</font></a><br/>

Thanks
------

<a href="thanks.html"><COLOUR2>thanks</font></a>

Contact
-------

<a href="contact.html"><COLOUR2>contact</font></a>
