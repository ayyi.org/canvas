/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __gl_mixer_c__

#include "global.h"
#include "math.h"
#include <stdbool.h>
#include <GL/glx.h>
#include <gtk/gtkgl.h>
#include "agl/ext.h"
#include "agl/utils.h"
#include "agl/shader.h"
#include "agl/gtk.h"
#include "model/track_list.h"
#include "support.h"
#include "gl_mixer/glarea.h"
#include "gl_mixer.h"

static AGl* agl = NULL;

#define STRIP_WIDTH 40
#define STRIP_HEIGHT 128
#define PADDING 4
#define INNER_WIDTH (STRIP_WIDTH - 2 * PADDING)

#include "mixer/shaders/shaders.c"
#include "gl_mixer/strip.c"

static GObject*  gl_mixer__constructor                 (GType, guint n_construct_properties, GObjectConstructParam*);
static void      gl_mixer__add_accels                  (AyyiGlMixerWinClass*);
static void      gl_mixer__on_ready                    (AyyiPanel*);
static void      gl_mixer__on_song_load                (GObject*, gpointer);
static void      gl_mixer__on_song_unload              (GObject*, gpointer);
static void      gl_mixer__on_channel_add              (GObject*, AMChannel*, GlMixerWin*);
static void      gl_mixer__on_channel_remove           (GObject*, AMChannel*, GlMixerWin*);
static void      gl_mixer__on_channel_change           (GObject*, gpointer);
static void      gl_mixer__on_channel_selection_change (GObject*, AyyiPanel* sender, gpointer);
static void      gl_mixer__destroy                     (GtkObject*);
static void      gl_mixer__on_resize                   (GtkWidget*, GtkAllocation*);
static void      gl_mixer__on_realise                  (GtkWidget*, gpointer);
static void      gl_mixer__on_unrealise                (GtkWidget*, gpointer);
#if 0
static gint      gl_mixer__on_event                    (GtkWidget*, GdkEvent*, gpointer);
#endif
static bool      gl_mixer__on_expose                   (GtkWidget*, GdkEventExpose*, gpointer);
static void      gl_mixer__on_channel_delete           (GObject*, AMChannel*, gpointer);
static void      gl_mixer__on_plugin_changed           (GObject*, AMChannel*, gpointer);
static void      gl_mixer__on_tracks_change_by_list    (GObject*, GList*, AMChangeType, AyyiPanel*, gpointer);
static void      gl_mixer__on_track_selection_change   (Observable*, AMVal, gpointer);
static AGlActor* gl_mixer__insert_new_strip            (GlMixerWin*, AMChannel*, int pos);
static void      gl_mixer__remove_strip                (GlMixerWin*, AMChannel*);
static void      gl_mixer_set_strip_positions          (GlMixerWin*);

static void      gl_mixer__select_all                  (AyyiPanel*);

static void      gl_mixer__periodic_update             (GlMixerWin*);

G_DEFINE_TYPE (AyyiGlMixerWin, ayyi_gl_mixer_win, AYYI_TYPE_PANEL)


static void
ayyi_gl_mixer_win_class_init (AyyiGlMixerWinClass* klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	g_object_class->constructor = gl_mixer__constructor;

	object_class->destroy = gl_mixer__destroy;

	widget_class->size_allocate = gl_mixer__on_resize;

	gl_mixer__add_accels(klass);

	panel->name           = "Gl Mixer";
	panel->has_link       = true;
	panel->has_follow     = true;
	panel->has_toolbar    = true;
	panel->default_size.x = 300;
	panel->default_size.y = STRIP_HEIGHT;
	panel->config         = (ConfigParam**)NULL;
	panel->select_all     = gl_mixer__select_all;
	panel->on_ready       = gl_mixer__on_ready;

	g_signal_connect(song->channels, "change", G_CALLBACK(gl_mixer__on_channel_change), NULL);
	g_signal_connect(song->channels, "delete", G_CALLBACK(gl_mixer__on_channel_delete), NULL);

	am_song__connect("plugin-change", G_CALLBACK(gl_mixer__on_plugin_changed), NULL);
	am_song__connect("tracks-change-by-list", G_CALLBACK(gl_mixer__on_tracks_change_by_list), NULL);

	void _mixer__periodic_update(gpointer _){ gl_mixer_foreach { gl_mixer__periodic_update(mixer); } end_gl_mixer_foreach }
	am_song__connect("periodic-update", (GCallback)_mixer__periodic_update, NULL);

	agl = agl_get_instance();
}


static void
ayyi_gl_mixer_win_init (AyyiGlMixerWin* mixer)
{
}


static GObject*
gl_mixer__constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_ > -1) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_gl_mixer_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_GL_MIXER_WIN);

	GlMixerWin* mixer = (GlMixerWin*)g_object;

	mixer->area = (GtkWidget*)gl_area_new();

	mixer->style = (Style){
		.fg = 0xff9966ff,
		.bg = 0xbbbbbbff,
		.bg_selected = 0x999999ff,
		.text = 0x000000ff,
	};

	mixer->actor = agl_new_scene_gtk ((GtkWidget*)mixer->area);
	mixer->actor->name = "Mixer";
	((AGlScene*)mixer->actor)->bg_colour = 0xccccccff;
	((AGlScene*)mixer->actor)->user_data = &mixer->style;

	((GlArea*)mixer->area)->scene = (AGlScene*)mixer->actor;

	agl_actor__add_child(mixer->actor, mixer->strips = agl_actor__new(AGlActor));

	void on_zoom (Observable* zoom, AMVal val, gpointer _mixer)
	{
		GlMixerWin* mixer = _mixer;

		gl_mixer_set_strip_positions(mixer);
		agl_actor__invalidate(mixer->strips);
	}
	observable_subscribe(((AyyiPanel*)g_object)->zoom, on_zoom, mixer);

	return g_object;
}


static void
gl_mixer__destroy (GtkObject* object)
{
	AyyiPanel* panel = (AyyiPanel*)object;
	GlMixerWin* mixer = (GlMixerWin*)panel;

	int n = 0;
	#define channels_handler_disconnect(FN) if((n = g_signal_handlers_disconnect_matched (song->channels, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, mixer)) != 1) pwarn("channel handler disconnection n=%i", n);
	channels_handler_disconnect(gl_mixer__on_channel_add);
	channels_handler_disconnect(gl_mixer__on_channel_remove);
	channels_handler_disconnect(gl_mixer__on_channel_change);
	channels_handler_disconnect(gl_mixer__on_channel_selection_change);

	observable_unsubscribe(am_tracks->selection2, NULL, mixer);
}


static void
gl_mixer__on_ready (AyyiPanel* panel)
{
	GlMixerWin* mixer = (GlMixerWin*)panel;

	am_song__connect("song-load", G_CALLBACK(gl_mixer__on_song_load), mixer);
	am_song__connect("song-unload", G_CALLBACK(gl_mixer__on_song_unload), mixer);
	g_signal_connect(song->channels, "add", G_CALLBACK(gl_mixer__on_channel_add), mixer);
	g_signal_connect(song->channels, "delete", G_CALLBACK(gl_mixer__on_channel_remove), mixer);
	g_signal_connect(song->channels, "change", G_CALLBACK(gl_mixer__on_channel_change), mixer);

	gtk_widget_set_can_focus(mixer->area, true);
	gtk_widget_add_events(mixer->area, GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_SCROLL_MASK);
	panel_pack(mixer->area, EXPAND_TRUE);

	g_signal_connect((gpointer)mixer->area, "realize", G_CALLBACK(gl_mixer__on_realise), mixer);
	g_signal_connect((gpointer)mixer->area, "unrealize", G_CALLBACK(gl_mixer__on_unrealise), mixer);
	g_signal_connect((gpointer)mixer->area, "expose_event", G_CALLBACK(gl_mixer__on_expose), mixer);

	if(song->loaded){
		gl_mixer__on_song_load((GObject*)song, mixer);
	}

	observable_subscribe_with_state(am_tracks->selection2, gl_mixer__on_track_selection_change, mixer);
}


static void
gl_mixer__on_song_load (GObject* _song, gpointer user_data)
{
	// TODO do unload first once we know what to unload

	GlMixerWin* mixer = user_data;

	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	while((channel = channel_next(&iter))){
		switch(channel->type){
			case AM_CHAN_INPUT:
				gl_mixer__insert_new_strip(mixer, channel, -1);
				break;
			default:
				break;
		}
	}
}


static void
gl_mixer__on_resize (GtkWidget* widget, GtkAllocation* allocation)
{
	GlMixerWin* mixer = (GlMixerWin*)widget;

	((GtkWidgetClass*)ayyi_gl_mixer_win_parent_class)->size_allocate(widget, allocation);

	AGlActor* scene = mixer->actor;
	if(scene){
		scene->region = (AGlfRegion){0, 0, mixer->area->allocation.width, mixer->area->allocation.height};
		scene->scrollable = (AGliRegion){0, 0, mixer->area->allocation.width, mixer->area->allocation.height};
		mixer->strips->region = scene->region;
		agl_actor__set_size(mixer->actor);
	}
}


static void
gl_mixer__on_song_unload (GObject* _song, gpointer _mixer)
{
}


static void
gl_mixer__on_channel_add (GObject* _song, AMChannel* channel, GlMixerWin* mixer)
{
	PF;
	gl_mixer__insert_new_strip(mixer, channel, -1);
}


static void
gl_mixer__on_channel_remove (GObject* _song, AMChannel* channel, GlMixerWin* mixer)
{
	PF;
	gl_mixer__remove_strip(mixer, channel);
}


static void
gl_mixer__on_channel_change (GObject* _song, gpointer user_data)
{
}


static void
gl_mixer__on_channel_selection_change (GObject* _song, AyyiPanel* sender, gpointer user_data)
{
	GlMixerWin* mixer = user_data;
	AyyiPanel* panel = (AyyiPanel*)mixer;

	g_return_if_fail(AYYI_IS_GL_MIXER_WIN(mixer));

	if((panel == sender) || panel->link){
		GList* l = mixer->selectionlist;
		for(;l;l=l->next){
			GlMixerStrip* strip = g_list_nth_data(mixer->strips->children, GPOINTER_TO_INT(mixer->selectionlist->data));
			if(strip){
				strip->selected = true;
				agl_actor__invalidate((AGlActor*)strip);
			}
		}
	}
}


static int
gl_mixer__get_n_strips (GlMixerWin* mixer)
{
	return g_list_length(mixer->strips->children);
}


static void
gl_mixer_set_strip_positions (GlMixerWin* mixer)
{
	Ptf zoom = ((AyyiPanel*)mixer)->zoom->value.pt;

	GList* l = mixer->strips->children;
	for(int c=0;l;l=l->next,c++){
		AGlActor* actor = l->data;
		actor->region = (AGlfRegion){
			c * (float)STRIP_WIDTH * zoom.x + PADDING,
			9,
			(c + 1.0) * STRIP_WIDTH * zoom.x - PADDING,
			mixer->strips->region.y2
		};
		agl_actor__set_size(actor);
	}
}


/*
 *  Create a new Strip widget, and add it to the mixer window.
 *
 *  @param pos - set to -1 to insert at the default position.
 */
static AGlActor*
gl_mixer__insert_new_strip (GlMixerWin* mixer, AMChannel* channel, int pos)
{
	if(pos == -1){
		// default position is at the end, but before the master.
		pos = MAX(0, gl_mixer__get_n_strips(mixer));
	}
	dbg(1, "inserting new strip at pos=%i. n_strips=%i", pos, gl_mixer__get_n_strips(mixer));

	AGlActor* actor = agl_actor__add_child(mixer->strips, strip_new(channel));

	((ButtonActor*)((GlMixerStrip*)actor)->mute)->style = &mixer->style;

	gl_mixer_set_strip_positions(mixer);
	agl_actor__set_size(mixer->actor);

	int width = STRIP_WIDTH * song->channels->array->len;
	mixer->strips->region.x2 = width;
	gtk_widget_set_size_request(mixer->area, width, STRIP_HEIGHT);

	void mixer_on_strip_select (AMChannel* channel, gpointer _mixer)
	{
		AyyiPanel* panel = _mixer;

		if(panel->link){
			GList* channels = g_list_append(NULL, channel);
			am_collection_selection_replace((AMCollection*)song->channels, channels, NULL);
		}
	}

	((GlMixerStrip*)actor)->actions.select = mixer_on_strip_select;
	((GlMixerStrip*)actor)->actions.user_data = mixer;

	return actor;
}


static void
gl_mixer__remove_strip (GlMixerWin* mixer, AMChannel* channel)
{
	GList* l = mixer->strips->children;
	for(;l;l=l->next){
		AGlActor* actor = l->data;
		dbg(0, " * %s", actor->name);
		AMChannel* _channel = ((GlMixerStrip*)actor)->channel;
		if(_channel == channel){
			agl_actor__remove_child(mixer->strips, actor);
		}
	}
}


static void
gl_mixer__on_realise (GtkWidget* widget, gpointer _mixer)
{
	//AyyiGlMixerWin* mixer = _mixer;
}


static void
gl_mixer__on_unrealise (GtkWidget* widget, gpointer _mixer)
{
	//AyyiGlMixerWin* mixer = _mixer;
}


#if 0
static gint
gl_mixer__on_event (GtkWidget* widget, GdkEvent* event, gpointer _mixer)
{
	return NOT_HANDLED;
}
#endif


static bool
gl_mixer__on_expose (GtkWidget* widget, GdkEventExpose* event, gpointer _mixer)
{
	AyyiGlMixerWin* mixer = _mixer;

	return agl_actor__on_expose (widget, event, mixer->actor);
}


/*
 *  Channel selections can be multiple, but track selections are normally single,
 *  so following a track selection will lose the channel selection.
 */
static void
gl_mixer__on_track_selection_change (Observable* _, AMVal val, gpointer _mixer)
{
	GlMixerWin* mixer = _mixer;
	AyyiPanel* panel = _mixer;

	if(panel->link){
		if(am_tracks->selection){
			AMTrack* track = am_collection_selection_first(am_tracks);
			const AMChannel* channel = am_track__lookup_channel(track);

			GList* l = mixer->strips->children;
			for(;l;l=l->next){
				GlMixerStrip* strip = l->data;
				AMChannel* ch = strip->channel;

				if(ch == channel){
					if(!strip->selected){
						strip->selected = true;
						agl_actor__invalidate(l->data);
					}
				}else{
					if(strip->selected){
						strip->selected = false;
						agl_actor__invalidate(l->data);
					}
				}
			}
		}
	}
}


static void
gl_mixer__on_channel_delete (GObject* _song, AMChannel* channel, gpointer user_data)
{
}


static void
gl_mixer__on_plugin_changed (GObject* _song, AMChannel* ch, gpointer user_data)
{
}


static void
gl_mixer__on_tracks_change_by_list (GObject* _song, GList* tracks, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
}


static void
gl_mixer__periodic_update (GlMixerWin* mixer)
{
	GList* l = mixer->strips->children;
	int i = 0;
	for(;l;l=l->next){
		AGlActor* actor = l->data;
		if(!strcmp(actor->name, "Strip")){
			GlMixerStrip* strip = (GlMixerStrip*)actor;
			AMChannel* channel = g_ptr_array_index(song->channels->array, i);
			((Meter*)strip->meter)->level = channel->meter_level->value.f;

			strip_periodic_update(strip);
			i++;
		}
	}
	agl_actor__invalidate((AGlActor*)((GlArea*)mixer->area)->scene);
}


static void
gl_mixer__select_all (AyyiPanel* panel)
{
}


static void
gl_mixer__add_accels (AyyiGlMixerWinClass* klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	void k_zoom_in_hor (GtkAccelGroup* accel_group, gpointer user_data)
	{
		AyyiPanel* panel = app->active_panel;

		if(panel){
			float zoom = panel->zoom->value.pt.x * 1.25;
			observable_point_set(panel->zoom, &zoom, NULL);
		}
	}

	void k_zoom_out_hor (GtkAccelGroup* accel_group, gpointer user_data)
	{
		PF;
		AyyiPanel* panel = app->active_panel;

		if(panel){
			float zoom = panel->zoom->value.pt.x / 1.25;
			observable_point_set(panel->zoom, &zoom, NULL);
		}
	}

	AMAccel keys[] = {
		{"Zoom In Hor",  {{(char)'d', 0, }, {0, 0}}, k_zoom_in_hor,  NULL},
		{"Zoom Out Hor", {{(char)'a', 0, }, {0, 0}}, k_zoom_out_hor, NULL},
	};

	panel->accel_group = gtk_accel_group_new();
	make_accels(panel->accel_group, panel->action_group, keys, G_N_ELEMENTS(keys), NULL);
}


