/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

typedef  struct {
	unsigned int  vbo;
	AGlQuadVertex (*data)[];
	int           n_lines[2];
	int           size;
	bool          valid;
	float         zoom;
	fRange        range;
} Vertices;

typedef struct {
	AGlActor parent;
	Vertices vertices;
} GridActor;

static AGlActor* arr_gl_grid          (GtkWidget*);
static void      arr_gl_grid_free     (AGlActor*);
static void      arr_grid_on_locators (GObject*, GridActor*);

static AGlActorClass grid_class = {0, "Grid", (AGlActorNew*)arr_gl_grid, arr_gl_grid_free};

#include "agl/shader.h"
extern AGlShader dotted;


static AGlActor*
arr_gl_grid (GtkWidget* panel)
{
	void arr_gl_grid_set_state (AGlActor* actor)
	{
		SET_PLAIN_COLOUR (&dotted, 0xaaffaa66);
	}

	typedef void (*LineFn) (AGlActor*, float, gpointer);
	typedef struct { int n; } LineCounter;

	void line_foreach (AGlActor* actor, LineFn fn, gpointer data)
	{
		Arrange* arrange = actor->root->user_data;

		float vp_left = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
		float width = actor->region.x2 - actor->region.x1 - RR;
		float song_end = arr_gl_pos2px_(arrange, &song->loc[AM_LOC_END].vals[0].val.sp);
		float stop = MIN(vp_left + width - 2.0, song_end);

		int q = agl_power_of_two(24.0 / hzoom(arrange));
		GPos increment = {q, 0, 0};
		GPos pos = {((int)arr_px2beat(arrange, vp_left)) / q * q,};

		float x;
		while ((x = arr_gl_pos2px (arrange, &pos)) < stop) {
			double x2 = pos2px_nz (&pos);
			fn(actor, x2, data);

			pos_add(&pos, &increment);
		}
	}

	void arr_gl_grid_vertices (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		Vertices* v = &((GridActor*)actor)->vertices;

		double width = actor->region.x2 - actor->region.x1 - RR;
		double top = CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
		float song_end = arr_gl_pos2px_(arrange, &song->loc[AM_LOC_END].vals[0].val.sp);
		#define OVERSIZE 300. // allow for scrolling without rebuilding vertices
		float start = MAX(0, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 - OVERSIZE);
		float stop = MIN(-CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + width + OVERSIZE, song_end);

		//
		// major positions
		//
		int q = agl_power_of_two(24.0 / hzoom(arrange));
		GPos increment = {q, 0, 0};
		GPos pos = {((int)arr_px2beat(arrange, start)) / q * q,};
		int p = 0;
		double x;
		while ((x = arr_gl_pos2px (arrange, &pos)) < stop) {
			double x2 = pos2px_nz (&pos);
			if (!(p < v->size - 2)) {
				pwarn("! p=%i size=%i", p, v->size);
				break;
			}

			agl_set_quad (v->data, p++,
				(AGlVertex){x2, top},
				(AGlVertex){x2 + 1. / ((AyyiPanel*)arrange)->zoom->value.pt.x, top + 10000.}
			);

			pos_add(&pos, &increment);
		}
		v->n_lines[0] = p;

		//
		// locators
		//
		int n = 0;
		for (int i=1;i<AYYI_MAX_LOC - 1;i++) {
			if (song->loc[i].vals[0].val.sp.beat > -1) {
				float x = spos2px_nz (&song->loc[i].vals[0].val.sp);
				g_return_if_fail(p < v->size);
				agl_set_quad (v->data, p++,
					(AGlVertex){x, top},
					(AGlVertex){x + 1. / ((AyyiPanel*)arrange)->zoom->value.pt.x, top + 10000.}
				);
				n++;
			}
		}
		v->n_lines[1] = n;

		glBindBuffer (GL_ARRAY_BUFFER, v->vbo);
		glBufferData (GL_ARRAY_BUFFER, sizeof(AGlQuadVertex) * v->size, v->data, GL_STATIC_DRAW);

		v->zoom = ((AyyiPanel*)arrange)->zoom->value.pt.x;
		v->range = (fRange){start, stop};
		v->valid = true;
	}

	bool arr_gl_grid_draw (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		Vertices* v = &((GridActor*)actor)->vertices;

		void fn (AGlActor* actor, float x, gpointer count)
		{
			((LineCounter*)count)->n++;
		}

		if (!v->valid) {
			LineCounter count = {0};
			line_foreach (actor, fn, &count);

			const int max_lines = MAX(2, count.n + 2);
			if (v->size != max_lines) {
				v->size = max_lines + AM_LOC_MAX - 2;
				if (v->data) g_free(v->data);
				v->data = g_malloc0(sizeof(AGlQuadVertex) * v->size);
			}
			arr_gl_grid_vertices (actor);
		}

		//
		// vertical grid lines
		//
		glBindBuffer (GL_ARRAY_BUFFER, v->vbo);
		glEnableVertexAttribArray (0);
		glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

		agl_scale (actor->program, wf_context_get_zoom(CGL->wfc), 1.);
		agl_translate_abs (actor->program, builder()->offset.x / wf_context_get_zoom(CGL->wfc), builder()->offset.y);

		glDrawArrays(GL_TRIANGLES, 0, v->n_lines[0] * AGL_V_PER_QUAD);

		glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, GINT_TO_POINTER(v->n_lines[0] * sizeof(AGlQuadVertex)));
		glDrawArrays(GL_TRIANGLES, 0, v->n_lines[1] * AGL_V_PER_QUAD);

		agl_scale (actor->program, 1., 1.);
		agl_translate (actor->program, 0., 0.);

		//
		// grey out after song end
		//
		float song_end = arr_gl_pos2px_(arrange, &song->loc[AM_LOC_END].vals[0].val.sp);
		float height = actor->region.y2 + 48.;

		if (arr_gl_pos_is_in_viewport (arrange, &song->loc[9].vals[0].val.sp)) {
			SET_PLAIN_COLOUR (agl->shaders.plain, 0xaaaaaa2f);
			agl_use_program (agl->shaders.plain);
			agl_rect (song_end, CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1 - 40, 2048.0, height);
		}

		return true;
	}

	void arr_gl_grid_layout (AGlActor* actor)
	{
		Vertices* v = &((GridActor*)actor)->vertices;
		AGlActor* parent = actor->parent;

		AGlfPt size = {parent->scrollable.x2 - parent->scrollable.x1, parent->region.y2 - parent->region.y1 + 10000};
		if (actor->region.x2 != size.x || actor->region.y2 != size.y) {
			actor->region = (AGlfRegion){
				.x2 = size.x,
				.y2 = size.y,
			};

			v->valid = false;
		}
	}

	void arr_gl_grid_init (AGlActor* actor)
	{
		Vertices* v = &((GridActor*)actor)->vertices;

		actor->paint = agl->use_shaders ? arr_gl_grid_draw : agl_actor__null_painter;

		if (!v->vbo) {
			glGenBuffers(1, &v->vbo);
		}

		am_song__connect ("locators-change", G_CALLBACK(arr_grid_on_locators), actor);
	}

	AGlActor* grid = (AGlActor*)AGL_NEW(GridActor, {
		.class     = &grid_class,
		.program   = &dotted,
		.init      = arr_gl_grid_init,
		.set_size  = arr_gl_grid_layout,
		.set_state = arr_gl_grid_set_state
	});

	void grid_on_zoom (Observable* zoom, AMVal val, gpointer grid)
	{
		AGlActor* actor = grid;
		Vertices* v = &((GridActor*)grid)->vertices;

		if (zoom->value.pt.x > 2. * v->zoom || zoom->value.pt.x < 0.75 * v->zoom) {
			v->valid = false;
		}

		if (actor->root) arr_gl_grid_layout (actor);
	}
	observable_subscribe_with_state (((AyyiPanel*)panel)->zoom, grid_on_zoom, grid);

	void grid_on_hscroll (GtkWidget* widget, AGlActor* actor)
	{
		GridActor* grid = (GridActor*)actor;
		Arrange* arrange = actor->root->user_data;

		fRange range = {
			-CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1,
			MIN(-CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + agl_actor__width(actor), arr_gl_pos2px_(arrange, &song->loc[AM_LOC_END].vals[0].val.sp))
		};

		if (range.start < grid->vertices.range.start || range.end > grid->vertices.range.end) {
			grid->vertices.valid = false;
		}
	}
	g_signal_connect(((Arrange*)panel)->canvas->h_adj, "value-changed", G_CALLBACK(grid_on_hscroll), grid);

	return grid;
}


static void
arr_gl_grid_free (AGlActor* actor)
{
	Vertices* v = &((GridActor*)actor)->vertices;

	glDeleteBuffers(1, &v->vbo);
}


static void
arr_grid_on_locators (GObject* _song, GridActor* grid)
{
	grid->vertices.valid = false;
}
