/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <yaml.h>

extern unsigned char* str_tag;
extern unsigned char* map_tag;

#define PLAIN_IMPLICIT true

#define yaml_start(fp) \
	if(!yaml_emitter_initialize(&emitter)){ perr("failed to initialise yaml writer."); goto out; } \
	yaml_emitter_set_output_file(&emitter, fp); \
	yaml_emitter_set_canonical(&emitter, false); \
	EMIT_(yaml_stream_start_event_initialize(&event, YAML_UTF8_ENCODING)); \
	EMIT_(yaml_document_start_event_initialize(&event, NULL, NULL, NULL, 0)); \
	EMIT_(yaml_mapping_start_event_initialize(&event, NULL, (guchar*)"tag:yaml.org,2002:map", 1, YAML_BLOCK_MAPPING_STYLE));

#define map_open(A) \
	if(!yaml_scalar_event_initialize(&event, NULL, str_tag, (guchar*)A, -1, PLAIN_IMPLICIT, 0, YAML_PLAIN_SCALAR_STYLE)) goto error; \
	if(!yaml_emitter_emit(&emitter, &event)) goto error; \
	if(!yaml_mapping_start_event_initialize(&event, NULL, map_tag, 1, YAML_BLOCK_MAPPING_STYLE)) goto error; \
	if(!yaml_emitter_emit(&emitter, &event)) goto error; \

#define end_map \
	if(!yaml_mapping_end_event_initialize(&event)) goto error; \
	if(!yaml_emitter_emit(&emitter, &event)) goto error;

#define end_document \
	yaml_document_end_event_initialize(&event, 0); \
	yaml_emitter_emit(&emitter, &event); \
	yaml_stream_end_event_initialize(&event); \
	if(!yaml_emitter_emit(&emitter, &event)) goto error;

#define EMIT(A) \
	if(!A) return FALSE; \
	if(!yaml_emitter_emit(&emitter, &event)) return FALSE;

#define EMIT_(A) \
	if(!A) goto error; \
	if(!yaml_emitter_emit(&emitter, &event)) goto error;

bool yaml_add_key_value_pair       (const char* key, const char*);
bool yaml_add_key_value_pair_int   (const char* key, int);
bool yaml_add_key_value_pair_float (const char* key, float);
bool yaml_add_key_value_pair_bool  (const char* key, bool);
bool yaml_add_key_value_pair_array (const char* key, int[], int size);

#ifndef __yaml_save_c__
extern yaml_emitter_t emitter;
#endif

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC(yaml_emitter_t, yaml_emitter_delete)
