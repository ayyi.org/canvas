/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define __wf_private__
#include "global.h"
#include "animator.h"

static GList* items = NULL;
static guint timer = 0;

typedef struct
{
	GSourceFunc function;
	gpointer user_data;
} Item;


void
animator_add (GSourceFunc function, gpointer user_data)
{
	bool on_frame (gpointer user_data)
	{
		GList* l = items;
		while(l){
			Item* i = l->data;
			l = l->next;
			bool finished = i->function(i->user_data);
			if(finished == G_SOURCE_REMOVE){
				items = g_list_remove(items, i);
				g_free(i);
			}
		}
		return items ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
	}

	if(!items){
		timer = g_timeout_add(40, (gpointer)on_frame, NULL);
	}

	items = g_list_prepend(items, AYYI_NEW(Item,
		.function = function,
		.user_data = user_data
	));
}

