/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __main_c__

#include "global.h"
#include <getopt.h>
#include <model/am_client.h>
#include "window.h"
#include "windows.h"
#include "arrange.h"
#include "support.h"
#include "song.h"
#include "transport.h"
#include "icon.h"
#include "part_manager.h"
#include "statusbar.h"
#include "plugins.h"
#include "panels/log.h"
#ifdef HAVE_LASH
  #include <lash.h>
#endif
#ifdef USE_LV2
  #include "lv2.h"
#endif
#ifdef USE_SEED
  #include "embed.h"
#endif

static const struct option long_options[] = {
  //{ "offline",          0, NULL, 'o' },
  { "help",             0, NULL, 'h' },
  { "debug",            1, NULL, 'd' },
};

static const char* const short_options = "d:oh";

static const char* const usage =
  "Usage: %s [ options ]\n"
  //" -o --offline   dont connect to core. For testing only.\n"
  " -d --debug     show debugging information. Increase value for more output.\n"
  " -h --help      show this usage information and quit.\n"
  "\n";

extern bool   config_load      ();
extern void   config_load0     ();
extern void   create_cursors   ();
extern void   shortcuts__init  ();

static void   app_on_shm       (AyyiShmCallbackData*);


int main (int argc, char* argv[])
{
	printf("%s%s %s%60s\n", ayyi_green_r, PACKAGE_NAME, VERSION, white);

	set_log_handlers();

#ifdef HAVE_LASH
	lash_args_t* lash_args = lash_extract_args(&argc, &argv);
	gboolean lash = start_lash(lash_args);
#endif

	gtk_init(&argc, &argv);

	app_new();
	app_set_state(APP_STATE_STARTED);

#ifdef USE_OPENGL
	gtk_gl_init(&argc, &argv);
	if (!gdk_gl_query_extension()) perr("OpenGL extension not supported");
#endif

#ifdef USE_CLUTTER
	if (gtk_clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS) g_error ("Unable to initialize GtkClutter");
#endif

#define LOAD_LOCAL_GTKRC
#ifdef LOAD_LOCAL_GTKRC
	GBytes* gtkrc = g_resources_lookup_data ("/seismix/resources/gtkrc", 0, NULL);
	gtk_rc_parse_string (g_bytes_get_data(gtkrc, 0));
	g_bytes_unref (gtkrc);
#endif

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
	switch (opt) {
		case 'd':
			printf("using debug level: %s\n", optarg);
			int d = atoi(optarg);
			if (d < 0 || d > 5) { pwarn ("bad arg. debug=%i", d); } else _debug_ = d;
			break;
		/*
		case 'o':
			printf("offline mode. Currently not fully implemented. Do not use.\n");
			ayyi.offline = TRUE;
			break;
		*/
		case 'h':
			printf(usage, argv[0]);
			exit(0);
		break;
			case '?':
			printf("unknown option: %c\n", optopt);
			break;
		}
	}
	for (; optind < argc; optind++) printf("unexpected argument: %s\n", argv[optind]);

	dbg(2, "debug_level=%i", _debug_);

	create_cursors();

	// check display bitdepth
	GdkVisual* vis = gdk_colormap_get_visual(gdk_colormap_get_system());
	if (vis->depth < 1) {
		perr ("bad colour depth %i", vis->depth);
		return EXIT_FAILURE;
	}

	config_load0();

	icons_init();

#ifdef HAVE_LASH
	if (lash) {
		GError* error = NULL;
		if (!g_thread_create(lash_thread_func, NULL, FALSE, &error)) {
			perr ("error creating lash thread: %s", error->message);
			g_error_free(error);
		}
	}
#endif

#ifdef USE_SEED
	ayyi_seed_init(&argc, &argv);
#endif

	am_init(NULL);
#ifdef USE_LV2
	lv2_init();
#endif
	shortcuts__init();
	global_accels_init();
	windows_init();

	statusbar__enable_logging();
	if (_debug_) ayyi.log.to_stdout = true;

	gboolean on_start (gpointer user_data)
	{
		AyyiService* song = known_services[AYYI_SERVICE_AYYID1];

		gboolean open_log_window ()
		{
			if (app->state == APP_STATE_STARTED) {
				log_print(0, "waiting for server...");
				if (!LOG_FIRST) log_win__new(NULL, NULL, GTK_ORIENTATION_HORIZONTAL);
			}
			return G_SOURCE_REMOVE;
		}
		g_timeout_add(500, open_log_window, NULL);

		void on_connected (const GError* error, gpointer user_data)
		{
			if (error) {
				g_print("%s", error->message);
				log_print(LOG_FAIL, "server connection");
				exit(1);
			} else {
				log_print(LOG_OK, "server connection");
				am_get_shm_(ayyi.services, app_on_shm, NULL);
			}

			HandlerData* c = user_data;
			call((AyyiCallback)c->callback, c->user_data);
			g_free(c);

			ayyi_client_load_plugins();

			song_init(); //too late!
			part_manager__init();
			transport__init();

			config_load();

			dbg(2, "opening main window...");

			//if(config_log_window_is_open()) log_win_new(NULL, GTK_ORIENTATION_HORIZONTAL);

			if (app->state == APP_STATE_ENGINE_OK) {
				// case where engine was done quickly, and songload had to be delayed...
				song_unload();
				song_load();
			}

			run_plugins();
		}

		am_connect_messaging(song,
			on_connected,
			AYYI_NEW(HandlerData,
				.callback = (AyyiHandler)NULL,
				.user_data = user_data
			),
			NULL
		);

		return G_SOURCE_REMOVE;
	}

	gboolean focus_hook (GSignalInvocationHint* ihint, guint n_param_values, const GValue* param_values, gpointer data)
	{
		// callback when _any_ widget is focussed.

		GObject* object = g_value_get_object (param_values + 0);
		if (GTK_IS_WIDGET(object)) {
			if (GTK_IS_WINDOW(object)) {
				if (gtk_window_get_type_hint((GtkWindow*)object) != GDK_WINDOW_TYPE_HINT_POPUP_MENU) {
					AyyiWindow* window = (AyyiWindow*)object;
					if (window->focussed_panel)
						ayyi_panel_on_focus(window->focussed_panel);
				}
			} else {
				dbg(2, "signal=%s object=%s %s", g_signal_name (ihint->signal_id), G_OBJECT_TYPE_NAME(object), GTK_WIDGET(object)->name);
				AyyiPanel* panel;
				if ((panel = windows__get_panel_from_widget((GtkWidget*)object))) {
					ayyi_panel_on_focus(panel);
				}
			}
			if (app->active_panel) dbg(2, "focussed_panel=%s", G_OBJECT_TYPE_NAME(app->active_panel));
		}
		return true;
	}
	g_signal_add_emission_hook(g_signal_lookup("focus-in-event", GTK_TYPE_WIDGET), 0, focus_hook, NULL, NULL);

	g_timeout_add(10, on_start, NULL);

	gtk_main();

	exit(0);
}


static void
app_on_shm (AyyiShmCallbackData* data)
{
	void app_on_shm_complete ()
	{
		// shm is now attached successfully.

		if (((AyyiSongService*)ayyi.service)->song->version != AYYI_SHM_VERSION) {
			pwarn ("wrong shm version: %i - should be %i", ((AyyiSongService*)ayyi.service)->song->version, AYYI_SHM_VERSION);
			app_set_state(APP_STATE_ABORT);
			return;
		}

		log_print(LOG_OK, "connected to server");

		app_set_state(APP_STATE_ENGINE_OK);

		AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_ARRANGE);
		if (k->instances) { //TODO remove this requirement for the arrange window to be done before loading a song.
			song_unload();
			if (!song_load()){ log_print(LOG_FAIL, "song failed to load."); }
		}
	}

	//one or more shm segments have been added.
	if (ayyi.got_shm) app_on_shm_complete();

	if (!data->shm_seg) {
		windows__ensure(AYYI_TYPE_LOG_WIN);
	}
}
