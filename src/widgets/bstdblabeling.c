/* BEAST - Bedevilled Audio System
 * Copyright (C) 2004 Tim Janik
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


G_DEFINE_TYPE (BstDBLabeling, bst_db_labeling, GTK_TYPE_WIDGET);

static void
bst_db_labeling_init (BstDBLabeling *self)
{
  GtkWidget *widget = GTK_WIDGET (self);
  GTK_WIDGET_SET_FLAGS (self, GTK_NO_WINDOW);
  gtk_widget_show (widget);

  self->dbsetup = bst_db_setup_copy (bst_db_setup_get_default ());
  self->border = DEFAULT_BORDER;
  self->orientation = GTK_ORIENTATION_VERTICAL;
  self->justify = GTK_JUSTIFY_CENTER;
}

static void
bst_db_labeling_destroy (GtkObject *object)
{
  GTK_OBJECT_CLASS (bst_db_labeling_parent_class)->destroy (object);
}

static void
bst_db_labeling_finalize (GObject *object)
{
  BstDBLabeling *self = BST_DB_LABELING (object);
  bst_db_setup_unref (self->dbsetup);
  G_OBJECT_CLASS (bst_db_labeling_parent_class)->finalize (object);
}

static PangoLayout*
bst_db_labeling_create_layout (BstDBLabeling *self,
                               double         dB)
{
  gchar *buffer = g_strdup_printf ("%u", (int) (0.5 + ABS (dB)));
  PangoLayout *layout = gtk_widget_create_pango_layout (GTK_WIDGET (self), buffer);
  g_free (buffer);
  return layout;
}

static void
bst_db_labeling_size_request (GtkWidget      *widget,
                              GtkRequisition *requisition)
{
  BstDBLabeling *self = BST_DB_LABELING (widget);
  const gboolean vertical = self->orientation == GTK_ORIENTATION_VERTICAL;
  /* always request size based on digit dimensions, regardles of whether
   * numeric values are to be drawn.
   */
  PangoRectangle irect = { 0, }, lrect = { 0 };
  gint i, breadth = 0, length = 0;
  PangoLayout *layout;
  
  for (i = 0; i < self->dbsetup->spline->n_segs; i++)
    {
      double v = self->dbsetup->spline->segs[i].x;
      layout = bst_db_labeling_create_layout (self, v);
      pango_layout_get_pixel_extents (layout, &irect, &lrect);
      if (vertical)
        {
          breadth = MAX (breadth, lrect.width);
          length += lrect.height + 3;
        }
      else
        {
          length += lrect.width + 3 + NUMBER_HPADDING;
          breadth = MAX (breadth, lrect.height);
        }
      g_object_unref (layout);
    }
  if (!self->draw_values)
    {
      /* font width */
      PangoRectangle irect = { 0, }, lrect = { 0 };
      PangoLayout *layout = gtk_widget_create_pango_layout (GTK_WIDGET (self), "9");
      pango_layout_get_pixel_extents (layout, &irect, &lrect);
      g_object_unref (layout);
      guint dash_length = lrect.width;

      if (self->justify == GTK_JUSTIFY_CENTER)
        breadth = 2 * dash_length | 1;  /* always request odd size */
      else
        breadth = dash_length;
    }
  if (vertical)
    {
      requisition->width = breadth;
      requisition->height = length;
    }
  else
    {
      requisition->width = length;
      requisition->height = breadth;
    }
}

static void
bst_db_labeling_size_allocate (GtkWidget     *widget,
                               GtkAllocation *allocation)
{
  BstDBLabeling *self = BST_DB_LABELING (widget);

  const gboolean vertical = self->orientation == GTK_ORIENTATION_VERTICAL;
  widget->allocation = *allocation;
  guint thickness = vertical ? YTHICKNESS (self) : XTHICKNESS (self);
  db_setup_size_allocate (self->dbsetup, thickness, self->border, vertical ? allocation->height : allocation->width, vertical);
}

typedef enum {
  DRAW_SKIP,
  DRAW_ETCHED,
  DRAW_MAJOR,
  DRAW_MINOR,
  DRAW_MICRO,
  DRAW_SUBRO,
  DRAW_NUM
} DrawType;

static void
db_labeling_draw_lateral_line (BstDBLabeling   *self,
                               GdkGC           *gc,
                               gint             x,
                               gint             y,
                               gint             pos,
                               gint             breadth,
                               double           indent)
{
  GtkWidget *widget = GTK_WIDGET (self);
  GdkWindow *drawable = widget->window;
  g_return_if_fail (indent <= 0.5);
  gint pixindent = indent * breadth;
  gint breadth_reduz = 2 * pixindent;
  switch (self->justify)
    {
    case GTK_JUSTIFY_FILL:
      pixindent = 0;
      breadth_reduz = 0;
      break;
    case GTK_JUSTIFY_CENTER:
      pixindent = indent * breadth;
      breadth_reduz = 2 * pixindent;
      break;
    case GTK_JUSTIFY_LEFT:
      breadth_reduz = 2 * pixindent;
      pixindent = 0;
      break;
    case GTK_JUSTIFY_RIGHT:
      breadth_reduz = 2 * pixindent;
      pixindent = breadth_reduz;
      break;
    }
  if (self->orientation == GTK_ORIENTATION_VERTICAL)
    gdk_draw_hline (drawable, gc, x + pixindent, y + pos, breadth - breadth_reduz);
  else  /* horizontal */
    gdk_draw_vline (drawable, gc, x + pos, y + pixindent, breadth - breadth_reduz);
}

static void
db_labeling_draw_vline (BstDBLabeling *self,
                        GdkGC         *gc,
                        gint           x,
                        gint           y,
                        gint           pos,
                        gint           breadth,
                        gint           indent)
{
  GtkWidget *widget = GTK_WIDGET (self);
  GdkWindow *drawable = widget->window;
  if (self->orientation == GTK_ORIENTATION_VERTICAL)
    gdk_draw_vline (drawable, gc, x + indent, y + pos, breadth);
  else  /* horizontal */
    gdk_draw_hline (drawable, gc, x + pos, y + indent, breadth);
}

static void
bst_db_labeling_draw_value (BstDBLabeling *self,
                            GdkRectangle  *expose_area,
                            GdkRectangle  *canvas,
                            double         dB,
                            GdkRectangle  *cover1,
                            GdkRectangle  *cover2,
                            GdkRectangle  *consumed,
                            DrawType       dtype)
{
  GtkWidget *widget = GTK_WIDGET (self);
  GtkAllocation *allocation = ALLOCATION (self);
  const gboolean vertical = self->orientation == GTK_ORIENTATION_VERTICAL;
  BstDBSetup *dbsetup = self->dbsetup;
  gint pos = bst_db_setup_get_pixel (dbsetup, dB);
  if (dtype == DRAW_NUM)
    {
      PangoLayout *layout = bst_db_labeling_create_layout (self, dB);
      PangoRectangle irect = { 0, }, lrect = { 0 };
      pango_layout_get_pixel_extents (layout, &irect, &lrect);
      gint llength = vertical ? lrect.height : lrect.width;
      pos -= llength / 2;               /* center number around position */
      consumed->width = lrect.width;
      consumed->height = lrect.height;
      if (vertical)
        {
          consumed->x = canvas->x + (canvas->width - lrect.width) / 2;
          consumed->y = canvas->y + pos;
          /* keep maxdb, 0db, mindb inside allocation */
          if (dB == dbsetup->mindb || dB == 0 || dB == dbsetup->maxdb)
            {
              if (consumed->y + consumed->height > allocation->y + allocation->height)
                consumed->y = allocation->y + allocation->height - consumed->height;
              consumed->y = MAX (consumed->y, allocation->y);
            }
        }
      else
        {
          consumed->x = canvas->x + pos;
          consumed->y = canvas->y + (canvas->height - lrect.height) / 2;
          /* keep maxdb, 0db, mindb inside allocation */
          if (dB == dbsetup->mindb || dB == 0 || dB == dbsetup->maxdb)
            {
              if (consumed->x + consumed->width > allocation->x + allocation->width)
                consumed->x = allocation->x + allocation->width - consumed->width;
              consumed->x = MAX (consumed->x, allocation->x);
            }
        }
      GdkRectangle dummy;
      if (!gdk_rectangle_intersect (cover1, consumed, &dummy) &&
          !gdk_rectangle_intersect (cover2, consumed, &dummy) &&
          consumed->x >= allocation->x && consumed->x + consumed->width <= allocation->x + allocation->width &&
          consumed->y >= allocation->y && consumed->y + consumed->height <= allocation->y + allocation->height)
        {
          gtk_paint_layout (widget->style, widget->window,
                            GTK_WIDGET_IS_SENSITIVE (self) ? GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE,
                            FALSE, expose_area, widget, NULL, consumed->x, consumed->y, layout);
          if (!vertical)
            {
              consumed->x -= NUMBER_HPADDING / 2;
              consumed->width += NUMBER_HPADDING;
            }
        }
      else
        consumed->width = consumed->height = 0;
      g_object_unref (layout);
    }
  else if (dtype != DRAW_SKIP)
    {
      // ensure the mindb line is not drawn outside the box
      pos = MIN(pos, allocation->height - 3);

      gint cbreadth = vertical ? canvas->width : canvas->height;
      gint clength = vertical ? canvas->height : canvas->width;
      gboolean draw_minors = clength > 8 * dbsetup->spline->n_segs;
      gboolean draw_micros = clength > 16 * dbsetup->spline->n_segs;
      gboolean draw_subros = clength > 24 * dbsetup->spline->n_segs;
      GdkGC *light_gc = widget->style->light_gc[widget->state];
      GdkGC *line_gc = widget->style->fg_gc[widget->state];
      GdkGC *dark_gc = widget->style->dark_gc[widget->state];
      GdkGC *minor_gc = draw_subros ? line_gc : dark_gc;
      double minor_indent = 0.10;
      double micro_indent = 0.18;
      double subro_indent = 0.30;
      /* db_labeling_draw_lateral_line (self, gc, x, y, pos, breadth, indent) draws a horizontal line if orientation == VERTICAL */
      if (dtype == DRAW_ETCHED)
        db_labeling_draw_lateral_line (self, dark_gc, canvas->x, canvas->y, pos - 1, cbreadth, 0);
      if (dtype == DRAW_ETCHED ||
          dtype == DRAW_MAJOR)
        db_labeling_draw_lateral_line (self, line_gc, canvas->x, canvas->y, pos, cbreadth, 0);
      else if (dtype == DRAW_SUBRO && draw_subros)
        db_labeling_draw_lateral_line (self, dark_gc, canvas->x, canvas->y, pos, cbreadth, subro_indent);
      else if (dtype == DRAW_MICRO && draw_micros)
        db_labeling_draw_lateral_line (self, dark_gc, canvas->x, canvas->y, pos, cbreadth, micro_indent);
      else if (dtype == DRAW_MINOR && draw_minors)
        db_labeling_draw_lateral_line (self, minor_gc, canvas->x, canvas->y, pos, cbreadth, minor_indent);
      if (dtype == DRAW_ETCHED)
        db_labeling_draw_lateral_line (self, light_gc, canvas->x, canvas->y, pos + 1, cbreadth, 0);
    }
}

static gboolean
bst_db_labeling_expose (GtkWidget      *widget,
                        GdkEventExpose *event)
{
  BstDBLabeling *self = BST_DB_LABELING (widget);
  const gboolean vertical = self->orientation == GTK_ORIENTATION_VERTICAL;
  GdkWindow *drawable = event->window;
  GtkAllocation *allocation = ALLOCATION (self);
  BstDBSetup *dbsetup = self->dbsetup;
  gint breadth = vertical ? allocation->width : allocation->height;
  if (drawable != widget->window)
    return FALSE;

  if (0)
    {
      gdk_draw_line (widget->window, widget->style->black_gc, widget->allocation.x, widget->allocation.y,
                     widget->allocation.x + widget->allocation.width-1, widget->allocation.y + widget->allocation.height-1);
      gdk_draw_line (widget->window, widget->style->black_gc, widget->allocation.x + widget->allocation.width-1, widget->allocation.y,
                     widget->allocation.x, widget->allocation.y + widget->allocation.height-1);
    }
  
  GdkGC *dark_gc = widget->style->light_gc[widget->state];
  GdkGC *line_gc = widget->style->fg_gc[widget->state];
  GdkGC *light_gc = widget->style->dark_gc[widget->state];
  gint longitudinal_pos = 0, draw_longitudinal = !self->draw_values;
  switch (self->justify)
    {
    case GTK_JUSTIFY_FILL:
      draw_longitudinal = FALSE;
      break;
    case GTK_JUSTIFY_CENTER:
      longitudinal_pos = breadth / 2;
      break;
    case GTK_JUSTIFY_LEFT:
      // longitudinal_pos = 0 + 1;
      draw_longitudinal = FALSE;
      break;
    case GTK_JUSTIFY_RIGHT:
      // longitudinal_pos = breadth - 1 - 1;
      draw_longitudinal = FALSE;
      break;
    }
  /* vline shades */
  if (draw_longitudinal && breadth >= 3)
    {
      /* db_labeling_draw_vline (self, gc, x, y, pos, breadth, indent) draws a vertical line if orientation == VERTICAL */
      db_labeling_draw_vline (self, light_gc, allocation->x, allocation->y, dbsetup->offset, dbsetup->length, longitudinal_pos - 1);
      db_labeling_draw_vline (self,  dark_gc, allocation->x, allocation->y, dbsetup->offset, dbsetup->length, longitudinal_pos + 1);
    }
  DrawType draw_etched = self->draw_values ? DRAW_NUM : DRAW_ETCHED;
  DrawType draw_major = self->draw_values ? DRAW_NUM : DRAW_MAJOR;
  DrawType draw_minor = self->draw_values ? DRAW_SKIP : DRAW_MINOR;
  DrawType draw_micro = self->draw_values ? DRAW_SKIP : DRAW_MICRO;
  DrawType draw_subro = self->draw_values ? DRAW_SKIP : DRAW_SUBRO;
  gint i;
  GdkRectangle zero_consumed = { 0, }, consumed = { 0, }, last_consumed = { 0, };
  GdkRectangle canvas = *allocation;
  if (vertical)
    {
      // canvas.y += dbsetup->offset;
      canvas.height = dbsetup->length;
    }
  else
    {
      // canvas.x += dbsetup->offset;
      canvas.width = dbsetup->length;
    }
  /* draw zero */
  bst_db_labeling_draw_value (self, &event->area, &canvas, 0, &consumed, &consumed, &zero_consumed, draw_etched);
  /* draw upper half */
  if (dbsetup->maxdb > 0)
    {
      /* draw max */
      GdkRectangle max_consumed = { 0, };
      bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->maxdb, &zero_consumed, &zero_consumed, &max_consumed, draw_major);
      consumed = zero_consumed;
      /* draw zero..max */
      for (i = dbsetup->zero_index + 1; i < dbsetup->spline->n_segs && dbsetup->spline->segs[i].x < dbsetup->maxdb; i++)
        {
          if (consumed.width && consumed.height)
            last_consumed = consumed;
          bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->spline->segs[i].x, &max_consumed, &last_consumed, &consumed, draw_major);
        }
      /* draw minors */
      for (i = dbsetup->zero_index; i < dbsetup->spline->n_segs - 1 && dbsetup->spline->segs[i].x < dbsetup->maxdb; i++)
        {
          double db1 = dbsetup->spline->segs[i].x, db2 = dbsetup->spline->segs[i + 1].x;
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.125,
                                      &max_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.375,
                                      &max_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.625,
                                      &max_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.875,
                                      &max_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.25,
                                      &max_consumed, &zero_consumed, &consumed, draw_micro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.75,
                                      &max_consumed, &zero_consumed, &consumed, draw_micro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, (db1 + db2) * 0.5,
                                      &max_consumed, &zero_consumed, &consumed, draw_minor);
        }
      /* redraw max */
      bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->maxdb, &zero_consumed, &zero_consumed, &max_consumed, draw_major);
    }
  /* draw lower half */
  if (dbsetup->mindb < 0)
    {
      /* draw min */
      GdkRectangle min_consumed = { 0, };
      bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->mindb, &zero_consumed, &zero_consumed, &min_consumed, draw_etched);
      consumed = zero_consumed;
      /* draw min..zero */
      for (i = dbsetup->zero_index - 1; i >= 0 && dbsetup->spline->segs[i].x > dbsetup->mindb; i--)
        {
          if (consumed.width && consumed.height)
            last_consumed = consumed;
          bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->spline->segs[i].x, &min_consumed, &last_consumed, &consumed, draw_major);
        }
      /* draw minors */
      for (i = 0; i < dbsetup->zero_index; i++)
        {
          double db1 = dbsetup->spline->segs[i].x, db2 = dbsetup->spline->segs[i + 1].x;
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.125,
                                      &min_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.375,
                                      &min_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.625,
                                      &min_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.875,
                                      &min_consumed, &zero_consumed, &consumed, draw_subro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.25,
                                      &min_consumed, &zero_consumed, &consumed, draw_micro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, db1 + (db2 - db1) * 0.75,
                                      &min_consumed, &zero_consumed, &consumed, draw_micro);
          bst_db_labeling_draw_value (self, &event->area, &canvas, (db1 + db2) * 0.5,
                                      &min_consumed, &zero_consumed, &consumed, draw_minor);
        }
      /* redraw min */
      bst_db_labeling_draw_value (self, &event->area, &canvas, dbsetup->mindb, &zero_consumed, &zero_consumed, &min_consumed, draw_etched);
    }
  /* redraw zero */
  consumed.width = consumed.height = 0;
  bst_db_labeling_draw_value (self, &event->area, &canvas, 0, &consumed, &consumed, &zero_consumed, draw_etched);
  /* vline bar */
  if (draw_longitudinal)
    db_labeling_draw_vline (self, line_gc, allocation->x, allocation->y, dbsetup->offset, dbsetup->length, longitudinal_pos);
  
  return FALSE;
}

void
bst_db_labeling_setup (BstDBLabeling      *self,
                       BstDBSetup     *db_setup)
{
  bst_db_setup_unref (self->dbsetup);
  self->dbsetup = bst_db_setup_copy (db_setup);
  gtk_widget_queue_resize (GTK_WIDGET (self));
}

void
bst_db_labeling_set_border (BstDBLabeling  *self,
                            guint           border)
{
  if (self->border != border)
    {
      self->border = border;
      gtk_widget_queue_resize (GTK_WIDGET (self));
    }
}

static void
bst_db_labeling_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  BstDBLabeling *self = BST_DB_LABELING (object);
  GtkWidget *widget = GTK_WIDGET (self);
  switch (prop_id)
    {
    case PROP_DRAW_VALUES:
      self->draw_values = g_value_get_boolean (value);
      gtk_widget_queue_resize (widget);
      break;
    case PROP_ORIENTATION:
      self->orientation = g_value_get_enum (value);
      gtk_widget_queue_resize (widget);
      break;
    case PROP_JUSTIFY:
      self->justify = g_value_get_enum (value);
      gtk_widget_queue_resize (widget);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
bst_db_labeling_get_property (GObject     *object,
                              guint        prop_id,
                              GValue      *value,
                              GParamSpec  *pspec)
{
  BstDBLabeling *self = BST_DB_LABELING (object);
  switch (prop_id)
    {
    case PROP_DRAW_VALUES:
      g_value_set_boolean (value, self->draw_values);
      break;
    case PROP_ORIENTATION:
      g_value_set_enum (value, self->orientation);
      break;
    case PROP_JUSTIFY:
      g_value_set_enum (value, self->justify);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
bst_db_labeling_class_init (BstDBLabelingClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  gobject_class->set_property = bst_db_labeling_set_property;
  gobject_class->get_property = bst_db_labeling_get_property;
  gobject_class->finalize = bst_db_labeling_finalize;
  
  object_class->destroy = bst_db_labeling_destroy;
  
  widget_class->size_request = bst_db_labeling_size_request;
  widget_class->size_allocate = bst_db_labeling_size_allocate;
  widget_class->expose_event = bst_db_labeling_expose;
  
  g_object_class_install_property (gobject_class, PROP_DRAW_VALUES,
                                   g_param_spec_boolean ("draw-values", "Draw Values", "Adjust whether to draw dB values instead of lines",
                                                         FALSE, G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, PROP_ORIENTATION,
                                   g_param_spec_enum ("orientation", "Orientation", "Choose horizontal or vertical orientation",
                                                      GTK_TYPE_ORIENTATION, GTK_ORIENTATION_VERTICAL, G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, PROP_JUSTIFY,
                                   g_param_spec_enum ("justify", "Justify", "Adjust relative alignment of the values or bars to be drawn",
                                                      GTK_TYPE_JUSTIFICATION, GTK_JUSTIFY_CENTER, G_PARAM_READWRITE));
}

