/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#undef BACKGROUND_2

enum {
    TEXTURE_BG = 0,
    TEXTURE_MAX,
};


typedef struct {
    AGlActor    actor;
} BackgroundLayer;

static GLuint bg_textures[TEXTURE_MAX + 1/* TODO */] = {0,};

#ifdef BG_TEXTURE
static void
arr_create_bg_texture (AGlActor* actor)
{
	if (bg_textures[TEXTURE_BG]) return;

#ifdef BACKGROUND_2
	if (!pattern_shader.shader.program) {
		agl_create_program(&pattern_shader.shader);
	}
#endif

	glGenTextures(TEXTURE_MAX, bg_textures);
#ifdef DEBUG
	if (glGetError() != GL_NO_ERROR) { perr ("failed to generate textures."); return; }
#endif

	{
		// create an alpha-map gradient for use as background

		int width = 256;
		int height = 256;
		char* pbuf = g_new0(char, width * height);
		for (int y=0;y<height;y++) {
			for (int x=0;x<width;x++) {
				*(pbuf + y * width + x) = ((x+y) * 0xff) / (width * 2);
			}
		}

		int pixel_format = GL_ALPHA;
		glBindTexture  (GL_TEXTURE_2D, bg_textures[TEXTURE_BG]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D   (GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, pixel_format, GL_UNSIGNED_BYTE, pbuf);
		gl_warn("bg texture bind");

		g_free(pbuf);
	}

	create_textures();
}
#endif


static AGlActor*
arr_background (GtkWidget* panel)
{
	bool background_paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

	#ifdef BACKGROUND_2
		// procedural background texture.

		agl_use_program((AGlShader*)&pattern_shader);

		glBegin(GL_QUADS);
		double top = 0.0;
		double bot = c->image->allocation.height;
		double x1 = 0.0;
		double x2 = c->image->allocation.width - RRf;
		glVertex2d(x1, top);
		glVertex2d(x2, top);
		glVertex2d(x2, bot);
		glVertex2d(x1, bot);
		glEnd();
	#else
		agl->shaders.alphamap->uniform.fg_colour = actor->colour;
		agl_use_program((AGlShader*)agl->shaders.alphamap);
		agl_textured_rect(bg_textures[TEXTURE_BG], actor->region.x1, actor->region.y1, actor->region.x2 - RRf, actor->region.y2, NULL);

		gl_warn("gl error");
	#endif // BACKGROUND_2

		// highlight the selected track
		{
			SET_PLAIN_COLOUR (agl->shaders.plain, 0xffffff08);
			agl_use_program (agl->shaders.plain);

			for (GList* s = arrange->tracks.selection;s;s=s->next) {
				ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, (AMTrack*)s->data);

				agl_rect_((AGlRect){
					.x = 0.0,
					.y = c->actors[ACTOR_TYPE_TC]->region.y1 + atr->y * c->zoom->value.pt.y + c->actors[ACTOR_TYPE_MAIN]->scrollable.y1,
					.w = c->image->allocation.width,
					.h = atr->height * c->zoom->value.pt.y,
				});
			}
		}
		return true;
	}

	void background_set_size (AGlActor* a)
	{
		a->region.x2 = ((AGlActor*)a->root)->region.x2;
		a->region.y2 = ((AGlActor*)a->root)->region.y2;
	}

	return (AGlActor*)agl_actor__new(BackgroundLayer,
		.actor = {
			.name = g_strdup("Background"),
			.program = (AGlShader*)agl->shaders.alphamap,
#ifdef BG_TEXTURE
			.init = arr_create_bg_texture,
#endif
			.paint = background_paint,
			.set_size = background_set_size,
			.colour = 0xffffff40
		},
	);
}
