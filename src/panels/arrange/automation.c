/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "model/track_list.h"
#include "arrange.h"
#ifdef USE_GNOMECANVAS
#include "gnome/automation.h"
#endif

#ifdef USE_OPENGL
#include "gl_canvas.h"
#endif


void
automation_setup (ArrTrk* atr)
{
	if (atr->track->bezier.vol->len || atr->track->bezier.pan->len) {
		if (!atr->auto_paths) {
			atr->auto_paths = g_ptr_array_sized_new(AUTO_MAX);
		} else {
			pwarn("auto_paths unexpectedly already created");
		}

		AMCurve** curves = (AMCurve**)&atr->track->bezier;

		if (atr->track->bezier.vol->len) {
			if (atr->auto_paths->len <= VOL)
				g_ptr_array_set_size(atr->auto_paths, VOL + 1);
			g_return_if_fail(!atr->auto_paths->pdata[VOL]);
			atr->auto_paths->pdata[VOL] = AYYI_NEW(ArrAutoPath, .curve = curves[VOL]);
		}

		if (atr->track->bezier.pan->len) {
			if (atr->auto_paths->len <= PAN)
				g_ptr_array_set_size(atr->auto_paths, PAN + 1);
			g_return_if_fail(!atr->auto_paths->pdata[PAN]);
			atr->auto_paths->pdata[PAN] = AYYI_NEW(ArrAutoPath, .curve = curves[PAN]);
		}
	}
}


void
automation_on_song_load (Arrange* arrange)
{
	ArrTrk* atr = NULL;
	while ((atr = track_list__next_visible(arrange->priv->tracks, atr ? atr->track : NULL))) {
		automation_setup(atr);
	}

#ifdef USE_GNOMECANVAS
	AMTrack* trk;
	for (int t=0; (trk = song->tracks->track[t]); t++) { 
		GList* l = trk->controls;
		for (;l;l=l->next) {
			Curve* curve = l->data;
			g_automation_on_new_curve(arrange, curve);
		}
	}
#endif
}


void
automation_free (ArrTrk* atr)
{
	if (atr->auto_paths) {
		g_ptr_array_foreach(atr->auto_paths, (GFunc)g_free, NULL);
		g_ptr_array_free(atr->auto_paths, true);
		atr->auto_paths = NULL;
	}
}


/*
 *  Bring volume automation curve to the front. If it is empty, add a start and end point.
 */
void
automation_view_vol (ArrTrk* atr)
{
	Arrange* arrange = atr->arrange;

	if (atr->auto_paths->len) {
		AUTO_PATH(VOL)->hidden = false;
	}

	Curve* curve = atr->track->bezier.vol;
	if (!curve->path) {
		am_automation_make_empty(curve);
#ifdef USE_GNOMECANVAS
		if (CANVAS_IS_GNOME) {
			g_automation_curve_draw(arrange, curve);
		}
#endif
	}

	arrange->canvas->on_auto_ctl_show(arrange, atr, AUTO_PATH(VOL));
}


void
automation_view_pan (ArrTrk* atr)
{
#if defined(USE_GNOMECANVAS) || defined(USE_OPENGL)
	Arrange* arrange = atr->arrange;
#endif
	AMCurve* curve = atr->track->bezier.pan;

	if (!curve->path) {
		dbg(0, "not yet created...");
		am_automation_make_empty(curve);

		automation_setup(atr);

#ifdef USE_GNOMECANVAS
		if (CANVAS_IS_GNOME) {
			g_automation_curve_draw(arrange, curve);
		}
#endif
	}

	((ArrAutoPath*)atr->auto_paths->pdata[PAN])->hidden = false;

#ifdef USE_GNOMECANVAS
	if (CANVAS_IS_GNOME) {
		g_automation_view_pan(atr);
	}
#endif

	arrange->canvas->on_auto_ctl_show(arrange, atr, AUTO_PATH(PAN));
}


ArrAutoPath*
automation_current_path (ArrTrk* atr)
{
	for (AyyiAutoType i=0;i<atr->auto_paths->len;i++) {
		ArrAutoPath* aap = g_ptr_array_index(atr->auto_paths, i);
		if (aap->curve->auto_type == atr->arrange->automation.sel.type) return aap;
	}

	return NULL;
}
