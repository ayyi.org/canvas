//----------------------------------------------------------------------------
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------
#ifndef __event_h__
#define __event_h__

#include <glib.h>

/*
const unsigned char  EVENT_STATUS_BIT       = 0x80;
const unsigned char  EVENT_NOTE_OFF         = 0x80;
const unsigned char  EVENT_NOTE_ON          = 0x90;
const unsigned char  EVENT_AFTERTOUCH       = 0xA0;
const unsigned char  EVENT_CONTROL_CHANGE   = 0xB0;
const unsigned char  EVENT_PROGRAM_CHANGE   = 0xC0;
const unsigned char  EVENT_CHANNEL_PRESSURE = 0xD0;
const unsigned char  EVENT_PITCH_WHEEL      = 0xE0;
const unsigned char  EVENT_CLEAR_CHAN_MASK  = 0xF0;
const unsigned char  EVENT_MIDI_CLOCK       = 0xF8;
const unsigned char  EVENT_SYSEX            = 0xF0;
const unsigned char  EVENT_SYSEX_END        = 0xF7;
*/
#define EVENT_STATUS_BIT        0x80
#define EVENT_NOTE_OFF          0x80
#define EVENT_NOTE_ON           0x90
#define EVENT_AFTERTOUCH        0xA0
#define EVENT_CONTROL_CHANGE    0xB0
#define EVENT_PROGRAM_CHANGE    0xC0
#define EVENT_CHANNEL_PRESSURE  0xD0
#define EVENT_PITCH_WHEEL       0xE0
#define EVENT_CLEAR_CHAN_MASK   0xF0
#define EVENT_MIDI_CLOCK        0xF8
#define EVENT_SYSEX             0xF0
#define EVENT_SYSEX_END         0xF7


//midi event struct based on seq24 event class:
struct _midi_event
{
    unsigned long timestamp;     // timestamp in ticks

    /*
	status byte without channel
    channel will be appended on bus
    high nibble = type of event
    low nibble = channel
    bit 7 is present in all status bytes
	*/
    unsigned char  status;

    unsigned char  data[2];     // data for event

    unsigned char* sysex;       // data for sysex

    struct _midi_event* linked; // used to link note ons and offs together
    gboolean       has_link;

    gboolean       selected;    // is this event selected in editing

    long           size;        // size of sysex message
};
typedef struct _midi_event midi_event;


unsigned char midi_event__get_note    (midi_event*);
gboolean      midi_event__is_note_on  (midi_event*);
gboolean      midi_event__is_note_off (midi_event*);
gboolean      event__is_linked  (midi_event*);


#endif //__event_h__
