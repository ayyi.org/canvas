/*
  This file is part of the Ayyi Project. http://www.ayyi.org
  copyright (C) 2004-2021 Tim Orford <tim@orford.org>

  Part canvas item.
  ----------------

  derived from polygon, but also includes functions copied from gnome-canvas-pixbuf.c.

*/

/*
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*  based on Polygon item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Author: Federico Mena <federico@nuclecu.unam.mx>
 *         Rusty Conover <rconover@bangtail.net>
 */

#include "global.h"
#include <math.h>
#include <sys/time.h>
#include "libart_lgpl/art_vpath.h"
#include <libart_lgpl/art_svp.h>
#include "libart_lgpl/art_svp_vpath.h"
#include "libart_lgpl/art_svp_vpath_stroke.h"
#include <libart_lgpl/art_bpath.h>
#include <libart_lgpl/art_vpath_bpath.h>
#include <libart_lgpl/art_vpath_dash.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libart_lgpl/art_svp_intersect.h>
#include <libart_lgpl/art_rgba.h>
#include <libgnomecanvas/libgnomecanvas.h>

#define __wf_private__ // libwaveform wants to hide blocking details, but we are also implementing a block based solution here.
#include "waveform/private.h"
#include "waveform/audio.h"
#include "waveform/pixbuf.h"
#include "model/pool_item.h"
#include "model/am_part.h"
#include "model/time.h"
#include "model/midi_part.h"
#include "model/track_list.h"
#include "windows.h"
#include "arrange.h"
#include "arrange/gnome_canvas_utils.h"
#include "arrange/part.h"
#include "song.h"
#include "support.h"
#include "style.h"
#include "arrange/track_control_midi.h"
#include "arrange/canvas_op.h"
#include "part_item.h"


#define NUM_STATIC_POINTS 256	/* Number of static points to use to avoid allocating arrays */

#define HAS_ALPHA_TRUE 1

#define CORNER_SIZE 0.0 //5.0

#define MAX_PART_WIDTH 128000
#define MAX_X_POS 128000
#define MAX_PIXBUF_WIDTH 1 << 14 // setting bigger than this seems to cause segfaults.
#define MIN_OVERVIEW_HEIGHT 2

#define PEAK_TILE_SIZE 256       // pixels per block

//we need 32bit pixel buffer for cairo, but widget crashes if we use this...
#undef BPP32
#define BITS_PER_SAMPLE 8

#define SHOW_CONTENTS part->arrange->view_options[SHOW_PART_CONTENTS].value

#define CAN_SHOW_FADES part->arrange->part_fades_show && hzoom(part->arrange) > 1.0
#undef SHOW_FADES //buggy!
#define SHOW_NOTE_SHADOW

extern SMConfig* config;
int display_depth = 0;

enum {
	PROP_0,
	PROP_POINTS,
	PROP_TRACK,
	PROP_GPART,
	PROP_ARRANGE,
	PROP_X,
	PROP_Y,
	PROP_LEFT,
	PROP_RIGHT,
	PROP_LABEL,
	PROP_SHOW_CONTENTS,
};

typedef struct _peak_tile
{
	int           index;
	gboolean      dirty;
} PeakTile;

/* Private part of the GnomeCanvasPixbuf structure */
typedef struct
{
	GdkPixbuf*       pixbuf;
	GdkPixmap*       label_pixmap;        // the complete rendered text.
	GnomeCanvasLine* fade_in;
	GnomeCanvasLine* fade_out;

	double           width;
	double           height;

	double           x;                   // translations
	double           y;

	guint         x_in_pixels : 1;
	guint         y_in_pixels : 1;

	guint         pixbuf_needs_update : 1; // Whether the pixbuf has changed

	GtkAnchorType anchor;

	int           old_width;              // used to detect if part dimensions have changed.
	int           old_height;
} PartPrivate;

typedef struct _rect { double x1, y1; double x2; double y2;} rect;


static void gnome_canvas_part_class_init       (GnomeCanvasPartClass*);
static void gnome_canvas_part_init             (GnomeCanvasPart*);
static void gnome_canvas_part_destroy          (GtkObject*);
static void gnome_canvas_part_set_property     (GObject*, guint param_id, const GValue*, GParamSpec*);
static void gnome_canvas_part_get_property     (GObject*, guint param_id, GValue*, GParamSpec*);
static void gnome_canvas_part_realize          (GnomeCanvasItem*);
static void gnome_canvas_part_render           (GnomeCanvasItem*, GnomeCanvasBuf*);
static void gnome_canvas_part_invalidate_pixbuf(GnomeCanvasPart*);
static void gnome_canvas_part_update_pixbuf    (GnomeCanvasItem*);
static void gnome_canvas_part_midi_draw_pixbuf (GnomeCanvasItem*);
static void gnome_canvas_part_update           (GnomeCanvasItem*, double *affine, ArtSVP *clip_path, int flags);
static void g_canvas_part_set_show_contents    (GnomeCanvasPart*, gboolean);
static int  g_canvas_part_get_min_n_tiers      (Arrange*);
static void part_try_set_peak_tile             (GnomeCanvasPart*, PeakTile*);
static void part_set_peak_tile                 (GnomeCanvasPart*, PeakTile*);
static int  gnome_canvas_part_get_peak_block_num(GnomeCanvasPart*, PeakTile*, int* block_count);
static void gnome_canvas_part_on_peakdata_available(GnomeCanvasPart*, int block_num);
static bool part_tile_is_visible               (GnomeCanvasItem*, int block_num);
static double
            arr_get_part_core_width_pix_zoomed (Arrange*, AMPart*);

static GnomeCanvasItemClass *parent_class;

static guint  fades_idle       = 0;
static GList* fade_redraw_list = NULL;

uint32_t midi_colours[3] = {0x00ffffff, 0xffff00ff, 0xff9900ff};


GType
gnome_canvas_part_get_type(void)
{
	static GType part_type;

	if(!part_type){
		static const GTypeInfo object_info = {
			sizeof (GnomeCanvasPartClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_canvas_part_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,                                       /* class_data */
			sizeof(GnomeCanvasPart),
			0,                                          /* n_preallocs */
			(GInstanceInitFunc)gnome_canvas_part_init,
			NULL                                        /* value_table */
		};

		part_type = g_type_register_static(GNOME_TYPE_CANVAS_SHAPE, "GnomeCanvasPart", &object_info, 0);
	}
	return part_type;
}

static void
gnome_canvas_part_class_init(GnomeCanvasPartClass *class)
{

	GObjectClass         *gobject_class = (GObjectClass *) class;
	GtkObjectClass       *object_class  = (GtkObjectClass *) class;
	GnomeCanvasItemClass *item_class    = (GnomeCanvasItemClass *) class;

	parent_class = g_type_class_peek_parent(class);

	gobject_class->set_property = gnome_canvas_part_set_property;
	gobject_class->get_property = gnome_canvas_part_get_property;

	g_object_class_install_property(gobject_class, PROP_POINTS,
                 g_param_spec_boxed("points", NULL, NULL, GNOME_TYPE_CANVAS_POINTS,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_TRACK,
                 g_param_spec_int("track", NULL, NULL, 0, G_MAXINT, 0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_GPART,
                 g_param_spec_pointer("gpart", NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_ARRANGE,
                 g_param_spec_pointer("arrange", NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_X,
                 g_param_spec_double("x", NULL, NULL, -10000.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_Y,
                 g_param_spec_double("y", NULL, NULL, -10000.0, 100000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_RIGHT,
                 g_param_spec_double("right", NULL, NULL, 0.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_LEFT,
                 g_param_spec_double("left", NULL, NULL, 0.0, 1000000.0, 0.0,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_LABEL,
                 g_param_spec_string("label", NULL, NULL, NULL,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property(gobject_class, PROP_SHOW_CONTENTS,
                 g_param_spec_boolean("show_contents", NULL, NULL, TRUE,
				   					 (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	object_class->destroy = gnome_canvas_part_destroy;

	item_class->realize= gnome_canvas_part_realize;
	item_class->render = gnome_canvas_part_render;
	item_class->update = gnome_canvas_part_update;

	class->edit_colour = 0xffffffff;

	GdkVisual* vis = gdk_colormap_get_visual(gdk_colormap_get_system());
	display_depth = vis->depth;
}


static void
gnome_canvas_part_init(GnomeCanvasPart* part)
{
	part->path_def              = NULL;
	part->gpart                 = NULL;
	part->transient_offset_left = 0.0;
	part->pts                   = gnome_canvas_points_new(5);
	part->tiles                 = NULL;
	part->min_height            = 16;
	part->arrange               = NULL;
	part->label_pixbuf          = NULL;

	PartPrivate* priv = part->private = g_new0(PartPrivate, 1);
	priv->pixbuf      = NULL;
	priv->fade_in     = NULL;
	priv->fade_out    = NULL;

	GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(part);
	shape->priv->outline_rgba = config->part_outline_colour;

	shape->priv->width = PART_OUTLINE_WIDTH;
	shape->priv->width_pixels = 1;
}


static void
gnome_canvas_part_destroy(GtkObject *object)
{
	g_return_if_fail (object);
	g_return_if_fail (GNOME_IS_CANVAS_PART (object));

	GnomeCanvasPart* part = GNOME_CANVAS_PART(object);
	PartPrivate* priv = part->private;

	/* remember, destroy can be run multiple times! */

	if(part->path_def) gnome_canvas_path_def_unref(part->path_def);
	part->path_def = NULL;

	if(part->pts) gnome_canvas_points_unref(part->pts);
	part->pts = NULL;

	if(priv->fade_in){
		fade_redraw_list = g_list_remove(fade_redraw_list, part);
		if(GTK_IS_OBJECT(priv->fade_in))
			gtk_object_destroy(GTK_OBJECT(priv->fade_in));
		priv->fade_in = NULL;
	}
	if(priv->fade_out){
		if(GTK_IS_OBJECT(priv->fade_out))
			gtk_object_destroy(GTK_OBJECT(priv->fade_out));
		priv->fade_out = NULL;
	}

	if(part->peakdata_ready_handler){
		g_signal_handler_disconnect((gpointer)part->gpart->pool_item, part->peakdata_ready_handler);
		part->peakdata_ready_handler = 0;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy) (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
pixbuf_transparent_corner(GdkPixbuf* pixbuf)
{
  //make the top left corner of the pixbuf transparent.

  g_return_if_fail(pixbuf);
  if(gdk_pixbuf_get_width(pixbuf) < 4) return;

  guchar* pixels = gdk_pixbuf_get_pixels   (pixbuf);
  int rowstride  = gdk_pixbuf_get_rowstride(pixbuf);
  int corner_width = CORNER_SIZE;

  int i,x;
  int row = 0;
  for(x=corner_width;x>=0;x--){
    for(i=0;i<x;i++) pixels[i*4 + 3 + row*rowstride] = 0;
    row++;
  }
}


double
get_min_left(GnomeCanvasPart *part)
{
  // Return the canvas position corresponding to the beginning of the Part's region.

  // Used during transient ops so dont cause the part to be synced to core.

  unsigned sample_start = part->gpart->region_start;

  // we need to find the canvas position for which the sample_num is zero.
  // -we know the pos for the part start, so lets subtract sample_start samples from that.
  GPos part_start;
  //FIXME change 2 fns below so they have the same name!!

  am_part__get_start_pos(part->gpart, &part_start); //core start is used here as a stable reference unaffected by any transient op.

  double left = arr_pos2px(part->arrange, &part_start) - arr_samples2px(part->arrange, sample_start);

  return left;
}


static double
get_max_right(GnomeCanvasPart *part)
{
  // Return the canvas position corresponding to the end of the Part's file source.

  AMPart* gpart = part->gpart;
  if(!PART_IS_AUDIO(gpart)) return MAX_PART_WIDTH;

  AMPoolItem* pool_item = part->gpart->pool_item;

  uint32_t len   = ((Waveform*)pool_item)->n_frames - part->gpart->region_start;
  uint32_t start = ayyi_pos2samples(&part->gpart->start);

  double right = arr_samples2px(part->arrange, start + len);
  dbg(2, "start=%u samplecount=%Lu region_start=%u len=%u", start, ((Waveform*)pool_item)->n_frames, part->gpart->region_start, len);

  return right;
}


static void
set_points(GnomeCanvasPart* part, GnomeCanvasPoints* points)
{
  if(_debug_ > 1) printf("GnomeCanvasPart::%s(): x=%.1f, %.1f, %.1f y=%.1f.\n", __func__, points->coords[6], points->coords[0], points->coords[2], points->coords[3]);
  PartPrivate* priv = part->private;
  gboolean dimensions_changed = FALSE;

  if(part->path_def) gnome_canvas_path_def_unref(part->path_def);

  if(!points){
    part->path_def = gnome_canvas_path_def_new();
    gnome_canvas_shape_set_path_def(GNOME_CANVAS_SHAPE(part), part->path_def);
    return;
  }

  // make points integer so the item aligns with the pixbuf
  int i;
  for(i=0;i<points->num_points;i++){
    points->coords[i] = floor(points->coords[i]);
  }

  // enforce minimum height
  float top    = points->coords[1];
  float bottom = points->coords[5];
  points->coords[5] = MAX(points->coords[5], top + part->min_height);
  points->coords[7] = MAX(points->coords[7], top + part->min_height);

  double left = points->coords[0]; //check.
  double right = points->coords[4];

  double old_width  = priv->old_width;
  double old_height = priv->old_height;

  // optimise the path def to the number of points
  part->path_def = gnome_canvas_path_def_new_sized(points->num_points+1);

#if 0
	/* No need for explicit duplicate, as closepaths does it for us (Lauris) */
	/* See if we need to duplicate the first point */
	duplicate = ((points->coords[0] != points->coords[2 * points->num_points - 2])
		     || (points->coords[1] != points->coords[2 * points->num_points - 1]));
#endif

  if(points->coords[0]>40000) errprintf("GnomeCanvasPart::%s(): max x position exceeded. (x=%.1f)\n", __func__, points->coords[0]);

  //--------

  // set the coords into the path_def

  gnome_canvas_path_def_moveto(part->path_def, points->coords[0], points->coords[1]);

  for(i=1;i<points->num_points;i++){
    if(points->coords[(i*2)] > MAX_PART_WIDTH){
      points->coords[(i*2)] = MAX_PART_WIDTH;//FIXME
      errprintf("GnomeCanvasPart::%s(): max width exceeded. (x2=%.1f %.1f)\n", __func__, points->coords[2], points->coords[4]);
    }
    gnome_canvas_path_def_lineto(part->path_def, points->coords[i * 2], points->coords[(i * 2) + 1]);
  }

  gnome_canvas_path_def_closepath(part->path_def);

  gnome_canvas_shape_set_path_def(GNOME_CANVAS_SHAPE(part), part->path_def);

  double new_width  = right - left;
  double new_height = bottom - top;
  if((fabs(new_width - old_width) > 1) || (fabs(new_height - old_height) > 1)) dimensions_changed  = TRUE;

  if(dimensions_changed){
    priv->old_width  = new_width;
    ((PartPrivate*)part->private)->old_height = new_height;
    gnome_canvas_part_invalidate_pixbuf(part);

#ifdef SHOW_FADES
    if(priv->fade_in){
      if(new_width > 12.0 && CAN_SHOW_FADES){
        gnome_canvas_item_show((GnomeCanvasItem*)priv->fade_in);
        gnome_canvas_item_show((GnomeCanvasItem*)priv->fade_out);
      } else {
        gnome_canvas_item_hide((GnomeCanvasItem*)priv->fade_in);
        gnome_canvas_item_hide((GnomeCanvasItem*)priv->fade_out);
      }
    }
#endif
  }
}


static void
gnomecanvas_part_set_track(GnomeCanvasPart* part, TrackNum track_num)//FIXME should probably be d
{
  // @param track_num - the display track number.

  part->transient_offset_left = 0;
  AMPart* gpart = part->gpart;
  g_return_if_fail(gpart);
  Arrange* arrange = part->arrange;
  TrackDispNum d = arr_t_to_d(arrange->priv->tracks, track_num);
  ArrTrk* arr_trk = track_list__track_by_index(arrange->priv->tracks, track_num);
  if(!arr_trk){ pwarn("arr_trk not yet set."); return; }

  if(PART_IS_MIDI(gpart)){
    dbg(2, "t=%i (%s / %s)", track_num, part->gpart->name, song->tracks->track[track_num]->name);
  }

  GnomeCanvasPoints* pts = part->pts;

  double x = arr_part_start_px(arrange, gpart);
  double y = arrange->canvas->gnome->track_pos[d] + 1.0;
  double w = arr_get_part_core_width_pix_zoomed(arrange, gpart);
  double h = (double)(arr_trk->height * vzoom(arrange) - PART_OFFSET_Y2);
  pts->coords[0] = x+CORNER_SIZE;
  pts->coords[1] = y;
  pts->coords[2] = x+w;
  pts->coords[3] = y;
  pts->coords[4] = x+w;
  pts->coords[5] = y+h;
  pts->coords[6] = x;
  pts->coords[7] = y+h;
  pts->coords[8] = x;
  pts->coords[9] = y+CORNER_SIZE;
  set_points(part, pts);
}


static void
set_gpart(GnomeCanvasPart* part, AMPart* gpart)
{
	// Set the gui data struct associated with the canvas item.
	// -this should only be done when the item is first created.

	// -this is currently being used as an initialiser, for want of anywhere better to put it (why not in _init?).

	g_return_if_fail(part);
	g_return_if_fail(gpart);
	part->gpart = gpart;
	part->velocity_bar_width = 6;
	gnomecanvas_part_set_track(part, am_track_list_position(song->tracks, gpart->track));
	dbg (2, "length=%lu", gpart->length.beat);

	void _part_item_on_peakdata_available(Waveform* waveform, int block, gpointer _part)
	{
		dbg(2, "block=%i", block);
		g_return_if_fail(_part);
		GnomeCanvasPart* part = _part;

		gnome_canvas_part_on_peakdata_available(part, block);
	}
	if(part->peakdata_ready_handler){
		g_signal_handler_disconnect((gpointer)gpart->pool_item, part->peakdata_ready_handler);
		part->peakdata_ready_handler = 0;
	}
	if(gpart->pool_item) part->peakdata_ready_handler = g_signal_connect (gpart->pool_item, "peakdata-ready", (GCallback)_part_item_on_peakdata_available, part);

	GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(part);
	shape->priv->fill_rgba = song->palette[gpart->bg_colour]; //we need to set something for this so that the item is opaque to mouse clicks.
	shape->priv->fill_set = 1;

	gnome_canvas_part_invalidate_pixbuf(part);
}


static void
set_x(GnomeCanvasPart* part, double x)
{
  // Set a new start position for the part.

  GnomeCanvasPoints *pts = part->pts;
  //double y = arrange->track_pos[track_num] + 1.0;
  double w = arr_get_part_core_width_pix_zoomed(part->arrange, part->gpart);
  //double h = (double)(track[track_num]->height * arrange->vzoom - PART_OFFSET_Y2);
  pts->coords[0] = x+CORNER_SIZE;
  //pts->coords[1] = y;
  pts->coords[2] = x+w;
  //pts->coords[3] = y;
  pts->coords[4] = x+w;
  //pts->coords[5] = y+h;
  pts->coords[6] = x;
  //pts->coords[7] = y+h;
  pts->coords[8] = x;
  //pts->coords[9] = y+CORNER_SIZE;
  set_points(part, pts);
}


static void
set_y(GnomeCanvasPart *part, double y)
{
  TrackNum track_num = am_track_list_position(song->tracks, part->gpart->track);
  GnomeCanvasPoints *pts = part->pts;
  double h = (double)(track_list__track_by_index(part->arrange->priv->tracks, track_num)->height * vzoom(part->arrange) - PART_OFFSET_Y2);
  //pts->coords[0] = x+CORNER_SIZE;
  pts->coords[1] = y;
  pts->coords[3] = y;
  pts->coords[5] = y+h;
  pts->coords[7] = y+h;
  pts->coords[9] = y+CORNER_SIZE;
  set_points(part, pts);
}


double
gnome_canvas_part_get_x(GnomeCanvasPart *part)
{
  // Get the x position from the path_def.

  GnomeCanvasPathDef *path_def = part->path_def; //or does the path_def belong to the parent struct?
  ArtBpath* bpath = gnome_canvas_path_def_first_bpath(path_def);

  return bpath->x3; // -5.0? 
}


static void
set_left(GnomeCanvasPart *part, double left)
{
  // This is used during transitory resize operations.
  // -the left of the part is set without affecting the right.
  // -it doesnt access core.

  printf("gnomeCanvasPart::set_left(): left=%.1f.\n", left);

  GnomeCanvasPoints *pts = part->pts;
  left = MAX(left, get_min_left(part));

  double previous_left = pts->coords[6];
  part->transient_offset_left += left - previous_left;

  pts->coords[0] = left + CORNER_SIZE;
  pts->coords[6] = left;
  pts->coords[8] = left;

  set_points(part, pts);
}


static void
set_right(GnomeCanvasPart *part, double right)
{
  // This is used in resize operations, so dont get the width from core.
  // @param right is in pixels.

  right = MIN(right, get_max_right(part));

  GnomeCanvasPoints *pts = part->pts;

  pts->coords[2] = right;
  pts->coords[4] = right;
  set_points(part, pts);
}


double
gnome_canvas_part_get_right(GnomeCanvasPart *part)
{
  GnomeCanvasPoints *pts = part->pts;
  return pts->coords[2];
}


void
gnome_canvas_part_clear_pixbuf(GnomeCanvasPart *part)
{
  gnome_canvas_part_invalidate_pixbuf(part);
}


static void
gnome_canvas_part_set_label(GnomeCanvasPart *part, char* label)
{
  gnome_canvas_part_text2pic(part);
  gnome_canvas_part_invalidate_pixbuf(part);
}


static void
g_canvas_part_set_show_contents(GnomeCanvasPart *part, gboolean show)
{
  //there is currently limited point setting this, as the setting is done at the Arrange level (not per-Part).
  gnome_canvas_part_invalidate_pixbuf(part);
}


static void
gnome_canvas_part_set_property(GObject *object, guint param_id, const GValue *value, GParamSpec *pspec)
{
	GnomeCanvasPoints *points;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GNOME_IS_CANVAS_PART(object));

	GnomeCanvasItem *item = GNOME_CANVAS_ITEM(object);
	GnomeCanvasPart *part = GNOME_CANVAS_PART(object);

	switch (param_id) {
	case PROP_POINTS:
		points = g_value_get_boxed(value);
		set_points(part, points);
		break;
	case PROP_TRACK:
		gnomecanvas_part_set_track(part, g_value_get_int(value));
		break;
	case PROP_GPART:
		;AMPart* gpart = g_value_get_pointer(value);
		set_gpart(part, gpart);
		break;
	case PROP_ARRANGE:
		part->arrange = g_value_get_pointer(value);
		break;
	case PROP_X:
		set_x(part, g_value_get_double(value));
		break;
	case PROP_Y:
		set_y(part, g_value_get_double(value));
		break;
	case PROP_LEFT:
		set_left(part, g_value_get_double(value));
		break;
	case PROP_RIGHT:
		set_right(part, g_value_get_double(value));
		break;
	case PROP_LABEL:
		//set_label(part, g_value_get_string(value));
		gnome_canvas_part_set_label(part, g_value_dup_string(value));
		break;
	case PROP_SHOW_CONTENTS:
		g_canvas_part_set_show_contents(part, g_value_get_boolean(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		return;
		break;
	}
	gnome_canvas_item_request_update(item);
}


static void
gnome_canvas_part_get_property(GObject *object, guint param_id, GValue *value, GParamSpec *pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(GNOME_IS_CANVAS_PART (object));

	switch(param_id){
		case PROP_POINTS:
		break;
	case PROP_X:
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnome_canvas_part_realize(GnomeCanvasItem *item)
{
	GnomeCanvasPart* part = GNOME_CANVAS_PART(item);
	PartPrivate* priv = part->private;

	if (parent_class->realize)
		(* parent_class->realize) (item);

	if (!item->canvas->aa) {
		dbg(0, "FIXME gdk mode.");
		/*
		gcbp_ensure_gdk (shape);

		g_assert(item->canvas->layout.bin_window != NULL);

		shape->priv->gdk->fill_gc = gdk_gc_new (item->canvas->layout.bin_window);
		shape->priv->gdk->outline_gc = gdk_gc_new (item->canvas->layout.bin_window);
		*/
	}

	if (part->arrange->part_fades_show) {
		GnomeCanvasPoints* pts = gnome_canvas_points_new(2);
		pts->coords[0] = pts->coords[1] = 0.0;
		pts->coords[2] = 100.0;
		pts->coords[3] = 100.0;
		//group: part doesnt have a group as such, so lines will be drawn on the root
		GnomeCanvasItem* f_in  = gnome_canvas_item_new(GNOME_CANVAS_GROUP(item->parent), gnome_canvas_line_get_type(),
		                                              "fill-color-rgba", colour_get_offset_rgba(song->palette[part->gpart->bg_colour], 4),
		                                              "points", pts,
		                                              "width-pixels", 1,
		                                              NULL);
		GnomeCanvasItem* f_out = gnome_canvas_item_new(GNOME_CANVAS_GROUP(item->parent), gnome_canvas_line_get_type(),
		                                              "fill-color-rgba", colour_get_offset_rgba(song->palette[part->gpart->bg_colour], 4),
		                                              "points", pts,
		                                              "width-pixels", 1,
		                                              NULL);
		priv->fade_in  = GNOME_CANVAS_LINE(f_in);
		priv->fade_out = GNOME_CANVAS_LINE(f_out);
		gnome_canvas_item_raise_to_top(f_in);
		gnome_canvas_item_raise_to_top(f_out);
		gnome_canvas_item_hide(f_in);
		gnome_canvas_item_hide(f_out);
		gnome_canvas_points_unref(pts);
	}
}


#ifdef NOT_USED
static gboolean
part_note_is_selected(GnomeCanvasPart *part, MidiNote *note)
{
	GList* l = ((MidiPart*)part->gpart)->note_selection;
	for(;l;l=l->next){
		MidiNote* selected = l->data;
		if(note == selected) return TRUE;
	}
	return FALSE;
}
#endif


static inline void
draw_note (GnomeCanvasPart* part, MidiNote* note, GdkColor* colour)
{
	Arrange* arrange = part->arrange;
	ArrTrackMidi* miditrack = (ArrTrackMidi*)track_list__lookup_track(arrange->priv->tracks, part->gpart->track);

	double x1 = arr_samples2px(arrange, note->start);
	double x2 = MAX(arr_samples2px(arrange, note->length), ARR_MIN_NOTE_DISPLAY_LENGTH) + x1;
	double y1 = arr_track_note_pos_y((ArrTrk*)miditrack, note);
	if(x1 < 0 || x1 > 1000 || x2 < 0 || x2 > 1000){ pwarn("x out of range!"); return; }
	dbg(3, "note=%i start=%i x=%.2f->%.2f y=%.2f", note->note, note->start, x1, x2, y1);
	if(y1 > 0){
		//double y2 = y1 + part->arrange->track[part->track_num]->note_height;
		double height = miditrack->note_height;
		DRect pts = {x1, y1, x2, y1};
#ifdef BPP32
		pixbuf_draw_line_cairo(part->cr, &pts, height, colour);
#else
		pixbuf_draw_line(part->pixbuf, &pts, height, colour);

		#ifdef SHOW_NOTE_SHADOW
		GdkColor shadow_colour = {0, 0, 0, 0};
		pts.y1 = pts.y2 = y1 + height - 5;
		pixbuf_draw_line(part->pixbuf, &pts, 2, &shadow_colour);
		pts.y1 = y1 - 4;
		pts.x1 = pts.x2 = x2;
		pixbuf_draw_line(part->pixbuf, &pts, 1, &shadow_colour);
		#endif
#endif
	}
}


static inline void
draw_note_velocity_bar(GnomeCanvasPart* part, MidiNote* note, GdkColor* colour)
{
#ifndef DEBUG_TRACKCONTROL
	ArrTrk* trk = track_list__lookup_track(part->arrange->priv->tracks, part->gpart->track);
	int chart_height = ((ArrTrackMidi*)trk)->velocity_chart_height;
	if(chart_height < 1) return;
	double x1 = arr_samples2px(part->arrange, note->start);
	double y2 = gdk_pixbuf_get_height(part->pixbuf);
	double y1 = y2 - (chart_height * note->velocity) / 128;

	GdkRectangle rect = {x1, y1, part->velocity_bar_width, (chart_height * note->velocity) / 128};
	pixbuf_fill_rect(part->pixbuf, &rect, colour);
#endif
}


static void
gnome_canvas_part_midi_draw_pixbuf(GnomeCanvasItem* item)
{
  GnomeCanvasPart* part = GNOME_CANVAS_PART(item);

#ifdef BPP32
  GdkPixbuf* pixbuf = part->pixbuf;
  //TODO i think the warning below is no longer correct
  if (gdk_pixbuf_get_n_channels(pixbuf) == 3){ printf("%s(): **** warning: cairo cannot draw to 24bpp buffer!", __func__); return; }
  cairo_format_t format = CAIRO_FORMAT_ARGB32;

  cairo_surface_t* surface = cairo_image_surface_create_for_data (gdk_pixbuf_get_pixels(pixbuf), format, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), gdk_pixbuf_get_rowstride(pixbuf));
  cairo_t*         cr      = cairo_create(surface);
  float r=1.0, g=1.0, b=1.0;
  cairo_set_source_rgb(cr, r, g, b);
  cairo_set_line_width(cr, 1.0);

  {
    rect pts = {0, 0, 20, 20};
    cairo_rectangle(cr, pts.x1, pts.y1, pts.x2 - pts.x1 + 1, pts.y2 - pts.y1 + 1);
    cairo_fill (cr);
    cairo_stroke (cr);
  }

  cairo_destroy(cr);
  cairo_surface_destroy(surface);
#else

  // velocity bar background
  GdkColor velo_bg = {0, 0x7777, 0xaaaa, 0xffff};
  am_palette_lookup(song->palette, &velo_bg, part->gpart->bg_colour);
  colour_lighter_gdk(&velo_bg, 3);

  ArrTrk* trk = track_list__lookup_track(part->arrange->priv->tracks, part->gpart->track);
  g_return_if_fail(trk);
  g_return_if_fail(trk->trk_ctl);
#ifndef DEBUG_TRACKCONTROL
  int chart_height = ((ArrTrackMidi*)trk)->velocity_chart_height;
  int x2 = gdk_pixbuf_get_width(part->pixbuf);
  int y2 = gdk_pixbuf_get_height(part->pixbuf);
  int y1 = y2 - chart_height;
  GdkRectangle rect = {0, y1, x2, chart_height};
  if(y1 > 0) pixbuf_fill_rect(part->pixbuf, &rect, &velo_bg);

  int note_area_height = gdk_pixbuf_get_height(part->pixbuf) - chart_height;
  if(part->editing){
    // draw background
    GdkColor black_bg = {0, 0xeeee, 0xeeee, 0xeeee};
    ArrTrackMidi* atm = (ArrTrackMidi*)trk;
    int n_notes = arr_track_midi_get_n_notes(atm);
    int top_note = atm->centre_note + n_notes / 2;
    int part_width = gdk_pixbuf_get_width(part->pixbuf);
    int line_width = atm->note_height;
    int i; for(i=0;i<n_notes;i++){
      int note_num = top_note - i;
      if(note_is_black(note_num)){
        int y = i * atm->note_height; // XXX
        if(y > note_area_height) break;

        DRect pts = {0, y, part_width, y};
        pixbuf_draw_line(part->pixbuf, &pts, line_width, &black_bg);
      }
    }
  }

  GdkColor colour_fg   = {0, 0, 0xffff, 0};
  GdkColor colour_bar  = {0, 0xffff, 0xffff, 0};
  MidiNote* note = NULL;
  int safety = 0;
  while((note = am_midi_part__get_next_event(part->gpart, note))){
    uint32_t rgba = (note->velocity < 50) ? midi_colours[0] : ((note->velocity < 100) ? midi_colours[1] : midi_colours[2]);
    am_rgba_to_gdk(&colour_fg, rgba);
    draw_note(part, note, &colour_fg);
    draw_note_velocity_bar(part, note, &colour_bar);
    if(safety++ > 512){ perr("bailing out!"); break; }
  }
  if(!safety) dbg(1, "no events found");

  // draw the Selected notes again
  colour_fg.red = 0xffff;
  GdkColor colour_fg2   = {0, 0xffff, 0, 0};
  if(part->arrange->canvas->op->note_selection){
    GList* l = canvas_op__get_note_selection(part->arrange->canvas->op, (MidiPart*)part->gpart);
    for(;l;l=l->next){
      TransitionalNote* tn = l->data;
      MidiNote* note = &tn->transient;
      draw_note(part, note, &colour_fg2);
      draw_note_velocity_bar(part, note, &colour_fg2);
    }

  } else if(/*part->editing &&*/ ((MidiPart*)part->gpart)->note_selection){
    GList* l = ((MidiPart*)part->gpart)->note_selection;
    for(;l;l=l->next){
      MidiNote* note = l->data;
      dbg(0, "%i selected! drawing.", note->note);
      draw_note(part, note, &colour_fg2);
      draw_note_velocity_bar(part, note, &colour_fg2);
    }
  }
#endif
#endif
}


static void
gnome_canvas_part_render(GnomeCanvasItem* item, GnomeCanvasBuf* buf)
{
	// @buf is not the whole canvas, its a tile of usually 256 x 64.

	// item->x1 etc is the canvas item *bounding box* - it is slightly bigger than the specified size of the Part.

	dbg(2, "buf: x=%i->%i y=%i->%i. item: y=%.1f->%.1f", buf->rect.x0, buf->rect.x1, buf->rect.y0, buf->rect.y1, item->y1, item->y2);

	GnomeCanvasPart* part   = GNOME_CANVAS_PART(item);
	GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(item);
	PartPrivate* priv       = part->private;

	if(shape->priv->fill_svp != NULL)
		gnome_canvas_render_svp (buf, shape->priv->fill_svp, shape->priv->fill_rgba);

	if(shape->priv->outline_svp != NULL)
		gnome_canvas_render_svp (buf, shape->priv->outline_svp, shape->priv->outline_rgba);

	// pixbuf stuff

	if(!priv->pixbuf_needs_update) gnome_canvas_part_update_pixbuf(item); //TODO isnt this the wrong way round?
	if(!part->pixbuf) return; //part too small?

	//FIXME the difference between the bounding box and start of the pixbuf is not the same as priv->width.
	int outline_width = PART_OUTLINE_WIDTH; //convert outline to int.

	int buf_top    = buf->rect.y0;
	int buf_bottom = buf->rect.y1;
	int buf_left   = buf->rect.x0;
	int buf_right  = buf->rect.x1;
	int part_top    = item->y1 + outline_width;
	int part_bottom = part_top + gdk_pixbuf_get_height(part->pixbuf); //we dont use item->y2 here as it is 1px too big.
	int part_left   = item->x1 + outline_width;
	int part_right  = item->x2 - outline_width;

	AMPart* gpart = part->gpart;
	if(PART_IS_AUDIO(gpart) && part->tiles){
		// do we need to refresh the peak data?
		int tile_num;
		if(part_left >= buf_left){
			tile_num = 0;
			PeakTile* tile = gnome_canvas_part_get_tile_n(tile_num);
			part_try_set_peak_tile(part, tile);
// #warning TODO now get other tiles
			int i; for(i=1;i<part->tiles->len;i++){
				if(part_tile_is_visible((GnomeCanvasItem*)part, i)){
					PeakTile* tile = gnome_canvas_part_get_tile_n(tile_num);
					if (tile->dirty){
						part_try_set_peak_tile(part, tile);
					}
				}
			}
		}
		else if(part_left <  buf_left){
			tile_num = (buf_left - part_left) / PEAK_TILE_SIZE;
			if (tile_num >= part->tiles->len) {
				// this can happen during part resizing. Perhaps harmless
				pwarn ("tile_num out of range! %i", tile_num);
			} else {
				PeakTile* tile = gnome_canvas_part_get_tile_n(tile_num);
				if (tile->dirty){
					part_try_set_peak_tile(part, tile);
				}
			}
			// check the right edge of the buffer
			int tile_num2 = (buf_right - part_left) / PEAK_TILE_SIZE;
			if(tile_num2 < part->tiles->len){
				PeakTile* tile = gnome_canvas_part_get_tile_n(tile_num2);
				if(tile->dirty){
					part_try_set_peak_tile(part, tile);
				}
			}
		}
		dbg(2, "tile=%i", tile_num);
	}

	GdkPixbuf* dest_pixbuf = gdk_pixbuf_new_from_data(buf->buf,
                            GDK_COLORSPACE_RGB,
#ifdef BPP32
                            HAS_ALPHA_TRUE,   //crashes ?
#else
                            HAS_ALPHA_FALSE,
#endif
                            8,
                            buf->rect.x1 - buf->rect.x0,
                            buf->rect.y1 - buf->rect.y0,
                            buf->buf_rowstride,
                            NULL, NULL);

  char debug_x[64];
  char debug_y[64];

#define USE_GDK_COMPOSITE
#ifdef USE_GDK_COMPOSITE
  int x0 = 0, y0 = 0, x1 = 0, y1 = 0;    //the rect area within the current tile (ie 0 to 63).
  gboolean draw = true;

  if(buf_top > item->y1){   //we are not rendering the top of the Part.
    if(buf_top < part_bottom){
      snprintf(debug_y, 64, "y-mid");
      y0 = 0;
      y1 = part_bottom - buf_top;
    }else{
      draw = false;
      sprintf(debug_y, "y-false");
	}
  }else{
    sprintf(debug_y, "y-%s1st%s", bold, white);
    y0 = part_top - buf_top;     //the distance of the part top from the start of this tile.

    y1 = MIN(part_bottom,        //complate part: y1 is part bottom, ie item->y2.
             buf_bottom);        //partial part:  y1 is start of next block.

  }
  double offset_y = part_top - buf_top;

  if(buf->rect.x0 > item->x1){   //we are not rendering the lhs of the Part.
    if(buf_left < part_right){   //we are off the end of the Part.
      snprintf(debug_x, 64, "x-2nd");

      x0 = 0;
      x1 = MIN(part_right - buf_left, buf_right - buf_left);

      if(x0 > x1){ 
        draw = FALSE;
      }
    } else draw = FALSE;
  }else{
    snprintf(debug_x, 64, "x-1st");
    x0 = part_left - buf_left;
    x1 = MIN(part_right - buf_left, buf_right - buf_left);
      if(x0 > x1){ 
        draw = FALSE;
      }
	}
#ifndef NOT_TEMP
	dbg(2, "transient_offset_left=%.2f", part->transient_offset_left);
	double offset_x = part_left - buf_left - part->transient_offset_left;
#else
	double offset_x = part_left - buf_left;
#endif

	//bounds checking:

	//occasionally the bounds are out by a small amount but it doesnt seem to have any adverse
	//effects so the warning messages are disabled.

	//if(x1 < 0 || x0 < 0 ) pwarn("<0: x1=%i x0=%i\n", x1, x0);
	//if(x0 < 0) pwarn("%s x < 0.\n", debug_x);
	//if(x0 > (buf_right - buf_left)) pwarn("%s x0 > buf_width.\n", debug_x);
	x0 = CLAMP (x0, 0, buf_right - buf_left);
	y0 = CLAMP (y0, 0, buf_bottom - buf_top);

	//if(x1 < 0) pwarn("%s x1 < 0.\n", debug_x);
	//if(x1 > (buf_right - buf_left)) pwarn("%s x1 > buf_width.\n", debug_x);
	x1 = CLAMP (x1, 0, buf_right - buf_left);
	y1 = CLAMP (y1, 0, buf_bottom - buf_top);

	//if(buf_top + y1 > part->arrange->track_pos[part->track_num + 1]) pwarn("!!");
	y1 = MIN (y1, part_bottom - buf_top);

	//if(x0==0 && ((x1 - x0) > part_width)) pwarn("too wide.\n");

#if 0
	if(part->arrange == (Arrange*)windows__get_first(AYYI_TYPE_ARRANGE) && gpart->ayyi->shm_idx == 3 && !strcmp(debug_x, "x-1st")){
		dbg(0, "%i: %s %s y0=%i y1=%i offset_y=%.1f", gpart->idx, debug_x, debug_y, y0, y1, offset_y);
		dbg(0, "   backing_height=%i dest_height=%i copied=%02i buf-y=%i-->%i part-y=%i-->%i %s", gdk_pixbuf_get_height(part->pixbuf), gdk_pixbuf_get_height(dest_pixbuf), y1-y0, buf_top, buf_bottom, part_top, part_bottom, part->gpart->name);
		dbg(0, "   top=%i=%i bottom=%i=%i", part_top, part->arrange->track_pos[part->track_num] + outline_width, part_bottom, part->arrange->track_pos[part->track_num + 1] - outline_width);
	}
#endif
	if(draw){
		gdk_pixbuf_composite(part->pixbuf,
		                     dest_pixbuf,
		                     x0, y0,                    //int dest x,y
		                     x1 - x0, y1 - y0,          //dest w,h
		                     offset_x, offset_y,        //double offset (translation)
		                     1.0, 1.0,                  //double scale
		                     GDK_INTERP_NEAREST,
		                     255);
	}
#endif

#ifdef USE_GDK_COPY
	// looks like gdk_pixbuf_copy_area requires both pixbufs to have the same transparency options
	// which is not the case for us.
	int src_x=0, src_y=0;
	int width=0, height=0;
	int dest_x=0, dest_y=0;
	int src_height  = gdk_pixbuf_get_height(part->pixbuf);
	int dest_width  = gdk_pixbuf_get_width (dest_pixbuf);
	int dest_height = gdk_pixbuf_get_height(dest_pixbuf);
	gboolean draw = TRUE;
	printf(" canvas has alpha: %i\n", gdk_pixbuf_get_has_alpha(dest_pixbuf));

	if(buf_top > item->y1){                    // we are not rendering the top of the Part.
		if(buf_top < part_bottom){             // we are off the bottom.
      snprintf(debug_y, 64, "y-mid");
      src_y = buf_top - part_top;              // distance of buffer start from part start
      height = MIN(buf_bottom - buf_top,       // distance from buffer start to buffer end
                   part_bottom - buf_top);     // or distance from buffer start to part end.
      printf(" a: height: buf_bottom =%i - buf_top=%i\n", buf_bottom, buf_top);
      printf(" b: height: part_bottom=%i - buf_top=%i\n", part_bottom, buf_top);
      dest_y = 0;
    } else draw = FALSE;
	}else{
		snprintf(debug_y, 64, "y-1st");
		src_y = 0;
		height = MIN(buf_bottom - part_top,    //distance from part start to buffer end.
		         part_bottom - part_top);      //or distance to part end.
		printf(" a: height: buf_bottom =%i - part_top=%i\n", buf_bottom, part_top);
		dest_y = part_top - buf_top; //distance of the part top from the start of this tile.
	}
	if(buf->rect.y1 > item->y2){//part bottom.
		//y1 -= (outline_width+5);
	}

	if(buf->rect.x0 > item->x1){                    // we are not rendering the lhs of the Part.
		if(buf_left < part_right){                  // we are off the end of the Part.
			snprintf(debug_x, 64, "x-2nd");
			src_x = buf_left - part_left;           // distance of buffer start from part start
			width = MIN(buf_right - buf_left,       // distance from buffer start to buffer end
			            part_right - buf_left);     // or distance from buffer start to part end.
			dest_x = part_left - buf_left;
		} else draw = FALSE;
	}else{
		snprintf(debug_x, 64, "x-1st");
		src_x = 0;
		width = MIN(buf->rect.x1 - part_left,   // distance from part start to buffer end.
		            part_right - part_left);    // or distance to part end.
		dest_x = part_left - buf_left;
	}
	//if(buf->rect.x1 > item->x2) x1 -= (outline_width+10);//PART_BORDER;

	if(src_y < 0) errprintf("  1a: src_y=%i\n", src_y);
	if(src_y + height > gdk_pixbuf_get_height(part->pixbuf)) errprintf("  %s src_y=%i height=%i src_height=%i dest_height=%i\n", debug_y, src_y, height, src_height, dest_height);
	if(height < 1) warnprintf("  %s height=%i\n", debug_y, height);

	printf("  src_y=%i height=%i src_height=%i\n", src_y, height, gdk_pixbuf_get_height(part->pixbuf));

	//int render_height = MIN(gdk_pixbuf_get_height(part->pixbuf), buf->rect.y1 - buf->rect.y0 - y0);

	//if(x0==0 && ((x1 - x0) > render_width)) warnprintf("gnome_canvas_part_render(): too wide.\n");

	//check the src area isnt bigger than its dimensions: (src_x + width <= src_pixbuf->width)
	//if(offset_x + (x1 - x0) > render_width) warnprintf("gnome_canvas_part_render(): specified src area is too wide.\n");

	if((width > 0) && (height > 0) && (draw)){
	gdk_pixbuf_copy_area(part->pixbuf,       //const GdkPixbuf *src_pixbuf,
	                     src_x, src_y,       //int src_x,src_y
	                     width, height,      //int width,height
	                     dest_pixbuf,
	                     dest_x,             //int dest_x
	                     dest_y);            //int dest_y
	}
#endif

	g_object_unref(dest_pixbuf);
  
#if 0
	GnomeCanvasLineClass* line_class = GNOME_CANVAS_LINE_GET_CLASS(priv->fade_in);
	/*if(priv->fade_in->update)*/ (* line_class->parent_class.render) (GNOME_CANVAS_ITEM(priv->fade_in), buf);
	gnome_canvas_item_show(GNOME_CANVAS_ITEM(priv->fade_in));
	//dbg(0, "rendered=%i", GNOME_CANVAS_ITEM(priv->fade_in)->object.flags & GNOME_CANVAS_ITEM_REALIZED);
#endif
}


static void
blit_label(GnomeCanvasPart* part)
{
  //put the cached label pixbuf onto the Part canvas pixbuf:
  gboolean hide = FALSE;
  GdkPixbuf* cache = part->label_pixbuf;
  GdkPixbuf* dest_pixbuf = part->pixbuf;
  #define PART_LABEL_LEFT_OFFSET 0
  int part_width  = gdk_pixbuf_get_width (part->pixbuf) - PART_LABEL_LEFT_OFFSET;
  int part_height = gdk_pixbuf_get_height(part->pixbuf);
  int width  = MIN(gdk_pixbuf_get_width (part->label_pixbuf), part_width);
  int height = MIN(gdk_pixbuf_get_height(part->label_pixbuf), part_height);

  if(!hide){
    gdk_pixbuf_copy_area(cache,                 //const GdkPixbuf *src_pixbuf,
                         0,0,                   //int src_x,src_y
                         width, height,         //int width,height
                         dest_pixbuf,
                         PART_LABEL_LEFT_OFFSET,//int dest_x
                         part_height - height); //int dest_y
  }
}


gboolean
canvas_item_is_visible(GnomeCanvasItem *item)
{
  // Return TRUE if an item is both with the scroll-region, and not hidden.

  GnomeCanvasPart* part = GNOME_CANVAS_PART(item);

  double x1, y1, x2, y2;
  gnome_canvas_item_get_bounds(item, &x1, &y1, &x2, &y2);

  int cx1, cy1, cx2, cy2;
  gnome_canvas_get_scroll_offsets(GNOME_CANVAS(part->arrange->canvas->widget), &cx1, &cy1);
  cy2 = cy1 + part->arrange->canvas_scrollwin->allocation.height;
  cx2 = cx1 + part->arrange->canvas_scrollwin->allocation.width;

  if(y1 > cy2){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off bottom.
  if(y2 < cy1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off top.
  if(x1 > cx2){ /*printf("canvas_item_is_visible(): no. off right.\n");*/ return FALSE; }//off right.
  if(x2 < cx1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off left.

  return TRUE;
}


static bool
part_tile_is_visible (GnomeCanvasItem* item, int block_num)
{
  // return TRUE if the item block is both with the scroll-region, and the part is not hidden.

  // note: its a little inefficient to check each block individually.

  GnomeCanvasPart* part = GNOME_CANVAS_PART(item);

  double x1, y1, x2, y2;
  gnome_canvas_item_get_bounds(item, &x1, &y1, &x2, &y2);

  int cx1, cy1, cx2, cy2;
  gnome_canvas_get_scroll_offsets(GNOME_CANVAS(part->arrange->canvas->widget), &cx1, &cy1);
  cy2 = cy1 + part->arrange->canvas_scrollwin->allocation.height;
  cx2 = cx1 + part->arrange->canvas_scrollwin->allocation.width;

  int bx1, bx2;                //left and right edge of the block.
  int scale = PEAK_TILE_SIZE;  //zooming is already done (?)
  bx1 = x1 +  block_num    * scale;
  bx2 = x2 + (block_num+1) * scale;

  if(y1 > cy2){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off bottom.
  if(y2 < cy1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off top.
  if(bx1 > cx2){ /*printf("canvas_item_is_visible(): no. off right.\n");*/ return FALSE; }//off right.
  if(bx2 < cx1){ /*printf("canvas_item_is_visible(): no.\n");*/ return FALSE; }//off left.

  return TRUE;
}


static void
gnome_canvas_part_invalidate_pixbuf(GnomeCanvasPart* part)
{
	//this only needs to be called if the dimensions have changed.
	//-otherwise mark tiles as dirty.

	//indicate that pixbuf is invalid:
	if(part->pixbuf) g_object_unref(part->pixbuf);
	part->pixbuf = NULL;

	if(part->tiles) g_array_free(part->tiles, FREE_SEGMENTS);
	part->tiles = NULL;

#ifdef BPP32
	cairo_destroy(part->cr);
	cairo_surface_destroy(part->surface);
#endif
}


static void
gnome_canvas_part_update_pixbuf(GnomeCanvasItem *item)
{
	// having a single pixbuf is slightly troublesome at high zoom.
	// For a "full zoom" part of length 1 hour and height 1024px, memory used is 1024 * 44100 * 3 * 60 * 60 = 400GB ??
	// So at some point, we will have to change to using one pixbuf per tile (or per block).

	GnomeCanvasShape* shape = GNOME_CANVAS_SHAPE(item);
	GnomeCanvasPart*  part  = GNOME_CANVAS_PART(item);
	PartPrivate* priv = part->private;
	AMPart* gpart = part->gpart;

	double outline_width = shape->priv->width;
	int w        = item->x2 - item->x1 - outline_width*2 + part->transient_offset_left; 
	float top    = part->pts->coords[1];
	float bottom = part->pts->coords[5];
	//int h      = item->y2 - item->y1 - outline_width*2;
	int h        = MIN(MAX_PART_HEIGHT, bottom - top - outline_width*2);

	if(w < 1) return; //not an error - just small.
	if(!SHOW_CONTENTS) w = MIN(w, 100); //testing
	if(w > MAX_PIXBUF_WIDTH){ pwarn("max width exceeded: %i", w); w = MAX_PIXBUF_WIDTH; }
	if(h < 1){
		perr("bad dimensions (%ix%i) x1=%.1f x2=%.1f\n", w, h, item->x1, item->x2); 
		h = 1; //it behaves better when there is a pixbuf....
	}

	//FIXME dont make a new pixbuf during transient ops if pixbuf is big enough.
	if(priv->pixbuf_needs_update || !part->pixbuf){
		struct timeval time_start;
		gettimeofday(&time_start, NULL);

#ifdef DEBUG
		if(_debug_ > 1){
			double mem = ((double)w * h * 4) / (1024*1024);
			dbg(0, "new pixbuf: %ix%i memory=%.3fMB '%s'", w, h, mem, part->gpart->name);
		}
#endif
#if 0
    if(part->pixbuf) gdk_pixbuf_unref(part->pixbuf);
#endif
    if(!part->pixbuf){
      part->pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
#ifdef BPP32
                                    HAS_ALPHA_TRUE,
#else
                                    HAS_ALPHA_FALSE,
#endif
                                    BITS_PER_SAMPLE, w, h);
#ifdef BPP32
      cairo_format_t format;
      part->surface = cairo_image_surface_create_for_data(gdk_pixbuf_get_pixels(part->pixbuf), format, w, h, gdk_pixbuf_get_rowstride(part->pixbuf));
      part->cr = cairo_create(part->surface);
#endif
    }
    GdkPixbuf* pixbuf = part->pixbuf;

    uint32_t bg_color = part->editing ? GNOME_CANVAS_PART_GET_CLASS(part)->edit_colour : song->palette[part->gpart->bg_colour];
    gdk_pixbuf_fill(pixbuf, bg_color);

    //test line:
    /*
    struct _ArtDRect pts = {0.0,0.0,30.0,30.0};
    GdkColor colour_fg = {0,0,0,0};
    pixbuf_draw_line(pixbuf, &pts, 2.0, &colour_fg);
    */

    if(gpart->pool_item && !part->tiles && SHOW_CONTENTS){
      //setup pixbuf tiles:
      int tile_count = w / PEAK_TILE_SIZE + 1;
      part->tiles = g_array_sized_new(ZERO_TERMINATED, TRUE, sizeof(struct _peak_tile), tile_count + 1);
      g_array_set_size(part->tiles, tile_count);
      dbg(2, "tile_count=%i", tile_count);
      int i; for(i=0;i<part->tiles->len;i++){
        PeakTile* tile = &g_array_index(part->tiles, struct _peak_tile, i);
        tile->index = i;
        tile->dirty = TRUE;
        if(part_tile_is_visible(item, i)){
          //dbg(0, "part=%p arrange=%p", part, part->arrange);
          part_try_set_peak_tile(part, tile);
        }
      }
    }

    if(PART_IS_MIDI(gpart)){
      gnome_canvas_part_midi_draw_pixbuf(item);
    }

    //part_set_peak(gpart);
    gnome_canvas_part_text2pic(part); //no not here!
    blit_label(part);

    pixbuf_transparent_corner(pixbuf);

    priv->pixbuf_needs_update = false;

    //report_time(&time_start);
  }
}


static gboolean
gnome_canvas_part_update_fades(GnomeCanvasPart *_part)
{
  //we have to do this in an idle, else the canvas is not updated properly.

  while(fade_redraw_list){
    GnomeCanvasPart* part = fade_redraw_list->data;
    PartPrivate* priv = part->private;
    if (CAN_SHOW_FADES) {
      AMPart* gpart = part->gpart;
      uint32_t fade_in  = am_part__get_fade_in(gpart);
      uint32_t fade_out = am_part__get_fade_out(gpart);

      //TODO fade line angles are not currently to scale. Fade value is in samples.
      GnomeCanvasPoints* pts = gnome_canvas_points_new(2);
      pts->coords[0] = part->pts->coords[0] + /*PART_OUTLINE_WIDTH + */fade_in * hzoom(part->arrange) * 0.01;
      pts->coords[1] = part->pts->coords[1];
      pts->coords[2] = part->pts->coords[6] /*+ PART_OUTLINE_WIDTH*/;
      pts->coords[3] = part->pts->coords[7];
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(priv->fade_in), "points", pts, NULL);

      pts->coords[0] = part->pts->coords[2] - fade_out * hzoom(part->arrange) * 0.01;
      pts->coords[1] = part->pts->coords[3];
      pts->coords[2] = part->pts->coords[4];
      pts->coords[3] = part->pts->coords[5];
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(priv->fade_out), "points", pts, NULL);

      gnome_canvas_points_unref(pts);

    } else {
      gnome_canvas_item_hide(GNOME_CANVAS_ITEM(priv->fade_in));
    }
    fade_redraw_list = g_list_remove(fade_redraw_list, part);
  }
  fades_idle = 0; //show queue has been cleared
  return false;   //dont run again.
}


static void
gnome_canvas_part_update(GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags)
{
  GnomeCanvasPart* part = GNOME_CANVAS_PART(item);
  PartPrivate* priv = part->private;

  //common part
  if(parent_class->update) (* parent_class->update) (item, affine, clip_path, flags);

  //pixbuf:
  if(canvas_item_is_visible(item)){      //TODO now we're using tiles, we should move the visible check into gnome_canvas_part_update_pixbuf() ?
    if(!priv->pixbuf_needs_update) gnome_canvas_part_update_pixbuf(item);
  }else{
    //printf("gnome_canvas_part_update(): not visible. ignoring...\n");
  }

  if(!g_list_find(fade_redraw_list, item)) fade_redraw_list = g_list_append(fade_redraw_list, item);
  if(!fades_idle) fades_idle = g_idle_add((GSourceFunc)gnome_canvas_part_update_fades, item);
}


void
gnome_canvas_part_set_selected(GnomeCanvasItem* item, gboolean selected)
{
  g_return_if_fail(item);

  if(selected){
    gnome_canvas_item_set(item, "outline_color_rgba", config->part_outline_colour_selected, NULL);
  }else{
    GNOME_CANVAS_PART(item)->editing = FALSE;
    GNOME_CANVAS_PART(item)->arrange->part_editing = FALSE; //move!
    gnome_canvas_item_set(item, "outline_color_rgba", config->part_outline_colour, NULL);
  }
}


static int
gnome_canvas_part_get_peak_block_num(GnomeCanvasPart* part, PeakTile* tile, int* block_count)
{
  //note that blocks are directly related to the source file,
  //but tiles are dependent on the view scale.

  //@block_count: Return either 1 or 2. Near a block boundary there could be 2 blocks in the 1 tile.

  /*
  samples_per_px   tile  --->  scale  block
               1      1            1      0
               2      1            2      0
               8      1            8      1 maybe
  */

  //'scale' is the number of samples per pixel. It is 1 at max zoom, and gets larger as we zoom out.
  //guint scale = part->gpart->pool_item->samplecount / gdk_pixbuf_get_width(part->pixbuf); //TODO only true for full regions
  guint scale = ayyi_pos2samples(&part->gpart->length) / gdk_pixbuf_get_width(part->pixbuf);

  uint32_t sample_start = part->gpart->region_start;
  uint32_t tile_start_samples = sample_start + (PEAK_TILE_SIZE * tile->index  ) * scale;
  uint32_t tile_end_samples   = sample_start + (PEAK_TILE_SIZE * tile->index+1) * scale - 1;
  #define PEAK_RESOLUTION 256
  int peakbuf_block_num1 = tile_start_samples / (WF_PEAK_BLOCK_SIZE * PEAK_RESOLUTION);
  int peakbuf_block_num2 = tile_end_samples   / (WF_PEAK_BLOCK_SIZE * PEAK_RESOLUTION);
  *block_count = (peakbuf_block_num1 == peakbuf_block_num2) ? 1 : 2;
  dbg(2, "tile=%i scale=%i start=%u block=%i", tile->index, scale, tile_start_samples, peakbuf_block_num1);
  if(GPOINTER_TO_INT(block_count[0]) == 2) dbg(0, "2 blocks ******");

  return peakbuf_block_num1;
}


static int
gnome_canvas_part_get_tiles_for_block(GnomeCanvasPart* part, int block_num, PeakTile** first_tile)
{
	//convert block_num to px relative to part start:
	g_return_val_if_fail(ayyi_pos2samples(&part->gpart->length), 0);
	g_return_val_if_fail(part->pixbuf, 0);
	guint samples_per_px = ayyi_pos2samples(&part->gpart->length) / gdk_pixbuf_get_width(part->pixbuf);
	int px_start = (block_num   ) * wf_get_peakbuf_len_frames() / samples_per_px;
	int px_end   = (block_num +1) * wf_get_peakbuf_len_frames() / samples_per_px;

	int n_tiles_per_block = (px_end - px_start) / PEAK_TILE_SIZE;
	dbg(2, "b=%i samples_per_px=%i px_start=%i px_end=%i n_tiles_per_block=%i", block_num, samples_per_px, px_start, px_end, n_tiles_per_block);
	int n_tiles = MIN(n_tiles_per_block, part->tiles->len - block_num * n_tiles_per_block);
	if(n_tiles > part->tiles->len){ pwarn("n_tiles too high! %i > %i (block_num=%i)", n_tiles, part->tiles->len, block_num); n_tiles = part->tiles->len; }
	if(first_tile) *first_tile = gnome_canvas_part_get_tile_n(px_start / PEAK_TILE_SIZE);
	return n_tiles;
}


int
gnome_canvas_part_get_width_pixels(GnomeCanvasPart* part)
{
  return gdk_pixbuf_get_width(part->pixbuf);
}


static void
gnome_canvas_part_on_peakdata_available(GnomeCanvasPart* part, int block_num)
{
	//TODO dont do anything if the block is not visible
	PF;

	if(!part->pixbuf){ dbg(2, "no pixbuf (b=%i)", block_num); return; } // this is not an error. e.g. part might not be visible

	PeakTile* first_tile = NULL;
	int n_tiles = gnome_canvas_part_get_tiles_for_block(part, block_num, &first_tile);
	g_return_if_fail(first_tile && part->tiles);
	dbg(2, "b=%i first_tile=%p=%i n=%i tot_tiles=%i", block_num, first_tile, first_tile->index, n_tiles, part->tiles->len);
	if(n_tiles){
		int n = first_tile->index;
		int i; for(i=0;i<n_tiles;i++){
//temporarily downgrading to warning instead of error
//			g_return_if_fail(n < part->tiles->len);
if(!(n < part->tiles->len)){ pwarn("n > tiles->len"); break; }
			//the tiles will most likely be marked dirty anyway, but just in case...
			PeakTile* tile = gnome_canvas_part_get_tile_n(n);
			if(tile) tile->dirty = TRUE;
			n++;
		}

		gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(part));
	}
		else pwarn("no tiles");
}


void
gnome_canvas_part_begin_edit(GnomeCanvasPart* part)
{
  PF;
  part->editing = TRUE;
  gnome_canvas_part_invalidate_pixbuf(part);
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(part));
}


void
gnome_canvas_part_end_edit(GnomeCanvasPart* part)
{
  PF;
  part->editing = FALSE;
  gnome_canvas_part_invalidate_pixbuf(part);
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(part));
}


void
gnome_canvas_part_set_edit_selection(GnomeCanvasPart* part, GList* selection)
{
  //note: is part of observer chain, so model data must be already set. We just redraw.

  gnome_canvas_part_invalidate_pixbuf(part);
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(part));
}


void
gnome_canvas_part_edit_select_next(GnomeCanvasPart* part)
{
  // if nothing previously selected, first note will be selected.
  // TODO refactoring probably needed following Observer changes to gnome_canvas_part_set_edit_selection()
  PF;

  MidiNote* prev = ((MidiPart*)part->gpart)->note_selection ? ((MidiPart*)part->gpart)->note_selection->data : NULL;
  MidiNote* note = am_midi_part__get_next_event(part->gpart, prev);
  if(note){
    GList* new_selection = g_list_append(NULL, note);
    gnome_canvas_part_set_edit_selection(part, new_selection);
  }
}


#if 0
void
gnome_canvas_part_edit_select_prev(GnomeCanvasPart* part)
{
  PF;
  MidiNote* old = NULL;
  if(((MidiPart*)part->gpart)->note_selection){
    old = ((MidiPart*)part->gpart)->note_selection->data;
  }
  MidiNote* note = am_midi_part__get_prev_event(part->gpart, old);
  //((MidiPart*)part->gpart)->note_selection = g_list_append(((MidiPart*)part->gpart)->note_selection, note);
  if(note){
    gnome_canvas_part_set_edit_selection(part, g_list_append(NULL, note));
  }
}
#endif

const MidiNote*
gnome_canvas_part_pick_velocity_bar(GnomeCanvasPart* part, Ptd p, gboolean* at_top)
{
  // @param p:       coords relative to top left of the velocity chart area.
  // @param at_top:  set to NULL if you dont care.

#ifndef DEBUG_TRACKCONTROL
  MidiNote* note = NULL;
  while((note = am_midi_part__get_next_event(part->gpart, note))){
    guint start = note->start;
    guint x = arr_px2samples(part->arrange, p.x);
    guint bar_width = arr_px2samples(part->arrange, part->velocity_bar_width);
    if(x > start && x < start + bar_width){
      ArrTrk* trk = track_list__lookup_track(part->arrange->priv->tracks, part->gpart->track);
      int chart_height = ((ArrTrackMidi*)trk)->velocity_chart_height;
      int bar_top = chart_height - (note->velocity * chart_height) / 128;
      if(at_top){ if(p.y > bar_top && p.y < bar_top + ARR_PICK_SIZE) *at_top = TRUE; else *at_top = FALSE; }
      return note;
    }
  }
#endif
  return NULL;
}


void
gnome_canvas_part_text2pic(GnomeCanvasPart* canvas_part)
{
	// Render text string to a bitmap suitable for display in a Part.

	// 1-render the full text onto a pixmap. //FIXME actually it appears that if part if small, not all text is copied.
	// 2-create a new pixbuf and copy the complete pixmap onto it.
	//  -the clipped text is not copied to the canvas item until later.

	Arrange* arrange = canvas_part->arrange;
	AMPart* gpart = canvas_part->gpart;
	g_return_if_fail(gpart);

	static GdkColor colour;
	int width = 0;
	int part_width = arr_spos2px(arrange, &gpart->length) - 2;
	int height = arrange->text_height;

	if(part_width < 4){
		//this isnt an error. Theres just no point drawing parts smaller than this.
		dbg (2, "part width too small.");
		width = 4;
		//return NULL; //dont return yet, as there may be no label.
	}

	//-----------------------------------------------

	// font rendering

	if (!gpart->name[0]) pwarn ("gpart->name is not set.");

	PangoLayout* p_layout = gtk_widget_create_pango_layout(GTK_WIDGET(arrange->canvas->widget), gpart->name);
	char font_string[64];
	get_font_string(font_string, -1);

	PangoFontDescription* font_desc = pango_font_description_from_string(font_string);
	pango_layout_set_font_description(p_layout, font_desc);

	//versions of pango, eg 0.14 seem to render at a y pos of 4. Other >1.0 versions (RH?) also do this.
	//we need to subract the y val.
	PangoRectangle ink_rect;
	PangoRectangle logical_rect;
	pango_layout_get_pixel_extents(p_layout, &ink_rect, &logical_rect);
	//pango_layout_get_extents(p_layout, &ink_rect, &logical_rect); //no, these vals are huge.
	#define FIX 2 //It actually seems to work better if we ignore ink_rect.y, but maybe its just a coincidence..?
	int h = ink_rect.height;// - ink_rect.y + FIX;
	arrange->text_height = height = MAX(h, arrange->text_height);
	width = CLAMP(part_width, 1, ink_rect.width + 1);
	if(!width || !height){ perr ("bad dimensions: w=%i h=%i", width, height); return; }

	//-----------------------------------------------

	// set the background colour
	unsigned ci = gpart->bg_colour;
	if(ci > 63) ci = 0;
	am_palette_lookup(song->palette, &colour, ci);

	GdkColormap* colormap = gdk_colormap_get_system();
	if(!colormap){ perr ("colormap NULL"); return; }

	// we use the pixmap as a gdkdrawable to render onto.
	GdkPixmap* pixmap = ((PartPrivate*)canvas_part->private)->label_pixmap = gdk_pixmap_new(NULL/*drawable*/, width, height, display_depth);

	gdk_drawable_set_colormap(GDK_DRAWABLE(pixmap), colormap);

	gdk_drawable_set_colormap(GDK_DRAWABLE(pixmap), colormap);

	//FIXME this should be free'd ? - as we're modifying the gc, maybe we do need to make a new one, as we are doing?
	GdkGC* gc = gdk_gc_new(GDK_DRAWABLE(pixmap)); //default settings.
	//GdkGC *gc = style->base_gc[GTK_STATE_SELECTED];

	//GdkGC *gc = arrange->canvas_widget->style->fg_gc;//[GTK_STATE_NORMAL];
	//gdk_gc_set_foreground(gc, &color);
	gdk_gc_set_rgb_fg_color(gc, &colour);//this avoids having to 'allocate' the color.

	//clear the pixmap:
	gdk_draw_rectangle(GDK_DRAWABLE(pixmap), gc, TRUE, 0,0, width, height);
	//gdk_pixbuf_fill(pixbuf, palette_get_rgba(gpart->colour, 0xff));

	//set text color:
	am_rgba_to_gdk(&colour, gpart->fg_colour);
	gdk_gc_set_rgb_fg_color(gc, &colour);

	//-----------------------------------------------

	//render the pango layout onto the drawable:
	gdk_draw_layout(GDK_DRAWABLE(pixmap),
	                gc,
	                0, //gint x
	                -ink_rect.y, //gint y
	                p_layout);

	//-----------------------------------------------

	//put the pixmap onto a pixbuf:
	if(canvas_part->label_pixbuf){
		//pixbufs cant be resized, so we delete the old one and start again:
		g_object_unref(canvas_part->label_pixbuf);
	}
	canvas_part->label_pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE, BITS_PER_PIXEL, width, height);
	/*
	void on_dispose(gpointer data, GObject* where_the_object_was)
	{
		PF0;
	}
	g_object_weak_ref((GObject*)canvas_part->label_pixbuf, on_dispose, NULL);
	*/

	gdk_pixbuf_get_from_drawable(canvas_part->label_pixbuf, GDK_DRAWABLE(pixmap), colormap,
	                             0,0,       //int src_x, int src_y
	                             0,0,       //int dest_x, int dest_y
	                             width, height);
	g_object_unref(p_layout);

	//-----------------------------------------------

#ifdef DOESNT_WORK
	//testing:
	cairo_format_t format = (gdk_pixbuf_get_n_channels(canvas_part->label_pixbuf) == 3) ? CAIRO_FORMAT_RGB24 : CAIRO_FORMAT_ARGB32;
	height = 10;
	cairo_surface_t* surface = cairo_image_surface_create_for_data(gdk_pixbuf_get_pixels(canvas_part->label_pixbuf), format, width, height, gdk_pixbuf_get_rowstride(canvas_part->label_pixbuf));
	cairo_t*         cr      = cairo_create(surface);
	pwarn("testing! cr=%p", cr);

	cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
	cairo_rectangle(cr, /*height-*/2, 2, 20/*width*/, 4);
	cairo_fill (cr);
	cairo_stroke (cr);

	cairo_destroy(cr);
	cairo_surface_destroy(surface);
#endif

#if 0
	DRect pts = {0, height-2, width, height-2};
	GdkColor color = {0xffff, 0x9999, 0x0000, 0};
	pixbuf_draw_line(canvas_part->label_pixbuf, &pts, 1.0, &color);
	pts.y1 =  pts.y2 = 4;
	pixbuf_draw_line(canvas_part->label_pixbuf, &pts, 1.0, &color);
#endif
}


static int
g_canvas_part_get_min_n_tiers(Arrange* arrange)
{
  //because of part borders, samples_per_pix may slightly higher, so this function is slightly pesimistic

  int n_tiers_needed = WF_PEAK_RATIO / arr_samples_per_pix(arrange);
  return n_tiers_needed;
}


static void
part_try_set_peak_tile (GnomeCanvasPart* part, PeakTile* tile)
{
	//render overview directly onto the part pixbuf.

	Arrange* arrange = part->arrange;
	g_return_if_fail(arrange);
	if(!SHOW_PARTCONTENTS) return;

	AMPart* gpart = part->gpart;
	g_return_if_fail(gpart);
	g_return_if_fail(gpart->pool_item);
	g_return_if_fail(tile);

	if(gpart->pool_item->state != AM_POOL_ITEM_OK){ if(_debug_ > 1) pwarn("pool item not valid. ignoring..."); return; }

	gboolean draw = TRUE;

	GdkPixbuf* pixbuf = part->pixbuf;
	g_return_if_fail(pixbuf);

	int part_width = gdk_pixbuf_get_width(pixbuf);
	if(part_width < 4){
		draw = FALSE;
		return;
	}

	int height = gdk_pixbuf_get_height(pixbuf);
	if(height < MIN_OVERVIEW_HEIGHT) draw = FALSE;

	//draw the waveform directly onto the pixbuf:

	if(draw){
		// x scaling:
#if 1
		//alternative scaling method taking into account the part border. Scaling is slightly smaller than that of the canvas.
		double samples_per_px = ayyi_pos2samples(&gpart->length) / part_width;
#else
		//this is the canvas scaling that would be used if part border was 0.
		float onepx = beats2samples(1) / PX_PER_BEAT;   //units = samples.
		double samples_per_px = onepx / arrange->hzoom; //constant multiplier setting horizontal magnification.
#endif
		Waveform* w = (Waveform*)part->gpart->pool_item;

		int peakbuf_block_num = 0;
		bool hi_res_mode = ((samples_per_px / WF_PEAK_RATIO) < 1.0);
		if (hi_res_mode) {

			//how many tiers of audio data do we need, and are they loaded?
			//int n_tiers_needed = WF_PEAK_RATIO / samples_per_px;
			int n_tiers_needed = g_canvas_part_get_min_n_tiers(arrange);
			int n_blocks = 1;
			peakbuf_block_num = gnome_canvas_part_get_peak_block_num(part, tile, &n_blocks);
			dbg(2, "tile=%i n_tiers_needed=%i block=%i", tile->index, n_tiers_needed, peakbuf_block_num);

			//we dont need this?? can trigger via request for peakbuf?
			waveform_load_audio(w, peakbuf_block_num, n_tiers_needed, NULL, NULL);
		}

		Peakbuf* peakbuf = hi_res_mode ? waveform_get_peakbuf_n(w, peakbuf_block_num) : NULL;
		if(!hi_res_mode || (peakbuf && peakbuf->buf[0])){
			//dbg(0, "calling part_set_peak_tile()... tile=%i", tile->index);
			part_set_peak_tile(part, tile);
		}
		else dbg(1, "no peakdata. waiting for callback...");

		tile->dirty = FALSE;
	}
}


static void
part_set_peak_tile(GnomeCanvasPart* part, PeakTile* tile)
{
	//render a single tile (256px) for the given part.
	//peakbuf must be valid before calling this!

	AMPart* gpart = part->gpart;
	g_return_if_fail(gpart);
	GdkPixbuf* pixbuf = part->pixbuf;

	// set colours
	GdkColor fg_colour; am_rgba_to_gdk(&fg_colour, gpart->fg_colour);
	uint32_t bg_colour = song->palette[gpart->bg_colour];

	int px_start =      tile->index      * PEAK_TILE_SIZE;
	int px_stop  = MIN((tile->index + 1) * PEAK_TILE_SIZE, gnome_canvas_part_get_width_pixels(part));
	dbg(2, "%i: partsize=%ipx start=%i end=%i", tile->index, gnome_canvas_part_get_width_pixels(part), px_start, px_stop);

	AyyiRegion* region = (AyyiRegion*)gpart->ayyi;
	uint32_t src_inset = ayyi_region__get_start_offset(region);

	int part_width = gdk_pixbuf_get_width(pixbuf);
	double samples_per_px = ayyi_pos2samples(&gpart->length) / part_width;

	GdkColor bg_gdk; am_palette_lookup(song->palette, &bg_gdk, gpart->bg_colour);
	GdkColor fg_ = fg_colour;
	colour_mix(&fg_, &bg_gdk, 30);
	waveform_peak_to_pixbuf_full((Waveform*)gpart->pool_item, pixbuf, src_inset, &px_start, &px_stop, samples_per_px, am_gdk_to_rgba(&fg_), bg_colour, part->arrange->peak_gain, false);
	fg_ = fg_colour;
	colour_mix(&fg_, &bg_gdk, 15);
	waveform_rms_to_pixbuf      ((Waveform*)gpart->pool_item, pixbuf, src_inset / WF_PEAK_RATIO, &px_start, &px_stop, samples_per_px, am_gdk_to_rgba(&fg_colour), bg_colour, part->arrange->peak_gain);
}


static double
part_get_width_pix_nz (AMPart* part)
{
	g_return_val_if_fail(part, 0.0);

	AyyiRegionBase* region = part->ayyi;
	g_return_val_if_fail(region, 0.0);

	return samples2px_nz(region->length);
}


static double
arr_get_part_core_width_pix_zoomed (Arrange* arrange, AMPart* part)
{
	//returns the *zoomed* width of a part in pixels.

	//note: As this fn currently uses the songcore length, we cannot use it during operations such
	//      as resizing where the songcore is not uptodate.

	double len = part_get_width_pix_nz(part);
	return len * ((AyyiPanel*)arrange)->zoom->value.pt.x;
}


#ifdef NEVER
static Peakbuf*
peakbuf_get_for_canvaspart(GnomeCanvasPart* part, int block_num)
{
	Peakbuf* peakbuf = get_peakbuf_n(part->gpart->pool_item, block_num);
	if(peakbuf_is_empty) {
		dbg(0, "not initialised. block_num=%i", block_num);
		pool_item__get_audio_async(part->gpart->pool_item, block_num, g_canvas_part_get_min_n_tiers(part->arrange));
	}
	return peakbuf;
}
#endif

