/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * gimpaction.c
 * Copyright (C) 2004 Michael Natterer <mitch@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include "gimpaction.h"


enum
{
  PROP_0,
  PROP_COLOR,
  PROP_VIEWABLE
};


static void   gimp_action_init          (GimpAction      *action);
static void   gimp_action_class_init    (GimpActionClass *klass);

static void   gimp_action_finalize      (GObject         *object);
static void   gimp_action_set_property  (GObject         *object,
                                         guint            prop_id,
                                         const GValue    *value,
                                         GParamSpec      *pspec);
static void   gimp_action_get_property  (GObject         *object,
                                         guint            prop_id,
                                         GValue          *value,
                                         GParamSpec      *pspec);
static void   gimp_action_connect_proxy (GtkAction       *action,
                                         GtkWidget       *proxy);
static void   gimp_action_set_proxy     (GimpAction      *action,
                                         GtkWidget       *proxy);


static GtkActionClass *parent_class = NULL;


GType
gimp_action_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info =
      {
        sizeof (GimpActionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gimp_action_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        sizeof (GimpAction),
        0, /* n_preallocs */
        (GInstanceInitFunc) gimp_action_init,
      };

      type = g_type_register_static (GTK_TYPE_ACTION,
                                     "GimpAction",
                                     &type_info, 0);
    }

  return type;
}

static void
gimp_action_class_init (GimpActionClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkActionClass *action_class = GTK_ACTION_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize      = gimp_action_finalize;
  object_class->set_property  = gimp_action_set_property;
  object_class->get_property  = gimp_action_get_property;

  action_class->connect_proxy = gimp_action_connect_proxy;
}

static void
gimp_action_init (GimpAction *action)
{
}

static void
gimp_action_finalize (GObject *object)
{
  GimpAction *action = GIMP_ACTION (object);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gimp_action_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  GimpAction *action = GIMP_ACTION (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gimp_action_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  GimpAction *action    = GIMP_ACTION (object);
  gboolean    set_proxy = FALSE;

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }

  if (set_proxy)
    {
      GSList *list;

      for (list = gtk_action_get_proxies (GTK_ACTION (action));
           list;
           list = g_slist_next (list))
        {
          gimp_action_set_proxy (action, list->data);
        }
    }
}

static void
gimp_action_connect_proxy (GtkAction *action,
                           GtkWidget *proxy)
{
  GTK_ACTION_CLASS (parent_class)->connect_proxy (action, proxy);

  gimp_action_set_proxy (GIMP_ACTION (action), proxy);
}


/*  public functions  */

GimpAction *
gimp_action_new (const gchar *name,
                 const gchar *label,
                 const gchar *tooltip,
                 const gchar *stock_id)
{
  return g_object_new (GIMP_TYPE_ACTION,
                       "name",     name,
                       "label",    label,
                       "tooltip",  tooltip,
                       "stock_id", stock_id,
                       NULL);
}

gint
gimp_action_name_compare (GimpAction  *action1,
                          GimpAction  *action2)
{
  return strcmp (gtk_action_get_name ((GtkAction *) action1),
                 gtk_action_get_name ((GtkAction *) action2));
}


/*  private functions  */

static void
gimp_action_set_proxy (GimpAction *action,
                       GtkWidget  *proxy)
{
  if (! GTK_IS_IMAGE_MENU_ITEM (proxy))
    return;

  if (action->color)
    {
      GtkWidget *area;

      area = gtk_image_menu_item_get_image (GTK_IMAGE_MENU_ITEM (proxy));

      if (area && ! GIMP_IS_COLOR_AREA (area))
        {
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (proxy), NULL);
          area = NULL;
        }

      if (! area)
        {
          GdkScreen   *screen   = gtk_widget_get_screen (proxy);
          GtkSettings *settings = gtk_settings_get_for_screen (screen);
          gint         width, height;

          area = gimp_color_area_new (action->color,
                                      GIMP_COLOR_AREA_SMALL_CHECKS, 0);
          gimp_color_area_set_draw_border (GIMP_COLOR_AREA (area), TRUE);

          gtk_icon_size_lookup_for_settings (settings, GTK_ICON_SIZE_MENU,
                                             &width, &height);

          gtk_widget_set_size_request (area, width, height);
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (proxy), area);
          gtk_widget_show (area);
        }
      else
        {
          gimp_color_area_set_color (GIMP_COLOR_AREA (area), action->color);
        }
    }
  else if (action->viewable)
    {
      GtkWidget *view;

      view = gtk_image_menu_item_get_image (GTK_IMAGE_MENU_ITEM (proxy));

      if (view && ! GIMP_IS_VIEW (view))
        {
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (proxy), NULL);
          view = NULL;
        }

      if (! view)
        {
          GdkScreen   *screen   = gtk_widget_get_screen (proxy);
          GtkSettings *settings = gtk_settings_get_for_screen (screen);
          GtkIconSize  size;
          gint         width, height;
          gint         border_width;

          if (GIMP_IS_IMAGEFILE (action->viewable))
            {
              size         = GTK_ICON_SIZE_LARGE_TOOLBAR;
              border_width = 0;
            }
          else
            {
              size         = GTK_ICON_SIZE_MENU;
              border_width = 1;
            }

          gtk_icon_size_lookup_for_settings (settings, size, &width, &height);

          view = gimp_view_new_full (action->viewable,
                                     width, height, border_width,
                                     FALSE, FALSE, FALSE);
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (proxy), view);
          gtk_widget_show (view);
        }
      else
        {
          gimp_view_set_viewable (GIMP_VIEW (view), action->viewable);
        }
    }
  else
    {
      GtkWidget *image;

      image = gtk_image_menu_item_get_image (GTK_IMAGE_MENU_ITEM (proxy));

      if (image && (GIMP_IS_VIEW (image) || GIMP_IS_COLOR_AREA (image)))
        {
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (proxy), NULL);
          g_object_notify (G_OBJECT (action), "stock-id");
        }
    }
}
