/* BEAST - Bedevilled Audio System
 * Copyright (C) 2004 Tim Janik
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


static BstDBSetup*
bst_db_setup_get_default (void)
{
  static BstDBSetup *dbsetup = NULL;
  if (!dbsetup)
    {
      static const GxkSplinePoint beast_db_points[] = {
        { +12, 0 },
        {  +6, 1 },
        {  +3, 2 },
        {  +0, 3 },
        {  -3, 4 },
        {  -6, 5 },
        { -12, 6 },
        { -24, 7 },
        { -48, 8 },
        { -96, 9 },
      };
      guint n_points = G_N_ELEMENTS (beast_db_points);
      const GxkSplinePoint *points = beast_db_points;
      GxkSpline *spline = gxk_spline_new_natural (n_points, points);
      dbsetup = bst_db_setup_new (spline, points[0].x, points[n_points - 1].x);
      gxk_spline_free (spline);
    }
  return dbsetup;
}

static int
db_color_pixel_cmp (const void *v1,
                    const void *v2)
{
  const BstDBColor *c1 = v1;
  const BstDBColor *c2 = v2;
  return c1->pixel < c2->pixel ? -1 : c1->pixel > c2->pixel;
}

BstDBSetup*
bst_db_setup_new (GxkSpline* db2pixel_spline, double maxdb, double mindb)
{
	g_return_val_if_fail (db2pixel_spline, NULL);

	guint i, zindex = 0;
	double miny = db2pixel_spline->segs[0].y, maxy = miny;
	for (i = 1; i < db2pixel_spline->n_segs; i++) {
		miny = MIN (miny, db2pixel_spline->segs[i].y);
		maxy = MAX (maxy, db2pixel_spline->segs[i].y);
		if (!db2pixel_spline->segs[i].x)
			zindex = i;
	}
	g_return_val_if_fail (miny == 0, NULL);
	g_return_val_if_fail (miny < maxy, NULL);

	BstDBSetup *dbsetup = g_new0 (BstDBSetup, 1);
	dbsetup->ref_count = 1;
	dbsetup->spline = gxk_spline_copy (db2pixel_spline);
	dbsetup->offset = dbsetup->length = dbsetup->spzoom = 0;
	dbsetup->zero_index = zindex;
	dbsetup->maxdb = maxdb;
	dbsetup->mindb = mindb;

	/* setup colors */
	BstDBColor default_colors[] = {
		{  +6, 0xff0000 },
		{   0, 0xffff00 },
		{  -6, 0x00ff00 },
		{ -96, 0x00c000 },
	};
	dbsetup->n_colors = G_N_ELEMENTS (default_colors);
#ifdef HAVE_GLIB_2_67
	dbsetup->colors = g_memdup2 (default_colors, sizeof (dbsetup->colors[0]) * dbsetup->n_colors);
#else
	dbsetup->colors = g_memdup (default_colors, sizeof (dbsetup->colors[0]) * dbsetup->n_colors);
#endif

	/* setup zoom and sort colors */
	bst_db_setup_relocate (dbsetup, 0, 99, FALSE);

	return dbsetup;
}

void
bst_db_setup_relocate (BstDBSetup* dbsetup, gint offset, gint range, gboolean flipdir)
{
	dbsetup->flipdir = flipdir != FALSE;
	guint i;

	double miny = dbsetup->spline->segs[0].y, maxy = miny;
	for (i = 1; i < dbsetup->spline->n_segs; i++) {
		miny = MIN (miny, dbsetup->spline->segs[i].y);
		maxy = MAX (maxy, dbsetup->spline->segs[i].y);
	}
	dbsetup->offset = offset;
	dbsetup->length = range + 1;
	dbsetup->spzoom = range / maxy;

	/* sort colors in ascending order */
	for (i = 0; i < dbsetup->n_colors; i++)
		dbsetup->colors[i].pixel = bst_db_setup_get_pixel (dbsetup, dbsetup->colors[i].db);
	qsort (dbsetup->colors, dbsetup->n_colors, sizeof (dbsetup->colors[0]), db_color_pixel_cmp);
}

guint
bst_db_setup_get_color (BstDBSetup* dbsetup, double pixel, double saturation)
{
	/* find segment via bisection */
	guint offset = 0, n = dbsetup->n_colors;
	while (offset + 1 < n) {
		guint i = (offset + n) >> 1;
		if (pixel < dbsetup->colors[i].pixel)
			n = i;
		else
			offset = i;
	}

	g_assert (offset == 0 || pixel >= dbsetup->colors[offset].pixel);

	if (pixel >= dbsetup->colors[offset].pixel && offset + 1 < dbsetup->n_colors) {   /* linear interpolation */
		guint c1 = dbsetup->colors[offset].rgb;
		guint c2 = dbsetup->colors[offset + 1].rgb;
		double delta = pixel - dbsetup->colors[offset].pixel;     /* >= 0, see assertion above */
		double range = dbsetup->colors[offset + 1].pixel - dbsetup->colors[offset].pixel; /* >= 0, ascending sort */
		double d2 = delta / range;                                /* <= 1, due to bisection */
		double d1 = 1.0 - d2;
		guint8 red = saturation * (((c1 >> 16) & 0xff) * d1 + ((c2 >> 16) & 0xff) * d2);
		guint8 green = saturation * (((c1 >> 8) & 0xff) * d1 + ((c2 >> 8) & 0xff) * d2);
		guint8 blue = saturation * ((c1 & 0xff) * d1 + (c2 & 0xff) * d2);

		return (red << 16) | (green << 8) | blue;

	} else { /* pixel is out of range on either boundary */
		guint8 red = saturation * ((dbsetup->colors[offset].rgb >> 16) & 0xff);
		guint8 green = saturation * ((dbsetup->colors[offset].rgb >> 8) & 0xff);
		guint8 blue = saturation * (dbsetup->colors[offset].rgb & 0xff);

		return (red << 16) | (green << 8) | blue;
	}
}

BstDBSetup*
bst_db_setup_copy (BstDBSetup *srcdb)
{
	g_return_val_if_fail (srcdb != NULL, NULL);
	g_return_val_if_fail (srcdb->ref_count > 0, NULL);

#ifdef HAVE_GLIB_2_67
	BstDBSetup *dbsetup = g_memdup2 (srcdb, sizeof (srcdb[0]));
#else
	BstDBSetup *dbsetup = g_memdup (srcdb, sizeof (srcdb[0]));
#endif
	dbsetup->spline = gxk_spline_copy (srcdb->spline);
#ifdef HAVE_GLIB_2_67
	dbsetup->colors = g_memdup2 (srcdb->colors, sizeof (dbsetup->colors[0]) * dbsetup->n_colors);
#else
	dbsetup->colors = g_memdup (srcdb->colors, sizeof (dbsetup->colors[0]) * dbsetup->n_colors);
#endif
	dbsetup->ref_count = 1;

	return dbsetup;
}

BstDBSetup*
bst_db_setup_ref (BstDBSetup* dbsetup)
{
	g_return_val_if_fail (dbsetup != NULL, NULL);
	g_return_val_if_fail (dbsetup->ref_count > 0, NULL);
	dbsetup->ref_count += 1;
	return dbsetup;
}

void
bst_db_setup_unref (BstDBSetup* dbsetup)
{
	g_return_if_fail (dbsetup != NULL);
	g_return_if_fail (dbsetup->ref_count > 0);

	dbsetup->ref_count -= 1;

	if (!dbsetup->ref_count) {
		gxk_spline_free (dbsetup->spline);
		dbsetup->spline = NULL;
		g_free (dbsetup->colors);
		g_free (dbsetup);
	}
}

double
bst_db_setup_get_pixel (BstDBSetup *dbsetup, double dbvalue)
{
	double pixel = gxk_spline_y (dbsetup->spline, dbvalue) * dbsetup->spzoom;
	if (dbsetup->flipdir)
		pixel = (dbsetup->length - 1) - pixel;
	return pixel + dbsetup->offset;
}

double
bst_db_setup_get_dbvalue (BstDBSetup *dbsetup, double pixel)
{
	pixel -= dbsetup->offset;
	if (dbsetup->flipdir)
		pixel = (dbsetup->length - 1) - pixel;
	return gxk_spline_findx (dbsetup->spline, pixel / dbsetup->spzoom);
}


static void
db_setup_size_allocate (BstDBSetup* dbsetup, gint thickness, gint border, gint length, gboolean vertical)
{
	border = MAX (border, 1);             /* account for the 1 pixel shadow of labeling's max/min lines */
	border = MAX (border, thickness);     /* account for outer shadow of dbbeam */
	gint size = length - 2 * border;
	size -= 1;                            /* account for length = range + 1 pixels */
	bst_db_setup_relocate (dbsetup, border, MAX (size, 0), !vertical);
}

