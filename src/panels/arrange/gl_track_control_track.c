/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2010-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "agl/behaviours/cache.h"
#include "icon.h"

#define USE_VBO
#define VEC2 2

extern TrackActor* arr_gl_tracks_find_track (AGlActor*, ArrTrk*);

static void gl_track_control_track_set_text (TrackControlTrackActor*);
static void gl_track_control_track_free     (AGlActor*);
static bool track_on_event                  (AGlActor*, GdkEvent*, AGliPt);

static AGlActorClass track_control_track_class = {0, "TrackControlTrack", NULL, gl_track_control_track_free};

static unsigned int tct_vao = 0;


static void
resize_xy_to_val (ViewObject* obj, AGliPt xy, AGliPt offset)
{
	g_return_if_fail(obj);

	am_object_val(obj->model).i = xy.y;

	TrackControlTrackActor* ta = obj->user_data;
	Arrange* arrange = ta->track->arrange;
	ta->track->height = MAX(MIN_TRACK_HEIGHT, am_object_val(obj->model).i / vzoom(arrange));

	track_control_actor_layout(CGL->zoom, (AMVal){0,}, arrange);
	agl_actor__invalidate((AGlActor*)ta);

	arr_on_view_changed(arrange, AM_CHANGE_ZOOM_V);
}


static AGlActor*
track_control_track_actor_new (Arrange* arrange, ArrTrk* track)
{
	void _set_height (AMObject* obj, int property, AMVal value, AyyiHandler2 _, gpointer user_data)
	{
		AMTrack* track = ((TrackControlTrackActor*)obj->user_data)->track->track;
		arr_track_set_height(NULL, track, value.i);
	}

	TrackControlTrackActor* ta = agl_actor__new(TrackControlTrackActor,
		.actor = {
			.parent = {
				.class = &track_control_track_class,
				.name = g_strdup(track->track->name),
				.program = agl->shaders.plain,
				.on_event = track_on_event,
			},
			.n_objs = OBJ_MAX
		},
		.track = track,
		.height = {
			.set = _set_height,
		}
	);
	ta->height.user_data = ta;
	AGlActor* a = (AGlActor*)ta;

	a->behaviours[0] = cache_behaviour();

	int height = track->height * vzoom(arrange);

	void resize_change_done (ViewObject* obj, ArrTrk* trk, AyyiSongPos* pos, AyyiHandler2 h, gpointer u)
	{
		AMVal val;
		songpos_ayyi2gui(&val.pos, pos);
		obj->model->set(obj->model, 0, val, h, u);
	}

	resize_class.change_done = resize_change_done; // dont use. will be removed. see _set_height instead.
	resize_class.xy_to_val = resize_xy_to_val;

	view_object_init(&resize_class, &((VOActor*)ta)->objs[OBJ_RESIZE], (ViewObject){
		.model = &ta->height,
		.region = (iRegion){0, height - 2, 1024, height},
		.user_data = ta,
	});

	{
		void _drag_motion (ViewObject* obj, AGliPt xy, AGliPt offset)
		{
																			// TODO why is this called when mouse apparently has not moved ?
			TrackControlTrackActor* ta = obj->user_data;
			AGlActor* actor = (AGlActor*)ta;
			Arrange* arrange = ta->track->arrange;

			if(!drag.pressed){
				drag.pressed = actor;
				drag.orig.y = xy.y;
			}

			if(ABS(xy.y - drag.orig.y) > 4){
				set_cursor(arrange->canvas->widget->window, CURSOR_HAND_OPEN);
				TrackDispNum track_d;
				arrange->canvas->pick(arrange, xy.x, /*actor->region.y1 +*/ xy.y, &track_d, NULL);
				if(track_d > -1){
					ArrTrk* atr = track_list__track_by_display_index(arrange->priv->tracks, track_d);
					if(atr != ta->track && atr != drag.new.atr){
						dbg(1, "track changed %i: (%s)", track_list__get_display_num(arrange->priv->tracks, atr->track), atr->track->name);
						drag.new.atr = atr;
					}
				}
			}
		}

		void _set_drag (AMObject* obj, int property, AMVal value, AyyiHandler2 _, gpointer user_data)
		{
			PF;
			g_return_if_fail(drag.pressed);

			TrackControlTrackActor* ta = obj->user_data;
			Arrange* arrange = ta->track->arrange;
			TrackList* tl = arrange->priv->tracks;
			ArrTrk* atr = drag.new.atr;
			if(atr && (atr != ta->track)){
				dbg(1, ">> new track: %i (%s)", am_track_list_position(song->tracks, atr->track), atr->track->name);

				arr_track_move_to_pos(arrange, track_list__get_display_num(tl, ta->track->track), track_list__get_display_num(tl, atr->track));
			}
			drag.pressed = NULL;
			drag.new.atr = NULL;
			set_cursor(arrange->canvas->widget->window, CURSOR_NORMAL);
		}

		drag_class.xy_to_val = _drag_motion;

		am_object_init(&ta->drag, 1);
		ta->drag.set = _set_drag;
		ta->drag.user_data = ta;

		view_object_init(&drag_class, &((VOActor*)ta)->objs[OBJ_DRAG], (ViewObject){
			.model = &ta->drag,
			.region = (iRegion){0, 0, 1024, height - 3},
			.user_data = ta,
		});
	}

#ifdef AGL_ACTOR_RENDER_CACHE
	#define INVALIDATE agl_actor__invalidate(actor)
#else
	#define INVALIDATE ;
#endif
	void track_arm_toggle  (AGlActor* actor, gpointer at) { am_track__arm(((ArrTrk*)at)->track, !am_track__is_armed(((ArrTrk*)at)->track), NULL, NULL); INVALIDATE; }
	void track_solo_toggle (AGlActor* actor, gpointer at) { AMTrack* tr = ((ArrTrk*)at)->track; am_track__solo(tr, !am_track__is_solod(tr), NULL, NULL);INVALIDATE;  }
	void track_mute_toggle (AGlActor* actor, gpointer at) { am_track__mute(((ArrTrk*)at)->track, !am_track__is_muted(((ArrTrk*)at)->track), NULL, NULL);INVALIDATE;  }

	bool get_armd_state (AGlActor* a, gpointer at) { return am_track__is_armed(((ArrTrk*)at)->track); }
	bool get_mute_state (AGlActor* a, gpointer at) { return am_track__is_muted(((ArrTrk*)at)->track); }
	bool get_solo_state (AGlActor* a, gpointer at) { return am_track__is_solod(((ArrTrk*)at)->track); }

	AGlActor* button = agl_actor__add_child(a, gl_button(ICON_REC, track_arm_toggle, get_armd_state, track));
	button->region = (AGlfRegion){0, 0, 16, height};

	button = agl_actor__add_child(a, gl_button(ICON_SOLO, track_solo_toggle, get_solo_state, track));
	button->region = (AGlfRegion){16, 0, 16 + 16, height};

	button = agl_actor__add_child(a, gl_button(ICON_MUTE, track_mute_toggle, get_mute_state, track));
	button->region = (AGlfRegion){32, 0, 32 + 16, height};

	bool track_control_track_paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		TrackControlActor* parent = (TrackControlActor*)actor->parent;
		TrackControlTrackActor* ta = (TrackControlTrackActor*)actor;
		ArrTrk* at = ta->track;
		AMTrack* track = at->track;

		double total_width = agl_actor__width(actor);
		double width = MAX(0, (total_width - 3. * 16.) / 2.);
		double widths[] = {width * 5. / 4., width * 3. / 4.};

		double x1 = 16. * 3.;
		double x2 = x1 + widths[0];

		bool track_is_selected = arr_track_is_selected(arrange, track);

		int tcolour = am_track__get_colour(track);
		uint32_t colour = track_is_selected
			? parent->selected_bg
			: tcolour > -1 ? song->palette[tcolour] : parent->default_bg;

		// the button background colour is determined by its parent, so it is set here.
		GList* l = ((AGlActor*)ta)->children;
		for (;l;l=l->next) {
			AGlActor* b = l->data;
			((ButtonActor*)b)->bg_colour = (colour & 0xffffff00) + ((int)(0xff * BG_OPACITY));
		}

		if (!(actor->region.x2 > x1)) return true;

		double height = at->height * CGL->zoom->value.pt.y;

		// track control background
		x1 = 16.0 * 3.0;
		x2 = 16.0 * 3.0 + widths[0] + widths[1];
		if (agl->use_shaders) {
#if 0
			agl->shaders.plain->uniform.colour = (ta->animatable.val.f > 0.01)
				? colour_mix_rgba(colour, colour_lighter_rgba(colour, 16), ta->animatable.val.f)
				: colour;
			agl->shaders.plain->uniform.colour = (agl->shaders.plain->uniform.colour & 0xffffff00) + 0xff * BG_OPACITY;
			agl_use_program(agl->shaders.plain);
#else
			// modulating alpha instead of colour has the advantage that it works with white
			// but the effect is stronger on some colours more than others
			int alpha = 0xff * (BG_OPACITY + (1 - BG_OPACITY) * ta->hover_opacity / 2);
			agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], (colour & 0xffffff00) + alpha);
#endif

			glBlendFuncSeparate (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			agl_use_va (tct_vao);
			glBindBuffer (GL_ARRAY_BUFFER_ARB, ta->vbo_id);
			glEnableVertexAttribArray (0);
			glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
			glDrawArrays (GL_QUADS, 0, 4);
		} else {
			agl_enable(AGL_ENABLE_BLEND /* !AGL_ENABLE_TEXTURE_2D */);

			AGlColourFloat cf;
			colour_rgba_to_float(&cf, track_is_selected ? parent->selected_bg : parent->default_bg);
			glColor4f(cf.r, cf.g, cf.b, BG_OPACITY);

			glRectf(x1, 0.0, x2, height);
		}

		// track name

		x2 = x1 + widths[0];

		agl_push_clip(x1, 0., widths[0], 10000.f);

		/*
		 *  We cannot vertically centre using layout height because
		 *  the text is not centred in the layout.
		 */
		int bl = PANGO_PIXELS(pango_layout_get_baseline(ta->layout));
		int yb = (height + 16) / 2 - 1;
		PangoRectangle logical_rect;
		pango_layout_get_pixel_extents(ta->layout, NULL, &logical_rect);
		// changed to using text colour instead of fg colour but it does not look as good - contrast too high
		// - TODO is this true generally?
		#define FIX 1
		uint32_t textcolour = get_style_text_color_rgba(GTK_STATE_NORMAL);
		agl_pango_show_layout(ta->layout, x1 + 8, yb - bl - logical_rect.y - FIX, 0.0, textcolour);

		agl_pop_clip();

		// output name
		{
			x1 = x2;
			x2 += widths[1];

			agl_push_clip (x1, 0., widths[1], height);

			pango_layout_get_pixel_extents(ta->output_layout, NULL, &logical_rect);
			bl = PANGO_PIXELS(pango_layout_get_baseline(ta->output_layout));

			//textcolour = get_style_text_color_rgba(GTK_STATE_NORMAL);
			agl_pango_show_layout(ta->output_layout, x1 + 8, yb - bl - logical_rect.y, 0.0, textcolour);
			agl_pop_clip();
		}

		if (AM_TRK_IS_MIDI(track) && track_is_selected) {
			ArrTrackMidi* atm = (ArrTrackMidi*)at;
			if (agl->use_shaders) {
				PLAIN_COLOUR2 (agl->shaders.plain) = 0xffffffff;
				agl_use_program (agl->shaders.plain);
			}
			agl_rect_((AGlRect){x2 - 48, 0, 48, height});

			int kh = atm->note_height * 12 / 7;
			int bottom_note = arr_track_get_note_num_from_y(atm, height - 1);
			if (bottom_note < 0) return false;
			int offset = note_is_black(bottom_note) ? kh / 2 : 0;

			agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], 0x000000ff);
			int height = at->height * vzoom(arrange);
			int n_notes = arr_track_midi_get_n_notes(atm);
			for (int i=0;i<n_notes * 7 / 12 + 2;i++) {
				glRecti(x2 - 48, height -offset - kh * i, x2, height -offset - kh * i + 1);
			}

			int black[] = {0, 1, 3, 4, 5};
			//              C  C# D  Eb E  F  F# G  Ab A  Bb B
			int start[] = { 0, 0, 0, 1, 1, 2, 2, 2, 3, 4, 4, 4}; // for each note num, the note offset to the start of the black keys sequence
			int st   [] = {-1, 0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5}; // increasing value moves black notes up
			int s = start[bottom_note % 12];
			#define OCTAVE() (7 * ((i + s) / 5))
			for (int i=0;true;i++) {
				float y = height - offset + kh * st[bottom_note%12] - kh * OCTAVE() - kh * black[(i + s) % 5];
				if(y < 0) break;
				float w = ((float)kh) / 3.;
				glRectf(x2 - 48, y - w, x2 - 18, y + w);
			}

			//         C  C# D  Eb E  F  F# G  Ab A  Bb B
			int p[] = {0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6};
			if (atm->hover_note) {
				agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], 0x00000033);
				if (!note_is_black(atm->hover_note)) {
					int y = height - kh * p[atm->hover_note - bottom_note];
					glRecti(x2 - 48, y, x2, y - kh);
				}
			}

#if 0
			agl_print(x2 -16, arr_track_note_pos_y(at, &(MidiNote){.note=60}) + 1, 0.0, 0x000000ff, "C4");
#else
			int n_white_notes_difference (int a, int b)
			{
				//         C  C# D  Eb E  F  F# G  Ab A  Bb B
				int l[] = {0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6};
				return 7 * (a/12 - b/12) + l[a%12] - l[b%12];
			}
			int yy = height -kh + offset - n_white_notes_difference(60, bottom_note) * kh;
			agl_print(x2 - 14, yy + 2, 0.0, 0x000000ff, "C4");
#endif
		}
		return true;
	}

	void gl_track_control_track_init (AGlActor* actor)
	{
		TrackControlTrackActor* ta = (TrackControlTrackActor*)actor;

		glGenBuffers(1, &ta->vbo_id);

		if (!tct_vao) glGenVertexArrays(1, &tct_vao);

		PangoFontDescription* font_desc = pango_font_description_new();
		pango_font_description_set_family(font_desc, "Sans");

		{
			ta->layout = pango_layout_new (agl_pango_get_context());

			pango_font_description_set_size(font_desc, 10 * PANGO_SCALE);
			pango_font_description_set_weight(font_desc, PANGO_WEIGHT_SEMIBOLD);

			/* TODO
			// for some reason there seems to be an issue with pixmap fonts
			if(!font_is_scalable(PGRC, pango_font_description_get_family(font_desc))){
				pango_font_description_set_family(font_desc, family);
			}
			*/
			pango_layout_set_font_description(ta->layout, font_desc);
		}
		{
			ta->output_layout = pango_layout_new(agl_pango_get_context());

			pango_font_description_set_size(font_desc, 7 * PANGO_SCALE);
			pango_font_description_set_weight(font_desc, PANGO_WEIGHT_NORMAL);
			pango_layout_set_font_description(ta->output_layout, font_desc);
		}

		pango_font_description_free (font_desc);
		gl_track_control_track_set_text(ta);

		agl_actor__set_size(actor);
	}

	void track_control_track_set_size (AGlActor* actor)
	{
		// note that the y position is not set here as it depends on the preceding tracks.
		// -in particular, at->y is not valid during track resize operations.

		TrackControlTrackActor* ta = (TrackControlTrackActor*)actor;
		ArrTrk* at = ta->track;
		Arrange* arrange = at->arrange;

		float zoom = vzoom2(arrange);
		int top = at->y * zoom;
		float h = at->height * zoom;

		actor->region = (AGlfRegion){0, top, actor->parent->region.x2, top + h};

		// main button
		if (ta->vbo_id) {
			float width = MAX(0, (agl_actor__width(actor) - 3. * 16.) / 2.);
			float widths[] = {width * 5. / 4., width * 3. / 4.};

			float x1 = 16. * 3.;
			float x2 = 16. * 3. + widths[0] + widths[1];
			float vertices[] = {
				x1, 0.,
				x2, 0.,
				x2, h,
				x1, h,
			};
			agl_use_va (tct_vao);
			glBindBuffer (GL_ARRAY_BUFFER_ARB, ta->vbo_id);
			glBufferData (GL_ARRAY_BUFFER_ARB, G_N_ELEMENTS(vertices) * sizeof(float), vertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray (0);
			glVertexAttribPointer (0, VEC2, GL_FLOAT, GL_FALSE, 0, NULL);
			agl_use_va (1); // check if needed
		}

		((VOActor*)ta)->objs[OBJ_RESIZE].region = (iRegion){0, h - 2, 1024, h};
		((VOActor*)ta)->objs[OBJ_DRAG].region.y2 = h - 3;

		GList* k = actor->children;
		for (;k;k=k->next) {
			AGlActor* button = k->data;
			button->region.y2 = h;
		}
	}

	am_object_init(&ta->height, 1);

	a->paint = track_control_track_paint;
	a->init = gl_track_control_track_init;
	a->set_size = track_control_track_set_size;

	ta->animatable = (WfAnimatable){
		.val.f       = &ta->hover_opacity,
		.start_val.f = 0.0,
		.target_val.f= 0.0,
		.type        = WF_FLOAT
	};

	return a;
}


static void
gl_track_control_track_free (AGlActor* actor)
{
	TrackControlTrackActor* ta = (TrackControlTrackActor*)actor;

	g_object_unref(ta->layout);
	g_object_unref(ta->output_layout);
	glDeleteBuffers(1, &ta->vbo_id);
	if (!actor->class->instances) {
		glDeleteVertexArrays (1, &tct_vao);
	}
	am_object_deinit(&ta->drag);
	am_object_deinit(&ta->height);
	g_free(actor->name);
	g_free(actor);
}


static bool
track_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	TrackControlTrackActor* ta = (TrackControlTrackActor*)actor;
	ArrTrk* at = ta->track;
	Arrange* arrange = at->arrange;

	switch (event->type) {
		case GDK_ENTER_NOTIFY:
			ta->hover_opacity = 1.0;
			agl_actor__start_transition(actor, g_list_append(NULL, &ta->animatable), NULL, NULL);
			break;
		case GDK_LEAVE_NOTIFY:
			ta->hover_opacity = 0.0;
			agl_actor__start_transition(actor, g_list_append(NULL, &ta->animatable), NULL, NULL);
			break;
		case GDK_SCROLL:
			if(AM_TRK_IS_MIDI(at->track) && arr_track_is_selected(arrange, at->track)){
				GdkEventScroll* ev = (GdkEventScroll*)event;

				GdkModifierType state; gdk_event_get_state(event, &state);
				if (state & GDK_SHIFT_MASK) {
					((ArrTrackMidi*)at)->note_height = CLAMP(((ArrTrackMidi*)at)->note_height +
						(ev->direction == GDK_SCROLL_UP ? 1 : -1), 2, 10);
				} else {
					((ArrTrackMidi*)at)->centre_note = CLAMP(((ArrTrackMidi*)at)->centre_note +
						(ev->direction == GDK_SCROLL_UP ? 1 : -1), 0, 127);
				}

				agl_actor__invalidate(actor);
				TrackActor* track = arr_gl_tracks_find_track(CGL->actors[ACTOR_TYPE_TRACKS], at);
				if (track) {
					for (GList* l=((AGlActor*)track)->children;l;l=l->next) {
						agl_actor__invalidate(l->data);
					}
				}
			}
			break;
		default:
			break;
	}
	if (actor__on_viewobject_event(actor, event, xy) == HANDLED) return HANDLED;

	switch (event->type) {
		case GDK_BUTTON_PRESS:
			switch (event->button.button) {
				case 3:
					dbg(1, "right click");
					if (xy.x < 100) {
						arr_track_menu_show(arrange, am_track_list_position(song->tracks, at->track), (GdkEventButton*)event);
					} else {
						if (gui_song.ch_menu) {
							const AMChannel* ch = am_track__lookup_channel(at->track);
							output_menu_update((AyyiPanel*)arrange, ch);
							output_menu_popup();
						}
					}
					break;
				case 1:
					if (at && !arr_track_is_selected(arrange, at->track)) {
						arr_track_select(arrange, at->track);
					}
					break;
				default:
					break;
			}
			return HANDLED;
		case GDK_2BUTTON_PRESS:
			dbg(1, "double click");

			void rename_done (char* new_text, gpointer track)
			{
				PF;
				am_track__set_name((AMTrack*)track, new_text, NULL, NULL);
			}

			AGliPt o = agl_actor__find_offset(actor);
			Pti pos = {16 * 3 + 2, o.y + agl_actor__height(actor) / 2 - 10};
			track_rename_popup(&arrange->panel, arrange->canvas->widget, pos, FALSE, at->track, rename_done, at->track);

			return HANDLED;
		default:
			break;
	}
	return NOT_HANDLED;
}


static void
gl_track_control_track_set_text (TrackControlTrackActor* ta)
{
	char str[256];

	pango_layout_set_text(ta->layout, ta->track->track->name, -1);
	pango_layout_set_text(ta->output_layout, am_track__get_channel_label(ta->track->track, str), -1);
}
