/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2015 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
  ViewObject

  ViewObject is a generic view object to:
   - handle mouse events. First the actor needs to Pick the ViewObject, then it can update the object as it is moved.
   - update the model (AMObject) during and after user changes.
   - reset the model if change cancelled.

  It is designed to simplify the event handler by making behaviour uniform for each type of object.

  The objects are contained within an Actor. The actor, not the view-object is responsible for drawing.

  ViewObject is currently used for: locators, track_controls and song_overview.
  Possible additional object examples:
    - parts,
    - contents of midi parts, automation lines/pts. buttons?

  It remains to be seen whether this object is generally useful and whether the functionality should be
  merged into the Actor. It would be nice if the object could handle management of signals
  (connecting, disconnecting, and responding to).
*/

#ifndef __view_object_h__
#define __view_object_h__
#include "agl/actor.h"
#include "gui_types.h"

typedef struct _ViewObjectClass ViewObjectClass;
typedef struct _ViewObjectActor VOActor;

typedef ViewObject* (*ActorPick) (AGlActor*, AGliPt);

struct _ViewObjectClass
{
    int         cursor;    // for hover

    void        (*xy_to_val)   (ViewObject*, AGliPt, AGliPt offset);
    void        (*change_done) (ViewObject*, ArrTrk*, AyyiSongPos*, AyyiHandler2, gpointer);
    void        (*abort)       (ViewObject*);
};

struct _ViewObject
{
    ViewObjectClass* class;
    AMObject*        model;
    AMVal            orig_val;
    iRegion          region;    // the region to initiate interaction.
    struct {
        char*        text;
    }                hover;
    gpointer         user_data;
};

struct _ViewObjectActor
{
    AGlActor         parent;
    ActorPick        pick;
	int              n_objs;
	ViewObject       objs[];
};

void view_object_init           (ViewObjectClass*, ViewObject*, ViewObject prototype);
void view_object_box            (ViewObject*, int);

bool actor__on_viewobject_event (AGlActor*, GdkEvent*, AGliPt);

#define abort_change(A) A->class->abort(A);
#define view_object_width(A) (A->region.x2 - A->region.x1)

#endif
