//----------------------------------------------------------------------------
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------
#include "config.h"
#include <glib.h>
#include "seq24/sequence.h"

#if 0
static midi_event* sequence__iterator_draw_begin  (Sequence*);
static midi_event* sequence__iterator_draw_next   (Sequence*);
#endif


Sequence*
sequence__new( )
{
	Sequence* seq   = g_new(Sequence, 1);
 
    seq->time_beats_per_measure = 4;
    seq->time_beat_width = 4;
    seq->rec_vol = 0;

    //m_tag           = 0;

/*
    m_name          = c_dummy;
    m_length        = 4 * c_ppqn;
    m_snap_tick     = c_ppqn / 4;
*/

    /* no notes are playing */
/*
    for (int i=0; i< c_midi_notes; i++ )
        m_playing_notes[i] = 0;
*/


/*
    m_dirty_main = true;
    m_dirty_edit = true;
    m_dirty_perf = true;
    m_dirty_names = true;
*/
	return seq;
}

#if 0
static void 
sequence__reset_draw_marker(Sequence* seq)
{
    //lock();
    
    sequence__iterator_draw_begin(seq);

    //unlock();
}


static midi_event*
sequence__iterator_draw_begin(Sequence* seq)
{
	seq->iterator_draw = seq->events;
	return seq->iterator_draw->data;
}
#endif


static midi_event*
sequence__iterator_draw_next(Sequence* seq)
{
	seq->iterator_draw = seq->iterator_draw->next;
	return seq->iterator_draw->data;
}


static midi_event*
sequence__iterator_draw_current(Sequence* seq)
{
	return seq->iterator_draw ? seq->iterator_draw->data : NULL;
}


DrawType
sequence__get_next_note_event (Sequence* seq, long *a_tick_s, long *a_tick_f, int *a_note, gboolean * a_selected, int *a_velocity)
{
	DrawType ret = DRAW_FIN;
	g_return_val_if_fail(seq, ret);
	*a_tick_f = 0;

	midi_event* event;
	//while (  seq->iterator_draw  != m_list_event.end() ) {
	while ((event = sequence__iterator_draw_current(seq))) {
		*a_tick_s   = event->timestamp;
		*a_note     = event->data[0];
		*a_selected = event->selected;
		*a_velocity = event->data[1];

		/* note on, so its linked */
		if( midi_event__is_note_on(event) && event__is_linked(event) ){

			*a_tick_f   = event->linked->timestamp;

			ret = DRAW_NORMAL_LINKED;
			sequence__iterator_draw_next(seq);
			return ret;
		}

		else if( midi_event__is_note_on(event) && (! event__is_linked(event)) ){

			ret = DRAW_NOTE_ON;
			sequence__iterator_draw_next(seq);
			return ret;
		}

		else if( midi_event__is_note_off(event) && (! event__is_linked(event)) ){

			ret = DRAW_NOTE_OFF;
			sequence__iterator_draw_next(seq);
			return ret;
		}

		/* keep going until we hit null or find a NoteOn */
		sequence__iterator_draw_next(seq);
	}
	return DRAW_FIN;
}

