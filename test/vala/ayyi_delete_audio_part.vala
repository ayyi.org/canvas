using GLib;
using Gdk;
using Ayyi;

static int part_idx = 0;
const OptionEntry[] entries = 
{
	{ "part", 'p', 0, OptionArg.INT, ref part_idx, "index of part to delete (use ayyi_show_parts to get list of parts)", "<N>" },
	{ null }
};

public class Ayyi.DeleteAudioPartClient : Ayyi.SimpleClient
{
	public int part_index;

	public static int main(string[] args)
	{
		part_idx = -1;

		OptionContext context = new OptionContext("");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}
		if(part_idx < 0){
			print("part number not specified\n");
			return 1;
		}

		app = new DeleteAudioPartClient();
		DeleteAudioPartClient* client = (DeleteAudioPartClient*)app;
		client->part_index = part_idx;

		if(!app.run(((DeleteAudioPartClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		//as not all regions have AMParts, we use the AyyiRegion.

		AyyiRegion* region = song__audio_region_get(part_index);

		if(!(bool)region){
			print("part not found\n");
			print_region_numbers();
			abort();
			return;
		}

		print("deleting: %i: %s\n", part_index, (string)region->name);

		region__delete(region, delete_done);
	}

	public void delete_done(Ident ident, GLib.Error* error)
	{
		log_print(LogType.OK, "done.");
		AM.Song.save((obj, error) => {
			quit();
		});
	}

	private void print_region_numbers()
	{
		print("available parts:\n");

		AyyiRegion* part = null;
		while((part = ayyi_song__audio_region_next(part)) != null){
			print("%i ", part->shm_idx);
		}
		print("\n");
	}

	public void abort()
	{
		log_print(LogType.FAIL, "aborted.");
		app.quit();
	}
}


