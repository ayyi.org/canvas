/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define OVERVIEW_HEIGHT 24

static AGlActor* arr_gl_song_overview      (GtkWidget* panel);
static void      overview_free             (AGlActor*);
static void      overview_set_size         (AGlActor*);
static void      overview_window_xy_to_val (ViewObject*, AGliPt, AGliPt);

static AGlActorClass overview_class = {0, "Overview", (AGlActorNew*)arr_gl_song_overview, overview_free};


typedef struct {
    VOActor    actor;
	ViewObject window;
    AMObject   position;
} OverviewActor;

static ViewObjectClass overview_view_class = {
	.cursor = CURSOR_HAND_CLOSE,
	.xy_to_val = overview_window_xy_to_val
};


static AGlActor*
arr_gl_song_overview (GtkWidget* panel)
{
	void arr_gl_overview_draw_part (AGlActor* actor, AMPart* part, AGlRect* rect)
	{
		// TODO better to do this in pixels instead?
		unsigned len_samples = ayyi_beats2samples(am_object_val(&song->loc[AM_LOC_END]).sp.beat - am_object_val(&song->loc[AM_LOC_START]).sp.beat);

		rect->x = ayyi_pos2samples(&part->start) * agl_actor__width(actor) / len_samples;
		rect->w = MAX(1, ayyi_pos2samples(&part->length) * agl_actor__width(actor) / len_samples);

		SET_PLAIN_COLOUR (agl->shaders.plain, song->palette[part->bg_colour]);
		agl->shaders.plain->set_uniforms_(agl->shaders.plain);
		agl_rect_(*rect);

		if (am_partmanager__is_selected(part)) {
			SET_PLAIN_COLOUR (agl->shaders.plain, 0xffffffff);
			agl->shaders.plain->set_uniforms_(agl->shaders.plain);

			agl_box (1, rect->x, 0, rect->w, agl_actor__height(actor));
		}
	}

	#define GL_OVERVIEW_POS_TO_X(POS) (ayyi_pos2samples((POS)) * agl_actor__width(actor) / (ayyi_beats2samples(am_object_val(&song->loc[AM_LOC_END]).sp.beat - am_object_val(&song->loc[AM_LOC_START]).sp.beat)))

	bool arr_gl_overview_draw (AGlActor* actor)
	{
		OverviewActor* overview = (OverviewActor*)actor;
		Arrange* arrange = actor->root->user_data;

		SET_PLAIN_COLOUR (agl->shaders.plain, actor->colour);
		agl_use_program((AGlShader*)agl->shaders.plain);
		agl_rect(0, 0, actor->region.x2, agl_actor__height(actor));

		AGlRect rect = {.h = agl_actor__height(actor) / arrange->priv->tracks->display_max};

		for (int t=0; t<AM_MAX_TRK; t++) {
			ArrTrk* track = arrange->priv->tracks->display[t];
			if (!track) break;

			rect.y = t * rect.h;

			FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, track->track);
			AMPart* part;
			while ((part = (AMPart*)filter_iterator_next(i))) {
				arr_gl_overview_draw_part(actor, part, &rect);
			}
			filter_iterator_unref(i);
		}

		for (GList* l=arrange->part_selection;l;l=l->next) arr_gl_overview_draw_part(actor, l->data, &rect);

		SET_PLAIN_COLOUR (agl->shaders.plain, 0x00ff00b0);
		agl->shaders.plain->set_uniforms_(agl->shaders.plain);
		for (int i=0;i<AM_LOC_MAX;i++) {
			if(am_locator_is_set(&song->loc[i])) agl_rect(GL_OVERVIEW_POS_TO_X(&song->loc[i].vals[0].val.sp), 0, 1, agl_actor__height(actor));
		}

		// window outline
		double fx;
		arrange->canvas->get_view_fraction(arrange, &fx, NULL);
		if (fx < 1.0) {
			SET_PLAIN_COLOUR (agl->shaders.plain, 0xff0000b0);
			agl->shaders.plain->set_uniforms_(agl->shaders.plain);

			view_object_box(&overview->window, 2);
		}

		return true;
	}

	uint32_t colour_premultiply_alpha (uint32_t colour)
	{
		uint32_t a = colour & 0x000000ff;
		uint32_t r = (((colour & 0xff000000) / 0xff * a) & 0xff000000);
		uint32_t g = (((colour & 0x00ff0000) / 0xff * a) & 0x00ff0000);
		uint32_t b = (((colour & 0x0000ff00) / 0xff * a) & 0x0000ff00);
		uint32_t out = r + g + b + 0xff;
		return out;
	}

	void overview_init (AGlActor* actor)
	{
		OverviewActor* overview = (OverviewActor*)actor;
		Arrange* arrange = actor->root->user_data;

		if (CGL->actors[ACTOR_TYPE_BACKGROUND]) {
			actor->colour = colour_premultiply_alpha(CGL->actors[ACTOR_TYPE_BACKGROUND]->colour);
		}

		view_object_init(&overview_view_class, &overview->actor.objs[0], (ViewObject){
			.model = &overview->position,
			.user_data = actor
		});

		am_object_init(&overview->position, 1);
		overview->position.vals[0].type = G_TYPE_INT;

    	void gl_overview_set_position (AMObject* obj, int property, AMVal val, AyyiHandler2 handler, gpointer user_data)
		{
			g_return_if_fail(val.i >= 0);

			AGlActor* actor = obj->user_data;
			Arrange* arrange = actor->root->user_data;

			am_object_val(obj).i = val.i; // TODO should be done automatically?

			float pct = ((float)am_object_val(obj).i) / agl_actor__width(actor);
			unsigned len_samples = ayyi_beats2samples(am_object_val(&song->loc[AM_LOC_END]).sp.beat - am_object_val(&song->loc[AM_LOC_START]).sp.beat);

			GPos pos;
			samples2pos(((float)len_samples) * pct, &pos);
			arrange->canvas->scroll_to_pos(arrange, &pos, -1);

			AyyiSongPos p;
			ayyi_samples2pos(((float)len_samples) * pct, &p);
			char bbst[16]; shell__statusbar_print(((AyyiPanel*)actor->root->user_data)->window, 2, ayyi_pos2bbss(&p, bbst));
		}
		overview->position.set = gl_overview_set_position;

#ifdef AGL_ACTOR_RENDER_CACHE
		actor->fbo = agl_fbo_new(agl_power_of_two(agl_actor__width(actor)), agl_actor__height(actor), 0, 0);
		actor->cache.enabled = true;
#endif
	}

	OverviewActor* actor = agl_actor__new(OverviewActor,
		.actor = {
			.parent = {
				.class = &overview_class,
				.program = (AGlShader*)agl->shaders.plain,
				.init = overview_init,
				.set_size = overview_set_size,
				.paint = arr_gl_overview_draw,
				.on_event = actor__on_viewobject_event,
			},
			.n_objs = 1
		}
	);
	actor->position.user_data = actor;

	void overview_on_part_selection_change (Observable* o, AMVal val, gpointer actor)
	{
		agl_actor__invalidate((AGlActor*)actor);
	}
	observable_subscribe(am_parts->selection2, overview_on_part_selection_change, actor);

	void overview_on_part_change (GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, AGlActor* actor)
	{
		agl_actor__invalidate(actor);
	}
	g_signal_connect(am_parts, "item-changed", G_CALLBACK(overview_on_part_change), actor);

	return (AGlActor*)actor;
}


static void
overview_free (AGlActor* actor)
{
	am_object_deinit(&((OverviewActor*)actor)->position);

#if DEBUG
	int disconnected =
#endif
	g_signal_handlers_disconnect_by_data(am_parts, actor);
#if DEBUG
	g_return_if_fail(disconnected == 1);
#endif
	observable_unsubscribe(am_parts->selection2, NULL, actor);
	g_free(actor);
}


static void
overview_set_size (AGlActor* actor)
{
	Arrange* arrange = actor->root->user_data;
	AGlActor* root = (AGlActor*)actor->root;

	if(!SHOW_OVERVIEW){
		actor->region = (AGlfRegion){0,};
		return;
	}

	actor->region = (AGlfRegion){
		.x1 = 0,
		.x2 = root->region.x2,
		.y1 = root->region.y2 - OVERVIEW_HEIGHT,
		.y2 = root->region.y2
	};
}


static void
arr_gl_overview_on_view_change (AGlActor* actor, AMChangeType change)
{
	OverviewActor* overview = (OverviewActor*)actor;
	Arrange* arrange = actor->root->user_data;

	if(change & (AM_CHANGE_VIEWPORT_Y | AM_CHANGE_SHOW_SONG_OVERVIEW)){
		overview_set_size(actor);
	}

	if(change & (AM_CHANGE_POS_X | AM_CHANGE_WIDTH | AM_CHANGE_ZOOM_H | AM_CHANGE_SHOW_SONG_OVERVIEW)){
		double fx; arrange->canvas->get_view_fraction(arrange, &fx, NULL);
		int x = am_object_val(overview->window.model).i = -((double)CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1) * fx;

		if(fx >= 1.0) fx = 0.0; // disable the window handle

		overview->window.region = (iRegion){
			.x1 = x,
			.x2 = x + fx * agl_actor__width(actor),
			.y1 = 0,
			.y2 = OVERVIEW_HEIGHT
		};

		agl_actor__invalidate(actor);
	}
}


/*
 *  Return the handle position from the mouse coordinate
 */
static void
overview_window_xy_to_val (ViewObject* obj, AGliPt xy, AGliPt offset)
{
	g_return_if_fail(obj);
	AGlActor* actor = obj->user_data;

	int delta = xy.x - offset.x;

	const int fix = agl_actor__width(actor) * 8 / 100;
	int x = CLAMP(delta * 108 / 100, 0, agl_actor__width(actor) - view_object_width(obj) + fix);

	if(ABS(delta) > 1)
		obj->model->set(obj->model, 0, (AMVal){.i=x}, NULL, NULL);
}


