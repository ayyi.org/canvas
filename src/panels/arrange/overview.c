/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "model/time.h"
#include "model/part_manager.h"
#include "part_manager.h"
#include "windows.h"
#include "arrange.h"
#include "song.h"
#include "support.h"

static void arr_song_overview_update                ();
static void arr_song_overview_on_song_length_change (GObject*, gpointer);
static void overview_on_part_selection_change       (GObject*, AyyiPanel*, Arrange*);
static void overview_on_part_change                 (GObject*, AMPart*, AMChangeType, AyyiPanel*, Arrange*);


GtkWidget*
arr_song_overview_new (Arrange* arrange)
{
	song_overview* overview = &arrange->song_overview;

	GtkWidget* scrollwin = overview->widget = gtk_scrolled_window_new(NULL, NULL);
	if(arrange->view_options[SHOW_SONG_OVERVIEW].value) gtk_widget_show(scrollwin);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	gtk_widget_set_size_request(scrollwin, -1, 30);

	GtkWidget* canvas_widget = gnome_canvas_new_aa();
	overview->canvas = (GnomeCanvas*)canvas_widget;
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas_widget), 0, 0, 400, 30);
	gtk_widget_show (canvas_widget);
	gtk_container_add(GTK_CONTAINER(scrollwin), canvas_widget);
	gtk_widget_set_name(canvas_widget, "Overview");

	GnomeCanvasGroup* rootgroup = gnome_canvas_root(GNOME_CANVAS(canvas_widget));

	int width  = 400;
	int height =  30;
	arrange->song_overview.citem = gnome_canvas_item_new(rootgroup, gnome_canvas_pixbuf_get_type(),
                        "width",  (double)width,
                        "height", (double)height,
                        "x", 0.0,
                        "y", 0.0,
                        NULL);
	gnome_canvas_item_show(arrange->song_overview.citem);

	g_signal_connect(song, "song-length-change", G_CALLBACK(arr_song_overview_on_song_length_change), NULL);

	g_signal_connect(am_parts, "selection-change", G_CALLBACK(overview_on_part_selection_change), arrange);

	g_signal_connect(am_parts, "item-changed", G_CALLBACK(overview_on_part_change), arrange);

	return scrollwin;
}


void
arr_song_overview_destroy (Arrange* arrange)
{
	#define parts_handler_disconnect(FN) {int n; if((n = g_signal_handlers_disconnect_matched (am_parts, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange)) != 1) pwarn("part handler disconnect: %i", n);}
	parts_handler_disconnect(overview_on_part_selection_change);
	parts_handler_disconnect(overview_on_part_change);
	#undef parts_handler_disconnect
}


static void
arr_song_overview_draw_part (AMPart* part, cairo_t* cr, int width, int height)
{
	void part_bg_colour_to_double(AMPart* part, double* r, double* g, double* b)
	{
		//get part colour suitable for cairo. colour range is 0.0 to 1.0

		double _r = (song->palette[part->bg_colour] & 0xff000000) >> 24;
		double _g = (song->palette[part->bg_colour] & 0x00ff0000) >> 16;
		double _b = (song->palette[part->bg_colour] & 0x0000ff00) >>  8;

		*r = _r / 0xff;
		*g = _g / 0xff;
		*b = _b / 0xff;
	}

	g_return_if_fail(part);

	unsigned len_samples = ayyi_beats2samples(am_object_val(&song->loc[AM_LOC_END]).sp.beat - am_object_val(&song->loc[AM_LOC_START]).sp.beat);
	unsigned x = ayyi_pos2samples(&part->start) * width / len_samples;
	unsigned w = ayyi_pos2samples(&part->length) * width / len_samples;

	double r, g, b; part_bg_colour_to_double(part, &r, &g, &b);
	dbg (3, "colour: %i %.2f %.2f %.2f", part->bg_colour, r, g, b);

	cairo_set_source_rgb (cr, b, g, r); //some mistake here. reversing colour order gives the correct results!
	cairo_rectangle(cr, x, 0, w, height);
	cairo_fill (cr);
	cairo_stroke (cr);

	//outline:
	if(am_partmanager__is_selected(part)){
		cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
		cairo_rectangle(cr, x, 0, w, height);
		cairo_stroke (cr);
	}
}


void
arr_song_overview_queue_for_update ()
{
	//using an idle allows us to detect if the main Arrange canvas scrollbars are visible.

	static guint sov_idle = 0;

	gboolean _arr_song_overview_update (gpointer data)
	{
		arrange_foreach {
			arr_song_overview_update(arrange);
		} end_arrange_foreach

		sov_idle = 0; //show queue has been cleared
		return G_SOURCE_REMOVE;
	}

	if(!sov_idle) sov_idle = g_idle_add(_arr_song_overview_update, NULL);
}


static void
arr_song_overview_update (Arrange* arrange)
{
	if(!arrange->view_options[SHOW_SONG_OVERVIEW].value) return;

	song_overview* sov       = &arrange->song_overview;
	static GdkPixbuf* pixbuf = NULL;
	bool width_changed       = true;

	int width  = sov->widget->allocation.width;
	int height = 30;

	if(width_changed){
		gnome_canvas_set_scroll_region(GNOME_CANVAS(sov->canvas), 0, 0, width, height);

		if(pixbuf) g_object_unref(pixbuf); 
		pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_TRUE, BITS_PER_PIXEL, width, height);
		cairo_format_t format;
		if (gdk_pixbuf_get_n_channels(pixbuf) == 3) format = CAIRO_FORMAT_RGB24; else format = CAIRO_FORMAT_ARGB32;

		cairo_surface_t* surface = cairo_image_surface_create_for_data(gdk_pixbuf_get_pixels(pixbuf), format, width, height, gdk_pixbuf_get_rowstride(pixbuf));
		cairo_t*         cr      = cairo_create(surface);

		//background:
		cairo_rectangle(cr, 0, 0, width, height);
		//cairo_clip (cr); //this stops it crashing if we try to draw outside the pixbuf? - perhaps but it stops the background being cleared!
		float r, g, b;
		dbg (2, "arrange->bg_colour=0x%08x", arrange->bg_colour);
		am_rgba_to_float(arrange->bg_colour, &r, &g, &b);
		cairo_set_source_rgb (cr, b, g, r);
		cairo_fill (cr);
		cairo_stroke (cr);

		GList* l = song->parts ? song->parts->list : NULL;
		for(;l;l=l->next) arr_song_overview_draw_part(l->data, cr, width, height);

		//now do selected parts over the top. Not very efficient but still...
		l = arrange->part_selection;
		for(;l;l=l->next) arr_song_overview_draw_part(l->data, cr, width, height);

		//show the outline of the current window: TODO make this a separate canvas item to save redrawing parts.
		double fx;
		arrange->canvas->get_view_fraction(arrange, &fx, NULL);
		if(fx < 1.0){
			cairo_set_source_rgb (cr, 0.0, 0, 1.0);
			int xscrolloffset, yscrolloffset;
			arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
			double w = width * fx;
			double x = xscrolloffset * fx;
			cairo_rectangle(cr, x, 0, w, height);
			cairo_stroke (cr);
		}

		gnome_canvas_item_set(sov->citem, "pixbuf", pixbuf, NULL);
		cairo_destroy(cr);
		cairo_surface_destroy(surface);
	}
}


void
arr_song_overview_toggle (Arrange* arrange)
{
	g_return_if_fail(arrange);

	if(SHOW_OVERVIEW){
		if(!arrange->song_overview.widget){
			AyyiPanel* panel = (AyyiPanel*)arrange;
			panel_pack(arr_song_overview_new(arrange), EXPAND_FALSE);
		}
		gtk_widget_show(arrange->song_overview.widget);
	} else {
		gtk_widget_hide(arrange->song_overview.widget);
	}
}


static void
overview_on_part_selection_change (GObject* am_song, AyyiPanel* sender, Arrange* arrange)
{
	arr_song_overview_queue_for_update();
}


static void
overview_on_part_change (GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, Arrange* arrange)
{
	arr_song_overview_queue_for_update();
}


//FIXME only connect once, not per window.
static void
arr_song_overview_on_song_length_change (GObject* _song, gpointer user_data)
{
#ifdef USE_GNOMECANVAS
	arr_song_overview_queue_for_update();
#endif
}


