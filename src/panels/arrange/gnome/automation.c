/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. https://www.ayyi.org          |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define AUTOMATION_C
#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <libart_lgpl/libart.h>

#include "model/am_message.h"
#include "model/am_palette.h"
#include "model/time.h"
#include "model/curve.h"
#include "model/plugin.h"
#include "model/track_list.h"
#include "automation.h"
#include "windows.h"
#include "window.statusbar.h"
#include "panels/arrange.h"
#include "icon.h"
#include "song.h"
#include "arrange/automation_menu.h"
#include "arrange/automation.h"

#define EDGE_WIDTH 2
#define AUTO_HND_RADIUS 2.0
#define VOL_COLOUR "blue"
#define PAN_COLOUR "green"

char curve_colours[3][32] = {"blue", "green", "red"};

static void             g_automation_track_init   (ArrTrk*);
static GnomeCanvasItem* automation_handle_new     (GnomeCanvasGroup*, uint32_t colour);
static ArrAutoPath*     curve_find_aap            (Arrange*, Curve*);
static GnomeCanvasItem* get_curve_pathitem        (Arrange*, Curve*);
//static void           automation_load_dummy();
//static void           arr_coords_win_to_canvas  (Arrange*, int* x, int* y);
static void             arr_coords_win_to_track   (Arrange*, int* x, int* y);

//static int              automation_bpath_get_end  (ArtBpath*);
static void             automation_gpath_update_and_emit(Curve*);
//static void           automation_pathitem_update(Arrange*, Curve*);
static void             automation_update_gpath   (Curve*);

static bool             automation_pt_is_last     (Curve*, int);

static bool             track__add_control        (AMTrack*, Curve*);

static gint             auto_pt_cb1               (GnomeCanvasItem*, GdkEvent*, Arrange*);
static gint             auto_pt_cb2               (GnomeCanvasItem*, GdkEvent*, Arrange*);
static gint             auto_pt_cb                (GnomeCanvasItem*, GdkEvent*, int handle, Arrange*);
static gint             auto_hdl_cb               (GnomeCanvasItem*, GdkEvent*, gpointer data);

static void             g_automation_on_curve_change(Curve*);

static void             on_automation_change      (AyyiPanel* sender);

static ArrAutoPath*     get_aap_from_citem        (ArrTrk*, GnomeCanvasItem*);
static gint             track_curve_sel           (GnomeCanvasItem*, GdkEvent*, ArrTrk*);


void
g_automation_init ()
{
	// Because curves are shared between windows, this does not use a per-window callback

	static bool done = false;

	void _automation_on_curve_change (GObject* o, Curve* curve, gpointer user_data) {
		g_automation_on_curve_change(curve);
	}

	if (!done) {
		g_signal_connect(song, "curve-change", G_CALLBACK(_automation_on_curve_change), NULL);
		g_signal_connect(song, "curve-add", G_CALLBACK(_automation_on_curve_change), NULL);
		done = true;
	}
}


void
g_automation_uninit ()
{
}


/*
 *  Initialise the automation control points and lines.
 *  -this should only ever be called once per window.
 */
void
automation_ctlpts_init (Arrange* arrange)
{
	g_canvas* c = arrange->canvas->gnome;

	void on_show (Arrange* arrange, ArrTrk* atr, ArrAutoPath* aap)
	{
		g_automation_view_vol(atr);
	}
  	arrange->canvas->on_auto_ctl_show = on_show;

	void on_hide (Arrange* arrange, ArrAutoPath* aap)
	{
		GnomeCanvasItem* item = aap->pathitem;
		gnome_canvas_item_hide(item);

		// update the Selection
		automation_view__select_segment(arrange, aap, -1);
	}
  	arrange->canvas->on_auto_ctl_hide = on_hide;

	void on_add (Arrange* arrange, ArrAutoPath* aap, Curve* curve, int ins_pos)
	{
		automation_gpath_update_and_emit(curve);
		automation_curve_draw_fast(arrange, curve);
		automation_view__select_segment(arrange, aap, ins_pos);
	}
  	arrange->canvas->on_auto_pt_add = on_add;

	void on_remove (Arrange* arrange, ArrAutoPath* aap, Curve* curve, int new_selection)
	{
  		automation_update_gpath(curve);
  		automation_curve_draw_fast(arrange, curve);

		automation_view__select_segment(arrange, aap, new_selection);
	}
  	arrange->canvas->on_auto_pt_remove = on_remove;

	int ctl_colour  = 0xff0000ff;
	int ctl_colour2 = 0x55bb55ff; //used for lines and rects on the *adjacent* subpaths

	GnomeCanvasPoints* pts1 = c->auto_pts1 = gnome_canvas_points_new(2);
	GnomeCanvasPoints* pts2 = c->auto_pts2 = gnome_canvas_points_new(2);

	pts1->coords[0] = pts1->coords[2] = 50.0;
	pts1->coords[1] =    0.0; // y1
	pts1->coords[3] =  300.0; // y2

	pts2->coords[0] = pts2->coords[2] = 50.0;
	pts2->coords[1] =    0.0; // y1
	pts2->coords[3] =  300.0; // y2

	GnomeCanvasGroup* group = gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget)); // place to initially place the controls.
	g_return_if_fail(group);
	if (!GNOME_IS_CANVAS_GROUP(group)) { pwarn ("no automation track canvas group."); return; }

	c->auto_ctl_1 = gnome_canvas_item_new(group, gnome_canvas_line_get_type(),
		"fill-color-rgba", ctl_colour,
		"width-pixels", 2,
		"points", pts1,
		NULL);
	c->auto_ctl_2 = gnome_canvas_item_new(group, gnome_canvas_line_get_type(),
		"fill-color-rgba", ctl_colour,
		"width-pixels", 2,
		"points", pts2,
		NULL);
	gnome_canvas_item_hide(c->auto_ctl_1);
	gnome_canvas_item_hide(c->auto_ctl_2);

	c->auto_hnd_1 = automation_handle_new(group, 0x000000ff);
	c->auto_hnd_2 = automation_handle_new(group, 0x000000ff);
	c->auto_hnd_3 = automation_handle_new(group, 0x000000ff); // rect in prev seg.
	c->auto_hnd_4 = automation_handle_new(group, 0x000000ff); // rect in next seg.

	gtk_signal_connect(GTK_OBJECT(c->auto_hnd_1), "event", (GtkSignalFunc)auto_hdl_cb, (void*)1);
	gtk_signal_connect(GTK_OBJECT(c->auto_hnd_2), "event", (GtkSignalFunc)auto_hdl_cb, (void*)2);

	c->auto_pt_1 = automation_handle_new(group, 0x000000ff);
	c->auto_pt_2 = automation_handle_new(group, 0x000000ff);

	gtk_signal_connect(GTK_OBJECT(c->auto_pt_1), "event", (GtkSignalFunc)auto_pt_cb1, arrange);
	gtk_signal_connect(GTK_OBJECT(c->auto_pt_2), "event", (GtkSignalFunc)auto_pt_cb2, arrange);

	// lines for adjacent subpaths
	c->auto_ctl_3 = gnome_canvas_item_new(group,
		gnome_canvas_line_get_type(),
		"fill-color-rgba", ctl_colour2,
		"width-pixels", 1,
		"points", pts2,
		NULL);
	c->auto_ctl_4 = gnome_canvas_item_new(group,
		gnome_canvas_line_get_type(),
		"fill-color-rgba", ctl_colour2,
		"width-pixels", 1,
		"points", pts2,
		NULL);
	gnome_canvas_item_hide(c->auto_ctl_3);
	gnome_canvas_item_hide(c->auto_ctl_4);
}


static GnomeCanvasItem*
automation_handle_new (GnomeCanvasGroup* group, uint32_t colour)
{
  int ctl_colour  = 0xff0000ff;
  GnomeCanvasItem* item = gnome_canvas_item_new(group,
  							gnome_canvas_rect_get_type(),
					        "outline_color_rgba", colour,
					        "fill-color-rgba", ctl_colour,
					        "x1", 10.0, "y1", 10.0,
							"x2", 20.0, "y2", 20.0,
							"width_units", 1.0,
							NULL);
  gnome_canvas_item_hide(item);
  return item;
}


static void
g_automation_track_init (ArrTrk* atr)
{
	Arrange* arrange = atr->arrange;
	g_return_if_fail(arrange->canvas->gnome);

	atr->grp_auto = GNOME_CANVAS_GROUP(gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget)),
		gnome_canvas_group_get_type(),
		"x", 0.0,
		"y", (double)atr->y * vzoom(arrange),
		NULL
	));
}


void
automation_track_draw (Arrange* arrange, ArrTrk* atr)
{
	// Draw all the curves for this track onto the arrange canvas.

	// -vert: lets use vertical values of 0-100.0 covering the complete height of the track.
	//        Values have to be scaled and inverted before rendering.
	//        Drawing is done onto a per track automation canvasgroup, so no moving is necc.
	// -hor:  unzoomed pixel values are used.

	// FIXME
	// we need to disallow curve control pts that make dx negative, as it is non-sensical in this context

	if (!CANVAS_IS_GNOME) return;

	AMTrack* trk = atr->track;
	g_return_if_fail(trk->type);
	if (!atr->auto_paths) return;

	if (!atr->grp_auto) g_automation_track_init(atr);
	GnomeCanvasGroup* group = atr->grp_auto;

	GnomeCanvasPathDef* vol_g_path = trk->bezier.vol->g_path;

	if(trk->bezier.vol->path && !vol_g_path){
		automation_update_gpath(trk->bezier.vol); //TODO check
		vol_g_path = trk->bezier.vol->g_path;
	}

	if (atr->auto_paths->len <= PAN) {
		g_ptr_array_add(atr->auto_paths, AYYI_NEW(ArrAutoPath,
			.curve = trk->bezier.pan,
		));
	}

	int n_curves = 2 + g_list_length(trk->controls);
	GnomeCanvasItem** paths[n_curves + 1];
	int j = 0;
	paths[j++] = &AUTO_PATH(VOL)->pathitem;
	paths[j++] = &AUTO_PATH(PAN)->pathitem;

	GList* l = trk->controls;
	for (;l;l=l->next) {
		Curve* c = l->data;
#ifdef DEBUG
		if (!c->path) dbg(1, "curve has no path - need to make one...");
#endif
		if (!c->path)
			am_automation_make_empty(c); // makes path and gpath.
		else
			am_song__emit("curve-change", c);

		ArrAutoPath* aap = curve_find_aap(arrange, c);
		if (aap) {
			GnomeCanvasItem* pathitem = aap->pathitem;
			paths[j++] = &pathitem;
		}
		j++;
	}
	paths[n_curves] = NULL;

	int n_active_controls = 0;
	GPtrArray* aplist = atr->auto_paths;
	for (int i=0; paths[i]; i++) {
		if (i < 2 && !vol_g_path) continue; // no vol & pan automation

		GnomeCanvasItem* item = *paths[i];
		Curve* c = (i < 2)
			? ((Curve**)&trk->bezier)[i]
			: am_track__lookup_curve(trk, i);
		GnomeCanvasPathDef* g_path = c->g_path;

		dbg (0, "i=%i path=%p gpath=%p item=%p", i, c->path, g_path, item);
		if (!item) { // TODO use fn below for this? ..but multiple affine creation... - store the affine?
			if (g_path) {
				*paths[i] = gnome_canvas_item_new(group, gnome_canvas_bpath_get_type(),
					"bpath", g_path,
					"width_pixels", 2,
					"outline_color", curve_colours[i],
					NULL);
				dbg (0, "i=%i new item! item=%p", i, *paths[i]);
				g_signal_connect(GTK_OBJECT(*paths[i]), "event", (GtkSignalFunc)track_curve_sel, atr);
				if (i >= 2) ((ArrAutoPath*)g_ptr_array_index(aplist, i - 2))->pathitem = *paths[i];
			}
		} else gnome_canvas_item_set(item, "bpath", g_path, NULL);

		n_active_controls++;
	}

	if (!n_active_controls) return;

	//testing to see if the section below is still needed.
#if 0
	GnomeCanvasItem* pathitem = at->vol_autopath->pathitem; //FIXME this might not exist...

	//make a scaling affine:
	//what is the affine scaling in relation to? - i guess in relation to parent 0,0.
	double affine[6];
	art_affine_scale(affine, 1.0, (double)at->height * vzoom(arrange) / 100.0);//x,yscale
	//apply the affine:
	gnome_canvas_item_affine_relative(pathitem, affine);
	//note: vertical values are 'inverted' previously, as i dont know how to do this with an affine.
	//      ----maybe try art_affine_flip()??

	gnome_canvas_item_raise_to_top (pathitem);
#endif
	gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM(group));

	automation_view_on_size_change(arrange);
}


void
g_automation_track_destroy (Arrange* arrange, ArrTrk* atr)
{
	if (!atr->grp_auto) return;

	for (int i=0;i<atr->auto_paths->len;i++) {
		ArrAutoPath* aap = g_ptr_array_index(atr->auto_paths, i);
		if (aap->pathitem) {
			gtk_object_destroy(GTK_OBJECT(aap->pathitem));
			aap->pathitem = NULL;
		}
	}
}


void
g_automation_curves_undraw (Arrange* arrange)
{
	// It is assumed that the canvas is being destroyed, so we just reset the pointers.

	g_canvas* c = arrange->canvas->gnome;

	gnome_canvas_points_free(c->auto_pts1);
	gnome_canvas_points_free(c->auto_pts2);

	for (int t=0;t<AM_MAX_TRK;t++) {
		ArrTrk* at = track_list__track_by_index(arrange->priv->tracks, t);
		if (!at) continue;
		at->grp_auto = NULL;
		g_ptr_array_foreach(at->auto_paths, (GFunc)g_free, NULL);
		g_clear_pointer(&at->auto_paths, g_ptr_array_unref);
	}
}


static BPath*
get_selected_abp (Arrange* arrange)
{
	AMTrackNum t = arrange->automation.sel.tnum;
	int auto_type = arrange->automation.sel.type;
	switch (auto_type) {
		case 0:
			return song->tracks->track[t]->bezier.vol->path;
			break;
		case 1:
			return song->tracks->track[t]->bezier.pan->path;
			break;
		default:
			{
				GList* l = song->tracks->track[t]->controls;
				for(;l;l=l->next){
					Curve* curve = l->data;
					if (curve->auto_type == auto_type) return curve->path;
				}
			}
			break;
	}
	return NULL;
}


void
automation_view_on_size_change (Arrange* arrange)
{
	// what can have changed?
	//   -horiz or vert zoom
	//   -track height

	if (!SHOW_AUTO) return;
	if (!CANVAS_IS_GNOME) return;

	g_canvas* c = arrange->canvas->gnome;

	double affine[6];
	static double r = AUTO_HND_RADIUS;

	ArrTrackNum t;
	for (t=0;t<AM_MAX_TRK;t++){
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
		// move the groups to align with track strips
		AMTrack* tr = song->tracks->track[t];
		if (!tr || !atr) continue;
		if ((!tr->bezier.vol || !tr->bezier.vol->g_path) && !atr->auto_paths) continue; // not all tracks have automation.
		if (!atr->grp_auto) continue;                                                   // track not drawn yet.

		gnome_canvas_item_set(GNOME_CANVAS_ITEM(atr->grp_auto), "y", (double)(c->track_pos[t]), NULL);

		dbg (2, "bpath=%p y=%f.", tr->bezier.vol->g_path, (double)c->track_pos[t]);
		//this is just to make the grp move take effect:
		if (AUTO_PATH(VOL)->pathitem) gnome_canvas_item_move(AUTO_PATH(VOL)->pathitem, 0.0, 0.0); //relative.

		// now *scale* the curve canvas items to the height of the track and current zoom levels
		// (currently we do this whether or not it has changed)
		art_affine_scale(affine, arr_samples2px(arrange, 1), (double)atr->height * vzoom(arrange) / 100.0);
		if (AUTO_PATH(VOL)->pathitem) gnome_canvas_item_affine_absolute(AUTO_PATH(VOL)->pathitem, affine);
		if (AUTO_PATH(PAN)->pathitem) gnome_canvas_item_affine_absolute(AUTO_PATH(PAN)->pathitem, affine);
		for (int i=0;i<atr->auto_paths->len;i++) {
			ArrAutoPath* aap = g_ptr_array_index(atr->auto_paths, i);
			dbg(0, "t=%i: scaling plugin curve...", t);
			gnome_canvas_item_affine_absolute(aap->pathitem, affine);
		}

		// probably being overridden later
		gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM(atr->grp_auto));
	}
  
	t = arrange->automation.sel.tnum;
	int p; // current sub-path number.
	if ((p = arrange->automation.sel.subpath)) { // check that the control lines are visible.
		dbg (2, "subpathnum=%i", p);
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);

		// editing control pts (may not be visible)
		art_affine_scale(affine, arr_samples2px(arrange, 1), (double)atr->height * vzoom(arrange) / 100.0);
		gnome_canvas_item_affine_absolute(c->auto_ctl_1, affine);
		gnome_canvas_item_affine_absolute(c->auto_ctl_2, affine);
		gnome_canvas_item_affine_absolute(c->auto_ctl_3, affine);
		gnome_canvas_item_affine_absolute(c->auto_ctl_4, affine);

		// redo non-affine scaled items
		ArtBpath* abp = (ArtBpath*)get_selected_abp(arrange);
		if (!abp){ perr ("bpath array NULL!"); return; }

		double vscale = (double)atr->height * vzoom(arrange) / 100.0;
		gnome_canvas_item_set (c->auto_hnd_1,
			"x1", arr_samples2px(arrange, abp[p].x1) -r, "y1", abp[p].y1 * vscale -r,
			"x2", arr_samples2px(arrange, abp[p].x1) +r, "y2", abp[p].y1 * vscale +r,
			NULL);
		gnome_canvas_item_set (c->auto_hnd_2,
			"x1", arr_samples2px(arrange, abp[p].x2) -r, "y1", abp[p].y2 * vscale -r,
			"x2", arr_samples2px(arrange, abp[p].x2) +r, "y2", abp[p].y2 * vscale +r,
			NULL);
		gnome_canvas_item_set (c->auto_pt_1,
			"x1", arr_samples2px(arrange, abp[p-1].x3) -r, "y1", abp[p-1].y3 *vscale -r,
			"x2", arr_samples2px(arrange, abp[p-1].x3) +r, "y2", abp[p-1].y3 *vscale +r,
			NULL);
		gnome_canvas_item_set (c->auto_pt_2,
			"x1", arr_samples2px(arrange, abp[p].x3) -r, "y1", abp[p].y3 * vscale -r,
			"x2", arr_samples2px(arrange, abp[p].x3) +r, "y2", abp[p].y3 * vscale +r,
			NULL);
	}
}


#if 0
void
automation_view_on_song_load ()
{
	AMTrack* trk;
	for(int t=0; (trk = song->tracks->track[t]); t++){ 
		GList* l = trk->controls;
		for (;l;l=l->next) {
			Curve* curve = l->data;
			g_automation_on_new_curve(curve);
		}
	}
}
#endif


void
automation_on_curves_change (Arrange* arrange)
{
	// Update all curves on all tracks.
	// Assume that window sizes and zoom levels are unchanged.

	PF;
	dbg(0, "TODO also update plugin controls");

	AMIter iter;
	am_collection_iter_init(am_tracks, &iter);
	AMTrack* trk = NULL;
	while ((trk = am_collection_iter_next (am_tracks, &iter))) {
		if (track_list__lookup_track(arrange->priv->tracks, trk)->grp_auto) {
			g_automation_curve_draw(arrange, trk->bezier.vol);
			g_automation_curve_draw(arrange, trk->bezier.pan);
		}
	}
}


static ArrAutoPath*
curve_find_aap (Arrange* arrange, Curve* curve)
{
	// Return the canvasItem for the given curve. Will not be created if empty.

	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, curve->track);
	g_return_val_if_fail(atr, NULL);

	return arr_track_find_aap (atr, curve);
}


static GnomeCanvasItem*
get_curve_pathitem (Arrange* arrange, Curve* curve)
{
	// Return the canvasitem for the given curve. If the curve has not yet been drawn, the canvasitem will created.

	ArrTrk* arr_trk = track_list__lookup_track(arrange->priv->tracks, curve->track);
	g_return_val_if_fail(arr_trk, NULL);

	ArrAutoPath* aap = curve_find_aap(arrange, curve);
	if (!aap) { pwarn("curve not found for this Arrange window. name=%s at->auto_paths.size=%i", curve->name, arr_trk->auto_paths->len); return NULL; }

	if (!aap->pathitem) {
		// curve has not yet been drawn
		g_automation_curve_draw(arrange, curve);
	}
	return aap->pathitem;
}


static GnomeCanvasItem*
get_pathitem (ArrTrk* atr, int auto_type)
{
	g_return_val_if_fail(auto_type < atr->auto_paths->len, NULL);

	ArrAutoPath** aapaths = (ArrAutoPath**)atr->auto_paths->pdata;
	if (!aapaths[auto_type]) {
		aapaths[auto_type] = AYYI_NEW(ArrAutoPath,
			.curve = (auto_type == VOL)
				? atr->track->bezier.vol
				: (auto_type == PAN)
					? atr->track->bezier.pan
					: NULL
		);
	}
	return aapaths[auto_type]->pathitem;
}


void
g_automation_curve_draw (Arrange* arrange, Curve* curve)
{
	// Assume that the Gnomecanvaspathdef* is up to date.

	g_return_if_fail(curve);
	if (!curve->path) return;

	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, curve->track);
	if (!atr->grp_auto) g_automation_track_init(atr);

	ArrAutoPath** aapaths = (ArrAutoPath**)atr->auto_paths->pdata;
	GnomeCanvasItem* item = NULL;
	ArrAutoPath* aap = NULL;

	if (curve->auto_type < 2) { // old style
		item = get_pathitem(atr, curve->auto_type);
	} else {
		aap = curve_find_aap(arrange, curve);
		g_return_if_fail(aap);
		item = aap->pathitem;
	}

	if (item) {
		gnome_canvas_item_set(item, "bpath", curve->g_path, NULL);
	} else {
		dbg(1, "new canvas item...");
		if (curve->g_path) {
			item = gnome_canvas_item_new(
				atr->grp_auto,
				gnome_canvas_bpath_get_type(),
				"bpath", curve->g_path,
				"width_pixels", 2,
				"outline_color", curve_colours[curve->auto_type],
				NULL
			);
			dbg(1, "new_item=%p", item);

			if (curve->auto_type < 2) {
				aapaths[curve->auto_type]->pathitem = item;
			} else {
				aap->pathitem = item;
			}
			g_signal_connect(GTK_OBJECT(item), "event", (GtkSignalFunc)track_curve_sel, atr);
		}
	}
	g_return_if_fail(item);

	double affine[6];
	art_affine_scale(affine, arr_samples2px(arrange, 1), (double)atr->height * vzoom(arrange) / 100.0); // x, y scale
	gnome_canvas_item_affine_absolute(item, affine);

	gnome_canvas_item_show(item); //temporary!!
}


void
automation_curve_draw_fast (Arrange* arrange, Curve* curve)
{
	// This one doesnt redo the affine.

	// Assume that the Gnomecanvaspathdef* is up to date.

	ArrAutoPath* aap = curve_find_aap(arrange, curve);
	g_return_if_fail(aap);
	GnomeCanvasItem* item = aap->pathitem;

	g_return_if_fail(curve->g_path);
	gnome_canvas_item_set(item, "bpath", curve->g_path, NULL);

	gnome_canvas_item_show(item); //temporary!!
}


void
automation_view__select_segment (Arrange* arrange, ArrAutoPath* aap, int seg)
{
	g_canvas* c = arrange->canvas->gnome;

	if (seg < 0) {
		// reset the selection
		arrange->automation.sel.subpath = 0;
		arrange->automation.sel.type = -1;
		gnome_canvas_item_hide(c->auto_pt_1);
		gnome_canvas_item_hide(c->auto_pt_2);
		return;
	}

	Curve* curve = aap->curve;
	dbg(2, "autotype=%i", curve->auto_type);

	int p = seg;
	ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, curve->track);
	ArtBpath* abp = (ArtBpath*)curve->path;

	static double r = AUTO_HND_RADIUS; //the 'radius' of the handle rect.

	if (abp[p].code == ART_LINETO) {
		//straight lines dont need control lines anyway. Just move the endpoints.
		//pwarn ("linetype LINE. not implemented.");
		//return FALSE;
	}

	// draw the control lines
	// note: the ctlpts are scaled using affines only. The raw y coords are 0.0 to 100.0.

	GnomeCanvasItem* ctl1 = c->auto_ctl_1;
	GnomeCanvasItem* ctl2 = c->auto_ctl_2;
	GnomeCanvasItem* ctl3 = c->auto_ctl_3;
	GnomeCanvasItem* ctl4 = c->auto_ctl_4;
	g_return_if_fail(ctl1);
	g_return_if_fail(ctl2);

	// create the scaling affine
	double affine[6];
	art_affine_scale(affine, arr_samples2px(arrange, 1), (double)at->height * vzoom(arrange) / 100.0);//x,yscale

	gnome_canvas_item_reparent(ctl1,          at->grp_auto);
	gnome_canvas_item_reparent(ctl2,          at->grp_auto);
	gnome_canvas_item_reparent(ctl3,          at->grp_auto);
	gnome_canvas_item_reparent(ctl4,          at->grp_auto);
	gnome_canvas_item_reparent(c->auto_hnd_1, at->grp_auto);
	gnome_canvas_item_reparent(c->auto_hnd_2, at->grp_auto);
	gnome_canvas_item_reparent(c->auto_hnd_3, at->grp_auto);
	gnome_canvas_item_reparent(c->auto_hnd_4, at->grp_auto);
	gnome_canvas_item_reparent(c->auto_pt_1,  at->grp_auto);
	gnome_canvas_item_reparent(c->auto_pt_2,  at->grp_auto);

	gnome_canvas_item_affine_absolute(ctl1, affine); // note: the affine must come after the reparenting.
	gnome_canvas_item_affine_absolute(ctl2, affine);
	gnome_canvas_item_affine_absolute(ctl3, affine);
	gnome_canvas_item_affine_absolute(ctl4, affine);

	// 1st control line
	GnomeCanvasPoints* pts1 = c->auto_pts1;
	pts1->coords[0] = abp[p-1].x3;
	pts1->coords[1] = abp[p-1].y3;
	pts1->coords[2] = abp[p].x1;
	pts1->coords[3] = abp[p].y1;
	if(abp[p].code != ART_LINETO){
		gnome_canvas_item_set (ctl1, "points", pts1, NULL);
		gnome_canvas_item_show(ctl1);
	}

	// 1st handle rect (left)
	double vscale = (double)track_list__track_by_index(arrange->priv->tracks, arrange->automation.sel.tnum)->height * vzoom(arrange) / 100.0;
	gnome_canvas_item_set (c->auto_hnd_1,
		"x1", arr_samples2px(arrange, abp[p].x1) -r, "y1", abp[p].y1 * vscale -r,
		"x2", arr_samples2px(arrange, abp[p].x1) +r, "y2", abp[p].y1 * vscale +r,
		NULL);
	if(abp[p].code == ART_CURVETO) gnome_canvas_item_show(c->auto_hnd_1);

	// 2nd control line
	GnomeCanvasPoints* pts2 = c->auto_pts2;
	pts2->coords[0] = abp[p].x3;
	pts2->coords[1] = abp[p].y3;
	pts2->coords[2] = abp[p].x2;
	pts2->coords[3] = abp[p].y2;
	if (abp[p].code != ART_LINETO) {
		gnome_canvas_item_set (ctl2, "points", pts2, NULL);
		gnome_canvas_item_show(ctl2);
	}

	// 2nd handle rect
	gnome_canvas_item_set (c->auto_hnd_2,
		"x1", arr_samples2px(arrange, pts2->coords[2]) -r, "y1", pts2->coords[3] * vscale -r,
		"x2", arr_samples2px(arrange, pts2->coords[2]) +r, "y2", pts2->coords[3] * vscale +r,
		NULL);
	if (abp[p+1].code == ART_CURVETO) gnome_canvas_item_show(c->auto_hnd_2);

	// 1st *adjacent* subpath
	{
		gboolean firstsubpath = FALSE;
		if(!firstsubpath){
			GnomeCanvasItem* ctl3 = c->auto_ctl_3; // seg before.
			g_return_if_fail(ctl3);

			//1st _adjacent_ control line
			if(abp[p].code != ART_LINETO){
				pts1->coords[0] = abp[p-1].x3;
				pts1->coords[1] = abp[p-1].y3;
				pts1->coords[2] = abp[p-1].x2;
				pts1->coords[3] = abp[p-1].y2;
				gnome_canvas_item_set (ctl3, "points", pts1, NULL);
				gnome_canvas_item_show(ctl3);

				gnome_canvas_item_set (c->auto_hnd_3,
					"x1", arr_samples2px(arrange, abp[p-1].x2) -r, "y1", abp[p-1].y2 * vscale -r,
					"x2", arr_samples2px(arrange, abp[p-1].x2) +r, "y2", abp[p-1].y2 * vscale +r,
					NULL);
				gnome_canvas_item_show(c->auto_hnd_3);
			}
		}
	}

	// 2nd adjacent line
	{
		int len = gnome_canvas_path_def_length(curve->g_path); //get the length of the path
		if(len < p){ perr ("length too short: %i. seg=%i.", len, p); return; }
		gboolean is_lastsubpath = (p > (len-3)); //3 is empirically deduced.
		dbg (2, "len=%i p=%i is_last=%i.", len, p, is_lastsubpath);
		if(is_lastsubpath){
			gnome_canvas_item_hide(c->auto_hnd_4);
			gnome_canvas_item_hide(ctl4);
		}else{
			//GnomeCanvasItem *ctl4 = arrange->auto_ctl_4; //path segment after the selected one.
			g_return_if_fail(ctl4);

			//2nd _adjacent_ control line:
			if(abp[p].code != ART_LINETO){
			pts1->coords[0] = abp[p].x3;
			pts1->coords[1] = abp[p].y3;
			pts1->coords[2] = abp[p+1].x1;
			pts1->coords[3] = abp[p+1].y1;
			gnome_canvas_item_set (ctl4, "points", pts1, NULL);
			gnome_canvas_item_show(ctl4);

			gnome_canvas_item_set (c->auto_hnd_4,
				"x1", arr_samples2px(arrange, abp[p+1].x1) -r, "y1", abp[p+1].y1 * vscale -r,
				"x2", arr_samples2px(arrange, abp[p+1].x1) +r, "y2", abp[p+1].y1 * vscale +r,
				NULL);
			gnome_canvas_item_show(c->auto_hnd_4);
			}
		}
	}

	// move the 2 rects on the curve itself
	gnome_canvas_item_set (c->auto_pt_1,
		"x1", arr_samples2px(arrange, abp[p-1].x3) -r, "y1", abp[p-1].y3 *vscale -r,
		"x2", arr_samples2px(arrange, abp[p-1].x3) +r, "y2", abp[p-1].y3 *vscale +r,
		NULL);
	gnome_canvas_item_show(c->auto_pt_1);

	gnome_canvas_item_set (c->auto_pt_2,
		"x1", arr_samples2px(arrange, pts2->coords[0]) -r, "y1", pts2->coords[1] * vscale -r,
		"x2", arr_samples2px(arrange, pts2->coords[0]) +r, "y2", pts2->coords[1] * vscale +r,
		NULL);
	gnome_canvas_item_show(c->auto_pt_2);
}


static gint
auto_pt_cb1 (GnomeCanvasItem* item, GdkEvent* event, Arrange* arrange)
{
	// Fn serves to pass the handle num to the real callback.
	return auto_pt_cb(item, event, 1, arrange);
}


static gint
auto_pt_cb2 (GnomeCanvasItem* item, GdkEvent* event, Arrange* arrange)
{
	return auto_pt_cb(item, event, 2, arrange);
}


gint
auto_pt_cb (GnomeCanvasItem* item, GdkEvent* event, int hnd_num, Arrange* arrange)
{
	// Callback for mouse events on the rects on the curve itself.

	// @hnd_num: the point being edited - either 1 or 2.

	// FIXME some of the calculations for the points on the adjacent subpaths are a bit off
	// (maybe zoooming). Its pending simultaneous showing of the control pts for these.

	g_canvas* c = arrange->canvas->gnome;

	static int dragging;
	Ptd mouse;                      // mouse coords - canvas global.
	//double mouse_x, mouse_y;        // mouse coords - canvas global.
	double mouse_y_trk;             // mouse position relative to the current track.
	static int trk_vpos;            // the top of the current track.
	static double r=AUTO_HND_RADIUS;// handle radius
	static int t;                   // track number.
	static ArrTrk* at;

	static GnomeCanvasItem* hitem;  // handle rect.
	static GnomeCanvasItem* citem;  // control line.
	static GnomeCanvasItem* pitem;  // point on the curve that is relevant to the current drag operation.
	static GnomeCanvasItem* aitem;  // ctl line for the adjacent subpath.
	static GnomeCanvasItem* ahitem; // handle rect for the adjacent subpath.
	static GnomeCanvasPoints* pts;  // coords of the ends of the control line being moved.

	static Curve    *curve;
	static ArtBpath *abp = NULL;    // artbezierpath for the current track.
	static double   *abpx, *abpy;   // the curve pt that needs to be updated.
	static ArtBpath *abp_next = NULL;       // artbezierpath for the adjacent subpath. (before or after)
	static double   *abpx_next, *abpy_next; // the pt on the adjacent subpath that needs to be updated.
	static ArtBpath *abp_p = NULL;  // artbezierpath for the POINT on the curve that is being dragged.
	                                // -reason for this is that if the lhs pt is actually part of the
	                                //  previous subpath.
	static double *abp_px, *abp_py;
	static double ctl_line_x;       // location of the handle rect. Depends on which ctl pt is grabbed.
	static double ctl_line_y;       // location of the handle rect. Depends on which ctl pt is grabbed.

	mouse       = (Ptd){event->button.x, event->button.y};
	mouse_y_trk = mouse.y - (double)trk_vpos;
	static double vscale = 1.0;     // we need the track vmag to reverse affines on the ctl lines.

	double dx,dy;
	static double old_x, old_y;
	int           subpath;          // number of the curve segment being edited (>0).
	static int              p;      // number of the point being edited. Index into path array.     <--- merge with subpath?

	gboolean shift   = event->button.state & GDK_SHIFT_MASK;
	//gboolean control = event->button.state & GDK_CONTROL_MASK;

	switch (event->type){
		case GDK_BUTTON_PRESS:
			dbg (0 ,"BUTTON_PRESS");
			t        = arrange->automation.sel.tnum;
			trk_vpos = c->track_pos[t];
			at       = track_list__track_by_index(arrange->priv->tracks, t);
			ArrAutoPath* aap = automation_current_path(at);
			curve    = aap->curve;
			vscale   = (double)at->height * vzoom(arrange) / 100.0;
			subpath = arrange->automation.sel.subpath;
			abp      = &((ArtBpath*)curve->path)[subpath];
			old_x    = mouse.x;
			old_y    = mouse.y;
			//dbg (0, "auto_type=%i seg=%i path=%p abp=%p size=%i", arrange->automation.sel.type, subpath, curve->path, abp, automation_bpath_get_end((ArtBpath*)curve->path)); 
			g_return_val_if_fail(abp, false);

			if (hnd_num == 1) { // left hand control pt is being dragged.
				pitem = c->auto_pt_1;
				hitem = c->auto_hnd_1;
				citem = c->auto_ctl_1;
				aitem = c->auto_ctl_3;
				ahitem= c->auto_hnd_3;
				pts   = c->auto_pts1;
				abpx  = &abp->x1; //!! shouldnt be accessed??
				abpy  = &abp->y1;
				p     = subpath - 1;
				abp_p = &((ArtBpath*)curve->path)[arrange->automation.sel.subpath -1];//moving a coord in the *previous* subpath
				abp_px= &abp_p->x3;
				abp_py= &abp_p->y3;
				//since the first subpath is not visible, hopefully this index offset should be ok:
				abp_next  = &((ArtBpath*)curve->path)[arrange->automation.sel.subpath -1];
				abpx_next = &abp_next->x2;
				abpy_next = &abp_next->y2;
				ctl_line_x= abp->x1;
				ctl_line_y= abp->y1;
			} else if (hnd_num == 2) {
				pitem = c->auto_pt_2;
				hitem = c->auto_hnd_2;
				citem = c->auto_ctl_2;
				aitem = c->auto_ctl_4;
				ahitem= c->auto_hnd_4;
				pts   = c->auto_pts2;
				abpx  = &abp->x3;
				abpy  = &abp->y3;
				abp_p = abp; // dragged point is on the *current* subpath.
				abp_px= abpx;
				abp_py= abpy;
				p     = subpath;
				//nb: we cannot go off the end of the pathA cos the last one is ART_END which we cant clck on:
				abp_next  = &((ArtBpath*)curve->path)[arrange->automation.sel.subpath +1];
				abpx_next = &abp_next->x1;
				abpy_next = &abp_next->y1;
				ctl_line_x= abp->x2;
				ctl_line_y= abp->y2;
				dbg (0, "ctl_line_x=%.2f ctl_line_y=%.2f", ctl_line_x, ctl_line_y);
			} else {
				perr ("handle number not specified (%i).", hnd_num);
			}

			//dbg (0, "bpress! *trk=%p *pathdef=%p subpath=%i vscale=%.2f\n", at, at->track->bezier.vol, arrange->auto_subpath, vscale);
			dragging = TRUE;
			gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM(at->grp_auto));
			break;

		case GDK_MOTION_NOTIFY:
			if (!dragging) return FALSE;
			dbg (2, "motion! abpx=%.2f mousey_trk=%.2f", mouse.x/hzoom(arrange), mouse_y_trk);

			gboolean is_last = automation_pt_is_last(curve, arrange->automation.sel.subpath);

			// bounds checking
			dbg (2, "  mouse_y=%.2f tpos=%i", mouse.y, c->track_pos[t]);
			if (shift) {
				mouse.x = *abp_px * hzoom(arrange); // x cannot change. y movement only.
			}else{
				double min_x;
				double max_x;
				if (hnd_num == 1) {
					min_x = (arrange->automation.sel.subpath > 1) ? arr_samples2px(arrange, ((ArtBpath*)curve->path)[arrange->automation.sel.subpath -2].x3) : 0.0f;
					max_x = (arrange->automation.sel.subpath > 0) ? arr_samples2px(arrange, ((ArtBpath*)curve->path)[arrange->automation.sel.subpath -0].x3) : 100000000.0;
				} else {
					min_x = (arrange->automation.sel.subpath > 0) ? arr_samples2px(arrange, ((ArtBpath*)curve->path)[arrange->automation.sel.subpath -1].x3) : 0.0f;
					max_x = is_last
						? arr_spos2px(arrange, &am_object_val(&song->loc[AM_LOC_END]).sp) + 10000
						: arr_samples2px(arrange, abp_next->x3);   // constrained by following pt.
					if (!abp_next->x3) pwarn ("abp_next->x3 == 0"); // trouble with last segment...
				}
				mouse.x = MAX(MIN(mouse.x, max_x), min_x);
			}
			double min_y = (double)trk_vpos + EDGE_WIDTH;
			double max_y = (double)at->height * vzoom(arrange) - EDGE_WIDTH;
			if (mouse_y_trk < 0){ mouse.y = min_y; mouse_y_trk = 0; }
			else mouse_y_trk = MIN(mouse_y_trk, max_y);

			dx = mouse.x - old_x;
			dy = mouse.y - old_y;

			gnome_canvas_item_set (pitem, 
				"x1", mouse.x - r, "y1", mouse_y_trk - r,
				"x2", mouse.x + r, "y2", mouse_y_trk + r,
				NULL);

			//-------------------------------------------

			// update the 2 curve subpath abp's
			*abp_px = arr_px2samples(arrange, mouse.x);
			*abp_py = pts->coords[1];

			dbg (3, "motion! y=%.2f dy=%.2f mouseytrk=%.2f abpxnext=%.2f-->%.2f", pts->coords[1], dy, mouse_y_trk, *abpy_next, *abpy_next + dy);
			if (hnd_num == 1) {
				// update the previous subpath
				*abpx_next = *abpx_next + arr_px2samples(arrange, dx);
				*abpy_next = *abpy_next + (dy * vscale);
			}else if(hnd_num == 2){
				// update the next subpath
				*abpx_next = *abpx_next + arr_px2samples(arrange, dx);
				*abpy_next = *abpy_next + (dy * vscale);
			}else{
				perr ("handle number not specified (%i).", hnd_num);
			}

			automation_gpath_update_and_emit(curve);
			automation_curve_draw_fast(arrange, curve);
  
			//-------------------------------------------

			// move the control line (nb: pts is 100normalised, ie unscaled)
			pts->coords[0] = mouse.x / hzoom(arrange);
			pts->coords[1] = mouse_y_trk / vscale;
			//FIXME no! this is incorrect - the curve has changed.
			pts->coords[2] = ctl_line_x;//set the "other" end of the line.
			pts->coords[3] = ctl_line_y;
			gnome_canvas_item_set (citem, "points", pts, NULL);


			// move the handle rect
			gnome_canvas_item_set(hitem, 
				"x1", pts->coords[2] * hzoom(arrange) -r, "y1", pts->coords[3] * vscale -r,
				"x2", pts->coords[2] * hzoom(arrange) +r, "y2", pts->coords[3] * vscale +r,
				NULL);

			// move control line for *adjacent* subpath
			if (!is_last) {
				pts->coords[0] = mouse.x / hzoom(arrange); // this end follows the mouse.
				pts->coords[1] = mouse_y_trk / vscale;
				pts->coords[2] = *abpx_next; // set the "other" end of the line.
				pts->coords[3] = *abpy_next;
				gnome_canvas_item_set (aitem, "points", pts, NULL);

				gnome_canvas_item_set(ahitem, 
					"x1", pts->coords[2] * hzoom(arrange) -r, "y1", pts->coords[3] * vscale -r,
					"x2", pts->coords[2] * hzoom(arrange) +r, "y2", pts->coords[3] * vscale +r,
					NULL
				);
			}

			//-------------------------------------------

			extern double ayyi_automation_xlate_out(double val);
			arr_statusbar_printf(arrange, 3, "%.2f", am_gain2db(ayyi_automation_xlate_out(*abp_py)));

			old_x = mouse.x;
			old_y = mouse.y;
			break;

		case GDK_BUTTON_RELEASE:
			dbg (0, "BUTTON_RELEASE");
			am_automation_point_change(curve, p, NULL, NULL);
			on_automation_change(NULL); // TODO check the curve has changed. - TODO this should be called from the am_automation_point_change_async callback.
			dragging = FALSE;
			break;
		default:
			break;
	}

	return HANDLED;
}


static gint
auto_hdl_cb (GnomeCanvasItem* item, GdkEvent* event, gpointer data)
{
	// Callback for mouse events on the automation control points handle rects.

	static int dragging;
	double mouse_x, mouse_y;       // mouse coords - canvas global.
	double mouse_y_trk;            // mouse position relative to the current track.
	static int trk_vpos;           // the top of the current track.
	static double r=3.0;           // handle radius
	static int t;                  // track number.
	static AMTrack* trk;
	static ArrTrk* atr;

	int hnd_num = GPOINTER_TO_INT(data); //either 1 or 2.
	static GnomeCanvasItem *hitem; // handle rect
	static GnomeCanvasItem *citem; // control line
	static GnomeCanvasPoints *pts;
	static ArtBpath *abp  = NULL;  // array of artbezierpaths for the current track.
	static double *abpx, *abpy;    // the curve pt that needs to be updated. Prolly doesnt need to be a ptr.
	static double abpx2, abpy2;    // the curve pt before the selected one.
	static int p;                  // the number of the current subpath.
	double hdl_x=0.0, hdl_y=0.0;   // mouse coords, but scaled and bounds checked. Used to set hdl position.

	mouse_x     = event->button.x;
	mouse_y     = event->button.y;
	mouse_y_trk = mouse_y - (double)trk_vpos;
	static double vscale = 1.0;    // we need the track vmag to reverse affines on the ctl lines.

	Arrange* arrange; if( !(arrange = ARRANGE_FIRST) ) return false;

	g_canvas* c = arrange->canvas->gnome;

	switch (event->type){
		case GDK_BUTTON_PRESS:
			t        = arrange->automation.sel.tnum;
			trk      = song->tracks->track[t];
			trk_vpos = c->track_pos[t];
			atr      = track_list__track_by_index(arrange->priv->tracks, t);
			vscale   = (double)atr->height * vzoom(arrange) / 100.0;
			//abp_c    = &(((ArtBpath*)song->tracks->track[t]->bezier.vol->path)[arrange->automation.sel.subpath]);
			abp      = (ArtBpath*)song->tracks->track[t]->bezier.vol->path;
			p        = arrange->automation.sel.subpath;
			//printf("auto_ctl_cb(): subpathnum=%i abp=%p\n", p, abp);

			if(hnd_num == 1){
				hitem = c->auto_hnd_1;
				citem = c->auto_ctl_1;
				pts   = c->auto_pts1;
				//abp_cx= &abp_c->x1;
				//abp_cy= &abp_c->y1;
				abpx  = &(abp[p].x1);
				abpx2 = abp[p-1].x3;
				abpy  = &(abp[p].y1);
				abpy2 = abp[p-1].y3;

			} else if(hnd_num == 2){
				hitem = c->auto_hnd_2;
				citem = c->auto_ctl_2;
				pts   = c->auto_pts2;
				//abp_cx= &abp_c->x2;
				//abp_cy= &abp_c->y2;
				abpx  = &(abp[p].x2);
				abpx2 = abp[p].x3;
				abpy  = &(abp[p].y2);
				abpy2 = abp[p].y3;

			} else {
				perr ("handle number not specified (%i).", hnd_num);
			}

			dragging = TRUE;
			gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM(atr->grp_auto));
			break;

		case GDK_MOTION_NOTIFY:
			if(!dragging) return FALSE;

			// bounds checking
			hdl_y = mouse_y_trk / vscale;
			if(hnd_num == 1){
				// check the hdl isnt before the prev curve pt
				hdl_x = MAX(mouse_x / hzoom(arrange), abpx2);
			} else if(hnd_num == 2){
				// check the hdl isnt past the next curve pt
				hdl_x = MIN(mouse_x / hzoom(arrange), abpx2);
			}

			gnome_canvas_item_set (hitem, 
							 //"x1", mouse_x -r, "y1", mouse_y_trk -r, 
							 "x1", hdl_x * hzoom(arrange)/*warning this zoom is reapplied!*/ -r, "y1", mouse_y_trk -r,
							 "x2", hdl_x * hzoom(arrange) +r, "y2", mouse_y_trk +r,
							 NULL);
			//and move the control line:
			pts->coords[0] = abpx2;
			pts->coords[1] = abpy2;
			pts->coords[2] = hdl_x;
			pts->coords[3] = hdl_y;
			gnome_canvas_item_set(citem, "points", pts, NULL);

			//-------------------------------------------

			//update the data for the complete automation curve and redraw:

			//1: update the curve subpath:
			*abpx = hdl_x;
			*abpy = pts->coords[3];

			//2: update the canvaspathdef:
			//currently the old path is deleted, and a fresh one made, as it seems simpler than
			// emptying it and adding the subpaths one by one.
			//printf("auto_ctl_cb(): motion! mouseytrk=%.2f pathA=%p\n", mouse_y_trk, trk->bezier.g_vol);
			g_return_val_if_fail(trk->bezier.vol->g_path, false);

			gnome_canvas_path_def_unref(trk->bezier.vol->g_path); //does this really free it??
			song->tracks->track[t]->bezier.vol->g_path = gnome_canvas_path_def_new_from_foreign_bpath((ArtBpath*)song->tracks->track[t]->bezier.vol->path);//FIXME use the updata func instead?
			//redraw the complete automation curve canvas item:
			gnome_canvas_item_set (AUTO_PATH(VOL)->pathitem, "bpath", trk->bezier.vol->g_path, NULL);

			break;
		case GDK_BUTTON_RELEASE:
			dragging = FALSE;
			break;
		default:
			break;
	}

	return TRUE;
}


/*
 *  Toggle global visibilty of automation curves.
 */
void
automation_view_toggle (Arrange* arrange)
{
	arrange_verify((AyyiPanel*)arrange);

	if (SHOW_AUTO) {
		automation_hide(arrange);
	} else {
		automation_show(arrange);
	}
}


void
automation_show (Arrange* arrange)
{
	g_assert(SHOW_AUTO);

	for (int t=0;t<AM_MAX_TRK;t++) {
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
		if(atr && atr->grp_auto && GNOME_IS_CANVAS_GROUP(atr->grp_auto)) gnome_canvas_item_show(GNOME_CANVAS_ITEM(atr->grp_auto));
	}
}


/*
 *  Hide all automation curves following state change.
 */
void
automation_hide (Arrange* arrange)
{
	g_assert(!SHOW_AUTO);

	int t;
	for(t=0;t<AM_MAX_TRK;t++){
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
		if(atr && atr->grp_auto && GNOME_IS_CANVAS_GROUP(atr->grp_auto)) gnome_canvas_item_hide(GNOME_CANVAS_ITEM(atr->grp_auto));
	}
}


gboolean
automation_view__get_segment (Arrange* arrange, BPath* abp, double mousex, int* seg)
{
	// Find which bezier path segment we are on

	g_return_val_if_fail(abp, false);
	g_return_val_if_fail(arrange, false);

	int i=0;
	double x = abp[0].x3;
	while(arr_samples2px(arrange, x) < mousex){
		if(abp[i].code == BP_END) break;
		i++;
		x = abp[i].x3;
	}
	if(i < 1){ perr ("bad subpath idx! (<1) x=%.2f", mousex); return FALSE; }

	*seg = i;
	dbg (2, "subpath=%i", i);
	return TRUE;
}


#if 0
static void
automation_load_dummy()
{
  //construct our own libart bpath so we can store it independently:
 
  //-an artbpath is a single curve seg.
  // The gnome fns expect an *array* of artbpath.
  //-The gnome fns have no idea of how long the array is.
  // (they will stop when they find an ART_END.)
  //-there *must* be an ART_END or the path_good check will fail.
  //-better to use ART_MOVETO_OPEN instead of ART_OPEN as there will be no check for continuity.

  //TODO move this to test.c 
 
  //double x1,y1,x2,y2,x3,y3;	//ctl_pt, ctl_pt, path_end.
  
  int t = 2;

  ArtBpath** points = track[t]->bezier.vol->path;
  ArtBpath* abp1 = points[0];
  ASSERT_POINTER(abp1, "track[t]->bezier.vol");
  abp1->code = ART_MOVETO_OPEN;
  abp1->x1 =   0.0;  	abp1->y1 =   0.0;
  abp1->x2 =   0.0;		abp1->y2 =   0.0;
  abp1->x3 =  50.0;  	abp1->y3 = 100.0-50.0;
  
  ArtBpath* abp2 = points[1];
  abp2->code = ART_CURVETO;
  abp2->x1 =  50.0;  	abp2->y1 = 100.0;
  abp2->x2 = 101.0;  	abp2->y2 =  99.0;
  abp2->x3 = 200.0;  	abp2->y3 = 100.0-50.0;
  
  ArtBpath* abp3 = points[2];
  abp3->code = ART_CURVETO;
  abp3->x1 = 210.0;  	abp3->y1 = 100.0- 55.0;
  abp3->x2 = 330.0;  	abp3->y2 = 100.0- 10.0;
  abp3->x3 = 350.0;  	abp3->y3 = 100.0-  0.0;

  ArtBpath* abp4 = points[3];
  abp4->code = ART_CURVETO;
  abp4->x1 = 360.0;  	abp4->y1 = 100.0- 10.0;
  abp4->x2 = 450.0;  	abp4->y2 = 100.0- 90.0;
  abp4->x3 = 500.0;  	abp4->y3 = 100.0-100.0;

  ArtBpath* abp5 = points[4];
  //*abp4 = {ART_END, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}; //whats the correct format??
  abp5->code = ART_END;
  
  ArtBpath* abp6 = points[5];
  
  //printf("  &0=%p  =%p\n", track[i]->bezier.vol, &(track[i]->bezier.vol[0]));
  //printf("  &1=%p  =%p\n", abp2,  &pathA[1]); 
  //printf("  &2=%p  =%p\n", abp3,  &pathA[2]); 
  //printf("  artbpath2->y3 = %.2f\n", track[i]->bezier.vol[1].y3);
  
  Curve* curve = track[t]->bezier.vol;
  automation_gpath_update_and_emit(curve)
  automation_pathitem_update(NULL, curve);

  //----------------------------------------------------------------

  //track automation. dummy curves:
  //GnomeCanvasPathDef *bpath = gnome_canvas_path_def_new_sized(4);
  //gnome_canvas_path_def_moveto(bpath,  0.0,  100.0-70.0);
  //gnome_canvas_path_def_lineto(bpath, 100.0, 100.0-90.0);
  //gnome_canvas_path_def_curveto(bpath,105.0, 100.0-95, 95, 100.0-105, 200.0, 100.0-100.0);
  //gnome_canvas_path_def_lineto(bpath, 300.0, 100.0-80.0);
  //gnome_canvas_path_def_lineto(bpath, 500.0, 100.0 -0.0);
  //track[4]->bezier.g_vol = bpath;

  t=3;
  points = track[t]->bezier.vol->path;

  abp1 = points[0];
  abp1->code = ART_MOVETO_OPEN;
  abp1->x1 =   0.0;  	abp1->y1 =   0.0;
  abp1->x2 =   0.0;		abp1->y2 =   0.0;
  abp1->x3 =   0.0;  	abp1->y3 = 100.0-70.0;

  abp2 = points[1];
  abp2->code = ART_LINETO;
  abp2->x1 =  50.0;  	abp2->y1 = 100.0;
  abp2->x2 = 101.0;  	abp2->y2 =  99.0;
  abp2->x3 = 100.0;  	abp2->y3 = 100.0-90.0;
  
  abp3 = points[2];
  abp3->code = ART_CURVETO;
  abp3->x1 = 105.0;  	abp3->y1 = 100.0- 95.0;
  abp3->x2 =  95.0;  	abp3->y2 = 100.0-105.0;
  abp3->x3 = 200.0;  	abp3->y3 = 100.0-100.0;

  abp4 = points[3];
  abp4->code = ART_LINETO;
  abp4->x1 = 360.0;  	abp4->y1 = 100.0- 10.0;
  abp4->x2 = 450.0;  	abp4->y2 = 100.0- 90.0;
  abp4->x3 = 300.0;  	abp4->y3 = 100.0-  0.0;

  abp5 = points[3];
  abp5->code = ART_LINETO;
  abp5->x1 = 360.0;  	abp5->y1 = 100.0- 10.0;
  abp5->x2 = 450.0;  	abp5->y2 = 100.0- 90.0;
  abp5->x3 = 500.0;  	abp5->y3 = 100.0-  0.0;

  abp6 = points[4];
  abp6->code = ART_END;
  //track[t]->bezier.g_vol = gnome_canvas_path_def_new_from_bpath(track[t]->bezier.vol);
  automation_gpath_update_and_emit(curve);
  automation_pathitem_update(NULL, track[t]->bezier.vol);
  
  //--------------------------------------------------------
  
  //bpath = gnome_canvas_path_def_new_sized(4);
  //gnome_canvas_path_def_moveto(bpath,  50.0, 100.0 -0.0);
  //gnome_canvas_path_def_lineto(bpath, 100.0, 100.0-90.0);
  //gnome_canvas_path_def_curveto(bpath,105.0, 100.0-95, 95, 100.0-105, 200.0, 100.0-100.0);
  //gnome_canvas_path_def_lineto(bpath, 300.0, 100.0-70.0);
  //gnome_canvas_path_def_lineto(bpath, 500.0, 100.0-70.0);
  //track[2]->bezier.g_vol = bpath;
  
  t=1;
  points = track[t]->bezier.vol->path;

  abp1 = points[0];
  abp1->code = ART_MOVETO_OPEN;
  abp1->x1 =   0.0;  	abp1->y1 =   0.0;
  abp1->x2 =   0.0;		abp1->y2 =   0.0;
  abp1->x3 =   0.0;  	abp1->y3 = 100.0- 0.0;

  abp2 = points[1];
  //abp2->code = ART_LINETO;
  abp2->code = ART_CURVETO;
  abp2->x1 =  50.0;  	abp2->y1 = 100.0;
  abp2->x2 = 101.0;  	abp2->y2 =  99.0;
  abp2->x3 = 100.0;  	abp2->y3 = 100.0-80.0;
  
  abp3 = points[2];
  abp3->code = ART_CURVETO;
  abp3->x1 = 105.0;  	abp3->y1 = 100.0- 95.0;
  abp3->x2 = 115.0;  	abp3->y2 = 100.0-105.0;
  abp3->x3 = 140.0;  	abp3->y3 = 100.0- 90.0;

  abp4 = points[3];
  abp4->code = ART_CURVETO;
  abp4->x1 = 180.0;  	abp4->y1 = 100.0- 10.0;
  abp4->x2 = 280.0;  	abp4->y2 = 100.0- 90.0;
  abp4->x3 = 300.0;  	abp4->y3 = 100.0- 70.0;

  abp5 = points[3];
  abp5->code = ART_CURVETO;
  abp5->x1 = 360.0;  	abp5->y1 = 100.0- 10.0;
  abp5->x2 = 450.0;  	abp5->y2 = 100.0- 90.0;
  abp5->x3 = 420.0;  	abp5->y3 = 100.0- 70.0;

  abp3 = points[4];
  abp3->code = ART_CURVETO;
  abp3->x1 = 460.0;  	abp3->y1 = 100.0- 95.0;
  abp3->x2 = 540.0;  	abp3->y2 = 100.0-105.0;
  abp3->x3 = 600.0;  	abp3->y3 = 100.0- 90.0;

  abp6 = points[5];
  abp6->code = ART_END;
  //track[t]->bezier.g_vol = gnome_canvas_path_def_new_from_bpath(track[t]->bezier.vol);
  automation_gpath_update_and_emit(curve);
  automation_pathitem_update(NULL, track[t]->bezier.vol);
}
#endif


void
g_automation_view_vol (ArrTrk* atr)
{
	// Bring volume automation curve to the front

	Arrange* arrange = atr->arrange;
	g_return_if_fail(CANVAS_IS_GNOME);

	g_return_if_fail(AUTO_PATH(VOL));
	g_return_if_fail(AUTO_PATH(VOL)->pathitem);

	gnome_canvas_item_show(AUTO_PATH(VOL)->pathitem);
}


bool
g_automation_view_pan (ArrTrk* atr)
{
	// Bring pan automation curve to the front.

	PF;

	g_return_val_if_fail(AUTO_PATH(PAN), false);
	g_return_val_if_fail(AUTO_PATH(PAN)->pathitem, false);

	gnome_canvas_item_show(AUTO_PATH(PAN)->pathitem);

	return true;
}


void
g_automation_on_new_curve (Arrange* arrange, Curve* curve)
{
    // add an item to the arrange track
    //ArrAutoPath* aap = AYYI_NEW(ArrAutoPath, //TODO not yet freed
    //	.curve = curve,
	//);
    ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, curve->track);
    //g_ptr_array_add(atr->auto_paths, aap);
    //dbg(0, "aap=%p n_paths=%i", atr->auto_paths, atr->auto_paths->len);

    // force previously undrawn curve items to be created. Possibly could be refactored...
    automation_track_draw(arrange, atr);
}


static bool
track__add_control (AMTrack* trk, Curve* curve)
{
	if(g_list_find(trk->controls, curve)){ pwarn("control is already added to this track."); return FALSE; }

	trk->controls = g_list_append(trk->controls, curve);

	return TRUE;
}


bool
g_automation_show_control (GtkWidget* widget, gpointer data)
{
	// Widget should be GtkMenuItem

	PF;
	Arrange* arrange = (Arrange*)app->active_panel;
	if (!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return FALSE;

	Curve* curve = NULL;

	int plugin_idx = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "plugin_idx"));
	if (plugin_idx) {
		// possibly a previously unused plugin control
		plugin_idx--; // restore zero indexing.
		dbg(0, "plugin_idx=%i", plugin_idx);

		// lets for now just use the first control
		AyyiContainer* controls = ayyi_plugin_get_controls(plugin_idx);
		AyyiControl* control = ayyi_mixer_container_get_item(controls, 0);

		// make a new Curve for this control
		TrackNum t = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(arrange->tracks.menu), "for_track_idx"));
		AMTrack* trk = song->tracks->track[t];
		int auto_type = g_list_length(trk->controls) + 2;
		AMPlugin* plugin = am_plugin__get_by_idx(plugin_idx);
		curve = curve_new(trk, auto_type, plugin);
		curve->name = control->name;
		track__add_control(trk, curve);
		g_automation_on_new_curve(arrange, curve);
	} else {
		curve = g_object_get_data(G_OBJECT(widget), "curve");
	}

	if (!curve) { pwarn("unknown control item."); return FALSE; }

	if (!curve->path) {
		dbg(0, "not yet created... should this be done earlier?");
		am_automation_make_empty(curve);
		g_automation_curve_draw(arrange, curve);
	}

	GnomeCanvasItem* pathitem = get_curve_pathitem(arrange, curve);
	g_return_val_if_fail(pathitem, false);
	if (pathitem)
		gnome_canvas_item_show(pathitem);
	else
		return false;

	return TRUE;
}


static void
g_automation_on_curve_change (Curve* curve)
{
	// TODO this is now (more or less) a dup of automation_update_gpath() below

	g_return_if_fail(curve);

	dbg(0, "%s len=%i", curve->name, curve_get_length(curve));

	ArtBpath* bpath = (ArtBpath*)curve->path;
	g_return_if_fail(bpath);

	g_clear_pointer(&curve->g_path, gnome_canvas_path_def_unref);
	curve->g_path = gnome_canvas_path_def_new_from_foreign_bpath(bpath);

	arrange_foreach {
		if (CANVAS_IS_GNOME) {
			arrange->canvas->gnome->auto_.dirty_curves = g_list_append(arrange->canvas->gnome->auto_.dirty_curves, curve);
		}
	} end_arrange_foreach

#ifdef DEBUG
	if (_debug_ > -1) am_automation_print_path(curve);
#endif
}


static void
automation_update_gpath (Curve* curve)
{
	PF;
	g_return_if_fail(curve);
	ArtBpath* bpath = (ArtBpath*)curve->path;
	g_return_if_fail(bpath);

	if (curve->g_path) gnome_canvas_path_def_unref(curve->g_path);
	curve->g_path = gnome_canvas_path_def_new_from_foreign_bpath(bpath);

	arrange_foreach {
		arrange->canvas->gnome->auto_.dirty_curves = g_list_append(arrange->canvas->gnome->auto_.dirty_curves, curve);
	} end_arrange_foreach
}


static void
automation_gpath_update_and_emit (Curve* curve)
{
	pwarn("deprecated - unclear semantics");

	// For simplicity, the old path is deleted, and a fresh one made

	GnomeCanvasPathDef* g_path = curve->g_path;

	if (g_path) {
		gnome_canvas_path_def_unref(g_path);
		curve->g_path = NULL;
	}

	am_song__emit ("curve-change", curve);
}


#if 0
static int
automation_bpath_get_end (ArtBpath* bpath)
{
	int i = 0;
	ArtBpath* bp;
	for (bp = bpath; bp->code != ART_END; bp++) i++;
	return i;
}
#endif


static bool
automation_pt_is_last (Curve* curve, int p)
{
	int len = gnome_canvas_path_def_length(curve->g_path); //get the length of the path
	if (len < p) { perr ("length too short: %i. seg=%i.", len, p); return FALSE; }
	return (p > (len - 3)); // 3 is empirically deduced.
}


void
arr_k_next_node (GtkAccelGroup* accel_group, Arrange* arrangeX)
{
	Arrange* arrange = (Arrange*)windows__get_active();
	g_return_if_fail(AYYI_IS_ARRANGE(arrange));

	int current = arrange->automation.sel.subpath;
	AMTrackNum t  = arrange->automation.sel.tnum;

	Curve* curve = NULL;
	int type = arrange->automation.sel.type;
	switch (type) {
		case VOL:
			curve = song->tracks->track[t]->bezier.vol;
			break;
		case PAN:
			curve = song->tracks->track[t]->bezier.pan;
			break;
		default:
			type -= 2;
			dbg(0, "t=%i", t);
			curve = am_track__lookup_curve(song->tracks->track[t], type);
			break;
	}
	g_return_if_fail(curve);
	g_return_if_fail(curve->g_path);

	if (automation_pt_is_last(curve, current)) return;
	int seg = current + 1;

	ArrAutoPath* aap = curve_find_aap(arrange, curve);

	automation_view__select_segment(arrange, aap, seg);
}


/*
 *  Redraw the curves listed in arrange->dirty_curves. If this list is empty, ALL curves are considered dirty.
 *
 *  TODO shis should be called from the am_automation_point_change_async callback.
 */
static void
on_automation_change (AyyiPanel* sender_win)
{
	PF;

	arrange_foreach {
		GList* l = arrange->canvas->gnome->auto_.dirty_curves;
		if (l) {
			dbg (3, "n_dirty_curves=%i", g_list_length(l));
			for (;l;l=l->next) {
				g_automation_curve_draw(arrange, l->data);
			}
			g_list_clear(arrange->canvas->gnome->auto_.dirty_curves);

		} else {
			dbg (0, "dirty_curves is empty. Redrawing all curves...");
			automation_on_curves_change(arrange);
		}
	}  end_arrange_foreach
}


static gint
track_curve_sel (GnomeCanvasItem* item, GdkEvent* event, ArrTrk* at)
{
	// Shows the control handles when an automation curve subpath is clicked on.

	g_return_val_if_fail(at, false);

	if (event->type == GDK_BUTTON_RELEASE) return HANDLED;
	if (event->type != GDK_BUTTON_PRESS) return false;

	Arrange* arrange = at->arrange;
	const int t      = am_track_list_position(song->tracks, at->track);
	g_return_val_if_fail(t >= 0 && t < AM_MAX_TRK, false);
	arrange->automation.sel.track = at;
	arrange->automation.sel.tnum = t;

	if (event->button.button == 3) {
		int x, y;
		arr_coords_win_to_track(arrange, &x, &y);
		automation_menu_popup(arrange, event, (Ptd){x, y});
	}

	ArrAutoPath* aap = get_aap_from_citem(at, item);
	g_return_val_if_fail(aap, HANDLED);
	arrange->automation.sel.type = aap->curve->auto_type;

	//we need to show the curve 'selection'.
	{
		// get the paths from the source data
		Curve* curve = aap->curve;
		BPath* abp = curve->path;
		g_return_val_if_fail(abp, false);

		// find which segment we are on
		int p = 0;
		if(automation_view__get_segment(arrange, abp, event->button.x, &p)){
			arrange->automation.sel.subpath = p;
		}

		gnome_canvas_item_raise_to_top(item);
		gnome_canvas_item_raise_to_top(GNOME_CANVAS_ITEM(at->grp_auto));

		automation_view__select_segment(arrange, aap, p);
	}

	return HANDLED;
}


static ArrAutoPath*
get_aap_from_citem (ArrTrk* at, GnomeCanvasItem* citem)
{
	for (int i=0;i<at->auto_paths->len;i++) {
		ArrAutoPath* aap = g_ptr_array_index(at->auto_paths, i);
		dbg(1, "  type=%i pathitem=%p '%s'", aap->curve->auto_type, aap->pathitem, aap->curve->name);
		if (aap->pathitem == citem) return aap;
	}

#ifdef DEBUG
	pwarn ("cannot get automation path from canvas item. item=%p listsize=%i", citem, at->auto_paths->len);
#endif
	return NULL;
}


#if 0
static void
arr_coords_win_to_canvas(Arrange* arrange, int* x, int* y)
{
	// Translate window coords into canvas coords.

	// Make sure arrange->mouse is set before calling this!

	*x = arrange->mouse.x - arrange->canvas->widget->allocation.x;
	*y = arrange->mouse.y - arrange->canvas->widget->allocation.y;
}
#endif


static void
arr_coords_win_to_track (Arrange* arrange, int* x, int* y)
{
	// Translate window coords into canvas coords for the track.

	// !!! make sure arrange->mouse and arrange->automation.sel.track are set before calling this! Currently they are only set for automation editing.

	AMTrackNum t = arrange->automation.sel.tnum;

	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);

	*x = arrange->mouse.x - arrange->canvas->widget->allocation.x + xscrolloffset;
	*y = arrange->mouse.y - arrange->canvas->widget->allocation.y - arrange->canvas->gnome->track_pos[t];
}

