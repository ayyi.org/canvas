" Vim syntax file
" Language:     C seismix

syn keyword seismixMacro    g_auto
syn keyword seismixMacro    g_autofree

syn keyword seismixType		TrackControl
syn keyword seismixType		TrackControlMidi
syn keyword seismixType		TrackControlAudio
syn keyword seismixType		ArrTrk
syn keyword seismixType		Arrange
syn keyword seismixType		AyyiArrangeClass
syn keyword seismixType		Filer
syn keyword seismixType		GnomeCanvasMap
syn keyword seismixType		GnomeCanvasPart
syn keyword seismixType		GnomeCanvasPartClass
syn keyword seismixType		GdlDockPlacement
syn keyword seismixType		GdlDockObject
syn keyword seismixType		GdlDockItem
syn keyword seismixType		GdlDockMaster
syn keyword seismixType		GdlDock
syn keyword seismixType		AyyiPanel
syn keyword seismixType		AyyiPanelClass
syn keyword seismixType		AyyiShell
syn keyword seismixType		MixerHBox
syn keyword seismixType		CanvasOp
syn keyword seismixType		TimeRuler
syn keyword seismixType		MixerWin
syn keyword seismixType		AyyiListWin
syn keyword seismixType		PoolWindow
syn keyword seismixType		AyyiPoolWin
syn keyword seismixType		InspectorWin
syn keyword seismixType		LogWindow
syn keyword seismixType		FilemanagerWin
syn keyword seismixType		ConfigWindow
syn keyword seismixType		GtkKeys
syn keyword seismixType		GtkKeysClass
syn keyword seismixType		Strip
syn keyword seismixType		AyyiMixerStrip
syn keyword seismixType		AyyiMixerStripPrivate
syn keyword seismixType		MasterStrip
syn keyword seismixType		StripInsert
syn keyword seismixType		ArrTrackNum
syn keyword seismixType		TrackDispNum
syn keyword seismixType		TrackList

syn keyword seismixConstant    TYPE_TRACK_CONTROL
syn keyword seismixConstant    GDL_IS_DOCK_CLASS
syn keyword seismixConstant    GDL_DOCK_BAR_BOTH
syn keyword seismixConstant    GDL_DOCK_TOP
syn keyword seismixConstant    GDL_DOCK_BOTTOM
syn keyword seismixConstant    GDL_DOCK_LEFT
syn keyword seismixConstant    GDL_DOCK_RIGHT
syn keyword seismixConstant    MAX_SCROLL_SPEED
syn keyword seismixConstant    SCROLL_TIMEOUT_MS
syn keyword seismixConstant    YAML_SCALAR_EVENT
syn keyword seismixConstant    YAML_MAPPING_START_EVENT
syn keyword seismixConstant    YAML_MAPPING_END_EVENT
syn keyword seismixConstant    LIST_MODE_ARRANGE
syn keyword seismixConstant    LIST_MODE_MIDI

syn keyword seismixMacro       AYYI_PANEL
syn keyword seismixMacro       AYYI_PANEL_CLASS
syn keyword seismixMacro       AYYI_PANEL_GET_CLASS
syn keyword seismixMacro       AYYI_TYPE_PANEL
syn keyword seismixMacro       AYYI_IS_PANEL
syn keyword seismixMacro       AYYI_IS_ARRANGE
syn keyword seismixMacro       AYYI_TYPE_ARRANGE
syn keyword seismixMacro       AYYI_TYPE_MIXER_WIN
syn keyword seismixMacro       AYYI_TYPE_INSPECTOR_WIN
syn keyword seismixMacro       AYYI_IS_MIXER_WIN
syn keyword seismixMacro       AYYI_IS_MIXER_STRIP
syn keyword seismixMacro       AYYI_MIXER_STRIP_GET_PRIVATE
syn keyword seismixMacro       AYYI_TYPE_MIXER_STRIP
syn keyword seismixMacro       AYYI_IS_LISTWIN
syn keyword seismixMacro       GDL_TYPE_DOCK
syn keyword seismixMacro       GDL_DOCK
syn keyword seismixMacro       GDL_DOCK_ITEM
syn keyword seismixMacro       GDL_DOCK_OBJECT
syn keyword seismixMacro       GDL_DOCK_MASTER
syn keyword seismixMacro       arrange_foreach
syn keyword seismixMacro       end_arrange_foreach
syn keyword seismixMacro       mixer_foreach
syn keyword seismixMacro       end_mixer_foreach
syn keyword seismixMacro       inspector_foreach
syn keyword seismixMacro       end_inspector_foreach
syn keyword seismixMacro       pool_foreach
syn keyword seismixMacro       end_pool_foreach
syn keyword seismixMacro       list_foreach
syn keyword seismixMacro       end_list_foreach
syn keyword seismixMacro       event_foreach
syn keyword seismixMacro       end_event_foreach
syn keyword seismixMacro       colour_foreach
syn keyword seismixMacro       end_colour_foreach
syn keyword seismixMacro       transport_foreach
syn keyword seismixMacro       end_transport_foreach
syn keyword seismixMacro       spectrogram_foreach
syn keyword seismixMacro       end_spectrogram_foreach
syn keyword seismixMacro       shell_foreach
syn keyword seismixMacro       end_shell_foreach
syn keyword seismixMacro       MIXER_HBOX
syn keyword seismixMacro       MIXER_IS_HBOX

" arrange/track_list.h
syn keyword SeismixFunction track_list__new
syn keyword ayyiFunction track_list__free
syn keyword ayyiFunction track_list__add
syn keyword ayyiFunction track_list__remove
syn keyword ayyiFunction track_list__move_to_pos
syn keyword ayyiFunction track_list__lookup_track
syn keyword ayyiFunction track_list__track_by_index
syn keyword ayyiFunction track_list__track_by_display_index
syn keyword ayyiFunction track_list__get_display_num
syn keyword ayyiFunction track_list__get_trk_from_display_index
syn keyword ayyiFunction arr_t_to_d
syn keyword ayyiFunction track_list__track_num_from_display_num
syn keyword ayyiFunction track_list__count_disp_items
syn keyword ayyiFunction track_list__prev_visible
syn keyword ayyiFunction track_list__next_visible
syn keyword ayyiFunction track_list__last_visible
syn keyword ayyiFunction track_list__update_all_pointers

" gl_track_control.h
syn keyword seismixType        TrackControlActor
syn keyword seismixType        TrackControlTrackActor

" canvas_op.h
syn keyword seismixConstant OP_NONE
syn keyword seismixConstant OP_MOVE
syn keyword seismixConstant OP_COPY
syn keyword seismixConstant OP_RESIZE_LEFT
syn keyword seismixConstant OP_RESIZE_RIGHT
syn keyword seismixConstant OP_REPEAT
syn keyword seismixConstant OP_SPLIT
syn keyword seismixConstant OP_DRAW
syn keyword seismixConstant OP_BOX
syn keyword seismixConstant OP_SCROLL
syn keyword seismixConstant OP_EDIT
syn keyword seismixConstant OP_VELOCITY
syn keyword seismixConstant OP_VDIVIDER
syn keyword seismixConstant OP_NOTE_LEFT
syn keyword seismixConstant OP_NOTE_RIGHT
syn keyword seismixConstant OP_NOTE_SELECT
syn keyword seismixConstant OP_AUTO_DRAG
syn keyword seismixConstant N_OP_TYPES

" Default highlighting
if version >= 508 || !exists("did_seismix_syntax_inits")
  if version < 508
    let did_seismix_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink seismixType               Type
  HiLink seismixFunction           Function
  HiLink seismixMacro              Macro
  HiLink seismixConstant           Constant
  HiLink seismixBoolean            Boolean
  HiLink seismixDebug              Debug
  delcommand HiLink
endif

