[![Build status](https://gitlab.com/ayyi.org/canvas/badges/master/pipeline.svg)](https://gitlab.com/ayyi.org/canvas)

![Screenshot](https://gitlab.com/ayyi.org/canvas/-/raw/master/resources/seismix-2020.webp "Screenshot")

Getting started
---------------

# DEPENDENCIES

 - gtk+-2
 - libsndfile
 - opengl >= 3
 - libyaml
 - dbus-glib
 - libxml2
 - librsvg2
 - graphene
 - to build Help, you need webkitgtk

At runtime, you need to have a backend such as [ayyi/ardourd](http://www.ayyi.org/ayyid1.html) which will be autostarted
on demand. See: http://www.ayyi.org/downloads


# BUILD FROM GIT

```
	./autogen.sh
	make
```

# BUILD FROM TARBALL

```
	./configure
	make
	# optionally do 'make install'.
```

Run under gdb and, if you have the time to, [report issues](https://gitlab.com/ayyi.org/canvas/issues), it is very much appreciated.

Gentoo users: ebuilds are available at http://www.ayyi.org/downloads.

See [ayyi.org](http://www.ayyi.org/) for more information.
