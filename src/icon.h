/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __icon_h__
#define __icon_h__

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define SM_STOCK_BLANK "sm-blank"
#define SM_STOCK_ZOOM_OUT "sm-zoom-out"
#define SM_STOCK_POINTER "sm-pointer"
#define SM_STOCK_AUTOMATION "automation"
#define SM_STOCK_HISTORY "history"

#define ICON_SIZE_32 7
#define ICON_SIZE_48 8
#define ICON_SIZE_64 9
#define ICON_SIZE_96 10

extern GtkIconTheme* icon_theme;

void  icons_init          ();
#ifdef WITH_VALGRIND
void  icons_uninit        ();
#endif
void  create_textures     ();
void  create_icon_texture (const char* name, int t);
void  create_cursors      ();

typedef struct {
    char* name;
    int size;
    guint texture;
} Icon;

typedef enum {
	ICON_REC = 0,
	ICON_REC_ON,
	ICON_SOLO,
	ICON_SOLO_ON,
	ICON_MUTE,
	ICON_MUTE_ON,
	ICON_TR_REC_32,
	ICON_TR_REW_32,
	ICON_STOP_32,
	ICON_PLAY_32,
	ICON_TR_FF_32,
	ICON_REFRESH_32,
	ICON_MAX
} IconTypes;

#endif
