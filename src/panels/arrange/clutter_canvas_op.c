/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2011 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

static char type_strings[7][16] = {"OP_NONE", "OP_MOVE", "OP_COPY", "OP_RESIZE_LEFT", "OP_RESIZE_RIGHT", "OP_SPLIT", "OP_BOX"};
static void cl_canvas_op__motion(CanvasOp*);
static void cl_canvas_op__none_motion(CanvasOp*);
static void cl_canvas_op__finish(CanvasOp*);

static void vertical_snap(CanvasOp*);


static bool
has_moved(CanvasOp* op, Ptf* final)
{
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;

	clutter_actor_get_transformed_position(CLUTTER_ACTOR(cop->item), &final->x, &final->y);

	dbg (0, "start=%.2f mouse=%.2f", op->start.x, op->mouse.x);
	//detect false moves:
	//-this appears to be ok. If we need more control maybe use a timer (events are timestamped!),
	//-we do the check in the Release phase to avoid the 'sticky' intertia feeling.
	if((op->max_movement > 2.0) && ABS(op->start.x - op->mouse.x) > 0.5){
		//dbg(0, "px=%i", op->p1.x);
		if(final) final->x = MAX(final->x, 0);
		return TRUE;
	}else{
		//stick with the original position:
		final->x = op->start.x - op->diff.x;
		return FALSE;
	}
}


static void
cl_canvas_op__motion(CanvasOp* op)
{
	//do not set old_x here, they must be set by the timer.

	op->max_movement = MAX(op->max_movement, ABS(op->mouse.x - op->start.x));
	//dbg(0, "mouse=%.2f", op->mouse.x);
}


static void
cl_canvas_op__move_press(CanvasOp* op, GdkEvent* event)
{
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	ClutterPart* cpart = CLUTTER_PART(cop->item);

	cl_canvas_op__press(op, event);

	int x = op->mouse.x - op->diff.x;
	int y = op->mouse.y - op->diff.y;
	cpart->target_position.x = x;
	cpart->target_position.y = y;

	arr_statusbar_printf(op->arrange, 2, "%i parts selected", 1);
}


static void
cl_canvas_op__move_motion(CanvasOp* op)
{
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	ClutterActor* actor = cop->item;//clutter_stage_get_actor_at_pos(CLUTTER_STAGE(op->arrange->canvas->cl->stage), op->new_x, op->new_y);
	if(!actor) return;
	ClutterPart* cpart = CLUTTER_PART(actor);

	AMTrack* old_track = op->part->track;
	vertical_snap(op);

	//int x = op->mouse.x - op->diff.x;
	//dbg(0, "%i x %i", x, y);
	cpart->target_position.x = op->mouse.x - op->diff.x; //TODO make this p1.x
	//cpart->target_position.y = y;
	cpart->target_position.y = op->arrange->track_pos[op->part->track->track_num];
	//dbg(0, "target=%.1f x %.1f", cpart->target_position.x, cpart->target_position.y);

	//dbg(0, "track=%i", op->part->track->track_num);
	if(old_track != op->part->track){
		dbg(0, "track changed.");
		clutter_part_resize(cpart, -1, op->arrange->track_pos[op->part->track->track_num + 1] - op->arrange->track_pos[op->part->track->track_num]);
	}

	//clutter_actor_set_position(group, x, y);

	cl_canvas_op__motion(op);
}


static void
cl_canvas_op__move_finish(CanvasOp* op)
{
	g_return_if_fail(op->part);
	ClutterPart* cpart = CLUTTER_PART(((ClutterCanvasOp*)op)->item);
	g_return_if_fail(cpart);
	PF;

/*
	part_local_destroy(op->part_local);
*/

	vertical_snap(op);

	cpart->target_position.x = op->mouse.x - op->diff.x;//op->px1; //FIXME px1 not set!
	dbg(0, "x=%.1f %.1f", cpart->target_position.x, op->mouse.x - op->diff.x);
	cpart->target_position.y = op->arrange->track_pos[op->part->track->track_num];

	Ptf final;
	if(has_moved(op, &final)){
		dbg (0, "  dropping... updating part data...");

		//final_x = arr_pos2px(op->arrange, &pos);
		GPos pos;
		arr_snap_px2pos(op->arrange, &pos, final.x);

		am_part__move(op->part, &pos, op->part->track, NULL, NULL);

		AyyiShell* shell = ((AyyiPanel*)op->arrange)->shell;
		if(shell->statusbar_drag_id.cid)
			gtk_statusbar_remove(GTK_STATUSBAR(shell->statusbar[0]), shell->statusbar_drag_id.cid, shell->statusbar_drag_id.mid);
	}

/*
	gnome_canvas_item_hide(op->arrange->xhair);
	gnome_canvas_item_hide(op->arrange->yhair);
	gnome_canvas_item_hide(GNOME_CANVAS_ITEM(op->target->group));
*/

	//event_update((AyyiPanel*)op->arrange);
	cl_canvas_op__finish(op);
}


static void
cl_canvas_op__copy_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	ClutterPart* cpart = CLUTTER_PART(cop->item);

	int x = op->mouse.x - op->diff.x;
	int y = op->mouse.y - op->diff.y;
	cpart->target_position.x = x;
	cpart->target_position.y = y;

	arr_statusbar_printf(op->arrange, 2, "%i parts selected", 1);
}


static void
cl_canvas_op__copy_motion(CanvasOp* op)
{
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	ClutterActor* actor = cop->item;
	if(!actor) return;
	ClutterPart* cpart = CLUTTER_PART(actor);

	//if(!op->part_local) op->part_local = part_local_new_from_part(op->arrange, op->mouse.x, op->mouse.y, op->part, FALSE); //TODO free

	TrackNum old_track = op->part->track->track_num;
	ArrTrk* at = canvas_op__get_track(op, op->p1.y);
	if(at) op->part->track = at->track;

	//int x = op->mouse.x - op->diff.x;
	cpart->target_position.x = op->mouse.x - op->diff.x; //TODO make this px1
	//cpart->target_position.y = y;
	cpart->target_position.y = op->arrange->track_pos[op->part->track->track_num];

	if(old_track != op->part->track->track_num){
		clutter_part_resize(cpart, -1, op->arrange->track_pos[op->part->track->track_num + 1] - op->arrange->track_pos[op->part->track->track_num]);
	}

	//clutter_actor_set_position(group, x, y);

	cl_canvas_op__motion(op);
}


static void
cl_canvas_op__copy_finish(CanvasOp* op)
{
	g_return_if_fail(op->part);
	ClutterPart* cpart = CLUTTER_PART(((ClutterCanvasOp*)op)->item);
	g_return_if_fail(cpart);
	PF;

/*
	part_local_destroy(op->part_local);
*/

	//vertical snapping:
	vertical_snap(op);
	dbg(0, "track=%s", op->part->track->name);

	cpart->target_position.x = op->mouse.x - op->diff.x;//op->px1; //FIXME px1 not set!
	dbg(0, "x=%.1f %.1f", cpart->target_position.x, op->mouse.x - op->diff.x);
	cpart->target_position.y = op->arrange->track_pos[op->part->track->track_num];

	Ptf final;
	if(has_moved(op, &final)){
		dbg (0, "  drop. copying part data...");
		GPos pos;
		arr_snap_px2pos(op->arrange, &pos, final.x);

		LocalPart* local = part_local_new_from_part(op->arrange, final.x, op->mouse.y, op->part, FALSE); //TODO free
		if(gpos_cmp(&pos, &op->part->start)){
			local->part.start = pos;
			song_add_part_from_prototype(&local->part);
		}
		else dbg(1, "part has not moved. not copying.");

		AyyiShell* shell = ((AyyiPanel*)op->arrange)->shell;
		if(shell->statusbar_drag_id.cid)
			gtk_statusbar_remove(GTK_STATUSBAR(shell->statusbar[0]), shell->statusbar_drag_id.cid, shell->statusbar_drag_id.mid);
	}

	cl_canvas_op__finish(op);
}


static void
vertical_snap(CanvasOp* op)
{
	ArrTrk* at = canvas_op__get_track(op, op->p1.y);
	if(at) op->part->track = at->track;
}


