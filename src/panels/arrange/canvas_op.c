/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __arrange_private__
#include "global.h"
#include "model/time.h"
#include "model/note_selection.h"
#include "model/midi_part.h"
#include "windows.h"
#include "support.h"
#include "part_manager.h"
#include "panels/arrange.h"
#include "panels/arrange/part.h"
#include "song.h"
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
#include "widgets/time_ruler.h"
#endif
#ifndef USE_GNOMECANVAS
#include "canvas_op.h"
#endif

extern GHashTable* canvas_types;


ArrTrk*
canvas_op__get_track (CanvasOp* op, double y)
{
	Arrange* arrange = op->arrange; 

	if (y < 0) return NULL;

	TrackDispNum d = arr_px2trk(arrange, y);
	return op->arrange->priv->tracks->display[d];
}


const char*
canvas_op__print_type (CanvasOp* op)
{
	g_return_val_if_fail(op->type < N_OP_TYPES, NULL);

	static char type_strings[N_OP_TYPES][16];

#define CASE(x) case OP_##x: { strcpy(type_strings[OP_##x], "OP_"#x); break; }
	switch (op->type) {
		CASE(NONE);
		CASE(MOVE);
		CASE(COPY);
		CASE(RESIZE_LEFT);
		CASE(RESIZE_RIGHT);
		CASE(REPEAT);
		CASE(SPLIT);
		CASE(DRAW);
		CASE(BOX);
		CASE(SCROLL);
		CASE(EDIT);
		CASE(VELOCITY);
		CASE(VDIVIDER);
		CASE(NOTE_LEFT);
		CASE(NOTE_RIGHT);
		CASE(NOTE_SELECT);
		case N_OP_TYPES:
			break;
	}
#undef CASE

	return type_strings[op->type];
}


/*
 *  Determine which part feature the cursor is over.
 */
FeatureType
pick_feature_type (CanvasOp* op)
{
	if (op->diff.y < 0) { /*pwarn("diff out of range: %.2f mouse=%.2f py=%.2f", op->diff.y, op->mouse.y, op->p1.y);*/ return FEATURE_NONE; }

	AMPart* part = op->part;
	g_return_val_if_fail(part, FEATURE_NONE);

	if (op->mouse.x > op->p2.x) return (pwarn("after part end"), FEATURE_NONE);
	if (op->mouse.x < op->p1.x) return (pwarn("before part start"), FEATURE_NONE);

	// features common to audio and midi
	double part_width_px = op->p2.x - op->p1.x;
	if (part_width_px > 4) {
		double edge_size = MIN(4.0, (part_width_px) / 4);
		if (op->p2.x - op->mouse.x < edge_size) {
			return FEATURE_PART_RIGHT;
		}
		else if (op->mouse.x - op->p1.x < edge_size) {
			return FEATURE_PART_LEFT;
		}
	}

	op->ready = 0;

	if (PART_IS_MIDI(part)) {
		Arrange* arrange = op->arrange;
		return call_i(arrange->canvas->pick_feature, op);

	}
	return FEATURE_NONE;
}


#ifdef DEBUG
const char*
print_feature_type (FeatureType type)
{
	static char str[N_FEATURE_TYPES][32] = {"FEATURE_NONE", "FEATURE_PART_LEFT", "FEATURE_PART_RIGHT", "FEATURE_VEL_AREA", "FEATURE_VEL_DIVIDER", "FEATURE_VEL_BAR_TOP", "FEATURE_NOTE_AREA", "FEATURE_NOTE", "FEATURE_NOTE_LEFT", "FEATURE_NOTE_RIGHT"};
	return str[type];
}
#endif


Optype
optype_from_tooltype (ToolType tool)
{
	Optype o = OP_BOX;

	switch (tool) {
		case TOOL_SCISSORS:
			o = OP_SPLIT;
			break;
		case TOOL_PENCIL:
			o = OP_DRAW;
			break;
		case TOOL_SCROLL:
			o = OP_SCROLL;
			break;
		case TOOL_MUTE:
		case TOOL_MAG:
		case TOOL_MAX:
		default:
			break;
	}
	return o;
}


bool
canvas_op__is_left_edge (CanvasOp* op)
{
	return op->bnew.x - op->xscrolloffset < SCROLL_EDGE_WIDTH;
}


bool
canvas_op__is_right_edge (CanvasOp* op)
{
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &op->arrange->canvas->type);
	int tc_width = (cc->provides & PROVIDES_TRACKCTL) ? op->arrange->tc_width.val.i : 0;

	return op->bnew.x - op->xscrolloffset > op->arrange->canvas_scrollwin->allocation.width - tc_width - SCROLL_EDGE_WIDTH;
}


GList*
canvas_op__get_note_selection (CanvasOp* op, MidiPart* part)
{
	GList* l = op->note_selection;
	for (;l;l=l->next) {
		NoteSelectionListItem* item = l->data;
		if (item->part == part) return item->notes;
	}
	return NULL;
}


void
canvas_op__motion (CanvasOp* op)
{
	op->bnew.x = op->mouse.x;

	// vertical bounds checking
	switch (op->type) {
		case OP_VELOCITY:
			op->bnew.y = CLAMP(
				op->mouse.y,
				op->p1.y, // FIXME not bounded.
				op->p2.y
			);
			break;
		default:
			;ArrTrk* last_track = track_list__last_visible(op->arrange->priv->tracks);
			int tracks_bottom = last_track ? (last_track->y + last_track->height) * vzoom(op->arrange) : 4096;

			// this only works for op types where the Part is moved (due to use of op->diff.y).
			op->bnew.y = CLAMP(
				op->mouse.y,
				op->diff.y + PART_OUTLINE_WIDTH,
				tracks_bottom - (op->p2.y - op->p1.y) + op->diff.y + PART_OUTLINE_WIDTH
			);
			break;
	}

	int max_movement = MAX(ABS(op->start.x - op->mouse.x), ABS(op->start.y - op->mouse.y));
	op->max_movement = MAX(max_movement, op->max_movement);

#ifndef DEBUG_DISABLE_RULERBAR
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	if (op->arrange->ruler) time_ruler_set_pos(TIME_RULER(op->arrange->ruler), op->mouse.x);
#endif
#endif
}


#ifdef NEVER
static gboolean   
arr_resize_mouse_timeout (struct _pt* origin)
{
	// Handles arrange window scrolling during a part resize operation.
	// Always called by a timeout.

	// perhaps should be merged with scroll_mouse_timeout().
	// -differences: it scrolls in the opposite direction.

	// FIXME timer isnt imediately stopped when mouse is released.

	Arrange* arrange; if (!(arrange = ARRANGE_FIRST) ) return false;

	static int scroll_dx = SCROLL_MULTIPLIER; // maybe add this as a g_object property of the timer?
	if (!origin) { scroll_dx = SCROLL_MULTIPLIER; return FALSE; } // hack!! passing NULL data resets the scroll speed.

	gboolean stop = TRUE;
	static double direction = -1.0;

	GdkWindow* window = arrange->canvas->widget->window;
	ASSERT_POINTER_FALSE(window, "window");

	gint x=0, y=0;
	gdk_window_get_pointer(window, &x, &y, NULL);
	gint topwinx=0, topwiny=0;
	gdk_window_get_pointer(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &topwinx, &topwiny, NULL);
	gint rootx, rooty;
	gdk_window_get_origin(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &rootx, &rooty);

	// how big is the screen?
	//GdkScreen *screen = gdk_screen_get_default(); //this may not be the right screen in a multiscreen setup!!

	// do the bounds checking again in case the mouse has moved:
	if (x > arrange->canvas_scrollwin->allocation.width - SCROLL_EDGE_WIDTH) {
		// we are at the rhs edge.
		double a = (scroll_dx * direction) / SCROLL_MULTIPLIER;
		arr_scroll_left(arrange, a);
		origin->x = origin->x - a;  //update startx so we have an uptodate ref point.
		stop = FALSE;
		// update the scroll speed
		scroll_dx = MIN((int)(scroll_dx * 1.0 + 1), SCROLL_MAX_SPEED * SCROLL_EDGE_WIDTH);
	}

	if (stop){ arrange->mouse_timer = 0; /*printf("  timer stopped.\n");*/}
	return !stop; //0==stop
}
#endif


void
canvas_op__check_song_length (CanvasOp* op, double x)
{
	// Check the given x coord to see if it is 'off the end', and if neccesary updates the song length.

	// adjust value to take the scrollwindow position into account: ??

	// TODO this should not update the song length, it should just just adjust the visible area

	if (arr_canvas_width(op->arrange) < x + ARR_SONG_END_MARGIN) {
		am_song__set_end(arr_px2beat(op->arrange, x));
	}
}
