/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

static void pan_free (AGlActor*);

static AGlActorClass pan_class = {0, "Pan", NULL, pan_free};

typedef struct {
	AGlActor     actor;
	Observable*  level;
} Pan;

typedef struct {
	AGlShader    shader;
	struct {
		uint32_t colour;
		float    value;
		float    width;
	}            uniform;
} PanShader;


static void pan_set_uniforms();

static PanShader pan_shader = {{NULL, NULL, 0, agl_null_uniforms, pan_set_uniforms, &pan_text}};


static void
pan_set_uniforms ()
{
	float colour[4] = {0.0, 0.0, 0.0, ((float)(pan_shader.uniform.colour & 0xff)) / 0x100};
	agl_rgba_to_float(0xff9966ff, &colour[0], &colour[1], &colour[2]);
	glUniform4fv(glGetUniformLocation(pan_shader.shader.program, "colour"), 1, colour);

	glUniform1f(glGetUniformLocation(pan_shader.shader.program, "width"), pan_shader.uniform.width);
	glUniform1f(glGetUniformLocation(pan_shader.shader.program, "value"), pan_shader.uniform.value);
}


static AGlActor*
pan_new (AMChannel* channel)
{
	bool pan__paint (AGlActor* actor)
	{
		pan_shader.uniform.colour = 0xff9966ff;
		pan_shader.uniform.value = 0.5;
		pan_shader.uniform.width = agl_actor__width(actor);
		agl_use_program((AGlShader*)&pan_shader);
		agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));

		return true;
	}

	void pan_set_size(AGlActor* actor)
	{
		actor->region.x2 = agl_actor__width(actor->parent);
	}

	bool pan__on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		return NOT_HANDLED;
	}

	void pan_init (AGlActor* actor)
	{
		if(agl->use_shaders){
			if(!pan_shader.shader.program)
				agl_create_program((AGlShader*)&pan_shader);
		}
	}

	Pan* pan = agl_actor__new(Pan,
		.actor = {
			.class    = &pan_class,
			.init     = pan_init,
			.paint    = agl->use_shaders ? pan__paint : agl_actor__null_painter,
			.set_size = pan_set_size,
			.on_event = pan__on_event
		},
		.level = channel->level
	);

	return (AGlActor*)pan;
}


static void
pan_free (AGlActor* actor)
{
	g_free(actor);
}

