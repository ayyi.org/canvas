/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __list_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>

#include "model/time.h"
#include "model/midi_part.h"
#include "model/part_manager.h"
#include "model/track_list.h"
#include "model/note_selection.h"
#include "windows.h"
#include "window.statusbar.h"
#include "support.h"
#include "song.h"
#include "toolbar.h"
#include "part_manager.h"
#include "icon.h"
#include "../shortcuts.h"
#include "list.h"

typedef struct _editor Editor;

typedef struct
{
    AMPart* part;
    MidiNote* note;
} PartNote;

static GObject*     list_construct               (GType, guint n_construct_properties, GObjectConstructParam*);

static void         list_on_change               (AyyiListWin*);

static void         list_on_name_edited          (AyyiListWin*, int, gchar*, uint64_t);
static void         list_on_time_edited          (AyyiListWin*, int, gchar*, uint64_t);
static void         list_on_val1_edited          (AyyiListWin*, int, gchar*, uint64_t);
static void         list_on_val_edited           (GtkCellRendererText*, gchar* path_string, gchar* new_text, Editor*);

static void         list_on_allocate             (GtkWidget*, GtkAllocation*);
static void         list_win_update              (AyyiListWin*);
static void         list_store_clear             (AyyiListWin*);
static void         list_store_arrange           (AyyiListWin*);
static void         list_store_midi              (AyyiListWin*, GList* parts);
static bool         list_note_is_selected        (AyyiListWin*, AMPart*, MidiNote*);
static void         list_cell_bg_lighter         (GtkTreeViewColumn*, GtkCellRenderer*, GtkTreeModel*, GtkTreeIter*);
static gint         list_drag_received           (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer user_data);
static GtkTreeViewColumn* list_add_column        (AyyiListWin*, gint colnum, const gchar* title, Editor*);
static bool         list_on_button_pressed       (GtkTreeView*, GdkEventButton*, gpointer);
static bool         list_on_scroll               (GtkTreeView*, GdkEventScroll*, gpointer);
static void         list_on_cursor_changed       (GtkTreeView*, gpointer);
static bool         list_selection_changed       (AyyiListWin*, GList* parts);
static void         list_select_row              (AyyiListWin*, GtkTreePath*);
static void         list_select_parts            (AyyiListWin*, GList* parts);
static void         list_delete_selected         (AyyiListWin*);
static GList*       list_get_selection           (AyyiListWin*);
static void         list_clear_selection         (AyyiListWin*);
#ifdef NEVER
static gboolean     list_win_part_is_selected    (AyyiListWin*, AMPart*);
#endif
static void         list_editing_cancelled       (GtkCellRenderer*, gpointer);
static void         list_accels_connect          (GtkCellRenderer*, GtkCellEditable*, gchar* path, gpointer treeview);
static void         list_accels_disconnect       (GtkCellRenderer*, GtkCellEditable*, gchar* path, gpointer treeview);
static GtkTreePath* list_part_get_path           (AyyiListWin*, AMPart*);
static GList*       list_get_paths_for_selection (AyyiListWin*);
static void         list_show_arr_selection      (AyyiListWin*);

static GtkWidget*   list_menu_init               (AyyiListWin*);
static void         list_show_popupmenu          (GtkWidget* treeview, GdkEventButton*, gpointer);
#ifdef DEBUG
static void         list_print_row               (AyyiListWin*, GtkTreePath*);
#endif
static GList*       list_get_tree_selected       (AyyiPanel*);
static void         list_receive_selection_notify(AyyiPanel*);

static void         list_on_song_load            (GObject*, gpointer);
static void         list_on_parts_change         (GObject*, gpointer);
static void         list_on_part_change          (GObject*, AMPart*, AMChangeType, AyyiPanel* sender, gpointer);
static void         list_on_part_selection_change(GObject*, AyyiPanel*, gpointer);
static void         list_on_note_selection_change(GObject*, AyyiPanel*, GList*, gpointer);
static void         list_on_link_change          (AyyiPanel*);
static void         list_on_ready                (AyyiPanel*);

static void         list_on_delete_key           (GtkWidget* accel_group, gpointer);

static void         _track_to_string             (char*, AMTrack*);
static AMTrack*     _track_from_string           (const char*);

G_DEFINE_TYPE (AyyiListWin, ayyi_listwin, AYYI_TYPE_PANEL)

enum {
    LIST_MODE_ARRANGE,
    LIST_MODE_MIDI,
};

enum {
    COLUMN_TIME,
    COLUMN_TYPE_STR,
    COLUMN_VAL1,
    COLUMN_VAL2,
    COLUMN_LABEL,
    COLUMN_NUM,
    COLUMN_OBJIDX,
    COLUMN_COLOUR,
    COLUMN_COLOUR_SET,
    COLUMN_FG_COLOUR,
    COLUMN_FG_COLOUR_SET,
    N_COLUMNS
};

struct _editor
{
    AyyiListWin* panel;
    void         (*callback)(AyyiListWin*, int, gchar*, uint64_t);
    int          column;
};


static void
ayyi_listwin_class_init (AyyiListWinClass* klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	panel->name           = "List";
	panel->has_link       = true;
	panel->has_follow     = true;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F4;
	panel->default_size.x = 360;
	panel->default_size.y = 400;

	panel->on_link_change       = list_on_link_change;
	panel->on_ready             = list_on_ready;
	panel->get_selected_parts   = list_get_tree_selected;

	widget_class->size_allocate = list_on_allocate;

	g_object_class->constructor = list_construct;

	g_signal_connect(song, "song-load", G_CALLBACK(list_on_song_load), NULL);
	g_signal_connect(song->parts, "change", G_CALLBACK(list_on_parts_change), NULL);
	g_signal_connect(am_parts, "item-changed", G_CALLBACK(list_on_part_change), NULL);
	g_signal_connect(am_parts, "selection-change", G_CALLBACK(list_on_part_selection_change), NULL);
	am_song__connect("note-selection-change", G_CALLBACK(list_on_note_selection_change), NULL);

	void dummy_callback(AyyiPanel* panel){}

	AMAccel list_key[] = {
		{"Dummy", {{(char)'%', GDK_CONTROL_MASK}, {0, 0}}, dummy_callback, NULL, NULL}, //just testing
	};
	panel->action_group = shortcuts_add_group(panel->name);
	panel->accel_group = gtk_accel_group_new();
	make_accels(panel->accel_group, panel->action_group, list_key, G_N_ELEMENTS(list_key), NULL);
}


static GObject*
list_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_listwin_parent_class)->constructor(type, n_construct_properties, construct_param);

	AyyiListWin* list_win = (AyyiListWin*)g_object;

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_LISTWIN);

	list_win->list_store = gtk_list_store_new (N_COLUMNS,
		G_TYPE_STRING, //time
		G_TYPE_STRING, //type
		G_TYPE_STRING, //val1
		G_TYPE_STRING, //val2
		G_TYPE_STRING, //label.
		G_TYPE_INT,
		G_TYPE_UINT64, // obj idx
		G_TYPE_STRING, // colour
		G_TYPE_BOOLEAN,// colour set
		G_TYPE_STRING, // fg colour
		G_TYPE_BOOLEAN // fg colour set
	);

	return g_object;
}


static void
ayyi_listwin_init (AyyiListWin* object)
{
}


static void
list_on_ready (AyyiPanel* panel)
{
	/*
	window
	+--dock
	  +--dockItem
	    +---eventBox
	      +--vbox
	        +--toolbar
	        +--scrollwin
	          +--treeview

	model:
		-windows do not share the treemodel as they can show different objects.
		 They could conceivably share a model when in Link mode.

	*/

	AyyiListWin* list_win = (AyyiListWin*)panel; 
	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_LISTWIN);
	if(list_win->sort_model) return;

	AMAccel mod_keys[] = {
		{"Delete", {{GDK_Delete, 0,}, {0, 0}}, list_on_delete_key, NULL, NULL},
	};

	if(k->instances <= 1){
		GtkAccelGroup* mod_accels = gtk_accel_group_new();
		make_accels(mod_accels, k->action_group, mod_keys, G_N_ELEMENTS(mod_keys), NULL);
		k->mod_accels = g_list_append(k->mod_accels, mod_accels);
	}
	gtk_window_add_accel_group(GTK_WINDOW(panel->window), k->mod_accels->data);

	toolbar_add_separator(panel, 20);
  
	GtkWidget* scroll = gtk_scrolled_window_new(NULL, NULL); // adjustments created automatically.
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	//==================== view =================================

	//to make the view independently sortable, it is wrapped first:
	GtkTreeModel* sort_model = list_win->sort_model = gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(list_win->list_store));

	GtkWidget* tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(sort_model));
	list_win->treeview = (GtkTreeView*)tree;
	gtk_widget_show(tree);
	gtk_container_add(GTK_CONTAINER(scroll), tree);
	panel_pack(scroll, EXPAND_TRUE);

	void list_on_val2_edited (AyyiListWin* panel, int rownum, gchar* new_text, uint64_t id)
	{
		dbg(1, "%s", new_text);
		switch(panel->mode){
			case LIST_MODE_MIDI:
				dbg(1, "midi");
				int note_val = atoi(new_text);
				if(note_val > -1 && note_val < 128){
					AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id);
					if(part){
						MidiNote* note = am_midi_part__get_note_by_idx(part, rownum);
						am_part__set_note_selection(part, g_list_append(NULL, note));

						GList* note_list = NULL;
						note_selection_list__new(&note_list, part);
						NoteSelectionListItem* item = note_list->data;
						if(item->notes){
							TransitionalNote* n = item->notes->data;
							n->transient.note = note_val;
							note_selection_list__print(&note_list);
							am_midi_part__set_notes(part, item->notes, NULL, NULL);
						}
					}
				}
				break;
			case LIST_MODE_ARRANGE:
				;AMTrack* trk =_track_from_string(new_text);
				if(trk){
					AMPart* part = am_partmanager__get_by_id(id);
					if(part) am_part_set_track(part, trk, NULL, NULL);
				}
				else shell__statusbar_print(((AyyiPanel*)panel)->window, 0, "no track: %s", new_text);
				break;
			default:
				dbg(1, "unhandled");
		}
	}

	Editor (*editors)[4] = g_malloc0(4 * sizeof(Editor));
	(*editors)[0] = (Editor){
		.panel    = list_win,
		.callback = list_on_name_edited,
		.column   = COLUMN_LABEL,
	};
	(*editors)[1] = (Editor){
		.panel    = list_win,
		.callback = list_on_time_edited,
		.column   = COLUMN_TIME,
	};
	(*editors)[2] = (Editor){
		.panel    = list_win,
		.callback = list_on_val1_edited,
		.column   = COLUMN_VAL1,
	};
	(*editors)[3] = (Editor){
		.panel    = list_win,
		.callback = list_on_val2_edited,
		.column   = COLUMN_VAL2,
	};

	void remove_edit_closure (gpointer editors, GObject* was) { g_free(editors); }
	g_object_weak_ref((GObject*)panel, remove_edit_closure, editors);

	GtkTreeViewColumn* column_time = list_add_column(list_win, 0, "time", &(*editors)[1]);
	                                 list_add_column(list_win, 1, "type", NULL);
	list_win->column_val1          = list_add_column(list_win, 2, "val1", &(*editors)[2]);
	list_win->column_val2          = list_add_column(list_win, 3, "val2", &(*editors)[3]);
	list_win->column_label         = list_add_column(list_win, 4, "label", &(*editors)[0]);
	GtkTreeViewColumn* column_num  = list_add_column(list_win, 5, "num", NULL);
	                                 list_add_column(list_win, COLUMN_OBJIDX, "obj", NULL);

	// this makes the column heading sortable.
	// -it doesnt initially sort by default.
	gtk_tree_view_column_set_sort_column_id(column_num,             COLUMN_NUM);
	gtk_tree_view_column_set_sort_column_id(column_time,            COLUMN_TIME);
	gtk_tree_view_column_set_sort_column_id(list_win->column_val2,  COLUMN_VAL2);
	gtk_tree_view_column_set_sort_column_id(list_win->column_label, COLUMN_LABEL);
	gtk_tree_view_column_set_resizable     (column_num,             TRUE);
	gtk_tree_view_column_set_resizable     (column_time,            TRUE);
	gtk_tree_view_column_set_resizable     (list_win->column_label, TRUE);
	gtk_tree_view_column_set_reorderable   (column_num,             TRUE);
	gtk_tree_view_column_set_reorderable   (column_time,            TRUE);
	gtk_tree_view_column_set_reorderable   (list_win->column_label, TRUE);

	gtk_tree_view_column_set_resizable     (column_num,             TRUE);
	gtk_tree_view_column_set_resizable     (column_time,            TRUE);
	gtk_tree_view_column_set_resizable     (list_win->column_val1,  TRUE);
	gtk_tree_view_column_set_resizable     (list_win->column_val2,  TRUE);
	gtk_tree_view_column_set_resizable     (list_win->column_label, TRUE);

	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree));
	gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);

	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(sort_model), COLUMN_TIME, GTK_SORT_ASCENDING);

	list_win->cursor_handler_id = g_signal_connect(G_OBJECT(tree), "cursor-changed", G_CALLBACK(list_on_cursor_changed), list_win);
	g_signal_connect(tree, "button-press-event", (GCallback)list_on_button_pressed, list_win);
	g_signal_connect(tree, "scroll-event", (GCallback)list_on_scroll, list_win);
	g_signal_connect(G_OBJECT(panel->dnd_ebox), "drag-data-received", G_CALLBACK(list_drag_received), NULL);

	list_on_change(list_win);

	list_win->menu = list_menu_init(list_win);
	g_signal_connect(tree, "popup-menu", (GCallback)list_show_popupmenu, NULL); // SHIFT-F10 pressed.
}


static void
list_on_allocate (GtkWidget* panel, GtkAllocation* allocation)
{
	((GtkWidgetClass*)ayyi_listwin_parent_class)->size_allocate(panel, allocation);

	static gboolean first_time = TRUE;
	if(first_time && GTK_WIDGET_REALIZED(panel)){
		gtk_widget_set_size_request(panel, 20, 20);
		first_time = FALSE;
	}
}


static void
list_win_update (AyyiListWin* list)
{
	// this fn doesnt check window Link status.

	g_return_if_fail(list);

	if(am_parts_selection){      // show Part contents
		GList* show = NULL;
		GList* p = am_parts_selection;
		for(;p;p=p->next){
			AMPart* part = p->data;
			if(!PART_IS_MIDI(part)) continue;
			show = g_list_append(show, part);
		}
		if(show){
			list_store_midi(list, show);
			g_list_clear(show);
		}else{
			dbg(2, "nothing to show");
			list_store_arrange(list);
		}
	}else{
		dbg (2, "nothing selected.");
		list_store_arrange(list); // show list of Parts.
		list_select_parts(list, am_parts_selection);
		return;
	}
}


static void
list_store_clear (AyyiListWin* list_win)
{
	gtk_list_store_clear(list_win->list_store);
	if(list_win->src){ g_list_free(list_win->src); list_win->src = NULL; }
}


static void
list_store_arrange (AyyiListWin* list_win)
{
	// Fill the list window list_store with Arrange window events (eg parts).
	// -for now it is infact just parts.

	GtkListStore* list_store = list_win->list_store;
	g_return_if_fail(list_store);

	char          time[AYYI_BBST_MAX];
	GtkTreeIter   iter;
	static gchar* type = "PART";
	static char   val1[64];
	static char   val2[64];
	static char   label[AYYI_NAME_MAX];

	list_win->mode = LIST_MODE_ARRANGE;

	//it is possible that the data has not changed. But we dont check that:
	list_store_clear(list_win);

	ASSERT_SONG_SHM;

	int p = -1;
	GList* i = song->parts->list;
	for(;i;i=i->next, p++){
		AMPart* part = i->data;

		g_strlcpy(label, part->name, AYYI_NAME_MAX);
		ayyi_pos2bbss(&(part->start), time);

#if DEBUG
		if(PART_IS_AUDIO(part)) dbg(2, "PART_IS_AUDIO");
#endif
		AyyiRegionBase* shared = am_part_get_shared(part);
		if (!shared){ perr ("cannot get shared data for part. pod_index=%i", part->ayyi->shm_idx); continue; }

		uint64_t idx = shared->id;
		dbg (2, "id=%Lu", idx);

		_track_to_string(val2, part->track);

		char colour[8];
		colour_get_rgb_str(colour, part->bg_colour);

		char fg_colour[8];
		am_part__get_fg_colour(part, fg_colour, song->palette);

		//add a new row to the model:
		gtk_list_store_append (list_store, &iter);
		gtk_list_store_set (list_store, &iter,
                        COLUMN_TIME,       time,
                        COLUMN_TYPE_STR,   type,
                        COLUMN_VAL1,       am_part__get_length_bbst(part, val1),
                        COLUMN_VAL2,       val2,
                        COLUMN_LABEL,      label,
                        COLUMN_NUM,        p,
                        COLUMN_OBJIDX,     idx,
                        COLUMN_COLOUR,     colour,
                        COLUMN_COLOUR_SET, TRUE,
                        COLUMN_FG_COLOUR,  fg_colour,
                        COLUMN_FG_COLOUR_SET, TRUE,
                        -1);

		// is the part selected?
#if 0
		#warning should this be using the sort_model instead?
		//this is silly anyway - we already know the part..
		if(am_partmanager__is_selected(part)){}
		GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(list_store), &iter);
		ASSERT_POINTER(path, "path");
		if(list_win_part_is_selected(list_win, part)) list_select_row(list_win, path);
		gtk_tree_path_free(path);
#endif

		// currently val1 column shows the track number:
		gtk_tree_view_column_set_title(list_win->column_val1,  "length");
		gtk_tree_view_column_set_title(list_win->column_val2,  "track");
		gtk_tree_view_column_set_title(list_win->column_label, "label");

		// as the store will keep a copy of any strings etc internally, we can free the data.
		// -however, as the data is all statics, i dont think there is anything to free.
	}

	list_show_arr_selection(list_win);
}


static void
list_show_arr_selection (AyyiListWin* list_win)
{
	GList* selected = list_get_paths_for_selection(list_win);
	dbg(3, "selected.size=%i", g_list_length(selected));
	if(selected){
		GList* l = selected;
		for(;l;l=l->next){
			GtkTreePath* path = l->data;
			if(path){
				//list_print_row(list_win, path);
				list_select_row(list_win, path);
				gtk_tree_path_free(path);
			}
		}
		g_list_free(selected);
	}
}


static void
list_store_midi (AyyiListWin* list_win, GList* parts)
{
	GtkListStore* list_store = list_win->list_store;

	GtkTreeIter   iter;
	static char   time[64];
	static gchar* type = "MIDI";
	static char   val1[64] = "";
	static char   velocity[64] = "";

	list_win->mode = LIST_MODE_MIDI;

	// it is possible that the data has not changed. But we dont check that
	dbg (2, "clearing list... %p", list_store);
	gtk_list_store_clear(list_store);

	ASSERT_SONG_SHM;

	int p = -1;
	for(;parts;parts=parts->next){
		AMPart* part = parts->data;
		g_return_if_fail(part);
		GPos note_start;
		int safety = 0;
		MidiNote* note = NULL;
		while((note = am_midi_part__get_next_event(part, note))){
			p++;
			samples2pos(note->start, &note_start);
			pos2bbst(&note_start, time);

			char buff[128];
			ayyi_samples2bbst(note->length, buff);
			g_strlcpy(val1, buff, 64);

			snprintf(velocity, 63, "%i", note->velocity);

			char colour[8] = "#bbbbbb";
			char fg_colour[8] = "#000000";

			gtk_list_store_append (list_store, &iter);
			gtk_list_store_set (list_store, &iter,
			                    COLUMN_TIME, time,
			                    COLUMN_TYPE_STR,type,
			                    COLUMN_VAL1,   val1,
			                    COLUMN_VAL2,   note_format(note->note),
			                    COLUMN_LABEL,  velocity,
			                    COLUMN_NUM,    p,   //the ayyi container note index.
			                    COLUMN_OBJIDX, (uint64_t)part->ayyi->id,
			                    COLUMN_COLOUR, colour,
			                    COLUMN_COLOUR_SET, FALSE,
			                    COLUMN_FG_COLOUR, fg_colour,
			                    COLUMN_FG_COLOUR_SET, FALSE,
			                    -1);

			// is the note selected?
			GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(list_store), &iter);
			g_return_if_fail(path);
			if(list_note_is_selected(list_win, part, note)) list_select_row(list_win, path);
			gtk_tree_path_free(path);

			if(safety++ > 512){ perr("bailing out!"); break; }
		}
		list_win->src = g_list_append(list_win->src, part);
	}
	gtk_tree_view_column_set_title(list_win->column_val2, "note");
	gtk_tree_view_column_set_title(list_win->column_label, "velocity");
}



static void
list_on_change (AyyiListWin* panel)
{
	// Update unconditionally irrespective of link status.
	// -if neccesary, call link status before calling.

	bool list_window_update(AyyiListWin* list_win)
	{
		// updates the list window to reflect the model data.
		// FIXME this causes the window to scroll down to the bottom - we need to manually restore scrollpos? No, probably better to not clear the list..

		app->disable_help_focus = true;
		list_win_update((AyyiListWin*)list_win);
		app->disable_help_focus = false;

		list_win->update_id = 0;    //show update queue has been cleared.
		return G_SOURCE_REMOVE;
	}

	if(panel->update_id) return;

	panel->update_id = g_idle_add((GSourceFunc)list_window_update, panel);
}


static GtkTreeViewColumn*
list_add_column (AyyiListWin* panel, gint colnum, const gchar* title, Editor* editor)
{
	// add a column to the given TreeView.
	GtkTreeView* view = panel->treeview;

	GtkTreeViewColumn* column = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(column, title);
	gtk_tree_view_column_set_min_width(column, 50);
	//gtk_tree_view_column_set_max_width(column, 100); //doesnt work for last column.

	GtkCellRenderer* renderer = gtk_cell_renderer_text_new();

	//set visual properties:
	g_object_set(renderer, "ypad", 0, NULL);

	if(editor) g_object_set(renderer, "editable", TRUE, NULL);

	gtk_tree_view_column_pack_start(column, renderer, EXPAND_FALSE);
	gtk_tree_view_column_add_attribute(column, renderer, "text", colnum);

	gtk_tree_view_column_set_attributes(column, renderer,
		"text", colnum,
		"cell-background-set", COLUMN_COLOUR_SET,
		"cell-background", COLUMN_COLOUR,
		"foreground-set", COLUMN_FG_COLOUR_SET,
		"foreground", COLUMN_FG_COLOUR,
		NULL);

	// note: we can store our own data using eg this:
	//g_object_set_data(G_OBJECT(renderer), "my_column_num", GUINT_TO_POINTER(COLUMN_NAME));

	if(editor) g_signal_connect(renderer, "edited", (GCallback)list_on_val_edited, editor);

#ifdef HAVE_GTK_2_6
	g_signal_connect(renderer, "editing-started", (GCallback)list_accels_disconnect, view);
	g_signal_connect(renderer, "editing-canceled", (GCallback)list_editing_cancelled, view);
#endif

	gtk_tree_view_append_column(GTK_TREE_VIEW(view), column);

	// right align all colums except the label:
	if(colnum == COLUMN_LABEL) g_object_set(G_OBJECT(renderer), "xalign", 0.0, NULL);
	else g_object_set(G_OBJECT(renderer), "xalign", 1.0, NULL);

	if(colnum == COLUMN_TIME) gtk_tree_view_column_set_cell_data_func(column, renderer, (gpointer)list_cell_bg_lighter, NULL, NULL);

	return column;
}


static void
list_on_name_edited (AyyiListWin* panel, int rownum, gchar* new_text, uint64_t id)
{
	// called when the user has finished editing an entry in the 'label' column
	// (or 'velocity' in midi part mode).

	AMPart* part;

	switch(panel->mode){
		case LIST_MODE_ARRANGE:
			dbg (0, "setting part name: partid=%Lu \"%s\".", id, new_text);
			am_part_rename(am_partmanager__get_by_id(id), new_text, NULL, NULL);
			break;
		case LIST_MODE_MIDI:
			if((part = am_collection_find_by_idx((AMCollection*)song->parts, id))){
				MidiNote* note = am_midi_part__get_note_by_idx(part, rownum);
				am_part__set_note_selection(part, g_list_append(NULL, note));

				int velocity = atoi(new_text);
				if(velocity > 0 && velocity < 128){
					GList* note_list = NULL;
					note_selection_list__new(&note_list, part);
					NoteSelectionListItem* item = note_list->data;
					if(item->notes){
						TransitionalNote* n = item->notes->data;
						n->transient.velocity = velocity;
						note_selection_list__print(&note_list);
						am_midi_part__set_notes(part, item->notes, NULL, NULL);
					}
				}
			}
			break;
	}
}


static void
list_on_time_edited (AyyiListWin* panel, int rownum, gchar* new_text, uint64_t id)
{
	AyyiSongPos pos;
	if(bbss2pos(new_text, &pos)){
		switch(panel->mode){
			case LIST_MODE_ARRANGE:
				;AMPart* part = am_partmanager__get_by_id(id);
				if(part) am_part_move(part, &pos, NULL, NULL, NULL);
				break;
			case LIST_MODE_MIDI:
				if((part = am_collection_find_by_idx((AMCollection*)song->parts, id))){
					int note_idx = rownum;
					MidiNote* note = am_midi_part__get_note_by_idx(part, note_idx);
					am_part__set_note_selection(part, g_list_append(NULL, note));

					GList* note_list = NULL;
					note_selection_list__new(&note_list, part);
					NoteSelectionListItem* item = note_list->data;
					if(item->notes){
						TransitionalNote* n = item->notes->data;
						n->transient.start = ayyi_pos2samples(&pos);
						note_selection_list__print(&note_list);
						am_midi_part__set_notes(part, item->notes, NULL, NULL);
					}
				}
				break;
		}
	}
}


static void
list_on_val1_edited (AyyiListWin* panel, int rownum, gchar* new_text, uint64_t id)
{
	// val1 is usually Length.

	PF;
	GPos pos;
	if(bbst2pos(new_text, &pos)){

		switch(panel->mode){
			case LIST_MODE_ARRANGE:
				{
					AMPart* part = am_partmanager__get_by_id(id);
					if(part){
						songpos_gui2ayyi(&part->length, &pos);
						am_part_set_length(part);
					}
				}
				break;
			case LIST_MODE_MIDI:
				{
					AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, id);
					if(part){
						int note_idx = rownum;
						MidiNote* note = am_midi_part__get_note_by_idx(part, note_idx);
						am_part__set_note_selection(part, g_list_append(NULL, note));

						GList* note_list = NULL;
						note_selection_list__new(&note_list, part);
						NoteSelectionListItem* item = note_list->data;
						if(item->notes){
							TransitionalNote* n = item->notes->data;
							n->transient.length = pos2samples(&pos);
							note_selection_list__print(&note_list);
							am_midi_part__set_notes(part, item->notes, NULL, NULL);
						}
					}
				}
				break;
		}
	}
}


void
list_on_val_edited (GtkCellRendererText* cell, gchar* path_string, gchar* new_text, Editor* editor)
{
	PF;

	AyyiListWin* list = editor->panel;
	g_return_if_fail(editor->column < N_COLUMNS);

	int rownum;
	GtkTreeIter iter;
	gchar* old_text;
	uint64_t id;

	GtkTreePath* path = gtk_tree_path_new_from_string(path_string);
	GtkTreeModel* model = editor->panel->sort_model;
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, COLUMN_NUM, &rownum, editor->column, &old_text, COLUMN_OBJIDX, &id, -1);

	dbg (1, "id=%Lu old=%s new=%s", id, old_text, new_text);

	if(strcmp(new_text, old_text)){
		//value has changed.
		editor->callback(editor->panel, rownum, new_text, id);
	}

	list_accels_connect(GTK_CELL_RENDERER(cell), NULL, NULL, list->treeview);
	gtk_tree_path_free(path);
}


bool
list_on_button_pressed (GtkTreeView* treeview, GdkEventButton* event, gpointer user_data)
{
	PF;
	if (event->type == GDK_BUTTON_PRESS && event->button == 3){ //single click with the right mouse button
		// if no row is selected, select one.
		GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

		if (gtk_tree_selection_count_selected_rows(selection) <= 1){

			GtkTreePath* path;
			if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview), (gint) event->x, (gint) event->y, &path, NULL, NULL, NULL)){
				gtk_tree_selection_unselect_all(selection);
				gtk_tree_selection_select_path(selection, path);
				gtk_tree_path_free(path);
			}
		}

		list_show_popupmenu(GTK_WIDGET(treeview), event, user_data);

		return HANDLED;
	}

	return NOT_HANDLED;
}


static void
list_selection_foreach (AyyiListWin* list, void (*fn)(AyyiListWin*, GtkTreeIter, gpointer), gpointer user_data)
{
	// call a function for each selected row.

	// TODO ensure there is a selection

	GtkTreeSelection* selection = gtk_tree_view_get_selection(list->treeview);

	GList* l = gtk_tree_selection_get_selected_rows(selection, NULL);
	for(;l;l=l->next){
		gint* indices = gtk_tree_path_get_indices((GtkTreePath*)(l->data));
		gint row = indices[0];

		GtkTreeIter sort_iter;
		if(gtk_tree_model_iter_nth_child(list->sort_model, &sort_iter, NULL, row)){
			fn(list, sort_iter, user_data);
		}
	}
}


static bool
list_on_scroll (GtkTreeView* treeview, GdkEventScroll* event, gpointer user_data)
{
	// enables scroll wheel to be used to edit values.
	// currently only changes part length.

	AyyiListWin* list = (AyyiListWin*)user_data;

	static guint idle = 0;

	if(list->mode != LIST_MODE_ARRANGE) return NOT_HANDLED;

	GtkTreeViewColumn* column;
	if(gtk_tree_view_get_path_at_pos(list->treeview, event->x, event->y, NULL, &column, NULL, NULL)){
		if(column != list->column_val1) return NOT_HANDLED;
	} else return NOT_HANDLED;

	void delayed_update(AMPart* part, const char* length)
	{
		static struct _data {
			AMPart* part;
			char    length[64];
		} data = {NULL, ""};

		bool update(gpointer _data)
		{
			struct _data* data = (struct _data*)_data;
			idle = 0;

			GPos pos;
			if(bbst2pos(data->length, &pos)){
				songpos_gui2ayyi(&data->part->length, &pos);
				am_part_set_length(data->part);
			}
			return G_SOURCE_REMOVE;
		}

		if(data.part && data.part != part) update(&data);
	
		data.part = part;
		strcpy(data.length, (char*)length);

		if(idle) return;
		idle = g_timeout_add(500, (GSourceFunc)update, &data);
	}

	void foreach(AyyiListWin* list, GtkTreeIter sort_iter, gpointer user_data)
	{
		GdkEventScroll* event = (GdkEventScroll*)user_data;

		gchar* text;
		uint64_t id;
		gtk_tree_model_get(GTK_TREE_MODEL(list->sort_model), &sort_iter, COLUMN_VAL1, &text, COLUMN_OBJIDX, &id, -1);
		g_return_if_fail(id);
		AMPart* part = am_partmanager__get_by_id(id);
		if(part){
			char modified[64];
			strcpy(modified, text);
			bool changed = (event->direction == GDK_SCROLL_UP) ? bbst_increment(modified) : bbst_decrement(modified);
			if(changed){
				GtkTreeIter iter;
				gtk_tree_model_sort_convert_iter_to_child_iter (GTK_TREE_MODEL_SORT(list->sort_model), &iter, &sort_iter);
				gtk_list_store_set (list->list_store, &iter, COLUMN_VAL1, modified, -1);

				delayed_update(part, modified);
			}
		}
	}

	switch(event->direction){
		case GDK_SCROLL_UP:
		case GDK_SCROLL_DOWN:
			list_selection_foreach(list, foreach, event);
			break;
		default:
			break;
	}
	return NOT_HANDLED;
}


static bool
list_selection_changed (AyyiListWin* list, GList* new_selection)
{
	// Compare the given list with the previous list window selection list and returns TRUE if they are the different.

	// @new_selection If nothing is selected, this will be NULL.

	GList* selection = list_get_selection(list);

	if(selection && !new_selection) return TRUE;
	if(!selection && new_selection) return TRUE;

	if(g_list_length(new_selection) != g_list_length(selection)) return TRUE;

	GList* l1 = selection;
	GList* l2 = new_selection;
	for(;l1;l1=l1->next){
		if(l1->data != l2->data) return TRUE;
		l2 = l2->next;
	}

	dbg (2, "unchanged.");
	return FALSE;
}


static void
list_on_cursor_changed (GtkTreeView* treeview, gpointer user_data)
{
	// One or more rows has been selected by the user in the list window.

	PF;

	// unfortunate to have to do this explicitly but the active panel needs to be set imediately (default happens too late)
	gtk_widget_grab_focus(GTK_WIDGET(treeview));

	AyyiListWin* list_win = (AyyiListWin*)windows__get_panel_from_widget(GTK_WIDGET(treeview));
	if((gpointer)list_win != user_data) pwarn("panel difference %p %p", user_data, list_win);

	if(list_win->debug_freeze_selection){ dbg(0, "frozen"); return; }

	//TODO use list_selection_foreach() instead?

	void list_on_cursor_changed__midi(AyyiListWin* list_win, GtkTreeView* treeview)
	{
		GList* selection = gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(treeview), NULL);

		GtkTreeModel* model = gtk_tree_view_get_model(treeview);
		GList* new_selection = NULL;

		AMPart* part = NULL;
		GList* l = selection;
		for(;l;l=l->next){
			gint* indices = gtk_tree_path_get_indices((GtkTreePath*)(l->data));
			gint row = indices[0];

			// get the Part info from the model for this row
			GtkTreeIter iter;
			if(gtk_tree_model_iter_nth_child(model, &iter, NULL, row)){
				uint64_t id;
				int e;
				gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COLUMN_OBJIDX, &id, COLUMN_NUM, &e, -1);
				part = part ? part : am_partmanager__get_by_id(id);
				if(part){
					MidiNote* note = (MidiNote*)ayyi_song_container_get_item(&((AyyiMidiRegion*)part->ayyi)->events, e);
					new_selection = g_list_append(new_selection, note);
				}
			}
			else perr ("failed to get iter for row %i.", row);
		}

		if(list_selection_changed(list_win, new_selection)){
			dbg (0, "selection changed.");
			list_clear_selection(list_win); //XXX careful we are not inadvertently clearing the song selection!!

			if(((AyyiPanel*)list_win)->link){
				/*
				PartSelection* part_selection = g_new0(PartSelection, 1);
				part_selection->part = part;
				part_selection->notes = new_selection;
				if(((AyyiPanel*)list_win)->link) am_song__part_selection2_replace(g_list_append(NULL, part_selection), (AyyiPanel*)list_win);
				*/
				am_midi_part__set_note_selection((MidiPart*)part, new_selection, list_win);

				//note: the list selection is now NULL, as the song selection is the selection that should be used. 

			}else{
				list_win->selection = new_selection; //TODO this is only a list of notes, we ideally need to know the part also, though we can get it from the tree.
				list_win->selection_valid = true;
			}
		}
		else g_list_free(new_selection);

		g_list_foreach (selection, (GFunc)gtk_tree_path_free, NULL);
		g_list_free (selection);
	}

		void list_on_cursor_changed__arrange(AyyiListWin* list_win, GtkTreeView* treeview)
		{
			GtkTreeModel* model = gtk_tree_view_get_model(treeview);
			GList* new_selection = NULL;

			//check: get the selection again as it seems to help prevent VALID_ITER errors:
			GList* selection = gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(treeview), NULL);

			GList* l = selection;
			for(;l;l=l->next){
				gint* indices = gtk_tree_path_get_indices((GtkTreePath*)(l->data));
				gint row = indices[0];

				//get the Part info from the model for this row:
				GtkTreeIter iter;
				if(gtk_tree_model_iter_nth_child(model, &iter, NULL, row)){

					uint64_t id;
					gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, COLUMN_OBJIDX, &id, -1);
					if(!id){ perr ("failed to get id from tree iter. row=%i", row); continue; }
					AMPart* gpart = am_partmanager__get_by_id(id);
					if(gpart){
						new_selection = g_list_append(new_selection, gpart);
					}
				}
				else perr ("failed to get iter for row %i.", row);
			}

			if(list_selection_changed(list_win, new_selection)){
				dbg (1, "selection changed.");
				g_list_clear(list_win->selection);

				if(((AyyiPanel*)list_win)->link){
					am_collection_selection_replace ((AMCollection*)song->parts, new_selection, list_win);
					if(list_win->selection) pwarn("list selection should be empty");
				}else{
					list_win->selection = new_selection;
					list_win->selection_valid = true;
				}
			}
			else g_list_free(new_selection);

			g_list_foreach (selection, (GFunc)gtk_tree_path_free, NULL);
			g_list_free (selection);
		}

	switch(list_win->mode){
		case LIST_MODE_MIDI:
			list_on_cursor_changed__midi(list_win, treeview);
			break;
		case LIST_MODE_ARRANGE:
			list_on_cursor_changed__arrange(list_win, treeview);
			break;
	}
}


static void
list_receive_selection_notify (AyyiPanel* panel)
{
	//we need to update the window in response to selection changes elsewhere.

	windows__verify_pointer(panel, AYYI_TYPE_LISTWIN);

	if(!panel->link){ dbg (0, "not linked."); return; }

	list_on_change((AyyiListWin*)panel);
}


static void
list_select_row (AyyiListWin* list_win, GtkTreePath* path)
{
	// used to set the selection following model changes.
	// -not directly connected to user input, so focus changes etc are not done here.

	// gtk_tree_view_set_cursor() cant do more than row, so is perhaps useless. See list_select_parts() instead.

	// FIXME disconnecting the signal handle prevents gtk from drawing the selection?

	g_return_if_fail(path);
	GtkTreeView* treeview = list_win->treeview;

	// disconnect signal to prevent feedback
	gulong* handler = &list_win->cursor_handler_id;
	if(*handler) g_signal_handler_disconnect((gpointer)treeview, *handler);
	list_win->debug_freeze_selection = TRUE;

#ifdef DEBUG
	if(_debug_ > 1){
		gint* indices = gtk_tree_path_get_indices(path);
		gint row = indices[0];
		dbg (3, "row=%i", row);
		list_print_row(list_win, path);
	}
#endif

	gtk_tree_view_set_cursor(treeview, path, NULL, 0);

	// reconnect signal
	*handler = g_signal_connect(G_OBJECT(treeview), "cursor-changed", G_CALLBACK(list_on_cursor_changed), list_win);
	list_win->debug_freeze_selection = FALSE;
}


static void
list_select_parts (AyyiListWin* list_win, GList* parts)
{
	dbg(2, "invalidating tree selection...");
	GtkTreeSelection* selection = gtk_tree_view_get_selection(list_win->treeview);
	gtk_tree_selection_unselect_all(selection);

	GList* l = parts;
	for(;l;l=l->next){
		AMPart* part = l->data;
		if(!part->pool_item) continue; //temp
		GtkTreePath* path = list_part_get_path(list_win, part);
		if(!path){ perr ("cannot get treepath."); perr ("part=%p '%s'.", part, part->name); return; }

		gtk_tree_selection_select_path(selection, path);
		gtk_tree_path_free(path);
	}
}


static void
list_delete_selected (AyyiListWin* panel)
{
	g_return_if_fail(AYYI_IS_LISTWIN(panel));

	GList* parts = list_get_tree_selected((AyyiPanel*)panel);

	switch(((AyyiListWin*)panel)->mode){
		case LIST_MODE_ARRANGE:;
			GList* l = parts;
			for(;l;l=l->next){
				AMPart* part = l->data;
				if(PART_IS_AUDIO(part))
					ayyi_song__delete_audio_region((AyyiAudioRegion*)part->ayyi);
				if(PART_IS_MIDI(part))
					ayyi_song__delete_midi_region(part->ayyi);
			}
			break;
		case LIST_MODE_MIDI:;
			l = parts;
			for(;l;l=l->next){
				PartNote* pn = l->data;
				dbg(0, "note=%i", pn->note->note);
				am_midi_part__remove_notes ((MidiPart*)pn->part, g_list_prepend(NULL, pn->note), NULL, NULL);
				g_free(pn);
			}
			break;
	}
	g_list_free(parts);
}


static GList*
list_get_selection (AyyiListWin* list)
{
	AyyiPanel* panel = (AyyiPanel*)list;

	switch(list->mode){
		case LIST_MODE_MIDI:
  			if(panel->link){
				// currently we only return the selected notes for the first selected part.
				GList* parts = am_parts_selection;
				if(parts){
					GList* l = parts;
					for(;l;l=l->next){
						if(PART_IS_MIDI((AMPart*)l->data)) return ((MidiPart*)parts->data)->note_selection;
					}
				}
			}
			else return list->selection;
			break;
		case LIST_MODE_ARRANGE:
			return panel->link ? am_parts_selection : list->selection;
			break;
	}
	return NULL;
}


static void
list_clear_selection(AyyiListWin* list)
{
	if(list->selection){
		g_list_free(list->selection);
		list->selection = NULL;
	}
}


GtkTreePath*
list_part_get_path(AyyiListWin* list_win, AMPart* gpart)
{
	// find which row the given part is in.

	g_return_val_if_fail(list_win, NULL);
	g_return_val_if_fail(gpart, NULL);

	AyyiRegionBase* ayyi_part = am_part_get_shared(gpart);
	uint64_t id = ayyi_part->id;
	uint64_t iter_idx;
	GtkTreeModel* list_store = list_win->sort_model; //note we use the sort_model here.
	GtkTreePath*  path       = NULL;

	// iterate over all the rows in the list:
	GtkTreeIter iter;
	if(!gtk_tree_model_get_iter_first(list_store, &iter)){ perr ("cannot get iter."); return NULL; }
	int row=0;
	do {
		gtk_tree_model_get(list_store, &iter, COLUMN_OBJIDX, &iter_idx, -1);//get the objidx for this iter.
		dbg (3, "id=%Lu test=%Lu", id, iter_idx);
		if(iter_idx == id){
			if(!(path = gtk_tree_model_get_path(list_store, &iter))) perr ("failed to get tree path!");
			//path = gtk_tree_path_new_from_indices(indices[0], -1);
			break; 
		}

		row++;
	} while (gtk_tree_model_iter_next(list_store, &iter));


	dbg (2, "done. part->id=%Lu row_num=%i", id, row);
	return path;
}


/*
 *  The list and the treepaths must be freed by caller after use.
 */
static GList*
list_get_paths_for_selection (AyyiListWin* list_win)
{
	GList* paths = NULL;

	uint64_t row_part_id;
	GtkTreeModel* model = list_win->sort_model; // Note the sort_model is used here.
	GtkTreePath* path = NULL;
	int selection_size = g_list_length(am_parts_selection);

	// Iterate over the whole model, checking if the row-part is in the selection list
	GtkTreeIter iter;
	if(!gtk_tree_model_get_iter_first(model, &iter)) return NULL; // No items in the list.
	int row = 0;
	int found = 0;
	do {
		// Get the part id for this row
		gtk_tree_model_get(model, &iter, COLUMN_OBJIDX, &row_part_id, -1);

		GList* l = am_parts_selection;
		for(;l;l=l->next){
			AMPart* part = l->data;
			AyyiRegionBase* ayyi_part = am_part_get_shared(part);
			dbg (3, "id=%Lu test=%Lu", ayyi_part->id, row_part_id);
			if(row_part_id == ayyi_part->id){
				if(!(path = gtk_tree_model_get_path(model, &iter))){ perr ("failed to get tree path!"); break; }
				paths = g_list_append(paths, path);
				found++;
				break; 
			}
		}

		if(found == selection_size) break;
		row++;
	} while (gtk_tree_model_iter_next(model, &iter));

	if (g_list_length(paths) != selection_size) pwarn("selected.size=%i", g_list_length(paths));
	return paths;
}


/*
 *  Return a list of selected parts.
 *  The list must be freed after use.
 */
static GList*
list_get_tree_selected (AyyiPanel* panel)
{
	//g_return_val_if_fail(((AyyiListWin*)panel)->mode == LIST_MODE_ARRANGE, NULL);

	AyyiListWin* list_win       = (AyyiListWin*)panel;
	GtkTreeModel* model         = GTK_TREE_MODEL(list_win->sort_model);
	GtkTreeSelection* selection = gtk_tree_view_get_selection(list_win->treeview);
	GList* rows                 = gtk_tree_selection_get_selected_rows(selection, NULL);
	GList* items                = NULL;

	GList* l = rows;
	for(;l;l=l->next){
		gint* indices = gtk_tree_path_get_indices((GtkTreePath*)(l->data)); //free?
		gint row = indices[0];

		// Get the Part info from the model for this row:
		GtkTreeIter iter;
		if(gtk_tree_model_iter_nth_child(model, &iter, NULL, row)){

			switch(((AyyiListWin*)panel)->mode){
				case LIST_MODE_ARRANGE:;
					uint64_t id;
					gtk_tree_model_get(model, &iter, COLUMN_OBJIDX, &id, -1);

					items = g_list_prepend(items, am_partmanager__get_by_id(id));
					break;
				case LIST_MODE_MIDI:;
					int idx;
					gtk_tree_model_get(model, &iter, COLUMN_NUM, &idx, COLUMN_OBJIDX, &id, -1);
					AMPart* part = am_partmanager__get_by_id(id);
					g_assert(PART_IS_MIDI(part));
					MidiNote* note = am_midi_part__get_note_by_idx(part, idx);
					items = g_list_prepend(items, AYYI_NEW(PartNote, .part=part, .note=note));
					break;
			}
		}
	}

	g_list_foreach (rows, (GFunc)gtk_tree_path_free, NULL);
	g_list_free (rows);

	return items;
}


#ifdef NEVER
static gboolean
list_win_part_is_selected(AyyiListWin* list_win, AMPart* gpart)
{
	g_return_val_if_fail(list_win, false);
	g_return_val_if_fail(gpart, false);

	GList* found = NULL;

	if(((AyyiPanel*)list_win)->link){
		found = g_list_find(am_parts_selection, gpart);
	}else{
		found = g_list_find(am_parts_selection, gpart);
		pwarn ("FIXME list windows should have their own selection when not linked.");
	}

	if(found) dbg(0, "selected!: %s", gpart->name);
	if(found) return TRUE; else return FALSE;
}
#endif


static bool
list_note_is_selected (AyyiListWin* list_win, AMPart* part, MidiNote* note)
{
	g_return_val_if_fail(list_win, false);
	g_return_val_if_fail(note, false);

	GList* found = NULL;

	if(((AyyiPanel*)list_win)->link){
		found = g_list_find(((MidiPart*)part)->note_selection, note);
	}else{
		found = g_list_find(((MidiPart*)part)->note_selection, note);
		pwarn ("FIXME list windows should have their own selection when not linked.");
	}

	return (boolp)found;
}


static void
list_cell_bg_lighter (GtkTreeViewColumn* tree_column, GtkCellRenderer* cell, GtkTreeModel* tree_model, GtkTreeIter* iter)
{
	char* scolour;
	gtk_tree_model_get(tree_model, iter, COLUMN_COLOUR, &scolour, -1);

	GdkColor colour;
	if(!gdk_color_parse(scolour, &colour)) pwarn("parsing of colour string failed. %s", scolour);
	colour_lighter_gdk(&colour, 2);

	g_object_set(cell, "cell-background-set", TRUE, "cell-background-gdk", &colour, NULL);

	g_free(scolour);
}


static void
list_editing_cancelled(GtkCellRenderer* renderer, gpointer treeview)
{
	list_accels_connect(renderer, NULL, NULL, treeview);
}


static void
list_accels_connect(GtkCellRenderer* renderer, GtkCellEditable* editable, gchar* path, gpointer treeview)
{
	// Restore keyboard shortcuts after finished editing.

	accels_connect(treeview);

	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(treeview))), ((AyyiPanelClass*)g_type_class_peek(AYYI_TYPE_LISTWIN))->mod_accels->data);
}


static void
list_accels_disconnect(GtkCellRenderer* renderer, GtkCellEditable* editable, gchar* path, gpointer treeview)
{
	// normal keyboard shortcuts need to be disabled when we are "editing".

	accels_disconnect(treeview);

	GSList* connected_accels = gtk_accel_groups_from_object(G_OBJECT(gtk_widget_get_toplevel(treeview)));
	if(connected_accels){
		// TODO mod_accels are supposed to be not removed, so why are we removing them?
    	gtk_window_remove_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(treeview))), ((AyyiPanelClass*)g_type_class_peek(AYYI_TYPE_LISTWIN))->mod_accels->data);

		connected_accels = gtk_accel_groups_from_object(G_OBJECT(gtk_widget_get_toplevel(treeview)));
		if(connected_accels){
			dbg(1, "accel groups remaining: %u", g_slist_length(connected_accels));
		}
	}
}


#ifdef DEBUG
static void
list_print_row(AyyiListWin* list, GtkTreePath* path)
{
	int rownum;
	GtkTreeIter iter;
	gchar* text;
	uint64_t id;

	GtkTreeModel* model = list->sort_model;
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, COLUMN_NUM, &rownum, COLUMN_LABEL, &text, COLUMN_OBJIDX, &id, -1);
	dbg(0, "%i. '%s'", rownum, text);
}
#endif


static GtkWidget*
list_menu_init (AyyiListWin* window)
{
	// Create the context menu for pool window file entries.
	// -its a per window thing, so we have the convenience of passing the treeview to callbacks.

	GtkTreeView* treeview = window->treeview;
	g_return_val_if_fail(treeview, NULL);

	GtkWidget* menu = gtk_menu_new();

	GtkWidget* item = gtk_image_menu_item_new_from_stock (GTK_STOCK_DELETE, NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);

	bool _list_delete_selected (GtkWidget* widget, gpointer _window)
	{
		list_delete_selected(_window);
		return HANDLED;
	}
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(_list_delete_selected), window);

	gtk_widget_show_all(menu);

	return menu;
}


void
list_show_popupmenu(GtkWidget* treeview, GdkEventButton* event, gpointer userdata)
{
	PF;

	AyyiListWin* list = (AyyiListWin*)windows__get_panel_from_widget(treeview);
	g_return_if_fail(list);

	//note: -event can be NULL here. gdk_event_get_time() accepts a NULL argument.
	gtk_menu_popup(GTK_MENU(list->menu), NULL, NULL, NULL, NULL, (event != NULL) ? event->button : 0, gdk_event_get_time((GdkEvent*)event));
}


static gint
list_drag_received(GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	if(!data || data->length < 0){ pwarn ("no data!"); return -1; }

	dbg (0, "%s", data->data);

	AyyiPanel* panel = windows__get_panel_from_widget(widget);

	switch(info){
		case AYYI_TARGET_URI_LIST:
			dbg (3, "TARGET_URI_LIST len=%i", data->length);
#ifdef DEBUG
			int i = 0;
#endif
			GList* list = uri_list_to_glist((char*)data->data);
			GList* l = list;
			for(;l;l=l->next){
				char* u = l->data;
				dbg (2, "%i: %s", i, u);
				gchar* method_string;
				vfs_get_method_string(u, &method_string);
				dbg (0, "method=%s", method_string);

				if(!strcmp(method_string, "dock")){
					ayyi_panel_on_drop_dock_item(panel, u);
				}
			}
			uri_list_free(list);
			break;
		default:
			break;
	}
	return FALSE;
}


static void
list_on_song_load(GObject* object, gpointer user_data)
{
	list_foreach {
		list_on_change(list_win);
	} end_list_foreach
}


static void
list_on_parts_change(GObject* object, gpointer user_data)
{
	PF;
	list_foreach {
		list_on_change(list_win);
	} end_list_foreach
}


static void
list_on_part_change(GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	list_foreach {
		if((AyyiPanel*)list_win != sender_win){
			list_on_change(list_win);
		}
	} end_list_foreach
}


static void
list_on_part_selection_change(GObject* am_song, AyyiPanel* sender, gpointer user_data)
{
	list_foreach {
		if((AyyiPanel*)list_win != sender){
			list_receive_selection_notify((AyyiPanel*)list_win);
		}
	} end_list_foreach
}


static void
list_on_note_selection_change(GObject* am_song, AyyiPanel* sender, GList* items, gpointer user_data)
{
	// As this is a Selection observation, we only update if the panel is Linked.
	// To determine this, Sender must be supplied.

	// @param notes - list of NoteSelectionListItem*.

	g_return_if_fail(sender);
	if(!items){ pwarn("no parts."); return; }

	PF;
	list_foreach {
		list_receive_selection_notify((AyyiPanel*)list_win);
	} end_list_foreach
}


static void
list_on_link_change(AyyiPanel* panel)
{
	// If the panel becomes linked, private selection data is obsolete and needs to be cleared.

	PF;
	AyyiListWin* list = (AyyiListWin*)panel;

	// if not linked there is no private data to clear <----- check this!
#ifdef DEBUG
	if(!panel->link){
		// panel was linked before so should not have any private selection data
		g_assert(!list->selection);
		g_assert(!list->selection_valid);
	}
#endif
	if(!panel->link) return;

	// panel is now linked so any private selection data needs to be cleared
	if(list->selection_valid){
		g_list_clear(list->selection);
		list->selection_valid = false;
#ifdef DEBUG
	}else{
		if(list->selection) perr("selection should not be set!");
#endif
	}
}


static void
list_on_delete_key(GtkWidget* accel_group, gpointer data)
{
	PF;
	if(!GTK_IS_ACCEL_GROUP(accel_group)){ perr ("not accel_group!"); return; }
	AyyiPanel* panel = windows__get_active();
	if(AYYI_IS_LISTWIN(panel)){
		list_delete_selected((AyyiListWin*)panel);
	}
}


static void
_track_to_string (char* s, AMTrack* track)
{
	// we shouldnt really be exposing the internal track number here.
	// however track display numbers are specific to an Arrange window and of no relevance here.
	// -though we could use the last_arrange variable, and use the display numbers for that.

	// we do not adjust to 1-indexing as track 0 is always hidden.

	g_return_if_fail(track);

    snprintf(s, 63, "%i", am_track_list_position(song->tracks, track));
}


static AMTrack*
_track_from_string(const char* s)
{
	guint t = atoi(s);
	if(t<am_track_list_count(song->tracks)){
		return song->tracks->track[t];
	}else{
		pwarn("tracknum out of range: %i / %i", t, am_track_list_count(song->tracks));
	}
	return NULL;
}


