/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
	-each window has a statusbar containing 5 segments:
		0- icon
		1- event log
		2- selection
		3- rollover
		4- time
		5- progressbar
*/

#include "global.h"
#include "windows.h"
#include "src/window.h"
#include "statusbar.h"

#define STATUSBAR_PADDING 1
#undef STATUSBAR_DISABLE

static void statusbar_icon_on_click       (GtkWidget*, GdkEventButton*, gpointer);
static void statusbar_on_styles_available (GObject*, gpointer);

#undef log_print

static gpointer statusbar_parent_class = NULL;

static void  statusbar_finalize      (GObject*);
static GType statusbar_get_type_once (void);


Statusbar*
statusbar_construct (GType object_type)
{
	Statusbar* self = (Statusbar*) g_object_new (object_type, NULL);
	return self;
}


static GObject*
statusbar_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties)
{
	GObjectClass* parent_class = G_OBJECT_CLASS (statusbar_parent_class);
	GObject* obj = parent_class->constructor (type, n_construct_properties, construct_properties);
	return obj;
}


static void
statusbar_class_init (StatusbarClass * klass, gpointer klass_data)
{
	statusbar_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->constructor = statusbar_constructor;
	G_OBJECT_CLASS (klass)->finalize = statusbar_finalize;
}


static void
statusbar_instance_init (Statusbar * self, gpointer klass)
{
}


static void
statusbar_finalize (GObject * obj)
{
	G_OBJECT_CLASS (statusbar_parent_class)->finalize (obj);
}


GtkWidget*
statusbar__new (AyyiWindow* window)
{
	// Currently viewports are used to preserve relative sizing without affecting the size of the toplevel window.
	// This may no longer be neccesary with newer versions of gtk.

	// Either GtkViewport or GtkFixed can be used. GtkFixed looks better with some themes. GtkViewport doesnt seem to offer any advantages.
	#undef VIEWPORT_WRAPPER

	g_return_val_if_fail(window, NULL);

	char buff[128];

	Statusbar* statusbar = statusbar_construct (TYPE_STATUSBAR);
	GtkWidget* hbox = (GtkWidget*)statusbar;

	gtk_widget_set_size_request(hbox, -1, 18); //stop size modulation at small font sizes when icon is added/removed.
#ifdef STATUSBAR_DISABLE
	gtk_widget_set_no_show_all(hbox, TRUE);
#endif

#ifdef VIEWPORT_WRAPPER
	GtkWidget* sb_vp_new ()
	{
		// the adjustment values seem to be ignored (?)

		return gtk_viewport_new(
			gtk_adjustment_new(10.0/*init*/, 10.00/*min*/, 200.0/*max*/, 1.0, 10.0, 10.0),
			gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0)
		);
	}
#endif

	GtkWidget* wrap (GtkWidget* child)
	{
#ifdef VIEWPORT_WRAPPER
		GtkWidget* wrapper = sb_vp_new();
		gtk_container_add(GTK_CONTAINER(wrapper), child);
		return wrapper;
#else
		GtkWidget* fixed = gtk_fixed_new();
		gtk_fixed_put ((GtkFixed*)fixed, child, STATUSBAR_PADDING, STATUSBAR_PADDING);
		return fixed;
#endif
	}

	// icon
	GtkWidget* icon = statusbar->widget[STATUSBAR_ICON] = gtk_image_new_from_stock(GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU);
	GtkWidget* wrapped = wrap(icon);
	gtk_widget_set_size_request(wrapped, 16 + 2 * STATUSBAR_PADDING, 16 + 2 * STATUSBAR_PADDING);
	gtk_box_pack_start(GTK_BOX(hbox), wrapped, TRUE, TRUE, 0);
	g_signal_connect((gpointer)icon, "button-release-event", G_CALLBACK(statusbar_icon_on_click), window);

	GtkWidget*
	add_panel (Statusbar* _statusbar, int n, GtkWidget* container)
	{
		GtkWidget* statusbar = _statusbar->widget[n] = gtk_statusbar_new();
		gtk_statusbar_set_has_resize_grip(GTK_STATUSBAR(statusbar), FALSE);
		gint context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "ex");
		buff[0] = '\0';
		gtk_statusbar_push(GTK_STATUSBAR(statusbar), context_id, buff);
		gtk_box_pack_start(GTK_BOX(container), wrap(statusbar), TRUE, TRUE, 0);
		return statusbar;
	}
	add_panel(statusbar, STATUSBAR_MAIN, hbox);
	add_panel(statusbar, STATUSBAR_SELECTION, hbox);
	add_panel(statusbar, STATUSBAR_HOVER, hbox);

	statusbar->progress = gtk_progress_bar_new();
	gtk_widget_set_no_show_all(statusbar->progress, TRUE);
	gtk_box_pack_start(GTK_BOX(hbox), wrap(statusbar->progress), FALSE, FALSE, 0);

	#define ICO_WIDTH 24
	#define SB4_WIDTH 180
	#define SBP_WIDTH 100
	gtk_widget_set_size_request(statusbar->widget[STATUSBAR_ICON], ICO_WIDTH, -1);
	gtk_widget_set_size_request(gtk_widget_get_parent(statusbar->progress), SBP_WIDTH, -1);
	gtk_widget_set_size_request(statusbar->widget[STATUSBAR_HOVER], SB4_WIDTH, -1);

	if(!SONG_LOADED){
		sprintf(buff, "Welcome to "PACKAGE_NAME" %s. Song loading...", VERSION);
		gint context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar->widget[STATUSBAR_MAIN]), "ex");
		gtk_statusbar_push (GTK_STATUSBAR(statusbar->widget[STATUSBAR_MAIN]), context_id, buff);
	}

#ifndef STATUSBAR_DISABLE
	void statusbar_on_shell_resize (GtkWidget* widget, GtkAllocation* allocation, gpointer _window)
	{
		AyyiWindow* window = _window;

		gint window_width = 0;
		gtk_window_get_size(GTK_WINDOW(widget), &window_width, NULL);
		int available_width = MAX(4, window_width - ICO_WIDTH - SB4_WIDTH - SBP_WIDTH - 32);

		// warning: this can cause a feedback loop:
		// -make sure that the total width is not too big.
		// -currently the statusbars are inside viewports, but resizes are still propogated to the top-level window.
		//  we could try and remove them, but perhaps the shadow they provide is nice. Hmm maybe sb's already have one?
		if(window->statusbar->widget[0] && window_width != window->current_width){
			dbg(2, "changed: %i --> %i", window->current_width, window_width);
			gtk_widget_set_size_request(window->statusbar->widget[STATUSBAR_MAIN], 2 * available_width / 3, -1); // sb1 should be bigger than the others.
			gtk_widget_set_size_request(window->statusbar->widget[STATUSBAR_SELECTION], available_width / 3, -1);
			dbg(2, "total_width=%i/%i", ICO_WIDTH + 2 * available_width / 3 + available_width / 3 + SB4_WIDTH + SBP_WIDTH, window_width);

			window->current_width = window_width;
		}
	}
	g_signal_connect(G_OBJECT(window), "size-allocate", G_CALLBACK(statusbar_on_shell_resize), window);
#endif
	g_signal_connect(app, "styles-available", G_CALLBACK(statusbar_on_styles_available), window);

	return hbox;
}


void
statusbar__set_icon (Statusbar* statusbar, const gchar* stock_id)
{
	// Usually this would be called using GTK_STOCK_DIALOG_WARNING.

	if(statusbar->widget[STATUSBAR_ICON]){
		gtk_image_set_from_stock(GTK_IMAGE(statusbar->widget[STATUSBAR_ICON]), stock_id, GTK_ICON_SIZE_MENU);
	}
}


static void
statusbar_icon_on_click (GtkWidget* widget, GdkEventButton* event, gpointer _)
{
	// Clicking the snap button steps to the next of the available snap modes.

	PF;
	gtk_widget_hide(widget);
}


static void
statusbar_on_styles_available (GObject* ap, gpointer _window)
{
	AyyiWindow* window = _window;

	int default_size = 16 + 2 * STATUSBAR_PADDING;
	if(app->line_height < default_size){
#ifndef VIEWPORT_WRAPPER
		for(int i=0;i<4;i++){
			GtkFixed* fixed = GTK_FIXED(gtk_widget_get_parent(window->statusbar->widget[i]));
			gtk_fixed_move(fixed, window->statusbar->widget[i], 1, (default_size - app->line_height + 1) / 2);
		}
#endif
	}else{
		gtk_widget_set_size_request((GtkWidget*)window->statusbar, -1, app->line_height + 2 * STATUSBAR_PADDING);
	}
}


static GType
statusbar_get_type_once (void)
{
	static const GTypeInfo g_define_type_info = { sizeof (StatusbarClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) statusbar_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Statusbar), 0, (GInstanceInitFunc) statusbar_instance_init, NULL };
	GType statusbar_type_id;
	statusbar_type_id = g_type_register_static (GTK_TYPE_HBOX, "Statusbar", &g_define_type_info, 0);
	return statusbar_type_id;
}


GType
statusbar_get_type (void)
{
	static volatile gsize statusbar_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&statusbar_type_id__volatile)) {
		GType statusbar_type_id;
		statusbar_type_id = statusbar_get_type_once ();
		g_once_init_leave (&statusbar_type_id__volatile, statusbar_type_id);
	}
	return statusbar_type_id__volatile;
}

