/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __gl_mixer_h__
#define __gl_mixer_h__

#include <model/model_types.h>
#include <model/mixer.h>
#include <panels/panel.h>
#include <windows.h>
#include <gl_mixer/style.h>

G_BEGIN_DECLS

#define AYYI_TYPE_GL_MIXER_WIN            (ayyi_gl_mixer_win_get_type ())
#define AYYI_GL_MIXER_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_GL_MIXER_WIN, AyyiGlMixerWin))
#define AYYI_GL_MIXER_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_GL_MIXER_WIN, AyyiGlMixerWinClass))
#define AYYI_IS_GL_MIXER_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_GL_MIXER_WIN))
#define AYYI_IS_GL_MIXER_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_GL_MIXER_WIN))
#define AYYI_GL_MIXER_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_GL_MIXER_WIN, AyyiGlMixerWinClass))

typedef struct _AyyiGlMixerWinClass AyyiGlMixerWinClass;

#define IS_GL_MIXER (AYYI_IS_GL_MIXER_WIN(panel))
#define GL_MIXER_FIRST (MixerWin*)windows__get_first(AYYI_TYPE_GL_MIXER_WIN)

#define gl_mixer_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_GL_MIXER_WIN) continue; \
		AyyiGlMixerWin* mixer = (AyyiGlMixerWin*)panel;
#define end_gl_mixer_foreach end_panel_foreach

struct _AyyiGlMixerWinClass {
    AyyiPanelClass parent_class;
};

#ifdef __gl_mixer_c__                   // AyyiGlMixerWin is currently private so no need for additional AyyiGlMixerWinPriv
struct _AyyiGlMixerWin
{
    AyyiPanel       panel;
    GtkWidget*      area;
    AGlActor*       actor;              // root actor
    AGlActor*       strips;
    GList*          selectionlist;      // list of channel strip numbers.
    Style           style;
};
#endif


GType       ayyi_gl_mixer_win_get_type  ();

void        gl_mixer_init               ();

typedef void (*ChannelCallback) (AMChannel*, gpointer);

typedef struct {
	union {
		ChannelCallback callbacks[2];
		struct {
			ChannelCallback select;
			ChannelCallback other;
		};
	};
	gpointer user_data;
} ChannelActionList;

G_END_DECLS

#endif
