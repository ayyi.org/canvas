/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

extern CircleShader circle_shader;

static AGlActor*
aux_new (AMChannel* channel)
{
	bool aux__paint (AGlActor* actor)
	{
		CIRCLE_BG_COLOUR() = 0x000000ff;
		CIRCLE_COLOUR() = 0xff00007f;
		circle_shader.uniform.centre = (AGliPt){15, 15};
		circle_shader.uniform.radius = 14;
		agl_use_program((AGlShader*)&circle_shader);

		agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));

		return true;
	}

	void aux__set_size (AGlActor* actor)
	{
		actor->region.x2 = agl_actor__width(actor->parent);
	}

	bool aux__on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		return NOT_HANDLED;
	}

	void aux__init (AGlActor* actor)
	{
		if(agl->use_shaders){
			if(!circle_shader.shader.program)
				agl_create_program(&circle_shader.shader);
		}
	}

	AGlActor* actor = (AGlActor*)agl_actor__new(AGlActor,
		.name     = g_strdup("Aux"),
		.region   = (AGlfRegion){0, 10, 0, 30},
		.init     = aux__init,
		.paint    = aux__paint,
		.set_size = aux__set_size,
		.on_event = aux__on_event
	);
	return actor;
}


