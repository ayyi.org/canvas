/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define __track_list_c__
#include "global.h"
#include "model/track_list.h"
#include "arrange/track.h"
#include "arrange/track_list.h"

static void track_list__pointers_rebuild     (TrackList*);
static void track_list__rebuild              (TrackList*);
static void track_list__update_display_order (TrackList*);
#ifdef DEBUG
static void track_list__verify               (TrackList*);
static void track_list__print                (TrackList*);
static void track_list__print_order          (TrackList*);
#endif
#define arr_track_pool_get_item(T) (ArrTrk*)((char*)tl->pool + T * sizeof(ArrTrackU))


TrackList*
track_list__new (Arrange* arrange)
{
	TrackList* tl = AYYI_NEW(TrackList,
		.pool = g_malloc0(sizeof(ArrTrackU) * AM_MAX_TRKX),
		.order = {0,},
		.display = {0,}
	);

	for (int t=0;t<AM_MAX_TRK;t++) {
		ArrTrk* atr = tl->track[t] = arr_track_pool_get_item(t);
		atr->arrange = arrange;
		atr->track = song->tracks->track[t];
		atr->height = 32;
#ifdef DEBUG
		atr->shm_idx = atr->track ? atr->track->ident.idx : -1;
#endif
		if (atr->track && atr->track->type == TRK_TYPE_MIDI) {
			ArrTrackMidi* m = (ArrTrackMidi*)atr;
			m->centre_note = 60;
			m->note_height = 8;
			m->velocity_chart_height = 30;
		}

		if (!atr->track || !atr->track->type) tl->track[t] = NULL;
	}

	return tl;
}


void
track_list__free (TrackList* tl)
{
	g_free(tl->pool);
	g_free(tl);
}


static ArrTrk*
_get_unassigned_track_pool_item (TrackList* tl)
{
	for (int t=0;t<AM_MAX_TRK;t++) {
		ArrTrk* a = arr_track_pool_get_item(t);
		if (!a->track || !a->track->type) return a;

		//is this already used?
		bool used = false;
		for (int i=0;i<AM_MAX_TRK;i++) {
			ArrTrk* test = tl->track[i];
			if (test == a) { used = true; break; }
		}
		if (!used) return a;
	}
	perr("unused item not found!");

	return NULL;
}


ArrTrk*
track_list__add (TrackList* tl, AMTrack* tr)
{
	// note: for additions we do not call update_all_pointers, it is all done here.

	int t = 0;
	for (ArrTrk* atr;(atr=tl->track[t]);t++) {
		if (!atr->track) break;
	}
	dbg(1, "found empty slot: %i (tr=%s)", t, tr->name);
	ArrTrk* trk = tl->track[t] = _get_unassigned_track_pool_item(tl);
	trk->track = tr;
#ifdef DEBUG
	trk->shm_idx = tr->ident.idx;
#endif

	g_return_val_if_fail(!trk->auto_paths, NULL);
	trk->auto_paths = g_ptr_array_sized_new(AUTO_MAX);

	if (tr->type == TRK_TYPE_MIDI) {
		ArrTrackMidi* m = (ArrTrackMidi*)trk;
		m->velocity_chart_height = 30;
		m->centre_note = 60;
		m->note_height = 8;
	}

	// display[]: add to next available slot
	{
		bool found = false;
		TrackDispNum d = 0;
		for (;d<AM_MAX_TRK;d++){
			if (!track_list__track_by_display_index(tl, d)) {
				found = true;
				break;
			}
		}
		if (found) {
			tl->display[d] = trk;
			tl->display_max++;

			ArrTrk* last_track = tl->display[d - 1];
			trk->y = last_track->y + last_track->height;
		} else {
			pwarn("empty display slot not found");
		}
	}

	track_list__update_display_order(tl);

	return trk;
}


void
track_list__remove (TrackList* tl, ArrTrk* atr)
{
	g_clear_pointer(&atr->auto_paths, g_ptr_array_unref);

	track_list__update_all_pointers(tl);
	// The ArrTrk is now removed from the lists.
}


void
track_list__move_to_pos (TrackList* tl, TrackDispNum track_d_num, TrackDispNum new_pos)
{
	// Be careful, dont use arrays here that have not yet been updated, eg track_order.

	ArrTrk* track_to_move = track_list__track_by_display_index(tl, track_d_num);
	if(new_pos < track_d_num){
		// track has been moved up. move down all tracks between a and b
		for(TrackDispNum d=track_d_num;d>new_pos;d--){
			dbg(1, "  %i <-- %i", d, d - 1);
			ArrTrk* moving_down = tl->display[d - 1];
			ArrTrk* atr = tl->display[d];

			tl->display[d] = moving_down;

			moving_down->trk_ctl = atr->trk_ctl;
		}
	}else{
		// track has been dragged down. move up all the tracks inbetween.
		TrackDispNum d; for(d=track_d_num;d<new_pos;d++){
			dbg(1, "  %i <-- %i", d, d + 1);
			ArrTrk* atr = tl->display[d];
			ArrTrk* moving_up = tl->display[d + 1];

			tl->display[d] = tl->display[d + 1];

			moving_up->trk_ctl = atr->trk_ctl;
		}
	}
	tl->display[new_pos] = track_to_move;

	track_list__update_display_order(tl);
}


/*
 *   Take care that the TrackNum relates to the Arrange track list and not the Model track list.
 *   You may need to use am_track_list_position()
 *
 *   If you have AMTrackNum, instead use: track_list__lookup_track(song->tracks->track[t])
 */
ArrTrk*
track_list__track_by_index (TrackList* tl, ArrTrackNum t)
{
	g_return_val_if_fail((t >= 0) && (t <= AM_MAX_TRK), NULL);

	return tl->track[t];
}


ArrTrk*
track_list__track_by_display_index (TrackList* tl, TrackDispNum d)
{
	g_return_val_if_fail((d >= 0) && (d <= AM_MAX_TRK), NULL);
	ArrTrk* atr = tl->display[d];
	return atr;
}


AMTrack*
track_list__get_trk_from_display_index (TrackList* tl, TrackDispNum d)
{
	g_return_val_if_fail((d >= 0) && (d <= AM_MAX_TRK), NULL);
	AMTrack* trk = tl->display[d]->track;
	return trk;
}


TrackDispNum
track_list__get_display_num (TrackList* tl, const AMTrack* trk)
{
	// note: its actually better to use tl->track_order[]

	g_return_val_if_fail(trk, -1);

	TrackDispNum d;
	bool found = false;
	for(d=0;d<am_track_list_count(song->tracks);d++){
		if(tl->display[d]->track == trk){ found = true; break; }
	}
	if(!found){
		pwarn("not found! track=%s%s%s type=%i", bold, trk->name, ayyi_white, trk->type);
		return -1;
	}
#if 0
	else if(d != track_list__get_display_num(tl, trk)){
		pwarn("display idx error (t=%i ato=%i) trk=%s", d, track_list__get_display_num(tl, trk), trk->name);
#ifdef DEBUG
		track_list__print_order(tl);
#endif
	}
#endif
	return d;
}


TrackDispNum
arr_t_to_d (TrackList* tl, TrackNum t)
{
	g_return_val_if_fail((t >= 0) && (t <= AM_MAX_TRK), -1);
	TrackDispNum d = tl->order[t];
	return d;
}


TrackNum
track_list__track_num_from_display_num (TrackList* tl, TrackDispNum display_num)
{
	TrackNum t;
	for(t=0;t<AM_MAX_TRK;t++){
		if(song->tracks->track[t] == tl->display[display_num]->track) break;
	}
	return t;
}


ArrTrk*
track_list__lookup_track (TrackList* tl, AMTrack* tr)
{
	ArrTrk* atr;
	for (int t=0;(atr=track_list__track_by_index(tl, t));t++) {
		if (tr == atr->track) return atr;
	}
	return NULL;
}


int
track_list__count_disp_items (TrackList* tl)
{
	int n = 0;
	ArrTrk* trk;
	for (TrackDispNum d = 0; d < AM_MAX_TRK && (trk = tl->display[d]) != NULL; d++) {
		n++;
	}
	return n;
}


AMTrack*
track_list__prev_visible (TrackList* tl, AMTrack* tr)
{
	AMTrack* prev;
	for (TrackDispNum d=track_list__get_display_num(tl, tr)-1; d>=0 && (prev = tl->display[d]->track) && prev->type; d--){
		if(prev->visible) return prev;
	}
	return NULL;
}


/*
 *  The TrackDispNum for the returned track may not be an increment of 1 because of hidden tracks.
 *  So if you need the TrackDispNum while iterating it might be better to use the tl->display array directly.
 *
 *  Tracks are considered visible if they are not explicitly hidden. They may still be offscreen.
 */
ArrTrk*
track_list__next_visible (TrackList* tl, AMTrack* tr)
{
	if (!tr) {
		ArrTrk* atr = tl->display[0];
		if (!atr) return NULL;
		tr = atr->track;
		if (tr->visible) return atr;
	}

	TrackDispNum d = track_list__get_display_num(tl, tr) + 1;
	ArrTrk* atr;
	for (; d<AM_MAX_TRK && (atr = tl->display[d]) && tl->display[d]->track->type; d++) {
		AMTrack* track = atr->track;
#ifdef DEBUG
		AMTrackNum t = am_track_list_position(song->tracks, track);
		g_assert(track->ident.idx == atr->shm_idx);
		if (d != tl->order[t]) perr("%i != %i", d, tl->order[t]);
#endif
		if (track == tr) continue; // this seems to happen during deletions
		if (track && track->visible) return atr;
	}
	return NULL;
}


ArrTrk*
track_list__last_visible (TrackList* tl)
{
	ArrTrk* at = NULL;
	for (TrackDispNum d=0; d<AM_MAX_TRK && tl->display[d] && tl->display[d]->track->type; d++) {
		at = tl->display[d];
	}
	return at;
}


static void
track_list__rebuild (TrackList* tl)
{
	// Following a track deletion, remove any empty slots in the tl->track[] array.

	int i = 0;
	int t; for(t=0;t<AM_MAX_TRK;t++){

		ArrTrk* at = arr_track_pool_get_item(t);
		AMTrack* tr = at->track; // the address of the track has changed so this is worthless???? hopefully not... the track[] array is a pointer to the actual data!

		if(tr && tr->ident.idx > -1){
			if(!tr || tr->type == 1){
				tr = am_track_list_find_by_shm_idx(song->tracks, tr->ident.idx, TRK_TYPE_AUDIO); //TODO type
			}
		}

		if(tr && tr->type){
			{
#if 0
				if(tr->ident.idx != at->shm_idx) pwarn("tr/atr shm_num mismatch! %i != %i (%s) ...not sure this is really a problem...", tr->ident.idx, at->shm_idx, tr->name);
#endif
			}
			at->track = tr;
			tl->track[i] = at;

			i++;
		}
#ifdef DEBUG
		else if(tr){
			if(tr->ident.idx != -1 && tr->ident.idx != at->shm_idx) pwarn("!! at->track mismatch %i %i", tr->ident.idx, at->shm_idx);
		}
#endif
	}
	if(i < AM_MAX_TRK) tl->track[i] = NULL; // null terminate the array

	//track_list__print(tl);

#ifdef ONLY_FOLLOWING_DELETE
	int j = 0;
	for(t=0;t<AM_MAX_TRK;t++){
		ArrTrk* at = tl->display[t];
		if(tl->display[t] && at->track && at->track->type){
			//dbg(0, "t=%i j=%i %s", t, j, tl->display[t]->name->str);
			tl->display[j] = tl->display[t];
			j++;
		}
	}
	tl->display[j] = NULL;
#endif
}


static void
track_list__pointers_rebuild (TrackList* tl)
{
	{
		// update the display array
		// -as hidden tracks need to preserve their order, this is done even if they are hidden.
		// this handles additions but not deletions explictly (are they handled via type=0 ?)

		TrackDispNum
		lookup_displaynum (int t) //this does the same thing as the track_order array? but track_order is not yet valid.
		{
			for (int d=0;d<AM_MAX_TRK;d++) {
				if (!tl->display[d]) return -1;
				if (am_track_list_position(song->tracks, tl->display[d]->track) == t) return d;
			}
			return -1;
		}

		ArrTrk* trk = NULL;
		for(int t=0;t<AM_MAX_TRK && (trk = tl->track[t]) && trk->track && trk->track->type;t++){
			if(lookup_displaynum(t) > -1) continue; //already exists

			// this track is not in the display list. add it at the next empty slot.
			bool found = false;
			int d; for(d=0;d<AM_MAX_TRK;d++) if(!tl->display[d] || !tl->display[d]->track->type){ found = true; break; }
			if(!found){
				perr("empty tracks->display not found!");
#ifdef DEBUG
				track_list__print_order(tl);
#endif
				continue;
			}
			tl->display[d] = trk;
		}
#ifdef DEBUG
		int i = 0;
#endif
#if 0 // removed use of private method am_track_list_get_pool_item
		for(t=0;t<AM_MAX_TRK;t++){
			AMTrack* tt = am_track_list_get_pool_item(song->tracks, t);
			if(tt && tt->type){
				// now any references to this track are out of date?
				/*
				TrackDispNum d; for(d=0;d<AM_MAX_TRK;d++){
					if(tl->display[d] == tt){
					}
				}
				*/
				i++;
			}
		}
#endif

		// display[] is not sparse, so remove any gaps
		int j = 0;
		tl->display_max = 1;
		for(int t=0;t<AM_MAX_TRK;t++){
			ArrTrk* tr = tl->display[t];
			if(tr && tr->track && tr->track->type){
				dbg(2, "t=%i j=%i %s", t, j, tr->track->name);
				tl->display[j] = tr;
				j++;
				tl->display_max = j;
			}
		}
		if(!j) dbg(2, "no display tracks");

#ifdef DEBUG
		i = j;
#endif

		dbg(2, "i=%i nulling track[%i]... td=%p \"%s\"", i, j, tl->display[i], song->tracks->track[i] ? song->tracks->track[i]->name : NULL);
		tl->display[j] = NULL;
	}

	track_list__update_display_order(tl);
}


static void
track_list__update_display_order (TrackList* tl)
{
	// Update the display-order cache.
	// This cache is used for looking up the display number from the internal number.

	if (!tl->display[0]) dbg(2, "no tracks");
	if (track_list__count_disp_items(tl) != am_track_list_count(song->tracks)) pwarn("track_disp doesnt have enough tracks? %i %i", track_list__count_disp_items(tl), am_track_list_count(song->tracks));

	for (TrackNum t=0;t<AM_MAX_TRK;t++) tl->order[t] = -1;

	ArrTrk* atr;
	int done = 0;
	for (TrackDispNum d=0; d<AM_MAX_TRK && (atr = tl->display[d])!=NULL; d++) {
		AMTrack* trk = atr->track;
		AMTrackNum t = am_track_list_position(song->tracks, trk);
		tl->order[t] = trk->type ? d : -1;
		if (d < 8) dbg(2, "  %i: d=%i to=%i", t, d, tl->order[d]);
		done++;
	}
	dbg(2, "track order: n reindexed: %i", done);
}


void
track_list__update_all_pointers (TrackList* tl)
{
	// update these 3 arrays:
	//   ArrTrk*   track
	//   ArrTrk*   display
	//   char      track_order

#ifdef DEBUG
	if(_debug_ > 1) printf("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
#endif

	track_list__rebuild(tl);        // only really needed after loading or after a delete. Does track[] and display[] (maybe)
	track_list__pointers_rebuild(tl); // display[], and track_order[] <-- check this last

#ifdef DEBUG
	if(_debug_ > 1) track_list__verify(tl);
	if(_debug_ > 1) printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n");
#endif
}


#ifdef DEBUG
static void
track_list__verify (TrackList* tl)
{
	gboolean err = false;

	int n_tracks = am_track_list_count(song->tracks);
	int found = 0;
	ArrTrackNum t; for(t=0;t<AM_MAX_TRK;t++){
		ArrTrk* atr = track_list__track_by_index(tl, t);
		if(!atr || !atr->track || !atr->track->type){
			if (t < n_tracks){ pwarn("found unexpected blank track: %i atr=%p", t, atr); err = true; }
			continue;
		}
		found++;
	}
	if (found != n_tracks) {
		pwarn("n_tracks=%i found=%i", n_tracks, found);
		err = true;
		if (_debug_) track_list__print(tl);
	}

	// check the display array
	found = 0;
	GList* td = NULL;
	TrackDispNum d; for(d=0;d<AM_MAX_TRK;d++){
		ArrTrk* atr = track_list__track_by_display_index(tl, d);
		if(!atr) continue;
		if(g_list_find(td, atr)) perr("track order: duplicate! t=%i tr=%p", d, atr);
		td = g_list_append(td, atr);
		found++;
	}
	if(found != n_tracks){err = true; pwarn("display: n_tracks=%i found=%i", n_tracks, found); }
	g_list_free(td);

	// check the track_order array
	found = 0;
	GList* to = NULL;
	for(t=0;t<AM_MAX_TRK;t++){
		TrackDispNum d = arr_t_to_d(tl, t);
		if(d > -1){
			if(g_list_find(to, GINT_TO_POINTER(d))) perr("track order: duplicate! t=%i order=%i", t, d);
			to = g_list_append(to, GINT_TO_POINTER(d));
			found++;
		}
		else{
			if(t<n_tracks){ err = true; perr("track_order: unexpected empty entry. t=%i", t); }
		}
	}
	g_list_free(to);
	if(found != n_tracks){ err = true; perr("track_order: n_tracks=%i found=%i", n_tracks, found); }

	//print_track_order(tl);
	//track_list__print(tl);

	if(!err && _debug_ > 1) log_print(LOG_OK, "track_list__verify");
}


static void
track_list__print (TrackList* tl)
{
	UNDERLINE;
	PF;
	char idx[16];
	char visible[8];
	printf("   t at        tctl      typ ai\n");
	int empty = 0;
	TrackNum t; for(t=0;t<AM_MAX_TRK && t<12;t++){
		ArrTrk* at = track_list__track_by_index(tl, t);
		GtkWidget* ctl = at ? at->trk_ctl : NULL;
		AMTrack* tr = at ? at->track : NULL;
		snprintf(idx, 15, "%2i", tr ? tr->ident.idx : -1);
		snprintf(visible, 7, "%s", tr ? (tr->visible ? "v" : " ") : " ");
		printf("  %2i %9p %9p %i %s %3i %s %s %s\n", t, at, ctl, (tr ? tr->type : 0), idx, at ? at->y : -1, visible, ((size_t)at && at->track) ? at->track->name : "", (!tr || !tr->type) ? "--" : "");

		if(!at || !tr || !tr->type) empty++;
		if(empty > 1) break;
	}
	UNDERLINE;
}


static void
track_list__print_order (TrackList* tl)
{
	UNDERLINE;

	printf("TrackList: display:\n");
	printf("  d\n");
	TrackDispNum d; for(d=0;d<AM_MAX_TRK;d++){
		ArrTrk* atr = tl->display[d];
		if(!atr || !atr->track || !atr->track->type){
			printf("  %i: <end>\n", d);
			break;
		}
		printf("  %i: %s\n", d, atr->track->name);
	}

	printf("TrackList: track order:\n");
	printf("  t d\n");
	//int empty = 0;
	TrackNum t; for(t=0;t<AM_MAX_TRK;t++){
		TrackDispNum d = arr_t_to_d(tl, t);
		ArrTrk* atr = tl->display[d];
		AMTrack* tr = atr ? atr->track : NULL;
		/*
		if(!tl->order[t]){
			printf("  %i: NULL\n", t);
			empty++;
			if(empty > 1)break;
		}
		*/
		if(t < 8) printf("  %i %i %s\n", t, d, tr ? tr->name : "");
	}

	UNDERLINE;
}
#endif


