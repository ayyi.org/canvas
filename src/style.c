/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include "style.h"


static void
palette_from_style(Palette palette, int i, gboolean bg, GtkStyle* style, GtkStateType state)
{
	palette[i] = am_gdk_to_rgba(bg ? &style->bg[state] : &style->fg[state]);
}


/*
 *  Must be called by the application after the gtk widget styles have been initialised.
 */
void
am_palette_set_style(Palette palette, GtkStyle* style)
{
	#define BG true
	#define FG false

	palette_from_style(palette, 0, BG, style, GTK_STATE_SELECTED);
	palette_from_style(palette, 1, FG, style, GTK_STATE_NORMAL);
	palette_from_style(palette, 2, FG, style, GTK_STATE_SELECTED);
	palette_from_style(palette, 3, BG, style, GTK_STATE_NORMAL);

	palette[4] = am_gdk_to_rgba(&style->base[GTK_STATE_SELECTED]);

	am_song__emit("palette-change");
}


/*
 *  Gives us a gdkcolor from the given colour index number.
 */
void
am_palette_lookup(Palette palette, GdkColor* color, int c_idx)
{
	g_return_if_fail(c_idx < AM_MAX_COLOUR);

	int r = (palette[c_idx] & 0xff000000) >> 24;
	int g = (palette[c_idx] & 0x00ff0000) >> 16;
	int b = (palette[c_idx] & 0x0000ff00) >>  8;

	*color = (GdkColor){
		.red = r * 0xff,
		.green = g * 0xff,
		.blue = b * 0xff,
	};
}


void
am_rgba_to_gdk(GdkColor* colour, uint32_t rgba)
{
	g_return_if_fail(colour);

	colour->red   = (rgba & 0xff000000) >> 16;
	colour->green = (rgba & 0x00ff0000) >> 8;
	colour->blue  = (rgba & 0x0000ff00);
}


uint32_t
am_gdk_to_rgba(GdkColor* color)
{
	return ((color->red / 0x100) << 24) + ((color->green / 0x100) << 16) + ((color->blue / 0x100) << 8) + 0xff;
}


void
colour_get_style_base(GdkColor* color, int state)
{
  //get the default style base colour for the given widget state.

  GtkStyle* style = gtk_widget_get_default_style();
  color->red   = style->base[state].red;
  color->green = style->base[state].green;
  color->blue  = style->base[state].blue;
}


void
colour_get_style_bg_(GdkColor* color, int state)
{
  //get the default style background colour for the given widget state.

  GtkStyle* style = gtk_widget_get_default_style();
  color->red   = style->bg[state].red;
  color->green = style->bg[state].green;
  color->blue  = style->bg[state].blue;
}


void
colour_get_style_fg(GdkColor* color, int state)
{
  //get the default style foreground colour for the given widget state.

  GtkStyle* style = gtk_widget_get_default_style();
  color->red   = style->fg[state].red;
  color->green = style->fg[state].green;
  color->blue  = style->fg[state].blue;
}


