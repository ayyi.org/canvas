/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __gui_types_h__
#define __gui_types_h__

#include <gdl/gdl.h>
#include "model/track.h"

#define MAX_RECENT_SONGS 4

#define MAX_SENDS 16
#define MAX_TOOLBAR_BUT 20
#define MAX_TRANSPORT_BUT 6

#define PART_OUTLINE_WIDTH 2.0 //this is global, as the track needs to know this as well as the part.
#define MAX_PART_HEIGHT 1024

#define EXPAND_TRUE 1
#define EXPAND_FALSE 0
#define FILL_TRUE 1
#define FILL_FALSE 0
#define NON_HOMOGENOUS 0
#define HAS_ALPHA_TRUE 1
#define HAS_ALPHA_FALSE 0
#define BITS_PER_PIXEL 8
#define NO_PADDING 0
#define _8_BITS_PER_CHAR 8
#define HANDLED TRUE
#define NOT_HANDLED FALSE
#define SCROLL_TIMEOUT_MS 50

#define SCROLL_EDGE_WIDTH 3       //distance of mouse from window edge to trigger scrolling.
#define SCROLL_MULTIPLIER 4
#define SCROLL_MAX_SPEED 15       //the max pixels we scroll in one operation. (timeout time will also impact)

#define FREE_SEGMENTS 1

#define USER_CONFIG_DIR ".ayyigtk"

#define ZERO_TERMINATED FALSE

typedef struct _AyyiPanel          AyyiPanel;
typedef struct _AyyiPanelClass     AyyiPanelClass;
typedef struct _AyyiWindow         AyyiWindow;
typedef struct _Arrange            Arrange;
typedef struct _AyyiMixerWin       MixerWin;
typedef struct _AyyiMixerWin       AyyiMixerWin;
typedef struct _AyyiGlMixerWin     GlMixerWin;
typedef struct _AyyiGlMixerWin     AyyiGlMixerWin;
typedef struct _AyyiListWin        AyyiListWin;
typedef struct _AyyiEventWin       EventWindow;
typedef struct _AyyiEventWin       AyyiEventWin;
typedef struct _AyyiColourWin      ColourWin;
typedef struct _AyyiColourWin      AyyiColourWin;
typedef struct _AyyiInspectorWin   InspectorWin;
typedef struct _AyyiInspectorWin   AyyiInspectorWin;
typedef struct _AyyiPoolWin        PoolWindow;
typedef struct _AyyiPoolWin        AyyiPoolWin;
typedef struct _AyyiTransportWin   TransportWin;
typedef struct _AyyiTransportWin   AyyiTransportWin;
typedef struct _AyyiAudioWin       AudioWin;
typedef struct _AyyiAudioWin       AyyiAudioWin;
typedef struct _AyyiPluginWin      PluginWindow;
typedef struct _AyyiPluginWin      AyyiPluginWin;
typedef struct _AyyiFilePanel      AyyiFilePanel;
typedef struct _AyyiHelpWin        HelpWindow;
typedef struct _AyyiHelpWin        AyyiHelpWin;
typedef struct _AyyiLogWin         LogWindow;
typedef struct _AyyiLogWin         AyyiLogWin;
typedef struct _AyyiMidiWin        MidiWindow;
typedef struct _AyyiMidiWin        AyyiMidiWin;
typedef struct _AyyiShortcutsWin   ShortcutsWin;
typedef struct _AyyiSpectrogramWin SpectrogramWin;
typedef struct _AyyiSpectrogramWin AyyiSpectrogramWin;
typedef struct _AyyiLv2Win         AyyiLv2Win;
typedef struct _AyyiMeterWin       AyyiMeterWin;
typedef struct _dock_win           dock_win;
typedef struct _GimpActionGroup    GimpActionGroup;
typedef struct _EventField         EventField;
typedef struct _ConfigWindow       ConfigWindow;
typedef struct _CanvasOp           CanvasOp;
typedef struct _GnomeCanvasOp      GnomeCanvasOp;
typedef struct _clutter_canvas_op  ClutterCanvasOp;
typedef struct _ArrTrack           ArrTrk;
typedef struct _ArrTrackMidi       ArrTrackMidi;
typedef struct _AyyiMidiNote       MidiNote;
typedef int                        TrackDispNum;
typedef GType                      PanelType;
typedef struct _PartLocal          LocalPart;
typedef struct _AyyiMixerStrip     Strip;
typedef struct _AyyiMixerStrip     AyyiMixerStrip;
typedef struct _MasterStrip        MasterStrip;
typedef struct _StripInsert        StripInsert;
typedef struct _song_overview      song_overview;
typedef struct _SMConfig           SMConfig;
typedef struct _rulerbar           rulerbar;
typedef struct _scroll_op          scroll_op;
typedef int                        CanvasType;
typedef GtkWidget* (*NewCanvas)    (Arrange*);
typedef struct _canvas             ArrCanvas;
typedef struct _GlCanvas           GlCanvas;
typedef struct _cl_canvas          ClCanvas;
typedef struct _cl_blocks          cl_blocks;
typedef struct _gui_pool_item      GuiPoolItem;
typedef struct _GnomeCanvasPart    GnomeCanvasPart;
typedef struct _GnomeCanvasMap     GnomeCanvasMap;
typedef struct _ArrAutoPath        ArrAutoPath;
typedef struct _ToolbarButton      ToolbarButton;
typedef struct _TransportButton    TransportButton;
typedef struct _TrackControlMidi   TrackControlMidi;
typedef struct _SongMap            SongMap;
typedef struct _glyph              Glyph;
typedef struct _ImageCache         ImageCache;
typedef struct _GimpConfig         GimpConfig;
typedef struct _AyyiApp            AyyiApp;
typedef struct _ViewOption         ViewOption;
typedef struct _MenuDef            MenuDef;
typedef struct _ConfigParamValue   ConfigParamValue;
typedef struct _ViewObject         ViewObject;
typedef struct _SnapMode           SnapMode;
typedef struct _GuiSong            GuiSong;
typedef struct _Statusbar          Statusbar;
typedef int                        TrackNum;

typedef void   (*TrackCallback)    (AMTrack*, GError**, gpointer);

typedef struct {
    double x;
    double y;
    double width;
    double height;
} Rectangle;

typedef struct {
    int x1, y1, x2, y2;
} iRegion;

typedef struct {
    int width;
    int height;
} Size;

typedef struct {
    double x, y;
} Ptd;

typedef struct {
    double x1, y1, x2, y2;
} DRect;

typedef struct { Pathcode code; double x; double y; } VPath;

typedef struct
{
    guint         id;
    GdkWindow*    window;
    GdkCursorType type;
} CursorTimer;

typedef struct
{
    char    name[64];
    int     utype;
    union {
        void (*i)(AyyiPanel*, int);
        void (*f)(AyyiPanel*, float);
        void (*c)(AyyiPanel*, const char*);
        void (*b)(AyyiPanel*, bool);
    }         set;
    AMVal min;
    AMVal max;
    AMVal dfault;

} ConfigParam;

struct _ConfigParamValue
{
    ConfigParam* param;
	AMVal        val;
};

struct _SMConfig
{
    char*    svgdir;             // path for svg files.
#ifdef USE_LV2
    char*    plugindir;          // path for private so's
#endif
    char*    accels_file;
    int      arr_scrollwin_left;
    uint32_t part_outline_colour;
    uint32_t part_outline_colour_selected;
    char*    arr_background_image;
#if 0
    uint32_t arr_background_colour;
#endif
    guchar   snap_mode;      
    char*    icon_theme;      

    GArray*  windows;
};

typedef enum {
    ZOOM_H    = 1,
    ZOOM_V    = 2,
    ZOOM_BOTH = 3,
} ZoomType;

enum {
  CURSOR_NORMAL,
  CURSOR_MOVE,
  CURSOR_PENCIL,
  CURSOR_ZOOM_IN,
  CURSOR_ZOOM_OUT,
  CURSOR_CROSSHAIR,
  CURSOR_SCISSORS,
  CURSOR_HAND1,
  CURSOR_LEFT_PTR,
  CURSOR_WATCH,
  CURSOR_H_DOUBLE_ARROW,
  CURSOR_V_DOUBLE_ARROW,
  /*
  CURSOR_HORIZ,
  CURSOR_HORIZ_PLUS,
  CURSOR_HORIZ_MINUS,
  CURSOR_NEEDLE,
  CURSOR_NOISE,
  */
  CURSOR_HAND_OPEN,
  CURSOR_HAND_CLOSE,
  CURSOR_MAX
};

typedef enum{
  APP_STATE_STARTED,
  APP_STATE_ENGINE_OK,
  APP_STATE_SONG_LOADED,
  APP_STATE_SONG_LOAD_FAILED,
  APP_STATE_ABORT,
  APP_STATE_SHUTDOWN,
} AppState;
#define SONG_LOADED (app->state==APP_STATE_SONG_LOADED)

extern AyyiApp* app;

typedef struct
{
  GtkWidget* widget;
  char       detail[32];
  int        handler_id;
  GCallback  callback;
  gpointer   user_data;
  gboolean   blocked;
} SignalNfo;

struct _MenuDef
{
	char*     label;
	GCallback callback;
	char*     icon;
	bool      sensitive;
};

typedef struct {
	int           code;
	int           mask;
} AMKey;

typedef struct {               // variation on GtkActionEntry - has two keys instead of one.
    char          name[16];
    AMKey         key[2];
    gpointer      callback;
    GtkStockItem* stock_item;
    gpointer      user_data;
} AMAccel;

typedef struct {
    int start, end;
} iRange;

typedef struct {
    float start, end;
} fRange;

enum {
  TYPE_PART,
  SELECTION_TYPE_TRK
};

enum{
  METER_NONE = 0,
  METER_JAMIN,
  METER_GTK,
  METER_BST,
};

typedef struct {
    int         sendcount;            // the default number of *visible* aux-send widgets.
    bool        rotary_aux;
} MixerOptions;

//dnd:
enum {
    TARGET_APP_COLLECTION_MEMBER,
    AYYI_TARGET_URI_LIST,
    TARGET_TEXT_PLAIN
};

#ifdef HAVE_GLIB_2_32
#define SHOW_TREEVIEW_LINES(TREE) \
	GValue gval = {0,}; \
	g_value_init(&gval, G_TYPE_CHAR); \
	g_value_set_schar(&gval, '1'); \
	g_object_set_property(G_OBJECT(TREE), "enable-tree-lines", &gval);
#else
#define SHOW_TREEVIEW_LINES(TREE) \
	GValue gval = {0,}; \
	g_value_init(&gval, G_TYPE_CHAR); \
	g_value_set_char(&gval, '1'); \
	g_object_set_property(G_OBJECT(TREE), "enable-tree-lines", &gval);
#endif

#define log_print ayyi_log_print

#ifndef true
#define true  1
#define false 0
#endif

#endif
