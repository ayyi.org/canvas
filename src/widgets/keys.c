/*
  This file is part of AyyiGtk. http://ayyi.org
  copyright (C) 2004-2009 Tim Orford <tim@orford.org>

  Widget implementing an interactive musical keyboard.
  Based on GtkCurve, a child class of GtkDrawingarea.
 */

/* GTK - The GIMP Toolkit
 * Copyright (C) 1997 David Mosberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, version 3.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */


#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gtk/gtksignal.h>
#include <cairo.h>
#include "debug/debug.h"
#include "windows.h"
#include "panels/seqedit.h"
#include "widgets/keys.h"

enum {
  CHANGE_HIGHLIGHT,
  CHANGE_NOTE_HEIGHT,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

gboolean        note_is_black                (int);
int             note_get_white_num           (int);
int             note_get_black_num           (int);
const char*     note_format                  (int);


enum {
  PROP_0,
  PROP_CURVE_TYPE,
  PROP_MIN_X,
  PROP_MAX_X,
  PROP_MIN_Y,
  PROP_MAX_Y
};

typedef enum {
  FEATURE_NONE,
  FEATURE_TOP_SCROLL,
  FEATURE_BOTTOM_SCROLL,
} KeyFeature;

#define c_keyarea_x 40   // total width of keyboard //FIXME this should really be allocwidth-2
#define c_keyoffset_x 15 // the width of the lhs space where the octave labels go.
#define c_key_x 25       // width of each key FIXME shouldnt this be allocwidth - c_keyoffset_x ?
#define n_keys 88
#define SCROLL_BUTTON_HEIGHT 10
#define MAX_NOTE_HEIGHT 20

static GtkDrawingAreaClass* parent_class = NULL;
static gboolean             button_pressed = false;
static guint                timer_id = 0;
static gint                 y = 0;
struct {
	gboolean ctl;
} op;

static void     gtk_keys_class_init          (GtkKeysClass*);
static void     gtk_keys_init                (GtkKeys*);
static void     gtk_keys_realize             (GtkWidget*);
static void     gtk_keys_size_allocate       (GtkWidget*, GtkAllocation*);
static gboolean gtk_keys_focus_in            (GtkWidget*, GdkEventFocus*);
static gboolean gtk_keys_focus_out           (GtkWidget*, GdkEventFocus*);
static gint     gtk_keys_button_press        (GtkWidget*, GdkEventButton*);
static gint     gtk_keys_button_release      (GtkWidget*, GdkEventButton*);
static gint     gtk_keys_motion_notify       (GtkWidget*, GdkEventMotion*);
static void     gtk_keys_get_property        (GObject*, guint param_id, GValue*, GParamSpec*);
static void     gtk_keys_set_property        (GObject*, guint param_id, const GValue*, GParamSpec*);
static void     gtk_keys_finalize            (GObject*);
static gint     gtk_keys_expose              (GtkWidget*, GdkEventExpose*, GtkKeys*);
static void     keys_draw                    (GtkKeys*);
static void     keys_update_pixmap           (GtkWidget*);
static void     keys_update_pixmap_minimalist(GtkWidget*);
static void     keys_change_highlight        (GtkKeys*, int);
static void     keys_change_note_height      (GtkKeys*, int);

static void     keys_change_vert             ();
static cairo_t* keys_drawable_to_cairo       (GdkDrawable*, GdkRectangle*);
static void     start_mouse_timer            (GtkWidget*);
static void     stop_mouse_timer             ();
static int      keys_get_note_num_from_y     (GtkWidget*, int y);
static int      gtk_keys_get_n_visible       (GtkWidget*);
static gboolean scroll_if_required           (GtkWidget*, int y);
static int      gtk_keys_pick_feature        (GtkWidget*, int y);
static void     keys_allocate_pixmap         (GtkKeys*);
static void     keys_delete_pixmap           (GtkKeys*);



GType
gtk_keys_get_type(void)
{
  static GType keys_type = 0;

  if(!keys_type){
    static const GTypeInfo keys_info = {
       sizeof (GtkKeysClass),
       NULL,     /* base_init */
       NULL,     /* base_finalize */
       (GClassInitFunc) gtk_keys_class_init,
       NULL,     /* class_finalize */
       NULL,     /* class_data */
       sizeof (GtkKeys),
       0,        /* n_preallocs */
       (GInstanceInitFunc)gtk_keys_init,
    };

    keys_type = g_type_register_static(GTK_TYPE_DRAWING_AREA, "GtkKeys", &keys_info, 0);
  }
  return keys_type;
}


static void
gtk_keys_class_init(GtkKeysClass *class)
{
  GObjectClass   *gobject_class = G_OBJECT_CLASS(class);
  GtkWidgetClass *widget_class  = (GtkWidgetClass*)class;

  parent_class = g_type_class_peek_parent(class);
  
  gobject_class->finalize            = gtk_keys_finalize;

  gobject_class->set_property        = gtk_keys_set_property;
  gobject_class->get_property        = gtk_keys_get_property;
 
  widget_class->realize              = gtk_keys_realize;
  widget_class->size_allocate        = gtk_keys_size_allocate;
  widget_class->button_press_event   = gtk_keys_button_press;
  widget_class->button_release_event = gtk_keys_button_release;
  widget_class->motion_notify_event  = gtk_keys_motion_notify;
  widget_class->focus_in_event       = gtk_keys_focus_in;
  widget_class->focus_out_event      = gtk_keys_focus_out;

  class->change_highlight            = keys_change_highlight;
  class->change_note_height          = keys_change_note_height;

  signals[CHANGE_HIGHLIGHT] = g_signal_new ("change_highlight",
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (GtkKeysClass, change_highlight),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__INT,
                  G_TYPE_NONE, 1,
                  G_TYPE_INT);
  signals[CHANGE_NOTE_HEIGHT] = g_signal_new ("change_note_height",
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (GtkKeysClass, change_note_height),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__INT,
                  G_TYPE_NONE, 1,
                  G_TYPE_INT);

  op.ctl = false;
}


static void
gtk_keys_init(GtkKeys *keys)
{
  GTK_WIDGET_SET_FLAGS (keys, GTK_CAN_FOCUS/* | GTK_RECEIVES_DEFAULT*/);

  keys->cursor_type = GDK_TOP_LEFT_ARROW;
  keys->pixmap = NULL;

  keys->min_x = 0.0;
  keys->max_x = 1.0;
  keys->min_y = 0.0;
  keys->max_y = 1.0;

  /*
  gint old_mask = gtk_widget_get_events(GTK_WIDGET(keys));
  gtk_widget_set_events(GTK_WIDGET(keys), old_mask | GRAPH_MASK);
  */
  g_signal_connect(keys, "expose-event", G_CALLBACK(gtk_keys_expose), keys);

  keys->gc          = NULL;

  keys->num_keys    = 88; //the number of keys to draw on the keyboard.
  keys->note_height = 6;

  keys->black.red   = 0x0000;
  keys->black.green = 0x0000;
  keys->black.blue  = 0x0000;
  keys->white.red   = 0xffff;
  keys->white.green = 0xffff;
  keys->white.blue  = 0xffff;
  keys->grey.red    = 0x7777;
  keys->grey.green  = 0x7777;
  keys->grey.blue   = 0x7777;

  GdkColormap *colormap = gdk_colormap_get_system();
  gdk_colormap_alloc_color(colormap, &(keys->black), TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(keys->white), TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(keys->grey),  TRUE, TRUE);
}


GtkWidget*
gtk_keys_new(void* none, GtkAdjustment *v_adj)
{
  GtkWidget* widget = g_object_new(GTK_TYPE_KEYS, NULL);
  GtkKeys* keys = GTK_KEYS(widget);

  keys->vadjust = v_adj;

/*
//(GDK_EXPOSURE_MASK | GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK |	\
//			 GDK_ENTER_NOTIFY_MASK |	\
//			 GDK_BUTTON_RELEASE_MASK |	\
//			 GDK_BUTTON1_MOTION_MASK)
*/
  gtk_widget_add_events(widget, GDK_BUTTON_PRESS_MASK/* | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK*/);

  return widget;
}


static void 
gtk_keys_realize(GtkWidget *widget)
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_KEYS(widget));

  GtkKeys *k = GTK_KEYS(widget);

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

  GdkWindowAttr attributes;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK;
                            //GDK_ENTER_NOTIFY_MASK |
                            //GDK_LEAVE_NOTIFY_MASK |
                            //GDK_BUTTON_MOTION_MASK |
                            //GDK_BUTTON1_MOTION_MASK);
  gint attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new(gtk_widget_get_parent_window(widget), &attributes, attributes_mask);
  gdk_window_set_user_data(widget->window, widget);

  widget->style = gtk_style_attach(widget->style, widget->window);

  //gtkdrawingarea.c also calls this - any significance?
  //gtk_drawing_area_send_configure(GTK_DRAWING_AREA(widget));

  k->gc = gdk_gc_new(widget->window);
  gdk_window_clear(widget->window); //sets the background onto the whole window.

  keys_allocate_pixmap(k);

  //m_vadjust->signal_value_changed().connect( slot( *this, &seqkeys::change_vert ));
  keys_change_vert();
}


static void
gtk_keys_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  g_return_if_fail (widget);
  g_return_if_fail (allocation);
  GtkKeys* k = GTK_KEYS(widget);

  widget->allocation = *allocation; //allocation contains x,y, width,height (all gint).

  if(GTK_WIDGET_REALIZED(widget)){
    gdk_window_move_resize(widget->window,
                    allocation->x, allocation->y, //x,y pos relative to windows parent. Normally zero.
                    allocation->width,
                    allocation->height
                  );
    keys_allocate_pixmap(k);
  }
}


static void
keys_allocate_pixmap(GtkKeys* k)
{
  //if neccesary, create a new backing pixmap.
  //-we vertically scroll a tall pixmap, so pixmap only needs to change if widget width changes, or if note height changes.

  GtkWidget* widget = (GtkWidget*)k;

  if(k->pixmap){
    int w, h; gdk_drawable_get_size(k->pixmap, &w, &h);
    if(w != widget->allocation.width){
      keys_delete_pixmap(k);
    }
  }
  if(!k->pixmap){
    int height = k->note_height * n_keys;
    k->pixmap = gdk_pixmap_new(widget->window, widget->allocation.width, height, -1);
  }

  keys_draw(k);
}


void
gtk_keys_set_highlighted_note(GtkKeys* keys, int note)
{
  keys->highlighted_note = note;
  keys_draw(keys);
  gtk_widget_queue_draw(GTK_WIDGET(keys));
  g_signal_emit_by_name (keys, "change_highlight", note);
}


void
gtk_keys_set_note_height(GtkKeys* keys, int height)
{
  //set the height that corresponds to the note height in associated canvases.
  //The height of each key is larger than this due to the overlap.

  if(height < 1 || height > MAX_NOTE_HEIGHT) return;

  keys->note_height = height;

  keys_delete_pixmap(keys);
  keys_allocate_pixmap(keys);

  keys_draw(keys);
  gtk_widget_queue_draw(GTK_WIDGET(keys));
}


static gboolean
scroll_if_required(GtkWidget *widget, int y)
{
  GtkKeys* k = GTK_KEYS(widget);

  KeyFeature f;
  if((f = gtk_keys_pick_feature(widget, y))){
    dbg(3, "adj=%.1f", k->vadjust->value);
    switch(f){
      case FEATURE_TOP_SCROLL:
        if(op.ctl){
          //magnify
          gtk_keys_set_note_height(k, MIN(k->note_height + 1, MAX_NOTE_HEIGHT));
          g_signal_emit_by_name (k, "change_note_height", k->note_height);
        }else{
          //scroll up
          if (k->vadjust) {
            double max = k->vadjust->upper - gtk_keys_get_n_visible(widget) / 2;
            if (k->vadjust->value < max) {
              gtk_adjustment_set_value(k->vadjust, k->vadjust->value + 1);
              gtk_widget_queue_draw(widget);
            }
          }
        }
        return true;
        break;
      case FEATURE_BOTTOM_SCROLL:
        if(op.ctl){
          gtk_keys_set_note_height(k, MAX(k->note_height - 1, 1));
          g_signal_emit_by_name (k, "change_note_height", k->note_height);
        }else{
          //scroll down
          if (k->vadjust) {
            double min = k->vadjust->lower + gtk_keys_get_n_visible(widget) / 2;

            int w, h; gdk_drawable_get_size(k->pixmap, &w, &h);

#define EXCESS 14
            dbg(0, "val=%.1f min=%.1f h=%i=%i top=%i", k->vadjust->value, min, h, n_keys * k->note_height, k->vadjust->value + gtk_keys_get_n_visible(widget) / 2);
            if (k->vadjust->value > min - EXCESS) {
              gtk_adjustment_set_value(k->vadjust, k->vadjust->value - 1);
              gtk_widget_queue_draw(widget);
            }
          }
        }
        return true;
        break;
      default:
        break;
    }
  }

  return false;
}


static int
gtk_keys_get_n_visible(GtkWidget *widget)
{
  //return the vertical keyboard span.

  GtkKeys* k = GTK_KEYS(widget);

  int track_height = widget->allocation.height;
  //dbg(0, "n=%i", track_height / k->note_height);
  return track_height / k->note_height;
}


static int
gtk_keys_pick_feature(GtkWidget *widget, int y)
{
  if(y < SCROLL_BUTTON_HEIGHT) return FEATURE_TOP_SCROLL;
  else if (y > widget->allocation.height - SCROLL_BUTTON_HEIGHT) return FEATURE_BOTTOM_SCROLL;
  return FEATURE_NONE;
}


static int
keys_get_note_num_from_y(GtkWidget *widget, int y)
{
  GtkKeys* k = GTK_KEYS(widget);

  int top_note = k->vadjust->value + gtk_keys_get_n_visible(widget) / 2;
  int note_num = top_note - ((y + k->note_height / 2) / k->note_height);
  dbg(2, "y=%i offset=%.0f top_note=%i note=%i", y, k->vadjust->value, top_note, note_num);
  return note_num;
}


static gint
gtk_keys_button_press(GtkWidget *widget, GdkEventButton *event)
{
  g_return_val_if_fail (widget, FALSE);
  g_return_val_if_fail (event, FALSE);
  GtkKeys* k = GTK_KEYS(widget);

  if(event->button != 1) return NOT_HANDLED;

  //gtk_widget_grab_focus (widget);
  if (event->state & GDK_SHIFT_MASK) {}
  op.ctl = event->state & GDK_CONTROL_MASK;

  y = event->y;
  if(!scroll_if_required(widget, event->y)){

    int note = keys_get_note_num_from_y(widget, y);
    if (note) gtk_keys_set_highlighted_note(k, note);
  }

  button_pressed = true;
  start_mouse_timer(widget);
  return HANDLED;
}


static gint
gtk_keys_button_release(GtkWidget *widget, GdkEventButton *event)
{
  button_pressed = false;
  stop_mouse_timer();

  //this prevents the dock from getting the signal
  return HANDLED;
}


static gint
gtk_keys_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
  GtkKeys* k = GTK_KEYS(widget);

  int x = event->x;
  int y = event->y;
  GdkModifierType mods;
  if (event->is_hint || (event->window != widget->window)) gdk_window_get_pointer (widget->window, &x, &y, &mods);

  switch(gtk_keys_pick_feature(widget, y)){
    case FEATURE_TOP_SCROLL:
    case FEATURE_BOTTOM_SCROLL:
      gdk_window_set_cursor(widget->window, gdk_cursor_new(GDK_SB_V_DOUBLE_ARROW));
      break;
    case FEATURE_NONE:
      ;int note_num = keys_get_note_num_from_y(widget, y);
      gtk_keys_set_highlighted_note(k, note_num); //temp! need new highlighter.
      gdk_window_set_cursor(widget->window, 0);
      break;
  }

  return false;
}


static gboolean 
gtk_keys_focus_in(GtkWidget *widget, GdkEventFocus* event)
{
  dbg(0, "...");
  gtk_widget_queue_draw (widget);
  return FALSE;
}


static gboolean 
gtk_keys_focus_out(GtkWidget *widget, GdkEventFocus* event)
{
  gtk_widget_queue_draw (widget);
  return FALSE;
}


static void
gtk_keys_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  //GtkKeys *keys = GTK_KEYS(object);
  
  switch (prop_id)
    {
    //case PROP_MIN_X:
    //  gtk_curve_set_range (curve, g_value_get_float (value), curve->max_x,
	//		   curve->min_y, curve->max_y);
    //  break;
    ////case PROP_MAX_X:
    //  gtk_curve_set_range (curve, curve->min_x, g_value_get_float (value),
	//		   curve->min_y, curve->max_y);
    //  break;	
    //case PROP_MIN_Y:
    //  gtk_curve_set_range (curve, curve->min_x, curve->max_x,
	//		   g_value_get_float (value), curve->max_y);
    //  break;
    //case PROP_MAX_Y:
    //  gtk_curve_set_range (curve, curve->min_x, curve->max_x,
	//		   curve->min_y, g_value_get_float (value));
    //  break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gtk_keys_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  //GtkKeys *keys = GTK_KEYS(object);

  switch (prop_id)
    {
    /*
    case PROP_MIN_X:
      g_value_set_float (value, curve->min_x);
      break;
    case PROP_MAX_X:
      g_value_set_float (value, curve->max_x);
      break;
    case PROP_MIN_Y:
      g_value_set_float (value, curve->min_y);
      break;
    case PROP_MAX_Y:
      g_value_set_float (value, curve->max_y);
      break;
    */
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
keys_draw(GtkKeys *k)
{
  //redraws the pixmap.

  GtkWidget* widget = GTK_WIDGET(k);

  g_return_if_fail(k->pixmap);

#if 0
  GtkStateType state = GTK_STATE_NORMAL;
  if (!GTK_WIDGET_IS_SENSITIVE(GTK_WIDGET(k))) state = GTK_STATE_INSENSITIVE;
#endif

  if (0) {
    keys_update_pixmap(widget);
  } else {
    keys_update_pixmap_minimalist(widget);
  }
}


static void
gtk_keys_paint_focus(GtkWidget* widget, GdkEventExpose *event)
{
  if (GTK_WIDGET_HAS_FOCUS(widget)) {
    gint border_width = 1;
    gint border_width_h = MAX(border_width, 2); //must not be drawn over black border.
    gint x = /*widget->allocation.x +*/ border_width_h;
    gint y = widget->allocation.y + border_width;
    gint width  = widget->allocation.width  - border_width_h * 2;
    gint height = widget->allocation.height - border_width * 2;
    printf("%s(): painting focus... border=%i x=%i y=%i width=%i height=%i\n", __func__, border_width, x, y, width, height);
    if(width > 1 && height > 1){
      //GdkRectangle *area = &event->area;

      gtk_paint_focus (widget->style, widget->window, GTK_WIDGET_STATE (widget),
                       &event->area, widget, "button",
                       x, y, width, height);
      }
  }
}


static gint
gtk_keys_expose(GtkWidget *widget, GdkEventExpose *event, GtkKeys *k)
{
  g_return_val_if_fail(k->pixmap, false);
  //g_return_val_if_fail(k->debug_drawn, false);
  gint retval = FALSE;

  gint width = widget->allocation.width;
  gint height = widget->allocation.height;

  GdkColormap *colormap = gdk_colormap_get_system(); //free?
  gdk_drawable_set_colormap(GDK_DRAWABLE(k->pixmap), colormap);

  GtkStateType state = GTK_STATE_NORMAL;
  if (!GTK_WIDGET_IS_SENSITIVE(widget)) state = GTK_STATE_INSENSITIVE;

  //blit the pixmap onto the window:
  gint middle_note = k->vadjust->value;
  gint top_note = middle_note + gtk_keys_get_n_visible(widget) / 2;
  gint ysrc = (k->num_keys - top_note) * k->note_height;
  dbg(2, "top_note=%i ysrc=%i bottom=%i", top_note, ysrc, ysrc + height);
  if (ysrc < 0) pwarn("y_src < 0 !");
  gdk_draw_drawable(widget->window, widget->style->fg_gc[state], k->pixmap, 0, ysrc, 0, 0, width, height);

  gtk_keys_paint_focus(widget, event);

  return retval;
}


static void
keys_change_highlight(GtkKeys *k, int note)
{
}


static void
keys_change_note_height(GtkKeys *k, int height)
{
}


static void
keys_update_pixmap(GtkWidget *widget)
{
  GtkKeys *k = GTK_KEYS(widget);
  gint key_h = (k->note_height * 12) / 7;

  if (!GDK_IS_DRAWABLE(k->pixmap)) return;

  int w,h,w2,h2;
  gdk_drawable_get_size(k->pixmap, &w, &h);
  gdk_drawable_get_size(widget->window, &w2, &h2);

  //draw black border:
  gdk_gc_set_foreground(k->gc, &(k->black));
  gdk_draw_rectangle(k->pixmap, k->gc, TRUE,
                             0,
                             0,
                             w, //c_keyarea_x,
                             h
                             );

  //??? smaller area of white:
  gdk_gc_set_foreground(k->gc, &(k->white));
  gdk_draw_rectangle(k->pixmap, k->gc, TRUE,
                             1,
                             1,
                             c_keyoffset_x - 1,
                             h - 2
                             );

  int i;
  for(i=0;i<k->num_keys;i++){
    //draw all keys white:
    gdk_gc_set_foreground(k->gc, &(k->white));
    gdk_draw_rectangle(k->pixmap, k->gc, TRUE,
                                 c_keyoffset_x + 1,
                                 (key_h * i) + 1,
                                 c_key_x - 2,
                                 key_h - 1 );

    //get which note in the octave:
    int key = (k->num_keys - i - 1) % 12;

    if ( key == 1 ||
         key == 3 ||
         key == 6 ||
         key == 8 ||
         key == 10 ){

      //draw black keys:
      gdk_gc_set_foreground(k->gc, &(k->black));
      gdk_draw_rectangle(k->pixmap, k->gc, TRUE,
                                     c_keyoffset_x + 1,
                                     (key_h * i) + 2,
                                     c_key_x - 3,
                                     key_h - 3 );
    }

    if(key == 0
            /////// || key == 2 || key == 4 || key == 5 || key == 7 || key == 9 || key == 11
            ){

            char note= ' ';
            if ( key == 0  ) note = 'c';
            if ( key == 2  ) note = 'd';
            if ( key == 4  ) note = 'e';
            if ( key == 5  ) note = 'f';
            if ( key == 7  ) note = 'g';
            if ( key == 9  ) note = 'a';
            if ( key == 11 ) note = 'b';

            // notes:
            char notes[5];

            sprintf(notes, "%c%1d", note, ((k->num_keys - i - 1) / 12) - 1);

            //i'm not familiar with this fn below, so i used the more verbose version.
            //p_font_renderer->render_string_on_drawable(k->gc,
            //                                           2,
            //                                           c_key_h * i - 1,
            //                                           m_pixmap, notes, font::BLACK );
            PangoLayout *playout = gtk_widget_create_pango_layout(widget, notes);
            PangoFontDescription *fdesc = pango_font_description_from_string("Bold 8 Black");
            pango_layout_set_font_description(playout, fdesc);
            gdk_draw_layout(k->pixmap,
                     widget->style->fg_gc[GTK_STATE_NORMAL],
                     2,             //x
                     key_h * i - 1, //y
                     playout);
            g_object_unref(playout);
    }
  }
}


float black_notes[5] = {6, 16, 36, 46, 56}; //divide by 70 to normalise

static void
keys_update_pixmap_minimalist(GtkWidget *widget)
{
  GtkKeys *k = GTK_KEYS(widget);
  gint key_h = (k->note_height * 12) / 7;

  if(!GDK_IS_DRAWABLE(k->pixmap)) return;

  int w, h;
  gdk_drawable_get_size(k->pixmap, &w, &h);

  GdkRectangle area = {0, 0, w, h};
  cairo_t* cr = keys_drawable_to_cairo (k->pixmap, &area);
  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);

  //draw black border/background:
  gdk_gc_set_foreground(k->gc, &(k->black));
  gdk_draw_rectangle(k->pixmap, k->gc, TRUE, 0, 0, w, h);

  int num_octaves = 8;
  //int note_pos[12]   = {0,   6,    10,  16,   20,  30,  36,   40,  46,   50,  56,   60 };

  int i, octave;
  //float octave_h = key_h * 7; //c_keyarea_y / num_octaves;
  float octave_h = k->note_height * 12;
  for(octave=0;octave<num_octaves;octave++){

    //draw white keys
    for(i=0;i<7;i++){
      cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
      cairo_rectangle (cr, 1, h - octave_h * (octave + i / 7.0), w - 2, -(key_h - 1));
      cairo_fill (cr);
    }

    //draw black keys
    for(i=0;i<5;i++){
      cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
      cairo_rectangle (cr, 1, h - octave_h * (octave + black_notes[i] / 70) + 0, w/2 - 1, -(key_h - 3));
      cairo_fill (cr);
    }

    char lbl[4];
    sprintf(lbl, "%i", octave);
    PangoLayout *playout = gtk_widget_create_pango_layout(widget, lbl);
    PangoFontDescription *fdesc = pango_font_description_from_string("Bold 6");
    pango_layout_set_font_description(playout, fdesc);
    gdk_draw_layout(k->pixmap,
                    widget->style->fg_gc[GTK_STATE_NORMAL],
                    11,                                // x
                    h - octave_h * octave - key_h + 1, // y
                    playout);
  }

  int note; if((note = k->highlighted_note)){
    octave = note / 12;
    cairo_set_source_rgb (cr, 1.0, 0.0, 0.0);

    if(note_is_black(note)){
//#if 0
      i = note_get_black_num(note);
      cairo_rectangle (cr, 1, h - octave_h * (octave + black_notes[i] / 70), w/2 - 1, -(key_h - 3));
//#endif
    }else{
      i = note_get_white_num(note);
      cairo_rectangle (cr, 1, h - octave_h * (octave + i / 7.0 - 1) + 1, w - 2, key_h - 2);
      dbg(2, "bottom=%.0f '%s' highlighted=%i %s white_num=%i", k->vadjust->lower, note_format(k->vadjust->lower), note, note_format(note), i);
    }
    cairo_fill (cr);
  }

  //indicate top and bottom of pixmap for debugging:
  cairo_set_source_rgb (cr, 1.0, 4.0, 0.0);
  cairo_rectangle (cr, 1, 0, w - 2, 2);
  cairo_fill (cr);
  cairo_rectangle (cr, 1, h-1, w - 2, h-3);
  cairo_fill (cr);

  //k->debug_drawn = true;
}


static void 
keys_change_vert(void)
{
  //update vars in response to a change in widget height?

  /* FIXME
  m_scroll_offset_key = (int)m_vadjust->get_value();
  m_scroll_offset_y   = m_scroll_offset_key * c_key_h,

  force_draw();
  */
}


static void
gtk_keys_finalize(GObject *object)
{
  GtkKeys *keys;

  g_return_if_fail(GTK_IS_KEYS(object));

  keys = GTK_KEYS(object);
  if (keys->pixmap) g_object_unref(keys->pixmap);
  //if (keys->point) g_free(keys->point);
  //if (curve->ctlpoint) g_free(curve->ctlpoint);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static cairo_t*
keys_drawable_to_cairo (GdkDrawable *window, GdkRectangle *area)
{
    g_return_val_if_fail (window != NULL, NULL);

    cairo_t *cr = (cairo_t*) gdk_cairo_create (window);
    cairo_set_line_width (cr, 1.0);
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_SQUARE);
    cairo_set_line_join (cr, CAIRO_LINE_JOIN_MITER);

    if (area) {
        cairo_rectangle (cr, area->x, area->y, area->width, area->height);
        cairo_clip_preserve (cr);
        cairo_new_path (cr);
    }

    return cr;
}


static void
keys_delete_pixmap(GtkKeys *k)
{
	g_object_unref(k->pixmap);
	k->pixmap = NULL;
}


static gboolean
on_mouse_timer(gpointer data)
{
	scroll_if_required(GTK_WIDGET(data), y);
	return button_pressed; //continue while mouse is held.
}


static void
start_mouse_timer(GtkWidget* widget)
{
	if (!timer_id) timer_id = g_timeout_add(100, on_mouse_timer, widget);
}


static void
stop_mouse_timer()
{
	//timer will terminate itself once button is released.

	timer_id = 0;
}


