#include "global.h"
#include "vala_utils.h"


#if 0
void
am_track__set_output_with_target(Trk* track, AyyiConnection* connection, AyyiHandler4 _handler, void* _target, void* data)
{
	static void* target; target = _target;
	static AyyiHandler4 handler; handler = _handler;

	void am_track__set_output_with_target_done(AyyiIdent ident , GError* error, gpointer data)
	{
		if(error) printf("error\n");
		call(handler, target, ident, error, data);
	}

	am_track__set_output_async(track, connection, am_track__set_output_with_target_done);
}


void
am_song__add_track_with_target(TrackType type, char* name, AyyiHandler4 _handler, void* _target, void* data)
{
	static void* target; target = _target;
	static AyyiHandler4 handler; handler = _handler;

	void am_song__add_track_with_target_done(AyyiIdent ident , GError* error, gpointer data)
	{
		if(error) printf("error\n");
		call(handler, target, ident, error, data);
	}

	am_song__add_track(type, name, 1, am_song__add_track_with_target_done, data);
}
#endif


