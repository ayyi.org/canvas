/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <stdio.h>
#include <glib.h>
#include <ayyi/ayyi.h>
#include <ayyi/ayyi_client.h>
#include "ayyi_example_plugin_so.h"
#include "ayyi_example_plugin_shm.h"

char hello[] = "hello";

static struct ExamplePlugin exampleplugin_info = {
	.api_version   = 1,
	.name          = "Name",
	.get_hello     = exampleplugin_get_hello,
};

static struct _AyyiPlugin pi = {
	PLUGIN_API_VERSION,
	"Ayyi Example Plugin",
	PLUGIN_TYPE_A,
	APPLICATION_SERVICE_NAME,
	DBUS_APP_PATH,
	DBUS_INTERFACE,
	&exampleplugin_info
};

DECLARE_PLUGIN(pi);
DECLARE_EXAMPLE_PLUGIN(exampleplugin_info);

const char*
exampleplugin_get_hello (AyyiPluginPtr plugin)
{
	/* TODO take the hello from shm instead of global
	struct _example_shm* example_shm = plugin->client_data;
	if(!example_shm){ printf("%s(): error! example_shm not set!\n", __func__); return 0; }
	return example_shm->hello;
	*/

	return hello;
}

