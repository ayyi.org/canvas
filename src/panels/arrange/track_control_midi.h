/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __track_control_midi_h__
#define __track_control_midi_h__

#ifndef DEBUG_TRACKCONTROL

#include <gdk/gdk.h>
#include <gtk/gtkbox.h>
#include "track_control.h"

G_BEGIN_DECLS

#define TYPE_TRACK_CONTROL_MIDI            (track_control_midi_get_type ())
#define TRACK_CONTROL_MIDI(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TRACK_CONTROL_MIDI, TrackControlMidi))
#define TRACK_CONTROL_MIDI_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TRACK_CONTROL_MIDI, TrackControlMidiClass))
#define IS_TRACK_CONTROL_MIDI(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TRACK_CONTROL_MIDI))
#define IS_TRACK_CONTROL_MIDI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TRACK_CONTROL_MIDI))
#define TRACK_CONTROL_MIDI_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TRACK_CONTROL_MIDI, TrackControlMidiClass))

typedef struct _TrackControlMidiClass  TrackControlMidiClass;

struct _TrackControlMidi
{
  TrackControl   TrackControl;
  GtkWidget*     keys;
  GtkAdjustment* vadjust;         // controls scrolling of keyboard range.
};

struct _TrackControlMidiClass
{
    TrackControlClass parent_class;
    int          test[256]; //FIXME for error in class definition - check if is still needed
};


GType      track_control_midi_get_type           (void) G_GNUC_CONST;
GtkWidget* track_control_midi_new                (ArrTrk*, gint spacing);
void       track_control_midi_redraw             (TrackControl*);
void       track_control_midi_set_height         (TrackControlMidi*, int height);
void       track_control_midi_highlight_note     (TrackControlMidi*, int note);


G_END_DECLS

#endif //DEBUG_TRACKCONTROL

#endif
