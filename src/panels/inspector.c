/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __inspector_c__
#include "global.h" 
#include <gdk/gdkkeysyms.h>

#include <model/song.h>
#include <model/channel.h>
#include <model/track_list.h>
#include "windows.h"
#include "mixer/strip.h"
#include "support.h"
#include "song.h"
#include "toolbar.h"
#include "inspector.h"

#define verify_inspector() windows__verify_pointer(panel, AYYI_TYPE_INSPECTOR_WIN)

struct _ParamConfig
{
   char*        name;
   AyyiPropType prop;        // eg PB_DEL.
   gpointer     callback;
};

struct _PlaybackParam
{
   char*       name;
   char        val[32];
   GtkWidget*  row;          // GtkHBox.
   GtkWidget*  wlabel;
   GtkWidget*  entry;
   int         prop;         // the songcore property number, eg PB_DEL.
};

static GObject* inspector__construct                 (GType, guint n_construct_properties, GObjectConstructParam*);
static void     inspector__on_ready                  (AyyiPanel*);
static void     inspector__destroy                   (GtkObject*);

static void     inspector__on_realise                (GtkWidget*, gpointer);
static void     inspector__on_vbox_allocate          (GtkWidget*, GtkAllocation*, gpointer);
//static int    inspector__drag_dataget              (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, AyyiPanel*);
static gint     inspector__drag_received             (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer);
static int      inspector__pb_delay_cb               (GtkWidget*, gpointer);
static void     inspector__receive_selection_notify  (AyyiPanel*);
static void     inspector__on_track_selection_change (Observable*, AMVal, gpointer);
static void     inspector__on_part_selection_change  (GObject*, AyyiPanel*, gpointer);
static void     inspector__on_channel_change         (GObject*, AMChannel*, AyyiPanel* sender, gpointer);
static void     inspector__on_channel_delete         (GObject*, AMChannel*, gpointer);
static void    _inspector__periodic_update           (GObject*, gpointer);
static void     inspector__periodic_update           (AyyiPanel*);
static void     inspector__on_track_label_change     (GtkWidget*, GtkWidget*, gpointer trk_num);

struct _ParamConfig pbconfig[3] = {
	{"delay", AYYI_PB_DELAY, inspector__pb_delay_cb},
	{"level", AYYI_PB_LEVEL, inspector__pb_delay_cb},
	{"pan",   AYYI_PB_PAN,   inspector__pb_delay_cb},
};

G_DEFINE_TYPE (AyyiInspectorWin, ayyi_inspector_win, AYYI_TYPE_PANEL)


static void
ayyi_inspector_win_class_init (AyyiInspectorWinClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	panel->name           = "Inspector";
	panel->has_link       = true;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F5;
	panel->default_size.x = 120;
	panel->default_size.y = 680;
	panel->on_ready       = inspector__on_ready;

	object_class->destroy = inspector__destroy;

	g_object_class->constructor = inspector__construct;

	g_signal_connect(am_parts, "selection-change", G_CALLBACK(inspector__on_part_selection_change), NULL);
	g_signal_connect(song->channels, "change", G_CALLBACK(inspector__on_channel_change), NULL);
	g_signal_connect(song->channels, "delete", G_CALLBACK(inspector__on_channel_delete), NULL);
	am_song__connect("periodic-update", (GCallback)_inspector__periodic_update, NULL);
}


static GObject*
inspector__construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	// For the moment we assume that only one object is selected.
	// -ideally if more than one object is selected, objects that have different values
	//   would be 'greyed' or '*'d, with the ability to override them.

	AYYI_DEBUG_1 printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_inspector_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	AyyiPanel* panel = (AyyiPanel*)g_object;

	ayyi_panel_set_name(panel, AYYI_TYPE_INSPECTOR_WIN);

	panel->meter_type = METER_GTK;//METER_JAMIN;

	return g_object;
}


static void
ayyi_inspector_win_init (AyyiInspectorWin* object)
{
}


static void
inspector__destroy (GtkObject* object)
{
	PF;

	AyyiPanel* panel = (AyyiPanel*)object;
	InspectorWin* inspector = (InspectorWin*)panel;

	gulong* handler = &inspector->obj_label_changed;
	if (*handler) g_signal_handler_disconnect((gpointer)inspector->obj_label, *handler);
	inspector->obj_label_changed = 0;

	observable_unsubscribe(am_tracks->selection2, NULL, inspector);

	g_clear_pointer(&inspector->pb, g_free);
	g_clear_pointer(&inspector->strip, strip__destroy);

	((GtkObjectClass*)ayyi_inspector_win_parent_class)->destroy(object);
}


static void
inspector__on_ready (AyyiPanel* panel)
{
	/*

	  inspector (InspectorWin)
	  +-dock_item
	   +-vbox
	    +--alignbox
	    |  +--scroll window
	    |     +--toolbar hbox
	    |
	    +--event box
	    |  +--object label
	    |
	    +--playback params(table)
	    |  +--
	    |
	    +--mixer strip (vbox)

	*/
	InspectorWin* inspector = (InspectorWin*)panel;

	if (inspector->obj_label_evbox) return;

	// toolbar
	GtkObject* hadj_ctl = gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0);
	GtkObject* vadj_ctl = gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0);
	GtkWidget* viewport = gtk_viewport_new(GTK_ADJUSTMENT(hadj_ctl), GTK_ADJUSTMENT(vadj_ctl));
	gtk_widget_set_size_request(viewport, 10, -1);
	panel_pack(viewport, EXPAND_FALSE);

	// eventbox for track label
	GtkWidget* obj_label_evbox = inspector->obj_label_evbox = gtk_event_box_new();
	panel_pack(obj_label_evbox, FALSE);

	// track/part name
	GtkWidget* obj_label = inspector->obj_label = gtk_label_new ("obj undefined"); // waiting on real data
	gtk_container_add(GTK_CONTAINER(obj_label_evbox), obj_label);
	inspector->obj_label_changed = 0; // too early to set the object rename handler

	//-----------------------

	// playback params

	GtkSizeGroup* size_group = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);

	PlaybackParam* pb = inspector->pb = g_new0(PlaybackParam, 3);

	for (int r=0;r<3;r++) {
		PlaybackParam* p = &pb[r];
		*p = (PlaybackParam){
			.name = pbconfig[r].name,
			.prop = pbconfig[r].prop,
			.row  = gtk_hbox_new(FALSE, 0),
			.wlabel = gtk_label_new(pbconfig[r].name),
			.entry = gtk_entry_new(),
		};
		g_strlcpy(p->val, "0.0", 32);

		// label
		gtk_size_group_add_widget(size_group, p->wlabel);
		gtk_misc_set_alignment(GTK_MISC(p->wlabel), 0.0, 0.5);
		gtk_box_pack_start(GTK_BOX(p->row), p->wlabel, FALSE, FALSE, 0);

		// entry
		gtk_entry_set_text(GTK_ENTRY(p->entry), "0.0");
		gtk_entry_set_width_chars(GTK_ENTRY(p->entry), 6);
		g_signal_connect((gpointer)p->entry, "focus-out-event", G_CALLBACK(pbconfig[r].callback), inspector);
		gtk_box_pack_start(GTK_BOX(p->row), p->entry, FALSE, FALSE, 0);

		// spacer
		GtkWidget* spacer = gtk_alignment_new(0, 0, 1.0, 1.0);
		gtk_widget_set_size_request(spacer, 2, -1);
		gtk_box_pack_start(GTK_BOX(p->row), spacer, FALSE, FALSE, 0);

		// slider
		GtkObject* adj   = gtk_adjustment_new(0.0, -100.0, 101.0, 0.1, 1.0, 1.0);
		GtkWidget* scale = gtk_hscale_new(GTK_ADJUSTMENT(adj));
		gtk_scale_set_draw_value(GTK_SCALE(scale), FALSE);
		gtk_box_pack_start(GTK_BOX(p->row), scale, TRUE, TRUE, 0);

		panel_pack(p->row, EXPAND_FALSE);
	}
	g_object_unref(size_group);

	// spacer
	GtkWidget* spacer = gtk_alignment_new(0, 0, 1.0, 1.0);
	gtk_widget_set_size_request(spacer, -1, 8);
	panel_pack(spacer, EXPAND_FALSE);

	// channel strip
	Strip* strip = inspector->strip = strip__new(NULL);
	AyyiMixerStrip* s = strip;

	s->cha_name  = g_string_new("channel undefined");
	s->strip_num = 0;
	s->panel     = panel;

	GtkWidget* strip_w = strip__add_widgets(panel, s, &(MixerOptions){.sendcount = 2,});
	if (strip_w)
		panel_pack(strip_w, EXPAND_TRUE);
	else
		pwarn ("mixer not available.");

	gtk_drag_source_set(panel->vbox, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);
#if TMP_DISABLED_FOR_TESTING
	g_signal_connect(G_OBJECT(panel->vbox), "drag-data-get", G_CALLBACK(inspector__drag_dataget), panel);
#endif
	g_signal_connect(G_OBJECT(panel->dnd_ebox), "drag-data-received", G_CALLBACK(inspector__drag_received), NULL);
	//g_signal_connect(G_OBJECT(panel->dock), "drag-data-received", G_CALLBACK(inspector__drag_received), NULL);

	g_signal_connect(G_OBJECT(panel->vbox), "size-allocate", G_CALLBACK(inspector__on_vbox_allocate), inspector);
	g_signal_connect(G_OBJECT(panel->vbox), "realize",       G_CALLBACK(inspector__on_realise), NULL);

	observable_subscribe_with_state(am_tracks->selection2, inspector__on_track_selection_change, inspector);
}


static void
inspector__receive_selection_notify (AyyiPanel* panel)
{
	static guint update_id = 0;
	if (update_id) return;

	if (!panel->link) return;
	InspectorWin* inspector = (InspectorWin*)panel;

	if (!am_tracks->selection) return;

	inspector->trk = am_collection_selection_first(am_tracks);

	bool _inspector_update (gpointer data)
	{
		int type = SELECTION_TYPE_TRK; //FIXME

		void inspector_disable (InspectorWin* insp)
		{
			gtk_widget_set_sensitive(insp->obj_label, false);
			gtk_widget_set_sensitive(insp->pb[0].row, false);
			gtk_widget_set_sensitive(insp->pb[1].row, false);
			gtk_widget_set_sensitive(insp->pb[2].row, false);
		}

		void inspector_enable (InspectorWin* insp)
		{
			gtk_widget_set_sensitive(insp->obj_label, true);
			gtk_widget_set_sensitive(insp->pb[0].row, true);
			gtk_widget_set_sensitive(insp->pb[1].row, true);
			gtk_widget_set_sensitive(insp->pb[2].row, true);
		}

		inspector_foreach {
			GtkWidget* wlabel = inspector->obj_label;

			// change the name (the inspector label)
			switch (type) {
				case SELECTION_TYPE_TRK:
					g_return_val_if_fail(am_tracks->selection, G_SOURCE_REMOVE);
					AMTrack* trk = am_collection_selection_first(am_tracks);
					if (trk->type && (trk->type == TRK_TYPE_BUS)) { pwarn("bus selected!"); inspector_disable(inspector); continue; }
					g_return_val_if_fail(trk->type && (trk->type < TRK_TYPE_BUS), G_SOURCE_REMOVE);
					dbg (2, "t=%i %s", am_track_list_position(song->tracks, trk), trk->name);
					inspector_enable(inspector);

					gulong* handler = &inspector->obj_label_changed;
					if (*handler) g_signal_handler_disconnect((gpointer)inspector->obj_label, *handler);
					*handler = g_signal_connect(G_OBJECT(inspector->obj_label), "hierarchy-changed", G_CALLBACK(inspector__on_track_label_change), inspector);

					char tname[256] = "";
					if (wlabel && am_track__get_name(trk, tname)) {
						gtk_label_set_text(GTK_LABEL(wlabel), tname);
					}

					const AMChannel* ch = am_track__lookup_channel(trk);
					if (!ch || ch->type != AM_CHAN_INPUT) continue;
					AyyiMixerStrip* s = inspector->strip;
					strip__set_channel(s, ch);

					strip__update(s);

#ifdef ENGINE_HAS_CHANNELS
					// which channel? for now lets make channel=track.
					char ch_lbl[256];
					am_track__get_channel_label(trk, ch_lbl);

					strip__set_label(s, ch_lbl);
#else
					strip__set_label(s, tname);
#endif
					break;

				case TYPE_PART:
					// we are currently not handling renames of parts
					handler = &inspector->obj_label_changed;
					if (*handler) g_signal_handler_disconnect((gpointer)inspector->obj_label, *handler);

					if (g_list_length(am_parts_selection) == 1) {
						/*
						int part_num = GPOINTER_TO_INT(g_list_nth_data(arrange->part_selection_nums, 0));
						//dbg(0, "PART: part_num=%i\n", part_num);
						struct _part *gpart = g_list_nth_data(partlist, part_num);
						//FIXME use label from core instead:
						//gtk_label_set_text(GTK_LABEL(child), gpart->label);
						*/

						//list_win_update();
					}
					else dbg (0, "FIXME: cannot cope with this number of parts!");
					break;
				default:
					pwarn ("invalid type!\n");
					break;
			}
		} end_inspector_foreach;

		update_id = 0; //show update queue has been cleared.
		return G_SOURCE_REMOVE;
	}

	update_id = g_idle_add((GSourceFunc)_inspector_update, NULL);
}


void
inspector_track_label_update (InspectorWin* inspector)
{
	// re-read and show the track label.

	if (inspector->trk) {
		char tname[256];
		am_track__get_name(inspector->trk, tname);
		if(inspector->obj_label) gtk_label_set_text(GTK_LABEL(inspector->obj_label), tname);
	}
}


static void
_inspector__periodic_update (GObject* _song, gpointer user_data)
{
	inspector_foreach {
		inspector__periodic_update((AyyiPanel*)inspector);
	} end_inspector_foreach
}


static void
inspector__periodic_update (AyyiPanel* panel)
{
	//Strip* strip = ((InspectorWin*)panel)->strip;
}


static void
inspector__on_vbox_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer user_data)
{
	InspectorWin* insp = AYYI_INSPECTOR_WIN(user_data);

	dbg(3, "width=%i", widget->allocation.width);
	if (!GTK_WIDGET_REALIZED(widget)) return;

	static bool first_time = true;
	if (first_time) {
		dbg(3, "removing size request...");
		gtk_widget_set_size_request(widget, 20, 20);
		first_time = false;
	}

	if (insp->prev_size.x != allocation->width) {
		request_toolbar_update();
	}

	insp->prev_size.x = allocation->width;
	insp->prev_size.y = allocation->height;
}


static void
inspector__on_realise (GtkWidget* widget, gpointer user_data)
{
}


/*
 *  Set the delay playback parameter following editing of a textentry widget.
 *
 *  @param widget is used to determine which parameter we are dealing with.
 *  @param user_data is not used.
 */
static int
inspector__pb_delay_cb (GtkWidget* widget, gpointer user_data)
{
	int val = 0; //FIXME
	val = val;

#if 0 // TODO
	InspectorWin* win = (InspectorWin*)windows__get_panel_from_widget(widget);

	int i = widget == win->pb[0].entry
		? 0
		: widget == win->pb[1].entry
			? 1
			: widget == win->pb[2].entry
				? 2
				: -1;

	int prop = 0;
	if(i > -1){
		prop = win->pb[i].coreprop;
	}else{ perr ("pb entry widget not found. widget=%p.", widget); return FALSE; }
#endif

	pwarn ("FIXME");

	return FALSE;
}


static void
inspector__on_part_selection_change (GObject* am_song, AyyiPanel* sender, gpointer user_data)
{
	inspector_foreach {
		if ((AyyiPanel*)inspector != sender) {
			inspector__receive_selection_notify((AyyiPanel*)inspector);
		}
	} end_inspector_foreach;
}


static void
inspector__on_track_selection_change (Observable* o, AMVal val, gpointer inspector)
{
	inspector__receive_selection_notify((AyyiPanel*)inspector);
}


static void
inspector__on_channel_change (GObject* _song, AMChannel* channel, AyyiPanel* sender, gpointer user_data)
{
	PF;
	inspector_foreach {
		Strip* strip = inspector->strip;
		if (strip->channel == channel) {
			strip__auxs_update(strip);
		}
	} end_inspector_foreach
}


static void
inspector__on_channel_delete (GObject* _song, AMChannel* channel, gpointer user_data)
{
	PF;
	inspector_foreach {
		AyyiMixerStrip* s = inspector->strip;
		if (s->channel == channel) strip__set_channel(s, NULL);
	} end_inspector_foreach
}


static void
inspector__on_track_label_change (GtkWidget* widget, GtkWidget* widget2, gpointer _inspector)
{
	inspector_track_label_update(_inspector);

#if 0
#ifndef DEBUG_TRACKCONTROL
	arrange_foreach {
		ArrTrk* atr = track_list__track_by_index(arrange, tnum);
		if(atr){
			TrackControl* tc = TRACK_CONTROL(atr->trk_ctl);
			if(tc) track_control_set_tname(tc);
		}
	} end_arrange_foreach
#endif
#endif
}


#ifdef TMP_DISABLED_FOR_TESTING
static int
inspector__drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, AyyiPanel* panel)
{
	// For _outgoing_ drags

	PF;

	verify_inspector();

	char text[256], win_id[STRLEN_WIN_ID];

	ayyi_panel_get_id_string(panel, win_id);
	gboolean data_sent = FALSE;

	snprintf(text, 255, "dock:%s%c%c", win_id, 13, 10);

	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (unsigned char*)text, strlen(text));
	data_sent = TRUE;
	PF_DONE;
	return 0; //correct?
}
#endif


static gint
inspector__drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	if (!data || data->length < 0) { perr ("no data!"); return -1; }

	dbg (0, "%s", data->data);

	AyyiPanel* panel = windows__get_panel_from_widget(widget);

	switch (info) {
		case GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING):
			dbg (0, "GDK_SELECTION_TYPE_STRING");
			break;
		case AYYI_TARGET_URI_LIST:
			dbg (3, "TARGET_URI_LIST len=%i", data->length);

			for (GList* l = uri_list_to_glist((char*)data->data); l; l = l->next) {
				char* u = l->data;
				dbg (2, "  %s", u);
				gchar* method_string;
				vfs_get_method_string(u, &method_string);
				dbg (2, "method=%s", method_string);

				if (!strcmp(method_string, "dock")) {
					ayyi_panel_on_drop_dock_item(panel, u);
				}
			}
			break;
		default:
			break;
	}

	return FALSE;
}
