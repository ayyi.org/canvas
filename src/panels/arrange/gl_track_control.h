/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __gl_track_control_h__
#define __gl_track_control_h__

#include "view_object.h"

typedef struct {
    AGlActor    actor;
    GHashTable* tracks;      // type GlTrack*
    uint32_t    default_bg;
    uint32_t    selected_bg;
} TrackControlActor;

typedef struct {
    VOActor      actor;
    ViewObject   flex_padding[2];
    ArrTrk*      track;
    bool         cache_is_empty;
    AMObject     height;
    AMObject     drag;
    float        hover_opacity;
    GLuint       vbo_id;
    PangoLayout* layout;
    PangoLayout* output_layout;
    WfAnimatable animatable;
} TrackControlTrackActor;


AGlActor* track_control_actor_new (GtkWidget*);

#endif
