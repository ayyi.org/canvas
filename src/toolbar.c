/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include "svgbutton.h"
#include "svgtogglebutton.h"
#include "windows.h"
#include "window.statusbar.h"
#include "support.h"
#include "toolbar.h"
#include "icon.h"
#include "src/transport.h"
#include "panels/arrange.h"

#define MIN_TOOLBAR_HEIGHT 8

static bool toolbar_on_button_event(GtkWidget*, GdkEventButton*, AyyiPanel*);
static bool toolbar_mouseover      (GtkWidget*, GdkEventCrossing*, gpointer);
static void follow_toggle          (GtkWidget*, gpointer);
static void link_toggle            (GtkWidget*, gpointer);
static bool toolbar_update         ();
static bool toolbar_is_linked      (AyyiPanel*);
static bool toolbar_is_follow      (AyyiPanel*);

static GArray* toolbar_items = NULL;


void
toolbar_init ()
{
	void toolbar_on_zoom_out (GtkWidget* widget, gpointer user_data)
	{
		AyyiPanel* panel = windows__get_panel_from_widget(widget);

		Ptf zoom = {panel->zoom->value.pt.x / 1.25, panel->zoom->value.pt.y / 1.25};
		observable_point_set(panel->zoom, &zoom.x, &zoom.y);
	}

	void toolbar_on_zoom_in (GtkWidget* widget, gpointer user_data)
	{
		AyyiPanel* panel = windows__get_panel_from_widget(widget);

		Ptf zoom = {panel->zoom->value.pt.x * 1.25, panel->zoom->value.pt.y * 1.25};
		observable_point_set(panel->zoom, &zoom.x, &zoom.y);
	}

	toolbar_items = g_array_sized_new(false, true, sizeof(ToolbarButton), 4);

	g_array_append_vals(
		toolbar_items,
		&(ToolbarButton[]){{
			.key      = "follow",
			.icon     = "follow",
			.callback = G_CALLBACK(follow_toggle),
			.active   = toolbar_is_follow,
		},
		{
			.key      = "link",
			.icon     = "link",
			.callback = G_CALLBACK(link_toggle),
			.active   = toolbar_is_linked
		},
		{
			.key      = "zoom_in",
			.icon     = "zoom_in",
			.callback = G_CALLBACK(toolbar_on_zoom_in),
		},
		{
			.key      = "zoom_out",
			.icon     = "zoom_out",
			.callback = G_CALLBACK(toolbar_on_zoom_out),
		}},
		4
	);
}


void
toolbar_class_free ()
{
	g_array_free(toolbar_items, true);
}


Toolbar*
toolbar_new (AyyiPanel* panel, GtkWidget* container)
{
	/*

	+--eventbox
	   +--hbox
	      +--icon1
	      +--icon2 etc...

	*/
	g_return_val_if_fail(panel, NULL);
	AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel));

	Toolbar* toolbar = AYYI_NEW(Toolbar,
		.closures = g_ptr_array_new_full(8, g_free)
	);

	// this event box prevents the dock from getting mouse clicks
	GtkWidget* ebox = gtk_event_box_new();
	if(container) gtk_box_pack_start(GTK_BOX(container), ebox, FALSE, FALSE, 0);
	g_signal_connect((gpointer)ebox, "button-press-event", G_CALLBACK(toolbar_on_button_event), panel);
	g_signal_connect((gpointer)ebox, "button-release-event", G_CALLBACK(toolbar_on_button_event), panel);

	GtkWidget* hbox_toolbar = panel->toolbar_box[0] = gtk_hbox_new (FALSE, 0);

	GtkWidget** b_toolbar = panel->b_toolbar;
	GtkWidget** transport = toolbar->b_transport;

	gtk_container_add(GTK_CONTAINER(ebox), hbox_toolbar);

	if(AYYI_IS_ARRANGE(panel)){
		// 2nd toolbar row, in case the window is too small
		GtkWidget* hbox_toolbar2 = panel->toolbar_box[1] = gtk_hbox_new(FALSE, 0);
		gtk_box_pack_start(GTK_BOX(container), hbox_toolbar2, FALSE, FALSE, 0);
	}

	// create basic toolbar buttons
	for(int i = 0; i < toolbar_items->len; i++){
		if(i == 0 && !panel_class->has_follow) continue;
		if(i == 1 && !panel_class->has_link  ) continue;

		ToolbarButton* butdata = &g_array_index(toolbar_items, ToolbarButton, i);

		GtkWidget* button = NULL;
		if(i == 0 || i == 1){
			button = b_toolbar[i] = (GtkWidget*)svg_toggle_button_new(butdata->icon);
		}else{
			button = b_toolbar[i] = svgbutton_new();
			svgbutton_add_state((SvgButton*)button, "", butdata->icon);
		}
		dbg (3, "%i: newbut=%p toolbar[0]=%p", i, button, b_toolbar[0]);

		gtk_box_pack_start(GTK_BOX(hbox_toolbar), button, EXPAND_FALSE, FILL_FALSE, 0);

		g_return_val_if_fail(butdata->callback, NULL);
		g_signal_connect((gpointer)button, "clicked", butdata->callback, NULL);

		ButtonContext* c = AYYI_NEW(ButtonContext, .button = butdata, .panel = panel);
		g_ptr_array_add(toolbar->closures, c);

		g_signal_connect(G_OBJECT(button), "enter-notify-event", G_CALLBACK(toolbar_mouseover), c);
		g_signal_connect(G_OBJECT(button), "leave-notify-event", G_CALLBACK(mouseover_leave), panel);
	}

	// add transport buttons
	typedef struct {
		AyyiPanel*       panel;
		TransportButton* button;
	} C;

	for(int i=0;i<tr_array->len;i++){
		TransportButton* transport_button = &g_array_index(tr_array, TransportButton, i);

		GtkWidget* b = transport[i] = (GtkWidget*)svg_toggle_button_new(transport_button->stock_id);
		gtk_box_pack_start(GTK_BOX(hbox_toolbar), b, EXPAND_FALSE, FILL_FALSE, 0);
		if(transport_button->callback) g_signal_connect(G_OBJECT(b), "clicked", transport_button->callback, NULL);
		C* c = toolbar->tr_button_closure[i] = AYYI_NEW(C,
			.panel = panel,
			.button = transport_button,
		);
		if(transport_button->mouseover){
			g_signal_connect(G_OBJECT(b), "enter-notify-event", transport_button->mouseover, c);
			g_signal_connect(G_OBJECT(b), "leave-notify-event", G_CALLBACK(mouseover_leave), panel);
		}
	}

	return toolbar;
}


void
toolbar_free (Toolbar** toolbar)
{
	g_ptr_array_free((*toolbar)->closures, true);
	(*toolbar)->closures = NULL;

	for(int i=0;i<tr_array->len;i++){
		if((*toolbar)->tr_button_closure[i]) g_free0((*toolbar)->tr_button_closure[i]); //TODO make sure the signal handler doesnt try to access this.
	}
	g_clear_pointer(toolbar, g_free);
}


static guint toolbar_idle = 0;

void
request_toolbar_update ()
{
	if(toolbar_idle) return;

	toolbar_idle = g_idle_add((GSourceFunc)toolbar_update, NULL);
}


bool
toolbar_update ()
{
	// redraws toolbar icons to reflect changes in status.

	// in future, this should only be called at setup, not for running updates.

	if(!SONG_LOADED) return FALSE;
	ASSERT_SONG_SHM_RET_FALSE;

	static TransportButton* transport_button;

	panel_foreach {
		Toolbar* toolbar = panel->toolbar;
		if((panel->b_toolbar[0]) && (panel->b_toolbar[1])){

			// standard toolbar buttons
			for(int b = 0; b < toolbar_items->len; b++){
				GtkWidget* button = panel->b_toolbar[b];
				if(button){
					ToolbarButton* bdata = &g_array_index(toolbar_items, ToolbarButton, b);
					if((bdata->active)){
						svg_toggle_button_set_active((SvgToggleButton*)button, (*(bdata->active))(panel));
					}
				}
			}
		}

		// transport buttons

		bool show_transport = (panel->vbox->allocation.width > 240);

		AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel));
		if(panel_class->has_toolbar){
			for(int i=0;i<tr_array->len;i++){
				transport_button = &g_array_index(tr_array, TransportButton, i);
				g_return_val_if_fail(toolbar->b_transport, false);
				GtkWidget* button = toolbar->b_transport[i];

				if(show_transport){
					if(transport_button->active) svg_toggle_button_set_active((SvgToggleButton*)button, (*(transport_button->active))());
					gtk_widget_show(button);
				} else {
					gtk_widget_hide(button);
				}
			}
		}

		// window specific toolbar items
		if(AYYI_IS_ARRANGE(panel)) arr_toolbar_update((Arrange*)panel);
	} end_panel_foreach

	toolbar_idle = 0;
	return G_SOURCE_REMOVE;
}


void
toolbar_enable (Toolbar* toolbar)
{
	PF;
}


void
toolbar_disable (Toolbar* toolbar)
{
	PF;
}


gboolean
toolbar_on_resize (GtkWidget* widget, GdkEvent* event, gpointer user_data)
{
	// handles mouse events on the toolbar resize eventbox.

	// copied from trackresize(), so some var names may not make sense.

	// TODO is this a feature we want to keep? if so apply it to all panel types.

	static double initial_tb_height, initialrooty, new_height, old_dy_z;
	double dy_z;
	gdouble rootx, rooty;
	static int dragging;
	GdkCursor *curs;

	gdk_event_get_root_coords (event, &rootx, &rooty);

	Arrange* arrange = (Arrange*)windows__get_panel_from_widget(widget);

	switch (event->type) {
		case GDK_BUTTON_PRESS:
			switch(event->button.button) {
				case 1:
					if (event->button.state & GDK_SHIFT_MASK) {
						//gtk_object_destroy(GTK_OBJECT(item));
					} else {
						initialrooty = rooty;

						initial_tb_height = toolbar_get_iconsize(arrange) -4;

						dragging = TRUE;
					}
					break;

				default:
					break;
			}
			break;

		case GDK_MOTION_NOTIFY:
			if (dragging && (event->motion.state & GDK_BUTTON1_MASK)) {
				dy_z = rooty - initialrooty;
				if (dy_z==old_dy_z) break; //nothing to do.

				new_height  = initial_tb_height + dy_z;
				if(new_height < MIN_TOOLBAR_HEIGHT) new_height = MIN_TOOLBAR_HEIGHT;

				// resize the icons
				for(int i = 0; i < toolbar_items->len; i++){
					if(i == 0 || i == 1){
						svg_toggle_button_set_size((SvgToggleButton*)((AyyiPanel*)arrange)->b_toolbar[i], new_height);
					}else{
						svgbutton_set_size((SvgButton*)((AyyiPanel*)arrange)->b_toolbar[i], new_height);
					}
				}

				old_dy_z = dy_z;
			}
			break;

		case GDK_BUTTON_RELEASE:
			curs = gdk_cursor_new(GDK_LEFT_PTR);
			gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
			gdk_cursor_unref(curs);
			dragging = FALSE;
			break;

		case GDK_ENTER_NOTIFY:
			curs = gdk_cursor_new(GDK_DOUBLE_ARROW);
			gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
			gdk_cursor_unref(curs);
			break;

		case GDK_LEAVE_NOTIFY:
			if(dragging == FALSE){
				curs = gdk_cursor_new(GDK_LEFT_PTR);
				gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
				gdk_cursor_unref(curs);
			}
			break;

		default:
			break;
	}
	return HANDLED;
}


void
toolbar_add_separator (AyyiPanel* window, int size)
{
	AyyiPanel* panel = AYYI_PANEL(window);

	GtkWidget* w = panel->toolbar_box[0];
	GtkWidget* space = gtk_fixed_new(); //the choice of widget is arbitrary.
	gtk_widget_set_size_request(space, size, 1);
	gtk_widget_show(space);
	gtk_box_pack_start (GTK_BOX(w), space, FALSE, FALSE, 0);
}


void
toolbar_show_toggle (AyyiPanel* panel)
{
	GtkWidget* toolbar = panel->toolbar_box[0];
	if (toolbar) {
		if (GTK_WIDGET_VISIBLE(toolbar)) {
			for (int i=0;i<2;i++) if (panel->toolbar_box[i]) gtk_widget_hide(panel->toolbar_box[i]);
		} else {
			gtk_widget_show(toolbar);
			if (AYYI_IS_ARRANGE(panel)) gtk_widget_show(panel->toolbar_box[1]);
		}
	}
}


GtkWidget*
toolbar_add_new_button (AyyiPanel* panel, ToolbarButton* button)
{
	g_return_val_if_fail(button->callback, NULL);

	AyyiPanel* panel2 = AYYI_PANEL(panel);

	GtkWidget* widget = svgbutton_new();

	const gchar* icon = gtk_action_get_icon_name(button->action);
	if(!icon){
		icon = gtk_action_get_stock_id(button->action);
	}

	svgbutton_add_state((SvgButton*)widget, "", icon);

	gtk_box_pack_start(GTK_BOX(panel2->toolbar_box[0]), widget, EXPAND_FALSE, FILL_FALSE, 0);

	ButtonContext* c = AYYI_NEW(ButtonContext,
		.button = button,
		.panel = panel
	);
	g_ptr_array_add(panel->toolbar->closures, c);

	void on_button_press (GObject* widget, gpointer _action)
	{
		gtk_action_activate(_action);
	}
	g_signal_connect((gpointer)widget, "clicked", (GCallback)on_button_press, button->action);

	g_signal_connect((gpointer)button->action, "activate", button->callback, c);

	return widget;
}


/*
 *  return TRUE if the 'link' property is set for the given window.
 */
static bool
toolbar_is_linked (AyyiPanel* panel)
{
	g_return_val_if_fail(panel, false);
	return panel->link;
}


/*
 *  return TRUE if the 'follow' property is set for the given window.
 */
static bool
toolbar_is_follow (AyyiPanel* panel)
{
	g_return_val_if_fail(panel, false);
	return panel->follow;
}


static void
link_toggle (GtkWidget* widget, gpointer user_data)
{
	// toggles the Link property of the window.

	PF;

	AyyiPanel* panel = windows__get_panel_from_widget(widget);
	bool* link = NULL;

	if(AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel))->has_link)
		link = &panel->link;

	if(link) *link = !*link; else pwarn ("link not set!");

	AyyiPanelClass* k = AYYI_PANEL_GET_CLASS(panel);
	if(k->on_link_change) k->on_link_change(panel);

	toolbar_update();
}


static void
follow_toggle (GtkWidget* widget, gpointer user_data)
{
	// toggles the Follow property of the calling window.

	AyyiPanel* panel = windows__get_panel_from_widget(widget);
	if(AYYI_IS_ARRANGE(panel)){
		panel->follow = !panel->follow;
	}
	toolbar_update();
}


int
toolbar_get_iconsize (Arrange* arrange)
{
	// currently just for Arrange window.

	int size = ((AyyiPanel*)arrange)->toolbar_box[0]->allocation.height;

	return size;
}


static bool
toolbar_mouseover (GtkWidget* widget, GdkEventCrossing* event, gpointer _context)
{
	ButtonContext* context = (ButtonContext*)_context;
	ToolbarButton* button = context->button;

	// set widget style
	// (this is wrong, but we still do it for now for some buttons)
	if(button->callback != G_CALLBACK(link_toggle) && button->callback != G_CALLBACK(follow_toggle)){
		GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));
		GtkStateType state = widget->state;

		if(button->active && button->active(context->panel)){
			style->bg[GTK_STATE_PRELIGHT] = style->bg[state];
			colour_lighter_gdk(&style->bg[GTK_STATE_PRELIGHT], 5);

			//warning: this causes size allocation.
			gtk_widget_set_style(widget, style);
		}
		g_object_unref(style);
	}

	shell__statusbar_print((AyyiWindow*)gtk_widget_get_toplevel(widget), 2, button->key);

	return NOT_HANDLED;
}


/*
 *  Handle mouse clicks on the toolbar background
 */
static bool
toolbar_on_button_event (GtkWidget* widget, GdkEventButton* event, AyyiPanel* panel)
{
	if(!windows__verify_pointer(panel, 0)) return NOT_HANDLED;

	GtkWidget* hbox = panel->toolbar_box[0];
	int x = 0, w = 0;
	GList* children = gtk_container_get_children(GTK_CONTAINER(hbox));
	if(children){
		for(GList* l=children;l;l=l->next){
			GtkWidget* child = l->data;
			if(gtk_widget_get_visible(child)) break;
			x = MAX(x, child->allocation.x);
			w = (GTK_WIDGET(l->data))->allocation.width;
		}
		g_list_free(children);
	}

	if(event->x > x + w){
		// we are after the last toolbar button, so we propagate the button-press signal for dnd ops.
		return NOT_HANDLED;
	}

	return HANDLED;
}


gboolean
mouseover_enter (GtkWidget* widget, GdkEventCrossing* event, AyyiPanel* user_data)
{
	char* text = (char*)user_data;

	arrange_foreach {
		arr_statusbar_printf(arrange, 3, text);
	} end_arrange_foreach

	return FALSE;
}


int
mouseover_leave (GtkWidget* widget, GdkEventCrossing* event, AyyiPanel* panel)
{
	shell__statusbar_print(panel->window, 2, " ");
	return FALSE;
}


