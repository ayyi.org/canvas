extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
	println!(r"cargo:rustc-link-lib=dbus-glib-1");
	println!(r"cargo:rustc-link-lib=sndfile");
	println!(r"cargo:rustc-link-lib=ass");
	println!(r"cargo:rustc-link-lib=avformat");
	println!(r"cargo:rustc-link-lib=avcodec");
	println!(r"cargo:rustc-link-lib=avutil");
	println!(r"cargo:rustc-link-lib=gtk-x11-2.0");
	println!(r"cargo:rustc-link-lib=gtkglext-x11-1.0");
	println!(r"cargo:rustc-link-lib=GL");
	println!(r"cargo:rustc-link-lib=Xmu");
	println!(r"cargo:rustc-link-lib=Xt");
	println!(r"cargo:rustc-link-lib=SM");
	println!(r"cargo:rustc-link-lib=ICE");
	println!(r"cargo:rustc-link-lib=gdk-x11-2.0");
	println!(r"cargo:rustc-link-lib=pangox-1.0");
	println!(r"cargo:rustc-link-lib=X11");
	println!(r"cargo:rustc-link-lib=gmodule-2.0");
	println!(r"cargo:rustc-link-lib=pangocairo-1.0");
	println!(r"cargo:rustc-link-lib=cairo");
	println!(r"cargo:rustc-link-lib=gdk_pixbuf-2.0");

	if false {
		let bindings = bindgen::Builder::default()
			.header("wrapper.h")
			.whitelist_function("am__init")
			.clang_arg("-I/home/tim/docs/devel/seismix/src/gui")
			.clang_arg("-I/home/tim/docs/devel/seismix/src/gui/lib")
			.clang_arg("-I/home/tim/docs/devel/seismix/src/gui/lib/waveform")
			.clang_arg("-I/usr/include/glib-2.0")
			.clang_arg("-I/usr/lib64/glib-2.0/include")
			.clang_arg("-I/usr/include/gtk-2.0")
			.clang_arg("-I/usr/lib64/gtk-2.0/include")
			.clang_arg("-I/usr/include/pango-1.0")
			.clang_arg("-I/usr/include/atk-1.0")
			.clang_arg("-I/usr/include/cairo")
			.clang_arg("-I/usr/include/pixman-1")
			.clang_arg("-I/usr/include/libdrm")
			.clang_arg("-I/usr/include/gdk-pixbuf-2.0")
			.clang_arg("-I/usr/include/libpng16")
			.clang_arg("-I/usr/include/pango-1.0")
			.clang_arg("-I/usr/include/harfbuzz")
			.clang_arg("-I/usr/include/pango-1.0")
			.clang_arg("-I/usr/include/fribidi")
			.clang_arg("-I/usr/include/uuid")
			.clang_arg("-I/usr/include/freetype2")
			.rustfmt_bindings(true)
			.rustfmt_configuration_file(Some(PathBuf::from("rustfmt.toml")))
			.generate()
			.expect("Unable to generate bindings");

		let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
		bindings
			.write_to_file(out_path.join("bindings.rs"))
			.expect("Couldn't write bindings!");
	}
}
