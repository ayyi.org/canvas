/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi Project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __arrange_private__
#include "global.h"
#include "model/time.h"
#include "model/track_list.h"
#include "model/note_selection.h"
#include "song.h"
#include "windows.h"
#include "window.statusbar.h"
#include "support.h"
#include "panels/arrange.h"
#include "track_control_midi.h"
#include "gnome_canvas_utils.h"
#include "arrange/utils.h"
#include "arrange/gnome/part_item.h"
#include "arrange/gnome_canvas_op.h"

static Op ops [N_OP_TYPES] = {{0,}};

static CanvasOp* canvas_op__new            (Arrange*, Optype);

static void canvas_op__press               (CanvasOp*, GdkEvent*);
static void canvas_op__copy_press          (CanvasOp*, GdkEvent*);
static void canvas_op__repeat_press        (CanvasOp*, GdkEvent*);
static void canvas_op__box_press           (CanvasOp*, GdkEvent*);
static void canvas_op__draw_press          (CanvasOp*, GdkEvent*);
static void canvas_op__edit_press          (CanvasOp*, GdkEvent*);
static void canvas_op__move_press          (CanvasOp*, GdkEvent*);
static void canvas_op__velocity_press      (CanvasOp*, GdkEvent*);
static void canvas_op__note_left_press     (CanvasOp*, GdkEvent*);
static void canvas_op__note_right_press    (CanvasOp*, GdkEvent*);
static void canvas_op__divider_press       (CanvasOp*, GdkEvent*);

static void canvas_op__none_motion         (CanvasOp*);
static void canvas_op__move_motion         (CanvasOp*);
static void canvas_op__copy_motion         (CanvasOp*);
static void canvas_op__resize_left_motion  (CanvasOp*);
static void canvas_op__velocity_motion     (CanvasOp*);
static void canvas_op__divider_motion      (CanvasOp*);
static void canvas_op__note_left_motion    (CanvasOp*);
static void canvas_op__note_right_motion   (CanvasOp*);
static void canvas_op__repeat_motion       (CanvasOp*);

static void canvas_op__finish              (CanvasOp*);
static void canvas_op__move_finish         (CanvasOp*);
static void canvas_op__copy_finish         (CanvasOp*);
static void canvas_op__resize_left_finish  (CanvasOp*);
static void canvas_op__resize_right_finish (CanvasOp*);
static void canvas_op__repeat_finish       (CanvasOp*);
static void canvas_op__box_finish          (CanvasOp*);
static void canvas_op__draw_finish         (CanvasOp*);
static void canvas_op__edit_finish         (CanvasOp*);
static void canvas_op__note_left_finish    (CanvasOp*);
static void canvas_op__note_right_finish   (CanvasOp*);
static void canvas_op__velocity_finish     (CanvasOp*);
static void canvas_op__divider_finish      (CanvasOp*);
static void canvas_op__note_select_finish  (CanvasOp*);

static void snap_vertical                  (CanvasOp*);
static void canvas_op__start_scroll_timer  (CanvasOp*);

static target* target_new                  (Arrange*);
static void    target_destroy              (CanvasOp*);
static void    set_target                  (CanvasOp*, double, double);

static bool    drag_mouse_timeout          (CanvasOp*);

static gboolean edit_drag_ready = FALSE;


GnomeCanvasOp*
gnome_canvas_op__new(Arrange* arrange, Optype type)
{
	if(!arrange->canvas->op){
		arrange->canvas->op = (CanvasOp*)g_new0(GnomeCanvasOp, 1);

		ops[OP_NONE]         = (Op){canvas_op__press, canvas_op__none_motion, canvas_op__finish};
		ops[OP_MOVE]         = (Op){canvas_op__move_press, canvas_op__move_motion, canvas_op__move_finish};
		ops[OP_COPY]         = (Op){canvas_op__copy_press, canvas_op__copy_motion, canvas_op__copy_finish};
		ops[OP_RESIZE_LEFT]  = (Op){canvas_op__press, canvas_op__resize_left_motion, canvas_op__resize_left_finish};
		ops[OP_RESIZE_RIGHT] = (Op){canvas_op__press, canvas_op__resize_right_motion, canvas_op__resize_right_finish};
		ops[OP_REPEAT]       = (Op){canvas_op__repeat_press, canvas_op__repeat_motion, canvas_op__repeat_finish};
		ops[OP_BOX]          = (Op){canvas_op__box_press, canvas_op__box_motion, canvas_op__box_finish};
		ops[OP_DRAW]         = (Op){canvas_op__draw_press, canvas_op__draw_motion, canvas_op__draw_finish};
		ops[OP_EDIT]         = (Op){canvas_op__edit_press, canvas_op__edit_motion, canvas_op__edit_finish};
		ops[OP_VELOCITY]     = (Op){canvas_op__velocity_press, canvas_op__velocity_motion, canvas_op__velocity_finish};
		ops[OP_NOTE_LEFT]    = (Op){canvas_op__note_left_press, canvas_op__note_left_motion, canvas_op__note_left_finish};
		ops[OP_NOTE_RIGHT]   = (Op){canvas_op__note_right_press, canvas_op__note_right_motion, canvas_op__note_right_finish};
		ops[OP_VDIVIDER]     = (Op){canvas_op__divider_press, canvas_op__divider_motion, canvas_op__divider_finish};
		ops[OP_NOTE_SELECT]  = (Op){canvas_op__press, canvas_op__motion, canvas_op__note_select_finish};
	}

	canvas_op__new(arrange, type);
	return (GnomeCanvasOp*)arrange->canvas->op;
}


static CanvasOp*
canvas_op__new(Arrange* arrange, Optype type)
{
	CanvasOp* op = arrange->canvas->op;

	if(arrange){
		gnome_canvas_op__set_arrange(arrange->canvas->op, arrange);
	}

	op->type = type;
	dbg(2, "type=%s", canvas_op__print_type(op));
	switch(type){
		case OP_NONE:
		case OP_MOVE:
		case OP_COPY:
		case OP_RESIZE_LEFT:
		case OP_RESIZE_RIGHT:
		case OP_REPEAT:
		case OP_BOX:
		case OP_DRAW:
		case OP_EDIT:
		case OP_VELOCITY:
		case OP_NOTE_LEFT:
		case OP_NOTE_RIGHT:
		case OP_VDIVIDER:
		case OP_NOTE_SELECT:
			op->op = &ops[type];
			break;
		default:
			*op->op = (Op){canvas_op__press, canvas_op__motion, canvas_op__finish};
			break;
	}
	return op;
}


static void
canvas_op__press(CanvasOp* op, GdkEvent* event)
{
	op->start = op->mouse;
	op->diff.x = op->mouse.x - op->p1.x;
	op->diff.y = op->mouse.y - op->p1.y;
	op->old_bounded = op->mouse;
}


static void
canvas_op__move_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	canvas_op__press(op, event);
	canvas_item_grab(((GnomeCanvasOp*)op)->item, event, GDK_FLEUR);

	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;
	if(!cop->target) cop->target = target_new(op->arrange);
	cop->target->width = arr_get_selection_size(op->arrange, op->arrange->part_selection, &cop->target->n_tracks, NULL, NULL);
	cop->target->inner_width = MIN(15.0, cop->target->width / 2 - 2.0);

	arr_statusbar_printf(op->arrange, 2, "%i parts selected", 1);
}


static void
canvas_op__copy_press(CanvasOp* op, GdkEvent* event)
{
	canvas_op__press(op, event);

	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;
	if(!cop->target) cop->target = target_new(op->arrange);
	cop->target->width = arr_get_selection_size(op->arrange, op->arrange->part_selection, &cop->target->n_tracks, NULL, NULL);
	cop->target->inner_width = MIN(15.0, cop->target->width / 2 - 2.0);

	op->part_local = part_local_new_from_part(op->arrange, op->p1.x, op->p1.y, op->part, FALSE);
	gcanvas_part_local_draw(gnome_canvas_root(GNOME_CANVAS(op->arrange->canvas->widget)), op->part_local);
}


static void
canvas_op__repeat_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	g_canvas* g = op->arrange->canvas->gnome;

	if(!g->record_part && op->arrange->canvas->record_part_new) op->arrange->canvas->record_part_new(op->arrange, arr_px2trk(op->arrange, op->mouse.y));

	if(g->record_part){
		//int width = arr_get_selection_size(op->arrange, op->arrange->part_selection, NULL, NULL);
		GPos selection_end;
		am_partmanager__get_pos(op->arrange->part_selection, NULL, &selection_end);

		//start on the next beat:
		GPos repeat_start = selection_end;
		repeat_start.beat += 1;
		repeat_start.sub = 0;
		repeat_start.tick = 0;

		uint32_t rgba = 0x999999ff;
		gnome_canvas_item_set(g->record_part,
			"fill-color-rgba", rgba,
			"x1", arr_pos2px(op->arrange, &repeat_start),
			"y1", op->p1.y,
			"x2", arr_pos2px(op->arrange, &repeat_start) + 2.0,
			"y2", op->p2.y, NULL);
		gnome_canvas_item_show(g->record_part);
	}
}


static void
canvas_op__box_press(CanvasOp* op, GdkEvent* event)
{
	canvas_op__press(op, event);

	// start a selection box
	GnomeCanvasItem* box = op->arrange->canvas->gnome->drag_box;
	gnome_canvas_item_raise_to_top(box);
	gnome_canvas_item_set(box,
			"x1", op->mouse.x,
			"y1", op->mouse.y,
			"x2", op->mouse.x,
			"y2", op->mouse.y, NULL);
	gnome_canvas_item_show(box);
}


static void
canvas_op__draw_press(CanvasOp* op, GdkEvent* event)
{
	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;

	canvas_op__press(op, event);

	if(!PART_IS_MIDI(op->part)){ pwarn("not AYYI_MIDI!"); return; }
	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);

#ifndef DEBUG_TRACKCONTROL
	int note = arr_coords_get_note(op->arrange, op->start.x, op->start.y);
	track_control_midi_highlight_note(TRACK_CONTROL_MIDI(at->trk_ctl), note);
#endif

	if(cop->note){
		gnome_canvas_item_show(cop->note);
		gnome_canvas_item_set(cop->note, "x1", op->start.x, "y1", op->start.y, "x2", op->start.x+5, "y2", op->start.y + ((ArrTrackMidi*)at)->note_height, NULL);
		dbg(0, "re-using note item... x1=%.2f", op->start.x);
	}else{
		cop->note = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(op->arrange->canvas->widget)), 
				   //gnome_canvas_line_get_type(),
				   gnome_canvas_rect_get_type(),
				   "fill-color-rgba", 0xff0000ff,
				   "width-pixels", 8,
				   "width_units", 1.0,
				   "x1", op->start.x,
				   "y1", op->mouse.y,
				   "x2", op->start.x+5,
				   "y2", op->start.y + ((ArrTrackMidi*)at)->note_height,
				   NULL);
		dbg(0, "new canvas_item: x=%.2f y=%.2f", op->start.x, op->start.y);
	}
}


static void
canvas_op__edit_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	canvas_op__press(op, event);

	if(0) edit_drag_ready = TRUE;
}


static void
canvas_op__velocity_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	GnomeCanvasPart* part = GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item);

	canvas_op__press(op, event);

	// if no notes are selected, we need to select one
	if(!((MidiPart*)op->part)->note_selection){
		const MidiNote* note = gnome_canvas_part_pick_velocity_bar(part, op->diff, NULL);
		if(note){
			TransitionalNote* t = transitional_note__new((MidiNote*)note);
			gnome_canvas_part_set_edit_selection(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item), g_list_append(NULL, t));
		}
	}

	canvas_item_grab(((GnomeCanvasOp*)op)->item, event, GDK_SB_V_DOUBLE_ARROW);
}


static void
canvas_op__note_left_press(CanvasOp* op, GdkEvent* event)
{
	canvas_op__note_right_press(op, event);
}


static void
canvas_op__note_right_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	canvas_op__press(op, event);

	// if no notes are selected, we need to select one
	if(!((MidiPart*)op->part)->note_selection){
		const MidiNote* note = arr_part_pick_note(op->arrange, op->part, op->diff, NULL, NULL);
		if(note){
			TransitionalNote* t = transitional_note__new((MidiNote*)note);
			gnome_canvas_part_set_edit_selection(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item), g_list_append(NULL, (void*)t));
		}
	}

	canvas_item_grab(((GnomeCanvasOp*)op)->item, event, GDK_SB_H_DOUBLE_ARROW);
}


static void
canvas_op__divider_press(CanvasOp* op, GdkEvent* event)
{
	PF;
	canvas_op__press(op, event);

	canvas_item_grab(((GnomeCanvasOp*)op)->item, event, GDK_SB_V_DOUBLE_ARROW);
}


static void
canvas_op__none_motion(CanvasOp* op)
{
	// no operation, but we need to show mouse-over effects.
	// this is separate to canvas_op__motion because the pick doesnt need to be done during other operations.

	g_return_if_fail(((GnomeCanvasOp*)op)->item);

	canvas_op__motion(op);

	op->diff.x = op->mouse.x - op->p1.x;
	op->diff.y = op->mouse.y - op->p1.y;
	op->ready = 0;

	if(!op->part){ pwarn("part not set - how to determine rollover cursor?"); return; }

#ifndef DEBUG_TRACKCONTROL
	ArrTrk* trk = op->part ? track_list__lookup_track(op->arrange->priv->tracks, op->part->track) : NULL;
#endif

	FeatureType feature = pick_feature_type(op);
	switch(feature){
		case FEATURE_NONE:
			arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
			break;
		case FEATURE_PART_LEFT:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			op->ready = READY_RESIZE_LEFT;
			break;
		case FEATURE_PART_RIGHT:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			op->ready = READY_RESIZE_RIGHT;
			break;
		case FEATURE_NOTE:
			arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
			break;
		case FEATURE_VEL_DIVIDER:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_V_DOUBLE_ARROW);
			break;
		case FEATURE_VEL_AREA:
			arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
#ifndef DEBUG_TRACKCONTROL
			int velocity = arr_track_get_velocity_from_y((ArrTrackMidi*)trk, op->bnew.y);
			shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 3, "%i", velocity);
#endif
			break;
		case FEATURE_VEL_BAR_TOP:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_V_DOUBLE_ARROW);
			break;
		case FEATURE_NOTE_AREA:
			arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
#ifndef DEBUG_TRACKCONTROL
			int note = arr_track_get_note_num_from_y((ArrTrackMidi*)trk, op->bnew.y);
			shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 3, "%s%i", note_format(note), note_get_octave_num(note));
#endif
			break;
		case FEATURE_NOTE_LEFT:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			break;
		case FEATURE_NOTE_RIGHT:
			set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			break;
		default:
			break;
	}
}


static void
canvas_op__move_motion(CanvasOp* op)
{
	g_canvas* g = op->arrange->canvas->gnome;

	canvas_op__motion(op);

	if(!op->part_local){
		// create a 'ghost' part to show the original position
		// note that we dont show it until after movement has started.
		op->part_local = part_local_new_from_part(op->arrange, op->p1.x, op->p1.y, op->part, TRUE);
		GCanvasLocalPart* part_ = (GCanvasLocalPart*)op->part_local;
		gcanvas_part_local_draw(gnome_canvas_root(GNOME_CANVAS(op->arrange->canvas->widget)), op->part_local);
		gnome_canvas_item_lower_to_bottom(part_->canvasgroup);
		//gnome_canvas_item_lower(part_local->canvasgroup, 1);
		gnome_canvas_item_raise(part_->canvasgroup, 1); //hack!
	}

	canvas_op__handle_scrolling(op);

	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "x", op->bnew.x - op->diff.x, "y", op->bnew.y - op->diff.y, NULL);

	gcanvas_set_xhairs(op->arrange, op->p1.x, op->p1.y);
	gnome_canvas_item_show(g->xhair);
	gnome_canvas_item_show(g->yhair);

	set_target(op, op->p1.x, op->p1.y);

	op->old_bounded = op->bnew;

	// update statusbar
	char lbl[64];
	arr_px2bbs(op->arrange, op->p1.x, lbl);
	arr_statusbar_printf(op->arrange, 1, lbl);
}


static void
canvas_op__copy_motion(CanvasOp* op)
{
	canvas_op__motion(op);

	// move the new part to follow the mouse
	op->bnew.x = op->mouse.x;
	op->bnew.y = op->mouse.y;
	if (op->bnew.y < op->diff.y) op->bnew.y = op->diff.y; //FIXME use same bounds checking as DRAGGING

	if(canvas_op__is_left_edge(op) || canvas_op__is_right_edge(op)){
		canvas_op__start_scroll_timer(op);
	}

	GCanvasLocalPart* part_ = (GCanvasLocalPart*)op->part_local;
	gnome_canvas_item_move(part_->canvasgroup, op->bnew.x - op->old_bounded.x, op->bnew.y - op->old_bounded.y);//relative movement!
	op->old_bounded.x = op->bnew.x;
	op->old_bounded.y = op->bnew.y;

	set_target(op, op->p1.x - (op->start.x - op->mouse.x), op->mouse.y);

	char str[64];
	arr_px2bbs(op->arrange, op->mouse.x, str);
	shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 3, str);
}


static void
canvas_op__resize_left_motion (CanvasOp* op)
{
	canvas_op__motion(op);

	// set part start, converting to pixels to GPos format:
	arr_snap_px2spos(op->arrange, &op->part->start, op->mouse.x / hzoom(op->arrange));
	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "left", op->mouse.x > 0.0 ? op->mouse.x : 0.0, NULL); // note: the bounds checking is supposedly in the widget, but gnome also checks bounds and complains...
}


void
canvas_op__resize_right_motion (CanvasOp* op)
{
	canvas_op__motion(op);

	if(canvas_op__is_left_edge(op) || canvas_op__is_right_edge(op)){
		canvas_op__start_scroll_timer(op);
	}

	double new_part_length = MAX(2.0, op->mouse.x - op->p1.x);
	arr_px2spos(op->arrange, &op->part->length, new_part_length);
	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "right", MAX(op->mouse.x, op->p1.x + 3.0), NULL);

	// FIXME this is better than nothing, but stops the autoscrolling from working.
	//       It moves the cursor away from the part end, and scrolls to end.
	canvas_op__check_song_length(op, op->mouse.x);

	// update statusbar
	char lbl[64];
	arr_px2bbs (op->arrange, new_part_length, lbl);
	arr_statusbar_printf (op->arrange, 1, "part length: %s", lbl);
}


static void
canvas_op__repeat_motion (CanvasOp* op)
{
	g_canvas* g = op->arrange->canvas->gnome;

	double x1; g_object_get(g->record_part, "x1", &x1, NULL);
	double x2 = op->mouse.x;

	if(x2 > x1) gnome_canvas_item_set(g->record_part, "x2", x2, NULL);
}


void
canvas_op__box_motion (CanvasOp* op)
{
	canvas_op__motion(op);

	gnome_canvas_item_set(op->arrange->canvas->gnome->drag_box,
                    "x2", op->mouse.x,
                    "y2", op->mouse.y, NULL);
	canvas_op__handle_scrolling(op);
}


void
canvas_op__draw_motion (CanvasOp* op)
{
	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;

	canvas_op__motion(op);

	if(!PART_IS_MIDI(op->part)) return;

	if(cop->note){
		dbg(2, "x2=%.2f", op->bnew.x);
	    gnome_canvas_item_set(cop->note, "x2", op->bnew.x, NULL);
	}
}


void
canvas_op__edit_motion(CanvasOp* op)
{
	canvas_op__motion(op);

#if 0
	GnomeCanvasPart* part = GNOME_CANVAS_PART(op->item);

	if(op->type == OP_VELOCITY){
		dbg(0, "velocity...");
		if(!part->notes_in_transition){
			part->notes_in_transition = note_selection_list__new(op);
			//dbg(0, "notes copied. list=%p", part->notes_in_transition);
		}
		//move each selected note:
		GList* l = part->notes_in_transition;
		for(;l;l=l->next){
			TransitionalNote* note = l->data;
      		ArrTrk* trk = op->arrange->track[op->part->track];
			int chart_height = TRACK_CONTROL_MIDI(trk->trk_ctl)->velocity_chart_height;
			int bottom_px = op->py2;
			int velocity_px = MIN(bottom_px - op->bnew.y, chart_height);
			dbg(0, "velocity=%i", velocity_px);
			//note->copy.velocity = note->orig->velocity + arr_px2samples(op->arrange, op->bnew.x - op->start.x);
		}
		gnome_canvas_item_set(op->item, "gpart", op->part, NULL);

	}else 
#endif
	if(op->max_movement > 2){

		// move each selected note
		GList* l = canvas_op__get_note_selection(op, (MidiPart*)op->part);
		for(;l;l=l->next){
			TransitionalNote* t = l->data;
			MidiNote* note = &t->transient;
			int orig_start = t->orig->start;
			int px = arr_px2samples(op->arrange, op->bnew.x - op->start.x);
			note->start = MAX(0, orig_start + px);
			dbg(2, "start=%.2f-%.2f --> %i %.1f", op->bnew.x, op->start.x, px, arr_samples2px(op->arrange, px));
			arr_statusbar_print_xpos(op->arrange, 1, op->bnew.x - op->start.x);
		}

		// redraw pixbuf
		gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "gpart", op->part, NULL);
	}
}


static void
canvas_op__velocity_motion(CanvasOp* op)
{
	canvas_op__motion(op);

#ifndef DEBUG_TRACKCONTROL
	if(!op->note_selection){
		note_selection_list__new(&op->note_selection, op->part);
	}
	int bottom_px = op->p2.y;
#ifdef DEBUG
	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);
	int chart_height = ((ArrTrackMidi*)at)->velocity_chart_height;
#endif
	float new_velocity_px = bottom_px - op->bnew.y;
	float old_velocity_px = bottom_px - op->start.y;
	float factor = /*chart_height * */new_velocity_px / old_velocity_px;
	if(factor < 0) factor = 0;
	dbg(1, "bottom_px=%i newy=%.1f=%.1f chart_height=%i factor=%.2f (%.2f --> %.2f)", bottom_px, op->bnew.y, op->mouse.y, chart_height, factor, old_velocity_px, new_velocity_px);

	// change velocity of each selected note:
	GList* l = canvas_op__get_note_selection(op, (MidiPart*)op->part);
	for(;l;l=l->next){
		TransitionalNote* tn = l->data;
		MidiNote* note = &tn->transient;
		note->velocity = MIN(tn->orig->velocity * factor, 0x7f);
	}
	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "gpart", op->part, NULL);
#endif
}


static void
canvas_op__note_left_motion(CanvasOp* op)
{
	PF;

	canvas_op__motion(op);

	if(!op->note_selection){
		note_selection_list__new(&op->note_selection, op->part);
	}

	// move each selected note
	GList* l = canvas_op__get_note_selection(op, (MidiPart*)op->part);
	for(;l;l=l->next){
		TransitionalNote* tn = l->data;
		MidiNote* note = &tn->transient;
		int dx = arr_px2samples(op->arrange, op->bnew.x - op->start.x);
		int len = MAX(tn->orig->length - dx, 5);
		dbg(0, "len=%i", len);
		note->start = tn->orig->start + dx;
		note->length = len;
	}
	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "gpart", op->part, NULL);
}


static void
canvas_op__note_right_motion(CanvasOp* op)
{
	canvas_op__motion(op);

	dbg(0, "...");
	if(!op->note_selection){
		note_selection_list__new(&op->note_selection, op->part);
	}
	//move each selected note:
	GList* l = canvas_op__get_note_selection(op, (MidiPart*)op->part);
	for(;l;l=l->next){
		TransitionalNote* tn = l->data;
		MidiNote* note = &tn->transient;
		uint32_t orig_len = tn->orig->length;
		int dx = arr_px2samples(op->arrange, op->bnew.x - op->start.x);
		int len = MAX(orig_len + dx, 5);
		dbg(0, "len=%i", len);
		note->length = len;
	}
	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "gpart", op->part, NULL);
}


static void
canvas_op__divider_motion(CanvasOp* op)
{
	canvas_op__motion(op);
#ifndef DEBUG_TRACKCONTROL
	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);
#endif

	op->diff.x = op->mouse.x - op->p1.x;
	op->diff.y = op->mouse.y - op->p1.y;

#ifndef DEBUG_TRACKCONTROL
	int part_height = at->height * vzoom(op->arrange) - PART_OUTLINE_WIDTH;
	int chart_height = CLAMP(part_height - op->diff.y, 0, part_height);
	((ArrTrackMidi*)at)->velocity_chart_height = chart_height;
#endif

	gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "gpart", op->part, NULL);
}


void
canvas_op__finish(CanvasOp* op)
{
	PF;
	if(((GnomeCanvasOp*)op)->item) gnome_canvas_item_ungrab(((GnomeCanvasOp*)op)->item, 0/*event->button.time*/);

	op->op = &ops[op->type = OP_NONE];

	op->max_movement = 0;
}


static void
canvas_op__move_finish(CanvasOp* op)
{
	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;
	g_canvas* g = op->arrange->canvas->gnome;

	g_return_if_fail(op->part);

	part_local_destroy(op->part_local);
	op->part_local = NULL;

	TrackNum old_track = am_track_list_position(song->tracks, op->part->track);

	snap_vertical(op);

	// detect false moves:
	// -this appears to be ok. If we need more control maybe use a timer (events are timestamped),
	// -the check is done in the Release phase to avoid the sticky intertia feeling.
	if(op->max_movement > 2.0){
		op->p1.x = MAX(op->p1.x, 0);

		dbg (2, "  dropping... updating part data...");

		AyyiSongPos px1_pos;
		arr_snap_px2spos(op->arrange, &px1_pos, op->p1.x);

		dbg(0, "t=%i-->%i n_parts=%i", old_track, am_track_list_position(song->tracks, op->part->track), g_list_length(am_parts_selection));
		int track_offset = am_track_list_position(song->tracks, op->part->track) - old_track;

		// calculate the _change_ in position.
		AyyiSongPos pos_offset = px1_pos;
		bool forwards = ayyi_pos_is_after(&px1_pos, &op->part->start);
		if(forwards){
			ayyi_pos_sub(&pos_offset, &op->part->start);
		}else{
			AyyiSongPos p = op->part->start;
			ayyi_pos_sub(&p, &pos_offset);
			pos_offset = p;
		}

		// FIXME we actually want to use the Arrange selection, not the Song selection.
		am_song__part_selection_move(&pos_offset, forwards, track_offset); // track_offset is currently ignored

		AyyiWindow* window = ((AyyiPanel*)op->arrange)->window;
		if(window->statusbar->drag_id.cid)
			gtk_statusbar_remove(GTK_STATUSBAR(window->statusbar->widget[0]), window->statusbar->drag_id.cid, window->statusbar->drag_id.mid);
	}

	gnome_canvas_item_hide(g->xhair);
	gnome_canvas_item_hide(g->yhair);
	gnome_canvas_item_hide(GNOME_CANVAS_ITEM(cop->target->group));

	canvas_op__finish(op);
}


static void
canvas_op__copy_finish(CanvasOp* op)
{
	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;

	//set the part start according to mouse position:
	arr_snap_px2spos(op->arrange, &op->part_local->part.start, op->mouse.x - op->diff.x);

	char bbst[32]; ayyi_pos2bbst(&op->part_local->part.start, bbst);
	dbg (0, "diffx=%.2f pos=%s inset=%u", op->diff.x, bbst, op->part_local->part.region_start);

	if(op->max_movement > 2.0){
		//request a new part be added:

		AMTrack* old_track = op->part->track;
		ArrTrk* new_track = canvas_op__get_track(op, op->p1.y);
		dbg(0, "old track: d=%i", track_list__get_display_num(op->arrange->priv->tracks, old_track));
		dbg(0, "new track: d=%i", track_list__get_display_num(op->arrange->priv->tracks, new_track->track));
		int track_offset = am_track_list_position(song->tracks, new_track->track) - am_track_list_position(song->tracks, old_track);

		if(g_list_length(op->arrange->part_selection) > 1) pwarn ("FIXME - unique name may fail when copying multiple parts.");
		song_add_parts_from_selection((AyyiPanel*)op->arrange, &op->part_local->part.start, track_offset);
	}

	part_local_destroy(op->part_local); //remove the temp local part
	op->part_local = NULL;
	gnome_canvas_item_hide(GNOME_CANVAS_ITEM(cop->target->group));

	canvas_op__finish(op);
}


static void
canvas_op__resize_left_finish(CanvasOp* op)
{
	canvas_op__finish(op);
}


static void
canvas_op__resize_right_finish(CanvasOp* op)
{
	AMPart* part = op->part;

#ifdef DEBUG
	double max_part_length = am_part__max_length_px_nz(op->part);
	double length = (gnome_canvas_part_get_right(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item)) - op->p1.x) / hzoom(op->arrange);
	dbg(0, "length=%.3f max_length=%.3f", length, max_part_length);
#endif

#if 0 //snapping disabled - needs a lot more work
	arr_snap_px2pos(op->arrange, &op->part->length, gnome_canvas_part_get_right(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item)) - op->p1.x);
	if(gpos_is_empty(&part->length)){
		am_snap_to_next(&part->length);
	}
#else
	arr_px2spos(op->arrange, &op->part->length, gnome_canvas_part_get_right(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item)) - op->p1.x);
#endif

	char bbst[128]; ayyi_pos2bbst(&op->part->start, bbst);
	dbg (0, "length=%dpx (%u) start=%s", (int)arr_spos2px(op->arrange, &op->part->length), (unsigned)ayyi_pos2samples(&op->part->length), bbst);

	AyyiNFrames orig = part->ayyi->length;
	if(ayyi_pos2samples(&part->length) != orig){
		am_part_set_length(part);
	}else{
		log_print(0, "length unchanged.");
		ayyi_samples2pos(orig, &part->length);
		gnome_canvas_item_set(((GnomeCanvasOp*)op)->item, "right", op->p1.x + arr_samples2px(op->arrange, orig), NULL);
	}

	canvas_op__finish(op);
}


static void
canvas_op__repeat_finish(CanvasOp* op)
{
	PF;
	g_canvas* g = op->arrange->canvas->gnome;
	gnome_canvas_item_hide(g->record_part);
	canvas_op__finish(op);

	double x1; g_object_get(g->record_part, "x1", &x1, NULL);
	double x2 = op->mouse.x;

	if(x2 > x1){
		// create some new parts
		GPos repeat_end; arr_px2pos(op->arrange, &repeat_end, x2);
		song_part_selection_repeat(op->arrange->part_selection, &repeat_end);
	}
}


static void
canvas_op__box_finish(CanvasOp* op)
{
	gnome_canvas_item_hide(op->arrange->canvas->gnome->drag_box);

#ifndef DEBUG_DISABLE_TOOLBAR
	if(op->arrange->toolbox.current == TOOL_MAG){
		// zoom mode
		arr_zoom_to(op->arrange, op->start.x, op->start.y, op->mouse.x, op->mouse.y);
	}else{
#endif
		// Selection mode (normal):

		// how many parts are inside the box?
		// -lets make it so that parts only need to be partially inside the box.
		GList* new_selection = NULL;
		int p;
		int pstart, pend;
		int track_start, track_end;
		bool end_before, start_after;
		p = -1;
		GList* part_item = NULL;
		for(part_item=song->parts->list;part_item;part_item=part_item->next){
			p++;
			AMPart* part  = part_item->data;
			pstart        = arr_part_start_px(op->arrange, part);
			pend          = arr_part_end_px(op->arrange, part);
			int box_start = MIN(op->start.x, op->mouse.x);
			int box_end   = MAX(op->start.x, op->mouse.x);
			if(pstart > box_end    ) start_after  = TRUE; else start_after = FALSE;//p st is after box
			if(pend   < box_start  ) end_before   = TRUE; else end_before  = FALSE;//p end is before box
#if 0
			bool start_before = ((pstart < box_start  ));                       // p st is before box
			bool end_after    = ((pend   > box_end    ));                       // p end is after box
			bool start_inside = ((pstart > box_start  ) && (pstart < box_end));
			bool end_inside   = ((pend   > box_start  ) && (pend   < box_end));
#endif
			track_start = arr_px2trk(op->arrange, op->start.y);
			track_end   = arr_px2trk(op->arrange, op->mouse.y);

			bool selected = false;
			if(end_before || start_after){
				selected = false;
			}else if(true){
				selected = true;
#if 0
			// crap
			}else if(start_before && end_after){
				dbg (0, "part %i: selected! 1. box is inside part.", p);
				selected = true;
			}else if(start_before && end_inside){ //starts before but ends inside the box.
				dbg (0, "part %i: selected! 2. part starts before box", p);
				selected = TRUE;
			}else if(end_after &&  start_inside){ //ends  after but starts inside the box.
				dbg (0, "part %i: selected! 3. part ends after box.", p);
				selected = TRUE;
			}else if(end_before &&  start_inside){ //part is completely inside the box.
				dbg (0, "part %i: selected! 3. part ends after box.", p);
				selected = TRUE;
			}else{
#endif
			}

			if(selected){
				dbg (3, "part %i: selected. box=%i->%i part=%i->%i", p, (int)op->start.x, (int)op->mouse.x, pstart, pend);
				TrackNum tnum = am_track_list_position(song->tracks, part->track);
				if((tnum >= track_start) && (tnum <= track_end)){ // check vertical bounding.
					new_selection = g_list_append(new_selection, part);
				}
			}
		}
		if(new_selection && list_cmp(new_selection, op->arrange->part_selection)){
			arr_part_selection_replace(op->arrange, new_selection, true);
		}
#ifndef DEBUG_DISABLE_TOOLBAR
	}
#endif

	canvas_op__finish(op);
}


static void
canvas_op__draw_finish(CanvasOp* op)
{
	GnomeCanvasOp* cop = (GnomeCanvasOp*)op;

	PF;
#ifndef DEBUG_TRACKCONTROL
	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);

	int start = arr_px2samples(op->arrange, op->diff.x);
	int end = arr_px2samples(op->arrange, op->mouse.x);
	if(end > start){
		MidiNote* note = g_new0(MidiNote, 1);
		note->note = arr_track_get_note_num_from_y((ArrTrackMidi*)at, op->diff.y);
		note->start = start;
		note->length = end - start;
		note->velocity = 0x40;
		am_song__midi_note_add(op->part, note, NULL, NULL);
	}
#endif

	if(cop->note) gnome_canvas_item_hide(cop->note);

	canvas_op__finish(op);
}


static void
canvas_op__edit_finish(CanvasOp* op)
{
	PF;

	if(op->max_movement > 2.0){
		// Move
		if(op->note_selection) am_midi_part__set_notes(op->part, canvas_op__get_note_selection(op, (MidiPart*)op->part), NULL, NULL);

	}else{
		// a Selection event

		//FIXME duplication added with new op type

		const MidiNote* note = arr_part_pick_note(op->arrange, op->part, op->diff, NULL, NULL);
		if(note){
			dbg(0, "got note! %i start=%i", note->note, note->start);
			am_part__set_note_selection(op->part, g_list_append(NULL, (MidiNote*)note));
			note_selection_list__new(&op->note_selection, op->part);
			shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 2, "1 note selected");
		}
		//reset the selection:
		else note_selection_list__reset(op->note_selection, op->part);

		am_song__emit("note-selection-change", op->arrange, op->note_selection);
	}

	canvas_op__finish(op);
}


static void
canvas_op__velocity_finish(CanvasOp* op)
{
	PF;

	if(op->max_movement > 1.0){
		if(op->note_selection) am_midi_part__set_notes(op->part, canvas_op__get_note_selection(op, (MidiPart*)op->part), NULL, NULL);
	}

	canvas_op__finish(op);
}


static void
canvas_op__divider_finish(CanvasOp* op)
{
	PF;

	canvas_op__finish(op);
	//TODO update other parts on same track.
}


static void
canvas_op__note_left_finish(CanvasOp* op)
{
	PF;
	canvas_op__note_right_finish(op);
}


static void
canvas_op__note_right_finish(CanvasOp* op)
{
	PF;

	if(op->max_movement > 1.0){
		if(op->note_selection) am_midi_part__set_notes(op->part, canvas_op__get_note_selection(op, (MidiPart*)op->part), NULL, NULL);
	}

	canvas_op__finish(op);
}


static void
canvas_op__note_select_finish(CanvasOp* op)
{
	PF;

	if(op->max_movement < 2.0){
		const MidiNote* note = arr_part_pick_note(op->arrange, op->part, op->diff, NULL, NULL);
		am_part__set_note_selection(op->part, g_list_append(NULL, (MidiNote*)note));
		note_selection_list__new(&op->note_selection, op->part);
		am_song__emit("note-selection-change", op->arrange, op->note_selection);
	}

	canvas_op__finish(op);
}


static void
snap_vertical(CanvasOp* op)
{
	// update the part object with the new track property

	ArrTrk* tr = canvas_op__get_track(op, op->p1.y);
	//dbg(2, "old track: d=%i", arr_t_to_d(op->arrange->priv->tracks, op->part->track->track_num));
	//dbg(2, "new track: d=%i", tr->track->track_num);
	g_return_if_fail(tr);

	op->part->track = tr->track;
}


static target*
target_new(Arrange* arrange)
{
	target* target = g_malloc(sizeof(*target));

	target->group = GNOME_CANVAS_GROUP(gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget)), gnome_canvas_group_get_type(), "x", 0.0, "y", 0.0, NULL));
	target->a     = gnome_canvas_item_new(target->group, gnome_canvas_polygon_get_type(), "fill-color-rgba", 0x00ff0077, NULL);
	target->b     = gnome_canvas_item_new(target->group, gnome_canvas_polygon_get_type(), "fill-color-rgba", 0x00ff0077, NULL);
	target->c     = gnome_canvas_item_new(target->group, gnome_canvas_polygon_get_type(), "fill-color-rgba", 0x00ff0077, NULL);
	target->d     = gnome_canvas_item_new(target->group, gnome_canvas_polygon_get_type(), "fill-color-rgba", 0x00ff0077, NULL);

	return target;
}


static void
target_destroy(CanvasOp* op)
{
	target* target = ((GnomeCanvasOp*)op)->target;
	if(!target) return;

	gtk_object_destroy((GtkObject*)target->a);
	gtk_object_destroy((GtkObject*)target->b);
	gtk_object_destroy((GtkObject*)target->c);
	gtk_object_destroy((GtkObject*)target->d);
	gtk_object_destroy((GtkObject*)target->group);

	g_free0(((GnomeCanvasOp*)op)->target);
}


static void
set_target(CanvasOp* op, double px1, double py1)
{
	// Target always uses snap settings.

	Arrange* arrange = op->arrange; 
	target* target = ((GnomeCanvasOp*)op)->target;
	if(!target->n_tracks){ pwarn("!!"); return; }
	g_return_if_fail(target->n_tracks);

	GnomeCanvasPoints* pts = gnome_canvas_points_new(6);
	// FIXME dont use py1 here - we need to use the mouse coords
	ArrTrk* atr      = canvas_op__get_track(op, py1);
	double top       = (double)atr->y * vzoom(arrange);
	double bottom    = (double)(atr->y + atr->height) * vzoom(arrange);
	float thickness  =  5.0;
	float height     = 15.0; // corner size
	float width      = (float)MIN(15, target->width/2 -5);
	float w          = target->inner_width;
	float xx         = arr_snap(arrange, px1);
	float yy         = bottom;

	// top left
	pts->coords[0] = xx+0.0;                            pts->coords[1] = top;
	pts->coords[2] = xx+thickness+width;                pts->coords[3] = top;
	pts->coords[4] = xx+thickness+width;                pts->coords[5] = top+thickness;
	pts->coords[6] = xx+thickness;                      pts->coords[7] = top+thickness;
	pts->coords[8] = xx+thickness;                      pts->coords[9] = top+thickness+height;
	pts->coords[10]= xx+0.0;                            pts->coords[11]= top+thickness+height;
	gnome_canvas_item_set(target->a, "points", pts, NULL);

	// top right
	if(target->width > thickness){
		pts->coords[0] = xx+target->width-thickness-w;      pts->coords[1] = top;
		pts->coords[2] = xx+target->width;                  pts->coords[3] = top;
		pts->coords[4] = xx+target->width;                  pts->coords[5] = top+thickness+height;
		pts->coords[6] = xx+target->width-thickness;        pts->coords[7] = top+thickness+height;
		pts->coords[8] = xx+target->width-thickness;        pts->coords[9] = top+thickness;
		pts->coords[10]= xx+target->width-thickness-w;      pts->coords[11]= top+thickness;
		gnome_canvas_item_set(target->b, "points", pts, NULL);
	}

	// bottom left
	pts->coords[0] = xx;                                pts->coords[1] = yy;
	pts->coords[2] = xx;                                pts->coords[3] = yy-thickness-height;
	pts->coords[4] = xx+thickness;                      pts->coords[5] = yy-thickness-height;
	pts->coords[6] = xx+thickness;                      pts->coords[7] = yy-thickness;
	pts->coords[8] = xx+width;                          pts->coords[9] = yy-thickness;
	pts->coords[10]= xx+width;                          pts->coords[11]= yy;
	gnome_canvas_item_set(target->c, "points", pts, NULL);

	// bottom right
	if(target->width > thickness){
		pts->coords[0] = xx+target->width;                  pts->coords[1] = yy;
		pts->coords[2] = xx+target->width-thickness-w;      pts->coords[3] = yy;
		pts->coords[4] = xx+target->width-thickness-w;      pts->coords[5] = yy-thickness;
		pts->coords[6] = xx+target->width-thickness;        pts->coords[7] = yy-thickness;
		pts->coords[8] = xx+target->width-thickness;        pts->coords[9] = yy-thickness-height;
		pts->coords[10]= xx+target->width;                  pts->coords[11]= yy-thickness-height;
		gnome_canvas_item_set(target->d, "points", pts, NULL);
	}

	gnome_canvas_points_free(pts);
	gnome_canvas_item_raise_to_top(GNOME_CANVAS_ITEM(target->group));
	gnome_canvas_item_show        (GNOME_CANVAS_ITEM(target->group));
}


static void
canvas_op__start_scroll_timer(CanvasOp* op)
{
	op->timer = g_timeout_add(50/*ms*/, (gpointer)drag_mouse_timeout, op);
}


void
gnome_canvas_op__set_arrange(CanvasOp* op, Arrange* arrange)
{
	if(arrange != op->arrange){
#ifdef USE_GNOMECANVAS
		target_destroy(op);
#endif
	}
	op->arrange = arrange;
}


static bool
drag_mouse_timeout(CanvasOp* op)
{
	Arrange* arrange = op->arrange;
	Pti* origin = &((GnomeCanvasOp*)op)->origin;
	g_return_val_if_fail(arrange, false);

	static int scroll_dx = SCROLL_MULTIPLIER;

	gboolean stop = true;

	GdkWindow* window = arrange->canvas->widget->window;
	g_return_val_if_fail(window, false);

	gint x = 0, y = 0, topwinx = 0, topwiny = 0, rootx, rooty;
	gdk_window_get_pointer(window, &x, &y, NULL);
	gdk_window_get_pointer(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &topwinx, &topwiny, NULL);
	gdk_window_get_origin (gtk_widget_get_toplevel(arrange->canvas->widget)->window, &rootx, &rooty);

	int screen_height = gdk_screen_get_height(gdk_screen_get_default()); // this may not be the right screen in a multiscreen setup

	// do the bounds checking again in case the mouse has moved:
	if(x > arrange->canvas_scrollwin->allocation.width - SCROLL_EDGE_WIDTH){
		// we are at the rhs edge.
		arrange->canvas->scroll_right(arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->x = origin->x - scroll_dx/SCROLL_MULTIPLIER; //update starting pt so we have an up-to-date ref point.
		stop = false;
	}
	else if(x < SCROLL_EDGE_WIDTH){
		// we are at lhs edge of the screen
		call(arrange->canvas->scroll_left, arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->x = origin->x + scroll_dx / SCROLL_MULTIPLIER;
		stop = false;
	}
	else if(rooty + topwiny < SCROLL_EDGE_WIDTH){
		// we are at the top edge of the screen.
		call(arrange->canvas->scroll_down, arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->y += scroll_dx / SCROLL_MULTIPLIER;
		stop = false;
	}
	else if(rooty + topwiny > screen_height - SCROLL_EDGE_WIDTH){
		// we are at the bottom edge of the screen.
		call(arrange->canvas->scroll_up, arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->y = origin->y - scroll_dx / SCROLL_MULTIPLIER;
		stop = false;
	}
	else scroll_dx = 1;

	// update the scroll speed
	if(!stop) scroll_dx = MIN((int)(scroll_dx * 1.0 + 1), SCROLL_MAX_SPEED);

	if(stop) op->timer = 0;
	return !stop;
}


// TODO OP_MOVE uses scrolltime etc. For consistency, should it change to use this fn?
void
canvas_op__handle_scrolling(CanvasOp* op)
{
	// The edge detection is done here to determine whether the timer needs to be started.
	// It is done again in the timeout as the mouse may have moved.
	if(canvas_op__is_right_edge(op) || canvas_op__is_left_edge(op)){
		op->timer = g_timeout_add(50, (gpointer)drag_mouse_timeout, op);
	}
}


