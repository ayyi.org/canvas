/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include <gdk/gdkkeysyms.h>

#include "widgets/svgtogglebutton.h"
#include "windows.h"
#include "toolbox.h"
#include "toolbar.h"
#include "arrange.h"
#include "part_manager.h"
#include "arrange/part.h"
#include "support.h"
#include "song.h"
#include "icon.h"

static GtkWidget* toolbox_menu_init ();
static void       tool_on_click     (GtkWidget*, gpointer _tooltype);


static ToolboxButton toolboxi[TOOL_MAX] = {
	{"Normal",   "pointer",                    },
	{"Pencil",   "pencil",   CURSOR_PENCIL,    },
	{"Mute",     "cross",    CURSOR_CROSSHAIR, },
	{"Zoom",     "mag",      CURSOR_ZOOM_IN,   },
	{"Scissors", "scissors", CURSOR_SCISSORS,  },
	{"Scroll",   "hand",     CURSOR_HAND_OPEN, }
};


/*
 *  Create the context menu for the arrange window root canvas.
 */
static GtkWidget*
toolbox_menu_init ()
{
	AyyiArrangeClass* arrange_class = g_type_class_peek(AYYI_TYPE_ARRANGE);

	GtkWidget* menu = gtk_menu_new();

	for(int i=0;i<TOOL_MAX;i++){
		ToolboxButton* tool = &arrange_class->toolbox[i];

		tool->action = gtk_action_new(tool->key, tool->key, "Tooltip", tool->stock_id);
		GtkWidget* item = gtk_action_create_menu_item (tool->action);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		g_signal_connect(tool->action, "activate", G_CALLBACK(tool_on_click), GINT_TO_POINTER(i));
	}

	gtk_widget_show_all(menu);

	return menu;
}


void
toolbox_new (Arrange* arrange, GtkWidget* container)
{
	AyyiArrangeClass* klass = AYYI_ARRANGE_GET_CLASS(arrange);

	if(!klass->toolbox) klass->toolbox = (ToolboxButton*)&toolboxi;
	if(!klass->rootmenu) klass->rootmenu = toolbox_menu_init(); // canvas context menu

	for(int i=0;i<TOOL_MAX;i++){
		ToolboxButton* item = &toolboxi[i];
		GtkWidget* button = arrange->toolbox.buttons[i] = (GtkWidget*)svg_toggle_button_new(item->stock_id);
		((SvgToggleButton*)button)->on = item->action;
		gtk_box_pack_start(GTK_BOX(container), button, FALSE, FALSE, 0);

		g_signal_connect(G_OBJECT(button), "enter-notify-event", G_CALLBACK(mouseover_enter), (gpointer)item->key);
		g_signal_connect(G_OBJECT(button), "leave-notify-event", G_CALLBACK(mouseover_leave), &arrange->panel);
	}
}


void
toolbox_free (Arrange* arrange)
{
}


static void
tool_on_click (GtkWidget* widget, gpointer _tooltype)
{
	PF;
	static Arrange* a = NULL; //temp(!) workaround for double triggering. probably due to the gtk_toggle_button_set_active call?

	Arrange* arrange = app->latest_arrange; //g_object_get_data((GObject*)widget, "panel");
	if(a && a == arrange){ /*pwarn("aleady in handler! aborting...");*/ return; }
	a = arrange;
	g_return_if_fail(arrange_verify(arrange));

	toolbox_change_tool(arrange, GPOINTER_TO_INT(_tooltype));

	a = NULL;
}


void
toolbox_change_tool (Arrange* arrange, int tooltype)
{
	if(tooltype < TOOL_DEFAULT || tooltype >= TOOL_MAX){ pwarn("unknown tool type: %i", tooltype); return; }

	ToolboxButton* item = &toolboxi[tooltype];

	arrange->toolbox.current = tooltype;

	if(item && arrange->canvas->widget && GDK_IS_WINDOW(arrange->canvas->widget->window))
		set_cursor(arrange->canvas->widget->window, item->cursor);

	// Show button state
	for(int i=0;i<TOOL_MAX;i++) svg_toggle_button_set_active ((SvgToggleButton*)arrange->toolbox.buttons[i], false);
	GtkWidget* active_button = arrange->toolbox.buttons[arrange->toolbox.current];
	svg_toggle_button_set_active ((SvgToggleButton*)active_button, true);

	g_signal_emit_by_name (arrange, "tool-change");
}


void
toolbox_position (GtkMenu* menu, gint* x, gint* y, gboolean* push_in, gpointer user_data)
{
	// The menu is offset so that the mouse pointer is over the first entry.

	GdkEvent* event = user_data;

	gint ox, oy;
	gdk_window_get_origin(event->any.window, &ox, &oy);

	*x = ox + (gint)event->button.x -12;
	*y = oy + (gint)event->button.y -12;
}
