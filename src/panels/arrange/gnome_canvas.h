/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <libgnomecanvas/libgnomecanvas.h>
#include "panels/arrange/part.h"

#define CANVAS_IS_GNOME (arrange->canvas->type == gcanvas_get_type())

typedef struct _g_canvas
{
	GList*             parts;                // list of GnomeCanvasPart*.
	GList*             selected_parts;       // list of GnomeCanvasPart*. Stored so that they can be easily unselected.
	GnomeCanvasItem*   record_part;          // temp part used during recording.
	GnomeCanvasItem*   loc[20];              // locator line widgets.
	GnomeCanvasItem*   bg;                   // the canvas background.
	GnomeCanvasItem*   spp;
	GnomeCanvasItem*   spp2;
	GnomeCanvasPoints* spp_pts;              // the canvas coords of the spp indicator line.

	GnomeCanvasItem*   cyc_fill;             // fill between left and right locators.
	GnomeCanvasItem*   tline[AM_MAX_TRK+1];  // the track lines on canvas background. Indexed by display order
	GnomeCanvasItem*   xhair;                // crosshairs using during drag operations.
	GnomeCanvasItem*   yhair;
	GnomeCanvasItem*   drag_box;             // 'rubber band'. Normally invisible.
	GnomeCanvasItem*   split_line;           // for part split operations.

    struct _GCAuto {
        GList*         dirty_curves;         // list of curve* objects that need redrawing. The GnomeCanvasPathDef is updated, but the display needs to be updated to reflect this.
    }                  auto_;

    GnomeCanvasItem*   auto_ctl_1;           // control lines for selected automation bezier subpath.
    GnomeCanvasItem*   auto_ctl_2;
    GnomeCanvasItem*   auto_ctl_3;           // control lines for subpaths adjacent to the selected one.
    GnomeCanvasItem*   auto_ctl_4;
    GnomeCanvasItem*   auto_hnd_1;           // rect handle on the ctl line for mouse interaction.
    GnomeCanvasItem*   auto_hnd_2;           // rect handle on the ctl line for mouse interaction.
    GnomeCanvasItem*   auto_hnd_3;           //  "" for the _adjacent_ subpath.
    GnomeCanvasItem*   auto_hnd_4;           //  ""
    GnomeCanvasItem*   auto_pt_1;            // rect handle on the main curve pt for mouse interaction.
    GnomeCanvasItem*   auto_pt_2;            // rect handle on the main curve pt for mouse interaction.

    // some of these control pts dont change. To make use of that we need to add 2 more (1 per pt).
    GnomeCanvasPoints* auto_pts1;            // the coords of the control line.
    GnomeCanvasPoints* auto_pts2;

    int             track_pos[AM_MAX_TRK+1]; // top of each track
} g_canvas;

typedef struct
{
	LocalPart          part;

	GnomeCanvasItem*   canvasgroup;
	GnomeCanvasItem*   box;                  // part box

} GCanvasLocalPart;


void             gcanvas_init                   ();
CanvasType       gcanvas_get_type               ();

void             gcanvas_part_rename_start      (Arrange*);
GnomeCanvasPart* gcanvas_get_canvaspart_by_part (Arrange*, AMPart*);
GnomeCanvasItem* gcanvas_get_part_widget        (Arrange*, AMPart*);

void             gcanvas_scrollregion_update    (Arrange*);
void             gcanvas_background_update      (Arrange*);
void             gcanvas_set_xhairs             (Arrange*, double x, double y);

void             gcanvas_part_local_draw        (GnomeCanvasGroup*, LocalPart*);
