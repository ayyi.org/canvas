/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*   A GtkSpinButton that tracks an Ayyi Observable rather than a GtkAdjustable
*/
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "model/observable.h"
#include "ayyi_gtk_spin_button.h"

struct _AyyiGtkSpinButtonPrivate {
	Observable* observable;
};

GType ayyi_gtk_spin_button_get_type (void) G_GNUC_CONST;
G_DEFINE_TYPE_WITH_PRIVATE (AyyiGtkSpinButton, ayyi_gtk_spin_button, GTK_TYPE_SPIN_BUTTON)

static void ayyi_gtk_spin_button_finalize (GObject*);

enum  {
	AYYI_GTK_SPIN_BUTTON_0_PROPERTY
};


AyyiGtkSpinButton*
ayyi_gtk_spin_button_construct (GType object_type, Observable* observable)
{
	g_return_val_if_fail(observable, NULL);

	AyyiGtkSpinButton* self = (AyyiGtkSpinButton*) g_object_new (object_type,
		"adjustment", gtk_adjustment_new (observable->value.f, observable->min.f, observable->max.f, 1.0, 10.0, 0.0),
		"value", observable->value.f,
		NULL
	);

	self->priv->observable = observable;

	void a (Observable* o, AMVal val, gpointer _button)
	{
		AyyiGtkSpinButton* self = _button;
		gtk_adjustment_set_value(((GtkSpinButton*)self)->adjustment, val.f);
	}
	observable_subscribe(observable, a, self);

	return self;
}


AyyiGtkSpinButton*
ayyi_gtk_spin_button_new (Observable* observable)
{
	return ayyi_gtk_spin_button_construct (AYYI_TYPE_GTK_SPIN_BUTTON, observable);
}


static void
ayyi_gtk_spin_button_class_init (AyyiGtkSpinButtonClass* klass)
{
	ayyi_gtk_spin_button_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->finalize = ayyi_gtk_spin_button_finalize;
}


static void
ayyi_gtk_spin_button_init (AyyiGtkSpinButton* self)
{
	self->priv = ayyi_gtk_spin_button_get_instance_private(self);
}


static void
ayyi_gtk_spin_button_finalize (GObject* obj)
{
	G_OBJECT_CLASS (ayyi_gtk_spin_button_parent_class)->finalize (obj);
}
