/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __fader_h__
#define __fader_h__

#include <gtk/gtk.h>
#include <glib-object.h>
#include <glib.h>
#include "model/channel.h"

G_BEGIN_DECLS

#define TYPE_FADER            (fader_get_type ())
#define FADER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FADER, Fader))
#define FADER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FADER, FaderClass))
#define IS_FADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FADER))
#define IS_FADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FADER))
#define FADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FADER, FaderClass))

typedef struct _Fader Fader;
typedef struct _FaderClass FaderClass;
typedef struct _FaderPrivate FaderPrivate;

struct _Fader {
	GtkScale      parent_instance;
	FaderPrivate* priv;
};

struct _FaderClass {
	GtkScaleClass parent_class;
};

GType      fader_get_type    () G_GNUC_CONST;
GtkWidget* fader_new         (GtkAdjustment*);
Fader*     fader_construct   (GType object_type);

void       fader_set_channel (GtkWidget*, AMChannel*, AMChannel* prev);
void       fader__update     (GtkWidget*, AMChannel*);
void       fader__set_value  (GtkWidget*, double val);

G_END_DECLS

#endif
