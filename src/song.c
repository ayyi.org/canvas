/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __song_c__

#include "global.h"
#include "model/time.h"
#include "model/channel.h"
#include "model/connections.h"
#include "model/transport.h"
#include "model/am_message.h"
#include "model/track_list.h"
#include "window.h"
#include "windows.h"
#include "window.statusbar.h"
#include "part_manager.h"
#include "transport.h"
#include "support.h"
#include "toolbar.h"
#include "pool_model.h"
#include "panels/arrange.h"
#include "src/song.h"
#include "save.h"
#include "seqedit.h"
#include "menu.h"
#include "plugin.h"
#include "io.h"
#include "meter.h"
#include "settings.h"
#ifdef GDLDOCK_XML
#  include "gdl/gdl-dock-layout.h"
#endif

extern SMConfig* config;

extern const uint64_t mu_per_sub;
extern const uint64_t mu_per_beat;
extern const uint32_t ticks_per_beat;

GuiSong gui_song;

static void   song_load_windows ();
static void   on_song_load      (GObject*, gpointer);
static void   on_song_unload    (GObject*, gpointer);
static void   tempo_connect     ();
static void   channels_connect  ();

#include "channel.c"


void
song_init()
{
	song->snap_mode = config->snap_mode;

	tempo_connect();

	song->input_list = NULL;
	song->bus_list   = NULL;

	pool_init();
	channels_connect();

	gui_song.ch_menu = ch_menu_init();
	output_menu_init();

	am_song__connect("song-load", G_CALLBACK(on_song_load), NULL);
	am_song__connect("song-unload", G_CALLBACK(on_song_unload), NULL);
}


bool
song_free()
{
	// song object is only free'd when quitting the application

	pool_free();
	am_song__free();

	return true;
}


/*
 *  Setup and request a new track to be added to songcore.
 *
 *  It provides the following functionality on top of the AMSong method:
 *   -autogenerates a track name.
 *   -selects the new track.
 *   -outputs additional messages to the ui.
 */
void
song_add_track (AMTrackType type, int n_channels, TrackCallback callback, gpointer user_data)
{
	if (!ayyi.got_shm) return;
	g_return_if_fail (AM_TRACK_TYPE_IS_VALID(type));

	if(am_track_list_count(song->tracks) >= AM_MAX_TRK){
		log_print(LOG_FAIL, "Sorry, maxed out on tracks already.");
		return;
	}

	typedef struct { TrackCallback callback; gpointer user_data; AMTrackType track_type; } C;

	void song_add_track_done (AyyiIdent id, const GError* error, gpointer user_data)
	{
		PF;
		C* d = user_data;
		g_return_if_fail(d);

		if(!error){
			AMTrackType track_type = d->track_type;
			if(!track_type || track_type > TRK_TYPE_MIDI){ perr("bad track type!"); track_type = TRK_TYPE_AUDIO; }

			AMTrack* tr = am_track_list_find_by_shm_idx(song->tracks, id.idx, track_type);
			if(tr){
				log_print(0, "New track added '%s'", tr->name);
				arr_track_select(app->latest_arrange, tr);
			}else{
				pwarn("model has not added the track!");
			}

			call(d->callback, tr, (GError**)&error, d->user_data);

		} else {
			log_print(LOG_FAIL, "new track: %s", error->message);
		}

		g_free(d);
	}

	char* name = g_strdup_printf("%s %02i", (type==TRK_TYPE_MIDI) ? "Midi" : "Audio", am_track_list_count(song->tracks) + 1);
	dbg (1, "name='%s'", name);

	shell__statusbar_print (NULL, STATUSBAR_MAIN, "Adding track...");

	am_song_add_track(type, name, n_channels, song_add_track_done, AYYI_NEW(C,
		.callback = callback,
		.user_data = user_data,
		.track_type = type
	));

	g_free(name);
}


void
song_del_track (GList* track_selection, AyyiHandler callback, gpointer user_data)
{
	// Requests that the selected tracks be deleted.

	typedef struct _data
	{
		AMTrack*    tr;
		AyyiHandler callback;
		gpointer    user_data;
	} C;

	void song_del_track__done (AyyiIdent id, GError** error, gpointer _)
	{
		C* c = _;

#ifdef DEBUG
		if (am_track_list_count(song->tracks) != ayyi_song__get_track_count())
			pwarn ("unexpected track count: am_track_list_count()=%i engine_count=%i", am_track_list_count(song->tracks), ayyi_song__get_track_count());
#endif

		log_print(0, "track deleted");

		call(c->callback, id, error, c->user_data);

		meters_start();
		PF_DONE;
		if (_debug_) printf("\n");
		g_free(c);
	}

	dbg (0, "deleting %i tracks... %s%s%s", g_list_length(track_selection), bold, track_selection ? ((AMTrack*)track_selection->data)->name : NULL, white);

	meters_stop();

	if (_debug_ > 1) am_song__print_tracks();

	for (GList* l=track_selection;l;l=l->next) {
		AMTrack* tr = l->data;
		TrackNum t = am_track_list_position(song->tracks, tr);

		g_return_if_fail(am_track__is_valid(tr));
		dbg (2, "shm_num=%x", tr->ident.idx);

		am_song_remove_track(tr, song_del_track__done, AYYI_NEW(C,
			.tr = tr,
			.callback = callback,
			.user_data = user_data
		));

		dbg(0, "trk=%i trk_count=%i idx=%x", t, am_track_list_count(song->tracks), tr->ident.idx);
	}
}


void
song_add_parts_from_selection(AyyiPanel* panel, AyyiSongPos* pos, int track_offset)
{
	// copy the part selection to a new position.

	// @param panel:        specifies which selectionlist to use. If NULL, use the song selectionlist.
	// @param pos:          the position that the earliest Part in the list will be moved to.
	// @param track_offset: move each part by this number of tracks. Note that the track can also be specified using the AMPart->track property.

#ifdef DEBUG
	char bbst[32]; ayyi_pos2bbst(pos, bbst);
	dbg(1, "copying parts to: pos=%s", bbst);
#endif

	GList* parts = NULL;
	if(panel && AYYI_IS_ARRANGE(panel)){
		parts = ((Arrange*)panel)->part_selection;
	} else parts = am_parts_selection;

	if(!parts) log_print(0, "cannot copy Parts - nothing selected.");

	AyyiSongPos list_start;
	int track_min = 0;
	int track_max = 0;

	am_partmanager__find_boundary(parts, &list_start, NULL, &track_min, &track_max);
#ifdef DEBUG
	ayyi_pos2bbst(&list_start, bbst);
	dbg(1, "list_start=%s", bbst);
#endif

	if(track_min + track_offset < 0) perr ("track out of range");
	if(track_max + track_offset >= am_track_list_count(song->tracks)) perr ("track out of range");

	GList* l = parts;
	for(;l;l=l->next){
		AMPart* part = l->data;

		AyyiSongPos part_start = part->start;
		ayyi_pos_sub(&part_start, &list_start);
		char bbst[32]; ayyi_pos2bbst(&part_start, bbst); dbg(1, "distance from list start: %s", bbst);
		ayyi_pos_add(&part_start, pos);

		char bbst1[32]; ayyi_pos2bbst(&part->start, bbst1);
		char bbst2[32]; ayyi_pos2bbst(&part_start, bbst2);
		dbg (1, "start= %s %s", bbst1, bbst2);
		dbg (2, "part->length=%i", part->length.beat);

		uint64_t m = PART_MAX_MU;
		if(part->pool_item) m = ayyi_samples2mu(((Waveform*)part->pool_item)->n_frames);

		AyyiIdent ident = {PART_IS_AUDIO(part) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART, part->ayyi->shm_idx};

		GPos p; songpos_ayyi2gui(&p, &part_start);
		song_part_new(NULL, &ident, &p, am_track_list_at(song->tracks, am_track_list_position(song->tracks, part->track) + track_offset), MIN(ayyi_pos2mu(&part->length), m), NULL, part->bg_colour, NULL, NULL, NULL);
	}
}


void
song_add_part_from_prototype(AMPart* part)
{
	// ownerhip of @part is taken

	dbg(1, "inset=%u", part->region_start);
	GPos p; songpos_ayyi2gui(&p, &part->start);
	song_part_new(part->pool_item, NULL, &p, part->track, ayyi_pos2mu(&part->length), &part->region_start, part->bg_colour, NULL, NULL, NULL);
	g_free(part);
}


/*
 *  Request a new part to be made.
 *
 *  @param pool_item may be NULL, indicating either an empty part, or that we should get the pool_item from the region_idx.
 *  @param _region_idx - if empty, we use a region based on the whole pool_item.
 *                       Can be REGION_FULL.
 *  @param x             is a pixel value relative to window origin (ie not canvas coords).
 *  @param len           is in mu. Set it to zero to force usage of the pool length.
 *  @param inset         if this is NULL, the existing inset from the source will be used.
 *  @param name          if NULL then part name is taken from region_idx if there is one, else from pool_item.
 *
 *  -currently only usable for audio parts.
 *  -warning: if parts are created too quickly, part_name_make_unique() will not work properly.
 */
void
song_part_new (AMPoolItem* pool_item, AyyiIdent* region_ident, GPos* pos, AMTrack* trk, int64_t len, uint32_t* _inset, unsigned colour, const char* name, AyyiHandler callback, gpointer user_data)
{
	ASSERT_SONG_SHM;
	g_return_if_fail(trk);

	int32_t* _region_idx = region_ident ? &region_ident->idx : NULL;

	// ignore track type as parts can be dropped on other kinds of tracks.
	AyyiMediaType media_type = (pool_item || (region_ident && region_ident->type == AYYI_OBJECT_AUDIO_PART)) ? AYYI_AUDIO : AYYI_MIDI;

	bool have_region = false;
	uint32_t region_idx = -2;
	char* source_name = NULL;

	if(media_type == AYYI_AUDIO){
		if(!pool_item && !_region_idx){ perr ("bad args: no src."); return; }

		if(!pool_item){
			//try and get the missing pool_item from the region_idx:
			AMPoolItem* item;
			if((item = am_song__get_pool_item_from_region(*_region_idx))){
				dbg (1, "pool_item found!");
				pool_item = item;
			}
		}

		// Find the region_index. Region index is currently underused but is important for copy operations.
		// Note: we have both default_region and REGION_FULL. The latter is needed when default_region doesnt exist.
		if(_region_idx){
			region_idx = *_region_idx;
			have_region = true;
		} else {
			// use the default region for the given pool_item

			// wait a minute! the default region is almost irrelevant!
			// it does have the trimmed name, but we might as will retrim it?

			uint32_t default_region;
			if(false && pool_item_get_default_region(pool_item, &default_region)){
				dbg (1, "default region found: %i", region_idx);
				region_idx = default_region;
				have_region = true;
			}else{
				// pool_item has no default region. Probably is new. we need to get missing information directly.
				have_region = false;
			}
		}
		if(!have_region) region_idx = AM_REGION_FULL;
		dbg (1, "&_region_idx=%p region_idx=0x%x=%u", _region_idx, region_idx, region_idx);

		AyyiRegion* region = NULL;
		if(have_region){
			if(region_idx != AM_REGION_FULL){
				/*if(region_idx)*/ region = ayyi_song__audio_region_at(region_idx);
			}
			source_name = region ? region->name : pool_item->filename;
			dbg (1, "taking source from: %s: %s", region ? "region" : "pool_item", region ? region->name : " ");
		}
		else{
			source_name = pool_item->leafname;
		}
	}
	else{ //midi
		source_name = trk->name;
	}

	if(name) source_name = (char*)name;
	g_return_if_fail(source_name);

	//colour:
	if(colour > 63){ pwarn ("colour out of range (%i).", colour); colour=0; }

	//start position:
	AyyiSongPos start;
	songpos_gui2ayyi(&start, pos);

	// part length:
	{
		if(pool_item){
			uint64_t pool_len_mu = ayyi_samples2mu(((Waveform*)pool_item)->n_frames);
			dbg (2, "pool_len=%Lu", pool_len_mu);

			if(!len) len = pool_len_mu;

			//the part cannot be longer than the pool item:
			if(len > pool_len_mu){
				pwarn ("requested length is longer than the pool item. len=%"PRIu64" pool_len=%"PRIu64, len, pool_len_mu);
				len = pool_len_mu;
			}
		}else{
			//no pool item. use the len arg supplied.
		}
		if(len < 1){
			uint64_t default_length = bars2mu(2);
			len = default_length;
			dbg (0, "using default part length...");
		}
	}

	//inset:
	uint32_t inset = _inset ? *_inset : 0;
	if(pool_item && inset > ((Waveform*)pool_item)->n_frames){ pwarn("inset too long. _inset=%p", _inset); inset = 0; }

	//make a unique name for the new part:
	dbg (1, "source_name=%s", source_name);
	char name_unique[AYYI_NAME_MAX], truncated[256];
	if(have_region){
		strncpy(truncated, basename(source_name), 255);
	}else{
		//remove -L and -R:
		filename_remove_extension(basename(source_name), truncated);
		char* pos = truncated + strlen(truncated) - 2;
		if(g_strrstr(truncated, "-L") == pos || g_strrstr(truncated, "-R") == pos){
			truncated[strlen(truncated) - 2] = '\0';
		}
	}
	ayyi_region__make_name_unique(name_unique, truncated);

	int trk_idx = trk->ident.idx;

	char len_bbst[64];
	ayyi_mu2bbst(len, len_bbst);
	dbg (1, "tnum=%i %s startb=%i len=%Limu(%s) inset=%u '%s'", am_track_list_position(song->tracks, trk), ayyi_print_media_type(media_type), start.beat, len, len_bbst, inset, name_unique);

	void
	song_add_part_done(AyyiIdent obj, GError** error, gpointer user_data)
	{
		dbg (0, "idx=%u", obj.idx);

		if(!(error && *error)){
			//try and select the part. The part _must_ be created by now: make sure it is.
			AMPart* part = am_song__get_part_by_ident(obj);
			if(part){
				am_collection_selection_replace (am_parts, g_list_append(NULL, part), NULL);
			}else{
				pwarn("cannot get new object: %s.%i", ayyi_print_object_type(obj.type), obj.idx);
			}
		}
	}

	am_song__add_part(media_type, region_idx, pool_item, trk_idx, &start, len, colour, name_unique, inset, song_add_part_done, NULL);
}


AyyiAction*
song_part_delete (const AMPart* part)
{
	g_return_val_if_fail(part, NULL);
	PF;

	dbg (1, "idx=%i part_count=%i %s", part->ayyi->shm_idx, g_list_length(song->parts->list), part->name);

	void song_part_delete_done(AyyiIdent id, GError** error, gpointer user_data)
	{
		report(0, "part deleted");
#if 0
		dbg (2, "song selection size: %i", g_list_length(am_parts_selection));
		arrange_foreach {
			dbg (2, "arr selection size: %i", g_list_length(arrange->part_selection));
		} end_arrange_foreach
#endif
	}

	return am_song__remove_part((AMPart*)part, song_part_delete_done, NULL);
}


void
song_part_selection_repeat (GList* selection, GPos* _end)
{
	AyyiSongPos end; songpos_gui2ayyi(&end, _end);

	AyyiSongPos _selection_start;
	GPos selection_end;
	am_partmanager__get_pos(selection, &_selection_start, &selection_end);
	GPos selection_start; songpos_ayyi2gui(&selection_start, &_selection_start);

	#define REPEAT_Q Q_BEAT
	q_to_prev_or_current(&selection_start, &song->q_settings[REPEAT_Q]);
	q_to_next_or_current(&selection_end, &song->q_settings[REPEAT_Q]);

	songpos_gui2ayyi(&_selection_start, &selection_start);
	AyyiSongPos offset; songpos_gui2ayyi(&offset, &selection_end); ayyi_pos_sub(&offset, &_selection_start);
	AyyiSongPos d = offset; //cumulative offset
	int i = 0;
	#define MAX_REPEAT 100
	while(i++ < MAX_REPEAT){
		GList* l = selection;
		for(;l;l=l->next){
			AMPart* part = l->data;
			AyyiSongPos start = part->start;
			ayyi_pos_add(&start, &d);
			if(ayyi_pos_is_after(&start, &end)){ i = MAX_REPEAT; break; }

			char name[64];
			snprintf(name, 63, "%.50s repeat %.3i", part->name, i+1); //doesnt have to be unique, as part_new will do that.
			dbg(0, "***** new part! %s", name);
			AyyiIdent ident = {PART_IS_AUDIO(part) ? AYYI_OBJECT_AUDIO_PART : AYYI_OBJECT_MIDI_PART, part->ayyi->shm_idx};
			GPos p; songpos_ayyi2gui(&p, &start);
			song_part_new(part->pool_item, &ident, &p, part->track, ayyi_pos2mu(&part->length), &part->region_start, part->bg_colour, name, NULL, NULL);
		}
		ayyi_pos_add(&d, &offset);
	}
}


bool
song_load ()
{
	if(app->state < APP_STATE_ENGINE_OK) return false;
	if(!ayyi.got_shm){ dbg (0, "shm not available. cannot load song."); return false; }

	arr_statusbar_printf(NULL, 1, "Song loading...");

	pool_model_clear();

	am_song__load();

	return true;
}


static void
on_song_load (GObject* _song, gpointer user_data)
{
	PF;

	pool_model_update_now();
	channels_on_song_load();

	if(_debug_ > 1) am_song__print_tracks();

	song->snap_mode = config->snap_mode;

	song_get_history();

	song_load_windows();

	char gui_data_path[256];
	snprintf(gui_data_path, 255, "%s/gui_data.yaml", ayyi_song__get_file_path());
	load_gui_song_file(gui_data_path);

	app_set_state(APP_STATE_SONG_LOADED);

	request_toolbar_update(); 

	transport_enable();

	recent_songs_update();

	log_print(LOG_OK, "song loaded");
	arr_statusbar_printf(NULL, 1, "Song loaded. Tracks:%i Parts:%i Files:%i", am_track_list_count(song->tracks), g_list_length(song->parts->list), ayyi_song__get_file_count());

	arr_set_song_title();

	window_foreach {
		if(window->shell_menus) menu_sync_panel_items(window);
	} end_window_foreach

	dbg (2, "song->track_count=%i", am_track_list_count(song->tracks));
	if(false && !am_track_list_count(song->tracks)) song_add_track(TRK_TYPE_AUDIO, 1, NULL, NULL);
}


void
song_load_new (const char* filename)
{
	transport_disable(); //do we need a new app.state?

	am_song__load_new(filename, NULL, NULL);
}


static const char* layout_name = "__default__";

static void
song_load_windows ()
{
	// Open any windows reported as active in the config file.

	// Windows are stored in 2 places: the song file, and the application-config file.
	// This fn should only be concerned with song windows.
	// If there is gui information saved for the Song (there might not be), the application-config information will be overridden.

	PF;

	if(gui_song.loaded_layout && !strcmp(gui_song.loaded_layout, layout_name)) return;

	if(!config->windows){
		// make sure we always have at least one window open.
		if(!windows){
			window__new_floating(g_type_class_peek(AYYI_TYPE_ARRANGE));
		}
		return;
	}

	// Close windows already open that are no longer needed.
	extern GType ayyi_log_win_get_type ();
	panel_foreach {
		int n = windows__get_n_instances(G_OBJECT_TYPE(panel));
		int c = config_get_n_windows(G_OBJECT_TYPE(panel));
		if(n > c){
			dbg(1, "%s: window not needed: have=%i need=%i", G_OBJECT_CLASS_NAME(AYYI_PANEL_GET_CLASS(panel)), n, c);
			AyyiPanel* panel = windows__get_from_instance(ayyi_log_win_get_type(), n);
			if(panel) window__destroy_panel(window, panel);
		}
	} end_panel_foreach;

	AyyiWindow* ay = windows__open_new_window();
	gtk_widget_show_all((GtkWidget*)ay);

	gboolean resize (gpointer _w)
	{
		// (temporary) resize the window again as sometimes the original resize appears to be done too early.

		ConfigWindow* window = _w;
		gtk_window_resize(GTK_WINDOW(windows->data), window->width, window->height);
		return G_SOURCE_REMOVE;
	}

	ConfigWindow* window = &g_array_index(config->windows, ConfigWindow, 0);
	GdkScreen* screen = gdk_screen_get_default();
	if (window->x > -1 || window->y > -1) {
		gtk_window_move(GTK_WINDOW(ay), MIN(window->x, gdk_screen_get_width(screen)-20), MIN(window->y, gdk_screen_get_height(screen)-20));
	}
	if (window->width || window->height) {
		gtk_window_resize(GTK_WINDOW(ay), window->width, window->height);
		g_idle_add(resize, window);
	}

	if (window->maximised) {
		gtk_window_maximize(GTK_WINDOW(ay));
	}

	gboolean _song_load (gpointer _)
	{
		bool try_layout (const char* dir, void* user_data)
		{
			GdlDockLayout** layout = user_data;

			g_autofree char* fname = g_strdup_printf("%s/layout.yaml", dir);
			if (gdl_dock_layout_load_from_yaml_file (*layout, fname)) {
				return true;
			}
			return false;
		}

		PF;
		AyyiWindowClass* W = g_type_class_peek(AYYI_TYPE_WINDOW);
		GdlDockLayout* layout = gdl_dock_layout_new(W->master_dock);
		if (app_config_dir_foreach(try_layout, &layout)) {
			typedef struct {
				GArray* windows;
				int w;
			} C;
			C c = {config->windows, 0};

			void _on_load_dock_layout_foreach (GdlDockObject* object, gpointer user_data)
			{
				C* c = user_data;
				if (c->w) {
					ConfigWindow* window = &g_array_index(c->windows, ConfigWindow, c->w);

					if (GDL_IS_DOCK(object)) {
#if 0
						AyyiWindow* ay = shell__lookup_by_dock((GdlDock*)object);
#else
						AyyiWindow* ay = (AyyiWindow*)gtk_widget_get_toplevel((GtkWidget*)object);
#endif
						if (ay) {
							GdkScreen* screen = gdk_screen_get_default();
							gtk_window_move(GTK_WINDOW(ay), MIN(window->x, gdk_screen_get_width(screen)-20), MIN(window->y, gdk_screen_get_height(screen)-20));
							if (window->width && window->height) gtk_window_resize(GTK_WINDOW(ay), window->width, window->height);
						}
					} else pwarn("unexpected object");
				}
				c->w++;
			}
			gdl_dock_master_foreach_toplevel((GdlDockMaster*)((GdlDockObject*)W->master_dock)->master, TRUE, (GFunc) _on_load_dock_layout_foreach, &c);

			gui_song.loaded_layout = (char*)layout_name;
		}

		return G_SOURCE_REMOVE;
	}

	_song_load(NULL);

	//windows__print_dock();
	//gdl_dock_print_recursive((GdlDockMaster*)((GdlDockObject*)app.master_dock)->master);
}


void
song_unload ()
{
	// Set the gui state so that a new fresh song can be loaded.

	if(!SONG_LOADED) return;
	app->state = APP_STATE_ENGINE_OK;

	log_print(0, "unloading song...\n");

	am_song__unload();
}


static void
on_song_unload (GObject* _song, gpointer user_data)
{
	PF;

	transport_disable();

	g_list_free(song->input_list); //remove the input channel label list, etc.
	if(song->input_str) free(song->input_str);
	g_list_free(song->bus_list);
	if(song->bus_strs) free(song->bus_strs);

	//channel strip definitions:
#ifdef DONT_REUSE_CHAN_DATA  //currently, as we only have one song at a time, we reuse the existing striplist.
	if(song->channels) song->channels = (GArray*)g_array_free(song->channels, true); //when TRUE, frees the "actual element data" as well.
#endif

	pool_model_clear();
}


void
song_save ()
{
	g_return_if_fail(strlen(((AyyiSongService*)ayyi.service)->song->path));
	dbg (1, "session_path=%s", ((AyyiSongService*)ayyi.service)->song->path);

	char save_path[256];
	snprintf(save_path, 255, "%.245s/gui_data", ((AyyiSongService*)ayyi.service)->song->path);

	// save song data not supported by the engine into the project folder.
	save_gui_song_file(save_path);

	am_song__save(NULL, NULL);
}


static void
tempo_connect ()
{
	void on_tempo_adj_changed(Observable* tempo, AMVal value, gpointer _arrange)
	{
	}
	observable_subscribe(song->tempo, on_tempo_adj_changed, NULL);
}


/*
 *  If the current song is in the list, move it to the top
 *  If not, move the other songs down the list and put it on top.
 */
void
recent_songs_update ()
{
	const char* current = ayyi_song__get_file_path();
	dbg (2, "current=%s", current);

	// move the songs down the list
	char prev[256];
	for(int i=0;i<MAX_RECENT_SONGS;i++){
		dbg (2, "%i: %s", i, app->recent_songs[i]);
		if(!strcmp(app->recent_songs[i], current)){
			// current song found in list
			if(i) dbg (1, "%i: found! copying: %s here.", i, prev);
			if(i) strcpy(app->recent_songs[i], prev);
			break;
		}

		if(!strlen(app->recent_songs[i])){
			// end of list
			if(i) strcpy(app->recent_songs[i], prev);
			break;
		}

		// current song not yet found in list, so we are moving items downwards.
		strcpy(prev, app->recent_songs[i]);
		strcpy(app->recent_songs[i], prev);
	}
	strcpy(app->recent_songs[0], current);

	menu_update_recent_songs();
}


void
song_get_history ()
{
	// history updates are queued to reduce the sometimes large number of requests.

	static guint history_idle = 0;

	void
	song_get_history_cb(AyyiAction* action)
	{
		if(!action->ret.error){
			g_return_if_fail(action->ret.ptr_1);
			dbg(2, "val=%s", (char*)action->ret.ptr_1);
			menu_update_history((char*)action->ret.ptr_1);
		}
	}

	gboolean
	song_get_history_idle()
	{
		AyyiAction* a  = ayyi_action_new("get history");
		a->op          = AYYI_GET;
		a->obj.type    = AYYI_OBJECT_SONG;
		a->prop        = AYYI_HISTORY;
		a->callback    = song_get_history_cb;
		am_action_execute(a);

		history_idle = 0; // show queue has been cleared
		return false;     // dont run again.
	}

	if(history_idle) return;

	history_idle = g_idle_add((GSourceFunc)song_get_history_idle, NULL);
}


