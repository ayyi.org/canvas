/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include <glib.h>
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#define TYPE_SONG_MAP (song_map_get_type ())
#define SONG_MAP(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SONG_MAP, SongMap))
#define SONG_MAP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SONG_MAP, SongMapClass))
#define IS_SONG_MAP(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SONG_MAP))
#define IS_SONG_MAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SONG_MAP))
#define SONG_MAP_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SONG_MAP, SongMapClass))

typedef struct _SongMapClass SongMapClass;
typedef struct _SongMapPrivate SongMapPrivate;

struct _SongMap {
    GtkScrolledWindow  parent_instance;
    SongMapPrivate*    priv;

    GtkWidget*         canvas;          // the canvas_widget.
    float              height;
    GList*             parts;           // list of GnomeCanvasMap*.
    GnomeCanvasItem*   bg;
};

struct _SongMapClass {
    GtkScrolledWindowClass parent_class;
};

struct _SongMapPrivate {
    gint dummy;
};


GtkWidget* songmap_new            (Arrange*);
void       songmap_set_parts      (Arrange*);
void       songmap_set_part_name  (Arrange*, AMPart*);
void       songmap_set_size       (Arrange*);
void       songmap_redraw         (Arrange*);
void       songmap_print          ();

void       map_part_new           (Arrange*, AMPart*);
