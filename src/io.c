/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __io_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <model/song.h>
#include <model/channel.h>
#include <model/connections.h>
#include <model/track_list.h>
#include "panels/panel.h"
#include "support.h"
#include "io.h"

TreePopup input = {NULL,};
TreePopup output = {NULL,};

static void input_model_new();
static void output_model_new();


static gint
sort_func (GtkTreeModel* model, GtkTreeIter* a, GtkTreeIter* b, gpointer userdata)
{
	// this stops any sorting being done.
	return 0;
}


static void
output_menu_close (GtkWidget* widget)
{
	gtk_widget_hide(output.popup);
}


static void
on_checkbox_toggled (GtkCellRendererToggle* cell, gchar* path_string, gpointer user_data)
{
	PF;
	GtkTreeIter iter;
	if (gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(output.treestore), &iter, path_string)) {
		AyyiIdx connection;
		gtk_tree_model_get(GTK_TREE_MODEL(output.treestore), &iter, COL_INDEX, &connection, -1);
		dbg(0, "index=%i", connection);

		am_track__set_output(song->tracks->track[output.channel->ident.idx], ayyi_song__connection_at(connection), NULL, NULL);
		output_menu_close(NULL);
	}
}


static void
output_model_new ()
{
	if (output.treestore) { perr("!!"); return; }

	output.treestore = gtk_tree_store_new(IO_NUM_COLS, G_TYPE_INT, GDK_TYPE_PIXBUF, G_TYPE_INT/*for checkbox*/, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

	// highlight the name column - we dont actually want any sorting done.
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(output.treestore), IO_COL_NAME, GTK_SORT_ASCENDING);
	gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(output.treestore), IO_COL_NAME, sort_func, NULL, NULL);
}


static void
input_model_new ()
{
	if (input.treestore) { perr("!!"); return; }

	input.treestore = gtk_tree_store_new(IO_NUM_COLS, G_TYPE_INT, GDK_TYPE_PIXBUF, G_TYPE_INT/*for checkbox*/, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

	// highlight the name column - we dont actually want any sorting done.
	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(input.treestore), IO_COL_NAME, GTK_SORT_ASCENDING);
	gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(input.treestore), IO_COL_NAME, sort_func, NULL, NULL);
}


GtkWidget*
output_menu_init ()
{
	// a track output menu using gtktreeview

	input_model_new(); //move
	output_model_new();

	GtkWidget* treeview = output.treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(output.treestore));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), FALSE);
	g_object_ref(treeview);
	gtk_widget_show(treeview);

	// column 0 - icon
	GtkCellRenderer* cell0 = gtk_cell_renderer_pixbuf_new();
	GtkTreeViewColumn* col0 = gtk_tree_view_column_new_with_attributes("", cell0, "pixbuf", COL_ICON, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col0); //pack treeview-column into the treeview
	gtk_tree_view_column_set_fixed_width(col0, 16);

	// checkbox column
	GtkCellRenderer* renderer = gtk_cell_renderer_toggle_new();
	GtkTreeViewColumn* col = gtk_tree_view_column_new_with_attributes("Checkbox", renderer, "active", COL_CHECKBOX, /*"activatable", TRUE,*/ NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col);
	g_signal_connect(renderer, "toggled", (GCallback)on_checkbox_toggled, NULL);

	// column 1
	GtkTreeViewColumn* col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Name");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col1);
	GtkCellRenderer* renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", IO_COL_NAME);
	gtk_tree_view_column_set_sort_column_id(col1, IO_COL_NAME);

	// device name
	col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Device");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col1);
	renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);
	gtk_tree_view_column_add_attribute(col1, renderer1, "text", COL_DEVICE);

	// column2
	GtkTreeViewColumn* col2 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col2, "Port 1");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col2);
	GtkCellRenderer* renderer2 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col2, renderer2, TRUE);
	gtk_tree_view_column_add_attribute(col2, renderer2, "text", COL_PORT1);

	// column3
	GtkTreeViewColumn* col3 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col3, "Port 2");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col3);
	GtkCellRenderer* renderer3 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col3, renderer3, TRUE);
	gtk_tree_view_column_add_attribute(col3, renderer3, "text", COL_PORT2);

	return treeview;
}


void
input_menu_update (AyyiPanel* panel, AMChannel* channel)
{
	input.parent = panel;
	input.channel = channel;

	dbg(3, "clearing treestore... %p", input.treestore);
	gtk_tree_store_clear (input.treestore);

	AyyiTrack* trk = ayyi_song__audio_track_at(channel->ident.idx);

	dbg(0, "ch=%i n_connections=%i", input.channel->ident.idx, ayyi_list__length(trk->input_routing));

	char (*b)[64] = (char (*)[64])g_new0(char, 4 * 64);
	char device[64];
	AyyiConnection* out = NULL;
	GtkTreeIter iter;
	int count = 0;
	while ((out = ayyi_connection__next_output(out, 0))) {
		GdkPixbuf* icon = NULL;
		if (out->nports != channel->nchans) continue;
		if (!ayyi_connection__parse_string(out, b)) { pwarn("failed to parse connection string."); continue; }
		strcpy(device, out->device);
		replace2(device, "_", " ");
		replace2(b[1], "_", " ");
		replace2(b[2], "_", " ");

		gboolean active = ayyi_list__find(trk->input_routing, out->shm_idx) != NULL;

		gtk_tree_store_append (input.treestore, &iter, NULL);
		gtk_tree_store_set (input.treestore, &iter,
		                COL_INDEX, out->shm_idx,
		                COL_ICON, icon,
		                COL_CHECKBOX, active,
		                IO_COL_NAME, b[0],
		                COL_DEVICE, device,
		                COL_PORT1, b[1],
		                COL_PORT2, b[2],
		                -1);
		count++;
	}

	g_free(b);

	dbg (3, "rows added: %i", count);
}


/*
 *  Refresh the contents of the output menu GtkTreeView with a list of the currently
 *  available inputs for the channel output to connect to.
 *  If the input matches the one in AyyiRoute->output_idx, it is ticked.
 */
void
output_menu_update (AyyiPanel* panel, const AMChannel* ch)
{
	output.parent = panel;
	output.channel = (AMChannel*)ch;

	{ dbg(3, "clearing treestore... %p", output.treestore); gtk_tree_store_clear (output.treestore); }

	AyyiTrack* ayyi_trk = ayyi_song__audio_track_at(ch->ident.idx);

	char (*b)[64] = (char (*)[64])g_new0(char, 4 * 64);
	char device[64];
	AyyiConnection* input = NULL;
	GtkTreeIter iter;
	int count = 0;
	while ((input = ayyi_connection__next_input(input, 0))) {
		GdkPixbuf* icon = NULL;
		dbg(2, "  c->nports=%i route=%i %s", input->nports, ch->nchans, input->name);
		if (input->nports != ch->nchans) continue;
		if (!ayyi_connection__parse_string(input, b)) { pwarn("failed to parse connection string."); continue; }
		strcpy(device, input->device);
		replace2(device, "_", " ");
		replace2(b[1], "_", " ");
		replace2(b[2], "_", " ");

		gboolean active = ayyi_list__find(ayyi_trk->output_routing, input->shm_idx) != NULL;
		if (active) dbg(2, "active connection! idx=%i", input->shm_idx);

		gtk_tree_store_append (output.treestore, &iter, NULL);
		gtk_tree_store_set (output.treestore, &iter,
		                COL_INDEX, input->shm_idx,
		                COL_ICON, icon,
		                COL_CHECKBOX, active,
		                IO_COL_NAME, b[0],
		                COL_DEVICE, device,
		                COL_PORT1, b[1],
		                COL_PORT2, b[2],
		                -1);
		count++;
	}

	g_free(b);

	dbg (3, "rows added: %i", count);
}


static gboolean
output_menu_key_press_event (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
	PF;
	if (event->keyval == GDK_Escape ||
	  event->keyval == GDK_Tab ||
	  event->keyval == GDK_KP_Tab ||
	  event->keyval == GDK_ISO_Left_Tab)
	  {
		output_menu_close(widget);
		return TRUE;
	}

	return FALSE;
}


static void
output_menu_set_position ()
{
	gint x = 0, y = 0;
	gdk_window_get_pointer(((GtkWidget*)output.parent->window)->window, &x, &y, NULL);
	gint rootx, rooty;
	gdk_window_get_origin(((GtkWidget*)output.parent->window)->window, &rootx, &rooty);

	gtk_window_move(GTK_WINDOW(output.popup), x + rootx, y + rooty);
}


void
output_menu_popup ()
{
	PF;
	if(!output.popup){
		output.popup = gtk_window_new(GTK_WINDOW_POPUP);
		gtk_window_set_modal(GTK_WINDOW(output.popup), TRUE);

		gtk_container_add(GTK_CONTAINER(output.popup), output.treeview);
		gtk_widget_show(output.treeview);
	}

	g_signal_connect((gpointer)output.popup, "focus-out-event", G_CALLBACK(output_menu_close), NULL);
	gtk_widget_show(output.popup);
	output_menu_set_position();

	g_signal_connect(output.popup, "key_press_event", G_CALLBACK(output_menu_key_press_event), NULL);

	gboolean _on_button_press(GtkWidget* widget, GdkEventButton* event, gpointer data)
	{
		PF;
		output_menu_close(NULL);
		return NOT_HANDLED;
	}
	g_signal_connect(output.popup, "button-press-event", G_CALLBACK(_on_button_press), NULL);

	//this doesnt make any difference?:
	gtk_widget_grab_focus(output.treeview);
}


