/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __transport_c__
#include "global.h"
#include "model/transport.h"
#include "panels/transport.h"
#include "model/song.h"
#include "widgets/svgtogglebutton.h"
#include "windows.h"
#include "window.statusbar.h"
#include "icon.h"
#include "toolbar.h"
#include "transport.h"

GArray* tr_array = NULL;

static void transport_on_sync        (GObject*, gpointer);
static void transport_on_stop        (GObject*, gpointer);
static void transport_on_play        (GObject*, gpointer);
static void transport_on_rew         (GObject*, gpointer);
static void transport_on_ff          (GObject*, gpointer);
static void transport_on_cycle       (GObject*, gpointer);
static void transport_on_rec         (GObject*, bool enabled, gpointer);

static bool transport_rec            (GtkWidget*, gpointer);
static bool transport_rew            (GtkWidget*, gpointer);
static bool transport_play           (GtkWidget*, gpointer);
static bool transport_stop           (GtkWidget*, gpointer);
static void transport_ff             (GtkWidget*, gpointer);
static void transport_cycle_toggle   (GtkWidget*, gpointer);

static bool transport_button_enter   (GtkWidget*, GdkEventCrossing*, gpointer);


void
transport__init ()
{
	int i = 0;

	tr_array = g_array_sized_new(ZERO_TERMINATED, FALSE, sizeof(TransportButton), MAX_TRANSPORT_BUT);

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "Record",
		.callback  = G_CALLBACK(transport_rec),
		.active    = ayyi_transport_is_rec_enabled,
		.stock_id  = "record",
		.mouseover= transport_button_enter
	};

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "Rewind",
		.callback  = G_CALLBACK(transport_rew),
		.active    = ayyi_transport_is_rew,
		.stock_id  = "tr-rew",
		.mouseover= transport_button_enter,
	};

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "Stop",
		.callback  = G_CALLBACK(transport_stop),
		.active    = ayyi_transport_is_stopped,
		.stock_id  = "tr-stop",
		.mouseover = transport_button_enter,
	};

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "Play",
		.callback  = G_CALLBACK(transport_play),
		.active    = ayyi_transport_is_playing,
		.stock_id  = "tr-play",
		.mouseover = transport_button_enter,
	};

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "ff",
		.callback  = G_CALLBACK(transport_ff),
		.active    = ayyi_transport_is_ff,
		.stock_id  = "tr-ff",
		.mouseover = transport_button_enter,
	};

	g_array_index(tr_array, TransportButton, i++) = (TransportButton){
		.desc      = "cycle",
		.callback  = G_CALLBACK(transport_cycle_toggle),
		.active    = ayyi_transport_is_cycling,
		.stock_id  = "refresh",
		.mouseover = transport_button_enter,
	};

	g_array_set_size(tr_array, i);

	am_song__connect("transport-sync",  G_CALLBACK(transport_on_sync), NULL);
	am_song__connect("transport-stop",  G_CALLBACK(transport_on_stop), NULL);
	am_song__connect("transport-play",  G_CALLBACK(transport_on_play), NULL);
	am_song__connect("transport-rew",   G_CALLBACK(transport_on_rew), NULL);
	am_song__connect("transport-ff",    G_CALLBACK(transport_on_ff), NULL);
	am_song__connect("transport-cycle", G_CALLBACK(transport_on_cycle), NULL);
	am_song__connect("transport-rec",   G_CALLBACK(transport_on_rec), NULL);
}


static void
transport_on_sync (GObject* _song, gpointer user_data)
{
	// sets the correct state of all open transport windows following aquisition of rectran_shm, or a general change in transport state.

	if(ayyi_transport_is_rec_enabled())  transport_on_rec(NULL, TRUE, NULL);
}


static void
transport_on_stop (GObject* _song, gpointer user_data)
{
	// currently we leave the spp timer running for meter decay.

	transport_clear_buttons();

	transport_foreach {
		if(transport->b[TR_STOP]) svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_STOP], true);
	} end_transport_foreach;

	request_toolbar_update();
}


static void
transport_on_play (GObject* _song, gpointer user_data)
{
	transport_clear_buttons();

	request_toolbar_update();//FIXME we should possibly make this transport_update instead to include tr windows.
}


static void
transport_on_rew (GObject* _song, gpointer user_data)
{
	PF;
	transport_clear_buttons();
	request_toolbar_update();

	transport_foreach {
		svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_REW], true);
	} end_transport_foreach;
}


static void
transport_on_ff (GObject* _song, gpointer user_data)
{
	PF;

	transport_foreach {
		svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_FF], true);
	} end_transport_foreach;

	request_toolbar_update();
}


static void
transport_on_cycle (GObject* _song, gpointer user_data)
{
	PF;
	request_toolbar_update();
}


static void
transport_on_rec (GObject* _song, bool enabled, gpointer user_data)
{
	//currently everything neccesary is updated in transport_play_cb or transport_stop_cb
	PF;
}


void
transport_enable ()
{
	if(_debug_ && song->periodic) pwarn ("timer already started.");

	am_song__enable_meters(true);

	g_signal_emit_by_name (app, "app-start-meters");

	am_transport_sync();
}


void
transport_disable()
{
	// stop spp timer

	if(!song->periodic){ dbg(2, "timer not running?"); return; }

	am_song__enable_meters(false);
	g_signal_emit_by_name (app, "app-stop-meters", NULL);
}


static bool
transport_rec (GtkWidget* widget, gpointer user_data)
{
	printf("%s%s%s()...\n", ayyi_red, __func__, white);
	ayyi_transport_rec();
	return true;
}


static bool
transport_rew (GtkWidget* widget, gpointer user_data)
{
	PF;
	ayyi_transport_rew();
	return true;
}


static bool
transport_stop (GtkWidget* widget, gpointer not_used)
{
	ayyi_transport_stop();
	return true;
}


static bool
transport_play (GtkWidget* widget, gpointer user_data)
{
	// this fn may be called from a button press or a keyboard shortcut.

	ayyi_transport_play();

	return true; // stop further propogation of the signal - needed when using keyboard shortcut.
}


static void
transport_ff (GtkWidget* widget, gpointer user_data)
{
	PF;

	ayyi_transport_ff();

	//TODO check these are handled by the observer
	transport_clear_buttons();
	svg_toggle_button_set_active((SvgToggleButton*)widget, TRUE);

	//cant get Ardour to notify us when in FF
	am_song__emit("transport-ff");
}


static void
transport_cycle_toggle (GtkWidget* widget, gpointer user_data)
{
	PF;
	am_transport_cycle_toggle();
}


static bool
transport_button_enter (GtkWidget* widget, GdkEventCrossing* event, gpointer _data)
{
	struct _cb_data {
		AyyiPanel* panel;
		TransportButton* button;
	};
	struct _cb_data* data = (struct _cb_data*)_data;
	AyyiPanel* panel = data->panel;
	if(panel->window){
		shell__statusbar_print(panel->window, 2, data->button->desc);
	}
	return NOT_HANDLED;
}


