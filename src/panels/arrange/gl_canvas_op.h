/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2010-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "canvas_op.h"

typedef struct
{
    CanvasOp     canvas;
    Pti          event;
    AMTrack*     original_track;
    Rectangle    selection_box;
    AGlActor*    ghost;          // used in Move operations

} GlCanvasOp;

CanvasOp* gl_canvas_op__new (Arrange*, Optype);
Optype    get_optype        (Arrange*, FeatureType, guint button_state);

