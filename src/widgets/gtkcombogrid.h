/*
 * GtkComboGrid widget for GTK+
 *
 * Copyright (C) 2006 Andrea Zagli <azagli@inwind.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_COMBO_GRID_H__
#define __GTK_COMBO_GRID_H__

#include <gtk/gtkhbox.h>
#include <gtk/gtktreemodel.h>
#include <gtk/gtktreeviewcolumn.h>


G_BEGIN_DECLS


#define GTK_TYPE_COMBO_GRID            (gtk_combo_grid_get_type ())
#define GTK_COMBO_GRID(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_COMBO_GRID, GtkComboGrid))
#define GTK_COMBO_GRID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_COMBO_GRID, GtkComboGridClass))
#define GTK_IS_COMBO_GRID(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_COMBO_GRID))
#define GTK_IS_COMBO_GRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_COMBO_GRID))
#define GTK_COMBO_GRID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_COMBO_GRID, GtkComboGridClass))


typedef struct _GtkComboGrid GtkComboGrid;
typedef struct _GtkComboGridClass GtkComboGridClass;
typedef struct _GtkComboGridPrivate GtkComboGridPrivate;


struct _GtkComboGrid
{
  GtkHBox hbox;
  GtkComboGridPrivate* priv;
};

struct _GtkComboGridClass
{
  GtkHBoxClass parent_class;
};


GType        gtk_combo_grid_get_type            (void) G_GNUC_CONST;

GtkWidget*   gtk_combo_grid_new                 (void);
GtkWidget*   gtk_combo_grid_get_entry           (GtkComboGrid*);
const gchar* gtk_combo_grid_get_text            (GtkComboGrid*);
void         gtk_combo_grid_set_text            (GtkComboGrid*, const char*);
void         gtk_combo_grid_set_editable        (GtkComboGrid*, gboolean is_editable);
void         gtk_combo_grid_set_text_column     (GtkComboGrid*, gint text_column);
void         gtk_combo_grid_set_headers_visible (GtkComboGrid*, gboolean headers_visible);
void         gtk_combo_grid_set_model           (GtkComboGrid*, GtkTreeModel*);
void         gtk_combo_grid_set_model_updater   (GtkComboGrid*, gpointer func, gpointer);
void         gtk_combo_grid_set_height          (GtkComboGrid*, int popup_height);
void         gtk_combo_grid_append_column       (GtkComboGrid*, GtkTreeViewColumn *column);
gboolean     gtk_combo_grid_get_active_iter     (GtkComboGrid*, GtkTreeIter *iter);

G_END_DECLS


#endif /* __GTK_COMBO_GRID_H__ */
