/* renderdemo.c: Common code for rendering demos
 *
 * Copyright (C) 1999, 2004 Red Hat Software
 * Copyright (C) 2001 Sun Microsystems
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define DEFAULT_FONT_FAMILY "Sans"
#define DEFAULT_FONT_SIZE 18

#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <pango/pango.h>
#include <pango/pangoft2.h>

typedef struct _glyph Glyph;
#include "image.h"
#include "text_render.h"

#define _MAKE_FONT_NAME(family, size) family " " #size
#define MAKE_FONT_NAME(family, size) _MAKE_FONT_NAME(family, size)

int      opt_dpi = 96;
char*    opt_font = MAKE_FONT_NAME (DEFAULT_FONT_FAMILY, DEFAULT_FONT_SIZE);
char*    opt_output = NULL;
int      opt_margin = 0;
int      opt_markup = FALSE;
gboolean opt_rtl = FALSE;
int      opt_rotate = 0;
gboolean opt_auto_dir = TRUE;
gboolean opt_waterfall = TRUE;//FALSE;
int      opt_width = -1;
int      opt_indent = 0;
PangoEllipsizeMode opt_ellipsize = PANGO_ELLIPSIZE_NONE;
HintMode opt_hinting = HINT_DEFAULT;


static void
ft2_render (PangoLayout *layout, int x, int y, gpointer data)
{
	pango_ft2_render_layout(data, layout, x, y);
}


static void
set_pixel(GdkPixbuf* pixbuf, int p, int val)
{
	//only for debugging

	guchar* pixels = (guchar*)gdk_pixbuf_get_pixels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	if (p > rowstride * height) { printf("%s(): p! %i\n", __func__, p); return; }
	pixels[p] = val;
}


void
copy_ftbitmap_to_pixbuf(FT_Bitmap* bitmap, GdkPixbuf* pixbuf)
{
	int x, y;
	int width = gdk_pixbuf_get_width(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	//printf("%s(): rowstride=%i pitch=%i\n", __func__, rowstride, bitmap.pitch);
	int n = 0;
	for(x=0;x<width;x++){
		for(y=0;y<height;y++){
			n++;
			int p = y*rowstride + 3*x;
			//pixels[p  ] = bitmap.buffer + y * bitmap.pitch + x;
			//pixels[p+1] = bitmap.buffer + y * bitmap.pitch + x;
			//pixels[p+2] = bitmap.buffer + y * bitmap.pitch + x;
			set_pixel(pixbuf, p,   *(bitmap->buffer + y * bitmap->pitch + x));
			set_pixel(pixbuf, p+1, *(bitmap->buffer + y * bitmap->pitch + x));
			set_pixel(pixbuf, p+2, *(bitmap->buffer + y * bitmap->pitch + x));
		}
	}
}


void
render_text_fx(const char* text, Glyph* glyph, int* fx_width, PangoFontDescription* fontdesc)
{
	//render the @text to the @glyph pixbuf and apply an effect.

	GdkPixbuf* pixbuf = glyph->pixbuf;

	PangoFontMap* fontmap = pango_ft2_font_map_new();
	PangoContext* context = pango_ft2_font_map_create_context (PANGO_FT2_FONT_MAP (fontmap));
	g_object_unref(fontmap);

	int width, height;
	//do_output (context, fontdesc, text, NULL, NULL, NULL, &width, &height);

	width = gdk_pixbuf_get_width(pixbuf); //we totally ignore width got from above - is the above call still needed?
	height = gdk_pixbuf_get_height(pixbuf);

	//FT_Bitmap is b&w or greyscale. see: freetype2/freetype/ftimage.h
	FT_Bitmap bitmap;
	bitmap.width = width;
	bitmap.pitch = (bitmap.width + 3) & ~3;
	bitmap.pitch = 256; //FIXME made too big to stop gandal complaining - which sizes can gandalf accept? x8 ? x256?
	bitmap.rows  = height;
	guchar* buf  = bitmap.buffer = g_malloc (bitmap.pitch * bitmap.rows);
	bitmap.num_grays = 256;
	bitmap.pixel_mode = ft_pixel_mode_grays;
	memset (buf, 0x00, bitmap.pitch * bitmap.rows);
	//printf("%s(): *** width=%i height=%i pitch=%i\n", __func__, width, height, bitmap.pitch);

	//do_output (context, fontdesc, text, ft2_render, NULL, &bitmap, &width, &height);
	do_output (context, fontdesc, text, ft2_render, NULL, &bitmap, NULL, NULL);
	g_object_unref(context);
	render_finalize();

	bloom(&bitmap, fx_width);

	//copy the greyscale FT_Bitmap to 24bit GdkPixbuf:
	copy_ftbitmap_to_pixbuf(&bitmap, pixbuf);
	//printf("%s(): pixels copied: %i\n", __func__, n);

	g_free(bitmap.buffer);
}


void
text_get_max_char_size(const char* charlist, PangoFontDescription* font_desc, int* width, int* height)
{
	//it would be nice to use this if we could be sure a window had been created
	//PangoLayout* p_layout = gtk_widget_create_pango_layout(GTK_WIDGET(widget), value_str);

	// if the widget is already realised, an alternative is:
	//   PangoLayout* layout = gtk_widget_create_pango_layout (widget, "ABC012456789");
	//   pango_layout_get_extents (layout, &ink_rect, &logical_rect);
	
	int text_width = 0, text_height = 0;

	PangoContext* context = gdk_pango_context_get_for_screen(gdk_screen_get_default());
	PangoLayout* p_layout = pango_layout_new(context);

	pango_layout_set_font_description(p_layout, font_desc);
	int len = strlen(charlist);
	int i; for(i=0;i<len;i++){
		pango_layout_set_text(p_layout, &charlist[i], 1);

		#define GIVE_PANGO_A_BIT_EXTRA_HEIGHT 4
		#define GIVE_PANGO_A_BIT_EXTRA_WIDTH 2
		PangoRectangle ink_rect, logical_rect;
		pango_layout_get_pixel_extents(p_layout, &ink_rect, &logical_rect);
		text_width = MAX(text_width, ink_rect.width);
		text_height = MAX(text_height, ink_rect.height);
		//printf("%s(): width=%i\n", __func__, text_width);
	}
	g_object_unref(p_layout);
	*width = text_width + GIVE_PANGO_A_BIT_EXTRA_WIDTH;
	*height = text_height + GIVE_PANGO_A_BIT_EXTRA_HEIGHT;
}


void
render_fail (const char *format, ...)
{
  const char *msg;
  
  va_list vap;
  va_start (vap, format);
  msg = g_strdup_vprintf (format, vap);
  g_printerr ("%s\n", msg);
  
  exit (1);
}


PangoFontDescription *
get_font_description (void)
{
  PangoFontDescription *font_description = pango_font_description_from_string (opt_font);
  
  if ((pango_font_description_get_set_fields (font_description) & PANGO_FONT_MASK_FAMILY) == 0)
    pango_font_description_set_family (font_description, DEFAULT_FONT_FAMILY);

  if ((pango_font_description_get_set_fields (font_description) & PANGO_FONT_MASK_SIZE) == 0)
    pango_font_description_set_size (font_description, DEFAULT_FONT_SIZE * PANGO_SCALE);

  return font_description;
}


static PangoLayout *
make_layout(PangoContext *context, PangoFontDescription* font_desc, const char *text)
{
  //printf("%s(): text=%s\n", __func__, text);

  PangoLayout *layout = pango_layout_new (context);
  if (opt_markup)
    pango_layout_set_markup (layout, text, -1);
  else
    pango_layout_set_text (layout, text, -1);

  pango_layout_set_auto_dir (layout, opt_auto_dir);
  pango_layout_set_ellipsize (layout, opt_ellipsize);

  /*
  PangoDirection base_dir;

  static PangoFontDescription *font_description;
  font_description = get_font_description ();
  if (size > 0)
    pango_font_description_set_size (font_description, size * PANGO_SCALE);
    
  if (opt_width > 0)
    pango_layout_set_width (layout, (opt_width * opt_dpi * PANGO_SCALE + 32) / 72);

  if (opt_indent != 0)
    pango_layout_set_indent (layout, (opt_indent * opt_dpi * PANGO_SCALE + 32) / 72);

  base_dir = pango_context_get_base_dir (context);
  pango_layout_set_alignment (layout, base_dir == PANGO_DIRECTION_LTR ? PANGO_ALIGN_LEFT : PANGO_ALIGN_RIGHT);
  */
  
  pango_layout_set_font_description (layout, font_desc);

  //pango_font_description_free (font_description);

  return layout;
}

gchar *
get_options_string (void)
{
  PangoFontDescription *font_description = get_font_description ();

  if (opt_waterfall) pango_font_description_unset_fields (font_description, PANGO_FONT_MASK_SIZE);

  gchar* font_name = pango_font_description_to_string (font_description);
  gchar* result = g_strdup_printf ("%s: dpi=%d", font_name, opt_dpi);
  pango_font_description_free (font_description);
  g_free (font_name);

  return result;
}

static void
transform_point (PangoMatrix *matrix, double x_in, double y_in, double *x_out, double *y_out)
{
  *x_out = x_in * matrix->xx + y_in * matrix->xy + matrix->x0;
  *y_out = x_in * matrix->yx + y_in * matrix->yy + matrix->y0;
}

static void
output_body (PangoContext *context, PangoFontDescription* font_desc, const char *text, RenderCallback render_cb, gpointer cb_data, int *width, int *height)
{
	//printf("%s()... text=%s\n", __func__, text);

	PangoRectangle logical_rect;
	//*width = 0;
	//*height = 0;

	PangoLayout *layout = make_layout (context, font_desc, text);
	pango_layout_get_extents (layout, NULL, &logical_rect);
  
	*width = PANGO_PIXELS (logical_rect.width);
	*height = PANGO_PIXELS (logical_rect.height);

	if (render_cb) (*render_cb) (layout, 0, 0, cb_data);

	g_object_unref (layout);
}


static void
set_transform (PangoContext *context, TransformCallback transform_cb, gpointer cb_data, PangoMatrix *matrix)
{
  if (transform_cb)
    (*transform_cb) (context, matrix, cb_data);
  else
    pango_context_set_matrix (context, matrix);
}


void
do_output (PangoContext *context, PangoFontDescription* fontdesc, const char* text, RenderCallback render_cb, TransformCallback transform_cb, gpointer cb_data, int *width_out, int *height_out)
{
  //printf("%s(): text=%s\n", __func__, text);

  PangoMatrix matrix = PANGO_MATRIX_INIT;
  int x = opt_margin;
  int y = opt_margin;
  
  int width = 0;
  int height = 0;

  set_transform (context, transform_cb, cb_data, NULL);
  
  pango_context_set_language (context, pango_language_from_string ("en_US"));
  pango_context_set_base_dir (context, opt_rtl ? PANGO_DIRECTION_RTL : PANGO_DIRECTION_LTR);

#if 0
  PangoLayout *layout;
  PangoRectangle logical_rect;
  if (opt_header)
    {
      char *options_string = get_options_string ();
      layout = make_layout (context, options_string, 10);
      pango_layout_get_extents (layout, NULL, &logical_rect);

      width = MAX (width, PANGO_PIXELS (logical_rect.width));
      height += PANGO_PIXELS (logical_rect.height);

      if (render_cb) (*render_cb) (layout, x, y, cb_data);

      y += PANGO_PIXELS (logical_rect.height);

      g_object_unref (layout);
      g_free (options_string);
    }
#endif

  pango_matrix_rotate (&matrix, opt_rotate);

  set_transform (context, transform_cb, cb_data, &matrix);

  int rotated_width, rotated_height;
  output_body (context, fontdesc, text, NULL, NULL, &rotated_width, &rotated_height);
  //printf("%s(): size=%i x %i\n", __func__, rotated_width, rotated_height);

  double p1x, p1y;
  double p2x, p2y;
  double p3x, p3y;
  double p4x, p4y;
  double minx, miny;
  double maxx, maxy;
  transform_point (&matrix, 0,             0,              &p1x, &p1y);
  transform_point (&matrix, rotated_width, 0,              &p2x, &p2y);
  transform_point (&matrix, rotated_width, rotated_height, &p3x, &p3y);
  transform_point (&matrix, 0,             rotated_height, &p4x, &p4y);

  minx = MIN (MIN (p1x, p2x), MIN (p3x, p4x));
  miny = MIN (MIN (p1y, p2y), MIN (p3y, p4y));

  maxx = MAX (MAX (p1x, p2x), MAX (p3x, p4x));
  maxy = MAX (MAX (p1y, p2y), MAX (p3y, p4y));

  matrix.x0 = x - minx;
  matrix.y0 = y - miny;

  set_transform (context, transform_cb, cb_data, &matrix);

  if (render_cb) output_body (context, fontdesc, text, render_cb, cb_data, &rotated_width, &rotated_height);

  width = MAX (width, maxx - minx);
  height += maxy - miny;

  width += 2 * opt_margin;
  height += 2 * opt_margin;

  if (width_out) *width_out = width;
  if (height_out) *height_out = height;
}


/* This function gets called to convert a matched pattern into what
 * we'll use to actually load the font. We turn off hinting since we
 * want metrics that are independent of scale.
 */
void
fc_substitute_func (FcPattern *pattern, gpointer   data)
{
  if (opt_hinting != HINT_DEFAULT)
    {
      FcPatternDel (pattern, FC_HINTING);
      FcPatternAddBool (pattern, FC_HINTING, opt_hinting != HINT_NONE);
      
      FcPatternDel (pattern, FC_AUTOHINT);
      FcPatternAddBool (pattern, FC_AUTOHINT, opt_hinting == HINT_AUTO);
    }
}

void
parse_ellipsis (/*ArgContext *arg_context, */const char *name, const char *arg, gpointer    data)
{
  static GEnumClass *class = NULL;

  if (!class) class = g_type_class_ref (PANGO_TYPE_ELLIPSIZE_MODE);
  
  GEnumValue *value = g_enum_get_value_by_nick (class, arg);
  if (!value) render_fail ("--ellipsize option must be one of none/start/middle/end");

  opt_ellipsize = value->value;
}

void
parse_hinting (/*ArgContext *arg_context, */const char *name, const char *arg, gpointer    data)
{
  static GEnumClass *class = NULL;

  if (!class)
    class = g_type_class_ref (PANGO_TYPE_ELLIPSIZE_MODE);

  if (strcmp (arg, "none") == 0)
    opt_hinting = HINT_NONE;
  else if (strcmp (arg, "auto") == 0)
    opt_hinting = HINT_AUTO;
  else if (strcmp (arg, "full") == 0)
    opt_hinting = HINT_FULL;
  else
    render_fail ("--hinting option must be one of none/auto/full");
}

/*
void
parse_options (int argc, char *argv[])
{
  static const ArgDesc args[] = {
    { "no-auto-dir","Don't set layout direction according to contents",
      ARG_NOBOOL,   &opt_auto_dir },
    { "dpi",        "Set the dpi'",
      ARG_INT,      &opt_dpi },
    { "ellipsize",  "Ellipsization mode [=none/start/middle/end]",
      ARG_CALLBACK, NULL, parse_ellipsis },
    { "font",       "Set the font name",
      ARG_STRING,   &opt_font },
    { "header",     "Display the options in the output",
      ARG_BOOL,     &opt_header },
    { "help",       "Show this output",
      ARG_CALLBACK, NULL, show_help, },
    { "hinting",    "Hinting style [=none/auto/full]",
      ARG_CALLBACK, NULL, parse_hinting, },
    { "margin",     "Set the margin on the output in pixels",
      ARG_INT,      &opt_margin },
    { "markup",     "Interpret contents as Pango markup",
      ARG_BOOL,     &opt_markup },
    { "output",     "Name of output file",
      ARG_STRING,   &opt_output },
    { "rtl",        "Set base dir to RTL",
      ARG_BOOL,     &opt_rtl },
    { "rotate",     "Angle at which to rotate results",
      ARG_INT,      &opt_rotate },
    { "waterfall",  "Create a waterfall display",
      ARG_BOOL,     &opt_waterfall },
    { "width",      "Width in points to which to wrap output",
      ARG_INT,      &opt_width },
    { "indent",     "Width in points to indent paragraphs",
      ARG_INT,      &opt_indent },
    { NULL }
  };

  ArgContext *arg_context;
  GError *error = NULL;
  size_t len;
  char *p;

  arg_context = arg_context_new (NULL);
  arg_context_add_table (arg_context, args);

  if (!arg_context_parse (arg_context, &argc, &argv, &error))
    fail ("%s", error->message);

  arg_context_free (arg_context);
  
  // Strip trailing whitespace
  p = text + len;
  while (p > text)
    {
      gunichar ch;
      p = g_utf8_prev_char (p);
      ch = g_utf8_get_char (p);
      if (!g_unichar_isspace (ch))
	break;
      else
	*p = '\0';
    }

  // Make sure we have valid markup
  if (opt_markup &&
      !pango_parse_markup (text, -1, 0, NULL, NULL, NULL, &error))
    fail ("Cannot parse input as markup: %s", error->message);
}
*/

void
render_finalize (void)
{
}
