/* renderdemo.c: Common code for rendering demos
 *
 * Copyright (C) 1999, 2004 Red Hat Software
 * Copyright (C) 2001 Sun Microsystems
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <pango/pango-layout.h>
#include <pango/pangofc-fontmap.h>

typedef enum {
  HINT_DEFAULT,
  HINT_NONE,
  HINT_AUTO,
  HINT_FULL
} HintMode;

struct _glyph
{
  GdkPixbuf*     pixbuf;
  GdkRectangle*  rect;   //size and position of the text char. There is a left and right margin outside of this for fx.
};

struct _ImageCache
{
	Glyph*  text_buf[11];
	int     kernel_size;
};

typedef void (*RenderCallback)    (PangoLayout*, int x, int y, gpointer data);
typedef void (*TransformCallback) (PangoContext*, PangoMatrix *transform, gpointer data);

void   render_text_fx         (const char* text, struct _glyph*, int* fx_width, PangoFontDescription*);
void   text_get_max_char_size (const char* charlist, PangoFontDescription*, int* width, int* height);
void   render_fail            (const char *format, ...) G_GNUC_PRINTF (1, 2);
void   do_output              (PangoContext*, PangoFontDescription*, const char* text, RenderCallback, TransformCallback transform_cb, gpointer cb_data, int *width, int *height);
void   render_finalize        (void);
void   fc_substitute_func     (FcPattern *pattern, gpointer data);

void   copy_ftbitmap_to_pixbuf(FT_Bitmap*, GdkPixbuf*);

extern PangoEllipsizeMode opt_ellipsize;
extern HintMode opt_hinting;

