/*
  copyright (C) 2012-2021 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

uniform vec2 modelview;
uniform vec2 translate;

uniform float width;
uniform float height;
uniform float selected;
uniform float frame;
uniform vec4 colour;
uniform vec4 border_colour;

varying vec2 position;

void main (void)
{
	// For coordinate calculations to work the quad must be positioned using glTranslatef(), with x1=0, y1=0.

	// The quad is oversized by 2 px to allow for selection fx.

	// Possible states: outside, outer border edge, outer border (dark), inner border (light), inside.

	vec4 c = colour;
	c[3] = 1.0 - (min(2.0, position.x / 512.0 + (position.y / (height * 2.0)))) / 2.0;

	float from_left = position.x;
	float from_right = width + 4.0 - position.x;
	float from_top = position.y;
	float from_bot = height + 4.0 - position.y;

	const float b0 = 1.0; // start of outer border edge
	const float bo = 2.0; // start of outer border
	const float bi = 3.0; // start of inner border
	const float b2 = 5.0; // end of border

	bool is_inside = from_left > b2 && from_right > b2 && from_top > b2 && from_bot > b2;
	if(is_inside){
		float convex = (0.5 - position.y / height) * 0.5;
		//c[0] += convex;
		//c[1] += convex;
		//c[2] += convex;
		//c[0] = min(1.0, c[0] + convex);
		//c[1] = min(1.0, c[1] + convex);
		//c[2] = min(1.0, c[2] + convex);
		//c[3] = max(0.5, c[3]);
		gl_FragColor = c;// + vec4(convex, convex, convex, 1.0);

	}else{
		if(from_left < b0 || from_right < b0 || from_top < b0 || from_bot < b0){
			// outside
			// selection animation originally extended out here, but it looks better without.
			//gl_FragColor = (selected > 0.5 && selected == 3.0)? vec4(1.0, 1.0, 1.0, 0.5) : vec4(0.0);
			gl_FragColor = vec4(0.0);
		}else{
			if(from_left < bo || from_right < bo || from_top < bo || from_bot < bo){ // outer border edge / shadow
				if(selected > 0.5 && frame > 1.9 && frame < 4.1){
					gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
				}else{
					vec4 border_colour_ = (border_colour + colour) / 2.0;
					border_colour_[3] = border_colour[3] / 3.0;
					gl_FragColor = border_colour_;
				}
			}else{
				bool is_border_o = from_left < bi || from_right < bi || from_top < bi || from_bot < bi;
				if(is_border_o){ // outer border (dark)
					vec4 border_colour_ = selected > 0.5
						? vec4(1.0, 1.0, 1.0, 1.0)
						: (border_colour * position.y / height + 0.5 * colour * (1.0 - position.y / height)) / 1.0;
					//border_colour_[3] = border_colour[3];
					gl_FragColor = border_colour_;
				}else{
					// inside border
					gl_FragColor = (c + vec4(1.0, 1.0, 1.0, c[3])) / 2.0;
				}
			}
		}
	}
}

