/***************************************************************************
 *            gtkvumeter.h
 *
 *  Fri Jan 10 20:06:41 2003
 *  Copyright  2003  Todd Goyen
 *  wettoad@knighthoodofbuh.org
 *  Source code is LGPL'd,
 *  but may be distributed under any other open source license
 ****************************************************************************/

#ifndef __GTKVUMETER_H__
#define __GTKVUMETER_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_TYPE_VUMETER                (gtk_vumeter_get_type ())
#define GTK_VUMETER(obj)                (GTK_CHECK_CAST ((obj), GTK_TYPE_VUMETER, GtkVUMeter))
#define GTK_VUMETER_CLASS(klass)        (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_VUMETER GtkVUMeterClass))
#define GTK_IS_VUMETER(obj)             (GTK_CHECK_TYPE ((obj), GTK_TYPE_VUMETER))
#define GTK_IS_VUMETER_CLASS(klass)     (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_VUMETER))
#define GTK_VUMETER_GET_CLASS(obj)      (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_VUMETER, GtkVUMeterClass))

#define GTK_VUMETER_USE_PEAKS TRUE

typedef struct _GtkVUMeter      GtkVUMeter;
typedef struct _GtkVUMeterClass GtkVUMeterClass;

typedef enum {
    GTK_VUMETER_PEAKS_FALLOFF_SLOW,
    GTK_VUMETER_PEAKS_FALLOFF_MEDIUM,
    GTK_VUMETER_PEAKS_FALLOFF_FAST,
    GTK_VUMETER_PEAKS_FALLOFF_USER,
    GTK_VUMETER_PEAKS_FALLOFF_LAST    
} GtkVUMeterPeaksFalloff;

typedef enum {
    GTK_VUMETER_SCALE_LINEAR,
    GTK_VUMETER_SCALE_LOG,
    GTK_VUMETER_SCALE_LAST    
} GtkVUMeterScale;

struct _GtkVUMeter {
    GtkWidget   widget;
    
    GdkColormap *colormap;
    gint        colors;
    
    GdkGC       **f_gc;
    GdkGC       **b_gc;
    GdkColor    *f_colors;
    GdkColor    *b_colors;
    
    gboolean    vertical;
    gint        level;
    gint        min;
    gint        max;

    gboolean    peaks;
    gint        peak_level;
    gint        rate;
    
    GtkVUMeterScale scale;
    gboolean     scale_inverted;

    /* size provides the fixed geometric dimension */
    /* The width on a vertical scale */
    /* The height on a horizontal scale */
    gint         size;

	guint        timer;
};

struct _GtkVUMeterClass {
    GtkWidgetClass  parent_class;
};

GtkType    gtk_vumeter_get_type (void) G_GNUC_CONST;
GtkWidget *gtk_vumeter_new (gboolean vertical, gboolean peaks, guint redraw_rate);

void gtk_vumeter_set_min_max (GtkVUMeter *vumeter, gint *min, gint *max);
void gtk_vumeter_set_level (GtkVUMeter *vumeter, gint level);

void gtk_vumeter_set_peaks_falloff (GtkVUMeter *vumeter, GtkVUMeterPeaksFalloff peaks_falloff, guint user_rate);
void gtk_vumeter_set_peaks (GtkVUMeter *vumeter, gboolean peaks);

void gtk_vumeter_set_scale (GtkVUMeter *vumeter, GtkVUMeterScale scale);
void gtk_vumeter_set_scale_inverted (GtkVUMeter *vumeter, gboolean inverted);

void gtk_vumeter_set_fixed_dimension (GtkVUMeter *vumeter, gint size);

G_END_DECLS

#endif /* __GTKVUMETER_H__ */
