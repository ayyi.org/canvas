/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

struct _StripInsert {
    Strip*        strip;
    int           slot;
    GtkWidget*    combobox;        // insert selector combo-box widget.
    int           changed_handler;
    char          previous[256];   // used to detect if the selection has really changed.
};

StripInsert* insert_new                  (Strip*);
void         insert_hide                 (StripInsert*);
GtkWidget*   pluginbox_new               (Strip*, StripInsert*);
void         pluginbox_set_selection     (Strip*, StripInsert*);
void         pluginbox_show_bypassed     (Strip*, int slot, gboolean bypassed);

