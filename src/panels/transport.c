/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __transport_window_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>

#include "model/time.h"
#include "model/am_message.h"
#include "agl/text/text_input.h"
#include "widgets/svgtogglebutton.h"
#include "windows.h"
#include "transport.h"
#include "song.h"
#include "icon.h"
#include "style.h"
#include "support.h"
#include "toolbar.h"
#include "transport/transport_locators_box.h"

#undef AGL_BUTTONS
#undef SHOW_LOCATORS // styling and layout needs fixing

#ifdef AGL_BUTTONS
#include "gl_mixer/style.h"
#include "gl_mixer/button.h"
#endif

extern Icon icons[];

//#define BIG_FONT "Verdana Bold 40"
//#define BIG_FONT "Vera Sans Bold 40"
#define BIG_FONT "Goha-Tiheb Zemen Bold 40"
#undef SET_FONT_USING_STYLE

enum {
  TRANSTYLE_PLAIN
};


static GObject* transport_construct         (GType, guint n_construct_properties, GObjectConstructParam*);
static void     transport_destroy           (GtkObject*);
static void     transport_on_ready          (AyyiPanel*);
static gboolean transport_spp_on_edit_start (GtkWidget*, gpointer);
static gboolean transport_spp_on_edit_done  (GtkWidget*, gpointer);
static gboolean transport_on_key_press      (GtkWidget*, GdkEventKey*, gpointer);
static void     transport_on_resize         (GtkWidget*, GtkAllocation*);
static void     transport_on_realize        (GtkWidget*);
static void     transport_locator_update    ();
static void     transport_sync              (GObject*, gpointer);
static void     transport_on_locators       (GObject*, gpointer);
static void     transport_on_play           (GObject*, gpointer);


G_DEFINE_TYPE (AyyiTransportWin, ayyi_transport_win, TYPE_GL_PANEL)


static void
ayyi_transport_win_class_init (AyyiTransportWinClass* klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	GtkObjectClass* gtk_class = GTK_OBJECT_CLASS (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	panel->name         = "Transport";
	panel->has_link     = false;
	panel->has_follow   = false;
	panel->accel        = GDK_F3;
	panel->on_ready     = transport_on_ready;

	gtk_class->destroy = transport_destroy;

	widget_class->size_allocate = transport_on_resize;
	widget_class->realize       = transport_on_realize;

	g_object_class->constructor = transport_construct;

	am_song__connect("locators-change", G_CALLBACK(transport_on_locators), NULL);
	am_song__connect("transport-sync", G_CALLBACK(transport_sync), NULL);
}


static void
ayyi_transport_win_init (AyyiTransportWin* object)
{
}


static GObject*
transport_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	GObject* object = G_OBJECT_CLASS(ayyi_transport_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)object, AYYI_TYPE_TRANSPORT_WIN);

	return object;
}


static void
transport_on_ready (AyyiPanel* panel)
{
	TransportWin* transport_win = (TransportWin*)panel;
	GlPanel* glpanel = (GlPanel*)panel;

	if (transport_win->spp) return;

	set_str(((AGlActor*)glpanel->scene)->name, g_strdup("Transport"));

	AGlActor* text = transport_win->spp = text_input(NULL);
	agl_actor__add_child((AGlActor*)glpanel->scene, transport_win->spp);
	text->colour = 0xffffffff;
	text_input_set_font_name((TextInput*)text, "Open Sans Bold");
	text_input_set_color((TextInput*)text, 0xbbbbbbff);

	void on_spp_edited (AGlObservable* o, AGlVal a, gpointer text)
	{
		AyyiSongPos pos;
		if (bbss2pos(text_input_get_text((TextInput*)text), &pos)) {
			am_transport_jump(&pos);
		}
	}
	text_input_set_activatable((TextInput*)text, true);
	agl_observable_subscribe(((TextInput*)text)->activated, on_spp_edited, text);

	am_song__connect("transport-play",  G_CALLBACK(transport_on_play), transport_win);

#ifdef AGL_BUTTONS
	void rec_action (AGlActor* actor, gpointer _c)
	{
		ayyi_transport_rec();
	}

	void rew_action (AGlActor* actor, gpointer _c)
	{
		ayyi_transport_rew();
	}

	void stop_action (AGlActor* actor, gpointer _c)
	{
		ayyi_transport_stop();
	}

	void play_action (AGlActor* actor, gpointer _c)
	{
		ayyi_transport_play();
	}

	bool get_state (AGlActor* actor, gpointer _c)
	{
		return false;
	}

	static Style style = (Style){
		.fg = 0xff9966ff,
		.bg = 0x000000ff,
		.bg_selected = 0x999999ff,
		.text = 0x000000ff,
	};

	void add_button (int i, ButtonAction action)
	{
		AGlActor* b = button(&icons[ICON_TR_REC_32 + i], action, get_state, NULL);
		agl_actor__add_child((AGlActor*)glpanel->scene, b);
		b->region = (AGlfRegion){(32 + 2) * i, 46, 32 * (i + 1), 46 + 32};
		((ButtonActor*)b)->style = &style;
	}
	add_button(0, rec_action);
	add_button(1, rew_action);
	add_button(2, stop_action);
	add_button(3, play_action);
	add_button(4, stop_action);
	add_button(5, stop_action);
#endif

	GtkWidget* hbox1 = transport_win->hbox = gtk_hbox_new(NON_HOMOGENOUS, 0);

	// if parent is GtkPaned, set the height to be 100%
#if 0
	void hbox_on_size_request (GtkWidget* widget, GtkRequisition* req, gpointer data)
	{
		req->height = widget_is_in_paned(widget) ? gtk_widget_get_parent(widget)->allocation.height : 100;
	}
	g_signal_connect(transport_win->hbox, "size-request", G_CALLBACK(hbox_on_size_request), NULL);
#endif

	panel_pack(hbox1, EXPAND_FALSE);

	// lhs vbox
	GtkWidget* vbox_lhs = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox_lhs), 2);
	gtk_box_pack_start(GTK_BOX(hbox1), vbox_lhs, FALSE, FALSE, 0);

	//an hbox for the spp and label so they can be padded on lhs: ? needed?

	// main spp display

#if 0
	PangoFontDescription* font_desc = pango_font_description_from_string (BIG_FONT);
	pango_font_description_set_size(font_desc, PANGO_SCALE * (app->font_size * 2 + 8));

	GtkWidget* spp_entry = transport_win->spp_entry = gtk_entry_new_with_max_length(13);
	gtk_entry_set_text(GTK_ENTRY(spp_entry), "001:02:03:470");
	gtk_entry_set_has_frame(GTK_ENTRY(spp_entry), FALSE);
	gtk_widget_modify_font(spp_entry, font_desc);
	gtk_widget_show(spp_entry);
	gtk_box_pack_start(GTK_BOX(vbox_lhs), spp_entry, FALSE, FALSE, 0);
	g_signal_connect((gpointer)spp_entry, "focus-in-event", G_CALLBACK(transport_spp_on_edit_start), NULL);
	g_signal_connect((gpointer)spp_entry, "focus-out-event", G_CALLBACK(transport_spp_on_edit_done), NULL);

	pango_font_description_free(font_desc);
#endif
	g_signal_connect((gpointer)glpanel->area, "focus-in-event", G_CALLBACK(transport_spp_on_edit_start), NULL);
	g_signal_connect((gpointer)glpanel->area, "focus-out-event", G_CALLBACK(transport_spp_on_edit_done), NULL);

	// buttons

	GtkWidget* hbox_b = transport_win->button_box = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_end(GTK_BOX(vbox_lhs), hbox_b, FALSE, FALSE, 0);

	int transport_style = TRANSTYLE_PLAIN;
	#define DEFAULT_TR_BUT_SIZE 48
	int size = DEFAULT_TR_BUT_SIZE;

	if(transport_style == TRANSTYLE_PLAIN){
		for(int i=0;i<tr_array->len;i++){
			TransportButton* button = &g_array_index(tr_array, TransportButton, i);
			GtkWidget* b = transport_win->b[i] = (GtkWidget*)svg_toggle_button_new(button->stock_id);
			svg_toggle_button_set_size((SvgToggleButton*)b, size);
			gtk_box_pack_start(GTK_BOX(hbox_b), b, FALSE, FALSE, 0);
			g_signal_connect(G_OBJECT(b), "clicked", button->callback, NULL);
		}
	} else {
		GtkWidget* image3 = gtk_image_new_from_file("pixmaps/buttons1.xpm");
		gtk_widget_show(image3);
		gtk_box_pack_start(GTK_BOX(hbox_b), image3, TRUE, TRUE, 0);
	}

	// locators

#ifdef SHOW_LOCATORS
	transport_win->locators = transport_locators_box_new(transport_win);
	gtk_box_pack_start(GTK_BOX(transport_win->hbox), GTK_WIDGET(((TransportLocatorsBox*)transport_win->locators)->locators_bg), FALSE, FALSE, 0);
#endif

	//key-press is connected to the toplevel window as connecting to other widgets doesnt always work.
	g_signal_connect((gpointer)panel->window, "key-press-event", G_CALLBACK(transport_on_key_press), transport_win);

	// focus the play button
	gtk_widget_grab_focus(transport_win->b[3]);

	ayyi_panel_on_open(panel);
	transport_sync(NULL, panel);

	void transport_win_on_spp (Observable* o, AMVal val, gpointer _transport)
	{
		AyyiTransportWin* transport = _transport;

		char spp_str[16];
		ayyi_transport_get_spp_string(spp_str);

		text_input_set_text((TextInput*)transport->spp, spp_str);
	}
	observable_subscribe_with_state(song->spp, transport_win_on_spp, transport_win);
}


static void
transport_destroy (GtkObject* object)
{
	AyyiPanel* panel = (AyyiPanel*)object;

	if (!panel->destroyed) {
		observable_unsubscribe(song->spp, NULL, object);
		g_signal_handlers_disconnect_by_data(song, object);
	}

	GTK_OBJECT_CLASS(ayyi_transport_win_parent_class)->destroy(object);

	panel->destroyed = true;
}


static gboolean
transport_spp_on_edit_start (GtkWidget* widget, gpointer user_data)
{
	PF;
	gtk_window_remove_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	return false;
}


static gboolean
transport_spp_on_edit_done (GtkWidget* widget, gpointer user_data)
{
	PF;
	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	return false;
}


/*
 *  Change the size of the widgets so they fit in the window.
 *  Note: there is no scrollwin on this window.
 */
static void
transport_on_resize (GtkWidget* widget, GtkAllocation* allocation)
{
	if (!GTK_WIDGET_REALIZED(widget)) return;
	PF;
	((GtkWidgetClass*)ayyi_transport_win_parent_class)->size_allocate(widget, allocation);

	TransportWin* panel = (TransportWin*)widget;

#if 1
	if (panel->b[0]) {
		int width = (allocation->width / 4) * 4;

		#define PADDING_GUESTIMATE 13
		int button_req = CLAMP(((width - 6 * PADDING_GUESTIMATE) / 6), 16, 48);
		for (int i=0;i<tr_array->len;i++) {
			svg_toggle_button_set_size((SvgToggleButton*)panel->b[i], button_req);
		}

		#define MIN_FONT 20
		#define MAX_FONT 40
		#define MIN_WIDTH 185
		#define MAX_WIDTH 500
		#define WIDTH (MAX_WIDTH - MIN_WIDTH)
		//int w = CLAMP(allocation->width - MIN_WIDTH, 0, WIDTH);
		int w = CLAMP(width - MIN_WIDTH, 0, WIDTH);
		agl_observable_set_int(
			((TextInput*)panel->spp)->font,
			MIN_FONT + (w * (MAX_FONT - MIN_FONT)) / WIDTH
		);

		int font_size = ((TextInput*)panel->spp)->font->value.i;
		gtk_widget_set_size_request(((GlPanel*)panel)->area,
			-1,
			font_size * 1.65
#ifdef AGL_BUTTONS
			+ 34
#endif
		);
		panel->spp->region = (AGlfRegion){4, 1 - font_size / 5, 280, font_size * 2};
	}

#else
	// previous layout code, sometimes does not resolve

	if(panel->old_allocation.height == allocation->height && panel->old_allocation.width == allocation->width && !panel->button_resize_pending){
		dbg(1, "unchanged");
		return;
	}

	// a trap to prevent endless resize attempts
	static int count = 0;
	gboolean resize__on_timeout(gpointer data)
	{
		int* count = data;
		(*count)--;
		if(*count < -10){ *count = 0; return TIMER_STOP; }
		return TIMER_CONTINUE;
	}
	if(!count) g_timeout_add(100, resize__on_timeout, &count);
	if(count++ > 50){ pwarn("too many resize attempts!"); return; }

	// FIXME we cannot get the width of the right hand column because it is being allocated space outside of the visible area!
	//       ...however, maybe this doesnt matter - if there isnt room for it, dont show it.
	//GtkWidget* parent = gtk_widget_get_parent(widget);
	int total_width = /*(G_OBJECT_TYPE(parent) == GTK_TYPE_VPANED) ? parent->allocation.width :*/ widget->allocation.width;
	if(panel->locators->allocation.width > panel->locators->requisition.width) pwarn("wider!");
	int right_hand_column_width = panel->locators->allocation.width > 10 ? panel->locators->allocation.width : 150;
	int left_hand_column_width = MAX(130, total_width - right_hand_column_width - 2);
	dbg(1, "panel_width=%i locators_width=%i left_width=%i", widget->allocation.width, panel->locators->allocation.width, left_hand_column_width);

	if(!panel->button_resize_pending) { // main spp size
		dbg(1, "1");

		transport_locators_box_update_n_visible(panel->locators);

#ifdef SET_FONT_USING_STYLE
		//because the gtk_entry has a style applied to it, we cannot use gtk_widget_modify_font(). We set the font_desc directly on the style.
#endif

		PangoFontDescription* font_desc = pango_font_description_from_string (BIG_FONT);
		PangoLayout* layout = gtk_entry_get_layout(GTK_ENTRY(panel->spp_entry));
		//GtkStyle* style = gtk_widget_get_style(panel->spp_entry);
		GtkStyle* style = gtk_style_copy(gtk_widget_get_style(panel->spp_entry));
		//			GtkRcStyle* rcstyle = gtk_widget_get_modifier_style(panel->spp_entry);
		PangoFontDescription* font_desc_o = style->font_desc;
		PangoRectangle logical_rect;
		pango_layout_get_pixel_extents(layout, NULL, &logical_rect);

		int spp_alloc_height = panel->spp_entry->allocation.height;
		int spp_text_height = logical_rect.height - logical_rect.y;//ink_rect.height;
		int spp_padding_height = spp_alloc_height - spp_text_height;
		int spp_font_size = pango_font_description_get_size(font_desc_o);
		bool absolute = pango_font_description_get_size_is_absolute(font_desc_o);

#ifndef SET_FONT_USING_STYLE
		g_object_unref(style);
#endif

		//how tall do we want to be?
		//if the box is not tall enough, use all the available height.
		#define SMALL_HEIGHT 50
		int spp_target_height = (widget->allocation.height < SMALL_HEIGHT) ? widget->allocation.height : widget->allocation.height / 2 - 2 - spp_padding_height;

		int spp_text_width = logical_rect.width - logical_rect.x;
		int spp_padding_width = 5; //total guess
		int spp_target_font_width = left_hand_column_width - spp_padding_width;

		if(spp_text_height > 0){
			g_return_if_fail(spp_text_width);
			dbg(1, "width: current=%.0f target=%i --> %i", (float)spp_text_width, spp_target_font_width, (int)(((spp_font_size * spp_target_font_width) / spp_text_width)) / PANGO_SCALE);
			dbg(1, "height: current=%i target=%i --> %i", spp_text_height, spp_target_height, (int)(((spp_font_size * spp_target_height) / spp_text_height) / PANGO_SCALE));
			int spp_target_font_size = MIN(
				(spp_font_size * spp_target_height) / spp_text_height,
				(spp_font_size * spp_target_font_width) / spp_text_width
			);
			spp_target_font_size = 2 * PANGO_SCALE * (spp_target_font_size / (2 * PANGO_SCALE));
			dbg (2, "spp: target: %i x %i", left_hand_column_width, spp_target_height);
			dbg (1, "vpadding=%i font_size=%i-->%i", spp_padding_height, spp_font_size/PANGO_SCALE, spp_target_font_size/PANGO_SCALE);
			if(ABS(spp_target_font_size - spp_font_size) >= 2){
				if(absolute) pango_font_description_set_absolute_size(font_desc, spp_target_font_size);
				else         pango_font_description_set_size(font_desc, spp_target_font_size);

				pango_font_description_set_weight(font_desc, PANGO_WEIGHT_HEAVY);

				dbg(2, "  %s", pango_font_description_to_string(font_desc));
#ifdef SET_FONT_USING_STYLE
				gtk_widget_set_style(panel->spp_entry, style);
#else

				GtkRcStyle* rcstyle = gtk_widget_get_modifier_style(panel->spp_entry);
				rcstyle->font_desc = font_desc;
				gtk_widget_modify_font(panel->spp_entry, font_desc);
#endif

				gtk_widget_set_size_request(panel->spp_entry, spp_target_font_width - 4, -1); // why does the widget do this itself?

				panel->button_resize_pending = true;
			}
			else if(_debug_) pwarn("style not applied: too similar.");
		}
	}

	else{
		dbg(1, "2");
		int aw = widget->allocation.width;
		int rw = widget->requisition.width;

		if(aw < rw){
			if(panel->button_resize_pending){
				// handle the (eg, tabbed wm) simple case where the alloc may be smaller than the requisition:

				if(widget->allocation.height >= SMALL_HEIGHT){
					gtk_widget_show(panel->button_box);

					int available_height = widget->allocation.height - panel->spp_entry->allocation.height;

					// change button size
					int n_buttons = tr_array->len;

#ifdef DEBUG
					int old_button_width = panel->b[0]->allocation.width;
#endif
					static int previous_button_req = 0;
					#define PADDING_BODGE 8 // There is a difference in the way we are getting and setting the button size.
					int button_req = MAX(
						16,
						MIN(left_hand_column_width / n_buttons - PADDING_BODGE, available_height) // set depending on whether horizontally or vertically constrained.
					);
					dbg(3, "reducing button size: win_wid=%i win_req=%i old_button_width=%i new_button_req=%i", aw, rw, old_button_width, button_req);
					if(button_req > 3 && button_req != previous_button_req){
						int i;
						for(i=0;i<tr_array->len;i++){
							svg_toggle_button_set_size((SvgToggleButton*)panel->b[i], button_req);
						}
					}
					previous_button_req = button_req;
				}else{
					gtk_widget_hide(panel->button_box);
				}
			}
		} else {

#if 0
			//do the buttton widgets need to be increased in size?
			int tot_widget_height = panel->spp_entry->allocation.height + panel->b[0]->allocation.height;
			//printf("%s(): widget_height=%i\n", __func__, widget_height);
			#define SLACK 10
			if(tot_widget_height < widget->allocation.height - SLACK){
				dbg (4, "widgets need to be taller!");
				//so is the widget width also too small?
				int widget_width = widget->allocation.width;
				if(widget->requisition.width < widget_width - SLACK){
					//printf("%s(): widgets need to be wider!\n", __func__);

					int new_button_size = svgbutton_get_size(panel->b[0]) + 6; //FIXME set the needed size!

					int i; for(i=0;i<tr_array->len;i++){
						TransportButton* tr_struct = &g_array_index(tr_array, TransportButton, i);
						svg_toggle_button_set_size((SvgToggleButton*)panel->b[i], button_req);
					}
				}
			}
#endif
			int available_height = widget->allocation.height - panel->spp_entry->allocation.height;
			int n_buttons = tr_array->len;
			int button_req = MAX(
				16,
				MIN(
					left_hand_column_width / n_buttons - PADDING_BODGE,
					available_height
				) //set depending on whether horizontally or vertically constrained.
			);

//this is the same as above
				dbg(3, "button size: win_wid=%i win_req=%i new_button_req=%i", aw, rw, button_req);
				if(button_req > 3/* && button_req != previous_button_req*/){
					int i;
					for(i=0;i<tr_array->len;i++){
						//TransportButton* tr_struct = &g_array_index(tr_array, TransportButton, i);
						svg_toggle_button_set_size((SvgToggleButton*)panel->b[i], button_req);
					}
				}
		}
		panel->button_resize_pending = false;
	} //end phase 2.

	//transport_locators_box_update_n_visible(panel->locators);

	// reduce the min window size so users can resize
	gtk_widget_set_size_request(widget, 10, 10);

	panel->old_allocation = *allocation;
#endif
}


static void
transport_on_realize (GtkWidget* widget)
{
	// Do window creation stuff that requires the widgets to be already realised.

#if 0
	TransportWin* window = (TransportWin*)widget;
	window->button_resize_pending = false;
#endif

	((GtkWidgetClass*)ayyi_transport_win_parent_class)->realize(widget);

	// set the background colour on the main spp display (we use the ACTIVE colour not the NORMAL):
#if 0
	if(window->spp_entry){
		GtkStyle* style = gtk_style_copy(gtk_widget_get_style(window->spp_entry));
		GdkColor colour = style->base[GTK_STATE_NORMAL];
		style->base[GTK_STATE_NORMAL] = style->text[GTK_STATE_NORMAL];
		style->text[GTK_STATE_NORMAL] = colour;
		dbg (2, "colour=#%x", am_gdk_to_rgba(&colour));
#ifdef TEMP
		gtk_widget_set_style(window->spp_entry, style);
#endif
		g_object_unref(style);
	}
#endif
}


void
transport_clear_buttons ()
{
	// reset visual status of all transport buttons in transport windows, and toolbars.

	panel_foreach {
		if (G_OBJECT_TYPE(panel) == AYYI_TYPE_TRANSPORT_WIN) {
			TransportWin* transport_win = (TransportWin*)panel;

			if (transport_win->b[TR_PLAY]) {
				for (int i=0;i<tr_array->len;i++) {
					gtk_widget_set_style(transport_win->b[i],  NULL);
				}
			}
		} else {
			// reset any toolbar transport buttons this window may have

			GtkWidget** toolbar = panel->toolbar->b_transport;
			if (toolbar && toolbar[0]) {
				for (int i=0;i<TR_SIZE;i++) {
					gtk_widget_set_style(toolbar[i],  NULL);
				}
			}
		}
	} end_panel_foreach
}


/*
 * Update displayed locator values.
 */
static void
transport_locator_update ()
{
#ifdef SHOW_LOCATORS
	static char str[16];

	transport_foreach {
		ayyi_pos2bbst(&song->loc[1].vals[0].val.sp, str);
		dbg(3, "loc1=%s", str);
		transport_locators_box_set_text(transport->locators, 0, str);
		ayyi_pos2bbst_1(&song->loc[2].vals[0].val.sp, str);
		transport_locators_box_set_text(transport->locators, 1, str);
	} end_transport_foreach
#endif
}


/*
 *  Handle RETURN key for locator boxes.
 */
static gboolean
transport_on_key_press (GtkWidget* window, GdkEventKey* event, gpointer user_data)
{
	int handled = FALSE;

#ifdef SHOW_LOCATORS
	TransportWin* transport = user_data;
	g_return_val_if_fail(transport, false);

	switch (event->keyval) {
		case GDK_Return:
			{
				GtkWidget* focused = gtk_window_get_focus(GTK_WINDOW(window));
				TransportLocatorsBox* locators = TRANSPORT_LOCATORS_BOX(transport->locators);
				g_return_val_if_fail(locators, false);

				for (int i=0;i<5;i++) {
					if (focused == locators->text_view[i]) {
						gboolean sig_ret = 1;
						g_signal_emit_by_name(focused, "focus-out-event", locators, &sig_ret);

						// restore the correct accel state
						g_signal_emit_by_name(focused, "focus-in-event", locators, &sig_ret);
						break;
					}
				}
			}
			handled = TRUE;
			break;
		default:
			dbg(2, "keyval=%i %x", event->keyval, event->keyval);
			break;
	}
#endif
	return handled;
}


static void
transport_sync (GObject* _song, gpointer _transport)
{
	TransportWin* tw = _transport;

	transport_locator_update();

	void a(TransportWin* transport)
	{
		if ((SvgToggleButton*)transport->b[TR_REW]) {
			svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_REW],  ayyi_transport_is_rew());
			svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_STOP], ayyi_transport_is_stopped());
			svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_FF],   ayyi_transport_is_ff());
			svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_PLAY], ayyi_transport_is_playing());
			svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_CYC],  ayyi_transport_is_cycling());
		}
	}

	if (tw) {
		a(tw);
	} else {
		transport_foreach {
			a(transport);
		} end_transport_foreach;
	}
}


static void
transport_on_locators (GObject* _song, gpointer user_data)
{
	transport_locator_update();
}


static void
transport_on_play (GObject* _song, gpointer user_data)
{
	TransportWin* transport = user_data;

	svg_toggle_button_set_active((SvgToggleButton*)transport->b[TR_PLAY], true);
}
