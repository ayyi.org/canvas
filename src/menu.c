/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include "model/song.h"
#include "model/part_manager.h"
#include "window.h"
#include "windows.h"
#include "support.h"
#include "panels/arrange.h"
#include "panels/arrange/automation.h"
#include "panels/pool.h"
#include "panels/mixer.h"
#include "panels/transport.h"
#include "panels/inspector.h"
#include "panels/colour.h"
#include "seqedit.h"
#include "save.h"
#include "song.h"
#include "icon.h"
#include "menu.h"
#include "test.h"

#ifdef USE_HELP
  extern GType ayyi_help_win_get_type();
  #define AYYI_TYPE_HELP_WIN (ayyi_help_win_get_type ())
  GtkWidget* help_win__new();
#endif

extern GType  ayyi_log_win_get_type  ();
extern void   meters_demo_toggle     ();
extern void   song_load_new          (const char* filename);

extern GList*      action_groups;
extern GHashTable* canvas_types;
extern GimpActionGroup* global_action_group;

typedef struct
{
    int        item;
    char*      name;
    bool       (*value)(AyyiWindow*, gpointer);
    void       (*toggle)(AyyiWindow*, gpointer);
    gpointer   user_data;
} ViewItem;

typedef struct {
    ViewItem*   view_item;
    AyyiWindow* window;
} ViewRef;

typedef struct _ViewInstances ViewInstances;

typedef struct {
    //ViewRef*   ref;
    ViewInstances* instances;
    GtkWidget*     menu_item;
    gulong         signal_id;
} ViewInstance;

struct _ViewInstances {
    ViewRef      ref;
    ViewInstance i[2];
};

struct _ShellMenus
{
    GArray* view_instances;
};

static GArray* view_items = NULL;

static GtkWidget* view_menu_new              (AyyiWindow*, GimpActionGroup*, bool icon);
static GtkWidget* edit_menu_new              (AyyiWindow*, GimpActionGroup*, bool icon);
static void       on_song_new_activate       (GtkMenuItem*, gpointer);
static void       on_song_reload_activate    (GtkMenuItem*, gpointer);
static void       on_song_open_activate      (GtkMenuItem*, gpointer);
static void       on_song_sync_activate      (GtkMenuItem*, gpointer);
static void       on_song_save_activate      (GtkMenuItem*, gpointer);
//static void     on_song_save_as_activate   (GtkMenuItem*, gpointer);
static void       on_quit                    (GtkMenuItem*, gpointer);
static void       on_cut_activate            (GtkMenuItem*, gpointer);
static void       on_copy_activate           (GtkMenuItem*, gpointer);
static void       on_paste_activate          (GtkMenuItem*, gpointer);
static void       on_delete_activate         (GtkMenuItem*, gpointer);
static void       on_new_track_activate      (GtkMenuItem*, gpointer);
static void       on_about_activate          (GtkMenuItem*, gpointer);
static void       on_new_win_activate        (GtkMenuItem*, AyyiPanelClass*);
static void       menu__on_zoom_in           (GtkMenuItem*, gpointer);
static void       menu__on_zoom_out          (GtkMenuItem*, gpointer);
static void       menu__on_zoom_to_selection (GtkMenuItem*, gpointer);
static void       peak_gain_inc_menu         (GtkMenuItem*, gpointer);
static void       peak_gain_dec_menu         (GtkMenuItem*, gpointer);
static void       menu_arr_zoom_song         (GtkMenuItem*, gpointer);
static void       menu_set_windows           (AyyiWindow*);
#ifdef DEBUG
static void       show_core_status           ();
#endif

static void       menu_set_view_item         (ViewInstances*, int i, bool value);
static void       menu_update_windows        ();

static GimpActionGroup* get_arrange_action_group();


/*
 *  Create a new menu bar for a window.
 */
GtkWidget*
shell_menu_new (AyyiWindow* window)
{
	if(!window->shell_menus){
		window->shell_menus = AYYI_NEW(ShellMenus,
			.view_instances = g_array_sized_new(ZERO_TERMINATED, TRUE, sizeof(ViewInstances), 5)
		);
		g_array_set_size(window->shell_menus->view_instances, 5);
	}

	GtkAccelGroup* accel_group = app->global_accel_group;

	GimpActionGroup* action_group = get_arrange_action_group();
	dbg(2, "action_group=%s", gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)));

	GtkWidget* menubar = gtk_menu_bar_new();

	GtkWidget* menuitem1 = gtk_menu_item_new_with_mnemonic ("_File");
	gtk_container_add(GTK_CONTAINER(menubar), menuitem1);

	window->menu.file = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem1), window->menu.file);

	menu_update_recent_songs();

	// "Edit" menu
	gtk_container_add(GTK_CONTAINER(menubar), edit_menu_new(window, action_group, false));

	// "View" menu
	gtk_container_add(GTK_CONTAINER(menubar), view_menu_new(window, action_group, false));

	//-------------------------------------------------------------

	// "Window" menu
	GtkWidget* window_menu = gtk_menu_item_new_with_mnemonic ("_Window");
	gtk_container_add (GTK_CONTAINER(menubar), window_menu);

	window->menu.window = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(window_menu), window->menu.window);

	// "Launch" submenu
	{
		void launch_application (GtkMenuItem* menuitem, AyyiLaunchInfo* info)
		{
			ayyi_launch(info, NULL);
		}

		GtkWidget* launch = gtk_image_menu_item_new_with_mnemonic("Launch");
		GtkWidget* ico = gtk_image_new_from_stock ("gtk-new", GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(launch), ico);
		gtk_container_add(GTK_CONTAINER(window->menu.window), launch);

		GtkWidget* launch_menu = gtk_menu_new();
		gtk_menu_item_set_submenu(GTK_MENU_ITEM (launch), launch_menu);

		void update_ayyi_application_menu (GtkWidget* launch_menu)
		{
			GList* l = ayyi.launch_info;
			for(;l;l=l->next){
				AyyiLaunchInfo* info = l->data;

				GtkWidget* item = gtk_image_menu_item_new_with_mnemonic(info->name);
				GtkWidget* ico = gtk_image_new_from_stock ("gtk-new", GTK_ICON_SIZE_MENU);
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), ico);
				gtk_container_add (GTK_CONTAINER(launch_menu), item);

				g_signal_connect((gpointer)item, "activate", G_CALLBACK(launch_application), info);
			}
		}

		update_ayyi_application_menu(launch_menu);
		gtk_widget_set_sensitive(launch, (boolp)ayyi.launch_info);
	}

	GtkWidget* new2 = gtk_image_menu_item_new_with_mnemonic("New");
	GtkWidget* ico = gtk_image_new_from_stock ("gtk-new", GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(new2), ico);
	gtk_container_add(GTK_CONTAINER(window->menu.window), new2);

	// "new" submenu
	GtkWidget* new2_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM (new2), new2_menu);

	// copy the panel-type list to a GArray and then sort by keyboard-shortcut
	GArray* panels = g_array_sized_new (FALSE, TRUE, sizeof(AyyiPanelClass*), g_hash_table_size(app->panel_classes));
	g_array_set_size (panels, g_hash_table_size(app->panel_classes));

	GHashTableIter iter;
	gpointer key;
	void* x;
	int i = 0;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, (gpointer)&x)){
		PanelType panel_type = *((gint64*)key);
		AyyiPanelClass* klass = g_type_class_peek(panel_type);
		g_array_index (panels, AyyiPanelClass*, i++) = klass;
	}

	gint panel_type_sort (gconstpointer a, gconstpointer b)
	{
		AyyiPanelClass** k = (AyyiPanelClass**)a;
		AyyiPanelClass** m = (AyyiPanelClass**)b;

		return !(*k)->accel
			? 1
			: !(*m)->accel
				? -1
				: (*k)->accel > (*m)->accel
					? 1
					: -1;
	}
	g_array_sort (panels, panel_type_sort);

	for(i=0;i<panels->len;i++){
		AyyiPanelClass* klass = g_array_index(panels, AyyiPanelClass*, i);
		PanelType panel_type = G_OBJECT_CLASS_TYPE(klass);

		GtkWidget* menu_item = gtk_image_menu_item_new_with_mnemonic(klass->name);
		gtk_container_add(GTK_CONTAINER(new2_menu), menu_item);
		if(klass->accel) gtk_widget_add_accelerator(menu_item, "activate", accel_group, klass->accel, 0, GTK_ACCEL_VISIBLE);
		g_signal_connect((gpointer)menu_item, "activate", G_CALLBACK(on_new_win_activate), klass);

		//TODO icons: shouldnt mix and match system with application icons!
		char* icon_name =
#ifdef USE_HELP
			(panel_type == AYYI_TYPE_HELP_WIN)
				? "gtk-help"
				:
#endif
				((panel_type == AYYI_TYPE_INSPECTOR_WIN)
					? "gtk-dialog-info"
					: ((panel_type == AYYI_TYPE_COLOUR_WIN)
						? GTK_STOCK_SELECT_COLOR
						: klass->name));

		GtkWidget* image = icon_name ? gtk_image_new_from_stock(icon_name, GTK_ICON_SIZE_MENU) : NULL;
		if(image) gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM(menu_item), image);

		#ifdef USE_HELP //TODO ideally, TYPE_HELP would always be defined, so we can show the menu item greyed out.
		if(panel_type == AYYI_TYPE_HELP_WIN){
			#ifndef USE_HELP
			gtk_widget_set_sensitive(GTK_WIDGET(menu_item), FALSE); //grey
			#endif
		}
		#endif

		if(panel_type == AYYI_TYPE_INSPECTOR_WIN){
			if(!((AyyiSongService*)ayyi.service)->amixer) gtk_widget_set_sensitive(menu_item, FALSE); //grey
		}
	}

	g_array_free(panels, true);

	//-------------------------------------------------------------

	// "Help" menu
	GtkWidget* menuitem7 = gtk_menu_item_new_with_mnemonic ("_Help");
	gtk_container_add (GTK_CONTAINER (menubar), menuitem7);

	GtkWidget* menuitem7_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem7), menuitem7_menu);

	GtkWidget* about = gtk_image_menu_item_new_from_stock ("gtk-about", accel_group);
	gtk_container_add (GTK_CONTAINER (menuitem7_menu), about);
	g_signal_connect ((gpointer)about, "activate", G_CALLBACK(on_about_activate), NULL);

	GtkWidget* help = gtk_image_menu_item_new_with_mnemonic ("Help    (F1)");
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM (help), gtk_image_new_from_stock ("gtk-help", GTK_ICON_SIZE_MENU));
#ifndef USE_HELP
	gtk_widget_set_sensitive(GTK_WIDGET(help), FALSE); //grey
#endif
	gtk_container_add(GTK_CONTAINER(menuitem7_menu), help);
#ifdef USE_HELP
	if(!g_type_class_peek(AYYI_TYPE_HELP_WIN)) pwarn("no help class. may need to do: make register.c");
	g_signal_connect ((gpointer)help, "activate", G_CALLBACK (on_new_win_activate), g_type_class_peek(AYYI_TYPE_HELP_WIN));
#endif

	GtkWidget* debug1 = gtk_image_menu_item_new_with_mnemonic("_Debug");
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(debug1), gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU));
	gtk_container_add(GTK_CONTAINER(menuitem7_menu), debug1);

#ifdef DEBUG
	// "debug" submenu
	GtkWidget* debug_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(debug1), debug_menu);

	GtkWidget* test1 = gtk_image_menu_item_new_with_mnemonic ("_Run Test");
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(test1), gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU));
	gtk_container_add(GTK_CONTAINER(debug_menu), test1);
	g_signal_connect ((gpointer)test1, "activate", G_CALLBACK (test_start), NULL);

	GtkWidget* status = gtk_image_menu_item_new_with_mnemonic ("_System Status");
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(status), gtk_image_new_from_stock (GTK_STOCK_INFO, GTK_ICON_SIZE_MENU));
	gtk_container_add(GTK_CONTAINER (debug_menu), status);
	g_signal_connect ((gpointer)status, "activate", G_CALLBACK (show_core_status), NULL);

	GtkWidget* test_meters = gtk_image_menu_item_new_with_mnemonic ("_Test Meters");
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(test_meters), gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU));
	gtk_container_add(GTK_CONTAINER (debug_menu), test_meters);
	g_signal_connect ((gpointer)test_meters, "activate", G_CALLBACK (meters_demo_toggle), NULL);
#endif

	void on_panel_list_changed ()
	{
		menu_update_windows();
	}

	g_signal_connect(app, "panel-list-changed", on_panel_list_changed, NULL);

	gtk_widget_show_all(menubar);

	return menubar;
}


GtkWidget*
shell_context_menu_new (AyyiWindow* window)
{
	GimpActionGroup* action_group = get_arrange_action_group();

	GtkWidget* menu = gtk_menu_new();

	gtk_container_add(GTK_CONTAINER(menu), edit_menu_new(window, action_group, true));
	gtk_container_add(GTK_CONTAINER(menu), view_menu_new(window, action_group, true));
	gtk_widget_show_all(menu);
	return menu;
}


void
shell_menu_free (AyyiWindow* window)
{
	if(window->shell_menus){
		g_array_free(window->shell_menus->view_instances, true);
		g_free0(window->shell_menus);
	}
}


void
shell__menu_popup (AyyiWindow* window, GdkEvent* event)
{
	if(!window->context_menu)
		window->context_menu = shell_context_menu_new(window); // context menu is accessed from the top of the track ctl area.
	gtk_menu_popup(GTK_MENU(window->context_menu), NULL, NULL, NULL, event, event->button.button, (guint32)(((GdkEventButton*)event)->time));
}


/*
 *  There are two instances of the edit menu. one on the window menubar, and one on the arrange context menu
 */
static GtkWidget*
edit_menu_new (AyyiWindow* window, GimpActionGroup* action_group, bool icon)
{
	g_return_val_if_fail(window, NULL);

	GtkAccelGroup* accel_group = app->global_accel_group;

	GtkWidget* menu_item = icon
		? gtk_image_menu_item_new_from_stock ("gtk-edit", accel_group)
		: gtk_menu_item_new_with_mnemonic ("_Edit");

	GtkWidget* edit_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu_item), edit_menu);

	GtkWidget* cut1 = gtk_image_menu_item_new_from_stock ("gtk-cut", accel_group);
	gtk_container_add (GTK_CONTAINER (edit_menu), cut1);
	gtk_widget_set_sensitive(cut1, FALSE);

	GtkWidget* copy = gtk_image_menu_item_new_from_stock ("gtk-copy", accel_group);
	gtk_container_add (GTK_CONTAINER (edit_menu), copy);
	gtk_widget_set_sensitive(copy, FALSE);

	GtkWidget* paste1 = gtk_image_menu_item_new_from_stock ("gtk-paste", accel_group);
	gtk_container_add (GTK_CONTAINER (edit_menu), paste1);
	gtk_widget_set_sensitive(paste1, FALSE);

	GtkWidget* delete1 = gtk_image_menu_item_new_from_stock ("gtk-delete", accel_group);
	gtk_container_add (GTK_CONTAINER (edit_menu), delete1);

	menu_separator_new(edit_menu);

	GtkWidget* undo = window->menus.edit.undo = gtk_image_menu_item_new_from_stock ("gtk-undo", accel_group);
	gtk_container_add (GTK_CONTAINER (edit_menu), undo);

// TODO accel_group
	GtkWidget* history = window->menu.history_item = gtk_image_menu_item_new_from_stock (SM_STOCK_HISTORY, accel_group);
	//GtkWidget* history = shell->menu.history_item = gtk_image_menu_item_new ();
	gtk_menu_item_set_label((GtkMenuItem*)history, "History");
	gtk_container_add(GTK_CONTAINER(edit_menu), history);

	// "history" submenu
	gtk_menu_item_set_submenu(GTK_MENU_ITEM (history), window->menu.history = gtk_menu_new());

	menu_separator_new(edit_menu);

	GtkAction* action = gtk_action_group_get_action(GTK_ACTION_GROUP(action_group), "New Track");
	GtkWidget* new_track = gtk_action_create_menu_item(action);
	gtk_container_add(GTK_CONTAINER(edit_menu), new_track);
	GtkWidget* ico = gtk_image_new_from_stock ("gtk-new", GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(new_track), ico);
	g_signal_connect ((gpointer)new_track, "activate", G_CALLBACK(on_new_track_activate), NULL);

	if(g_hash_table_size(canvas_types) > 1){
		menu_separator_new(edit_menu);

		GtkWidget* options = gtk_image_menu_item_new_with_mnemonic("Options");
		ico = gtk_image_new_from_stock (GTK_STOCK_PREFERENCES, GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(options), ico);
		gtk_container_add(GTK_CONTAINER(edit_menu), options);

		// "options" submenu
		GtkWidget* options_menu = gtk_menu_new();
		gtk_menu_item_set_submenu(GTK_MENU_ITEM (options), options_menu);
		GSList* group = NULL;

		int get_menu_instance (AyyiWindow* window)
		{
			return 0;
		}

		struct _Closure { AyyiWindow* window; GSList* group; int menu_instance; GtkWidget* options_menu;} closure = {window, group, get_menu_instance(window), options_menu};

		void menu_add_canvas (CanvasType canvas_type, CanvasClass* cc, struct _Closure* c)
		{
			dbg(2, "  %i=%p %s", canvas_type, cc, cc->name);
			EditMenu* edit_menu = &c->window->menus.canvas_type[canvas_type];
			GtkWidget* item = edit_menu->radio_item = gtk_radio_menu_item_new_with_label (c->group, cc->name);
#ifdef DEBUG
			gtk_widget_set_name(item, cc->name);
#endif
			c->group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (item));
			gtk_container_add (GTK_CONTAINER(c->options_menu), item);

			void menu_callback (GtkMenuItem* menu, gpointer _edit_menu)
			{
				EditMenu* edit_menu = _edit_menu;
				window_foreach {
					int canvas_type = array_index((void**)&window->menus.canvas_type[1], edit_menu, sizeof(EditMenu)) + 1;
					if(canvas_type){
						Arrange* arrange = (Arrange*)windows__find_panel(window, AYYI_TYPE_ARRANGE);
						CanvasClass* cc = g_hash_table_lookup(canvas_types, &canvas_type);
						if(arrange) cc->menu_callback(arrange);
					}
				} end_window_foreach
			}
			edit_menu->radio_id = g_signal_connect ((gpointer)item, "activate", G_CALLBACK(menu_callback), edit_menu);
		}
		arr_canvas_foreach((void*)menu_add_canvas, &closure);
	}

	void on_undo_activate(GtkMenuItem* menuitem, gpointer user_data) { am_song__undo(known_services[AYYI_SERVICE_AYYID1], 1); }

	g_signal_connect ((gpointer)cut1,    "activate", G_CALLBACK(on_cut_activate),     NULL);
	g_signal_connect ((gpointer)copy,    "activate", G_CALLBACK(on_copy_activate),    NULL);
	g_signal_connect ((gpointer)paste1,  "activate", G_CALLBACK(on_paste_activate),   NULL);
	g_signal_connect ((gpointer)delete1, "activate", G_CALLBACK(on_delete_activate),  NULL);
	g_signal_connect ((gpointer)undo,    "activate", G_CALLBACK(on_undo_activate),    NULL);

	return menu_item;
}


static GtkWidget*
view_menu_new (AyyiWindow* window, GimpActionGroup* action_group, bool icon)
{
	static int menu_instance = 0;

	GtkWidget* view_item = gtk_menu_item_new_with_mnemonic ("_View");

	GtkWidget* view_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(view_item), view_menu);

	gtk_menu_set_accel_group(GTK_MENU(view_menu), app->global_accel_group);

	void toggle (GtkMenuItem* menu, gpointer _instance)
	{
		ViewInstance* instance = _instance;
		ViewItem* view_item = instance->instances->ref.view_item;
		view_item->toggle((AyyiWindow*)instance->instances->ref.window, view_item->user_data);

		for(int j=0;j<2;j++){
			ViewInstance* i = &instance->instances->i[j];
			if(i != instance){
				menu_set_view_item(instance->instances, j, view_item->value((AyyiWindow*)instance->instances->ref.window, view_item->user_data));
			}
		}
	}

	for(int i=0;i<view_items->len;i++){
		ViewItem* item = &g_array_index(view_items, ViewItem, i);
		ViewInstances* instances = &g_array_index(window->shell_menus->view_instances, ViewInstances, i);
		if(!instances->ref.view_item){
			instances->ref = (ViewRef){.view_item = item, .window = window};
		}

		ViewInstance* instance = &instances->i[menu_instance];
		g_return_val_if_fail(!instance->menu_item, NULL);

		GtkWidget* menu_item = gtk_check_menu_item_new_with_label(item->name);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item), item->value(window, item->user_data));
		gtk_container_add (GTK_CONTAINER(view_menu), menu_item);

		*instance = (ViewInstance){
			.instances = instances,
			.menu_item = menu_item,
			.signal_id = g_signal_connect ((gpointer)menu_item, "activate", G_CALLBACK(toggle), instance)
		};
	}

	// peaks
	{
		GtkWidget* peaks_item = gtk_menu_item_new_with_mnemonic ("Peaks");
		gtk_container_add(GTK_CONTAINER(view_menu), peaks_item);

		GtkWidget* peaks_submenu = gtk_menu_new();
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(peaks_item), peaks_submenu);

		typedef struct { char name[16]; gpointer callback; } Items;
		// note that this duplicates info in arrange accel keys array.
		Items items[] = {
			{"Peak Gain+", peak_gain_inc_menu},
			{"Peak Gain-", peak_gain_dec_menu},
		};
		if (action_group) {
			for (int i=0;i<G_N_ELEMENTS(items);i++) {
				GtkAction* action = gtk_action_group_get_action(GTK_ACTION_GROUP(action_group), items[i].name);
				if (action) {
					GtkWidget* menu_item = gtk_action_create_menu_item(action);
					gtk_container_add(GTK_CONTAINER(peaks_submenu), menu_item);

					gchar* stock_id;
					g_object_get (action, "stock-id", &stock_id, NULL);
					GtkWidget* ico_peaks1 = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_MENU);
					g_free(stock_id);
					gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(menu_item), ico_peaks1);
					//why do we have to connect the signal? the action doesnt get called when item is activated otherwise, despite showing the shortcut.
					//-using an accelpath apparently shows the same behaviour...
					g_signal_connect((gpointer)menu_item, "activate", G_CALLBACK(items[i].callback), NULL);
				} else {
					pwarn("failed to get action: '%s' from group=%s", items[i].name, gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)));
				}
			}
		}
	}

	// zoom
 
	GtkWidget* zoom_item = gtk_image_menu_item_new_with_mnemonic ("Zoom");
	gtk_container_add(GTK_CONTAINER(view_menu), zoom_item);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(zoom_item), gtk_image_new_from_stock ("gtk-zoom-in", GTK_ICON_SIZE_MENU));

	GtkWidget* zoom_submenu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(zoom_item), zoom_submenu);

	// TODO make Actions for these two like below
	GtkWidget* zoom1 = gtk_image_menu_item_new_with_mnemonic ("1:1");
	gtk_container_add(GTK_CONTAINER(zoom_submenu), zoom1);
	GtkWidget* ico_zoom1 = gtk_image_new_from_stock ("gtk-zoom-100", GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(zoom1), ico_zoom1);
	void arr_zoom_1(GtkMenuItem* menuitem, gpointer _arrange){ arr_zoom_set(app->latest_arrange, 1.0, 1.0); }
	g_signal_connect((gpointer)zoom1, "activate", G_CALLBACK(arr_zoom_1), NULL);

	GtkWidget* zoom2 = gtk_image_menu_item_new_with_mnemonic ("Song");
	gtk_container_add(GTK_CONTAINER(zoom_submenu), zoom2);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(zoom2), gtk_image_new_from_stock ("gtk-zoom-fit", GTK_ICON_SIZE_MENU));
	g_signal_connect((gpointer)zoom2, "activate", G_CALLBACK(menu_arr_zoom_song), NULL);

	GimpActionGroup* ag[] = {
		global_action_group,
		AYYI_PANEL_CLASS(g_type_class_peek(AYYI_TYPE_ARRANGE))->action_group
	};

	struct _item {
		char* action_name;
		char* icon;
		GCallback callback;
		int ag;
	};

	struct _item zitems[] = {
		{"Zoom In",         "gtk-zoom-in",  G_CALLBACK(menu__on_zoom_in),           0},
		{"Zoom Out",        "gtk-zoom-out", G_CALLBACK(menu__on_zoom_out),          0},
		{"Zoom H Back",     "gtk-zoom-out", G_CALLBACK(arr_zoom_history_back),      1},
		{"Zoom To Selectn", "gtk-new",      G_CALLBACK(menu__on_zoom_to_selection), 1},
	};

	for(int i=0;i<G_N_ELEMENTS(zitems);i++){
		GtkAction* action = gtk_action_group_get_action(GTK_ACTION_GROUP(ag[zitems[i].ag]), zitems[i].action_name);
		GtkWidget* menu_item = gtk_action_create_menu_item(action);
		gtk_container_add(GTK_CONTAINER(zoom_submenu), menu_item);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(menu_item), gtk_image_new_from_stock (zitems[i].icon, GTK_ICON_SIZE_MENU));
		g_signal_connect ((gpointer)menu_item, "activate", zitems[i].callback, NULL);
	}

	menu_instance++;

	return view_item;
}


static void
on_song_new_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	// if an existing song directory, it will also be loaded.
	// TODO look at Gimp file save dialogue - much nicer than the gtk default...
	PF;

	GtkWindow* parent = NULL;

	GtkWidget* dialog = gtk_file_chooser_dialog_new ("Select folder for new Session.",
							parent,
							GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
							NULL);

	//gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), default_folder_for_saving);
	//gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), "New folder");
	//else gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), filename_for_existing_document);

	if(gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT){
		char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		char path[256];
		snprintf(path, 255, "%s/", filename);
		dbg (0, "filename=%s", path);
		song_load_new(path);
		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}


static void
on_song_open_activate (GtkMenuItem* menuitem, gpointer not_used)
{
	// Ideally this would allow either a folder or a .ardour file to be loaded.

	PF;
	GtkWidget* dialog = gtk_file_chooser_dialog_new ("Select a Session to load",
							NULL,
							GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
							NULL);

	if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT){
		char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		char path[256];
		snprintf(path, 255, "%s/", filename);
		dbg (0, "filename=%s", path);
		song_load_new(path); //FIXME
		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}


/*
 *  Open a song without prompting the user.
 *
 *  @param recent_song_num - if set we load one of the recent_files.
 */
static void
on_song_reload_activate (GtkMenuItem* menuitem, gpointer recent_song_num)
{
	PF;
	char path[256] = {0,};

	if(recent_song_num){
		int recent_num = GPOINTER_TO_INT(recent_song_num);
		if(recent_num-- > MAX_RECENT_SONGS){ perr ("bad recent_song_num (%i)", recent_num); return; }

		if(g_strrstr(app->recent_songs[recent_num], ".ardour")){
			pwarn(".ardour - NEVER GET HERE");
			char* dir_name = g_path_get_dirname(app->recent_songs[recent_num]);
			g_strlcpy(path, dir_name, 256);
			g_free(dir_name);
		}else{
			g_strlcpy(path, app->recent_songs[recent_num], 256);
		}
	}else{
		snprintf(path, 255, "%s", ayyi_song__get_file_path());
	}

	song_load_new(path);

	// load the gui data

	char gui_path[256] = {0,};
	snprintf(gui_path, 255, "%.240sgui_data.yaml", path);

	if(!load_gui_song_file(gui_path)) log_print(LOG_FAIL, "song load");
}


static void
on_song_sync_activate (GtkMenuItem* menuitem, gpointer recent_song_num)
{
	song_unload();
	song_load();
}


static void
on_song_save_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	song_save();
}


void
on_song_save_as_activate (GtkMenuItem* menuitem, gpointer user_data)
{
}


static void
on_cut_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	GList* l = am_parts_selection;
	if(l){
		for(;l;l=l->next){
			pwarn("TODO add AYYI_BUF_CUT op to AyyiOp");
		}
		return;
	}
	log_print(0, "No cut/paste objects selected.");
}


static void
on_copy_activate (GtkMenuItem* menuitem, gpointer user_data)
{
}


static void
on_new_track_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	song_add_track(AYYI_AUDIO, 1, NULL, NULL);
}


// "Window" Menu
void
on_window2_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
}


static void
on_about_activate (GtkMenuItem* menuitem, gpointer user_data)
{
	GtkWindow* parent = NULL; //GTK_WINDOW(gtk_widget_get_toplevel(arrange->canvas_widget))

	gtk_show_about_dialog(parent, "version", VERSION, "copyright", "Tim Orford", NULL);
}


static void
on_new_win_activate (GtkMenuItem* menuitem, AyyiPanelClass* class)
{
	PF;
	g_return_if_fail(class);


#if 0
	shell__new_floating(class);
#else
	AyyiPanel* panel = app->active_panel;
	if(panel) window__add_panel_from_menu (panel->window, class);
#endif
}


static void
on_paste_activate (GtkMenuItem* menuitem, gpointer user_data)
{
}


static void
on_delete_activate (GtkMenuItem* menuitem, gpointer user_data)
{
}


GtkWidget*
menu_separator_new (GtkWidget* container)
{
	GtkWidget* separator = gtk_menu_item_new();
	gtk_widget_set_sensitive(separator, FALSE);
	gtk_container_add(GTK_CONTAINER(container), separator);
	return separator;
}


void
menu_add_view_item (const char* name, bool (*value)(AyyiWindow*, gpointer), void (*toggle)(AyyiWindow*, gpointer), gpointer user_data)
{
	if(!view_items){
		view_items = g_array_sized_new(ZERO_TERMINATED, TRUE, sizeof(ViewItem), 5);
	}

	g_array_append_vals(view_items, &(ViewItem){.name = (char*)name, .toggle = toggle, .value = value, .user_data = user_data}, 1);
}


static void
menu_set_view_item (ViewInstances* instance, int i, bool value)
{
	GtkWidget* menu_item = instance->i[i].menu_item;
	if(menu_item){
		if(gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)) != value){
			g_signal_handler_block(menu_item, instance->i[i].signal_id);
			gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item), value);
			g_signal_handler_unblock(menu_item, instance->i[i].signal_id); 
		}
	}
}


void
menu_sync_panel_items (AyyiWindow* window)
{
	g_return_if_fail(window->shell_menus);

	for(int i=0;i<view_items->len;i++){
		ViewItem* item = &g_array_index(view_items, ViewItem, i);
		ViewInstances* instance = &g_array_index(window->shell_menus->view_instances, ViewInstances, i);
		bool value = item->value(window, item->user_data);
		for(int j=0;j<2;j++){
			menu_set_view_item(instance, j, value);
		}
	}
}


void
menu_view_item_enable (int i, bool enabled)
{
	ViewItem* item = &g_array_index(view_items, ViewItem, i);
	if(item){
		window_foreach {
			if(window->shell_menus){
				ViewInstances* instance = &g_array_index(window->shell_menus->view_instances, ViewInstances, i);
				for(int j=0;j<2;j++){
					GtkWidget* menu_item = instance->i[j].menu_item;
					if(menu_item){
						gtk_widget_set_sensitive(menu_item, enabled);
					}
				}
			}
		} end_window_foreach
	}
}


/*
 *  Ensure each shell menu contains an item for each open panel
 */
static void
menu_update_windows ()
{
	if(app->state == APP_STATE_SHUTDOWN) return;

	static guint idle = 0;
	if(idle) return;

	bool menu_update_windows_()
	{
		window_foreach {
			if(!window->menu.window) continue;

			GtkContainer* parent = GTK_CONTAINER(window->menu.window);
			if(!parent) continue;

			// clear old items
			GList* list = gtk_container_get_children(parent);
			GList* l = list->next; // dont delete the first menu item.
			l = l->next;           // dont delete the second.
			for(;l;l=l->next){
				GtkWidget* old_item = l->data;
				if(GTK_IS_WIDGET(old_item)) gtk_widget_destroy(old_item);
			}
			g_list_free(list);

			menu_set_windows(window);

		} end_window_foreach

		idle = 0; // indicate queue has been cleared
		return G_SOURCE_REMOVE;
	}
	idle = g_idle_add((GSourceFunc)menu_update_windows_, NULL);
}


/*
 *  Add a menu item for each open window. The Window menu must be empty (except for "New") before calling this.
 */
static void
menu_set_windows (AyyiWindow* window)
{
	GtkContainer* parent = GTK_CONTAINER(window->menu.window);
	if(!parent) return;

	void panel_present (GtkWidget* widget, AyyiPanel* panel)
	{
		ayyi_panel_present(panel);
	}

	panel_foreach {
		char window_type[64]; ayyi_panel_print_label(panel, window_type);
		GtkWidget* menu_item = gtk_image_menu_item_new_with_mnemonic(window_type);
		GtkWidget* image;
		if((image = gtk_image_new_from_stock (AYYI_PANEL_GET_CLASS(panel)->name, GTK_ICON_SIZE_MENU))) gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM(menu_item), image);
		gtk_container_add(parent, menu_item);
		gtk_widget_show(menu_item);

		g_signal_connect((gpointer)menu_item, "activate", G_CALLBACK(panel_present), panel);
	} end_panel_foreach
}


void
menu_clear (GtkWidget* menu)
{
	GList* list = gtk_container_get_children(GTK_CONTAINER(menu));
	GList* l = list;
	for(;l;l=l->next){
		GtkWidget* old_item = l->data;
		if(GTK_IS_WIDGET(old_item)) gtk_widget_destroy(old_item);
	}
	g_list_free(list);
}


void
menu_update_recent_songs ()
{
	// Currently this remakes the whole File menu, as its the easiest thing to do.
	// TODO use GList* GtkMenuShell->children and gtk_menu_shell_insert() instead

	window_foreach {
		GtkContainer* parent = GTK_CONTAINER(window->menu.file);
		if(!parent) continue;

		GtkAccelGroup* accel_group = app->global_accel_group;

		menu_clear(window->menu.file);

		GtkWidget* song_new = gtk_image_menu_item_new_from_stock ("gtk-new", accel_group);
		gtk_container_add(parent, song_new);

		GtkWidget* song_open = gtk_image_menu_item_new_from_stock ("gtk-open", accel_group);
		gtk_container_add(parent, song_open);

		GtkWidget* song_reload = gtk_image_menu_item_new_with_mnemonic("_Revert");
		gtk_container_add(parent, song_reload);
		GtkWidget* ico_reload = gtk_image_new_from_stock ("gtk-revert-to-saved", GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(song_reload), ico_reload);

		GtkWidget* song_sync = gtk_image_menu_item_new_with_mnemonic("Sync");
		gtk_container_add(parent, song_sync);
		GtkWidget* ico = gtk_image_new_from_stock ("gtk-refresh", GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(song_sync), ico);

		GtkWidget* song_save = gtk_image_menu_item_new_from_stock ("gtk-save", accel_group);
		gtk_container_add(parent, song_save);

		/*
		save_as = gtk_image_menu_item_new_from_stock ("gtk-save-as", accel_group);
		gtk_container_add (GTK_CONTAINER (window->file_menu), save_as1);
		*/

		if(*app->recent_songs[0]){
			menu_separator_new(GTK_WIDGET(parent));

			char item[256]; 
			int i=0; do{
				strcpy(item, app->recent_songs[i]);
				escape_for_menu(item);
				GtkWidget* recent = window->menu.recent_files[i] = gtk_image_menu_item_new_with_mnemonic(item);
				gtk_container_add(parent, recent);
				GtkWidget* ico_recent = gtk_image_new_from_stock("gtk-open", GTK_ICON_SIZE_MENU);
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(recent), ico_recent);
				//gtk_widget_add_accelerator(recent, "activate", accel_group, GDK_R, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

				g_signal_connect((gpointer)recent, "activate", G_CALLBACK(on_song_reload_activate), GINT_TO_POINTER(i+1));
			} while (++i < MAX_RECENT_SONGS && *app->recent_songs[i]);
		}

		menu_separator_new(GTK_WIDGET(parent));

		GtkWidget* quit1 = gtk_image_menu_item_new_from_stock ("gtk-quit", accel_group);
		gtk_container_add (parent, quit1);

		g_signal_connect ((gpointer)song_new,   "activate", G_CALLBACK(on_song_new_activate),       NULL);
		g_signal_connect ((gpointer)song_open,  "activate", G_CALLBACK(on_song_open_activate),      NULL);
		g_signal_connect ((gpointer)song_reload,"activate", G_CALLBACK(on_song_reload_activate),    NULL);
		g_signal_connect ((gpointer)song_sync,  "activate", G_CALLBACK(on_song_sync_activate),      NULL);
		g_signal_connect ((gpointer)song_save,  "activate", G_CALLBACK(on_song_save_activate),      NULL);
		//g_signal_connect ((gpointer) save_as, "activate", G_CALLBACK (on_save_as1_activate),      NULL);
		g_signal_connect ((gpointer)quit1,      "activate", G_CALLBACK(on_quit),                    NULL);

		gtk_widget_show_all(GTK_WIDGET(parent));

  } end_window_foreach
}


void
menu_update_options ()
{
	static guint menu_update_idle = 0;

	bool menu_update (gpointer _)
	{
		window_foreach {
			if(window->menus.canvas_type[0].radio_item){
				AyyiPanel* primary = windows__find_panel(window, AYYI_TYPE_ARRANGE);
				if(primary){
					Arrange* arrange = (Arrange*)primary;
					dbg (2, "instance=%i canvas->type=%i", ayyi_panel_get_instance_num((AyyiPanel*)arrange), arrange->canvas->type);

					int i; for(i=1;i<MAX_CANVAS_TYPES;i++){
						EditMenu* menu = &window->menus.canvas_type[i];
						if(menu->radio_id) g_signal_handler_block((gpointer)menu->radio_item, menu->radio_id);
					}
					EditMenu* active = &window->menus.canvas_type[arrange->canvas->type];
					dbg(2, "setting active: %i: '%s'", arrange->canvas->type, gtk_widget_get_name(active->radio_item));
					gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(active->radio_item), TRUE);

					for(i=1;i<MAX_CANVAS_TYPES;i++){
						EditMenu* menu = &window->menus.canvas_type[i];
						if(menu->radio_id) g_signal_handler_unblock((gpointer)menu->radio_item, menu->radio_id);
						//dbg(0, " active=%i", gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(items[i])));
					}
				}
			}
		} end_window_foreach

		menu_update_idle = 0;
		return G_SOURCE_REMOVE;
	}

	if(!menu_update_idle) menu_update_idle = g_idle_add((GSourceFunc)menu_update, NULL);
}


void
menu_update_history (const char* history_str)
{
	#define MAX_HISTORY 20
	gchar** split = g_strsplit(history_str, "\n", MAX_HISTORY);

	window_foreach {
		GtkContainer* parent = GTK_CONTAINER(window->menu.history);
		if(!parent) continue;

		menu_clear(GTK_WIDGET(parent));

		bool empty = (!split[0] || !strlen(split[0]));
		gtk_widget_set_sensitive(GTK_WIDGET(window->menu.history_item), !empty); //not working.
		if(window->menus.edit.undo) gtk_widget_set_sensitive(window->menus.edit.undo, !empty);

		for(int i=0;i<MAX_HISTORY;i++){
			if(!split[i] || !strlen(split[i])) break;
			dbg(2, "item=%s", split[i]);

			GtkWidget* menu_item = gtk_menu_item_new_with_mnemonic(split[i]);
			gtk_container_add(parent, menu_item);
			gtk_widget_show(menu_item);
		}
	} end_window_foreach

	g_strfreev(split);
}


static void
menu__on_zoom_in (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
	AyyiPanel* panel = app->active_panel;
	g_return_if_fail(panel);

	Ptf zoom = {panel->zoom->value.pt.x * 1.2, panel->zoom->value.pt.y * 1.2};
	observable_point_set(panel->zoom, &zoom.x, &zoom.y);
}


static void
menu__on_zoom_out (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
	AyyiPanel* panel = app->active_panel;
	g_return_if_fail(panel);

	Ptf zoom = {panel->zoom->value.pt.x / 1.2, panel->zoom->value.pt.y / 1.2};
	observable_point_set(panel->zoom, &zoom.x, &zoom.y);
}


static void
menu__on_zoom_to_selection (GtkMenuItem* menuitem, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	g_return_if_fail(panel);
	if(AYYI_IS_ARRANGE(panel)){
		arr_zoom_to_selection((Arrange*)panel);
	}
}


static void
peak_gain_inc_menu (GtkMenuItem* menuitem, gpointer user_data)
{
	arr_peak_gain_inc(app->latest_arrange);
}


static void
peak_gain_dec_menu (GtkMenuItem* menuitem, gpointer user_data)
{
	arr_peak_gain_inc(app->latest_arrange);
}


static void
menu_arr_zoom_song (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
	arr_zoom_song(app->latest_arrange);
}


static void
on_quit (GtkMenuItem* menuitem, gpointer user_data)
{
	app_quit();
}


#ifdef DEBUG
static void
show_core_status ()
{
	windows__ensure(ayyi_log_win_get_type());

	ayyi_print_action_status();

	//FIXME output these to log window
	am_partmanager__print_list();
	ayyi_song__print_pool();
	tracks_print();
	ayyi_song__print_midi_tracks();
}
#endif


static GimpActionGroup* 
get_arrange_action_group ()
{
	GimpActionGroup* action_group = NULL;
	GList* l = action_groups;
	for(;l;l=l->next){
		action_group = l->data;
		if(!strcmp("Arrange", gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)))) break;
	}
	return action_group;
}


