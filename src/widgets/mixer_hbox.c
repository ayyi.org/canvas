/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2008-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * Modified by the GTK+ Team and others 1997-2000.
 */

#include <config.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "ayyi/ayyi_utils.h"
#include "model/ayyi_model.h"
#include "model/song.h"
#include "model/channel.h"
#include "gui_types.h"
#include "panels/mixer/strip.h"

void     set_cursor (GdkWindow*, GdkCursorType);

#include "mixer_hbox.h"

#define DEFAULT_STRIP_WIDTH 40

enum {
  ZOOM_CHANGE,
#ifdef MIXER_HBOX_SIGNALS
  MOVE_HANDLE,
#endif
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

#if 0
static void            mixer_hbox_finalize      (GObject*);
#endif
static void            mixer_hbox_realize       (GtkWidget*);
static void            mixer_hbox_unrealize     (GtkWidget*);
static void            mixer_hbox_size_request  (GtkWidget*, GtkRequisition*);
static void            mixer_hbox_size_allocate (GtkWidget*, GtkAllocation*);
static gint            mixer_hbox_button_press  (GtkWidget*, GdkEventButton*);
static gint            mixer_hbox_button_release(GtkWidget*, GdkEventButton*);
static gint            mixer_hbox_motion_notify (GtkWidget*, GdkEventMotion*);
static gboolean        mixer_hbox_enter_notify  (GtkWidget*, GdkEventCrossing*);
static gboolean        mixer_hbox_leave_notify  (GtkWidget*, GdkEventCrossing*);
//static gboolean mixer_hbox_move_handle   (MixerHBox*, GtkScrollType scroll);
static AyyiMixerStrip* mixer_hbox_get_ch_from_x (MixerHBox*, int x, gboolean* in_gap);
#ifdef NOT_USED
static void            mixer_hbox_print_strips  (MixerHBox*);
#endif

static gint            mixer_hbox_expose        (GtkWidget*, GdkEventExpose*);
static void            draw_background          (GtkWidget*, GdkRectangle*);

struct _op {
	int             start_x;
	int             start_width;
	AyyiMixerStrip* strip;
};
static struct _op op = {0, 0, NULL};

/* BOOLEAN:ENUM (./gtkmarshalers.list:26) */
void
_gtk_marshal_BOOLEAN__ENUM (GClosure     *closure,
                            GValue       *return_value,
                            guint         n_param_values,
                            const GValue *param_values,
                            gpointer      invocation_hint,
                            gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__ENUM) (gpointer     data1,
                                                  gint         arg_1,
                                                  gpointer     data2);
  register GMarshalFunc_BOOLEAN__ENUM callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 2);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__ENUM) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1, g_value_get_enum (param_values + 1), data2);

  g_value_set_boolean (return_value, v_return);
}


G_DEFINE_TYPE (MixerHBox, mixer_hbox, NS_TYPE_BOX)

static void
mixer_hbox_class_init (MixerHBoxClass *class)
{
  GtkWidgetClass *widget_class = (GtkWidgetClass*) class;
  GObjectClass *object_class   = (GObjectClass *) class;

#if 0
  object_class->finalize             = mixer_hbox_finalize;
#endif

  widget_class->realize              = mixer_hbox_realize;
  widget_class->unrealize            = mixer_hbox_unrealize;
  widget_class->size_request         = mixer_hbox_size_request;
  widget_class->size_allocate        = mixer_hbox_size_allocate;
  widget_class->button_press_event   = mixer_hbox_button_press;
  widget_class->button_release_event = mixer_hbox_button_release;
  widget_class->motion_notify_event  = mixer_hbox_motion_notify;
  widget_class->enter_notify_event   = mixer_hbox_enter_notify;
  widget_class->leave_notify_event   = mixer_hbox_leave_notify;
  widget_class->expose_event         = mixer_hbox_expose;

  signals[ZOOM_CHANGE] =
    g_signal_new ("zoom_changed", G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__DOUBLE, G_TYPE_NONE, 1, G_TYPE_DOUBLE);

  //we're not using this signal atm, but it could be useful...
#ifdef MIXER_HBOX_SIGNALS
  class->move_handle                 = mixer_hbox_move_handle;

  signals[MOVE_HANDLE] =
    g_signal_new ("move_handle",
		  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (MixerHBoxClass, move_handle),
                  NULL, NULL,
                  _gtk_marshal_BOOLEAN__ENUM,
                  G_TYPE_BOOLEAN, 1,
                  GTK_TYPE_SCROLL_TYPE);
#endif
}

static void
mixer_hbox_init (MixerHBox *hbox)
{
  hbox->strip_width = DEFAULT_STRIP_WIDTH;
}


GtkWidget*
mixer_hbox_new (gint spacing)
{
  MixerHBox *hbox = g_object_new (MIXER_TYPE_HBOX, NULL);

  NS_BOX(hbox)->spacing = spacing;
  NS_BOX(hbox)->homogeneous = FALSE;

  //g_signal_connect((gpointer)hbox, "move_handle", G_CALLBACK(mixer_hbox_on_move_handle), NULL);

  return GTK_WIDGET (hbox);
}


void
mixer_hbox_add_strip (MixerHBox* mixer, AyyiMixerStrip* strip)
{
	g_return_if_fail(AYYI_IS_MIXER_STRIP(strip));
	dbg(1, "pos=%i", strip->strip_num);

	GtkWidget* child = (GtkWidget*)strip;

#ifdef USE_AYYI_CONTAINER
	ayyi_box_pack_start(AYYI_BOX(mixer), strip, TRUE, TRUE, 0);
	ayyi_box_reorder_child (AYYI_BOX(mixer), strip, strip->strip_num);
#else
	gtk_widget_show_all(child);
	gtk_widget_set_size_request(child, mixer->strip_width, -1);
	gtk_box_pack_start(GTK_BOX(mixer), child, TRUE, TRUE, 0);
	gtk_box_reorder_child (GTK_BOX(mixer), child, strip->strip_num);

	//gtk_widget_queue_draw ((GtkWidget*)mixer);
#endif

	void mixer_hbox_on_strip_delete(GtkObject* widget, Strip* strip)
	{
		//MixerHBox* mixer = (MixerHBox*)((MixerWin*)((AyyiMixerStrip*)strip->fixed)->panel)->hbox;
	}
	g_signal_connect((gpointer)child, "destroy", G_CALLBACK(mixer_hbox_on_strip_delete), strip);

	//mixer_hbox_print_strips(mixer);
}


/*
 *  Width is the base strip width, before any individual strip magnification.
 */
void
mixer_hbox_set_zoom (MixerHBox* mixer_box, double width)
{
	dbg(2, "width=%.2f", width);
#ifdef DEBUG
	if(width > 400 || width < mixer_box->min_zoom) pwarn ("requested zoom out of range. %.2f", width);
#endif

	mixer_box->strip_width = width;

	gtk_widget_queue_resize(GTK_WIDGET(mixer_box));
}


/*
 *  Return the strips position index in the container.
 */
int
mixer_hbox_get_strip_pos (MixerHBox* mixer, AyyiMixerStrip* strip)
{
  NsBox* box = NS_BOX(mixer);
  GList* children = box->children;
  int i = 0;
  for(;children;children=children->next){
    NsBoxChild* child = children->data;
    GtkWidget* widget = child->widget;
    if(widget == strip->e) return i;
    i++;
  }
  return -1;
}


static void
mixer_hbox_realize (GtkWidget *widget)
{
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  MixerHBox* mixer = MIXER_HBOX(widget);

#define SHOW_BACKGROUND //FIXME why does it crash when this is undefined?
  GdkWindowAttr attributes;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.wclass      = GDK_INPUT_ONLY; //invlisible - receives events only. not part of the xwindows heirarchy.
  attributes.x           = widget->allocation.x;
  attributes.y           = widget->allocation.y;
  attributes.width       = widget->allocation.width;
  attributes.height      = widget->allocation.height;
  attributes.cursor      = gdk_cursor_new_for_display (gtk_widget_get_display (widget), GDK_SB_H_DOUBLE_ARROW);
  attributes.event_mask  = gtk_widget_get_events (widget) |
                           (GDK_BUTTON_PRESS_MASK |
                            GDK_BUTTON_RELEASE_MASK |
                            GDK_ENTER_NOTIFY_MASK |
                            GDK_LEAVE_NOTIFY_MASK |
                            GDK_POINTER_MOTION_MASK |
                            //GDK_POINTER_MOTION_HINT_MASK |  //with this disabled we will get a lot of events. use a timer instead?
                            GDK_BUTTON_MOTION_MASK |
#ifdef SHOW_BACKGROUND
                            GDK_EXPOSURE_MASK |
#endif
                            GDK_BUTTON1_MOTION_MASK);
  gint attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_CURSOR;
  //gint attributes_mask = GDK_WA_X | GDK_WA_Y;
//#undef SHOW_BACKGROUND
#ifdef SHOW_BACKGROUND
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
#endif

  //note: 1st arg is parent:
  //mixer->window = gdk_window_new (widget->window, &attributes, attributes_mask);
  widget->window = mixer->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
#ifndef SHOW_BACKGROUND
  g_object_ref (widget->window);
#endif
  //widget->window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);
  widget->style = gtk_style_attach (widget->style, widget->window);
  gdk_window_set_user_data (mixer->window, mixer);
  gdk_cursor_unref (attributes.cursor);
  gdk_window_show (mixer->window);

  //int x, y;
  //gdk_drawable_get_size(mixer->window, &x, &y);

#ifdef SHOW_BACKGROUND
  //gtk_style_set_background (widget->style, widget->window, GTK_STATE_SELECTED);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
#endif
}


static void
mixer_hbox_unrealize (GtkWidget *widget)
{
#ifdef DEBUG
  MixerHBox* mixer = MIXER_HBOX (widget);

  dbg(2, "window=%p", mixer->window);
#endif

  //gtkwidget->unrealize() free's the gdkwindow - we dont have to do anything.

  GTK_WIDGET_CLASS (mixer_hbox_parent_class)->unrealize(widget);
}


#if 0
static void
mixer_hbox_finalize (GObject *object)
{
  PF;
  G_OBJECT_CLASS (mixer_hbox_parent_class)->finalize (object);
}
#endif


static void
mixer_hbox_size_request (GtkWidget* widget, GtkRequisition* requisition)
{
#ifdef USE_AYYI_CONTAINER
	AyyiBoxChild *child;
#else
	GtkBoxChild *child;
#endif
	gint nvis_children;
	gint width;

#ifdef USE_AYYI_CONTAINER
	AyyiBox *box = AYYI_BOX (widget);
#else
	GtkBox *box = GTK_BOX (widget);
#endif
	requisition->width = 0;
	requisition->height = 0;
	nvis_children = 0;

	GList *children = box->children;
	while (children) {
		child = children->data;
		children = children->next;

		if (GTK_WIDGET_VISIBLE (child->widget)) {
			GtkRequisition child_requisition;

			gtk_widget_size_request (child->widget, &child_requisition);

			if (box->homogeneous) {
				width = child_requisition.width + child->padding * 2;
				requisition->width = MAX (requisition->width, width);
			}
			else {
				requisition->width += child_requisition.width + child->padding * 2;
			}

			requisition->height = MAX (requisition->height, child_requisition.height);

			nvis_children += 1;
		}
	}

	if (nvis_children > 0) requisition->width += (nvis_children - 1) * box->spacing;

	requisition->width += NS_CONTAINER (box)->border_width * 2;
	requisition->height += NS_CONTAINER (box)->border_width * 2;

	//requisition->width = 100;
	requisition->height = 0;
	dbg(2, "requisition: x=%i y=%i", requisition->width, requisition->height);
}


static void
mixer_hbox_size_allocate (GtkWidget* widget, GtkAllocation* allocation)
{
  /*

  When increasing a the width of a strip, how do we know whether to increase total width or not?
  Options:
    1- always keep tot width the same unless some strips are already min width.
       This means that when resizing, all other strips will change!
       This is the current approach. Its not that smooth, but is good enough.
       ...this should only apply when scrollbar not visible??? if it is visible we should use option 2?
    2- always preserve width of other channels.
       This means showing scrollbar and increasing width of mixer.
    3- Have an 'intrinsic' strip width, with actual width being sometimes larger to fill the window.
       Messy.

  */
	if(!GTK_WIDGET_REALIZED(widget)) return;

	gdk_window_move_resize (widget->window, allocation->x, allocation->y, allocation->width, allocation->height);

	gboolean fill = TRUE; // if the strips are narrow, we magnify them to fit the whole window.
//FIXME why is fill mode causing recursion?
fill = FALSE;
	NsBoxChild *child;
	GtkAllocation child_allocation;
	gint x = 0;
	gint zoom     = 1 << 8; //magnified to work with integer math.
	gint min_zoom = 1 << 8;

	MixerHBox *mhbox = MIXER_HBOX (widget);
	NsBox *box = NS_BOX (widget);
	widget->allocation = *allocation;

	gint n_vis_children = 0;
	gint total_width_nz = 0;

	int i = 0;
	GList *children = box->children;
	while (children) {
		child = children->data;
		children = children->next;
		AyyiMixerStrip* s = (AyyiMixerStrip*)child->widget;
		gint child_width = mhbox->strip_width * s->hmag;

		if (GTK_WIDGET_VISIBLE (child->widget)) {
			n_vis_children++;
			total_width_nz += child_width;

			//note that the zoomed positions are now calculated twice; here and below.
			int child_width_z = (mhbox->strip_width * s->hmag * zoom);
			x += (child_width_z >> 8) + box->spacing;
		}

		i++;
	}
	if(!total_width_nz) return;
	min_zoom = ((allocation->width - n_vis_children * box->spacing) << 8) / (total_width_nz);

	//the allocation->height is reduced by 6px when the scrollbar is visible!!
	GtkAdjustment* h_adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(mhbox->scrollwin));
	//gtk is using hysteresis? to do this properly we might have to really check the scrollbar widget..
	#define FIX 24 
	gboolean h_scrollbar_visible = ((x - FIX) > allocation->width);
	int scrollbar_height = 0 * h_scrollbar_visible * 20; // <cough>
	//dbg(0, "vis?=%i h=%i %i", h_scrollbar_visible, allocation->height, ABS((x - FIX) - allocation->width));

	if (n_vis_children) {
		if (fill) {
			if(allocation->width > total_width_nz) zoom = min_zoom;
		}

		x = allocation->x + NS_CONTAINER (box)->border_width/* + MIXER_HBOX_SELECTION_BORDER*/;
		child_allocation.y = allocation->y + NS_CONTAINER (box)->border_width/* + MIXER_HBOX_SELECTION_BORDER*/;
		child_allocation.height = MAX (1, (gint) allocation->height - (gint) NS_CONTAINER (box)->border_width * 2 - MIXER_HBOX_SELECTION_BORDER - scrollbar_height);

		//allocate size and position of child strips:
		int i = 0;
		for (children = box->children; children; children = children->next) {
			child = children->data;
			AyyiMixerStrip* s = (AyyiMixerStrip*)child->widget;

			if (GTK_WIDGET_VISIBLE (child->widget)){
				gint child_width = mhbox->strip_width * s->hmag * zoom;
				dbg(3, "child_width=%i", child_width >> 8);

				child_allocation.width = (child_width >> 8)/* - 2 * MIXER_HBOX_SELECTION_BORDER*/;
				child_allocation.x = x;
				//if(child_allocation.x + child_allocation.width > allocation->x + allocation->width) gerr("x allocation is too big: %i > %i", child_allocation.x + child_allocation.width, allocation->x + allocation->width);

				gtk_widget_size_allocate (child->widget, &child_allocation);

				x += (child_width >> 8) + box->spacing/* + MIXER_HBOX_SELECTION_BORDER*/;
				i++;
			}
		}
		dbg(2, "allocated %i out of %i. zoom=%i", x, allocation->width, zoom);
	}

	mhbox->min_zoom = ((double)min_zoom) / 256.0;
	mhbox->zoom = mhbox->strip_width * ((double)zoom) / 256.0;

	g_signal_emit(mhbox, signals[ZOOM_CHANGE], 0, mhbox->zoom);

	h_adj->upper = x; //this is not the same as allocation->width - it can be bigger or smaller.
	gtk_adjustment_changed(h_adj);
#ifdef SCROLLBARS_NOT_AUTOMATIC //currently GTK_POLICY_AUTOMATIC seems to be working fine.
	GtkWidget* scrollbar = gtk_scrolled_window_get_hscrollbar(GTK_SCROLLED_WINDOW(mixer->scrollwin));
	if(h_scrollbar_visible){
		gtk_widget_show(scrollbar);
	} else gtk_widget_hide(scrollbar);
#endif
	dbg(2, "done. width=%i upper=%.2f val=%.2f x=%i", allocation->width, h_adj->upper, h_adj->value, allocation->x);
}


static gint
mixer_hbox_expose (GtkWidget* widget, GdkEventExpose* event)
{
	// FIXME this is not always being called when it should
	// -both hbox and scrolled_window have their expose in GtkContainer which calls
	//  gtk_container_propagate_expose() for each of its children.
	//  Maybe we need to run with debug version of gtk to see if gdk_region_empty() is triggering.

	// On the other hand it might be easier to handle our own scrollbars...
	// ..but would this fix the problem? we're still using GtkContainer (hbox)

	draw_background(widget, &event->area);

	//draw selection boxes:
#ifdef DEBUG
	int content_right = 0;
#endif
	int i = 0;
	GList* l = (NS_BOX(widget))->children;
	for (;l;l=l->next) {
		NsBoxChild* child = l->data;
		AyyiMixerStrip* strip = (AyyiMixerStrip*)child->widget;

		if (GTK_WIDGET_VISIBLE (child->widget)) {
			if (strip__is_selected(strip)) {
				//note that we use the _actual_ child widget allocations here, not the _requested_ dimensions.
				gint real_width = child->widget->allocation.width;
				gtk_paint_flat_box (
					widget->style, widget->window, GTK_STATE_SELECTED, GTK_SHADOW_NONE, &event->area, widget, NULL,
					child->widget->allocation.x, 0,
					real_width + (1 * MIXER_HBOX_SELECTION_BORDER), widget->allocation.height
				);
			}
#ifdef DEBUG
			content_right = child->widget->allocation.x + child->widget->allocation.width;
#endif
		}
		i++;
	}

	//TODO clear background of any remaining space?
	//     see draw_background()
#ifdef DEBUG
	int area_right = event->area.x + event->area.width;
	dbg(2, "right: content=%i area=%i", content_right, area_right);
#endif

	return FALSE;
}


static gint
mixer_hbox_button_press (GtkWidget *widget, GdkEventButton *event)
{
	PF;
	g_return_val_if_fail(widget, FALSE);
	g_return_val_if_fail(MIXER_IS_HBOX(widget), FALSE);
	g_return_val_if_fail(event, FALSE);

	int handled = FALSE;

	if(event->button != 1) return handled;

	MixerHBox* mixer = MIXER_HBOX(widget);

	gtk_widget_grab_focus(widget);
	gtk_grab_add(widget);
	mixer->button = event->button;

	if((op.strip = mixer_hbox_get_ch_from_x(mixer, event->x, NULL))){
		op.start_x = event->x;
		op.start_width = ((GtkWidget*)op.strip)->allocation.width;
		dbg(0, "ch=%i", op.strip->channel->ident.idx);
	}

	return handled = TRUE;
}


static gint
mixer_hbox_button_release(GtkWidget* widget, GdkEventButton* event)
{
  PF;
  g_return_val_if_fail(widget, FALSE);
  g_return_val_if_fail(MIXER_IS_HBOX(widget), FALSE);
  g_return_val_if_fail(event, FALSE);

  MixerHBox* mixer = MIXER_HBOX(widget);

  if (mixer->button == event->button){
    gtk_grab_remove (widget);

    mixer->button = 0;
  }

  return FALSE;
}


static gint
mixer_hbox_motion_notify(GtkWidget* widget, GdkEventMotion* event)
{
  g_return_val_if_fail(widget, FALSE);
  g_return_val_if_fail(MIXER_IS_HBOX(widget), FALSE);
  g_return_val_if_fail(event, FALSE);
  int handled = FALSE;

  MixerHBox* mixer = MIXER_HBOX(widget);

  switch(mixer->button){
    case 1:
      {
        int    old_strip_width_z  = op.start_width;
        double new_strip_width_z  = old_strip_width_z + (event->x - op.start_x);
        double new_strip_width_nz = new_strip_width_z/* / mixer->hmag*/;
        if((op.strip) && (new_strip_width_z > MIN_STRIP_WIDTH)){
          op.strip->hmag = new_strip_width_nz / mixer->strip_width;
/*
          //gdk_drawable_get_size(GdkDrawable *drawable, gint *width, gint *height);
          GdkRectangle rect;
          gdk_window_get_frame_extents(widget->window, &rect);
          draw_background(widget, &rect);
          //now we need to draw the selection box. Is strip->hmag set? yes, looks like it.
*/
          gtk_widget_set_size_request((GtkWidget*)op.strip, (int)new_strip_width_z, -1);
          gtk_widget_queue_draw (widget);
        }
      }
      handled = TRUE;
      break;
    default:
      break;
  }

  gint x = event->x;

  gboolean in_gap = false;
  mixer_hbox_get_ch_from_x(mixer, x, &in_gap);
  if(in_gap && x > 4){
    set_cursor(widget->window, CURSOR_H_DOUBLE_ARROW);
  }
  else set_cursor(widget->window, 0);

  return handled;
}


static gboolean
mixer_hbox_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  return FALSE;
}


static gboolean
mixer_hbox_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  set_cursor(widget->window, 0);
  return FALSE;
}


#ifdef MIXER_HBOX_SIGNALS
static gboolean
mixer_hbox_move_handle (MixerHBox *mixer, GtkScrollType scroll)
{
	//this can perhaps be removed.
	PF;
	return FALSE;
}
#endif


static AyyiMixerStrip*
mixer_hbox_get_ch_from_x(MixerHBox *mixer, int x, gboolean *in_gap)
{
	// @in_gap can be NULL

	int i = 0;
	AyyiMixerStrip* strip = NULL;
	GList* l = (NS_BOX(mixer))->children;
	for (;l;l=l->next) {
		NsBoxChild* child = l->data;
		int left = child->widget->allocation.x;
		int right = left + child->widget->allocation.width;
		dbg(4, "left=%i right=%i x=%i", left, right, x);
		if(x < right){
			if(in_gap) *in_gap = TRUE;
			if(x > left) if(in_gap) *in_gap = FALSE;
			return strip;
		}
		strip = (AyyiMixerStrip*)child->widget;
		i++;
	}

	return NULL;
}


static void
draw_background (GtkWidget *widget, GdkRectangle *area)
{
	MixerHBox *mhbox = MIXER_HBOX (widget);

#ifdef SHOW_BACKGROUND
	if (GTK_WIDGET_DRAWABLE (widget)) {

	GtkAdjustment* adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(mhbox->scrollwin));

	if (!GTK_WIDGET_NO_WINDOW (widget)){
		if (!GTK_WIDGET_APP_PAINTABLE (widget))
			gtk_paint_flat_box (widget->style, widget->window,
				widget->state, GTK_SHADOW_NONE,
				area, widget, NULL,

				//neither of these options make any difference, as we're not getting expose events
#if 0
				//0, 0,
				1 + widget->allocation.x, 1 + widget->allocation.y,
				widget->allocation.width - 2,
				widget->allocation.height - 2
#else
				area->x + adj->value, area->y, area->width, area->height
#endif
				/*-1, -1*/);
		}
	}
#endif
}


void
mixer_hbox_on_selection_change (GtkWidget* widget)
{
  gtk_widget_queue_draw (widget); // can we do better than this?
}


#ifdef NOT_USED
static void
mixer_hbox_print_strips (MixerHBox* mixer)
{
	PF;
	GList* l = (NS_BOX(mixer))->children;
	for (;l;l=l->next) {
		NsBoxChild* child = l->data;
		AyyiMixerStrip* strip = (AyyiMixerStrip*)child->widget;
		GtkWidget* widget = (GtkWidget*)child->widget;
		int width = widget->allocation.width;
		printf("  %i %i %s\n", strip->strip_num, width, strip__get_ch_name(strip));
	}
}
#endif

#define __mixer_hbox_c__
