/*
  This file is part of the Ayyi Project. http://www.ayyi.org
  copyright (C) 2004-2017 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ------------------------------------------------------------------

  Appears to be an abstract base class for panels using lv2 plugins?

*/
#define __lv2_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>
#include "toolbar.h"
#include "../lv2.h"
#include "panels/lv2.h"

extern SMConfig* config;

G_DEFINE_TYPE (AyyiLv2Win, ayyi_lv2_win, AYYI_TYPE_PANEL)

static GObject*   lv2_win__constructor (GType, guint n_construct_properties, GObjectConstructParam*);
static GtkWidget* lv2_win__new         (AyyiWindow*, Size*, GtkOrientation);
static void       lv2_win__on_ready    (AyyiPanel*);
static void       lv2_win__on_realise  (GtkWidget*, gpointer);
static void       lv2_win__on_allocate (GtkWidget*, gpointer);
static bool       lv2_win__on_timeout  (gpointer);
static void       lv2_win__update      (AyyiLv2Win*);

#define lv2_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_LV2_WIN) continue; \
		AyyiLv2Win* lv2 = (AyyiLv2Win*)panel;
#define end_lv2_foreach end_panel_foreach


static void
ayyi_lv2_win_class_init (AyyiLv2WinClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	g_object_class->constructor = lv2_win__constructor;

	panel->name           = "Lv2";
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F14;
	panel->default_size.x = 280;
	panel->default_size.y = 350;
	panel->new            = lv2_win__new;
	panel->on_ready       = lv2_win__on_ready;

	PanelType* dtype = g_malloc(sizeof(PanelType));
	*dtype = AYYI_TYPE_LV2_WIN;
	g_hash_table_insert(app->panel_classes, dtype, NULL);
}


static void
ayyi_lv2_win_init(AyyiLv2Win* object)
{
}


static GObject*
lv2_win__constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_ > -1) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_lv2_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_LV2_WIN);

	return g_object;
}


static GtkWidget*
lv2_win__new(AyyiWindow* _, Size* config_size, GtkOrientation orientation)
{
	AyyiLv2Win* self = AYYI_LV2_WIN (g_object_new (AYYI_TYPE_LV2_WIN, "behavior", GDL_DOCK_ITEM_BEH_NORMAL, "orientation", orientation, NULL));
	return (GtkWidget*)self;
}


static void
lv2_win__on_ready(AyyiPanel* panel)
{
	GList* widgets = gtk_container_get_children((GtkContainer*)panel->vbox);
	bool done = g_list_length(widgets) > 1;
	g_list_free(widgets);
	if (done) return;

	GtkWidget* win = panel->shell->widget;

	g_signal_connect((gpointer)win, "realize", G_CALLBACK(lv2_win__on_realise), NULL);
	g_signal_connect((gpointer)win, "size-allocate", G_CALLBACK(lv2_win__on_allocate), NULL);

	g_timeout_add(100, lv2_win__on_timeout, NULL); //should be per class, not per window.

	//GtkWidget* w = lv2_instantiate("http://calf.sourceforge.net/plugins/Compressor");
	//GtkWidget* w = lv2_instantiate_by_path (g_strdup_printf("file://%s/%s/", config->plugindir, "amp.lv2"));
	GtkWidget* w = lv2_instantiate_by_path ("file://" PACKAGE_LIB_DIR "/../ayyi_meter/meter.lv2/");
	if (w) {
		panel_pack(w, EXPAND_FALSE);
	}
}


static void
lv2_win__on_realise (GtkWidget* window, gpointer user_data)
{
}


static void
lv2_win__on_allocate (GtkWidget* window, gpointer user_data)
{
	// remove size forcing
	static gboolean first_time = TRUE;
	if (first_time &&  GTK_WIDGET_REALIZED(window)) {
		gtk_widget_set_size_request(window, 20, 20);
		first_time = FALSE;
	}
}


static bool
lv2_win__on_timeout(gpointer data)
{
	// TODO this timer is currently never stopped.

	bool dont_stop = true;
	lv2_foreach {
		lv2_win__update(lv2);
	} end_lv2_foreach;
	return dont_stop;
}


static void
lv2_win__update (AyyiLv2Win* win)
{
	AyyiPluginPtr plugin = ayyi_client_get_plugin("Ayyi Lv2 Plugin");
	if (!plugin) return;
	/*
	Lv2Symbols* lv2 = plugin->symbols;
	//dbg(0, "symbols=%p", lv2);

	if(lv2){
		win->meterlevel = 100 * lv2->get_meterlevel(plugin);
		dbg(2, "level=%.2f", win->meterlevel);
		gtk_widget_queue_draw_area(win->area, 0, 0, win->area->allocation.width, win->area->allocation.height);
	}
	*/
}
