/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
/*
 *  part_renamer.c - a popup text entry box used for renaming tracks and parts.
 *
 */

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include "model/am_part.h"
#include "arrange.h"
#include "part_renamer.h"

struct
{
	GtkWidget*      popup;
	GtkWidget*      entry;
	AyyiPanel*      panel;
	GtkWidget*      parent;
	AMPart*         part;
	AMTrack*        track;
	char            old_text[AYYI_NAME_MAX];

	RenamerCallback done;
	gpointer        user_data;

} renamer = {NULL, NULL};

static void rename_popup(AyyiPanel*, GtkWidget*, Pti, RenamerCallback, gpointer);


static void
rename_popup_close(GtkWidget* widget)
{
	const char* new_text = gtk_entry_get_text(GTK_ENTRY(renamer.entry));
	dbg (1, "text=%s", gtk_entry_get_text(GTK_ENTRY(renamer.entry)));
	if(strcmp(new_text, renamer.old_text)){
		//TODO remove this
		g_strlcpy(renamer.part ? renamer.part->name : renamer.track->name, new_text, AYYI_NAME_MAX);
	}

	call(renamer.done, (char*)new_text, renamer.user_data);

	gtk_widget_destroy(renamer.popup);
	renamer.popup = NULL;
}


static gboolean
rename_popup_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
	PF;
	if (event->keyval == GDK_Escape ||
		event->keyval == GDK_KP_Tab ||
		event->keyval == GDK_ISO_Left_Tab)
		{
		rename_popup_close(widget);
		return HANDLED;
	}

	switch (event->keyval) {
		case GDK_Tab:
		case GDK_Return:
			rename_popup_close(widget);
			return HANDLED;
			break;
		default:
			break;
	}

	return NOT_HANDLED;
}


static void
rename_popup_set_position (gint x, gint y)
{
	gtk_window_move(GTK_WINDOW(renamer.popup), x, y);
}


static void
canvas_rename_popup_modify_position (gint* _x, gint* _y)
{
	// Apply screen offset and bounds to x and y

	g_return_if_fail(renamer.panel);
	GdkWindow* window = ((GtkWidget*)renamer.panel->window)->window;

	gint canvas_x, canvas_y;
	gdk_window_get_origin(renamer.parent->window, &canvas_x, &canvas_y);

	gint entry_x, entry_y;
	gdk_window_get_origin(window, &entry_x, &entry_y);
	gint width, height;
	gdk_drawable_get_size(window, &width, &height);
	GtkRequisition requisition;
	gtk_widget_size_request(renamer.popup, &requisition); //get preferred size

	int xscrolloffset, yscrolloffset;
	((Arrange*)renamer.panel)->canvas->get_scroll_offsets((Arrange*)renamer.panel, &xscrolloffset, &yscrolloffset);

	GdkScreen* screen = gdk_drawable_get_screen(window);
	gint monitor_num = gdk_screen_get_monitor_at_window (screen, window);
	GdkRectangle monitor;
	gdk_screen_get_monitor_geometry (screen, monitor_num, &monitor);

	int x = CLAMP(canvas_x + *_x - xscrolloffset, 0, gdk_screen_get_width(screen) - requisition.width);
	int y = MIN(canvas_y + *_y -1, gdk_screen_get_height(screen) - requisition.height);

	*_x = x, *_y = y;
}


void
track_rename_popup(AyyiPanel* panel, GtkWidget* parent, Pti position, gboolean map_type, AMTrack* track, RenamerCallback done, gpointer user_data)
{
	if(renamer.popup) return; //already open.

	renamer.track = track;
	renamer.part = NULL;
	rename_popup(panel, parent, position, done, user_data);
}


void
part_rename_popup(AyyiPanel* panel, GtkWidget* parent, Pti position, gboolean map_type, AMPart* part, RenamerCallback done, gpointer user_data)
{
	if(renamer.popup) return; //already open.

	renamer.part = part;
	renamer.track = NULL;
	rename_popup(panel, parent, position, done, user_data);
}


static void
rename_popup(AyyiPanel* panel, GtkWidget* parent, Pti position, RenamerCallback done, gpointer user_data)
{
	int x = position.x, y = position.y;

	renamer.panel = panel;
	renamer.parent = parent;
	renamer.done = done;
	renamer.user_data = user_data;

	renamer.popup = gtk_window_new(GTK_WINDOW_POPUP);
	gtk_window_set_modal(GTK_WINDOW(renamer.popup), TRUE);

	canvas_rename_popup_modify_position(&x, &y);

	char* name = renamer.part ? renamer.part->name : renamer.track->name;
	GtkWidget* entry = renamer.entry = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(entry), AYYI_NAME_MAX);
	gtk_entry_set_text(GTK_ENTRY(entry), name);
	gtk_container_add(GTK_CONTAINER(renamer.popup), entry);
	gtk_widget_show (entry);

	g_signal_connect((gpointer)renamer.popup, "focus-out-event", G_CALLBACK(rename_popup_close), NULL);
	gtk_widget_show(renamer.popup);
	rename_popup_set_position(x, y);
	g_signal_connect (renamer.popup, "key_press_event", G_CALLBACK(rename_popup_key_press_event), NULL);

	GdkEvent* fevent = gdk_event_new (GDK_FOCUS_CHANGE);
	g_object_ref (entry);
	GTK_WIDGET_SET_FLAGS (entry, GTK_HAS_FOCUS);

	fevent->focus_change.type = GDK_FOCUS_CHANGE;
	fevent->focus_change.window = g_object_ref (entry->window);
	fevent->focus_change.in = TRUE;

	gtk_widget_event (entry, fevent);

	g_object_notify (G_OBJECT (entry), "has-focus");

	g_strlcpy(renamer.old_text, name, AYYI_NAME_MAX);
}


