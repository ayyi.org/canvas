/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "global.h"
#include "src/window.statusbar.h"
#include "windows.h"


static void
_log_print (AyyiLogType type, const char* text)
{
	char* str = g_strdup_printf("%s%s", text, type==LOG_OK ? " ok" : /*type==LOG_FAIL ? " fail!" :*/ "");
	if(app->state == APP_STATE_SONG_LOADED){
		window_foreach {
			shell__statusbar_print(window, STATUSBAR_MAIN, (char*)str);
			if(window->statusbar){
				if(type){
					if(window->statusbar->widget[STATUSBAR_ICON]){
						#ifdef HAVE_GTK_2_12
						gtk_widget_set_tooltip_text(window->statusbar->widget[STATUSBAR_ICON], str);
						#endif
						gtk_widget_show(window->statusbar->widget[STATUSBAR_ICON]);
					}
					switch(type){
						case LOG_FAIL:
							statusbar__set_icon(window->statusbar, GTK_STOCK_DIALOG_WARNING);
							break;
						case LOG_OK:
							statusbar__set_icon(window->statusbar, GTK_STOCK_APPLY);
							break;
					}
				}
				else if(window->statusbar->widget[STATUSBAR_ICON]) gtk_widget_hide(window->statusbar->widget[STATUSBAR_ICON]);
			}
		} end_window_foreach
	}
	g_free(str);
}


void
log_print_ok ()
{
}


AyyiLogImpl logger = {
	_log_print,
	log_print_ok,
};


void
statusbar__enable_logging ()
{
	ayyi_log_add_logger(&logger)
}

