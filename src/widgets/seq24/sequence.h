//----------------------------------------------------------------------------
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------
#ifndef __sequence_h__
#define __sequence_h__
#include "seq24/event.h"

struct _midi_sequence
{
	GList* events; //list of type struct _midi_event;

	GList* iterator_draw;

    long time_beats_per_measure;
    long time_beat_width;
    long rec_vol;
};
typedef struct _midi_sequence Sequence;


typedef enum
{
    DRAW_FIN = 0,
    DRAW_NORMAL_LINKED,
    DRAW_NOTE_ON,
    DRAW_NOTE_OFF
} DrawType;


Sequence* sequence__new                 ();
DrawType  sequence__get_next_note_event (Sequence*, long *a_tick_s, long *a_tick_f, int *a_note, gboolean * a_selected, int *a_velocity);


#endif //__sequence_h__
