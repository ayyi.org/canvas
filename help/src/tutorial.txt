<a href="index.html"><font color="#ff6600">AyyiGtk</font></a>/<a href="developers.html"><COLOUR2>Developers</COLOUR2></a>/Tutorial
======================================================================

*How do i make my application into a seismix client?*

This can be a relatively simple process. Below is an example of a C program
that connects to the dae-core and imports the Mixer SHM segment from another client.

See also the <a href="tutorial_cpp.html">c++ example</a>

<pre>
/*
 * A simple tutorial for Ayyi C clients.
 *
 * It demonstrates how to connect to a server, obtain and modify a Song object,
 * and how to be notified of remote model changes.
 * It runs in a shell, and outputs only debugging information, though changes
 * will be visible in other connected clients.
 *
 * For more advanced examples see the test/vala and model/test directories
 */
#include &lt;stdlib.h&gt;
#include &lt;gtk/gtk.h&gt;
#include "model/ayyi_model.h"

static void on_new_part  (AMPart*);
static void on_connected ();


int main (int argc, char *argv[])
{
	// Initialise the Ayyi Model library.
	am__init (NULL);

	// Connect to the default Ayyi service.
	// Once connection is complete, execution continues in on_connected().
	am__connect_all(on_connected, NULL);

	g_main_loop_run (g_main_loop_new (NULL, 0));

	return EXIT_SUCCESS;
}


static void
on_connected(GError* error, gpointer user_data)
{
	if (error){
		dbg (0, "server connection failed. %s", error ? error->message : "");
		exit(EXIT_FAILURE);
	}

	// We now have access to the Song object in shared memory
	// and can send and receive messages.

	// Add callbacks for server events.
	// In this example, the Parts 'add' signal will notify us when a new 
	// Part has been created, either by us, or another client.
	g_signal_connect(song->parts, "add", G_CALLBACK(on_new_part), NULL);

	ayyi_discover_clients();

	// get the list of Parts in the current Song

	GList* parts = song->parts->list;
	if (!parts) return;

	// move the first Part 1 bar later

	AMPart* part = parts->data;
	AyyiSongPos position = part->start;

	// offset the position by 4 beats, 0 sub-beats, and 0 ticks
	ayyi_pos_add (&position, &(AyyiSongPos){4, 0, 0});

	// request that the part be moved to the new position
	am_part__move (part, &position, part->track, NULL, NULL);
}


static void
on_new_part(AMPart* new_part)
{
	printf("new part created on server: %s\n", new_part->name);
}
</pre>


