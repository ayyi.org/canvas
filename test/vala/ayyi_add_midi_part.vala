using GLib;
using Gdk;
using Ayyi;

public class Ayyi.AddmidipartClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new AddmidipartClient();

		return app.run(((AddmidipartClient*)app)->on_shm)
			? 0
			: 1;
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		char* n = AyyiSong.container_next_name(&((SongService*)client->services[0])->song->midi_regions, "Midi Part");

		AyyiMidiTrack* track = AyyiSong.midi_track_next(null);
		if(track == null){
			print("no midi tracks\n");
			abort();
			return;
		}

		GPos l = {4, 0, 0};
		uint64 len = AM.pos2mu(&l);
		SongPos pos = {1, 0, 0};
		AM.Song.add_part(MediaType.MIDI, -1, null, track->shm_idx, &pos, len, 0, n, 0, done);
	}

	private void done(Ident ident, GLib.Error* error)
	{
		log_print(LogType.OK, "done.");
		quit();
	}

	private void abort()
	{
		log_print(LogType.FAIL, "aborted.");
		app.quit();
	}

}


