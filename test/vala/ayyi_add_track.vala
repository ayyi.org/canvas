/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

[CCode(cheader_filename = "ayyi_client.h")]

public class Ayyi.AddtrackClient : Ayyi.SimpleClient
{
	public static int main (string[] args)
	{
		app = new AddtrackClient();

		if(!app.run(((AddtrackClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm ()
	{
		string _name = "New Track";
		char* name = AyyiSong.container_next_name(&((SongService*)client->services[0])->song->audio_tracks, _name);
		AM.Song.add_track(TrackType.AUDIO, name, 1, (obj, error) => {
			AyyiTrack* tr = AyyiSong.get_audio_track(obj.idx);
			print("track added: name=%s\n", (string)tr->name);
			log_print(LogType.OK, "done.");
			quit();
		});
	}
}
