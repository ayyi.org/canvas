/*
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __audtioner_c__
#include "config.h"
#include "global.h"
#include <dbus/dbus-glib-bindings.h>

#include "model/pool_item.h"
#include "support.h"
#include "auditioner.h"

#define APPLICATION_SERVICE_NAME "org.ayyi.Auditioner.Daemon"
#define DBUS_APP_PATH            "/org/ayyi/auditioner/daemon"
#define DBUS_INTERFACE           "org.ayyi.auditioner.Daemon"

typedef struct _auditioner Auditioner;
static Auditioner* auditioner = NULL;


void
auditioner_connect ()
{
	auditioner = g_new0(Auditioner, 1);

	gboolean _auditioner_connect(gpointer _)
	{
		GError* error = NULL;
		DBusGConnection* _auditioner = dbus_g_bus_get(DBUS_BUS_SESSION, &error);
		if(!_auditioner){ 
			errprintf("failed to get dbus connection\n");
			return FALSE;
		}   
		if(!(auditioner->proxy = dbus_g_proxy_new_for_name (_auditioner, APPLICATION_SERVICE_NAME, DBUS_APP_PATH, DBUS_INTERFACE))){
			errprintf("failed to get Auditioner\n");
			return FALSE;
		}

		return G_SOURCE_REMOVE;
	}
	g_idle_add(_auditioner_connect, NULL);
}


#if 0
void
auditioner_play (AMPoolItem* sample)
{
	dbg(0, "%s", sample->filename);
	dbus_g_proxy_call_no_reply(auditioner->proxy, "StartPlayback", G_TYPE_STRING, sample->filename, G_TYPE_INVALID);
}


void
auditioner_stop (AMPoolItem* sample)
{
	dbg(0, "...");
	dbus_g_proxy_call_no_reply(auditioner->proxy, "StopPlayback", G_TYPE_STRING, sample->filename, G_TYPE_INVALID);
}
#endif


void
auditioner_toggle (AMPoolItem* sample)
{
	if(!auditioner) auditioner_connect();
	dbus_g_proxy_call_no_reply(auditioner->proxy, "TogglePlayback", G_TYPE_STRING, sample->filename, G_TYPE_INVALID);
}
