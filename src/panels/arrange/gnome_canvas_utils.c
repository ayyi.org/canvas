/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2004-2017 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#include "global.h"
#include <libgnomecanvas/libgnomecanvas.h>
#include <libart_lgpl/libart.h>
#include <libart_lgpl/art_rgba.h>
#include "gnome_canvas_utils.h"


void
canvas_item_grab(GnomeCanvasItem* item, GdkEvent* event, int cursor)
{
	GdkCursor* fleur = gdk_cursor_new(cursor);
	gnome_canvas_item_grab(item,
				  GDK_POINTER_MOTION_MASK |
				  GDK_BUTTON_RELEASE_MASK,
				  fleur,
				  event->button.time);
	gdk_cursor_unref(fleur);
}


void
pixbuf_draw_line(GdkPixbuf* pixbuf, DRect* pts, double line_width, GdkColor* colour)
{
  //FIXME this leaks - dont know why - replace with cairo functions instead of libart.

  //the pixbuf buffer can be either 24 or 32bits per pixel.

  //this fns draw non-antialiased lines, so strange effects can occur with non-integer pts.

  art_u8 *buffer = (art_u8*)gdk_pixbuf_get_pixels(pixbuf);
  int bufwidth  = gdk_pixbuf_get_width    (pixbuf);
  int bufheight = gdk_pixbuf_get_height   (pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride(pixbuf);

  art_u32 color_fg = ((colour->red/256)<< 24) | ((colour->green/256)<<16) | ((colour->blue/256)<<8) | (0xff); // RRGGBBAA

  //make vector:
  static ArtVpath* vec = NULL;
  if(!vec) vec = art_new(ArtVpath, 10);
  vec[0].code = ART_MOVETO;
  vec[0].x = pts->x1;
  vec[0].y = pts->y1;
  vec[1].code = ART_LINETO;
  vec[1].x = pts->x2;
  vec[1].y = pts->y2;
  vec[2].code = ART_END;

  ArtSVP* svp = art_svp_vpath_stroke(vec,
                             ART_PATH_STROKE_JOIN_ROUND,//ArtPathStrokeJoinType join,
                             ART_PATH_STROKE_CAP_BUTT,//ArtPathStrokeCapType cap,
                             line_width,//double line_width,
                             1.0, //??????? double miter_limit,
                             1.0);//double flatness
  //render to buffer:
  art_rgb_svp_alpha(svp, 0, 0,
                    bufwidth, bufheight,//width, height,
                    color_fg, buffer,
                    rowstride,//number of bytes in each row.
                    NULL);
  //art_free(vec);
  art_free(svp);
}


/*
 *  Non-transparent antialiased rendering.
 *
 *  art_rgb_svp_aa overwrites the whole buffer, so we have to do all drawing in one operation.
 *
 *  The pixbuf buffer must be 24bits per pixel!!
 */
void
pixbuf_draw_path_aa(GdkPixbuf* pixbuf, VPath* vec, double line_width, GdkColor* colour, uint32_t colour_bg)
{
  art_u8 *buffer = (art_u8*)gdk_pixbuf_get_pixels(pixbuf);
  int bufwidth  = gdk_pixbuf_get_width    (pixbuf);
  int bufheight = gdk_pixbuf_get_height   (pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride(pixbuf);

  art_u32 color_fg = ((colour->red/256)<< 16) | ((colour->green/256)<<8) | ((colour->blue/256)); // RRGGBB
  art_u32 color_bg_rgb = colour_bg >> 8;

  ArtSVP *svp = art_svp_vpath_stroke((ArtVpath*)vec,
                             ART_PATH_STROKE_JOIN_ROUND, // ArtPathStrokeJoinType join,
                             ART_PATH_STROKE_CAP_BUTT, // ArtPathStrokeCapType cap,
                             line_width, // double line_width,
                             1.0,  // ??????? double miter_limit,
                             1.0); // double flatness

  // render to buffer
  art_rgb_svp_aa(svp, 0, 0, 
                    bufwidth, bufheight, // width, height,
                    color_fg, color_bg_rgb, buffer,
                    rowstride, // number of bytes in each row.
                    NULL);
  free(vec);
  free(svp);
}


void
pixbuf_fill_rgb(GdkPixbuf* pixbuf, GdkColor* colour)
{
	//this appears to just write over the whole pixbuf.

	//what format does the pixbuf have to be? 24bit?

	art_u8 *buffer = (art_u8*)gdk_pixbuf_get_pixels(pixbuf);
	int bufwidth  = gdk_pixbuf_get_width(pixbuf);
	int bufheight = gdk_pixbuf_get_height(pixbuf);

	//art_rgba_run_alpha(buffer, colour->red/256, colour->green/256, colour->blue/256, 0xff, bufwidth*bufheight);
	art_rgb_fill_run(buffer, colour->red/256, colour->green/256, colour->blue/256, bufwidth*bufheight);
}


