/**
 * @file plugin.c Liferea plugin implementation
 * 
 * Copyright (C) 2005-2006 Lars Lindner <lars.lindner@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#define LIBPREFIX "lib"

#include <gmodule.h>
#include <string.h>
#include <gtk/gtk.h>

#include "html_plugin.h"
#include "ui_htmlview.h"

#include "ayyi/ayyi_utils.h"

/* plugin managment */

/** list of all loaded plugins */
static GSList *plugins = NULL;

typedef	HtmlPluginPtr (*infoFunc)();

//temporarily needed for externally compiled plugins
unsigned long debug_level;
GtkWidget* mainwindow;

//char* lib_path = PACKAGE_LIB_DIR;
#define local_lib_path "../libs/html/plugins/.libs"


static HtmlPluginPtr
plugin_mgmt_load(const gchar * filename)
{
	HtmlPluginPtr plugin = NULL;
	infoFunc	plugin_get_info;
	gboolean	success = FALSE;

	//FIXME is done twice!
	gchar* path = (g_file_test(local_lib_path, G_FILE_TEST_IS_DIR)) ? g_strdup_printf(local_lib_path G_DIR_SEPARATOR_S "%s", filename) : g_strdup_printf(PACKAGE_LIB_DIR G_DIR_SEPARATOR_S "%s", filename);

#if GLIB_CHECK_VERSION(2,3,3)
	GModule* handle = g_module_open(path, G_MODULE_BIND_LOCAL);
#else
	GModule* handle = g_module_open(path, 0);
#endif

	if(!handle) {
		pwarn("Cannot open %s (%s)!", path, g_module_error());
		g_free(path);
		return NULL;
	}

	g_free(path);

	if(g_module_symbol(handle, "plugin_get_info", (void*)&plugin_get_info)) {
		/* load generic plugin info */
		if(NULL != (plugin = (*plugin_get_info)())) {
			/* check plugin version */
			if(PLUGIN_API_VERSION != plugin->api_version)
				dbg(0, "API version mismatch: \"%s\" (%s, type=%d) has version %d should be %d", plugin->name, filename, plugin->type, plugin->api_version, PLUGIN_API_VERSION);

			/* try to load specific plugin type symbols */
			switch(plugin->type) {
				case PLUGIN_TYPE_HTML_RENDERER:
					success = htmlview_plugin_register (plugin, handle);
					break;
				default:
					if(plugin->type >= PLUGIN_TYPE_MAX) {
						dbg(0, "Unknown or unsupported plugin type: %s (%s, type=%d)", plugin->name, filename, plugin->type);
					} else {
						success = TRUE;		/* no special initialization */
					}
					break;
			}
		}
	} else {
		pwarn("file %s is not a valid html renderer plugin", filename);
	}

	if(!success) {
		g_module_close(handle);
		return NULL;
	}

	return plugin;
}

gboolean
html_plugin_mgmt_init(void)
{
	guint filenamelen;
	gchar* filename;
	HtmlPluginPtr plugin = NULL;
	GError* error  = NULL;

	if(!g_module_supported()) g_error("Modules not supported! (%s)", g_module_error());

	gchar* lib_path = (g_file_test(local_lib_path, G_FILE_TEST_IS_DIR)) ? local_lib_path : PACKAGE_LIB_DIR;

	dbg(2, "Scanning for plugins (%s):", lib_path);
	GDir* dir = g_dir_open(lib_path, 0, &error);
	if(!error) {
		/* The expected library name syntax: 

		       <LIBPREFIX>li<type><name>.<library extension> 

		   Examples:
		      liblihtmlg.so
		      liblihtmlm.so
		      liblifldefault.so
		      libliflopml.so
		*/
		filenamelen = 5 + strlen(G_MODULE_SUFFIX);
		filename = (gchar *)g_dir_read_name(dir);
		while(filename) {
			dbg(2, "testing %s...", filename);
			if((filenamelen < strlen(filename)) && (0 == strncmp(LIBPREFIX "li", filename, 5))) {
				/* now lets filter the files with correct library suffix */
				if(!strncmp(G_MODULE_SUFFIX, filename + strlen(filename) - strlen(G_MODULE_SUFFIX), strlen(G_MODULE_SUFFIX))) {
					/* If we find one, try to load plugin info and if this
					   was successful try to invoke the specific plugin
					   type loader. If the second loading went well add
					   the plugin to the plugin list. */
					if(!(plugin = plugin_mgmt_load(filename))) {
						dbg(0, "-> %s not valid plugin!", filename);
					} else {
						dbg(2, "-> %s (%s, type=%d)", plugin->name, filename, plugin->type);
						plugins = g_slist_append(plugins, plugin);
					}
				} else {
					dbg(2, "-> no library suffix");
				}
			} else {
				dbg(0, "-> prefix does not match: %s", filename);
			}
			filename = (gchar *)g_dir_read_name(dir);
		}
		g_dir_close(dir);
	} else {
		pwarn("dir='%s' failed. %s", lib_path, error->message);
		g_error_free(error);
		error = NULL;
	}

	if(!plugins){
		pwarn("no plugins found. help not initialised");
		return FALSE;
	}

	/* do plugin type specific startup init */
	htmlview_plugin_init ();

	return TRUE;
}

void plugin_mgmt_deinit(void) { }

GSList * plugin_mgmt_get_list(void) { return plugins; }

