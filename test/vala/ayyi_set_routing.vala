/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

static int channel = 0;
static int output = 0;
const OptionEntry[] entries = 
{
	{ "channel", 'c', 0, OptionArg.INT, ref channel, "Channel index", "N" },
	{ "output", 'o', 0, OptionArg.INT, ref output, "Output index", "N" },
	{ null }
};

public class Ayyi.SetRouting : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		OptionContext context = new OptionContext("Ayyi Set Routing");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			stdout.printf ("%s\n", e.message);
			stdout.printf ("Use '--help' to see available command line options.\n");
			return 1;
		}
		stdout.printf("channel=%i output=%i\n", channel, output);

		return (int)app.run(((SetRouting*)(app = new SetRouting()))->on_shm);
	}

	private void on_shm()
	{
		// !! we cannot use AM.track__set_output to set the master out as Master is not a track.

		Track* track = song->tracks->find_by_shm_idx(channel, TrackType.AUDIO);
		if(!(bool)track){
			AyyiTrack* tr = AyyiSong.get_audio_track(channel);
			if(AyyiSong.track_is_master(tr)) stdout.printf("cannot set Master output\n");
			else stdout.printf("no such channel: %i\n", channel);
			app.quit();
			return;
		}

		AyyiConnection* _connection = AyyiSong.get_connection(output);
		if((bool)output && !(bool)track){
			stdout.printf("no such connection: %i\n", output);
			app.quit();
			return;
		}

		if((bool)output)
			print("routing track '%s' to '%s'...\n", (string)track->name, (string)_connection->name);
		else
			print("unrouting track '%s'...\n", (string)track->name);

		AM.track__set_output(track, _connection, (ident, error) => {
			if((bool)error){
				print("error!\n");
				return;
			}
			print("done\n");

			int n_channels = 0;
			if((bool)AyyiMixer.channel_next(null)){
				stdout.printf("-----------------------------------------------------------------\n");
				stdout.printf("channel                    output\n");
				AyyiChannel* channel = null;
				while((bool)(channel = AyyiMixer.channel_next(channel))){
					AyyiList* routing = AyyiMixer.channel__get_routing(channel);
					if((bool)routing){
						AyyiList* item  = List.first(routing);
						if((bool)item){
							AyyiConnection* connection = AyyiSong.get_connection(item->id);
							if((bool)connection){
								stdout.printf("%i %20s --> %2i: %s\n", channel->shm_idx, (string)channel->name, connection->shm_idx, (string)connection->name);
							}
						}
					}
					else stdout.printf("%i %20s -->    no routing\n", channel->shm_idx, (string)channel->name);
					n_channels++;
				}
			}
			else stdout.printf("\tno channels\n");
		});

		app.quit();
	}
}


