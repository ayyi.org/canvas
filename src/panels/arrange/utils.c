/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "model/pool.h"
#include "pool_model.h"
#include "style.h"
#include "arrange/toolbox.h"
#include "window.h"
#include "window.statusbar.h"

static void     arr_k_begin_part_edit    (GtkAccelGroup*, gpointer);
static bool     arr_k_end_part_edit      (GtkAccelGroup*, Arrange*);
static void     arr_peak_gain_inc_k      (GtkAccelGroup*, gpointer);
static void     arr_peak_gain_dec_k      (GtkAccelGroup*, gpointer);
static void     arr_track_out_sel        (GtkWidget*, gpointer menu_item);
static void     arr_hzoom_slider_cb      (GtkWidget*, gpointer);
static void     arr_vzoom_slider_cb      (GtkWidget*, gpointer);
static void    _arr_set_hzoom            (Arrange*, double);
static void     arr_set_spp_samples      (uint32_t samples);
static void     arr_k_zoom_to_selection  (GtkAccelGroup*, Arrange*);
static void     arr_k_zoom_follow        (GtkAccelGroup*, Arrange*);
#ifdef USE_OPENGL
static void     arr_k_gl_canvas          (GtkAccelGroup*, gpointer);
#endif

static void     arr_zoom_history_clear   (Arrange*);
static void     arr_zoom_history_update  (Arrange*);


/*
 *  Return the track for the given arrange *canvas* coord.
 */
ArrTrk*
arr_px2track (Arrange* arrange, int y)
{
	g_return_val_if_fail(y >= 0, 0);

	int ty = 0;
	ArrTrk* at = NULL;
	while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL))){
		ty += at->height * vzoom(arrange);
		if(ty > y) break;
	}
	return at;
}


/*
 *  Return the track number given the arrange *canvas* coord.
 */
TrackDispNum
arr_px2trk (Arrange* arrange, int y)
{
	g_return_val_if_fail(arrange, 0);
	g_return_val_if_fail(y >= 0, 0);

	int ty = 0;
	ArrTrk* at = NULL;
	while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL))){
		ty += at->height * vzoom(arrange);
		if(ty > y) break;
	}
	return at ? track_list__get_display_num(arrange->priv->tracks, at->track) : -1;
}


/*
 *  Show the bbst time on the status bar using the supplied statusbar id.
 *  @param s - if set, will be prepended to the bbst string.
 *
 *  bbst is shown on statusbar4.
 */
void
arr_bbst2statusbar (Arrange* arrange, double x, const char* s)
{
	AyyiWindow* window = ((AyyiPanel*)arrange)->window;
	StatusbarId* statusbar_id = &window->statusbar->drag_id;

	guint cid = statusbar_id->cid;
	guint mid = statusbar_id->mid;

	#define MAX_64 63
																// TODO surely we are not concerned with STATUSBAR_MAIN here?
	if(cid) gtk_statusbar_remove(GTK_STATUSBAR(window->statusbar->widget[STATUSBAR_MAIN]), cid, mid);

	cid = gtk_statusbar_get_context_id(GTK_STATUSBAR(window->statusbar->widget[STATUSBAR_MAIN]), "Statusbar");

	char buff[MAX_64 + 1];
	char* b = buff;
	if(s && strlen(s)){
		snprintf(buff, MAX_64, "%s: ", s);
		b += MIN(strlen(s), MAX_64 - 16) + 2;
	}
	if(((AyyiPanel*)arrange)->zoom->value.pt.x < 4.0)
		arr_px2bbs(arrange, x, b);
	else
		arr_px2bbst(arrange, x, b);
	mid = gtk_statusbar_push(GTK_STATUSBAR(window->statusbar->widget[STATUSBAR_HOVER]), cid, buff);

	statusbar_id->mid = mid;
	statusbar_id->cid = cid;
}


float
arr_mu2px(Arrange* arrange, long long mu)
{
	float beats = (float)mu / (AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB);
	return arr_beats2px(arrange, (int)beats); 
}


double
arr_px2bar(Arrange* arrange, double px)
{
	// Convert a canvas position to a time postition in bars.
	// -why does this return a double when bar is an integer?
	// -this fn has no knowledge of scrollbar positions. px must be in canvas coords.

	return px / (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT * song->beats2bar);
}


double
arr_px2beat(Arrange* arrange, double px)
{
	// Convert a canvas position to a time postition in bars.
	// The beat returned is the one that the pixel is 'in', _not_ the nearest beat boundary.
	// -px is in 'canvas' coords, we dont need to know the scrolloffset.

	return px / (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT);
}


double 
arr_beats2px(Arrange* arrange, int beats)
{
	// Return the pixel number for the given beat, relative to the canvas origin.

	double x = (double)beats * PX_PER_BEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x;

	return x;
}


double 
arr_subs2px(Arrange* arrange, int subs)
{
	// Return the number of pixels corresponding to the given number of sub-beats..

	double subs_f = (double)subs;
	double x      = subs_f * PX_PER_SUBBEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x;

	return x;
}


	uint32_t arr_samples_per_pix_check(Arrange* arrangeX, float hzoom, float bpm)
	{
		float one_beat_samples = 44100 / 4;

		float onepx = one_beat_samples / PX_PER_BEAT;
		double samples_per_px = onepx / hzoom;
		return samples_per_px;
	}

double
arr_samples2px(Arrange* arrange, unsigned long long samples)
{
	//float beats_per_second = ((AyyiSongService*)ayyi.service)->song->bpm / 60.0;

	float secs = (float)samples / (float)song->sample_rate;
	float beats = secs * BEATS_PER_SECOND;

	double px = beats * PX_PER_BEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x;

#if 0
	{ //check
		double a = samples / arr_samples_per_pix(arrange);
		dbg(0, "%.3f %.3f", px, a);

		//simple case: 120bpm, 44100k, 44100samples, hzoom=1 --> 2 beats - should give PX_PER_BEAT * 2 = 16
		float bpm = 120.0;
		float hzoom = 1.0;
		float samples = 44100.0;
		float beats_per_second = bpm / 60.0;
		float secs = (float)samples / 44100.0; // =1.0
		float n_beats = secs * beats_per_second;
		double px = n_beats * PX_PER_BEAT * hzoom;
		dbg(0, "check1: %.3f (should be 16) secs=%.3f beats=%.2f", px, secs, n_beats);

		bpm = 60.0; //half the tempo, so for given n_samples at same zoom, n_beats is halved so px should half
		beats_per_second = bpm / 60.0;
		secs = (float)samples / 44100.0; // =1.0
		n_beats = secs * beats_per_second;
		px = n_beats * PX_PER_BEAT * hzoom;
		dbg(0, "check2: %.3f (should be 8) secs=%.3f beats=%.2f", px, secs, n_beats);

		bpm = 120.0; //same tempo but half n_samples, n_beats is halved so px should half
		samples = 22050.0;
		//----------------
		beats_per_second = bpm / 60.0;
		secs = (float)samples / 44100.0; // =1.0
		n_beats = secs * beats_per_second;
		px = n_beats * PX_PER_BEAT * hzoom;
		dbg(0, "check2: %.3f (should be 8) secs=%.3f beats=%.2f", px, secs, n_beats);

		float check2 = samples / arr_samples_per_pix_check(arrange, hzoom, bpm);
		dbg(0, "check3: %.3f (should be 16)", check2);
	}
#endif

	return px;
}


/*
 *  Convert a song position to a canvas position relative to the song start.
 */
double
arr_pos2px (Arrange* arrange, GPos* pos)
{
	g_return_val_if_fail(arrange, 0.0);
	am_pos_is_valid(pos);

	float ticks_per_beat_f = (float)AYYI_TICKS_PER_BEAT;

	double beats_float = pos->beat + (double)pos->sub / 4.0 + ((double)pos->tick / ticks_per_beat_f);

	double x = beats_float * PX_PER_BEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x;

	return x;
}


/*
 *  Convert a song position to a canvas position relative to the song start.
 */
double
arr_spos2px (Arrange* arrange, AyyiSongPos* pos)
{
	g_return_val_if_fail(arrange, 0.0);
	ayyi_pos_is_valid(pos);

	double beats_float = pos->beat + (double)pos->sub / ((double)AYYI_SUBS_PER_BEAT) + ((double)pos->mu) / ((double)AYYI_MU_PER_BEAT);

	return beats_float * PX_PER_BEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x;
}


uint64_t
arr_px2mu (Arrange* arrange, double px_zoomed)
{
	// Convert an arrange window pixel value to a core mu value.

	return px_zoomed * AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB / (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT);
}


int32_t
arr_px2samples (Arrange* arrange, double px_zoomed)
{
	// Convert an arrange window pixel value to a sample value.

	double beats = px_zoomed / (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT);

	//check:
	//double error = samples2px(ayyi_beats2samples_float(beats)) - px_zoomed;
	//printf(" error=%.1f\n", error);

	return ayyi_beats2samples_float(beats);
}


uint32_t
arr_samples_per_pix (Arrange* arrange)
{
	double onepx_in_samples = ayyi_beats2samples(1) / PX_PER_BEAT;
	return onepx_in_samples / (double)((AyyiPanel*)arrange)->zoom->value.pt.x;
}


double
samples2px_nz (uint64_t samples)
{
	float beats_per_second = ((AyyiSongService*)ayyi.service)->song->bpm / 60.0;

	float secs = (float)samples / (float)song->sample_rate;
	float beats = 4 * secs / beats_per_second; //why *4 ?

	return beats * PX_PER_BEAT;
}


uint32_t
px2samples_nz (double px_nonzoomed)
{
	// Convert an arrange window pixel value to a sample value.

	double beats = px_nonzoomed / PX_PER_BEAT;

	//check:
	//double error = samples2px(ayyi_beats2samples_float(beats)) - px_zoomed;
	//printf(" error=%.1f\n", error);

	dbg (3, "%.2f --> %.2f --> %i", px_nonzoomed, beats, ayyi_beats2samples_float(beats));
	return ayyi_beats2samples_float(beats);
}


uint64_t
px2mu_nz (double px_nonzoomed)
{
	// Convert a non-zoomed pixel value to a core mu value.

	// fn tests ok - see unit test.

	if(px_nonzoomed < 0){ perr("bad arg: px=%.4f", px_nonzoomed); return 0; }

	unsigned long long mu = px_nonzoomed * AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB / PX_PER_BEAT;
	dbg(3, "px=%.2f", px_nonzoomed);

	return mu;
}


void
px2pos_nz (GPos* pos, double px)
{
	int subbeats = (4 * px) / PX_PER_BEAT;
	int tot_ticks = (px * AYYI_TICKS_PER_BEAT) / (PX_PER_BEAT); 

	pos->beat = subbeats / 4;
	pos->sub  = subbeats % 4;
	pos->tick = tot_ticks - (subbeats * AYYI_TICKS_PER_SUBBEAT);
	dbg (2, "tot_ticks=%i subbeats=%i", tot_ticks, subbeats);
	if(!am_pos_is_valid(pos)) perr("!!");
}


/*
 *  Convert a song position to a canvas position.
 */
double
pos2px_nz (AMPos* pos)
{
	am_pos_is_valid(pos);

	float ticks_per_beat_f = (float)AYYI_TICKS_PER_BEAT;

	double beats_float = pos->beat + (double)pos->sub / 4.0 + ((double)pos->tick / ticks_per_beat_f);

	return beats_float * PX_PER_BEAT;
}


double
spos2px_nz (AyyiSongPos* pos)
{
	ayyi_pos_is_valid(pos);

	double beats =
		((double)pos->beat) +
		((double)pos->sub) / ((double)AYYI_SUBS_PER_BEAT) +
		((double)pos->mu) / ((double)AYYI_MU_PER_BEAT);

	return beats * PX_PER_BEAT;
}


double 
beats2px_nz (int beats)
{
	return (double)beats * (double)PX_PER_BEAT;
}


/*
 *	Convert a canvas position to a snapped one.
 *	-if snapping is off, no conversion is done.
 */
double
arr_snap (Arrange* arrange, double px)
{
	px = MAX(px, 0);

	if(song->snap_mode){
		GPos pos;
		arr_snap_px2pos(arrange, &pos, px);
		return arr_pos2px(arrange, &pos);
	}
	return px;
}


void
arr_snap_px2pos (Arrange* arrange, GPos* pos, double px)
{
	// Convert a horizontal arrange canvas position to a song position, snapping if required.

	switch(song->snap_mode){
		case SNAP_BAR:
			arr_px2pos(arrange, pos, px);
			q_to_nearest(pos, &song->q_settings[Q_BAR]);
			break;
		case SNAP_BEAT:
			dbg (2, "SNAP_BEAT");
			pos->beat = arr_px2beat_nearest(arrange, px);
			pos->sub  = 0;
			pos->tick = 0;
			break;
		case SNAP_16:
			pos->beat = arr_px2beat(arrange, px);
			pos->sub  = arr_px2subbeat_rel_nearest(arrange, px);
			pos->tick = 0;
			break;
		default:
			//snapping is off
			pos->beat = arr_px2beat(arrange, px);
			pos->sub  = arr_px2subbeat_rel(arrange, px);
			pos->tick = arr_px2tick_rel(arrange, px);
	}
}


/*
 *   Convert a horizontal arrange canvas position to a song position, snapping if required.
 */
void
arr_snap_px2spos (Arrange* arrange, AyyiSongPos* pos, double px)
{
	switch(song->snap_mode){
		case SNAP_BAR:
			;int bar = arr_px2beat(arrange, px) / song->beats2bar;

			*pos = (AyyiSongPos){bar * 4, 0, 0};
			dbg (1, "SNAP_BAR: beat=%i sub=%i", pos->beat, pos->sub);
			break;

		case SNAP_BEAT:
			*pos = (AyyiSongPos){
				.beat = arr_px2beat(arrange, px),
			};
			break;

		case SNAP_16:
			*pos = (AyyiSongPos){
				.beat = arr_px2beat(arrange, px),
				.sub  = arr_px2subbeat_rel_nearest(arrange, px) * AYYI_SUBS_PER_BEAT / 4,
			};
			break;

		default:
			// snapping is off
			ayyi_mu2pos(arr_px2mu(arrange, px), pos);
	}
}


void
arr_px2pos (Arrange* arrange, GPos* pos, double px)
{
	mu2pos(arr_px2mu(arrange, px), pos);
}


void
arr_px2spos (Arrange* arrange, AyyiSongPos* pos, double px)
{
	ayyi_mu2pos(arr_px2mu(arrange, px), pos);
}


int
arr_px2tick_rel (Arrange* arrange, double px)
{
	// Get the number of ticks after the previous sub-beat of the given pixel x position.

	int subbeat = (int)(4 * px / (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT));

	double remainder_px = px - subbeat * (((AyyiPanel*)arrange)->zoom->value.pt.x * PX_PER_BEAT) / 4;

	double px_per_tick = PX_PER_BEAT * ((AyyiPanel*)arrange)->zoom->value.pt.x / (4 * AYYI_TICKS_PER_SUBBEAT);

	int ticks = remainder_px / px_per_tick;

#ifdef DEBUG
	if(ticks > AYYI_TICKS_PER_SUBBEAT) perr ("ticks out of range!");
#endif
	return ticks;
}


void
arr_px2bbs (Arrange* arrange, ArrZoomedCanvasDouble px, char* str)
{ 
  // Convert a canvas position to a time string in bars, beats, subbeats, ticks.
  // -numbers are not padded with leading zeros.

  // FIXME this shouldnt simply truncate, it should take the *nearest* vals.

  sprintf(str, "%i:%i:%i",
						(int)arr_px2bar   (arrange, px),
						(int)arr_px2beat  (arrange, px) % (int)(song->beats2bar) +1,
						arr_px2subbeat_rel(arrange, px) +1);
}


void
arr_px2bbst (Arrange* arrange, ArrZoomedCanvasDouble px, char* str)
{ 
	ayyi_mu2bbst(arr_px2mu(arrange, px), str);
}


/*
 *  @param arrange can be NULL.
 */
void
arr_statusbar_printf (Arrange* arrange, int n, char* fmt, ...)
{
	if(n < STATUSBAR_MAIN || n >= STATUSBAR_MAX) n = STATUSBAR_MAIN;

	char s[128] = {0,};
	va_list argp;

	va_start(argp, fmt);
	vsnprintf(s, 127, fmt, argp);
	va_end(argp);

	window_foreach {
		if(window__has_panel_type(window, AYYI_TYPE_ARRANGE)){
			AyyiPanel* panel = arrange ? (AyyiPanel*)arrange : windows__find_panel(window, AYYI_TYPE_ARRANGE);
			if(panel){
				g_return_if_fail(panel->window);
				g_return_if_fail(panel->window->statusbar);
				g_return_if_fail(panel->window->statusbar->widget);
				GtkWidget* statusbar = panel->window->statusbar->widget[n];
				if(statusbar){
					gint cid = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Song");
					gtk_statusbar_push(GTK_STATUSBAR(statusbar), cid, s);
				}
			}
		}
	} end_window_foreach
}


void
arr_statusbar_print_pos (Arrange* arrange, int n, GPos* pos)
{
	g_return_if_fail(pos);

	char bbst[AYYI_BBST_MAX];
	pos2bbst(pos, bbst);
	arr_statusbar_printf(arrange, n, bbst);
}


void
arr_statusbar_print_xpos (Arrange* arrange, int n, double x)
{
	char lbl[32];
	arr_px2bbs(arrange, x, lbl);
	shell__statusbar_print(((AyyiPanel*)arrange)->window, n, lbl);
}


gboolean
arr_statusbar_print_idle (gpointer data)
{
	// For outputting statusbar messages from another thread.

	arrange_foreach {
		arr_statusbar_printf(arrange, 1, (char*)data);
	} end_arrange_foreach

	return false; //source should be removed.
}


Size
arr_canvas_viewport_size(Arrange* arrange)
{
	return (Size){
		arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.width : arrange->canvas->widget->allocation.width,
		arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.height : arrange->canvas->widget->allocation.height,
	};
}


int
arr_get_viewport_left_px(Arrange* arrange)
{
	// Return the left edge of the arrange window in pixels relative to '0'.

	int xscrolloffset, yscrolloffset;
	arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
	return xscrolloffset;
}


double
arr_get_viewport_right_px(Arrange* arrange)
{
	// Return the right hand edge of the currently visible part of the arrange canvas
	// *relative to the song start*.

	// get the current size of the viewport
	double width = arrange->canvas_scrollwin->allocation.width;

	int xscrolloffset;          // pixels
	int yscrolloffset;
	arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);

	return (double)xscrolloffset + width;
}


bool
arr_part_is_in_viewport (Arrange* arrange, AMPart* part)
{
	GdkRectangle arr_canvas_viewport (Arrange* arrange)
	{
		GdkRectangle r = {
			0,
			0,
			arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.width : arrange->canvas->widget->allocation.width,
			arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.height : arrange->canvas->widget->allocation.height,
		};
		return r;
	}

	GdkRectangle viewport = arr_canvas_viewport(arrange);
	AyyiRegionBase* region = part->ayyi;

	double start = arr_samples2px(arrange, region->position);
	double end = arr_samples2px(arrange, region->position + region->length);

	if(start > viewport.x + viewport.width) return false;
	if(end < viewport.x) return false;

	return true;
}


void
arr_zoom_to (Arrange* arrange, double x1, double y1, double x2, double y2)
{
	// Zoom the arrange canvas to the given size.
	// -x1 etc define the rect in the *current* window that should zoom to.
	// -warning! the canvas must be zoomed and updated before setting the scroll position otherwise it wont work properly.
	PF;
	AyyiPanel* panel = (AyyiPanel*)arrange;

	arr_zoom_history_update(arrange);

	// how much to zoom in?
	double old_width  = arrange->canvas_scrollwin->allocation.width;
	double old_height = arrange->canvas_scrollwin->allocation.height;
	double new_width  = x2 - x1;
	double new_height = y2 - y1;

	if(new_width < 2.0 && new_height < 2.0) return; // false trigger

	GPos new_scroll_pos;
	arr_snap_px2pos(arrange, &new_scroll_pos, x1);
	TrackDispNum tnum = arr_px2trk(arrange, y1);

	double mag_x   = old_width  / new_width;
	double mag_y   = old_height / new_height;

	Ptf zoom = {hzoom(arrange) * mag_x, vzoom(arrange) * mag_y};
	observable_point_set(panel->zoom, &zoom.x, &zoom.y);

	arrange->canvas->scroll_to_pos(arrange, &new_scroll_pos, tnum);
}


/*
 *   Zoom to a set of absolute coords
 *   -padding is to show a fixed amount of pixels above and below the target objects. Set to NULL for no padding.
 */
void
arr_zoom_to_1 (Arrange* arrange, AyyiSongPos* x1, AyyiSongPos* x2, TrackDispNum t1, TrackDispNum t2, Pti* padding)
{
	// TODO padding->y not implemented.

	g_return_if_fail(ayyi_pos_is_valid(x1));
	g_return_if_fail(ayyi_pos_is_valid(x2));
	g_return_if_fail(ayyi_pos_is_after(x2, x1));

	ArrTrk* at1 = track_list__track_by_display_index(arrange->priv->tracks, t1);
	ArrTrk* at2 = track_list__track_by_display_index(arrange->priv->tracks, t2);
	g_return_if_fail(at1 && at2);

	arr_zoom_history_update(arrange);

	double window_width = arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.width : arrange->canvas->widget->allocation.width;
	double window_height = arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.height : arrange->canvas->widget->allocation.height;

	if(padding) window_width -= padding->x;

	double default_length_beats = window_width / PX_PER_BEAT;
	double target_length_beats = ayyi_pos2beats(x2) - ayyi_pos2beats(x1);
	dbg(1, "default=%.2f target=%.2f", default_length_beats, target_length_beats);

	double old_hzoom = ((AyyiPanel*)arrange)->zoom->value.pt.x;
	double old_vzoom = vzoom(arrange);
	double new_hzoom = default_length_beats / target_length_beats;
	double unzoomed_tracks_height = (at2->y + at2->height - at1->y);
	double new_vzoom = 0.9 * window_height / unzoomed_tracks_height;
	dbg(1, "t1=%i t2=%i window_height=%.2f unzoomed_tracks_height=%.2f -> zoom=%.2f", t1, t2, window_height, unzoomed_tracks_height, new_vzoom);
	if(new_hzoom > ARR_MAX_HZOOM) pwarn("max hzoom exceeded");
	if(new_hzoom < ARR_MIN_HZOOM) pwarn("min hzoom exceeded");
	if(new_vzoom > ARR_MAX_VZOOM) pwarn("max vzoom exceeded: %.2f", new_vzoom);
	_arr_set_hzoom(arrange, new_hzoom);
	vzoom(arrange) = CLAMP(new_vzoom, ARR_MIN_VZOOM, ARR_MAX_VZOOM);
	dbg(1, "hzoom=%.2f-->%.2f", old_hzoom, ((AyyiPanel*)arrange)->zoom->value.pt.x);
	dbg(1, "vzoom=%.2f t1=%i t2=%i %.2f (%i->%i)", vzoom(arrange), t1, t2, unzoomed_tracks_height, at1->y, at2->y);

	AMChangeType change = 0;
	if(old_hzoom != new_hzoom) change |= AM_CHANGE_ZOOM_H;
	if(old_vzoom != new_vzoom) change |= AM_CHANGE_ZOOM_V;
	arr_on_view_changed(arrange, change);

#ifdef DEBUG
	char bbst1[64]; ayyi_pos2bbst(x1, bbst1);
	dbg(0, "scrolling to x=%s", bbst1);
#endif
	GPos p; songpos_ayyi2gui(&p, x1);
	arrange->canvas->scroll_to_pos(arrange, &p, t1);
}


void
arr_zoom_to_song (Arrange* arrange)
{
	// TODO should zoom-follow follow song-end marker, or last part?

	int n = track_list__count_disp_items(arrange->priv->tracks);
	g_return_if_fail(n);

	AyyiSongPos x1 = {0,};
	AyyiSongPos x2 = am_object_val(&song->loc[AM_LOC_END]).sp;
	Pti padding = {10, 0};

	arr_zoom_to_1(arrange, &x1, &x2, 0, n - 1, &padding);
}


static void
_arr_set_hzoom (Arrange* arrange, double zoom)
{
	// Scale properties must be set here and only here
	// -gui objects are updated later via arr_on_view_change

	float zoom_ = CLAMP(zoom, ARR_MIN_HZOOM, ARR_MAX_HZOOM);
	observable_point_set(((AyyiPanel*)arrange)->zoom, &zoom_, NULL);
}


static ArrTrk*
arr_find_tallest_track (Arrange* arrange)
{
	ArrTrk* tallest = NULL;
	AMTrack* track;
	AMIter iter;
	am_collection_iter_init(am_tracks, &iter);
	while((track = am_collection_iter_next (am_tracks, &iter))){
		ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, track);
		if((at) && (!tallest || (at->height > tallest->height))){
				tallest = at;
		}
	}
	return tallest;
}


/*
 *  arr_zoom_set is triggered by user actions and wraps observable_set
 *  with some additional functionality:
 *     - the zoom history is updated
 *     - max vzoom is determined
 */
void
arr_zoom_set (Arrange* arrange, float hzoom, float vzoom)
{
	dbg(1, "%.2f %.2f", hzoom, vzoom);

	arr_zoom_history_update(arrange);

	// Max vzoom is determined by both the window max, and the zoomed level of each track.
	ArrTrk* tallest = arr_find_tallest_track(arrange);
	float max_v_zoom = MIN(ARR_MAX_HZOOM, ((float)MAX_PART_HEIGHT) / (tallest ? tallest->height : 30));
	vzoom = MIN(vzoom, max_v_zoom);

	observable_point_set(((AyyiPanel*)arrange)->zoom, &hzoom, &vzoom);
}


void
arr_set_hzoom (AyyiPanel* panel, float hzoom)
{
	dbg (2, "hzoom=%.2f", hzoom);

	observable_point_set(panel->zoom, &hzoom, NULL);
}


void
arr_set_vzoom (AyyiPanel* panel, float vzoom)
{
	observable_point_set(panel->zoom, NULL, &vzoom);
}


static void
arr_set_canvas_type (AyyiPanel* arr, const char* canvas_type)
{
	g_return_if_fail(canvas_type);

	CanvasType type = arr_lookup_canvas_type(canvas_type);
	dbg(2, "%i", type);
	((Arrange*)arr)->canvas_type = type;
}


typedef struct {Arrange* arrange; int position;} C;

static void
arr_set_scroll_left (AyyiPanel* arr, int position)
{
	Arrange* arrange = (Arrange*)arr;

	if(arrange->canvas)
		arrange->canvas->scroll_to(arrange, position, -1);
}


static void
arr_set_scroll_top (AyyiPanel* arr, int position)
{
	Arrange* arrange = (Arrange*)arr;

	if(arrange->canvas)
		arrange->canvas->scroll_to(arrange, -1, position);
}


static void
arr_set_trkctl_width (AyyiPanel* arr, int width)
{
	((Arrange*)arr)->tc_width.val.i = width;
}


static void
arr_set_overview_visible (AyyiPanel* arr, int visible)
{
	((Arrange*)arr)->view_options[SHOW_SONG_OVERVIEW].value = visible;
}


static void
arr_set_songmap_visible (AyyiPanel* arr, int visible)
{
#ifdef USE_GNOMECANVAS
	((Arrange*)arr)->view_options[SHOW_SONG_MAP].value = visible;
#endif
}


static void
arr_set_partcontents_visible (AyyiPanel* arr, int visible)
{
	((Arrange*)arr)->view_options[SHOW_PART_CONTENTS].value = visible;
}


static void
arr_set_background_colour (AyyiPanel* arr, const char* bg_colour)
{
	g_return_if_fail(bg_colour);

	gint64 c = g_ascii_strtoll(bg_colour + 2, NULL, 16);
	((Arrange*)arr)->bg_colour = c;
	dbg(2, "%s 0x%08x", bg_colour, ((Arrange*)arr)->bg_colour);
}


#define HZOOM_SCALER 40.0
#define VZOOM_SCALER 0.08

double
get_hzoom_from_adj (double adj_val)
{
	// Translate the Arrange horizontal zoom widget adjustment value to the magnification factor.
	// -the adjustment range is 0.1 - 100

	dbg (2, "%.3f --> %.3f", adj_val, adj_val * adj_val / HZOOM_SCALER + 0.1);

	return adj_val * adj_val / HZOOM_SCALER + 0.1;
}


static double
get_vzoom_from_adj (double adj_val)
{
	// Translate the Arrange vertical zoom widget adjustment value to the magnification factor.
	// -the adjustment range is 0.1 - 30

	dbg (2, "%.3f --> %.3f", adj_val, adj_val * adj_val / VZOOM_SCALER + 0.6);

	return (adj_val * adj_val * VZOOM_SCALER) + 0.6;
}


double
hzoom_get_adj_val (Arrange* arrange)
{
	// Translate the horizontal magnification factor to the slider widget gtkadjustment value.

#ifdef DEBUG
	if(hzoom(arrange) < ARR_MIN_HZOOM) pwarn("hzoom out of range.");
#endif

	return sqrt((hzoom(arrange) - 0.1) * HZOOM_SCALER);
}


double
vzoom_get_adj_val (Arrange* arrange)
{
	// Translate the vertical magnification factor to the slider widget gtkadjustment value.

#ifdef DEBUG
	if(vzoom(arrange) < ARR_MIN_VZOOM) pwarn("vzoom out of range.");
#endif

	return sqrt((vzoom(arrange) - 0.6) / VZOOM_SCALER);
}


static void
arr_hzoom_slider_cb (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = (Arrange*)windows__get_panel_from_widget(widget);
	arr_zoom_history_update(arrange);

	//double old_hzoom = arrange->hzoom;
	arr_set_hzoom((AyyiPanel*)arrange, get_hzoom_from_adj(gtk_range_get_value(GTK_RANGE(widget))));

	// Scroll the canvas to preserve the correct focus:
	// TODO this does not work adequately.
#if 0
	if(arrange->zoom_focus == ZOOM_FOCUS_CENTRE){
		double dx = 0.0;

		int xscrolloffset, yscrolloffset;
		arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
		double old_width = arrange->canvas_scrollwin ? arrange->canvas_scrollwin->allocation.width : arrange->canvas->widget->allocation.width;
		double new_width = old_width * val / old_hzoom;

		dx = (new_width - old_width) / 2.0; //this calculation is slightly off.
		printf("%s(): dx=%.0f\n", __func__, dx);

		//changing the adjustment instead doesnt seem to make any difference...
		//GtkAdjustment *hadj  = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(arrange->canvas_scrollwin));
		//gtk_adjustment_set_value(hadj, gtk_adjustment_get_value(hadj)+ dx);

		arr_scroll_to(xscrolloffset + (int)dx, yscrolloffset); //FIXME this is causing a "double move" ! scroll redraws are _always_ done before other redraws it seems.
	}
#endif
}


static void
arr_vzoom_slider_cb (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = (Arrange*)windows__get_panel_from_widget(widget);
	g_return_if_fail(arrange_verify((AyyiPanel*)arrange));

	arr_zoom_history_update(arrange);

	arr_set_vzoom((AyyiPanel*)arrange, get_vzoom_from_adj(gtk_range_get_value(GTK_RANGE(widget))));
}


#define ZLIST arrange->zoom_history

static void
arr_zoom_history_clear (Arrange* arrange)
{
	while(ZLIST){
		ZoomSetting* item = (ZoomSetting*)ZLIST->data;
		g_free(item);
		ZLIST = g_list_remove(ZLIST, item);
	}
}


#define history_size 10
bool history_frozen = false;

static void
arr_zoom_history_update (Arrange* arrange)
{
	// Called everytime a zoom setting is changed.

	// The history list is stored oldest last.

	static ZoomSetting previous;

	if(!SONG_LOADED) return;
	if(history_frozen) return;
	if(hzoom(arrange) == previous.h && vzoom(arrange) == previous.v) return;

	if(g_list_length(ZLIST) >= history_size){
		ZoomSetting* last = g_list_last(ZLIST)->data;
		g_free(last);
		arrange->zoom_history = g_list_remove(arrange->zoom_history, last);
	}

	ZoomSetting* new = AYYI_NEW(ZoomSetting,
		.h = hzoom(arrange),
		.v = vzoom(arrange)
	);
	ZLIST = g_list_prepend(arrange->zoom_history, new);

	dbg(2, "%.2f x %.2f len=%i", new->h, new->v, g_list_length(ZLIST));
}


void
arr_zoom_history_back (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = (Arrange*)app->active_panel;
	if(!arrange_verify((AyyiPanel*)arrange)) return;

	if(!ZLIST){ arr_statusbar_printf(arrange, 1, "no zoom history"); return; }

	ZoomSetting* back = ZLIST->data;

#ifdef DEBUG
	int orig_len = g_list_length(ZLIST);
#endif
	history_frozen = TRUE;

	arr_zoom_set(arrange, back->h, back->v);

	history_frozen = FALSE;

	g_free(back);
	ZLIST = g_list_remove(ZLIST, back);

	dbg(1, "history_length: %i --> %i", orig_len, g_list_length(ZLIST));
}


void
arr_zoom_to_selection (Arrange* arrange)
{
	PF;
	#define ZOOM_T0_BORDER 15.0
	if(!arrange->part_selection) return;

	AyyiSongPos border = {0, AYYI_SUBS_PER_BEAT / 4,};
	AyyiSongPos x1, x2;
	TrackNum track_top, track_bottom;
	am_partmanager__find_boundary(arrange->part_selection, &x1, &x2, &track_top, &track_bottom);
	ayyi_pos_sub(&x1, &border);
	ayyi_pos_add(&x2, &border);

#ifdef DEBUG
	char bbst1[64]; ayyi_pos2bbst(&x1, bbst1);
	char bbst2[64]; ayyi_pos2bbst(&x2, bbst2);
	dbg(0, "x1=%s x2=%s", bbst1, bbst2);
#endif

	arr_zoom_to_1(arrange, &x1, &x2, track_top, track_bottom, NULL);
}


#if 0
uint32_t
arr_get_viewport_left_samples(Arrange* arrange)
{
#ifdef USE_CLUTTER
	if(CANVAS_IS_CLUTTER){
		return arrange->canvas->get_viewport_left(arrange);
	}
#endif

#ifdef USE_GNOMECANVAS
	if(CANVAS_IS_GNOME){
		return arrange->canvas->get_viewport_left(arrange);
	}

	if(!CANVAS_IS_GNOME){ perr("!!"); return 0; }
#endif

	return arr_px2samples(arrange, 0);
}
#endif


/*
 *  Scroll left by the number of pixels given.
 *  -the scrollbar should move to the left, objects inside the scrollwin move to the right.
 */
void
arr_scroll_left_k (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = app->latest_arrange;
	if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;

	int pix = 10 + MIN(keypress_get_repeat_count(1) / SCROLL__INVERSE_ACCEL, SCROLL__MAX_PX_PER_REPEAT);

	int xscrolloffset, yscrolloffset;
	arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
	arrange->canvas->scroll_to(arrange, xscrolloffset - pix, yscrolloffset);
}


/*
 *  Scroll right by the number of pixels given.
 *  -the scrollbar should move to the right, objects inside the scrollwin move to the left.
 */
void
arr_scroll_right_k (GtkWidget* accel_group, gpointer user_data)
{
	Arrange* arrange = app->latest_arrange;
	if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;

	int pix = 10 + MIN(keypress_get_repeat_count(1) / SCROLL__INVERSE_ACCEL, SCROLL__MAX_PX_PER_REPEAT);

	arrange->canvas->scroll_to(arrange, arrange->canvas->h_adj->value + pix, -1);
}


/*
 *  Scroll the arrange canvas up.
 *  -the scrollbar should move up, objects inside the scrollwin move down.
 */
void
arr_scroll_up_k (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = app->latest_arrange;
	if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;

	int pix = 10;
	int xscrolloffset, yscrolloffset;
	arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
	arrange->canvas->scroll_to(arrange, xscrolloffset, yscrolloffset - pix);
}


/*
 *  Scroll the arrange canvas down.
 *  -the scrollbar should move down, but objects inside the scrollwin move up.
 */
void
arr_scroll_down_k (GtkWidget* widget, gpointer user_data)
{
	Arrange* arrange = app->latest_arrange;
	if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;

	int pix = 10;

	int xscrolloffset, yscrolloffset;
	arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
	arrange->canvas->scroll_to(arrange, xscrolloffset, yscrolloffset + pix);
}


void
arr_zoom_song (Arrange* arrange)
{
	PF;
	g_return_if_fail(arrange);

	double song_length = (double)(am_object_val(&song->loc[AM_LOC_END]).sp.beat - am_object_val(&song->loc[AM_LOC_START]).sp.beat);

	ArrTrk* at = track_list__last_visible (arrange->priv->tracks);
	float h = at->y + at->height;

	arr_zoom_set(arrange,
		(arrange->canvas->widget->allocation.width - arrange->tc_width.val.i - 20) / (song_length * PX_PER_BEAT),
		(arrange->canvas->widget->allocation.height - arrange->ruler_height) / h
	);
}


static void
arr_up_k (GtkAccelGroup* accel_group, Arrange* __arrange)
{
	// For some reason, the user_data is not being passed via gtk_accel_groups_activate, so we store it as a property instead.

	Arrange* arrange = app->latest_arrange;
	g_return_if_fail(arrange);
	TrackList* tl = arrange->priv->tracks;

	AMTrack* tr = arrange->tracks.selection->data;

	int d = track_list__get_display_num(tl, tr);

	if(d){
		AMTrack* tr2 = track_list__prev_visible(tl, tl->display[d]->track);
		if(tr2){
			dbg (1, "d=%i", d);
			if(!tr2->visible) pwarn("track not visible");
			arr_track_select(arrange, tr2);
		}
	}
}


static void
arr_down_k (GtkAccelGroup* accel_group, Arrange* __arrange)
{
	Arrange* arrange = (Arrange*)app->active_panel;
	g_return_if_fail(arrange && arrange == app->latest_arrange);

	if(arrange->tracks.selection){
		AMTrack* tr = arrange->tracks.selection->data;
		int d = track_list__get_display_num(arrange->priv->tracks, tr);
		dbg (1, "d=%i", d);
		ArrTrk* tr2 = track_list__next_visible(arrange->priv->tracks, arrange->priv->tracks->display[d]->track);
		if(tr2){
			arr_track_select(arrange, tr2->track);
		}
	}
}


static void
arr_left_k (GtkAccelGroup* accel_group, Arrange* arrangeX)
{
	// Change the Part selection.
	// -a single Part becomes selected.
	// -if multiple parts were selected, the new selection is based on the first in the list.

	Arrange* arrange = (Arrange*)app->active_panel;
	g_return_if_fail(arrange->tracks.selection);
	PF;

	if(arrange->part_selection){

		AMPart* current = arrange->part_selection->data;

		GdkModifierType mods;
		gdk_window_get_pointer (((GtkWidget*)arrange)->window, NULL, NULL, &mods);
		if(!(mods & GDK_SHIFT_MASK)){
			arr_part_selection_reset(arrange, true);
		}

		// find the previous Part
		AMPart* closest = NULL;
		if((closest = (AMPart*)am_partmanager__get_previous(current, current->track))){
			arr_part_selection_add(arrange, closest);
		}

	}else{
		// nothing was previously selected. Select the last Part on the current track.
		AMTrack* track = arrange->tracks.selection->data;
		if(track){
			AMPart* last = NULL;
			FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, track);
			AMPart* part;
			while((part = (AMPart*)filter_iterator_next(i))){
				if(!last || ayyi_pos_is_after(&part->start, &last->start)) last = part;
			}
			filter_iterator_unref(i);
			if(last) arr_part_selection_add(arrange, last);
		}
	}
}


static void
arr_right_k (GtkAccelGroup* accel_group, Arrange* _arrange)
{
	// Change the Part selection.
	// -a single Part becomes selected.
	// -if multiple parts were selected, the new selection is based on the first in the list.

	Arrange* arrange = (Arrange*)app->active_panel;
	g_return_if_fail(windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE));
	g_return_if_fail(arrange->tracks.selection);

	AMPart* part = NULL;

	if(arrange->part_selection){

		AMPart* current = arrange->part_selection->data;

		GdkModifierType mods;
		gdk_window_get_pointer (((GtkWidget*)arrange)->window, NULL, NULL, &mods);
		if(!(mods & GDK_SHIFT_MASK)){
			arr_part_selection_reset(arrange, true);
		}

		// find the Part following
		part = (AMPart*)am_partmanager__get_next(current, current->track);

	}else{
		// nothing was previously selected. Select the first Part on the current track.
		part = am_partmanager__get_first(arrange->tracks.selection->data);
	}

	if(part)
		arr_part_selection_add(arrange, part);
	else
		dbg(1, "nothing found.");
}


void
arr_set_background (Arrange* arrange, int palette_num)
{
	ASSERT_PALETTE_NUM(palette_num);

	arrange->bg_colour = song->palette[palette_num];

	GdkColor colour;
	am_palette_lookup(song->palette, &colour, palette_num);

#ifdef USE_GNOMECANVAS
	if(arrange->song_overview.widget) arr_song_overview_queue_for_update(arrange);
#endif
	call(arrange->canvas->on_view_change, arrange, AM_CHANGE_BACKGROUND_COLOUR);
}


static void
arr_set_spp_samples (uint32_t samples)
{
	static uint32_t samples_old;

	if(samples != samples_old){
		dbg (3, "samples=%i", samples);
		arrange_foreach {
			if(arrange->canvas) call(arrange->canvas->set_spp, arrange, samples);
		} end_arrange_foreach;
	}

	samples_old = samples;
}


double
arr_get_spp_px (Arrange* arrange)
{
	GPos pos;
	am_transport_get_pos(&pos);
	return arr_pos2px(arrange, &pos);
}


void
arr_part_selection_reset (Arrange* arrange, bool update_self)
{ 
	// Clear any existing part selection.

	g_return_if_fail(windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE));
	AyyiPanel* panel = (AyyiPanel*)arrange;

	arr_part_editing_stop(arrange);

	arr_part_selection_replace(arrange, NULL, update_self);

	if(arrange->part_selection) perr("list not cleared.");
	if(panel->link && am_parts_selection) perr("song selection not cleared.");
}


void
arr_part_selection_replace (Arrange* arrange, GList* parts, bool update_self)
{
	// @param parts: ownership is taken. If panel is linked the list is owned by the model, if not linked it is owned by the Panel.

	AyyiPanel* p = (AyyiPanel*)arrange;
	int n_parts = g_list_length(parts);

	if(!p->link) g_list_free(arrange->part_selection); // when linked, the list is owned by the model.
	arrange->part_selection = parts;

	if(p->link){
		am_collection_selection_replace ((AMCollection*)song->parts, parts, update_self ? NULL : arrange);
	}else{
		arrange->canvas->set_part_selection(arrange);
	}

	if(n_parts)
		arr_statusbar_printf(arrange, 2, "%i parts selected", n_parts);
	else
		arr_statusbar_printf(arrange, 2, "%i tracks selected.", g_list_length(arrange->tracks.selection));
}


void
arr_part_selection_add(Arrange* arrange, AMPart* part)
{
	// Add a single part to the arrange window selection.

	dbg(2, "...");
	g_return_if_fail(part);

	if(arr_part_is_selected(arrange, part)) return;

	if(((AyyiPanel*)arrange)->link){
		GList* parts = g_list_append(NULL, part);
		am_collection_selection_add((AMCollection*)song->parts, parts, NULL);
		g_list_free(parts);
	}else{
		arrange->part_selection = g_list_prepend(arrange->part_selection, part);
		arrange->canvas->set_part_selection(arrange);
	}
}


/*
 *  Add all parts that are between the two parts. Used during shift-click.
 */
void
arr_part_selection_add_range (Arrange* arrange, const AMPart* new_part, const AMPart* ref_part)
{
	g_return_if_fail(new_part);
	if(!ref_part){ arr_part_selection_add(arrange, (AMPart*)new_part); return; }

	AyyiSongPos *range_start, *range_end;
	TrackNum track_start, track_end;

	TrackNum tp = am_track_list_position(song->tracks, new_part->track);
	TrackNum tr = am_track_list_position(song->tracks, ref_part->track);

	if(ayyi_pos_is_after(&new_part->start, &ref_part->start)){
		// new part is later than the other
		range_start = (AyyiSongPos*)&ref_part->start;
		range_end   = (AyyiSongPos*)&new_part->start;
	}else{
		// new part is before the other
		range_start = (AyyiSongPos*)&new_part->start;
		range_end   = (AyyiSongPos*)&ref_part->start;
	}

	if(new_part->track >= ref_part->track){
		track_start = tr;
		track_end   = tp;
	}else{
		track_start = tp;
		track_end   = tr;
	}

	for(GList* l=song->parts->list;l;l=l->next){
		AMPart* part = l->data;
		TrackNum t = am_track_list_position(song->tracks, part->track);
		if(
			!ayyi_pos_is_after(&part->start, range_end) &&
			!ayyi_pos_is_after(range_start, &part->start) &&
			t >= track_start && t <= track_end
		) {
			arr_part_selection_add(arrange, part);
		}
	}
}


/*
 *  De-select a part in the arrange window.
 *  If the part is not in the selection list, nothing will happen.
 */
void
arr_part_selection_remove (Arrange* arrange, AMPart* part)
{
	g_return_if_fail(part);

	if(((AyyiPanel*)arrange)->link){
		am_collection_selection_remove(am_parts, part, NULL);
		arrange->part_selection = am_parts->selection;
	}else{
		arrange->part_selection = g_list_remove(arrange->part_selection, part);
		arrange->canvas->set_part_selection(arrange);
	}
}


bool
arr_part_is_selected(Arrange* arrange, const AMPart* gpart)
{
	g_return_val_if_fail(gpart, false);

	return (boolp)g_list_find(arrange->part_selection, gpart);
}


void
arr_parts_select_by_track (Arrange* arrange, TrackDispNum d)
{
	// Select all the parts on the track with the given display number.

	if(d < 0 || d >= am_track_list_count(song->tracks)){ dbg (0, "bad tnum: %i", d); return; }

	TrackNum t = track_list__track_num_from_display_num(arrange->priv->tracks, d);
	dbg(2, "d=%i --> t=%i", d, t);

	arr_part_selection_replace(arrange, am_partmanager__get_by_track(song->tracks->track[t]), true);
}


static void
arr_select_all(AyyiPanel* panel)
{
	PF;
	g_return_if_fail(panel);
	g_return_if_fail(AYYI_IS_ARRANGE(panel));
	Arrange* arrange = (Arrange*)panel;

	arr_part_selection_reset(arrange, true);
	// select all parts
	GList* l = song->parts->list;
	for(;l;l=l->next){
		arr_part_selection_add(arrange, (AMPart*)l->data);
	}
	dbg (0, "%i parts selected.", g_list_length(song->parts->list));
}


int
arr_coords_get_note(Arrange* arrange, int x, int y)
{
	// For now this just uses @y to return the midi note number
	// @x and @y are in canvas coords (probably).

	int note_num = 60;
	TrackDispNum d = arr_px2trk(arrange, y);
	ArrTrk* atr = arrange->priv->tracks->display[d];
	if(atr){
		note_num = arr_track_get_note_num_from_y((ArrTrackMidi*)atr, (double)y);
	}

	return note_num;
}


double
arr_px2beat_nearest(Arrange* arrange, double px)
{
	int beat1 = arr_px2beat(arrange, px);
	int beat2 = beat1 + 1;
	int distance1 = ABS(px - arr_beats2px(arrange, beat1));
	int distance2 = ABS(px - arr_beats2px(arrange, beat2));

	return distance1 < distance2 ? beat1 : beat2;
}


int
arr_px2subbeat_rel(Arrange* arrange, double px)
{
	// Converts a canvas position to a time postition in subbeats relative to the last beat.
	// -because it uses 'canvas' coords, we dont need to know the scrolloffset.

	int beat    = (int)(px / (hzoom(arrange) * PX_PER_BEAT));
	int subbeat = (int)(4 * px / (hzoom(arrange) * PX_PER_BEAT));

	return (int)(subbeat - 4 * beat);
}


int
arr_px2subbeat_rel_nearest (Arrange* arrange, double px)
{
	// Convert a canvas position to a time postition in subbeats using either the 
	// previous or subsequent subbeat, whichever is nearest.
	// -the value is relative to the beat, so the return value is never greater than SUBS_PER_BEAT.

	int beat    = (int)(                px / (hzoom(arrange) * PX_PER_BEAT));
	int subbeat = (int)(SUBS_PER_BEAT * px / (hzoom(arrange) * PX_PER_BEAT));

	int previous = (int)(subbeat - SUBS_PER_BEAT * beat);
	int next     = previous + 1;

	// values are relative to the start of the bar
	double previous_px = arr_subs2px(arrange, previous);
	double next_px     = arr_subs2px(arrange, next);
	double un_q_px     = px - arr_beats2px(arrange, beat);

	int diff1 = ABS(un_q_px - previous_px);
	int diff2 = ABS(un_q_px - next_px);

	return diff1 < diff2 ? previous : next;
}


void
kzoom_in_hor_cb (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	if(panel){
		if(AYYI_IS_ARRANGE(panel)){
			float zoom = panel->zoom->value.pt.x * 1.25;
			observable_point_set(panel->zoom, &zoom, NULL);
		}
	}
}


void
kzoom_out_hor_cb (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	if(panel){
		if(AYYI_IS_ARRANGE(panel)){
			float zoom = panel->zoom->value.pt.x / 1.25;
			observable_point_set(panel->zoom, &zoom, NULL);
		}
	}
}


#ifdef USE_OPENGL
static void
arr_k_gl_canvas (GtkAccelGroup* accel_group, gpointer data)
{
	arr_set_canvas((Arrange*)app->active_panel, arr_gl_canvas_get_type());
	menu_update_options();
}
#endif


static void
arr_k_zoom_to_selection (GtkAccelGroup* accel_group, Arrange* arrange)
{
	AyyiPanel* panel = app->active_panel;
	arr_zoom_to_selection((Arrange*)panel);
}


static void
arr_k_zoom_follow(GtkAccelGroup* accel_group, Arrange* _arrange)
{
	// Toggle zoom-follow mode.
	// -zoom-follow maintains that the song fills the window.

	Arrange* arrange = (Arrange*)app->active_panel;

	arr_set_zoom_follow(arrange, !arrange->zoom_follow);

	shell__statusbar_print(((AyyiPanel*)arrange)->window, 0, "song zoom follow %s", arrange->zoom_follow ? "enabled" : "disabled");
}


static void
arr_peak_gain_inc_k(GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	arr_peak_gain_inc((Arrange*)panel);
}


void
arr_peak_gain_inc(Arrange* arrange)
{
	PF;
	g_return_if_fail(AYYI_IS_ARRANGE(arrange));

	arrange->peak_gain *= 1.5;
	if(arrange->peak_gain > 100) arrange->peak_gain = 100;

	arr_on_view_changed(arrange, AM_CHANGE_PEAK_GAIN);
}


static void
arr_peak_gain_dec_k(GtkAccelGroup* accel_group, gpointer user_data)
{
	arr_peak_gain_dec((Arrange*)app->active_panel);
}


void
arr_peak_gain_dec(Arrange* arrange)
{
	PF;
	g_return_if_fail(AYYI_IS_ARRANGE(arrange));

	arrange->peak_gain /= 1.5;
	if(arrange->peak_gain < 1.0) arrange->peak_gain = 1.0;

	arr_on_view_changed(arrange, AM_CHANGE_PEAK_GAIN);
}


static void
arr_k_begin_part_edit(GtkAccelGroup* accel_group, gpointer data)
{
	PF;
	AyyiPanel* panel = app->active_panel;
	g_return_if_fail(AYYI_IS_ARRANGE(panel));
	Arrange* arrange = (Arrange*)panel;

	if(!arrange->part_selection || arrange->part_editing) return;

	arr_part_editing_start(arrange);
}


static bool
arr_k_end_part_edit(GtkAccelGroup* accel_group, Arrange* arrangeX)
{
	// return FALSE if not editing

	PF;
	Arrange* arrange = (Arrange*)windows__get_active();
	windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE);
	g_return_val_if_fail(arrange, false);
	if(!arrange->part_editing) return false;

	arr_part_editing_stop(arrange);

	return true;
}


void
arr_k_next_note(GtkAccelGroup* accel_group, Arrange* arrangeX)
{
	PF;
	Arrange* arrange = (Arrange*)windows__get_active();
	if(!arrange->part_selection) return;

	AMPart* part = arrange->part_selection->data;
	MidiNote* prev = ((MidiPart*)part)->note_selection ? ((MidiPart*)part)->note_selection->data : NULL;
	MidiNote* note = am_midi_part__get_next_event(part, prev);

	arrange->canvas->set_note_selection(arrange, note);
}


void
arr_k_prev_note(GtkAccelGroup* accel_group, Arrange* arrangeX)
{
	PF;
	Arrange* arrange = (Arrange*)windows__get_active();
	if(!arrange->part_selection) return;

	AMPart* part = arrange->part_selection->data;
	MidiNote* prev = ((MidiPart*)part)->note_selection ? ((MidiPart*)part)->note_selection->data : NULL;
	MidiNote* note = am_midi_part__get_prev_event(part, prev);

	arrange->canvas->set_note_selection(arrange, note);
}


int
arr_get_n_gl_windows()
{
	// Return the number of open windows using the opengl canvas.

	int n = 0;
#ifdef USE_OPENGL
	arrange_foreach
		if(arrange->canvas && CANVAS_IS_OPENGL) n++;
	end_arrange_foreach
#endif
	return n;
}


double
arr_get_selection_size(Arrange* arrange, GList* selectionlist, int* n_tracks, double* x_pos, int* t0)
{
	// @return the zoomed width in pixels
	// @param selectionlist - currently can only be used with part_selection, but conceivably could be extended.

	// FIXME n_tracks calculations should use display num, not TrackNum.

	double selection_start_px = 0.0;
	double selection_end      = 0.0;
	int t_start               = AM_MAX_TRK;
	int t_end                 = 0;
	GList* l = selectionlist;
	dbg(2, "n_parts=%i", g_list_length(selectionlist));
	if(l){
		AyyiSongPos selection_start = ((AMPart*)l->data)->start;
		selection_start_px = arr_spos2px(arrange, &selection_start);
		for(;l;l=l->next){
			AMPart* part = l->data;
			if(ayyi_pos_is_after(&selection_start, &part->start)){
				selection_start = part->start;
				selection_start_px = arr_spos2px(arrange, &selection_start);
			}
			double part_end_px = arr_spos2px(arrange, &part->start) + arr_spos2px(arrange, &part->length);
			dbg (2, "part_start_px=%.1f part_end_px=%.1f partlength=%i", arr_spos2px(arrange, &part->start), part_end_px, arr_spos2px(arrange, &part->length));
			selection_end = MAX(selection_end, part_end_px);

			TrackNum t = am_track_list_position(song->tracks, part->track);
			t_start = MIN(t_start, t);
			t_end   = MAX(t_end,   t);
		}

		if(n_tracks) *n_tracks = t_end - t_start + 1;
		dbg(1, "n_tracks=%i", t_end - t_start + 1);
		if(x_pos) *x_pos = selection_start_px;
		if(t0) *t0 = t_start;
	}
	else if(n_tracks) *n_tracks = 0;

	return selection_end - selection_start_px;
}


GtkWidget*
ch_menu_init ()
{
	// Create the context menu for the arrange window channel select.

	GtkWidget* menu = gui_song.ch_menu = gtk_menu_new();
	char buff[128];

	// the list of available audio channels
	for(int c=0;c<AM_MAX_CHS;c++){
		snprintf(buff, 127, "A%i", c);

		GtkWidget* item = gui_song.ch_mitem[c] = gtk_check_menu_item_new_with_mnemonic("Channel");
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		gtk_widget_show(item);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item), FALSE);

#ifdef ENGINE_HAS_CHANNELS
		g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(tracks_change_ch), GINT_TO_POINTER(c));
#else
		g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(arr_track_out_sel), GINT_TO_POINTER(c));
#endif
		gtk_widget_show(item);
	}

#ifdef ENGINE_HAS_CHANNELS
	// off:
	snprintf(buff, 128, "OFF");
	GtkWidget* item = gtk_image_menu_item_new_with_label("OFF");
	GtkWidget* image = gtk_image_new_from_stock (SM_STOCK_POINTER, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL(menu), item);
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(tracks_change_ch), GINT_TO_POINTER(-1));
	gtk_widget_show(item);
 
	// "New" submenu

	GtkWidget* new = gtk_menu_item_new_with_mnemonic(_("New"));
	gtk_widget_show(new);
	gtk_container_add(GTK_CONTAINER(menu), new);

	GtkWidget* new_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(new), new_menu);

	item = gtk_image_menu_item_new_with_label("Audio");
	image = gtk_image_new_from_stock (SM_STOCK_POINTER, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), image);
	gtk_menu_shell_append(GTK_MENU_SHELL(new_menu), item);
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(song_add_ch_req), NULL);
	gtk_widget_show(item);

	item = gtk_image_menu_item_new_with_label("Midi");
	image = gtk_image_new_from_stock (SM_STOCK_POINTER, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL(new_menu), item);
	gtk_widget_show (item);

	// allow multiple channels
	snprintf(buff, 128, "A%i", c);
	item = gtk_image_menu_item_new_with_label(_("Allow Multiple"));
	image = gtk_image_new_from_stock (SM_STOCK_POINTER, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL(menu), item);
	gtk_widget_show(item);
#endif

	return menu;
}


void
arr_cursor_reset(Arrange* arrange, GdkWindow* window)
{
	// Return the cursor for the gdkwindow to its default appearance corresponding to the selected tool:

#ifndef DEBUG_DISABLE_TOOLBAR
	g_return_if_fail(arrange->toolbox.current < TOOL_MAX);

	ToolboxButton* item = &AYYI_ARRANGE_GET_CLASS(arrange)->toolbox[arrange->toolbox.current];

	set_cursor(window, item->cursor);
#endif

	app->cursor_timer.id = 0;
}


/*
 *  Handle all drops on the arrange canvas.
 *
 *  Expected types are:
 *    - "file://"    from external app,
 *    - "pool://"    from pool window,
 *    - "colour://"  from colour window
 */
void
arr_canvas_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	g_return_if_fail(data && data->length >= 0);

	if(info == GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING)) printf(" type=string.\n");

	Arrange* arrange = (Arrange*)windows__get_panel_from_widget(widget);
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);

	AyyiSongPos songpos;
	ArrTrk* at = NULL;
	arrange->canvas->px_to_model(arrange, (Pti){x, y}, &songpos, &at);
	g_return_if_fail(at);

#if 0
	//testing...
	GdkDragAction suggested_action = drag_context->suggested_action;
	dbg(2, "suggested_action=%i", suggested_action);
	// nomodifiers==2 (GDK_ACTION_COPY)  control==2 shift==4 (GDK_ACTION_LINK) mod4==2
	//gboolean control = ((drag_context->actions & GDK_ACTION_COPY) && !(drag_context->actions & GDK_ACTION_MOVE));
#endif

	if(info == GPOINTER_TO_INT(AYYI_TARGET_URI_LIST)){
		AyyiSongPos part_start = songpos; // the first drop is at this position. Subsequent ones are later.

		GList* droplist = uri_list_to_glist((char*)data->data);
#ifdef DEBUG
		int i = 0;
#endif
		GList* l = droplist;
		for(;l;l=l->next){
			char* u = l->data;
			dbg (1, "%i: '%s'", i++, u);
			gchar* method_string;
			vfs_get_method_string(u, &method_string);
			dbg (2, "method=%s", method_string);
			if(!strcmp(method_string, "file")){
				// drop from external program such as file manager

				//we could probably better use g_filename_from_uri() here
				//http://10.0.0.151/man/glib-2.0/glib-Character-Set-Conversion.html#g-filename-from-uri
				//-or perhaps get_local_path() from rox/src/support.c

				//strip off the "file:" from the beginning of the string:
				char filename[256];
				if(!dnd_get_filename(u, filename)) continue;
				dbg (2, "file=%s", filename);

				//create a prototype of the Part which will be created once the file has been imported:
				LocalPart* proto_part = part_local_new_at_position(arrange, &part_start, at);
				g_return_if_fail(proto_part);

				AMPoolItem* pool_item = am_pool__get_item_by_orig_name(filename);
				if(pool_item){
					dbg(2, "file is already loaded. adding part...");
					((AMPart*)proto_part)->pool_item = pool_item;
					song_add_part_from_prototype((AMPart*)proto_part);
				}else{
					dbg (0, "importing file...");
					shell__statusbar_print(((AyyiPanel*)arrange)->window, 0, "importing file...");

					if((pool_item = pool_import_file(filename, &proto_part->part))){
						proto_part->part.pool_item = pool_item;
						ayyi_samples2pos(((Waveform*)pool_item)->n_frames, &proto_part->part.length);
						if(!((Waveform*)pool_item)->n_frames) pwarn ("length not set yet! pool_item=%p", pool_item);
						pool_item_print(pool_item);
						dbg (0, "proto=%p pi=%p", &proto_part->part, proto_part->part.pool_item);
						dbg (0, "proto length=%i", proto_part->part.length.beat);
						ayyi_pos_add(&part_start, &proto_part->part.length);
					}
				}

			} else if(!strcmp(method_string, "pool")){
#if 0
				GPos part_start; arr_snap_px2pos(arrange, &part_start, x);
#else
				GPos pos_; songpos_ayyi2gui(&pos_, &songpos);
#endif

				char poolname[256];
				char* fname2 = memcpy(poolname, u+5, strlen(u)-5+1); //copy n chars from ct to s, and return s
				if(!strlen(fname2)){
					pwarn ("drag drop: empty pool name: '%s'. Ignoring.", u);
					continue;
				}
				dbg (0, "adding part for dropped pool item... %s", poolname);
				PoolDrop data;
				pool_get_dropdata(u, &data);
				if(data.file){
					unsigned colour = 0;
					uint64_t len = 0; // length should be taken from the pool.
					song_part_new(data.file, NULL, &pos_, at->track, len, NULL, colour, NULL, NULL, NULL);
				}else if(data.region > -1){
					unsigned colour = 0;
					uint64_t len = 0; // length should be taken from the pool.

					song_part_new(NULL, &(AyyiIdent){AYYI_OBJECT_AUDIO_PART, data.region}, &pos_, at->track, len, NULL, colour, NULL, NULL, NULL);
				}
				else pwarn ("nothing found");

			}else if(!strcmp(method_string, "colour")){

				// we need the palette number only, which is passed directly.
				char* str = (char*)data->data;
				int palette_num = atoi(str + 7);
				if(!palette_num){ pwarn ("cannot parse colour data!"); return; }
				palette_num--; // restore zero indexing.

				if((cc->provides & PROVIDES_TRACKCTL) && x < arrange->tc_width.val.i){
					dbg(1, "setting track colour...");
					ArrTrk* atr = arr_px2track(arrange, y - arrange->ruler_height);
					if(atr){
						am_track__set_colour(atr->track, palette_num, NULL, NULL);
					}
				}else{
					// canvas->pick needs the viewport offsets to be applied first
					int xscrolloffset, yscrolloffset;
					arrange->canvas->get_scroll_offsets(arrange, &xscrolloffset, &yscrolloffset);
					dbg (2, "x=%i+%i.", x, xscrolloffset);
					x += xscrolloffset;
					y += yscrolloffset;

					AMPart* part = NULL;
					arrange->canvas->pick(arrange, (double)x, (double)y, NULL, &part);
					if(part){
						GdkDragAction suggested_action = drag_context->suggested_action;
						dbg (0, "x=%i y=%i part=%s suggested_action=%i", x, y, part->name, suggested_action);

						// get list of files to apply the drop to:
						GList* list = am_partmanager__is_selected(part) ? am_parts_selection : g_list_prepend(NULL, part);

						// set colour depending on whether SHIFT was pressed:
						for(;list;list=list->next){
							AMPart* part = (AMPart*)list->data;
							if(suggested_action==2) am_part__set_colour(part, palette_num, NULL, NULL);
							else                    am_part__set_fg_colour(part, song->palette[palette_num]);
						}
					}else{
						dbg (0, "setting background...");
						arr_set_background(arrange, palette_num);
					}
				}
			}else if(!strcmp(method_string, "dock")){
				dbg(0, "received dock request!");
				ayyi_panel_on_drop_dock_item((AyyiPanel*)arrange, u);
			}else{
				dbg (0, "unknown method string.");
			}
			g_free(method_string);
		}
		if(droplist) uri_list_free(droplist);
	}
}


void
arr_set_song_title ()
{
	// Set window title in all appropriate windows following song load.

	const char* title = ayyi_song__get_file_path();

	char* window_string = g_strdup_printf("ayyi-gtk: %s", title);

	arrange_foreach {
		gtk_window_set_title(GTK_WINDOW(gtk_widget_get_toplevel((GtkWidget*)arrange)), window_string);
	} end_arrange_foreach

	g_free(window_string);
}


