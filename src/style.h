/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __src_style_h__
#define __src_style_h__

#include "model/am_palette.h"

void       am_palette_set_style       (Palette, GtkStyle*);
void       am_palette_lookup          (Palette, GdkColor*, int c_idx);
void       am_rgba_to_gdk             (GdkColor*, uint32_t rgba);
uint32_t   am_gdk_to_rgba             (GdkColor*);
void       colour_get_style_base      (GdkColor*, int state);
void       colour_get_style_bg_       (GdkColor*, int state);
void       colour_get_style_fg        (GdkColor*, int state);

#endif
