/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "canvas_op.h"

typedef struct _target target;

struct _GnomeCanvasOp
{
    CanvasOp         canvas;
    GnomeCanvasItem* item;
    target*          target;   // canvas group showing snapped drop location.
    GnomeCanvasItem* note;     // for entering new notes using the pencil tool. Never freed.

    Pti              win;      // cursor position relative to the window origin.
    Pti              origin;   // position that scrolling is done relative to.
                               //  - when not during a scroll op, it is the cursor position relative to canvas{0,0}.
};

struct _target
{
    GnomeCanvasGroup* group;
    GnomeCanvasItem*  a, *b, *c, *d;
    int   n_tracks;
    float width;               // f or multiple selection.
    float inner_width;
};

GnomeCanvasOp* gnome_canvas_op__new         (Arrange*, Optype);
void           gnome_canvas_op__set_arrange (CanvasOp*, Arrange*);

void           canvas_op__handle_scrolling  (CanvasOp*);
