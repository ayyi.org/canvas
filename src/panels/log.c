/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __log_win_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>
#include "windows.h"
#include "support.h"
#include "log.h"

extern char  ok  [];
extern char  fail[];

G_DEFINE_TYPE (AyyiLogWin, ayyi_log_win, AYYI_TYPE_PANEL)

static GObject* log_construct   (GType, guint n_construct_properties, GObjectConstructParam*);
static void     log_on_realize  (GtkWidget*, gpointer);
static void     log_on_ready    (AyyiPanel*);


static GtkTextBuffer* buf = NULL;
static GtkTextIter    iter = {0,};

typedef struct {
    GtkTextTag* green;
    GtkTextTag* red;
    GtkTextTag* orange;
    GtkTextTag* yellow;
} Tags;

static Tags tags = {0,};

static void txtbuffer_print_ok   ();
static void txtbuffer_print_fail ();
static void txtbuffer_print_warn ();


static void
txtbuffer_print (AyyiLogType type, const char* str)
{
	switch (type) {
		case LOG_WARN: txtbuffer_print_warn();
	}
	gtk_text_buffer_insert(buf, &iter, str, -1);

	switch (type) {
		case LOG_OK:   txtbuffer_print_ok();
			break;
		case LOG_FAIL: txtbuffer_print_fail();
			break;
	}
	gtk_text_buffer_get_end_iter(buf, &iter);
	gtk_text_buffer_insert(buf, &iter, "\n", -1);

}


static void
txtbuffer_print_ok ()
{
	//append coloured "[ ok ]" to the log textbuffer.

	gtk_text_buffer_insert(buf, &iter, " [ ", -1);
	gtk_text_buffer_insert_with_tags(buf, &iter, " ok ", 4, tags.green, NULL);
	gtk_text_buffer_insert(buf, &iter, " ] ", -1);
}


static void
txtbuffer_print_fail ()
{
	gtk_text_buffer_insert(buf, &iter, " [ ", -1);
	gtk_text_buffer_insert_with_tags(buf, &iter, " fail ", 6, tags.red, NULL);
	gtk_text_buffer_insert(buf, &iter, " ] ", -1);
}


static void
txtbuffer_print_warn ()
{
	gtk_text_buffer_insert_with_tags(buf, &iter, "warning: ", 9, tags.orange, NULL);
}

AyyiLogImpl txtbuffer = {
	txtbuffer_print,
	txtbuffer_print_ok,
	txtbuffer_print_fail,
	txtbuffer_print_warn,
};


static void
ayyi_log_win_class_init (AyyiLogWinClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	panel->name           = "Log";
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->accel          = GDK_F9;
	panel->default_size.x = 360;
	panel->default_size.y = 400;
	panel->on_ready       = log_on_ready;

	g_object_class->constructor = log_construct;

	buf = gtk_text_buffer_new(NULL);

	tags = (Tags){
		.green  = gtk_text_buffer_create_tag(buf, "green_foreground",  "foreground", "green", NULL),
		.red    = gtk_text_buffer_create_tag(buf, "red_foreground",    "foreground", "red",   NULL),
		.orange = gtk_text_buffer_create_tag(buf, "orange_foreground", "foreground", "orange",NULL),
		.yellow = gtk_text_buffer_create_tag(buf, "yellow_foreground", "foreground", "yellow",NULL)
	};

	ayyi_log_add_logger(&txtbuffer);

	gtk_text_buffer_get_end_iter(buf, &iter);
	gtk_text_buffer_insert_with_tags(buf, &iter, "Welcome to AyyiGtk\n", -1, tags.yellow, NULL);

	gtk_text_buffer_insert(buf, &iter, "Version: ", -1);
	gtk_text_buffer_insert(buf, &iter, VERSION, -1);
	gtk_text_buffer_insert(buf, &iter, "\n", -1);

}


static GObject*
log_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if (_debug_ > -1) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_log_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_LOG_WIN);

	return g_object;
}


static void
ayyi_log_win_init (AyyiLogWin* object)
{
}


GtkWidget*
log_win__new (AyyiWindow* _, Size* config_size, GtkOrientation orientation)
{
	LogWindow* window = AYYI_LOG_WIN (g_object_new (AYYI_TYPE_LOG_WIN, "behavior", GDL_DOCK_ITEM_BEH_NORMAL, "orientation", orientation, NULL));
	ayyi_panel_on_new((AyyiPanel*)window, config_size, orientation);

	return (GtkWidget*)window;
}


static void
log_on_ready(AyyiPanel* panel)
{
	LogWindow* window = (LogWindow*)panel;

	if (window->textview) return;

	GtkWidget* scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow);
	panel_pack(scrolledwindow, EXPAND_TRUE);

	GtkWidget* textview = window->textview = gtk_text_view_new_with_buffer(buf);
	gtk_widget_show(textview);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(textview), FALSE); //hide the cursor.
	gtk_container_add(GTK_CONTAINER(scrolledwindow), textview);

	g_signal_connect(G_OBJECT(panel->window), "realize", G_CALLBACK(log_on_realize), NULL);

	gboolean log__on_button_press (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
	{
		PF;
		bool control = event->state & GDK_CONTROL_MASK;
		if (!control) return NOT_HANDLED;

		pwarn("handle docking ops - forward to dockitem?");
		return HANDLED;
	}
	g_signal_connect(G_OBJECT(textview), "button-press-event", G_CALLBACK(log__on_button_press), panel);

	ayyi_panel_on_open(panel);
}


static void
log_on_realize (GtkWidget* widget, gpointer user_data)
{
	// give the window the normal background colour, instead of the text widget colour.

	LogWindow* log = (LogWindow*)windows__get_panel_from_widget(widget);

	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(log->textview));
	if (style) {
		style->base[GTK_STATE_NORMAL] = style->bg[GTK_STATE_NORMAL];
		style->text[GTK_STATE_NORMAL] = style->fg[GTK_STATE_NORMAL];
		gtk_widget_set_style(log->textview, style);

		g_object_unref(style);
	}
}
