/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 *  Currently there are 3 meter types in use:
 *    MixerStrip
 *      -GtkMeter    adjustment
 *      -GtkVuMeter  set directly
 *      -Bst         set directly
 *    TrackControl (GtkMeter)
 *    Opengl         value requested, not stored.
 */

#include "global.h"
#include <math.h>
#include <model/channel.h>
#include <model/track_list.h>
#include "windows.h"
#include "song.h"
#include "panels/mixer/strip.h"
#include "gtkmeter.h"
#include "widgets/gtkvumeter.h"
#include "widgets/bseenums.h"
#include "widgets/bstutils.h"
#include "widgets/bstdbmeter.h"
#include "support.h"
#include "meter.h"

static GtkWidget* meter_bst_new            (GtkOrientation, int n_chans);
#ifdef NEVER
static gboolean   bst_change_beam_value    (gpointer data);
#endif
static void       meter_on_channel_change  (AyyiMixerStrip*, gpointer);
static void       meter_connect_channel    (AyyiMixerStrip*);
static void       meter_disconnect_channel (AyyiMixerStrip*);

extern void       transport_enable         ();

struct _meter_demo
{
	gboolean enabled;
	int      j;
	double   val [16];    // array of meter values, one for each channel.        
	double   gain[16][2];
} demo = {false, 0};


/*
 *  Param n is the number of the meter in the strip. usually either 0 or 1.
 *
 *  All chans have meters but only input chans are currently 'connected'.
 */
GtkWidget*
meter_new (AyyiMixerStrip* strip, int n)
{
	g_return_val_if_fail(strip, NULL);
	GtkWidget* vumeter = NULL;
	AyyiPanel* panel = strip->panel;
	g_return_val_if_fail(panel, NULL);

	am_song__enable_meters(true);

	int meter_type = panel->meter_type;
	float meter_min = -70.0;

	dbg (2, "%i: win=%p type=%i.", strip->strip_num, panel, meter_type);

	switch(meter_type){
		case METER_JAMIN:
			{
				//GtkObject* meter_adj = strip->meteradj[n] = gtk_adjustment_new(0.0, meter_min, meter_max, 0.01, 0.1, 0.1);
				//TODO no longer need a strip->meteradj property
				vumeter = strip->meter.widget[n] = gtk_meter_new(GTK_ADJUSTMENT(strip->channel->meter_level), GTK_METER_UP);

				gtk_widget_show(vumeter);
				gtk_meter_set_warn_point(GTK_METER(vumeter), 0.6);
				dbg (1, "%i: n=%i\n", strip->strip_num, n);
			}
			break;
		case METER_GTK:
			{
				vumeter = strip->meter.widget[n] = gtk_vumeter_new(TRUE, GTK_VUMETER_USE_PEAKS, 33);
				gtk_vumeter_set_scale(GTK_VUMETER(vumeter), GTK_VUMETER_SCALE_LINEAR); // GTK_VUMETER_SCALE_LOG - our data is already logarithmic.
				// set the meter to use dB values directly.
				int min = (int)meter_min;
				int max = 0;
				gtk_vumeter_set_min_max(GTK_VUMETER(vumeter), &min, &max);
				gtk_widget_show(vumeter);
				if(!n){
					meter_disconnect_channel(strip);
					meter_connect_channel(strip);
				}
			}
			break;
		case METER_BST:
			{
				if (n) break;
				vumeter = strip->meter.widget[n] = meter_bst_new(GTK_ORIENTATION_VERTICAL, strip->channel->nchans);

				meter_disconnect_channel(strip);
				meter_connect_channel(strip);

				for (int i=0;i<strip->channel->nchans;i++) {
					BstDBBeam* beam = bst_db_meter_get_beam(BST_DB_METER(vumeter), i);
					if (beam) bst_db_beam_set_value (beam, -100.);
				}
			}
			break;
		default:
			perr ("unknown meter type %i. win->type=%zu", meter_type, G_OBJECT_TYPE(panel));
			break;
	}

	if (!n) {
		g_signal_connect(G_OBJECT(strip), "channel-changed", (gpointer)meter_on_channel_change, NULL);
	}

	void meter_on_strip_destroy (GtkObject* widget, gpointer _)
	{
		meter_destroy((AyyiMixerStrip*)widget, -1);
	}
	g_signal_connect((gpointer)strip, "destroy", G_CALLBACK(meter_on_strip_destroy), NULL);

	return vumeter;
}


/*
 *  meter_destroy destroys a meter created with meter_new().
 *
 *  If n is -1, destroy all meters for the strip.
 */
void
meter_destroy (AyyiMixerStrip* s, int n)
{
	g_return_if_fail(s);
	if (!s->channel) return; // inactive strip, eg inspector strip.

	if (s->panel->meter_type == METER_GTK || s->panel->meter_type == METER_BST) {
		meter_disconnect_channel(s);
	}
	if (g_signal_handlers_disconnect_matched(s, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, meter_on_channel_change, NULL) != 1) pwarn("handler disconnection (channel-change)");

	for (int c=0;c<s->channel->nchans;c++) {
		if (n >= 0 && n != c) continue;
		if (!s->meter.widget[c]) continue;

		gtk_widget_destroy(s->meter.widget[c]);
		s->meter.widget[c] = NULL;
	}
}


static void
meter_on_channel_change (AyyiMixerStrip* s, gpointer user_data)
{
	AMChannel* ch = s->channel;

	if (ch) {
		meter_disconnect_channel(s);
		meter_connect_channel(s);
	} else {
		meter_destroy(s, -1);
	}
}


static void
meter_connect_channel (AyyiMixerStrip* s)
{
	void gtk_vumeter_update (Observable* _, AMVal val, gpointer _s)
	{
		AyyiMixerStrip* s = _s;
		g_return_if_fail(s->channel);

		AMTrack* track = am_track_list_find_audio_by_idx(song->tracks, s->channel->ident.idx);
		if(track && s->meter.widget[0]){
			int level = am_track__get_meterlevel(track);
			gtk_vumeter_set_level(GTK_VUMETER(s->meter.widget[0]), level);
		}
	}

	void bst_meter_update (Observable* _, AMVal val, gpointer _s)
	{
		// this fn is called when the adjustment changes.

		AyyiMixerStrip* s = _s;

		AMTrack* track = am_track_list_find_audio_by_idx(song->tracks, s->channel->ident.idx);
		if(!track) return;

		float level[2];
		if(CH_IS_MASTER(s->channel)){
			am_track__get_meterlevel_master(level);
		}
		int n; for(n=0;n<s->channel->nchans;n++){
			if(!CH_IS_MASTER(s->channel)){
				level[n] = am_track__get_meterlevel(track);
			}

			BstDBBeam* beam = bst_db_meter_get_beam(BST_DB_METER(s->meter.widget[0]), n);
			if(beam) bst_db_beam_set_value (beam, level[n]);
		}
	}

	ObservableFn fn = NULL;

	switch(s->panel->meter_type){
		case METER_GTK:
			fn = gtk_vumeter_update;
			break;
		case METER_BST:
			fn = &bst_meter_update;
			break;
		default:
			break;
	}

	if(fn){
		observable_subscribe(s->channel->meter_level, fn, s);
	}
}


static void
meter_disconnect_channel (AyyiMixerStrip* s)
{
	observable_unsubscribe(s->channel->meter_level, NULL, s);
}


void
meters_start ()
{
	// TODO refactor
	// this actually starts more than just the meters

	if (!song->periodic) transport_enable();
}


void
meters_stop ()
{
	// TODO refactor
	// this actually stops more than just the meters

	am_song__enable_meters(false);
}


static void
meter_demo_update (gpointer data)
{
	static int counter = 0;
	static int v = 0;       // index to val[]

	demo.val[v] = ABS(- 0.75 * sin(counter/2.0)*sin(counter/16.0) + 0.4 * sin(counter/8.0) + 0.4 * sin(counter/64.0));
	dbg(0, "v=%i val=%.2f", v, demo.val[v]);
	demo.val[v] = am_gain2db(demo.val[v]);

	demo.gain[v][0] = ((double)rand()) * 100.0 / (RAND_MAX+1.0);

	AMTrack* tr;
	int t; for(t=0;(tr=song->tracks->track[t]);t++){
		if (!tr->meterval) continue;

		demo.j = (v + t) % 16;
		double m = demo.val[demo.j];
		observable_set(tr->meterval, (AMVal){.f = m});
	}

	AMIter iter;
	channel_iter_init(&iter);
	AMChannel* channel;
	printf("--------------------------------\n");
	while ((channel = channel_next(&iter))) {
		if (!channel->meter_level) continue;
		int c = channel->ident.idx;
		demo.j = (v + c) % 16; // make an index to val which is different for each channel
		observable_set(channel->meter_level, (AMVal){.f=demo.val[demo.j]});
		//dbg(0, "c=%i gain=%.2f val=%.2f ", c, demo.gain[v][0], demo.val[demo.j]);
	}

    //master channel:
#if 0
	mixer_foreach {
    if(!demo.enabled){
      Strip* ch = mixer->strips[song->channels->len - 1]; //FIXME wrong
      float level[2];
      engine_track_get_meterlevel_master(level);

      switch(mixer->meter_type){
        case METER_JAMIN:
          {
            int n; for(n=0;n<ch->channel->nchans;n++){
              if(!GTK_IS_ADJUSTMENT(ch->meteradj[n])){ perr ("master: !GTK_IS_ADJUSTMENT c=%i", ch->channel->ident.idx); meters_stop(); mixer__print_channels(); return; }

              gtk_adjustment_set_value(GTK_ADJUSTMENT(ch->meteradj[n]), level[n]);
            }
          }
          break;
        case METER_GTK:
          {
            int n; for(n=0;n<ch->channel->nchans;n++){
              gtk_vumeter_set_level(GTK_VUMETER(ch->meter[n]), level[n]);
            }
          }
          break;
        case METER_BST:
          {
            int n; for(n=0;n<ch->channel->nchans;n++){
              BstDBBeam* meter = bst_db_meter_get_beam(BST_DB_METER(ch->meter[0]), n);
              if(meter) bst_db_beam_set_value (meter, level[n]);
            }
          }
      }
    }
	} end_mixer_foreach
#endif

	counter++;
	v++; if(v >= 16) v=0;

	am_song__emit("periodic-update", NULL);
}


void
meters_demo_toggle ()
{
	PF;
	demo.enabled = !demo.enabled;

	void meters_demo_enable(gboolean enable)
	{
		PF0;
		demo.enabled = enable;

		if(demo.enabled){

			//replace the AMSong updater with the demo updater
			if(song->periodic) g_source_remove(song->periodic);
			song->periodic = g_timeout_add(50/*ms*/, (gpointer)meter_demo_update, NULL);

			#ifdef TTTT
			mixer_foreach {
				int meter_type = ((AyyiPanel*)mixer)->meter_type;
				if(meter_type == METER_BST){
					dbg(0, "starting bst demo timer");
					int n = 0;
					Strip* strip = mixer->strips[0];
					BstDBMeter* meter = (BstDBMeter*)strip->meter[n];
					BstDBBeam* dbbeam = bst_db_meter_get_beam(meter, n);
					g_timeout_add(50, bst_change_beam_value, g_object_ref (dbbeam)); //timer stops itself when no longer in demo mode.
					break;
				}
			} end_mixer_foreach
			#endif
		}
	}

	if(demo.enabled) meters_demo_enable(true);
}


#ifdef NEVER
static gboolean
bst_change_beam_value (gpointer data)
{
	//provides meter test signal

	if (!GTK_WIDGET_DRAWABLE (data)) { g_object_unref (data); return FALSE; }

	GDK_THREADS_ENTER ();
	BstDBBeam* self = BST_DB_BEAM (data);
	double v = rand() / ((double) RAND_MAX) * (self->dbsetup->maxdb - self->dbsetup->mindb) + self->dbsetup->mindb;
	if (v > self->dbsetup->maxdb) v = self->dbsetup->mindb;
	bst_db_beam_set_value (self, v);
	GDK_THREADS_LEAVE ();

	return demo.enabled; //stop if not in demo mode
}
#endif


static GtkWidget*
meter_bst_new (GtkOrientation orientation, int n_chans)
{
	#define BST_INITIAL_N_CHANS 0
	BstDBMeter* meter = BST_DB_METER(bst_db_meter_new(orientation, BST_INITIAL_N_CHANS));

	for (int i=0;i<n_chans;i++) {
		BstDBBeam* dbbeam = bst_db_meter_create_beam(meter, 0);
		gtk_widget_set_size_request((GtkWidget*)dbbeam, 10, -1);
	}
	bst_db_meter_create_dashes(meter, GTK_JUSTIFY_LEFT, 1);
	bst_db_meter_create_numbers (meter, 4);

	//this calls db_meter_build_channels()...
	//bst_db_meter_set_n_channels(BST_DB_METER(widget), n_chans);

	return (GtkWidget*)meter;
}

