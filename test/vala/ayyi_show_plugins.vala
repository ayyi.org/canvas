using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShowpluginsClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		return (int)(app = new ShowpluginsClient()).run(((ShowpluginsClient*)app)->on_shm);
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		print("-----------------------------------------------------------------\n");
		print("plugins:\n");
		if(AyyiMixer.plugin_next(null) != null){
			stdout.printf("  idx in out name\n");
			AyyiPlugin* plugin = null;
			while((plugin = AyyiMixer.plugin_next(plugin)) != null){
				stdout.printf("  %3i %2u %3u %-20s\n", plugin->idx, plugin->n_inputs, plugin->n_outputs, (string)plugin->name);
			}
		}
		else stdout.printf("\tno plugins\n");

		stdout.printf("-----------------------------------------------------------------\n");

		app.quit();
	}
}


