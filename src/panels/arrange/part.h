/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __arrange_part_h__
#define __arrange_part_h__

#include "model/am_part.h"

#define PART_MARGIN_Y1    0.5 // distance from track top to part top

struct _PartLocal
{
    AMPart      part;
    Arrange*    arrange;        // local parts are only on a single window.
    bool        ghost;          // true if the part is semi-invisible as in during a Move operation.
};

bool            arr_part_widgets_new       (Arrange*, AMPart*);

double          arr_part_start_px          (Arrange*, AMPart*);
double          arr_part_end_px            (Arrange*, AMPart*);
double          am_part__max_length_px_nz  (AMPart*);
void            parts_redraw_by_track      (Arrange*, AMTrack*);

LocalPart*      part_local_new             (Arrange*);
LocalPart*      part_local_new_at_position (Arrange*, AyyiSongPos*, ArrTrk*);
LocalPart*      part_local_new_from_part   (Arrange*, double x, double y, AMPart* template_part, gboolean ghost);
void            part_local_destroy         (LocalPart*);

bool            arr_part_note_is_selected  (Arrange*, AMPart*, MidiNote*);

const MidiNote* arr_part_pick_note         (Arrange*, AMPart*, Ptd, bool* at_start, bool* at_end);

GtkWidget*      part_menu_new              (Arrange*);

#endif
