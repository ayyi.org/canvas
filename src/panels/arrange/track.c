/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  track heights:
 |    -tracks have 2 sizes, a zoomed, and a non-zoomed height.
 |    -the non-zoomed size is stored in track[t]->height.
 |    -the zoomed size is cached in track_pos[t] (note: this array has a length 1 larger than the main track array.)
 |    -mostly the zoomed size is non-zoomed*vzoom, except at the upper and lower limits.
 |    -the relative sizes of the tracks must be preserved even if the zoomed size is bounded.
 |
 */

static void     track_menu_update          (Arrange*, TrackNum);
static void     track_menu__show_pan       (GtkWidget*, Arrange*);
static void     add_menu_item_for_curve    (Arrange*, GtkWidget*, Curve*);
static void     add_menu_item_for_control  (Arrange*, GtkWidget*, AyyiControl*, AMPlugin*);
static void     arr_update_track_selection (Arrange*, AMTrack*);

static void     arr_trckctl_on_selection_change (Arrange*);


void
arr_track_undraw (Arrange* arrange, ArrTrk* arr_trk)
{
	// Remove all widgets and gui data for the given track.

	g_return_if_fail(arr_trk);
	AMTrack* trk = arr_trk->track;
	g_return_if_fail(trk);

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
	TrackControl* tc = TRACK_CONTROL(arr_trk->trk_ctl);
	if (tc) {
		gtk_widget_destroy0(arr_trk->trk_ctl);
	}
#endif
#endif

#ifdef USE_GNOMECANVAS
	if (CANVAS_IS_GNOME) {
		g_automation_track_destroy(arrange, arr_trk);
	}
#endif
	automation_free(arr_trk);
}


/*
 *  A request to change the track. All linked tracks will be also selected.
 *  -cannot be static because sometimes a widget related to the Arrange window needs to change the selection specifically for this window,
 *   eg a Track button, or following creation of a new track.
 */
bool
arr_track_select (Arrange* arrange, AMTrack* track)
{
	g_return_val_if_fail(AYYI_IS_ARRANGE(arrange), false);
	g_return_val_if_fail(track, false);
	if(!track->type || !track->visible) return false;
	AyyiPanel* panel = (AyyiPanel*)arrange;

	dbg (2, "track selection set. %s len=%i selection=%p %p.", track->name, g_list_length(arrange->tracks.selection), arrange->tracks.selection, arrange->tracks.selection->data);

	if(panel->link){
		am_collection_selection_replace((AMCollection*)song->tracks, g_list_prepend(NULL, track), NULL);
		arr_parts_select_by_track(arrange, track_list__get_display_num(arrange->priv->tracks, track));
	}else{
		arr_update_track_selection(arrange, track);
	}
	return true;
}


void
arr_track_select_nearest (Arrange* arrange, TrackDispNum d)
{
	// Used when a track has been deleted.
	// select the previous track. If no previous track, select the following one.
	// -note: this has to bypass the usual panel linking, as the old selection is invalid.

	dbg (1, "tnum=%i new=%i", d, MAX(d - 1, 0));

	TrackDispNum d_ = MAX(d - 1, 0); // in the case of the top track, d will remain the same. This is not an error.

	ArrTrk* atr = arrange->priv->tracks->display[d_];
	if(!atr){
		if((atr = track_list__next_visible (arrange->priv->tracks, atr ? atr->track : NULL))){
			d_ = track_list__get_display_num(arrange->priv->tracks, atr->track);
		}
	}
	g_return_if_fail(atr);

	// This is wrong, it should be done independently in the model.
	// however only the current panel knows which is the adjacently displayed track
	//      *** should set the selection prior to deletion ? ***
	am_collection_selection_replace (am_tracks, g_list_append(NULL, atr->track), NULL);
}


/*
 *  Update the window in response to selection elsewhere.
 *
 *  For global (linked) changes, it acts as an observer,
 *  but for local (non-linked) changes, it actually records the new selection.
 *
 *  A track is _always_ selected, however there is an exception when the song is not loaded.
 */
static void
arr_update_track_selection (Arrange* arrange, AMTrack* trk)
{
	AyyiPanel* panel = (AyyiPanel*)arrange;

	if(!SONG_LOADED) return;

	if(!trk) trk = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);

	if(panel->link){
		arrange->tracks.selection = ((AMCollection*)song->tracks)->selection;
	}else{
		//if(!trk || !trk->type) return; // song may be unloaded

		dbg(1, "d=%i '%s'", track_list__get_display_num(arrange->priv->tracks, trk), trk->name);

		g_clear_pointer(&arrange->tracks.selection, g_list_free);
		arrange->tracks.selection = g_list_prepend(NULL, trk);
	}
	dbg (2, "track selection set. len=%i selection=%p %p.", g_list_length(arrange->tracks.selection), arrange->tracks.selection, arrange->tracks.selection ? arrange->tracks.selection->data : NULL);
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
	if(!(cc->provides & PROVIDES_TRACKCTL)){
		arr_trckctl_on_selection_change(arrange);
	}

#ifdef DEBUG
	if(track_list__track_by_display_index(arrange->priv->tracks, 0)){
		if(_debug_ > 1){
			int d = track_list__get_display_num(arrange->priv->tracks, trk);
			char idd[128]; ayyi_panel_get_id_string(panel, idd);
			dbg (0, "track selection: '%s' len=%i selection=%p d=%i.", idd, g_list_length(arrange->tracks.selection), arrange->tracks.selection, d);
		}
	}
#endif

	if(arrange->tracks.selection){
		trk = arrange->tracks.selection->data;

		arr_scroll_to_track(arrange, trk);
	}
}


bool
arr_track_is_selected (Arrange* arrange, AMTrack* trk)
{
	if(_debug_) g_return_val_if_fail(g_list_length(arrange->tracks.selection) <= am_track_list_count(song->tracks), false);

	return (g_list_index(arrange->tracks.selection, trk) > -1);
}


static void
arr_track_out_sel (GtkWidget* widget, gpointer menu_item)
{
	// Callback from the output-select pop-up-menu.

	AyyiIdx c = GPOINTER_TO_INT(menu_item);

	dbg (1, "c=%i selected=%s", c, am_connection__name(c));

	Arrange* arrange; if( !(arrange = ARRANGE_FIRST) ) return;

	GList* selection = arrange->tracks.selection;
	for(;selection;selection=selection->next){
		AMTrack* tr = selection->data;
		am_track__set_output(tr, ayyi_song__connection_at(c), NULL, NULL);
	}
}


/*
 *  Update the track_display array and redraw the track widgets following a track reorder.
 */
bool
arr_track_move_to_pos (Arrange* arrange, TrackDispNum track_d_num, TrackDispNum new_pos)
{
	dbg(1, "%i --> %i", track_d_num, new_pos);
	TrackList* tl = arrange->priv->tracks;

	// FIXME current strategy doesnt work when tracks are of different types.

	ArrTrk* a = track_list__track_by_display_index(tl, track_d_num);
	ArrTrk* b = track_list__track_by_display_index(tl, new_pos);
	if(a->track->type != b->track->type){
		perr("FIXME source and destination tracks must be the same type");
	}

	track_list__move_to_pos(arrange->priv->tracks, track_d_num, new_pos);

#ifdef USE_GNOMECANVAS
	TrackControl* tc_to_move = TRACK_CONTROL(track_list__track_by_index(tl, track_d_num)->trk_ctl); //!!! FIXME using displaynum offset !!!

	track_list__track_by_index(tl, new_pos)->trk_ctl = GTK_WIDGET(tc_to_move);

	tracks_print();
#endif

	int d0 = MIN(track_d_num, new_pos);
	int d1 = MAX(track_d_num, new_pos);
	GList* tracks = NULL;
	int d; for(d=d0;d<=d1;d++){
		tracks = g_list_append(tracks, tl->display[d]->track);
	}
	g_signal_emit_by_name (song, "tracks-change-by-list", tracks, AM_CHANGE_TRACK, arrange, NULL);
	g_list_free(tracks);
	arr_trckctl_on_selection_change(arrange);

	// update the window's track_selection list
	GList* selected = arrange ? arrange->tracks.selection : am_tracks->selection;
	arrange_foreach {
		AyyiPanel* sm_window = &arrange->panel;
		// FIXME this test will always fail
		if(sm_window != &arrange->panel){
			g_list_free(arrange->tracks.selection);
			arrange->tracks.selection = g_list_copy(selected);
		}
	} end_arrange_foreach

	return TRUE;
}


void
arr_track_pos_update (Arrange* arrange)
{
	int y = 0;

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	// Update the cache of vertical track positions in the track_pos index array following a
	// change in track count or resizing.

	// * track_pos is deprecated - use at->y instead *

	// the track_pos array is 1 bigger than the number of tracks
	// so we can access the top and bottom of the tracks.
	// -track_pos[0] is always zero.
	// -hidden tracks are not in this array.

	// note: track[t]->height is *non*-zoomed, and arrange->track_pos[t] is *zoomed*.

	// do not assume that all tracks have widgets.

	TrackDispNum d;
	if(arrange->canvas->gnome){

		int height_nz; // non-zoomed track height
		int height;

		if(_debug_ && !am_track_list_count(song->tracks)) pwarn ("no tracks!");

		for(d=0;d<am_track_list_count(song->tracks);d++){
			ArrTrk* atr = track_list__track_by_display_index(arrange->priv->tracks, d);
			if(!atr) continue;
			AMTrack* trk = atr->track;
			g_return_if_fail(trk);

			if(trk->visible){
				height_nz = ayyi.got_shm ? atr->height : 30;
				height = MIN(MAX_PART_HEIGHT, height_nz * vzoom(arrange));

				if(height < arr_track_min_height(atr)){
					dbg (2, "track height too small: %i.", height);
					height = arr_track_min_height(atr);
				}

				y += height;
#ifdef USE_GNOMECANVAS
				arrange->canvas->gnome->track_pos[d+1] = y;
#endif
			}
		}
	}

#ifdef DEBUG
	if(false){
		printf("%s(): nonzmd: ", __func__);
		printf("0, ");
		for(d=0;d<am_track_list_count(song->tracks);d++) printf("%i, ", track_list__track_by_display_index(arrange->priv->tracks, d) ? track_list__track_by_display_index(arrange->priv->tracks, d)->height : 0);
		printf("\n");

		printf("%s(): zoomed: ", __func__);
		for(d=0;d<am_track_list_count(song->tracks);d++) printf("%i, ", arrange->priv->tracks->track[d]->y);
		printf("\n");
	}
#endif
#endif

	y = 0;
	ArrTrk* at = NULL;
	while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL))){
		at->y = y;
		y += at->height;
	}
}


void
arr_trkctl_load (Arrange* arrange)
{
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
	if(!(cc->provides & PROVIDES_TRACKCTL)){
		AMTrack* tr;
		ArrTrk* at = NULL;
		while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (tr = at->track)){
			at->trk_ctl = arr_trk_ctl_factory(arrange, at);
		}
	}
	arr_track_pos_update(arrange);
}


void
arr_track_on_selection_change (Arrange* arrange)
{
	// Update the window's track_selection list
	GList* selected = arrange ? arrange->tracks.selection : am_tracks->selection;
	arrange_foreach {
		AyyiPanel* panel = &arrange->panel;
		if(panel != &arrange->panel){ // FIXME this will always be true ?
			g_list_free(arrange->tracks.selection);
			arrange->tracks.selection = g_list_copy(selected);
		}
	} end_arrange_foreach
}


void
arr_trkctl_redraw (Arrange* arrange)
{
	// Redraw the 'track control' area following creation or deletion of tracks.
	// -this does NOT do resizing, so dont use it for zooming.

	int track_count = am_track_list_count(song->tracks);
	dbg(2, "count=%i", track_count);
	g_return_if_fail(track_count <= AM_MAX_TRK);

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	gtk_widget_set_size_request(((TrackListBox*)arrange->track_list_box)->vbox, 20, 20);
#endif

	TrackNum t;
	for(t=0;t<track_count;t++){
		dbg (2, "%strack %i%s", bold, t, white);
		if(!song->tracks->track[t]->visible) continue;
		ArrTrk* arr_trk = track_list__track_by_index(arrange->priv->tracks, t);
		g_return_if_fail(arr_trk);

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		if(arr_trk->trk_ctl) TRACK_CONTROL(arr_trk->trk_ctl)->redraw(TRACK_CONTROL(arr_trk->trk_ctl));
#endif
#endif
	}

	// show selection status on all tracks
	arr_trckctl_on_selection_change(arrange);
}


void
arr_trkctl_enable (Arrange* arrange)
{
	// Show the default track_control (where not provided by the canvas)

	GtkWidget* parent = gtk_widget_get_parent(arrange->canvas->widget);
	if(parent && arrange->canvas->container)
		gtk_widget_reparent(arrange->canvas->container, arrange->priv->vbox_rhs);
	else if(parent)
		gtk_box_pack_start(GTK_BOX(arrange->priv->vbox_rhs), arrange->canvas_scrollwin, EXPAND_TRUE, TRUE, 0);
	else
		gtk_box_pack_start(GTK_BOX(arrange->priv->vbox_rhs), arrange->canvas->widget, EXPAND_TRUE, TRUE, 0);

	gtk_widget_set_no_show_all(arrange->priv->hpaned, false);
	gtk_widget_show_all(arrange->priv->hpaned);

	ArrTrk* at = track_list__next_visible(arrange->priv->tracks, NULL);
	if(at && !at->trk_ctl) arr_trkctl_load(arrange);
}


void
arr_trckctl_on_selection_change (Arrange* arrange)
{
	// Note: this doesnt use track_colour_set() which uses the palette rather than style colours.

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
	// We first 'reset' the previously selected track.
	// As that is currently not stored, we reset them all:
	for (TrackNum t=0; t<AM_MAX_TRK && song->tracks->track[t] && song->tracks->track[t]->type; t++){
		if(!song->tracks->track[t]->visible) continue;
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
		if(!atr){ perr("arrange->track[t] null! t=%i", t); continue; }
		TrackControl* tc = TRACK_CONTROL(atr->trk_ctl);
		if(!tc){ dbg(2, "tc null! t=%i", t); continue; }
		dbg(2, " %i tc=%p", t, tc);
		track_control_draw_unselected(tc);
	}
#endif
#endif

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
	// now draw the selected tracks
	GList* selected = arrange->tracks.selection;
	g_return_if_fail(arrange->tracks.selection);
	for(;selected;selected=selected->next){
		AMTrack* tr = selected->data;

		dbg (2, "drawing selected: arr=%i link=%i %s", ayyi_panel_get_instance_num((AyyiPanel*)arrange), ((AyyiPanel*)arrange)->link, tr->name);
		ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, tr);
		if(atr){
			TrackControl* tc = TRACK_CONTROL(atr->trk_ctl);
			if(tc) track_control_draw_selected(tc);
		}
	}
#endif
#endif
}


void
arr_track_menu_show (Arrange* arrange, TrackNum t, GdkEventButton* event)
{
	g_object_set_data(G_OBJECT(arrange->tracks.menu), "track_idx", track_list__track_by_index(arrange->priv->tracks, t)); //embed the trk ref in the menu.
	track_menu_update(arrange, t);
	gtk_menu_popup(GTK_MENU(arrange->tracks.menu), NULL, NULL, NULL, NULL, event->button, (guint32)(event->time));
}


static void
track_menu_update (Arrange* arrange, TrackNum t)
{
	void _automation_show_vol (GtkWidget* widget, Arrange* arrange)
	{
		ArrTrk* arr_trk = g_object_get_data(G_OBJECT(arrange->tracks.menu), "track_idx");
		automation_view_vol(arr_trk);
	}


	int menu_build_for = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(arrange->tracks.menu), "for_track_idx"));
	if (!t || menu_build_for != t) {
		dbg(1, "menu needs rebuilding...");
		AMTrack* track = song->tracks->track[t];

		GtkWidget* menu = arrange->tracks.menu;
		GList* items = GTK_MENU_SHELL(menu)->children;
		for (;items;items=items->next) {
			GtkMenuItem* item = items->data;
			GtkWidget* submenu = gtk_menu_item_get_submenu(item);
			if (submenu) {

				GtkWidget* auto_submenu = gtk_menu_new();
				gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), auto_submenu);

				const AMChannel* channel = am_track__lookup_channel(song->tracks->track[t]);
				if (!channel) continue;
				AyyiChannel* mixer_track = ayyi_mixer__channel_at_quiet(channel->ident.idx);

				// show already-used controls

				GtkWidget* vol = gtk_image_menu_item_new_with_mnemonic ("Volume");
				GtkWidget* ico_auto1 = gtk_image_new_from_stock ("gtk-zoom-100", GTK_ICON_SIZE_MENU);
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(vol), ico_auto1);
				g_signal_connect((gpointer)vol, "activate", G_CALLBACK(_automation_show_vol), arrange);

				GtkWidget* pan = gtk_image_menu_item_new_with_mnemonic ("Pan");
				GtkWidget* pan_ico = gtk_image_new_from_stock ("gtk-zoom-100", GTK_ICON_SIZE_MENU);
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(pan), pan_ico);
				g_signal_connect((gpointer)pan, "activate", G_CALLBACK(track_menu__show_pan), arrange);

				if (track->bezier.vol->len) {
					gtk_container_add(GTK_CONTAINER(auto_submenu), vol);
				}
				if (song->tracks->track[t]->bezier.pan->len) {
					gtk_container_add(GTK_CONTAINER(auto_submenu), pan);
				}

				AyyiTrack* ayyi_track = ayyi_song__audio_track_at(song->tracks->track[t]->ident.idx);
				if (ayyi_track) {
					AyyiList* l; for(l=ayyi_list__first(mixer_track->automation_list); l; l=ayyi_list__next(l)){
						if (!strlen(l->name)){ pwarn("automation item has no name."); continue; }
						dbg(0, "adding: %s", l->name);
						GtkWidget* mitem = gtk_image_menu_item_new_with_mnemonic(l->name);
						gtk_container_add(GTK_CONTAINER(auto_submenu), mitem);
						GtkWidget* ico = gtk_image_new_from_stock ("gtk-zoom-100", GTK_ICON_SIZE_MENU);
						gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(mitem), ico);
						//g_signal_connect((gpointer)auto2, "activate", G_CALLBACK(automation_view_pan), arrange);
					}
				}

				// show used plugin track->controls
				GList* l = song->tracks->track[t]->controls;
				for (;l;l=l->next) {
					Curve* curve = l->data;
					if (!curve->name || !strlen(curve->name)){ pwarn("automation control has no name - ignoring..."); continue; }

					add_menu_item_for_curve(arrange, auto_submenu, curve);
					/*
					//find the icon:
					plug* plugin = curve_get_plugin(curve);
					GtkWidget* icon = gtk_image_new_from_pixbuf(am_plugin__get_icon(plugin));

					GtkWidget* mitem = gtk_image_menu_item_new_with_mnemonic(curve->name);
					gtk_container_add(GTK_CONTAINER(auto_submenu), mitem);
					gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(mitem), icon);
					g_object_set_data(G_OBJECT(mitem), "curve", curve);
					g_signal_connect((gpointer)mitem, "activate", G_CALLBACK(g_automation_show_control), arrange);
					*/
				}

				menu_separator_new(auto_submenu);

				if (!song->tracks->track[t]->bezier.pan->len) {
					gtk_container_add(GTK_CONTAINER(auto_submenu), pan);
					if (!mixer_track->has_pan)
						gtk_widget_set_sensitive(pan, false);
				}
				if (!song->tracks->track[t]->bezier.vol->len) {
					gtk_container_add(GTK_CONTAINER(auto_submenu), vol);
				}

				// now show all other possible controls
				if (mixer_track) {
					for (int i=0;i<AYYI_PLUGINS_PER_CHANNEL;i++) {
						if (mixer_track->plugin[i].active) {
							AyyiIdx plugin_num = mixer_track->plugin[i].idx;
							AyyiContainer* controls = ayyi_plugin_get_controls(plugin_num);
							if (controls) {
								AyyiPlugin* plugin = ayyi_plugin_at(plugin_num);
								AMPlugin* plug = am_plugin__get_by_name(plugin->name);

								AyyiControl* control = NULL;
								while ((control = ayyi_mixer__plugin_control_next(controls, control))) {
									add_menu_item_for_control(arrange, auto_submenu, control, plug);
								}
							}
						}
					}
				}

				gtk_widget_show_all(GTK_WIDGET(item));

				break;
			}
		}

		// store the track that the menu is currently built for
		g_object_set_data(G_OBJECT(arrange->tracks.menu), "for_track_idx", GINT_TO_POINTER(t));
	}
}


static void
track_menu__show_pan (GtkWidget* widget, Arrange* arrange)
{
	automation_view_pan(g_object_get_data(G_OBJECT(arrange->tracks.menu), "track_idx"));
}


static void
add_menu_item_for_curve (Arrange* arrange, GtkWidget* menu, Curve* curve)
{
	int control_idx = curve->auto_type - 2;
	AyyiPlugin* ayyi_plugin = ayyi_plugin_at(curve->plugin->shm_num);
	if(ayyi_plugin){
		AyyiControl* control = ayyi_plugin_get_control(ayyi_plugin, control_idx);
		add_menu_item_for_control(arrange, menu, control, curve->plugin);
	}
}


static void
add_menu_item_for_control (Arrange* arrange, GtkWidget* menu, AyyiControl* control, AMPlugin* plugin)
{
	char control_name[64] = {0,};

	dbg(4, "control=%s", control->name);
#pragma GCC diagnostic ignored "-Wformat-truncation"
	snprintf(control_name, 63, "%s: %s", plugin->name, control->name);
#pragma GCC diagnostic warning "-Wformat-truncation"
	// escape underscore chars
	replace2(control_name, "_", " ");

	GtkWidget* mitem = gtk_image_menu_item_new_with_mnemonic(control_name);
	gtk_container_add(GTK_CONTAINER(menu), mitem);
	GtkWidget* plugin_icon = gtk_image_new_from_pixbuf(am_plugin__get_icon(plugin));
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(mitem), plugin_icon);

	g_object_set_data(G_OBJECT(mitem), "plugin_idx", GINT_TO_POINTER(plugin->shm_num + 1));
#ifdef USE_GNOMECANVAS
	g_signal_connect((gpointer)mitem, "activate", G_CALLBACK(g_automation_show_control), arrange);
#endif
}


void
arr_observer__midi_key_scroll (TrackControlMidi* trk_ctl)
{
#ifdef USE_GNOMECANVAS
#ifndef DEBUG_TRACKCONTROL
	TrackControl* tc = TRACK_CONTROL(trk_ctl);
	parts_redraw_by_track(tc->arrange, tc->atr->track);
#endif
#endif
}


void
arr_track_set_height (Arrange* arrange, AMTrack* trk, int height)
{
	g_return_if_fail(trk);
	g_return_if_fail(height >= MIN_TRACK_HEIGHT);

	TrackNum t = am_track_list_position(song->tracks, trk);
	// TODO use a gui signal instead, not a model signal.
	g_signal_emit_by_name (song, "tracks-change", t, t, AM_CHANGE_HEIGHT);
}


int
arr_track_min_height (ArrTrk* atr)
{
	// Return the minimum height that a track is able to have.
	// As well as MIN_TRACK_HEIGHT the minumum button size is considered.

#ifdef USE_GNOMECANVAS
	TrackControl* tc = TRACK_CONTROL(atr->trk_ctl);
	if(!tc) return MIN_TRACK_HEIGHT;
	g_return_val_if_fail(tc->bmute, false);

	return tc->bmute->requisition.height;
#else
	return MIN_TRACK_HEIGHT;
#endif
}


void
arr_track_undraw_all (Arrange* arrange)
{
	// Remove all track widgets and any other *gui* track stuff.

	int t;
	for(t=0;t<AM_MAX_TRK && arrange->priv->tracks->track[t];t++) arr_track_undraw(arrange, arrange->priv->tracks->track[t]);
	for(t=0;t<AM_MAX_TRK;t++) arrange->priv->tracks->display[t] = NULL;
}


static void track_menu__new_mono  (GtkWidget* widget, gpointer arrange) { song_add_track(TRK_TYPE_AUDIO, 1, NULL, NULL); }
static void track_menu__new_stereo(GtkWidget* widget, gpointer arrange) { song_add_track(TRK_TYPE_AUDIO, 2, NULL, NULL); }
static void track_menu__new_midi  (GtkWidget* widget, gpointer arrange) { song_add_track(TRK_TYPE_MIDI,  0, NULL, NULL); }
static void track_menu__on_delete (GtkMenuItem* item, Arrange* arrange) { song_del_track(arrange->tracks.selection, NULL, NULL); }


static MenuDef _menu_def[] = {
    {"Add Mono Track",   G_CALLBACK(track_menu__new_mono),   "zoom_in",    true},
    {"Add Stereo Track", G_CALLBACK(track_menu__new_stereo), "zoom_in",    true},
    {"Add Midi Track",   G_CALLBACK(track_menu__new_midi),   "semiquaver", true},
    {"Delete Track",     G_CALLBACK(track_menu__on_delete),  "cross",      true},
};


GtkWidget*
arr_track_menu_init (Arrange* arrange)
{
	// Create the context menu for the arrange window track-control panel.

	GtkWidget* menu = gtk_menu_new ();

	add_menu_items_from_defn(menu, _menu_def, G_N_ELEMENTS(_menu_def), arrange);

	// Automation sub menu (filled dynamically)
	{
		GtkWidget* item = gtk_image_menu_item_new_from_stock(SM_STOCK_AUTOMATION, NULL);
		gtk_menu_item_set_label((GtkMenuItem*)item, "Automation");
		gtk_container_add(GTK_CONTAINER(menu), item);

		gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), gtk_menu_new());
	}

	return menu;
}


// TODO this fn is not currently used.
void
arr_track_ch_menu_update (Arrange* arrange)
{
	// Fill the channel selection menu with the current songcore channel list.

	PF;

	GtkWidget* label;
#ifndef ENGINE_HAS_CHANNELS
	//if 'channels' are not supported, we show outputs instead.
	gboolean active = FALSE;

	// which output is the current track using? (for now lets assume the track is the one with the selection)
	AMTrack* selected = arrange->tracks.selection->data;
	const char* track_output = am_track__get_output_name(selected);
	g_return_if_fail(track_output);
	dbg (0, "track_output=%i: %s", am_track__get_output_num(selected), track_output);

	int out = 0;
	AyyiConnection* shared = NULL;
	while((shared = ayyi_connection__next_output(shared, 0))){

		if((label = gtk_bin_get_child(GTK_BIN(gui_song.ch_mitem[out])))){
			gtk_label_set_text(GTK_LABEL(label), shared->name);
			//is it active?
			dbg (0, "%s=%s", shared->name, track_output);
			if(!strcmp(shared->name, track_output)){ dbg (0, "  active output found! %s", shared->name); active = TRUE; } else active = FALSE;
			gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(gui_song.ch_mitem[out]), active);

			gtk_widget_show(gui_song.ch_mitem[out]);
			out++;
		}
	}

	// hide unused items
	for(;out<AM_MAX_CHS;out++) gtk_widget_hide(gui_song.ch_mitem[out]);

#else
	char* text;
	int c;
	for(c=0;c<shm.song->max_cha;c++){
		if(c<AM_MAX_CHS){
			// the name doesnt really need to be changed each time but still...
			label = gtk_bin_get_child(GTK_BIN(arrange->ch_mitem[c]));
			int label_idx = dcore->obj[shm.song->cha[c]].cha.label_idx;
			if(label_idx < 1 || label_idx > shm.song->max_slots){
				perr ("bad label_idx: %i.", label_idx);
				continue;
			}
			text = dcore->obj[label_idx].str.data;
			gtk_label_set_text(GTK_LABEL(label), text);

			gtk_widget_show(arrange->ch_mitem[c]);
		}
		else gtk_widget_hide(arrange->ch_mitem[c]);
	}

	// hide remaining unused channels
	if(shm.song->max_cha < AM_MAX_CHS){
		for(c=shm.song->max_cha;c<AM_MAX_CHS;c++){
			gtk_widget_hide(arrange->ch_mitem[c]);
		}
	}
	else pwarn ("gui doesnt have enough channels allocated for this song.");
#endif
}


/*
 *  Returns the vertical keyboard span
 */
int
arr_track_midi_get_n_notes (ArrTrackMidi* at)
{
	int note_height = at->note_height;
	g_return_val_if_fail(note_height, -1);

	int track_height = at->track.height * vzoom(at->track.arrange);
	return track_height / note_height;
}


double
arr_track_note_pos_y (ArrTrk* track, MidiNote* note)
{
	ArrTrackMidi* midi = (ArrTrackMidi*)track;

	int n_notes = arr_track_midi_get_n_notes((ArrTrackMidi*)track);
	int top_note = midi->centre_note + n_notes / 2;
	if (note->note > top_note) return -1;

	int y = (top_note - note->note) * midi->note_height;

	return y < track->height * vzoom(track->arrange)
		? y
		: -1;
}


int
arr_track_get_note_num_from_y (ArrTrackMidi* trk, double y)
{
	// Assume that the top note is aligned with the top of the track, which is not correct.

	// @param y    number of pixels relative to track top.

	ArrTrk* at = (ArrTrk*)trk;

	int part_height = at->height * vzoom(at->arrange);
	if (y < 0.0 || y > part_height) return -1;

	int n_notes = arr_track_midi_get_n_notes(trk);
	int top_note = trk->centre_note + n_notes / 2;

	return top_note - (int)(y / trk->note_height);
}


int
arr_track_get_velocity_from_y (ArrTrackMidi* trk, double y)
{
	int part_height = trk->track.height * vzoom(trk->track.arrange);
	int chart_height = trk->velocity_chart_height;
	if(y < 0 || y > part_height) return 0;

	dbg(0, "y=%.2f height=%i", y, part_height);
	y = part_height - y;

	return y * 128 / chart_height;
}


int
arr_track_count_active_controls (ArrTrk* atr)
{
	int n_controls = 0;

	for (int i=0;i<atr->auto_paths->len;i++) {
		ArrAutoPath* aap = atr->auto_paths->pdata[i];
		if (aap && !aap->hidden) n_controls++;
	}

	return n_controls;
}


AMiRange
arr_track_get_automation_range (ArrTrk* atr)
{
	AMiRange range = {INT_MAX,};
	#define PT_SIZE 2

	if (atr->auto_paths) {
		for (AyyiAutoType c=0;c<atr->auto_paths->len;c++) {
			ArrAutoPath* aap = AUTO_PATH(c);
			if (!aap || aap->hidden) continue;

			AMCurve* curve = ((AMCurve**)&atr->track->bezier)[c];
			if (curve && curve->len) {
				int32_t start = curve->path[0].x3;
				if (start < range.start) range.start = start - PT_SIZE;

				uint32_t end = curve->path[curve->len - 1].x3;
				if (end > range.end) range.end = end;
			}
		}
	}

	range.start = MAX(range.start, 0);
	range.end = MIN(range.end, ayyi_pos2samples(&am_object_val(&song->loc[AM_LOC_END]).sp));

	return range;
}


/*
 *  Return the automation data for the given curve.
 */
ArrAutoPath*
arr_track_find_aap (ArrTrk* atr, AMCurve* curve)
{
	for (int i=0;i<atr->auto_paths->len;i++) {
		ArrAutoPath* aap = g_ptr_array_index(atr->auto_paths, i);
		g_return_val_if_fail(aap->curve, NULL);
		if (aap->curve == curve) {
			return aap;
		}
	}

	return NULL;
}


void
tracks_print ()
{
	// Debugging function to show the contents of the track array.

	Arrange* arrange = ARRANGE_FIRST; g_return_if_fail(arrange);

	char arr_str[128];

#if 0
	//show the track[] array:
	int u; for(u=0;u<10 && u<AM_MAX_TRK;u++){
		AMTrack* tr = track[u];
		if(!tr) continue;
		if(tr->type){
			printf("%i: track=%i %s\n", u, tr->type, tr->name->str);
		} else {
			printf("%i: empty\n", u);
		}
	}
#endif

	dbg(0, "(tracks are printed in display order) ...\n");
	printf("   d  t           type     trkctl     bmute name       vis\n");
	TrackDispNum d;
	for(d=0;d<10 && d<AM_MAX_TRK;d++){
		ArrTrk* tr = arrange->priv->tracks->display[d];
		if(!tr){
			if(song->tracks->track[d]){
				printf("  %02i    %p    %i                      %-12s %i\n", d, song->tracks->track[d], song->tracks->track[d]->type, song->tracks->track[d]->name, song->tracks->track[d]->visible);
			} else {
				printf("  %02i    %p\n", d, song->tracks->track[d]);
			}
			continue;
		}
		TrackNum t = am_track_list_position(song->tracks, tr->track);

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		snprintf(arr_str, 127, " %9p %9p", tr->trk_ctl, tr->trk_ctl ? TRACK_CONTROL(tr->trk_ctl)->bmute : NULL);
#endif
#endif

		printf("  %02i %02i %p %4i %s %-12s %i\n", d, t, tr->track, tr->track->type, arr_str, tr->track->name ? tr->track->name : NULL, tr->track->visible);
	}

#if 0
	//show the arrange->track[] array:
	dbg(0, "arrange tracks:");
	for(u=0;u<10 && u<AM_MAX_TRK;u++){
		ArrTrk* at = arrange->track[u];
		if(!at){ printf("  %02i [empty]\n", u); continue; }
		AMTrack* tr = at->track;
		printf("  %02i t=%02i %s\n", u, tr->track_num, tr->name->str);
	}
	//show the arrange->priv->tracks->track_display[] array:
	dbg(0, "arrange track_display[]:");
	for(u=0;u<10 && u<AM_MAX_TRK;u++){
		ArrTrk* at = arrange->priv->tracks->track_display[u];
		if(!at){ printf("  %02i [empty]\n", u); continue; }
		AMTrack* tr = at->track;
		printf("  %02i t=%02i %s\n", u, tr->track_num, tr->name->str);
	}
#endif
}

