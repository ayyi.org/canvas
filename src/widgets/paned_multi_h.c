/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "paned_multi_h.h"
#include "gtk/gtkalias.h"

#include "ayyi_utils.h"

static void     hpaned_multi_size_request   (GtkWidget*, GtkRequisition*);
static void     hpaned_multi_size_allocate  (GtkWidget*, GtkAllocation*);

G_DEFINE_TYPE (HPanedMulti, hpaned_multi, TYPE_PANED_MULTI)

static void
hpaned_multi_class_init (HPanedMultiClass *class)
{
  GtkWidgetClass* widget_class = (GtkWidgetClass *) class;

  widget_class->size_request = hpaned_multi_size_request;
  widget_class->size_allocate = hpaned_multi_size_allocate;
}

static void
hpaned_multi_init (HPanedMulti *hpaned)
{
  g_return_if_fail (IS_PANED_MULTI (hpaned));

  PanedMulti* paned = PANED_MULTI (hpaned);
  
  paned->cursor_type = GDK_SB_H_DOUBLE_ARROW;
  //paned->orientation = GTK_ORIENTATION_VERTICAL;
  paned->orientation = GTK_ORIENTATION_HORIZONTAL;
}

GtkWidget *
hpaned_multi_new (void)
{
  HPanedMulti *hpaned = g_object_new (TYPE_HPANED_MULTI, NULL);

  return GTK_WIDGET (hpaned);
}

static void
hpaned_multi_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
  PanedMulti *paned = PANED_MULTI (widget);
  GtkRequisition child_requisition;

  requisition->width = 0;
  requisition->height = 0;

  int i; for(i=0;i<paned->children->len;i++){
    paned_child* pc = g_ptr_array_index(paned->children, i);
    //dbg(0, "%i: widget=%p", pc->widget);
    if (pc->widget && GTK_WIDGET_VISIBLE (pc->widget)) {
      gtk_widget_size_request (pc->widget, &child_requisition);

      requisition->height = child_requisition.height;
      requisition->width += child_requisition.width;

      //is there a better place for this?
      if(!pc->size) pc->size = child_requisition.width;
    }
  }

  /*
  GtkWidget* child0 = paned_multi_get_child_widget(0);
  GtkWidget* child1 = paned_multi_get_child_widget(1);
  paned_child* pane1 = g_ptr_array_index(paned->children, 1);

  if (child0 && GTK_WIDGET_VISIBLE (child0))
    {
      gtk_widget_size_request (child0, &child_requisition);

      requisition->height = child_requisition.height;
      requisition->width = child_requisition.width;
    }

  if (child1 && GTK_WIDGET_VISIBLE (child1))
    {
      gtk_widget_size_request (child1, &child_requisition);

      requisition->height = MAX(requisition->height, child_requisition.height);
      requisition->width += child_requisition.width;

    }
  */

  requisition->width += GTK_CONTAINER (paned)->border_width * 2;
  requisition->height += GTK_CONTAINER (paned)->border_width * 2;

  //FIXME
  if (1/*child0 && GTK_WIDGET_VISIBLE (child0) &&
      child1 && GTK_WIDGET_VISIBLE (child1)*/)
    {
      gint handle_size;
      
      gtk_widget_style_get (widget, "handle-size", &handle_size, NULL);
      requisition->width += handle_size * paned->children->len;
    }
}

#if 0
static void
flip_child (GtkWidget *widget, GtkAllocation *child_pos)
{
  gint x = widget->allocation.x;
  gint width = widget->allocation.width;

  child_pos->x = 2 * x + width - child_pos->x - child_pos->width;
}
#endif

static void
hpaned_multi_size_allocate (GtkWidget* widget, GtkAllocation *allocation)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);
  gint border_width = GTK_CONTAINER (paned)->border_width;

  widget->allocation = *allocation;

  //GtkWidget* widget0 = paned_multi_get_child_widget(0);
  //GtkWidget* widget1 = paned_multi_get_child_widget(1);

  if (1 /*widget0 && GTK_WIDGET_VISIBLE (widget0) &&  //FIXME what test is needed here? all widgets visible?
      child2 && GTK_WIDGET_VISIBLE (child2)*/)
    {
      int n_children = paned->children->len;
      GtkAllocation  child_allocation[n_children];
      GtkRequisition child_requisition[n_children];
      
      gint handle_size;
      gtk_widget_style_get (widget, "handle-size", &handle_size, NULL);

      int i; for(i=0;i<n_children;i++){
        paned_child* pc = g_ptr_array_index(paned->children, i);
        gtk_widget_get_child_requisition (pc->widget, &child_requisition[i]);
      }

	/*
      paned_multi_compute_position (paned,
				  MAX (1, widget->allocation.width - handle_size - 2 * border_width),
				  child_requisition[0].width,
				  child_requisition[1].width);
	*/

      int x = widget->allocation.x + border_width;
      for(i=0;i<n_children;i++){
        paned_child* pc = g_ptr_array_index(paned->children, i);
        x += pc->size;
        pc->handle_pos.x      = x;
        pc->handle_pos.y      = widget->allocation.y + border_width;
        pc->handle_pos.width  = handle_size;
        pc->handle_pos.height = MAX (1, widget->allocation.height - 2 * border_width);

        x += handle_size;
      }

      paned_child* child0 = g_ptr_array_index(paned->children, 0);
      child_allocation[0].height = MAX (1, (gint) allocation->height - border_width * 2);
      child_allocation[0].width  = MAX (1, child0->size);
      child_allocation[0].x      = widget->allocation.x + border_width;
      child_allocation[0].y      = child_allocation[1].y = widget->allocation.y + border_width;

      /*
      paned_child* pc = g_ptr_array_index(paned->children, 1);
      child2_allocation.x      = child1_allocation.x + child0->size + pc->handle_pos.width;
      //child2_allocation.width  = MAX (1, widget->allocation.x + widget->allocation.width - child2_allocation.x - border_width);
      child2_allocation.width  = MAX (1, paned_multi_get_child_size(1));
      */

      x = child_allocation[0].x + child_allocation[0].width;
      for(i=1;i<n_children;i++){
        paned_child* pc = g_ptr_array_index(paned->children, i);

        x += pc->handle_pos.width;
        dbg(2, "%i: x=%i size=%i", i, x, pc->size);
        child_allocation[i].x      = x;
        //child2_allocation.width  = MAX (1, widget->allocation.x + widget->allocation.width - child2_allocation.x - border_width);
        child_allocation[i].width  = MAX (1, pc->size);
        child_allocation[i].height = MAX (1, (gint) allocation->height - border_width * 2);
        x += pc->size;
      }

      if (GTK_WIDGET_REALIZED (widget)) {
        for(i=0;i<paned->children->len;i++){
          paned_child* pc = g_ptr_array_index(paned->children, i);
          GdkWindow* handle = paned_multi_get_handle(i);
          dbg(2, "  handle temp disabled!"); continue;
          if (GTK_WIDGET_MAPPED (widget)) {
              gdk_window_show (handle);
          }
          gdk_window_move_resize (handle, pc->handle_pos.x, pc->handle_pos.y, handle_size, pc->handle_pos.height);
        }
      }
      
      /* Now allocate the childen, making sure, when resizing not to
       * overlap the windows
       */
      if (1 || GTK_WIDGET_MAPPED (widget) /*&&
	    widget0->allocation.width < child_allocation[0].width*/)
        {
          for(i=0;i<paned->children->len;i++){
            paned_child* pc = g_ptr_array_index(paned->children, i);

            dbg(0, "%i: x=%02i req=%ix%i width=%ix%i visible=%i %i parent=%p", i, pc->widget->requisition.width, pc->widget->requisition.height, child_allocation[i].x, child_allocation[i].width, child_allocation[i].height, GTK_WIDGET_VISIBLE(pc->widget), GTK_WIDGET_REALIZED(pc->widget), gtk_widget_get_parent(pc->widget));
            gtk_widget_size_allocate (pc->widget, &child_allocation[i]);
            //gtk_widget_size_allocate (widget1, &child_allocation[1]);
            //gtk_widget_size_allocate (widget0, &child_allocation[0]);
          }
        }
      /*
      else
        {
          gtk_widget_size_allocate (widget0, &child_allocation[0]);
          gtk_widget_size_allocate (widget1, &child_allocation[1]);
        }
      */
    }
  else //handles case where only one child is visible. We shouldnt use this?
    {
    dbg(0, "***** never get here");
      if (GTK_WIDGET_REALIZED (widget)) {
        int i; for(i=0;i<paned->children->len;i++){
          gdk_window_hide (paned_multi_get_handle(i));
        }
      }

      int i; for(i=0;i<paned->children->len;i++){
        paned_child* pc = g_ptr_array_index(paned->children, i);
        if (pc->widget) gtk_widget_set_child_visible (pc->widget, TRUE);
        //if (child2) gtk_widget_set_child_visible (child2, TRUE);
      }

	  GtkAllocation child_allocation;
      //not sure we need to do this for all children...
      for(i=0;i<paned->children->len;i++){
        paned_child* pc = g_ptr_array_index(paned->children, i);

        child_allocation.x = widget->allocation.x + border_width;
        child_allocation.y = widget->allocation.y + border_width;
        child_allocation.width = MAX (1, allocation->width - 2 * border_width);
        child_allocation.height = MAX (1, allocation->height - 2 * border_width);

        if (pc->widget && GTK_WIDGET_VISIBLE(pc->widget)) gtk_widget_size_allocate (pc->widget, &child_allocation);
      }
      //if (widget0 && GTK_WIDGET_VISIBLE (widget0))      gtk_widget_size_allocate (widget0, &child_allocation);
      //else if (widget1 && GTK_WIDGET_VISIBLE (widget1)) gtk_widget_size_allocate (widget1, &child_allocation);
    }
}

#define __HPANED_MULTI_C__
#include "gtk/gtkaliasdef.c"
