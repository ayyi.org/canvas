/*
  copyright (C) 2004-2021 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#include "global.h"
#include <libgnomecanvas/libgnomecanvas.h>

#include "model/time.h"
#include "windows.h"
#include "song.h"
#include "support.h"
#include "../gnome_canvas_utils.h"
#include "panels/arrange.h"
#include "panels/arrange/part.h"
#include "panels/arrange/part_renamer.h"
#include "panels/arrange/canvas_op.h"
#include "panels/arrange/map_item.h"
#include "part_manager.h"
#include "songmap.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define RESIZING (op.type == OP_RESIZE_LEFT || op.type == OP_RESIZE_RIGHT)
#define SONGMAP_HEIGHT 32.0
G_DEFINE_TYPE_WITH_PRIVATE (SongMap, song_map, GTK_TYPE_SCROLLED_WINDOW)

enum  {
	SONG_MAP_DUMMY_PROPERTY
};

guint click_timer = 0;
static guint instance_num = 0;

static GList*          songmap_selection = NULL;

GType                  song_map_get_type         ();
SongMap*               song_map_construct        (GType object_type);
static void            song_map_finalize         (GObject* obj);
static void            songmap_part_select       (AMPart*);
static void            songmap_clear_selection   ();
static gboolean        songmap_part_is_selected  (AMPart*);
static GnomeCanvasMap* songmap_get_item_for_part (Arrange*, AMPart*);
static gint            songmap_item_on_event     (GnomeCanvasItem*, GdkEvent*, Arrange*);
static gint            songmap_bg_on_event       (GnomeCanvasItem*, GdkEvent*, Arrange*);
static gint            songmap_drag_received     (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer);
static gboolean        click_timer_reset         ();
//static void          songmap_remove_item       (Arrange*, AMPart*);
static void            songmap_remove_all        (Arrange*);
static void            songmap_on_palette_change (GObject*, gpointer);
static AMPart*         songmap_part_new_from_template(SongMap* self, LocalPart*);



SongMap*
song_map_construct (GType object_type)
{
	SongMap* self = g_object_newv (object_type, 0, NULL);
	self->priv->dummy = 5;
	return self;
}


static void
song_map_class_init (SongMapClass* klass)
{
	song_map_parent_class = g_type_class_peek_parent (klass);

	G_OBJECT_CLASS (klass)->finalize = song_map_finalize;

	g_signal_new ("changed", TYPE_SONG_MAP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("new_part", TYPE_SONG_MAP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
}


static void
song_map_init (SongMap* self)
{
	self->priv = song_map_get_instance_private(self);
}


static void
song_map_finalize (GObject* obj)
{
	G_OBJECT_CLASS (song_map_parent_class)->finalize (obj);
}


GtkWidget*
songmap_new (Arrange* arrange)
{
	SongMap* songmap = arrange->songmap = song_map_construct (TYPE_SONG_MAP);
	GtkWidget* self = (GtkWidget*)songmap;
	songmap->height = 16;

	GtkWidget* canvas = songmap->canvas = gnome_canvas_new_aa();

	gnome_canvas_set_scroll_region (GNOME_CANVAS(canvas), 0, 0, arr_canvas_width(arrange), SONGMAP_HEIGHT);

	// put the canvas in a scrollwindow
	GtkScrolledWindow* scrollwin = GTK_SCROLLED_WINDOW(songmap);
	gtk_widget_set_name((GtkWidget*)scrollwin, "Songmap");
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	gnome_canvas_set_center_scroll_region(GNOME_CANVAS(canvas), FALSE);
	show_widget_if((GtkWidget*)scrollwin, SHOW_SONGMAP);
	gtk_container_add(GTK_CONTAINER(scrollwin), canvas);
	gtk_widget_set_size_request(self, -1, songmap->height);
	gtk_widget_show (canvas);

	songmap->bg = gnome_canvas_item_new(gnome_canvas_root (GNOME_CANVAS(canvas)), gnome_canvas_rect_get_type(),
#if 0
		"fill-color-rgba", arrange->bg_colour,
#else
		"fill-color-rgba", song->palette[3], // must be set to catch clicks.
#endif
		"x1", 0.0,                       "y1", 0.0,
		"x2", arr_canvas_width(arrange), "y2", SONGMAP_HEIGHT,
		"width_units", 2.0,
		NULL);
	g_signal_connect(G_OBJECT(songmap->bg), "event", (GCallback)songmap_bg_on_event, arrange);

	songmap_set_parts(arrange);

	gtk_drag_dest_set(GTK_WIDGET(songmap->canvas), GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY/* | GDK_ACTION_LINK | GDK_ACTION_PRIVATE*/));
	g_signal_connect(G_OBJECT(songmap->canvas), "drag-data-received", G_CALLBACK(songmap_drag_received), NULL);

	am_song__connect("palette-change", G_CALLBACK(songmap_on_palette_change), songmap);

	return self;
}


static AMPart*
songmap_part_new_from_template (SongMap* self, LocalPart* part_local)
{
	PF;

	AMPart* part = AYYI_NEW(AMPart,
		.start     = part_local->part.start,
		.track     = part_local->part.track,
		.bg_colour = part_local->part.bg_colour,
		.length    = part_local->part.length
	);
	g_strlcpy(part->name, part_local->part.name, AYYI_NAME_MAX);

	song->map_parts->list = g_list_append(song->map_parts->list, part);

	g_signal_emit_by_name (self, "new-part", part);

	return part;
}


void
songmap_redraw (Arrange* arrange)
{
	SongMap* songmap = arrange->songmap;
	if(!songmap) return;

	GList* l = songmap->parts;
	for(;l;l=l->next){
		GnomeCanvasItem* item = l->data;
		AMPart* part = ((GnomeCanvasMap*)item)->gpart;
		if(part){
			gnome_canvas_item_set(item, "x", (double)(part->start.beat * 8 * hzoom(arrange)), NULL);
			gnome_canvas_map_set_selected(item, FALSE);
		}
	}

	// show selection
	l = songmap_selection;
	for(;l;l=l->next){
		AMPart* p = l->data;
		GnomeCanvasMap* item = songmap_get_item_for_part(arrange, p);
		gnome_canvas_map_set_selected((GnomeCanvasItem*)item, TRUE);
	}
}


void
map_part_new (Arrange* arrange, AMPart* gpart)
{
	g_return_if_fail(gpart);
	if(!arrange->songmap) return;

	GnomeCanvasGroup* rootgroup = gnome_canvas_root(GNOME_CANVAS(arrange->songmap->canvas));

#ifndef OLD_STYLE_MAP_PARTS
	GnomeCanvasItem* item = gnome_canvas_item_new(rootgroup, gnome_canvas_map_get_type(),
                                     //"outline_color_rgba", config->part_outline_colour, //FIXME fails if we remove this.
                                     "outline_color_rgba", 0x000000ff,
                                     "arrange", arrange,
                                     "gpart", gpart,
                                     NULL);
	GNOME_CANVAS_MAP(item)->box_handler_id = g_signal_connect(G_OBJECT(item), "event", (GCallback)songmap_item_on_event, arrange);
	arrange->songmap->parts = g_list_append(arrange->songmap->parts, item);

#else
  #if 0
  GdkColor color1;
  double   offset_y1 = 0;//distance from track top to part top.

  GnomeCanvasGroup* part = GNOME_CANVAS_GROUP(gnome_canvas_item_new(GNOME_CANVAS_GROUP(rootgroup), gnome_canvas_group_get_type(),
              "x", (double)gpart->start.beat * 8 * arrange->hzoom,
              "y", 0.0,
              NULL));
  //partdata->canvasgroup = GNOME_CANVAS_ITEM(part);

  color1.red   = song->palette.red[gpart->colour];
  color1.green = song->palette.grn[gpart->colour];
  color1.blue  = song->palette.blu[gpart->colour];

  int height = arrange->songmap->height;
  if(height<2){ P_ERR ("track_height too small!\n"); return; }

  gpart->box = gnome_canvas_item_new(part, gnome_canvas_rect_get_type(),
    "outline_color", "white",
    "fill_color_gdk", &color1,
    "x1", 0.0, "y1", offset_y1,
    "x2", (double)gpart->length * arrange->hzoom,
    "y2", (double)height-2,
    "width_units", 2.0,
    NULL);

  //label:
  gpart->label_item = gnome_canvas_item_new(part, gnome_canvas_text_get_type(),
           "x",2.0,
           "y", height - 7.0,
           "anchor", GTK_ANCHOR_WEST,
           "fill_color", "black",
           "text", gpart->label,
           NULL);

  //gtk_signal_connect(GTK_OBJECT(arrange->box[part_num]), "event", (GtkSignalFunc)canvas_item_cb, NULL);
  #endif
#endif
}


void
songmap_set_parts (Arrange* arrange)
{
	// Create or destroy any widgets to sync the window with the global songmap list.

	songmap_remove_all(arrange);

	dbg(2, "n_parts=%i", g_list_length(song->map_parts->list));

	GList* parts = song->map_parts->list;
	for(;parts;parts=parts->next){
		map_part_new(arrange, parts->data);
	}
}


void
songmap_set_part_name (Arrange* arrange, AMPart* part)
{
	GnomeCanvasMap* item = songmap_get_item_for_part(arrange, part);
	if(item){
		gnome_canvas_item_set(GNOME_CANVAS_ITEM(item), "label", part->name, NULL);
	}
}


void
songmap_set_size (Arrange* arrange)
{
	if(!arrange->songmap) return;

	gnome_canvas_set_scroll_region(GNOME_CANVAS(arrange->songmap->canvas), 0.0, 0.0, arr_canvas_width(arrange), (double)30.0);
}


static GnomeCanvasMap*
songmap_get_item_for_part (Arrange* arrange, AMPart* part)
{
	SongMap* songmap = arrange->songmap;
	GList* l = songmap->parts;
	for(;l;l=l->next){
		GnomeCanvasMap* item = l->data;
		if(part == item->gpart){
			return item;
		}
	}
	pwarn("canvas item not found. part=%p", part);
	return NULL;
}


static void
songmap_part_select (AMPart* part)
{
	PF;
	GList* old_selection = songmap_selection;
	songmap_selection = g_list_append(NULL, part);

	arrange_foreach {
		GList* l = old_selection;
		for(;l;l=l->next){
			AMPart* p = l->data;
			GnomeCanvasMap* item = songmap_get_item_for_part(arrange, p);
			gnome_canvas_map_set_selected((GnomeCanvasItem*)item, FALSE);
		}
		GnomeCanvasMap* item = songmap_get_item_for_part(arrange, part);
		if(item) gnome_canvas_map_set_selected((GnomeCanvasItem*)item, TRUE);
	} end_arrange_foreach

	g_list_free(old_selection);
}


static void
songmap_clear_selection ()
{
	GList* old_selection = songmap_selection;
	songmap_selection = NULL;

	arrange_foreach {
		GList* l = old_selection;
		for(;l;l=l->next){
			AMPart* p = l->data;
			GnomeCanvasMap* item = songmap_get_item_for_part(arrange, p);
			gnome_canvas_map_set_selected((GnomeCanvasItem*)item, FALSE);
		}
	} end_arrange_foreach

	g_list_free(old_selection);
}


static gboolean
songmap_part_is_selected (AMPart* part)
{
	return FALSE;
}


static void
songmap_on_palette_change (GObject* _song, gpointer user_data)
{
	// Any or all of the palette colours may have changed.

	SongMap* songmap = user_data;
	g_return_if_fail(songmap);

	gnome_canvas_item_set(songmap->bg, "fill-color-rgba", song->palette[3], NULL);
}


void
op__resize_right_motion ()
{
	PF;
}


static Op ops[N_OP_TYPES] = {
	{NULL,},
};


static gint
songmap_item_on_event (GnomeCanvasItem* item, GdkEvent* event, Arrange* arrange)
{
	SongMap* songmap = arrange->songmap;
	static double startx;          // cursor position before the start of a drag.
	static double old_x_bounded;   // values from the previous callback.
	double new_x;                  // mouse coords, bounded.
	static double diffx;           // the difference between the cursor and the left of the part.
	                               // -this should be constant throughout a drag operation.
	static CanvasOp op = {OP_NONE, NULL, NULL, NULL, NULL};
	//static Pti origin;           // position that scrolling is done relative to.
	                               // -intitially it is the original cursor position.
	static int scrolltime = 0;     // counts the number of consequtive window scrolls in order to set the speed.
	static double max_movement;    //  (x axis) used to detect false moves.
	GnomeCanvasMap* map_item = GNOME_CANVAS_MAP(item);
	op.part = map_item->gpart;
	static LocalPart* part_local;//temp part used during copy operations.
	static GCanvasLocalPart* part_local_;

	gboolean shift   = event->button.state & GDK_SHIFT_MASK;
	gboolean control = event->button.state & GDK_CONTROL_MASK;

	op.mouse.x = event->button.x;

	// get offset of cursor from part boundary to constrain movement:
	gnome_canvas_item_get_bounds(item, &op.p1.x, &op.p1.y, &op.p2.x, &op.p2.y);

	int xscrolloffset;
	int yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(songmap->canvas), &xscrolloffset, &yscrolloffset);

	void
	op__set_type(Optype type)
	{
		*op.op = ops[op.type = type];
	}

	/*
	FeatureType
	op__pick_feature_type()
	{
		//gboolean at_start, at_end;
		return FEATURE_NONE;
	}
	*/

	void rename_done(char* text, gpointer map_item)
	{
		arrange_foreach {
			songmap_set_part_name(arrange, ((GnomeCanvasMap*)map_item)->gpart);
		} end_arrange_foreach;
	}

	void gcanvas_rename_popup_get_position(GnomeCanvasMap* item, gint* _x, gint* _y)
	{
		double px1, py1, px2, py2;
		gnome_canvas_item_get_bounds(GNOME_CANVAS_ITEM(item), &px1, &py1, &px2, &py2);

		*_x = px1, *_y = py2;
	}

	switch (event->type){
		case GDK_ENTER_NOTIFY:
			break;
		case GDK_BUTTON_PRESS:
			if(click_timer){
				//double click. show the rename box.
				Pti pos; gcanvas_rename_popup_get_position(map_item, &pos.x, &pos.y);
				part_rename_popup(&arrange->panel, songmap->canvas, pos, TRUE, map_item->gpart, rename_done, map_item);
				return HANDLED;
			}else click_timer = g_timeout_add(200/*ms*/, (gpointer)click_timer_reset, NULL);

			switch(event->button.button){
				case 3:
					break;
				case 1:
          gnome_canvas_item_raise_to_top(item);

          startx = op.mouse.x;
          old_x_bounded  = op.mouse.x;
          diffx  = op.mouse.x - op.p1.x;
          max_movement = 0.0;
          //init autoscrolling vars:
          //origin.x = win_x + xscrolloffset;

          //update the selection:
          if (shift) {
            dbg(0, "FIXME not implemented");
          } else if (control) {
            dbg(0, "FIXME CTL not implemented");
          } else {
            if (songmap_part_is_selected(op.part)) {
              //dont change the selection.
            } else {
              //the part grabs the selection:
              songmap_part_select(op.part);
            }
          }

          if (shift){
          }
          else if(control){
            dbg (2, "COPY PRESS");
            op__set_type(OP_COPY);
            // make a new temp part
            part_local = part_local_new_from_part(arrange, op.p1.x, op.p1.y, map_item->gpart, FALSE);
            part_local->part.track = NULL;
            gcanvas_part_local_draw(gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget)), part_local);

          } else {
            FeatureType feature;
            if((feature = pick_feature_type(&op))){
              switch(feature){
                case FEATURE_PART_RIGHT:
                  dbg(0, "FEATURE_RIGHT!");
                  break;
                default:
                  break;
              }
            }

            op__set_type(OP_MOVE);

            canvas_item_grab(item, event, GDK_FLEUR);

            // create a 'ghost' part to show the original position
            part_local = part_local_new_from_part(arrange, op.p1.x, 0, op.part, TRUE);
			part_local_ = (GCanvasLocalPart*)part_local;
            gcanvas_part_local_draw(gnome_canvas_root(GNOME_CANVAS(songmap->canvas)), part_local);
            gnome_canvas_item_lower_to_bottom(part_local_->canvasgroup);
            //gnome_canvas_item_lower(part_local->canvasgroup, 1);
            gnome_canvas_item_raise(part_local_->canvasgroup, 1); //hack!
          }

          break;
      }
      break;

    case GDK_MOTION_NOTIFY:
      max_movement = MAX(max_movement, ABS(startx - op.mouse.x));
      new_x        = op.mouse.x;

      switch(op.type){
        case OP_MOVE:
          {
          //horizontal edge detection. Are we at the edge of the canvas?:
          Size vp = arr_canvas_viewport_size(arrange);
          if(new_x > vp.width + xscrolloffset -2){
            //scroll the window rightwards:
            //FIXME copy the timeout code from canvas_item_scroll_cb();
            //gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &sx, &sy);
            //printf("  scroll!! offsets: x=%i y=%i\n", xscrolloffset, yscrolloffset);
            xscrolloffset += 2 + scrolltime;
            gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset);
            scrolltime++;
            scrolltime = MIN(scrolltime, MAX_SCROLL_SPEED);
          } else if (new_x < xscrolloffset + 2){
            //scroll the window leftwards:
            //printf("  scroll!! offsets: x=%i y=%i\n", xscrolloffset, yscrolloffset);
            xscrolloffset -= 2 + scrolltime;
            gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset);
            scrolltime++;
            scrolltime = MIN(scrolltime, MAX_SCROLL_SPEED);
          } else scrolltime = 0;

          gnome_canvas_item_set(item, "x", new_x - diffx, NULL);
          }
          break;

        case OP_COPY:
          //move the new part to follow the mouse:
          new_x = MAX(op.mouse.x, diffx);
          gnome_canvas_item_move(part_local_->canvasgroup, new_x - old_x_bounded, 0.0);//relative movement!
          break;

        default:
          break;
      }
      if(op.op->motion) op.op->motion(&op);
      old_x_bounded = new_x;
      break;

    case GDK_BUTTON_RELEASE:
      gnome_canvas_item_ungrab(item, event->button.time); //should be done in canvas_op->finish() instead.
      switch(op.type){
        case OP_MOVE:
          {
            dbg (0, "MOVE RELEASE");

            op.part = map_item->gpart;

            part_local_destroy(part_local);

            //double final_x;

            //detect false moves:
            //-this appears to be ok. If we need more control maybe use a timer (events are timestamped!),
            //-we do the check in the Release phase to avoid the 'sticky' intertia feeling.
            if(max_movement > 2.0){
              GPos pos;
              arr_snap_px2pos(arrange, &pos, op.p1.x);
              //final_x = arr_pos2px(arrange, &pos);
            } else {
              //stick with the original position:
              //final_x = startx - diffx;
              //printf("  diffx=%.2f startx=%.2f final_x=%.2f\n", diffx, startx, final_x);
            }

            if(max_movement > 2.0){
              //update part data:
              dbg (2, "  dropping... updating part data...");
              arr_snap_px2spos(arrange, &op.part->start, op.p1.x);

              //int start_beat    = arr_px2beat(arrange, p1.x);
              //gpart->start.beat = (int)(start_beat + xscrolloffset);//hack for unused local data. (obsolete?)
              //printf("drop: setting beat=%i\n", partdata->start2.beat);

              gnome_canvas_item_set(item, "x", arr_spos2px(arrange, &op.part->start), NULL);
            }
          } break; //end MOVE.

        case OP_COPY:
          dbg (0, "COPY RELEASE");
          //set the part start according to mouse position:
          arr_snap_px2spos(arrange, &part_local->part.start, MAX(op.mouse.x - diffx, 0));

          //make a new permanent songmap part:
          if(max_movement > 2.0){
            songmap_part_new_from_template(songmap, part_local);
          }

          part_local_destroy(part_local); //remove the temp local part
          //gnome_canvas_item_hide(GNOME_CANVAS_ITEM(target->group));

          break;
      }
      op__set_type(OP_NONE);
      break;

    case GDK_LEAVE_NOTIFY:
      if(!RESIZING){
        arr_cursor_reset(arrange, songmap->canvas->window);
      }
      break;
    default:
      break;
  }
  return HANDLED;
}


static gint
songmap_bg_on_event (GnomeCanvasItem* item, GdkEvent* event, Arrange* arrange)
{
	switch (event->type){
		case GDK_BUTTON_PRESS:
			songmap_clear_selection();
			break;
		case GDK_2BUTTON_PRESS:
			dbg(0, "double click");
			instance_num++;
			AMPart* gpart = g_new0(AMPart, 1);

			gpart->start  = (AyyiSongPos){0,};
			gpart->length = (AyyiSongPos){8 * 4, 0, 0};
			snprintf(gpart->name, 63, "%i", instance_num);

			song->map_parts->list = g_list_append(song->map_parts->list, gpart);

			g_signal_emit_by_name (arrange->songmap, "new-part", gpart);
			break;
		default:
			break;
	}
	return HANDLED;
}


static gint
songmap_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	PF;
	int palette_num;

	if(!data || data->length < 0){ perr ("dnd: no data!"); return -1; }
	if(info == GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING)) printf(" type=string.\n");

	Arrange* arrange = (Arrange*)windows__get_panel_from_widget(widget);

	// get the scrollposition
	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
	dbg (2, "x=%i+%i.", x, xscrolloffset);
	x += xscrolloffset;
	y += yscrolloffset;

	if(info == GPOINTER_TO_INT(AYYI_TARGET_URI_LIST)){
		GList* list = uri_list_to_glist((gchar*)data->data);
		int i = 0;
		for(; list; list = list->next){
			char* u = g_list_nth_data(list, i);
			dbg (2, "%i: %s", i, u);
			gchar* method_string;
			vfs_get_method_string(u, &method_string);
			dbg (2, "method=%s", method_string);

			if(!strcmp(method_string, "colour")){

				//we need the palette number only, which is passed directly.
				char* str = (char*)data->data;
				palette_num = atoi(str+7);
				if(!palette_num){ pwarn ("cannot parse colour data!"); return -1; }
				palette_num--; //restore zero indexing.

				GnomeCanvasItem* item = gnome_canvas_get_item_at(GNOME_CANVAS(widget), (double)x, (double)y);
				if(!item) return false;
				if(GNOME_IS_CANVAS_MAP(item)){
					AMPart* gpart = GNOME_CANVAS_MAP(item)->gpart;
					if(gpart){
						GdkDragAction suggested_action = drag_context->suggested_action;
						dbg (2, "x=%i y=%i item=%p part=%s suggested_action=%i", x, y, item, gpart->name, suggested_action);

						//set colour depending on whether SHIFT was pressed:
						if(suggested_action == 2){
							gpart->bg_colour = palette_num; 
						} else gpart->fg_colour = song->palette[palette_num];
						gnome_canvas_item_set(item, "x", gnome_canvas_map_get_x(GNOME_CANVAS_MAP(item)), NULL); //force a redraw
					}
					i++;
				}else{
					dbg (0, "setting songmap background... FIXME");
				}
			}else{
				dbg (0, "unknown dnd method string.");
			}
		}
		uri_list_free(list);
	}

	return false; 
}


static gboolean
click_timer_reset ()
{
	// mouse double-click timer finished. No double-click.
	click_timer = 0;
	return false;
}


void
songmap_print ()
{
	PF;
	GList* l = song->map_parts->list;
	for(;l;l=l->next){
		AMPart* part = l->data;
		char bbst1[32]; ayyi_pos2bbst(&part->start, bbst1);
		char bbst2[32]; ayyi_pos2bbst(&part->length, bbst2);
		dbg(0, "  %s %s %s", bbst1, bbst2, part->name);
	}
}


#if 0
static void
songmap_remove_item (Arrange* arrange, AMPart* part)
{
}
#endif


static void
songmap_remove_all (Arrange* arrange)
{
	SongMap* songmap = arrange->songmap;
	if(!songmap) return;

	GList* l = songmap->parts;
	for(;l;l=l->next){
		GnomeCanvasItem* item = l->data;
		gtk_object_destroy(GTK_OBJECT(item));
	}
	g_list_free0(songmap->parts);
}


