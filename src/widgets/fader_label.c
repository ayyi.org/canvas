/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "fader_label.h"

struct _FaderLabelPrivate {
    int dummy;
};

G_DEFINE_TYPE_WITH_PRIVATE (FaderLabel, fader_label, GTK_TYPE_DRAWING_AREA)
enum  {
    FADER_LABEL_0_PROPERTY
};
static gboolean fader_label_expose_event (GtkWidget* base, GdkEventExpose* event);
static void     fader_label_finalize     (GObject*);


FaderLabel*
fader_label_construct (GType object_type)
{
	return (FaderLabel*) g_object_new (object_type, NULL);
}


FaderLabel*
fader_label_new (void)
{
	return fader_label_construct (TYPE_FADER_LABEL);
}


static void
fader_label_class_init (FaderLabelClass* klass)
{
	fader_label_parent_class = g_type_class_peek_parent (klass);
	((GtkWidgetClass*)klass)->expose_event = (gboolean (*) (GtkWidget*, GdkEventExpose*)) fader_label_expose_event;
	G_OBJECT_CLASS(klass)->finalize = fader_label_finalize;
}


static void
fader_label_init (FaderLabel* self)
{
	self->priv = fader_label_get_instance_private(self);
}


static void
fader_label_finalize (GObject* obj)
{
	G_OBJECT_CLASS (fader_label_parent_class)->finalize (obj);
}


static gboolean
fader_label_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	gdk_window_clear_area (widget->window, 0, 0, widget->allocation.width, widget->allocation.height);

	char* label[] = {"0", "-10", "-20", "-30", "-40"};

	for (int i=1; i<5; i++){
		gdk_draw_line (widget->window,
			widget->style->fg_gc[GTK_STATE_INSENSITIVE],
			0,
			i * widget->allocation.height / 8,
			5,
			i * widget->allocation.height / 8
		);

		// text
		if(widget->allocation.width > 15){
			PangoLayout* playout = gtk_widget_create_pango_layout (widget, label[i-1]);
			PangoFontDescription* fdesc = pango_font_description_from_string("Bold 8");
			pango_layout_set_font_description(playout, fdesc);
			gdk_draw_layout (widget->window,
				widget->style->base_gc[GTK_STATE_NORMAL],
				2,                                    // x
				i * widget->allocation.height / 8 +2, // y
				playout
			);
			g_object_unref(playout);
		}
	}
	return TRUE;
}

