/*
 * Copyright (C) 2008-2011 Tim Orford. Part of the Ayyi project. http://ayyi.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * time_ruler, modified from GtkHRuler
 * by Conrad Parker 2000 for Sweep.
 */

/*
 * GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __time_ruler_h__
#define __time_ruler_h__

#ifndef DEBUG_DISABLE_RULERBAR

#include <gdk/gdk.h>
#include <gtk/gtkruler.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

//--------------------------------------

//sweep types:

typedef struct _sw_format sw_format;
/*
 * sw_format: a sampling format.
 *
 * Multichannel data is interleaved: Stereo is stored LR.
 */
struct _sw_format {
  gint channels;  /* nr channels per frame */
  gint rate;      /* sampling rate (Hz) */
};

/* Time, in seconds */
typedef gfloat sw_time_t;

//--------------------------------------

typedef struct _scroll_op RulerScrollOp;

#define TIME_RULER(obj)          GTK_CHECK_CAST (obj, time_ruler_get_type (), TimeRuler)
#define TIME_RULER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, time_ruler_get_type (), TimeRulerClass)
#define GTK_IS_TIME_RULER(obj)   GTK_CHECK_TYPE (obj, time_ruler_get_type ())


typedef struct _TimeRuler       TimeRuler;
typedef struct _TimeRulerClass  TimeRulerClass;

struct _TimeRuler
{
  GtkRuler ruler;

  guint height;
  gint samplerate;
  Arrange* arrange;
  void (*text_out)(AyyiPanel*, const char* format, ...);
  int locators[AYYI_MAX_LOC]; //cached pixel position of song->loc[] - 1=LEFT, 2=RIGHT
  int end_loc;
  gboolean dragging;
  RulerScrollOp* scroll_op;
};

struct _TimeRulerClass
{
  GtkRulerClass parent_class;
};


GType      time_ruler_get_type    ();
GtkWidget* time_ruler_new         (Arrange*);
void       time_ruler_set_format  (TimeRuler*, sw_format*);
void       time_ruler_set_start   (TimeRuler*, double start);
void       time_ruler_set_pos     (TimeRuler*, int x);
void       time_ruler_set_zoom    (TimeRuler*, double zoom, double start);
void       time_ruler_update_flags(TimeRuler*);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //DEBUG_DISABLE_RULERBAR

#endif /* __time_ruler_h__ */
