/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

static AGlActor*   arr_gl_ruler                (GtkWidget*);
static void        arr_gl_ruler_free           (AGlActor*);
static void        arr_gl_ruler_set_size       (AGlActor*);
static bool        arr_gl_ruler_on_event       (AGlActor*, GdkEvent*, AGliPt);
static void        arr_gl_ruler_update_locator (ViewObject*, ArrTrk*, AyyiSongPos*, AyyiHandler2, gpointer);
static void        arr_gl_ruler_abort_change   (ViewObject*);
static ViewObject* arr_gl_ruler_pick           (AGlActor*, AGliPt);

extern const char* am_locator_name             (int);

static AGlActorClass ruler_class = {0, "Ruler", (AGlActorNew*)arr_gl_ruler, arr_gl_ruler_free};


static ViewObjectClass ruler_view_class;


typedef struct {
    VOActor    actor;
	ViewObject flex_padding[AM_LOC_MAX];
	struct {
	    int    start;
	} cache;
} RulerActor;

typedef struct {
   int       loc_num;
   AGlActor* actor;
   Arrange*  arrange;
} C;

typedef struct {
   ViewObject* grabbed;
   AGliPt     xy;
} RulerGrab;

static RulerGrab ruler_grab = {0,};


static AGlActor*
arr_gl_ruler (GtkWidget* panel)
{
	Arrange* arrange = (Arrange*)panel;

	bool arr_gl_ruler_draw_gl1 (AGlActor* actor)
	{
		#define ruler_line(A, Y, C) glVertex3f( A, Y, C ); glVertex3f( A, y0, -y0   ); \
					if(c->isometric){ \
						glVertex3f( A, ISOMETRIC_HEIGHT * vzoom(arrange), -Y ); \
						glVertex3f( A, ISOMETRIC_HEIGHT * vzoom(arrange), y0 ); \
					}

		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

		struct label {
			float x;
			float val;
		} labels[64];  // TODO estimate how many are needed.

		int lw = 0;    // write idx.
		int lr = 0;    // read idx.

		void rbl_add(float x, float val)
		{
			if(lw < 64){
				labels[lw].x = x;
				labels[lw].val = val;
				lw++;
			}
		}
		struct label* rbl_next(){
			return lr < lw ? &labels[lr++] : NULL;
		}

		double y0 = 0;
		double height = arrange->canvas->vtab[1];
		double y[4] = {y0 - height, y0 - 7, y0 - 3, y0 - 1.0};

		double beat_width = arr_gl_pos2px(arrange, &(GPos){1, 0, 0});
		int vp_width = arrange->canvas_scrollwin->allocation.width;

		GdkColor bg; get_style_bg_color(&bg, GTK_STATE_NORMAL);
		GdkColor fg; get_style_fg_color(&fg, GTK_STATE_NORMAL);

		agl_enable(0 /* !AGL_ENABLE_TEXTURE_2D */);

		// shadow
		{
			int top = -31; //FIXME y[0] is wrong (-48)
			glLineWidth(1);
			float start = 0.8;
			glBegin(GL_LINES);
			int i; for(i=0;i<4;i++){
				float o = start + i * ((1 - start)/4);
				glColor3f(((float)bg.red * o)/0x10000, ((float)bg.green * o)/0x10000, ((float)bg.blue * o)/0x10000);
				glVertex3f(0,        top + i, 0);
				glVertex3f(vp_width, top + i, 0);
			}
			glEnd();
		}

		glColor3f(((float)fg.red)/0x10000, ((float)fg.green)/0x10000, ((float)fg.blue)/0x10000);

		glColor3f(0.0, 1.0, 0.0);

		double vp_left = - c->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
		double beat = arr_px2beat(arrange, vp_left);

		int first_segment = beat / (4 * song->beats2bar);

		int i, j, k, m;
		for(i=first_segment; i<first_segment+40; i++){
			// start a 4 bar segment
			GPos pos = {i * 4 * song->beats2bar, 0, 0};
			double x = arr_gl_pos2px(arrange, &pos) - vp_left;

			if(x > arrange->canvas_scrollwin->allocation.width) break;

			glLineWidth(1);
			glBegin(GL_LINES);
			ruler_line(x, y[0], 0);
			if(c->isometric){
				//glVertex3f( x, ISOMETRIC_HEIGHT * arrange->vzoom, 100 );
				//glVertex3f( x, ISOMETRIC_HEIGHT * arrange->vzoom, y2 );
			}
			glEnd();

			if(hzoom(arrange) > 1 || !(i % 4) || (hzoom(arrange) > 0.5 && !(i % 2))){
				rbl_add(x, i*4);
			}

			if(hzoom(arrange) < 0.1) continue;

			// 3 more bar lines
			//glLineWidth( arrange->hzoom > 32 ? 2 : 1 );
			glBegin(GL_LINES);
			for(j=1;j<4;j++){
				pos.beat += song->beats2bar;
				x += 4 * beat_width;
				ruler_line(x, y[1], 0);
															glColor3f(0.0, 0.5, 1.0);
															ruler_line(x+1, y[1], 0);
															glColor3f(0.0, 1.0, 0.0);
				if(hzoom(arrange) > 4) rbl_add(x, i*4 + j);
			}
			glEnd();

			if(hzoom(arrange) > 0.5){
				glLineWidth(1);
				float name;

				// beat lines
				for(j=0;j<4;j++){
					glBegin(GL_LINES);
					for(k=1;k<song->beats2bar;k++){
						pos.beat = i * 4 * song->beats2bar + (j * song->beats2bar) + k;
						x = pos.beat * beat_width - vp_left;
						ruler_line( x, y[2], 0 );

						name = (float)k;
						if(hzoom(arrange) > 16) rbl_add(x, name/10);
					}
					glEnd();
				}

				glBegin(GL_LINES);
				if(hzoom(arrange) > 1.5){

					// sub-beat lines
					for(j=0;j<4;j++){
						for(k=0;k<song->beats2bar;k++){
							pos.beat = i * 4 * song->beats2bar + (j * song->beats2bar) + k;
							for(m=1;m<song->beats2bar;m++){
								x = pos.beat * beat_width + (beat_width * m)/ 4.0f - vp_left;
								ruler_line( x, y[3], 0 );
							}
						}
					}
				}
				glEnd();
			}
		}

		// end flag
		double end_px = arr_beats2px(arrange, am_object_val(&song->loc[AM_LOC_END]).sp.beat);
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_POLYGON);
		glVertex2f((end_px   ), y[0]);
		glVertex2f((end_px+10), y[0]);
		glVertex2f((end_px+10), y[0]-10);
		glEnd();

		// text labels
		struct label* lbl = NULL;
		while((lbl = rbl_next())){
			agl_print(lbl->x + 5, y0-18, 0, 0xcccccc80, "%.1f", lbl->val);
		}
		gl_warn("gl error");
		return true;
	}

	bool arr_gl_ruler_draw (AGlActor* actor)
	{
		RulerActor* ruler = (RulerActor*)actor;
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = arrange->canvas->gl;

		double beat_width = arr_gl_pos2px(arrange, &(GPos){1,});
		AGliRegion scrollable = c->actors[ACTOR_TYPE_MAIN]->scrollable;
#ifdef AGL_ACTOR_RENDER_CACHE
		scrollable.x1 -= actor->cache.offset.x;
		int width = actor->fbo->width;
#else
		int width = agl_actor__width(actor);
#endif

		RulerShader* shader = c->wfc->shaders.ruler;
		shader->uniform.fg_colour = 0xff99339f;
		shader->uniform.beats_per_pixel = 1.0 / beat_width;
		shader->uniform.samples_per_pixel = 1.0 / arr_gl_samples2px(arrange, 1);
		shader->uniform.viewport_left = -scrollable.x1;

		for(int i=0;i<AM_LOC_MAX;i++){
			// TODO shader val is currently int - change to samples
			shader->uniform.markers[i] = ayyi_pos2samples(&song->loc[i].vals[0].val.sp);
		}
		agl_use_program((AGlShader*)shader);

		agl_rect (0.0, 0.0, width, agl_actor__height(actor));

		//
		// text labels
		//

		int n_labels = width / 100;
		struct label { float x, val; } labels[n_labels];
		int lw = 0;    // write idx.
		int lr = 0;    // read idx.

		void rbl_add (float x, float val)
		{
			if(lw < n_labels){
				labels[lw++] = (struct label){.x = x, .val = val};
			}
		}
		struct label* rbl_next()
		{
			return lr < lw ? &labels[lr++] : (lr = 0, NULL);
		}

		double beat = arr_px2beat(arrange, -scrollable.x1);
#if 0
		iRange bar = {
			.start = beat / song->beats2bar,
			.end = (beat + arr_px2beat(arrange, viewport->x + ((AGlRootActor*)c->actor)->viewport.w + 64.0)) / song->beats2bar
		};

		for(i=bar.start; i<bar.end; i++){
			GPos pos = {i * song->beats2bar, 0, 0};
			double x = arr_gl_pos2px(arrange, &pos) - viewport->x;

			if(x > width - 2.0) break;

			if(!(i % 16) || arrange->hzoom > 4 || (arrange->hzoom > 2 && !(i % 2)) || (arrange->hzoom > 1 && !(i % 4)) || (arrange->hzoom > 0.5 && !(i % 8))){
				//dbg(0, "  x=%.2f i=%i 4=%i beat=%i", x, i, i*4, pos.beat);
				rbl_add(x, i);

						//beat
						//TODO this is repeated below
						/*
						int b = 0;
						if(arrange->hzoom > 8.0){
							int bt; for(bt=1;bt<song->beats2bar;bt++){
								rbl_add(x + arr_gl_pos2px(arrange, &(GPos){b * song->beats2bar + bt, 0, 0}), i + b + bt/10.0);
							}
						}
						*/
				//bar
				if(arrange->hzoom > 4.0){
					int b; for(b=1;b<4;b++){
						//rbl_add(x + b * arr_gl_pos2px(arrange, &(GPos){song->beats2bar, 0, 0}), i + b);

						//beat
						if(arrange->hzoom > 8.0){
							int bt; for(bt=1;bt<song->beats2bar;bt++){
								rbl_add(x + arr_gl_pos2px(arrange, &(GPos){b * song->beats2bar + bt, 0, 0}), i + b + bt/10.0);
							}
						}
					}
				}
			}
		}
#else
		int q = agl_power_of_two(12.0 / hzoom(arrange));
		GPos increment = {q, 0, 0};
		GPos pos = {((int)beat) / q * q,};
		double x;
		while((x = arr_gl_pos2px(arrange, &pos) + scrollable.x1) < width - 2.0){
			rbl_add(x, pos.beat / 4 + 1 + ((float)(pos.beat % 4 + 1)) / 10.0);

			pos_add(&pos, &increment);
		}
#endif

		double y0 = agl_actor__height(actor) - actor->region.y1;
		char* format = hzoom(arrange) > 2.5 ? "%.1f" : "%.0f";
		struct label* lbl = NULL;
		while((lbl = rbl_next())){
			agl_print(lbl->x + 3, y0 - 18, 0, 0xcccccc80, format, lbl->val);
		}

		// interactions
		if(actor_context.grabbed == actor){
			SET_PLAIN_COLOUR (agl->shaders.plain, 0xff00007f);
			agl_use_program (agl->shaders.plain);
#if 0
			agl_rect(ruler_grab.xy.x, 0.0, 9, 9);
#endif
			// move stem to shader? colour does not match
			agl_rect(ruler_grab.xy.x, 0, 1, 50);
		}

#ifdef AGL_ACTOR_RENDER_CACHE
		ruler->cache.start = -scrollable.x1;
#endif
		return true;
	}

	void loc_xy_to_val (ViewObject* obj, AGliPt xy, AGliPt offset)
	{
		g_return_if_fail(obj);
		Arrange* arrange = ((C*)obj->user_data)->arrange;
		AGlActor* actor = ((C*)obj->user_data)->actor;

		arr_gl_px2songpos(arrange, &am_object_val(obj->model).sp, xy.x - CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1);

		// song end must be after song start
		if(((C*)obj->user_data)->loc_num == 9){
			ayyi_pos_max(&am_object_val(obj->model).sp, &(AyyiSongPos){1,});
		}
#if 0
		char bbs[16];
		dbg(0, "x=%i x1=%.0f --> %s", xy.x, actor->region.x1, ayyi_pos2bbss(&am_object_val(obj->model).sp, bbs));
#endif
		agl_actor__invalidate(actor);
	}

	void arr_gl_ruler_init (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = arrange->canvas->gl;
		RulerShader* shader = c->wfc->shaders.ruler;

		actor->paint = agl->use_shaders ? arr_gl_ruler_draw : arr_gl_ruler_draw_gl1;

		if(agl->use_shaders){
			if(!shader->shader.program)
				agl_create_program(&shader->shader);
		}
#ifdef AGL_ACTOR_RENDER_CACHE
		actor->fbo = agl_fbo_new(agl_power_of_two(agl_actor__width(actor)), agl_actor__height(actor), 0, 0);
		actor->cache.enabled = true;
#endif
	}

	RulerActor* ruler = agl_actor__new(RulerActor,
		.actor = {
			.parent = {
				.class = &ruler_class,
				.program = (AGlShader*)CGL->wfc->shaders.ruler,
				.init = arr_gl_ruler_init,
				.set_size = arr_gl_ruler_set_size,
				.paint = agl_actor__null_painter,
				.on_event = arr_gl_ruler_on_event,
				.region = {arrange->tc_width.val.i, 0, 1024, arrange->ruler_height},
#ifdef AGL_ACTOR_RENDER_CACHE
				.cache = {
					.size_request = (AGliPt){1024, arrange->ruler_height}
				}
#endif
			},
			.pick = arr_gl_ruler_pick
		}
	);
	AGlActor* actor = (AGlActor*)ruler;

	ruler_view_class = (ViewObjectClass){
		.cursor = CURSOR_HAND_CLOSE,
		.abort = arr_gl_ruler_abort_change,
		.change_done = arr_gl_ruler_update_locator,
		.xy_to_val = loc_xy_to_val
	};

	for(int i=0;i<AM_LOC_MAX;i++){
#if 0
		view_object_init(&ruler_view_class, &actor->objs[i], ViewObject prototype);
#else
		((VOActor*)actor)->objs[i] = (ViewObject){
			.class = &ruler_view_class,
			.model = &song->loc[i],
			.hover = {
				.text = g_strdup(am_locator_name(i))
			}
		};
#endif
		*(C*)(((VOActor*)actor)->objs[i].user_data = g_new0(C, 1)) = (C){
			.actor = actor,
			.arrange = arrange,
			.loc_num = i,
		};
	}

	void ruler_on_zoom_changed (GObject* _wfc, gpointer _ruler)
	{
		AGlActor* ruler = (AGlActor*)_ruler;
#ifdef AGL_ACTOR_RENDER_CACHE
		agl_actor__invalidate(ruler);
#endif
	}
	g_signal_connect(CGL->wfc, "zoom-changed", (GCallback)ruler_on_zoom_changed, ruler);

	return actor;
}


static void
arr_gl_ruler_free (AGlActor* actor)
{
	int i; for(i=0;i<AM_LOC_MAX;i++){
		ViewObject* v = &((VOActor*)actor)->objs[i];

		if(v->hover.text) g_free0(v->hover.text);
		g_free0(v->user_data);
	}
	g_free(actor);
}


static void
arr_gl_ruler_set_size (AGlActor* actor)
{
	Arrange* arrange = actor->root->user_data;

	if(((AGlActor*)actor->root)->region.x2 > 0){
		actor->region.x1 = arrange->tc_width.val.i;
		actor->region.x2 = ((AGlActor*)actor->root)->region.x2;

#ifdef AGL_ACTOR_RENDER_CACHE
		actor->cache.size_request = (AGliPt){agl_power_of_two(agl_actor__width(actor)), agl_actor__height(actor)};
		actor->cache.valid = false;
#endif
	}
}


#define FLAG_DIRECTION(i) ((i == 2) ? GTK_DIR_LEFT : GTK_DIR_RIGHT)
#define FLAG_SIZE 10
static inline gboolean
is_over_loc (AGlActor* actor, Arrange* arrange, int loc_num, int x, int y)
{
	const double loc_x = arr_gl_pos2px_(arrange, &song->loc[loc_num].vals[0].val.sp);
	int left_px = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
	return (
		y > 2 &&
		x > 3 + loc_x - left_px - FLAG_SIZE/2 - (FLAG_DIRECTION(loc_num)==GTK_DIR_LEFT?5:0)) && (x < 3 + loc_x - left_px + FLAG_SIZE/2 - (FLAG_DIRECTION(loc_num)==GTK_DIR_LEFT?5:0)
	);
}


static void
arr_gl_ruler_on_view_change (AGlActor* actor, AMChangeType change)
{
	Arrange* arrange = actor->root->user_data;
#ifdef AGL_ACTOR_RENDER_CACHE
	RulerActor* ruler = (RulerActor*)actor;
#endif

	if(change & AM_CHANGE_VIEWPORT_Y){
		actor->region.y1 = 0;
		actor->region.y2 = arrange->ruler_height;
	}

#ifdef AGL_ACTOR_RENDER_CACHE
	if(change & AM_CHANGE_POS_X){
		iRange now = {
			.start = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1,
			.end = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + agl_actor__width(actor)
		};
		iRange valid = {
			.start = ruler->cache.start,
			.end = ruler->cache.start + actor->fbo->width
		};
		if(now.start < valid.start || now.end > valid.end){
			actor->cache.valid = false;

			// normally offset is information about the contents of the cache
			// In this case it is an instruction on where to create the new cache
			int excess = actor->fbo->width - agl_actor__width(actor);
			int start = MAX(0, now.start - excess/2);
			actor->cache.offset.x = -(now.start - start);
		}else{
			actor->cache.offset.x = ruler->cache.start + CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
		}
	}
#endif
}


static bool
arr_gl_ruler_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	Arrange* arrange = actor->root->user_data;

	if(actor__on_viewobject_event(actor, event, xy)){
		if(actor_context.grabbed == actor){
			ViewObject* locator;
			if((locator = arr_gl_ruler_pick(actor, xy))){
				dbg(1, "grabbed: loc=%i x=%i", ((C*)locator->user_data)->loc_num, xy.x);
				ruler_grab = (RulerGrab){.grabbed = locator, .xy = xy};
				agl_actor__invalidate(actor);
			}
		}

		return HANDLED;
	}

	double mouse_x = event->button.x - actor->region.x1;

	switch (event->type){
		case GDK_MOTION_NOTIFY:
			{
				shell__statusbar_print(((AyyiPanel*)arrange)->window, 2, "");
				arr_bbst2statusbar(arrange, mouse_x, NULL);
			}
			return HANDLED;
		case GDK_BUTTON_RELEASE:
			{
				AyyiSongPos pos;
				am_transport_jump(am_snap_(arr_gl_px2songpos(arrange, &pos, mouse_x)));
			}
			return HANDLED;
		default:
			break;
	}
	return NOT_HANDLED;
}


static ViewObject*
arr_gl_ruler_pick (AGlActor* actor, AGliPt xy)
{
	Arrange* arrange = actor->root->user_data;
	ViewObject* object = NULL;

	int i; for(i=0;i<AYYI_MAX_LOC;i++){
		if (am_locator_is_set(&song->loc[i]) && is_over_loc(actor, arrange, i, xy.x, xy.y)) {

			object = &((VOActor*)actor)->objs[i];
			AMObject* model_object = object->model;
			if(model_object && &model_object->vals[0]){
				object->orig_val = model_object->vals[0].val; // maybe shouldnt do this here
			}
			break;
		}
	}
	return object;
}


static void
arr_gl_ruler_update_locator (ViewObject* obj, ArrTrk* trk, AyyiSongPos* pos, AyyiHandler2 h, gpointer u)
{
	AMVal val;
	val.sp = *pos;
	obj->model->set(obj->model, 0, val, h, u);
}


static void
arr_gl_ruler_abort_change (ViewObject* obj)
{
	obj->model->vals[0].val = obj->orig_val;
}

