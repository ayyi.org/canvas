/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2004-2017 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#ifdef USE_GNOMECANVAS
void         canvas_item_grab         (GnomeCanvasItem*, GdkEvent*, int cursor);
void         pixbuf_draw_line         (GdkPixbuf*, DRect*, double line_width, GdkColor*);
void         pixbuf_draw_path_aa      (GdkPixbuf*, VPath*, double line_width, GdkColor*, uint32_t colour_bg);
void         pixbuf_fill_rgb          (GdkPixbuf*, GdkColor*);
#endif

