/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "global.h"
#include <seed/seed.h>

SeedEngine* eng;


void
ayyi_seed_init(int argc, char ** argv)
{
	PF;
	eng = seed_init(&argc, &argv);
	SeedScript * instruction_script = seed_make_script(eng->context, "print('hello')", "", 0);
	seed_evaluate(eng->context, instruction_script, NULL);
}
