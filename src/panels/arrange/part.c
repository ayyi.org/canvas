/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/


bool
arr_part_widgets_new (Arrange* arrange, AMPart* gpart)
{
	//create widgets for each Arrange window

	arrange->canvas->part_new(arrange, gpart);

	return true;
}


double
arr_part_start_px (Arrange* arrange, AMPart* part)
{
	//returns the canvas x position for the start of a part without accessing songcore.
	//This is useful for example during resizing, where the core isnt notified.

	g_return_val_if_fail(part, 0.0);

	double pixels = (double)arr_spos2px(arrange, &part->start);

	//char bbst[64]; pos2bbst(&gpart->start, bbst); printf("%s(): start=%s px=%.1f.\n", __func__, bbst, pixels);
	return pixels;
}


double
arr_part_end_px (Arrange* arrange, AMPart* part)
{
	//returns the canvas x position for the end of a part.

	double start_px = arr_part_start_px(arrange, part);
	double len_px = arr_spos2px(arrange, &part->length);
	return start_px + len_px;
}


double
am_part__max_length_px_nz (AMPart* part)
{
	//return the max allowable length for the given part, in non-zoomed pixels.

	if(!PART_IS_AUDIO(part)) return PX_PER_BEAT * AM_MAX_PART_LENGTH_BEATS;

	uint32_t source_length = ((Waveform*)part->pool_item)->n_frames;
	return samples2px_nz(source_length - part->region_start);
}


#ifdef USE_GNOMECANVAS
void
parts_redraw_by_track (Arrange* arrange, AMTrack* track)
{
  if (!CANVAS_IS_GNOME) return;

  for(GList* l=arrange->canvas->gnome->parts;l;l=l->next){
    GnomeCanvasPart* part = l->data;
    if(part->gpart->track == track){
      gnome_canvas_item_set(GNOME_CANVAS_ITEM(part), "gpart", part->gpart, NULL);
    }
  }
}
#endif


LocalPart*
part_local_new (Arrange* arrange)
{
#ifdef USE_GNOMECANVAS
	LocalPart* part = (CANVAS_IS_GNOME) ? (LocalPart*)g_new0(GCanvasLocalPart, 1) : g_new0(LocalPart, 1);
#else
	LocalPart* part = g_new0(LocalPart, 1);
#endif

	part->arrange = arrange;
	snprintf(part->part.name, 64, "local");

	return part;
}


LocalPart*
part_local_new_at_position (Arrange* arrange, AyyiSongPos* pos, ArrTrk* track)
{
	LocalPart* part = part_local_new(arrange);

	GPos gpos;
	songpos_ayyi2gui(&gpos, pos);
	am_snap(&gpos);

	part->part.track = track->track;
	songpos_gui2ayyi(&part->part.start, &gpos);

	return part;
}


/*
 *  Create a part only in the gui.
 *  -used during a part copy operation, and part Move operation.
 *
 *	-'ghost'  - means that the part is semi invisible. Used during Move operations.
 */
LocalPart*
part_local_new_from_part (Arrange* arrange, double x, double y, AMPart* template_part, gboolean ghost)
{
	LocalPart* part = part_local_new(arrange);
	AMPart* p = &part->part;

	if(template_part){
		*p = *template_part;
	} else {
		p->bg_colour = 4;
		p->length = (AyyiSongPos){4, 0, 0};
		GPos pos; songpos_ayyi2gui(&pos, &p->start);
		arr_snap_px2pos(arrange, &pos, x); //FIXME we dont want this to snap
	}
	dbg (2, "template='%s' x=%.1f len=%Lu", template_part ? template_part->name : "", x, part->part.length);

	ArrTrk* tr = arr_px2track(arrange, y);
	p->track = tr->track;
	part->ghost = ghost;

	return part;
}


void
part_local_destroy (LocalPart* part)
{
	if(!part) return;

#ifdef USE_GNOMECANVAS
	Arrange* arrange = part->arrange;
	if(CANVAS_IS_GNOME){
		GCanvasLocalPart* part_ = (GCanvasLocalPart*)part;
		if(part_->box){
			g_return_if_fail(part_->canvasgroup);
			if(!GTK_IS_OBJECT(part_->box)){ pwarn("box not GTK_IS_OBJECT"); return; }

			gtk_object_destroy(GTK_OBJECT(part_->box));
			gtk_object_destroy(GTK_OBJECT(part_->canvasgroup));
		}
	}
#endif

	g_free(part);
}


bool
arr_part_note_is_selected (Arrange* arrange, AMPart* part, MidiNote* note)
{
	GList* l = ((MidiPart*)part)->note_selection;
	for(;l;l=l->next){
		MidiNote* selected = l->data;
		if(note == selected) return TRUE;
	}
	return FALSE;
}


/*
 *  arguments:
 *    - p:        coords relative to top/left of Part.
 *    - at_start: if not NULL, its contents will be set to TRUE if mouse is over the start of the Part.
 *
 */
const MidiNote*
arr_part_pick_note (Arrange* arrange, AMPart* part, Ptd p, bool* at_start, bool* at_end)
{
	ArrTrk* trk = track_list__lookup_track(arrange->priv->tracks, part->track);
	int note_num = arr_track_get_note_num_from_y((ArrTrackMidi*)trk, p.y);
	dbg(2, "y=%.2f note=%i", p.y, note_num);

	MidiNote* note = NULL;
	while((note = am_midi_part__get_next_event(part, note))){
		if(note->note == note_num){
			//check start and end
			guint start = note->start;
			guint end = start + MAX(note->length, arr_px2samples(arrange, ARR_MIN_NOTE_DISPLAY_LENGTH));
			guint x = arr_px2samples(arrange, p.x);
			dbg(2, "%i %i %i", start, x, end);
			if(x > start && x < end){
				if(at_start){
					if(arr_px2samples(arrange, note->length) > 10){ //dont change cursor at low res.
						guint start_px = arr_samples2px(arrange, start);
						*at_start = (p.x - start_px < ARR_PICK_SIZE);
					}
				}
				if(at_end){
					if(arr_px2samples(arrange, note->length) > 10){
						guint end_px = arr_samples2px(arrange, end);
						*at_end = (end_px - p.x < ARR_PICK_SIZE);
					}
				}
				return note;
			}
		}
	}
	return NULL;
}


#ifdef USE_GNOMECANVAS
static void
arr_part_rename_start (GtkWidget* not_used, Arrange* arrange)
{
	// Called from context menu. Open the rename popup

	g_return_if_fail(arrange);

	if(arrange->part_selection){
		if(CANVAS_IS_GNOME)
			gcanvas_part_rename_start(arrange);
		else
			dbg(0, "TODO part rename not implemented for this canvas");
	}else{
		dbg (0, "no parts selected.");
	}
}
#endif


static void
delete_from_menu (GtkWidget* widget, Arrange* arrange)
{
	arr_delete(arrange);
}


static bool
menu_launch_beatdetector (GtkWidget* widget, Arrange* arrange)
{
	arrange_verify((AyyiPanel*)arrange);
	PF;

	if(arrange->part_selection){
		// just edit the first selected part
		AMPart* part = (AMPart*)arrange->part_selection->data;
		launch_beatdetector(part);
	}else{
		log_print(0, "Edit failed. No Parts selected.");
	}

	return true;
}


GtkWidget*
part_menu_new (Arrange* arrange)
{
	// Create the context menu for arrange window Parts.

	static MenuDef _menu_def[] = {
#ifdef USE_GNOMECANVAS
	    {"Rename Part",           G_CALLBACK(arr_part_rename_start),    "pencil",  true},
#endif
	    {"Delete Part",           G_CALLBACK(delete_from_menu),         "cross",   true},
	    {"Edit in Beat Detector", G_CALLBACK(menu_launch_beatdetector), "pointer", true},
	};


	return add_menu_items_from_defn(gtk_menu_new(), _menu_def, G_N_ELEMENTS(_menu_def), arrange);
}

