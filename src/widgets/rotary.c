/* Copyright (C) 2008-2009 Tim Orford. Ayyi Project. http://ayyi.org
 *
 * GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#define _XOPEN_SOURCE 700
#include <config.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gtk/gtkmarshal.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkmenu.h>

#include "rotary.h"

#define USE_CAIRO
#define DISABLE_DIRECT_CONNECTION 1 //change notification is via the "change_value" signal. The adjustment is not set directly.
#define SCROLL_DELAY_LENGTH  300
#define DIAL_DEFAULT_SIZE 30
#define DIAL_BORDER 4
#define X0 2
#define Y0 0
#define HANDLED TRUE

enum {
  CHANGE_VALUE,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };

static void gtk_knob_class_init               (GtkDialClass*);
static void gtk_knob_init                     (GtkDial*);
static void gtk_knob_destroy                  (GtkObject*);
static void gtk_knob_realize                  (GtkWidget*);
static void gtk_knob_size_request             (GtkWidget*, GtkRequisition *requisition);
static void gtk_knob_size_allocate            (GtkWidget*, GtkAllocation *allocation);
static gint gtk_knob_expose                   (GtkWidget*, GdkEventExpose *event);
static gint gtk_knob_button_press             (GtkWidget*, GdkEventButton *event);
static gint gtk_knob_button_release           (GtkWidget*, GdkEventButton *event);
static gint gtk_knob_motion_notify            (GtkWidget*, GdkEventMotion *event);
static gboolean gtk_knob_enter_notify         (GtkWidget*, GdkEventCrossing *event);
static gboolean gtk_knob_leave_notify         (GtkWidget*, GdkEventCrossing *event);
static gboolean gtk_knob_timer                (GtkDial*);

static void gtk_knob_update_mouse             (GtkDial*, gint x, gint y);
static void gtk_knob_update                   (GtkDial*);
static void gtk_knob_adjustment_value_changed (Observable*, AMVal, gpointer);
static void gtk_knob_render_text              (GtkWidget*);

static GtkWidgetClass *parent_class = NULL;

extern void colour_lighter_gdk (GdkColor*, int amount);
extern void colour_get_frame   (GdkColor*);
extern void colour_grey_out    (GdkColor*, GdkColor* bg);
extern void pixbuf_clear_alpha (GdkPixbuf*, guchar alpha);
#if 0
typedef struct
{
	float r;
	float g;
	float b;
} ColourFloat;
extern void colour_gdk_to_float(ColourFloat*, GdkColor*);
#endif

#ifdef G_ENABLE_DEBUG
#define g_marshal_value_peek_enum(v) g_value_get_enum (v)
#else
#define g_marshal_value_peek_enum(v) (v)->data[0].v_long //not part of api. may break with gtk upgrade?
#endif
extern void _gtk_marshal_BOOLEAN__ENUM_DOUBLE (GClosure     *closure, GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__ENUM_DOUBLE) (gpointer     data1,
                                                         gint         arg_1,
                                                         gdouble      arg_2,
                                                         gpointer     data2);
  register GMarshalFunc_BOOLEAN__ENUM_DOUBLE callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__ENUM_DOUBLE) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_enum (param_values + 1),
                       g_value_get_double (param_values + 2),
                       data2);

  g_value_set_boolean (return_value, v_return);
}


GType
gtk_knob_get_type ()
{
  static GType dial_type = 0;

  if (!dial_type)
    {
      static const GTypeInfo dial_info =
      {
        sizeof (GtkDialClass),
        NULL, NULL,
        (GClassInitFunc) gtk_knob_class_init,
        NULL, NULL,
        sizeof (GtkDial),
        0,
        (GInstanceInitFunc) gtk_knob_init,
      };

      dial_type = g_type_register_static (GTK_TYPE_WIDGET, "GtkDial", &dial_info, 0);
    }

  return dial_type;
}


static void
gtk_knob_class_init (GtkDialClass *class)
{
  GtkObjectClass *object_class = (GtkObjectClass*) class;
  GtkWidgetClass *widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  object_class->destroy = gtk_knob_destroy;

  widget_class->realize = gtk_knob_realize;
  widget_class->expose_event = gtk_knob_expose;
  widget_class->size_request = gtk_knob_size_request;
  widget_class->size_allocate = gtk_knob_size_allocate;
  widget_class->button_press_event = gtk_knob_button_press;
  widget_class->button_release_event = gtk_knob_button_release;
  widget_class->motion_notify_event = gtk_knob_motion_notify;

  widget_class->enter_notify_event = gtk_knob_enter_notify;
  widget_class->leave_notify_event = gtk_knob_leave_notify;

  //class->change_value = gtk_knob_change_value;

  signals[CHANGE_VALUE] =
    g_signal_new ("change_value",
		  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (GtkDialClass, change_value),
                  NULL, NULL,
                  _gtk_marshal_BOOLEAN__ENUM_DOUBLE,
                  G_TYPE_BOOLEAN, 2,
                  GTK_TYPE_SCROLL_TYPE,
                  G_TYPE_DOUBLE);
}


static void
gtk_knob_init (GtkDial *dial)
{
	GTK_WIDGET_SET_FLAGS (dial, GTK_CAN_FOCUS/* | GTK_RECEIVES_DEFAULT*/);

	dial->active = TRUE;
	dial->policy = GTK_UPDATE_CONTINUOUS;
	dial->value = NULL;
	dial->pan = NULL;
	dial->text_pixmap = NULL;
	dial->text_is_visible = TRUE;
	dial->text_position = TEXT_POSITION_NONE;
	dial->padding_top = 3;
	dial->padding_bottom = 3;
}


GtkWidget*
gtk_knob_new (Observable* value, gboolean active)
{
  GtkDial *dial = g_object_new (gtk_knob_get_type (), NULL);

  gtk_knob_set_adjustment (dial, value);

  dial->active = active;

  gtk_widget_add_events(GTK_WIDGET(dial), GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

  return GTK_WIDGET (dial);
}


static void
gtk_knob_destroy (GtkObject *object)
{
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_DIAL (object));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


#if 0
void
gtk_knob_set_update_policy (GtkDial *dial, GtkUpdateType policy)
{
  g_return_if_fail (dial != NULL);
  g_return_if_fail (GTK_IS_DIAL (dial));

  dial->policy = policy;
}
#endif


void
gtk_knob_set_adjustment (GtkDial* dial, Observable* observable)
{
  g_return_if_fail (dial);
  g_return_if_fail (GTK_IS_DIAL(dial));

  if (dial->value) {
    observable_unsubscribe (dial->value, NULL, dial);
  }

  dial->value = observable;

  observable_subscribe (observable, gtk_knob_adjustment_value_changed, dial);

  dial->old_value = observable->value.f;
  dial->old_lower = observable->min.f;
  dial->old_upper = observable->max.f;

  gtk_knob_update (dial);
}


void
gtk_knob_set_showvalue (GtkWidget *widget, gboolean show)
{
  //display of the text value next to the knob is optional.

  GtkDial *d = GTK_DIAL(widget);

  if(show) d->text_is_visible = TRUE; else d->text_is_visible = FALSE;

  gtk_knob_update(d);
}


gboolean
gtk_knob_get_showvalue (GtkWidget *widget)
{
  //returns True if the text value next to the knob is visible.

  GtkDial *d = GTK_DIAL(widget);

  if(d->text_is_visible) return TRUE; else return FALSE;
}


void
gtk_knob_set_padding (GtkWidget *widget, int top, int bottom)
{
  GtkDial *d = GTK_DIAL(widget);

  if(top > -1){
    d->padding_top = top;
  }

  if(bottom > -1){
    d->padding_bottom = bottom;
  }
}


void
gtk_knob_add_pan(GtkWidget *widget, GtkAdjustment *adj)
{
  GtkDial* dial = GTK_DIAL(widget);
  dial->pan = adj;
  gtk_widget_queue_draw (widget);
}


void
gtk_knob_remove_pan(GtkWidget *widget)
{
  GtkDial* dial = GTK_DIAL(widget);
  dial->pan = NULL;
  gtk_widget_queue_draw (widget);
}


void
gtk_knob_set_active(GtkWidget *widget, gboolean active)
{
  GtkDial* dial = GTK_DIAL(widget);
  dial->active = active;
  gtk_widget_queue_draw (widget);
}


static void
gtk_knob_realize (GtkWidget *widget)
{
  g_return_if_fail (widget);
  g_return_if_fail (GTK_IS_DIAL (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  GdkWindowAttr attributes;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) |
                          GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK |
                          GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
                          GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  gint attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);

  // change widget background color
  GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));
  GdkColor color1 = style->bg[GTK_STATE_NORMAL];
  //colour_lighter_gdk(&color1, 2);
  colour_get_frame(&color1);
  style->bg[GTK_STATE_NORMAL] = color1;
  gtk_widget_set_style (widget, style);
  g_object_unref(style);

  gdk_window_set_user_data (widget->window, widget);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  // create the text object
  gtk_knob_render_text(widget);
}


static void
gtk_knob_render_text (GtkWidget *widget)
{
  //render the knob value text to a pixmap, following widget creation or a change in value.

  //TODO using an intermediate pixmap here is not particularly efficient. We should just draw onto the window or backbuffer.
  //-pixmaps being server-side might actually be good in this case, as the fonts are server-side anyway...?

  GtkDial *dial = GTK_DIAL(widget);

  if(!dial->text_is_visible) return;
  if(!GTK_WIDGET_REALIZED(widget)) return;

  GtkStyle *style = gtk_style_copy(gtk_widget_get_style(widget));
  GdkColor color_bg = dial->in_button ? style->bg[GTK_STATE_PRELIGHT] : style->bg[GTK_STATE_NORMAL];
  GdkColor color_fg = style->fg[GTK_STATE_NORMAL];
  g_object_unref(style);

  GdkColor orig_fg = color_fg;
	if(!dial->active) colour_grey_out(&color_fg, &color_bg);

  GdkGC *gc = widget->style->fg_gc[0];

  float value = dial->value->value.f;
  char value_str[64];
  if(value > -90) snprintf(value_str, 64, "%.1f", value); else strcpy(value_str, "Off");
  PangoLayout *p_layout = gtk_widget_create_pango_layout(GTK_WIDGET(widget), value_str);

  //how big is the text?
  #define GIVE_PANGO_A_BIT_EXTRA_HEIGHT 4
  #define GIVE_PANGO_A_BIT_EXTRA_WIDTH 2
  PangoRectangle ink_rect;
  PangoRectangle logical_rect;
  pango_layout_get_pixel_extents(p_layout, &ink_rect, &logical_rect);
  int text_width = dial->text_width = ink_rect.width + GIVE_PANGO_A_BIT_EXTRA_WIDTH;
  int text_height= dial->text_height= ink_rect.height + GIVE_PANGO_A_BIT_EXTRA_HEIGHT;

  if(dial->text_pixmap) g_object_unref(dial->text_pixmap);
  GdkPixmap* pixmap = dial->text_pixmap = gdk_pixmap_new(NULL, text_width, text_height, gdk_colormap_get_system()->visual->depth);
  gdk_drawable_set_colormap(GDK_DRAWABLE(pixmap), gdk_colormap_get_system());

  // clear the pixmap
  gdk_gc_set_rgb_fg_color(gc, &color_bg);
  gdk_draw_rectangle(GDK_DRAWABLE(pixmap), gc, TRUE, 0,0, text_width, text_height);

  // render the pango layout onto the drawable
  // (warning: careful, we are modifying a gc which is shared by many windows)
  gdk_gc_set_rgb_fg_color(gc, &color_fg);
  gdk_draw_layout(GDK_DRAWABLE(pixmap), gc, X0, Y0, p_layout);
  gdk_gc_set_rgb_fg_color(gc, &orig_fg);

  g_object_unref(p_layout);
}


static void
gtk_knob_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
  //note: we dont have to override the default size_request() with this function...

  GtkDial *d = GTK_DIAL(widget);

  int extra_height = 0;
  if(d->text_position == TEXT_POSITION_TOP) extra_height = d->text_height;

  requisition->width = DIAL_DEFAULT_SIZE;
  requisition->height = DIAL_DEFAULT_SIZE + extra_height + d->padding_top + d->padding_bottom;//- DIAL_BORDER; No! the border is WITHIN the widget.
}


static void
gtk_knob_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  /*
    resizing rules:
    -if there is room we draw the text to the left of the knob, otherwise above it.
    -should we horizontally decentre the knob, to make more space for the text? dunno, not for now.
    -currently the knob square is opaque, so we dont need any trig maths to determine its edge.

  */

  //FIXME widget is being allocated too much vertical space.

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_DIAL (widget));
  g_return_if_fail (allocation != NULL);

  GtkDial *dial = GTK_DIAL(widget);

  widget->allocation = *allocation; //allocation contains x,y, width,height (all gint).

  dial->radius = MIN (allocation->width, allocation->height) * 0.5 - DIAL_BORDER/2;
  if(dial->radius > DIAL_DEFAULT_SIZE/2) dial->radius = DIAL_DEFAULT_SIZE/2 - DIAL_BORDER/2;

  if(dial->text_is_visible){
    //is there room for the text to the left?:
    int lspace = (allocation->width - dial->radius*2) / 2;
    if(lspace < dial->text_width){
      //there is no space for the text on the left, we need to put it above.
      dial->text_position = TEXT_POSITION_TOP;
      int topspace = allocation->height - dial->radius*2;
      if(topspace < dial->text_height){
        //not enough space at the top, we must reduce the size of the knob:
        dial->radius = (allocation->height - dial->text_height) / 2;
      }
    }
    else dial->text_position = TEXT_POSITION_LEFT;
  }
  else dial->text_position = TEXT_POSITION_NONE;

  if(GTK_WIDGET_REALIZED(widget)){
    //change the size of our gdkwindow:
    gdk_window_move_resize(widget->window,
               allocation->x, allocation->y, //x,y pos relative to windows parent. Normally zero.
                                             //FIXME so that does nonzero x,y mean?
               allocation->width,
               allocation->height
               );
  }

  //dial->pointer_width = dial->radius / 5;
}


static gint
gtk_knob_expose (GtkWidget *widget, GdkEventExpose *event)
{

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_DIAL (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (event->count > 0) return FALSE;

	GtkDial *dial = GTK_DIAL (widget);
	if(dial->radius <= 0) return FALSE;

	GdkGC *gc = gdk_gc_new(GDK_DRAWABLE(widget->window));

	GtkStyle *style = gtk_style_copy(gtk_widget_get_style(widget));
	GdkColor color_bg = dial->in_button ? style->bg[GTK_STATE_PRELIGHT] : style->bg[GTK_STATE_NORMAL];
	GdkColor color_fg = style->fg[GTK_STATE_NORMAL];
	if(!dial->active) colour_grey_out(&color_fg, &color_bg);
	g_object_unref(style);

	gdouble s = sin (dial->angle);
	gdouble c = cos (dial->angle);

#ifndef USE_CAIRO
	// pan pointer

	//TODO duplicate this in the cairo section

	if(dial->pan){
    gdouble s = sin (dial->pan_angle);
    gdouble c = cos (dial->pan_angle);

    vec[0].code = ART_MOVETO;
    vec[0].x = dial->radius;
    vec[0].y = dial->radius;
    vec[1].code = ART_LINETO;
    vec[1].x = dial->radius + c*dial->radius * 0.5;
    vec[1].y = dial->radius - s*dial->radius * 0.5;
    vec[2].code = ART_END;

    svp = art_svp_vpath_stroke (vec,
                              ART_PATH_STROKE_JOIN_ROUND,//ArtPathStrokeJoinType join,
	    					  ART_PATH_STROKE_CAP_BUTT,//ArtPathStrokeCapType cap,
							  2.0,//double line_width,
							  1.0, //??????? double miter_limit,
							  1.0);//double flatness

    art_rgb_svp_alpha (svp, 0, 0,
  					 bufwidth, bufwidth,//width, height,
				     a_color_fg, buffer,
					 rowstride,//number of bytes in each row.
					 NULL);
	}
#endif

	//---------------------------------------------------------------

	// copy the various parts onto the display gdkwindow

	if (dial->text_is_visible) {
		//copy the text pixmap onto the window:
		int textpos_x;
		#define TXTBORDER 4 //the border between the text rhs and the knob lhs.
		if (dial->text_position == TEXT_POSITION_LEFT) {
			textpos_x = MAX(widget->allocation.width/2 - dial->radius - dial->text_width -TXTBORDER, 0);
		} else {
			textpos_x = 0;
		}
		gdk_draw_drawable(GDK_DRAWABLE(widget->window), gc, GDK_DRAWABLE(dial->text_pixmap), 0,0,
                         textpos_x,             //x dest
                         dial->padding_top,     //y dest
                         dial->text_width, dial->text_height);
	}

	// copy the knob to the gdk window
#if 0
	int knob_top;
	if (dial->text_position != TEXT_POSITION_TOP) knob_top = dial->padding_top;
	else                                          knob_top = dial->text_height;
#endif

	cairo_t* cr = gdk_cairo_create (widget->window);

	cairo_rectangle (cr, event->area.x, event->area.y, event->area.width, event->area.height); //set clip rect
	cairo_clip (cr);

	double x = widget->allocation.width / 2;
	double y = widget->allocation.height / 2;

	cairo_arc (cr, x, y, dial->radius, 0, 2 * M_PI);

	cairo_set_line_width(cr, 4.0);
	/*
	cairo_set_source_rgb (cr, 1, 1, 1);
	cairo_fill_preserve (cr);
	*/

	cairo_set_source_rgb (cr, ((float)color_fg.red)/0x10000, ((float)color_fg.green)/0x10000, ((float)color_fg.blue)/0x10000);
	cairo_stroke (cr);

	// pointer
	{
		cairo_new_path (cr);
		cairo_set_line_width(cr, 3.0);
		cairo_move_to (cr, x, y);
		cairo_rel_line_to(cr, c*dial->radius, - s*dial->radius);
		cairo_stroke (cr);
	}

	cairo_destroy (cr);

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		gint border_width = 1;//GTK_CONTAINER (widget)->border_width;
		gint x = widget->allocation.x + border_width;
		GdkRectangle *area = &event->area;

		gtk_paint_focus (widget->style, widget->window, GTK_WIDGET_STATE (widget),
			area, widget, "button",
			x, 0, widget->allocation.width - 2, widget->allocation.height
		);
	}

	//---------------------------------------------------------------

	dial->last_angle = dial->angle;

	g_object_unref(gc);
  
	return FALSE;
}


static gint
gtk_knob_button_press (GtkWidget* widget, GdkEventButton *event)
{
	GtkDial *dial = GTK_DIAL(widget);

	if(event->button == 3 && dial->on_right_click){
		dial->on_right_click(widget, event);
		return TRUE;
	}
	if(event->button != 1) return FALSE; //propogate

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_DIAL (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if(!dial->active) return HANDLED;

	gtk_widget_grab_focus (widget);

	/* Determine if button press was within pointer region - we
	do this by computing the parallel and perpendicular distance of
	the point where the mouse was pressed from the line passing through
	the pointer */

	/*
	gint dx = event->x - widget->allocation.width / 2;
	gint dy = widget->allocation.height / 2 - event->y;

	double s = sin (dial->angle);
	double c = cos (dial->angle);

	double d_parallel = s*dy + c*dx;
	*/
	//double d_perpendicular = fabs (s*dx - c*dy);

	gtk_grab_add (widget);

	dial->is_pressed = TRUE;

	dial->button = event->button;

	gtk_knob_update_mouse (dial, event->x, event->y);

	return HANDLED;
}


static gint
gtk_knob_button_release (GtkWidget *widget, GdkEventButton *event)
{

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_DIAL (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  GtkDial* dial = GTK_DIAL (widget);

  dial->is_pressed = FALSE;

  if (dial->button == event->button){
    gtk_grab_remove (widget);

    dial->button = 0;

    if (dial->policy == GTK_UPDATE_DELAYED)	g_source_remove (dial->timer);
      
    if ((dial->policy != GTK_UPDATE_CONTINUOUS) && (dial->old_value != dial->value->value.f))
		observable_set (dial->value, dial->value->value);
  }

  return FALSE;
}


static gint
gtk_knob_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
  GdkModifierType mods;
  gint x, y, mask;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_DIAL (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  GtkDial *dial = GTK_DIAL (widget);

  if (dial->button != 0){
    x = event->x;
    y = event->y;

    if (event->is_hint || (event->window != widget->window))
				gdk_window_get_pointer (widget->window, &x, &y, &mods);

    switch (dial->button)
	{
	case 1:
	  mask = GDK_BUTTON1_MASK;
	  break;
	case 2:
	  mask = GDK_BUTTON2_MASK;
	  break;
	case 3:
	  mask = GDK_BUTTON3_MASK;
	  break;
	default:
	  mask = 0;
	  break;
	}

    if (mods & mask) gtk_knob_update_mouse (dial, x,y);
  }

  return FALSE;
}


static gboolean
gtk_knob_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  GtkDial* d = GTK_DIAL (widget);
  d->in_button = TRUE;
  gtk_widget_set_state(widget, GTK_STATE_PRELIGHT);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_PRELIGHT);

  gtk_knob_render_text (widget);
  gtk_widget_queue_draw (widget);
  return FALSE;
}


static gboolean
gtk_knob_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  GtkDial* d = GTK_DIAL(widget);
  d->in_button = FALSE;
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
  gtk_knob_render_text (widget);
  gtk_widget_queue_draw (widget);
  return FALSE;
}


static gboolean
gtk_knob_timer (GtkDial *dial)
{
  g_return_val_if_fail (dial != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_DIAL (dial), FALSE);

  if (dial->policy == GTK_UPDATE_DELAYED)
    observable_set (dial->value, dial->value->value);

  return FALSE;
}


static void
gtk_knob_update_mouse (GtkDial *dial, gint x, gint y)
{
  g_return_if_fail (dial != NULL);
  g_return_if_fail (GTK_IS_DIAL (dial));

  gint xc = GTK_WIDGET(dial)->allocation.width / 2;
  gint yc = GTK_WIDGET(dial)->allocation.height / 2;

  float old_value = dial->value->value.f;
  dial->angle = atan2(yc-y, x-xc);   //is 0 at max, increases as you go anticlockwise.
  //printf("angle=%f\n", dial->angle);

  if (dial->angle < -M_PI/2.)
    dial->angle += 2*M_PI;

  if (dial->angle < -M_PI/6)
    dial->angle = -M_PI/6;

  if (dial->angle > 7.*M_PI/6.)//changing this dont make much difference...
    dial->angle = 7.*M_PI/6.;
  //if (dial->angle > 8.*M_PI/6.)
  //  dial->angle = 8.*M_PI/6.;

  float new_value = dial->value->min.f + (7.*M_PI/6 - dial->angle) *
                    (dial->value->max.f - dial->value->min.f) / (4.*M_PI/3.);
#ifndef DISABLE_DIRECT_CONNECTION
  dial->adjustment->value = new_value;
#endif

  if (new_value != old_value) {

    gboolean handled;
    g_signal_emit(dial, signals[CHANGE_VALUE], 0, GTK_SCROLL_JUMP, new_value, &handled);

    if (dial->policy == GTK_UPDATE_CONTINUOUS) {
#ifndef DISABLE_DIRECT_CONNECTION
      observable_set (dial->value, dial->value->value);
#endif

	} else {
      gtk_widget_queue_draw (GTK_WIDGET (dial));

      if (dial->policy == GTK_UPDATE_DELAYED) {
        if (dial->timer) g_source_remove (dial->timer);

        dial->timer = g_timeout_add (SCROLL_DELAY_LENGTH, (GtkFunction) gtk_knob_timer, (gpointer) dial);
      }
	}
  }
}


static void
gtk_knob_update (GtkDial *dial)
{
  g_return_if_fail (dial != NULL);
  g_return_if_fail (GTK_IS_DIAL (dial));

  float new_value = dial->value->value.f;

#if 0
  if (new_value < dial->adjustment->lower)
    new_value = dial->adjustment->lower;

  if (new_value > dial->adjustment->upper)
    new_value = dial->adjustment->upper;

  if (new_value != dial->adjustment->value) {
#ifndef DISABLE_DIRECT_CONNECTION
      dial->adjustment->value = new_value;
      g_signal_emit_by_name (GTK_OBJECT (dial->adjustment), "value_changed");
#endif
    }
#endif

  dial->angle = 7.*M_PI/6. - (new_value - dial->value->min.f) * 4.*M_PI/3. /
                (dial->value->max.f - dial->value->min.f);

  gtk_widget_queue_draw (GTK_WIDGET (dial));
}


static void
gtk_knob_adjustment_value_changed (Observable* value, AMVal val, gpointer data)
{

  g_return_if_fail (value);
  g_return_if_fail (data);

  GtkDial *dial = GTK_DIAL (data);

  if (dial->old_value != value->value.f) {
      gtk_knob_render_text(GTK_WIDGET(data));
      gtk_knob_update(dial);

      dial->old_value = val.f;
  }
}

