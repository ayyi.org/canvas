/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_gtk_spin_button_h__
#define __ayyi_gtk_spin_button_h__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define AYYI_TYPE_GTK_SPIN_BUTTON (ayyi_gtk_spin_button_get_type ())
#define AYYI_GTK_SPIN_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_GTK_SPIN_BUTTON, AyyiGtkSpinButton))
#define AYYI_GTK_SPIN_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_GTK_SPIN_BUTTON, AyyiGtkSpinButtonClass))
#define AYYI_IS_GTK_SPIN_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_GTK_SPIN_BUTTON))
#define AYYI_IS_GTK_SPIN_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_GTK_SPIN_BUTTON))
#define AYYI_GTK_SPIN_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_GTK_SPIN_BUTTON, AyyiGtkSpinButtonClass))

typedef struct _AyyiGtkSpinButton AyyiGtkSpinButton;
typedef struct _AyyiGtkSpinButtonClass AyyiGtkSpinButtonClass;
typedef struct _AyyiGtkSpinButtonPrivate AyyiGtkSpinButtonPrivate;

struct _AyyiGtkSpinButton {
	GtkSpinButton parent_instance;
	AyyiGtkSpinButtonPrivate* priv;
};

struct _AyyiGtkSpinButtonClass {
	GtkSpinButtonClass parent_class;
};

GType              ayyi_gtk_spin_button_get_type  (void) G_GNUC_CONST;
AyyiGtkSpinButton* ayyi_gtk_spin_button_new       (Observable*);
AyyiGtkSpinButton* ayyi_gtk_spin_button_construct (GType, Observable*);

G_END_DECLS

#endif
