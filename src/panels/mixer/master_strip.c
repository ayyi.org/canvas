/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include "model/channel.h"
#include "widgets/editablelabelbutton.h"
#include "support.h"
#include "panels/mixer.h"
#include "panels/mixer/fader.h"
#include "master_strip.h"

static gpointer master_strip_parent_class = NULL;

GType master_strip_get_type();
enum  {
	MASTER_STRIP_DUMMY_PROPERTY
};

struct _MasterStripPrivate {
};
#define MASTER_STRIP_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), MASTER_STRIP, MasterStripPrivate))

extern GtkWidget* fader_bay_new              (Strip*);

static GtkWidget* _master_strip__add_widgets (AyyiPanel*, Strip*, MixerOptions*);


static void
master_strip_class_init (MasterStripClass * klass)
{
	master_strip_parent_class = g_type_class_peek_parent (klass);

	AyyiMixerStripClass* strip = AYYI_MIXER_STRIP_CLASS(klass);
	strip->add_widgets = _master_strip__add_widgets;
}


MasterStrip*
master_strip_construct (GType object_type)
{
#ifdef HAVE_GLIB_2_54
	return (MasterStrip*)g_object_new_with_properties(object_type, 0, NULL, NULL);
#else
	return (MasterStrip*)g_object_newv (object_type, 0, NULL);
#endif
}


MasterStrip*
master_strip_new (AMChannel* ch, AyyiPanel* panel)
{
	MasterStrip* self = master_strip_construct (TYPE_MASTER_STRIP);

	((Strip*)self)->channel = ch;

	return self;
}


static void
master_strip_instance_init (MasterStrip * self)
{
}


static GtkWidget*
_master_strip__add_widgets (AyyiPanel* panel, Strip* s, MixerOptions* options)
{
	g_return_val_if_fail(((AyyiSongService*)ayyi.service)->amixer, NULL);

	s->panel = panel;

	s->e = gtk_event_box_new ();
	gtk_fixed_put((GtkFixed*)s, s->e, 1/*SELECTION_BORDER*/, 0);

	GtkWidget* vbox = s->box = gtk_vbox_new(false, 0);
	gtk_container_add(GTK_CONTAINER(s->e), vbox);

	gtk_box_pack_start(GTK_BOX(vbox), (s->fdr_bay = fader_bay_new(s)), true, true, 0);

	spacer_new(GTK_BOX(s->box), 5);

	// channel label
	s->label = (GtkWidget*)editable_label_button_new("Mix");
	gtk_alignment_set((GtkAlignment*)gtk_bin_get_child(GTK_BIN(s->label)), 0.5, 0.0, 0.0, 0.0);
	gtk_box_pack_start(GTK_BOX(vbox), s->label, false, false, 0);
	if(AYYI_IS_MIXER_WIN(panel)) gtk_size_group_add_widget(((MixerWin*)panel)->strip_label_sizegroup, s->label);

#ifdef HAVE_GTK_2_10
#ifdef DEBUG
	GSList* l = gtk_size_group_get_widgets(((MixerWin*)panel)->strip_label_sizegroup);
	dbg(2, "sizegroup_size=%i", g_slist_length(l));
#endif
#endif

#if 0 // TODO
	g_signal_connect((gpointer)s->e, "button-press-event", G_CALLBACK(strip__on_clicked), s);
#endif

	return s->e;
}


gboolean
master_strip__update(Strip* s)
{
	g_return_val_if_fail(s, false);
	if(!s->channel) return false;
	g_return_val_if_fail((s->channel->type == AM_CHAN_MASTER), false);

	fader__set_value(s->fdr, am_channel__fader_level(s->channel));

	// TODO
	//strip_label_update(strip);

	return true;
}


GType
master_strip_get_type (void)
{
	static GType master_strip_type_id = 0;
	if (master_strip_type_id == 0) {
		static const GTypeInfo g_define_type_info = { sizeof (MasterStripClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) master_strip_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (MasterStrip), 0, (GInstanceInitFunc) master_strip_instance_init, NULL };
		master_strip_type_id = g_type_register_static (AYYI_TYPE_MIXER_STRIP, "MasterStrip", &g_define_type_info, 0);
	}
	return master_strip_type_id;
}




