/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShownotesClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		return (int)(app = new ShownotesClient()).run(((ShownotesClient*)app)->on_shm);
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		stdout.printf("-----------------------------------------------------------------\n");
		stdout.printf("midi parts:\n");
		if((bool)ayyi_song__midi_region_next(null)){
			stdout.printf("  %3s     len   flags          name\n", "idx");
			AyyiMidiRegion* midi_part = null;
			while((bool)(midi_part = ayyi_song__midi_region_next(midi_part))){
				stdout.printf("  %3i %7u %3i %20s\n", midi_part->shm_idx, midi_part->length, midi_part->flags, (string)midi_part->name);
				AM.Part* part = AM.Song.get_part_by_id(midi_part->id);
				AM.midi_part__print_events(part);
			}
		}
		else stdout.printf("\tno midi parts\n");

		stdout.printf("-----------------------------------------------------------------\n");

		app.quit();
	}
}


