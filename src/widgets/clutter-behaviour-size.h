/*
 * Clutter.
 *
 * An OpenGL based 'interactive canvas' library.
 *
 * Authored By Matthew Allum  <mallum@openedhand.com>
 *             Jorn Baayen  <jorn@openedhand.com>
 *             Emmanuele Bassi  <ebassi@openedhand.com>
 *
 * Copyright (C) 2006 OpenedHand
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __clutter_behaviour_size_h__
#define __clutter_behaviour_size_h__

//#include <clutter/clutter-behaviour.h>

G_BEGIN_DECLS

#define CLUTTER_TYPE_BEHAVIOUR_SIZE            (clutter_behaviour_size_get_type ())
#define CLUTTER_BEHAVIOUR_SIZE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CLUTTER_TYPE_BEHAVIOUR_SIZE, ClutterBehaviourSize))
#define CLUTTER_BEHAVIOUR_SIZE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CLUTTER_TYPE_BEHAVIOUR_SIZE, ClutterBehaviourSizeClass))
#define CLUTTER_IS_BEHAVIOUR_SIZE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CLUTTER_TYPE_BEHAVIOUR_SIZE))
#define CLUTTER_IS_BEHAVIOUR_SIZE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CLUTTER_TYPE_BEHAVIOUR_SIZE))
#define CLUTTER_BEHAVIOUR_SIZE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), CLUTTER_TYPE_BEHAVIOUR_SIZE, ClutterBehaviourSizeClass))

typedef struct _ClutterBehaviourSize           ClutterBehaviourSize;
typedef struct _ClutterBehaviourSizePrivate    ClutterBehaviourSizePrivate;
typedef struct _ClutterBehaviourSizeClass      ClutterBehaviourSizeClass;

/**
 * ClutterBehaviourSize:
 *
 * The #ClutterBehaviourSize struct contains only private data and
 * should be accessed using the provided API
 *
 * Since: 0.2
 */
struct _ClutterBehaviourSize
{
  /*< private >*/
  ClutterBehaviour parent_instance;

  ClutterBehaviourSizePrivate *priv;
};

/**
 * ClutterBehaviourSizeClass:
 *
 * The #ClutterBehaviourSizeClass struct contains only private data
 *
 */
struct _ClutterBehaviourSizeClass
{
  /*< private >*/
  ClutterBehaviourClass parent_class;
};

GType clutter_behaviour_size_get_type (void) G_GNUC_CONST;

ClutterBehaviour* clutter_behaviour_size_new        (ClutterAlpha*,         gdouble , gdouble , gdouble , gdouble );
void              clutter_behaviour_size_set_bounds (ClutterBehaviourSize*, gdouble , gdouble , gdouble , gdouble );
void              clutter_behaviour_size_get_bounds (ClutterBehaviourSize*, gdouble*, gdouble*, gdouble*, gdouble*);

G_END_DECLS

#endif /* __clutter_behaviour_size_h__ */
