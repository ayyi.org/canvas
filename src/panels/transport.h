/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "model/transport.h"
#include "src/transport.h"
#include "widgets/glpanel.h"

G_BEGIN_DECLS

#define AYYI_TYPE_TRANSPORT_WIN            (ayyi_transport_win_get_type ())
#define AYYI_TRANSPORT_WIN(obj)            (G_TYPE_CHECK_CAST ((obj), AYYI_TYPE_TRANSPORT_WIN, AyyiTransportWin))
#define AYYI_TRANSPORT_WIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_TRANSPORT_WIN, AyyiTransportWinClass))
#define AYYI_IS_TRANSPORT_WIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_TRANSPORT_WIN))
#define AYYI_IS_TRANSPORT_WIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_TRANSPORT_WIN))
#define AYYI_TRANSPORT_WIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_TRANSPORT_WIN, AyyiTransportWinClass))

typedef struct _AyyiTransportWinClass AyyiTransportWinClass;

#define TR_SIZE 6 // the number of transport buttons.

struct _AyyiTransportWinClass {
	GlPanelClass parent_class;
};


struct _AyyiTransportWin
{
	GlPanel        panel;

	GtkWidget*     hbox;
	GtkWidget*     button_box;
	GtkWidget*     b[TR_SIZE]; // buttons

	AGlActor*      spp;
#if 0
	GtkWidget*     spp_entry;  // single line version of spp_view
	GtkWidget*     textfx;     // spp display with image processing.
#endif
	GtkWidget*     locators;
	GtkTextBuffer* loc[5];

#if 0
	GtkAllocation  old_allocation;
	bool           button_resize_pending; //panel elements are being resized which will require a button resize.
#endif
};


GType      ayyi_transport_win_get_type ();
void       transport_clear_buttons     ();

#define transport_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_TRANSPORT_WIN) continue; \
		AyyiTransportWin* transport = (AyyiTransportWin*)panel;
#define end_transport_foreach end_panel_foreach

G_END_DECLS
