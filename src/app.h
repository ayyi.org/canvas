/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <glib-object.h>
#ifdef USE_OPENGL
#include <gtk/gtkgl.h>
#else
#define GdkGLConfig void
#endif
#include "gui_types.h"

G_BEGIN_DECLS

#define AYYI_TYPE_APP            (ayyi_app_get_type ())
#define AYYI_APP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_APP, AyyiApp))
#define AYYI_APP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_APP, AyyiAppClass))
#define AYYI_IS_APP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_APP))
#define AYYI_IS_APP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_APP))
#define AYYI_APP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_APP, AyyiAppClass))

typedef struct _AyyiAppPrivate AyyiAppPrivate;

struct _AyyiApp {
    GObject         parent_instance;
    AyyiAppPrivate* priv;
    AppState        state;
    gint            font_size;
    gchar           font_family[64];
    gint            line_height;

	#define N_CONFIG_DIRS 3
    const char*     dirs[N_CONFIG_DIRS];
    char            config_filename[256];

    char            recent_songs[MAX_RECENT_SONGS][256];

    GHashTable*     panel_classes;

    GtkAccelGroup*  global_accel_group;

    GdkCursor*      cursors[CURSOR_MAX];
    CursorTimer     cursor_timer;

    gboolean        styles_are_set;
    ImageCache*     image_cache;          // contains pre-rendered text. Currently just digits at one fontsize for transport window.

    AyyiPanel*      active_panel;
    Arrange*        latest_arrange;

    struct AppDnd {
        GtkTargetEntry file_drag_types[2];
        gint           file_drag_types_count;
    }               dnd;

    bool            disable_help_focus;
};

struct _AyyiAppClass {
    GObjectClass    parent_class;
};


GType    ayyi_app_get_type  (void) G_GNUC_CONST;
AyyiApp* app_new            (void);
AyyiApp* ayyi_app_construct (GType);
void     app_set_state      (AppState state);
void     app_quit           ();

#ifdef USE_OPENGL
GdkGLConfig* app_get_glconfig ();
#endif

bool     app_config_dir_foreach (bool (*fn)(const char*, void*), void*);

G_END_DECLS
