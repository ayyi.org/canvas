/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __ayyi_mixer_strip_h__
#define __ayyi_mixer_strip_h__

#define MIXER_HBOX_SELECTION_BORDER 2
#define INSERT_BOX_LIST_OFFSET 3      // the first 3 items are commands, not plugins

#include <glib.h>

G_BEGIN_DECLS

#define AYYI_TYPE_MIXER_STRIP            (ayyi_mixer_strip_get_type ())
#define AYYI_MIXER_STRIP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_MIXER_STRIP, AyyiMixerStrip))
#define AYYI_MIXER_STRIP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_MIXER_STRIP, AyyiMixerStripClass))
#define AYYI_IS_MIXER_STRIP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_MIXER_STRIP))
#define AYYI_IS_MIXER_STRIP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_MIXER_STRIP))
#define AYYI_MIXER_STRIP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_MIXER_STRIP, AyyiMixerStripClass))

typedef struct _AyyiMixerStripClass AyyiMixerStripClass;
typedef struct _AyyiMixerStripPrivate AyyiMixerStripPrivate;

struct _AyyiMixerStrip {
	GtkFixed          parent_instance;
	AyyiMixerStripPrivate* priv;

 	AMChannel*        channel;
	AyyiPanel*        panel;

	GString*          cha_name;       // local copy of the core channel name for this strip. So far pretty useless.

	int               strip_num : 8;  // index to mixer->strips. Not the shm index nor the channel index.

	bool              use_rotaries_for_auxs : 1;
	bool              show_aux_values : 1;

	int               colour;         // palette number, or -1 if not set.

	GPtrArray*        inserts;

	GtkWidget*        e;              // event box covering the whole channel.
	GtkWidget*        box;            // the ch strip vbox
	GtkWidget*        insel;          // input selector combo box widget.
	GtkWidget*        outsel;         // output selector combo box widget.
	GtkWidget*        wsend[MAX_SENDS];
	GtkWidget*        fdr_bay;        // hbox holding all widgets next to and including the fader.
	GtkWidget*        fdr;            // the fader scale widget.
	GtkObject*        pan_adj;        // the pan adjustment.
	GtkWidget*        pan_fdr;        // the pan scale widget.
	GtkWidget*        label;          // the main channel label.
#if 0
    GtkWidget*        src_label;      //label widget for the channel input src.
                                      //note: this is currently NOT bing used. ch->insel is instead.
                                      //note: the actual label text is not part of the channel but is a
                                      //part of the Input array. Eg in1 is not necc connected to ch1.
#endif

    struct {
        GtkWidget*    widget [2];
    }                 meter;

    double            hmag;           //relative horizontal size (initially 1.0)
};

struct _AyyiMixerStripClass {
	GtkFixedClass parent_class;

	GtkWidget*    (*add_widgets)  (AyyiPanel*, Strip*, MixerOptions*);
	gboolean      (*on_clicked)   (Strip*, GdkEventButton*, gpointer);
};


GType           ayyi_mixer_strip_get_type  ();
AyyiMixerStrip* ayyi_mixer_strip_new       ();
AyyiMixerStrip* ayyi_mixer_strip_construct (GType);

#define STRIP_IS_MASTER (strip->channel->type == AM_CHAN_MASTER)

Strip*      strip__new                    (AMChannel*);
void        strip__destroy                (AyyiMixerStrip*);
void        strip__set_channel            (AyyiMixerStrip*, const AMChannel*);
bool        strip__update                 (AyyiMixerStrip*);
void        strip__update_insert_size     (AyyiMixerStrip*);

void        strip__show                   (AyyiMixerStrip*);
void        strip__hide                   (AyyiMixerStrip*);
bool        strip__is_hidden              (AyyiMixerStrip*);
bool        strip__is_selected            (AyyiMixerStrip*);
void        strip__set_selected           (AyyiMixerStrip*);
void        strip__unselect               (AyyiMixerStrip*);
void        strip__set_pan                (AyyiMixerStrip*, double val);
void        strip__set_mute               (AyyiMixerStrip*, bool);
void        strip__set_solo               (AyyiMixerStrip*, bool);
void        strip__set_armed              (AyyiMixerStrip*, bool);

GtkWidget*  strip__add_widgets            (AyyiPanel*, AyyiMixerStrip*, MixerOptions*);

char*       strip__get_ch_name            (AyyiMixerStrip*);
void        strip__set_colour             (AyyiMixerStrip*, int);
int         strip__get_colour             (AyyiMixerStrip*);

void        strip__output_selector_update (AyyiMixerStrip*);
void        strip__input_selector_update  (AyyiMixerStrip*);

void        strip__show_sends             (AyyiMixerStrip*, bool show);
bool        strip__sends_visible          (AyyiMixerStrip*);
void        strip__set_label              (AyyiMixerStrip*, char* text);
void        strip__label_update           (AyyiMixerStrip*);

void        strip__auxs_update            (AyyiMixerStrip*);

void        strip__update_pluginbox       (AyyiMixerStrip*, int slot);

void        strip__show_insert            (AyyiMixerStrip*, int, bool);
G_END_DECLS

#endif
