/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include "global.h"
#include <jack/ringbuffer.h>
#include "lv2/lv2plug.in/ns/ext/atom/atom.h"
#include "lv2/lv2plug.in/ns/ext/atom/forge.h"
#include "lv2/lv2plug.in/ns/ext/event/event.h"
#include "lv2/lv2plug.in/ns/ext/presets/presets.h"
#include "lv2/lv2plug.in/ns/ext/time/time.h"
#include "lv2/lv2plug.in/ns/ext/uri-map/uri-map.h"
#include "lv2/lv2plug.in/ns/ext/urid/urid.h"
#include "lv2/lv2plug.in/ns/ext/worker/worker.h"
#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"
#include "lv2/lv2plug.in/ns/ext/log/log.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/ext/state/state.h"
#include "lilv/lilv.h"
#include "suil/suil.h"
#include "sratom/sratom.h"
#include "symap.h"

#define NS_EXT  "http://lv2plug.in/ns/ext/"

static struct __lv2
{
	LilvWorld* world;
} _lv2;

typedef struct {
	LV2_URID atom_eventTransfer;
	LV2_URID log_Trace;
	LV2_URID midi_MidiEvent;
	LV2_URID time_Position;
	LV2_URID time_bar;
	LV2_URID time_barBeat;
	LV2_URID time_beatUnit;
	LV2_URID time_beatsPerBar;
	LV2_URID time_beatsPerMinute;
	LV2_URID time_frame;
	LV2_URID time_speed;
} URIDs;

struct Lv2Host {
	URIDs              urids;          ///< URIDs
	LV2_URID_Map       map;            ///< URI => Int map
	LV2_URID_Unmap     unmap;          ///< Int => URI map
	LV2_Atom_Forge     forge;          ///< Atom forge
	Sratom*            sratom;         ///< Atom serialiser
	const LilvPlugin*  plugin;         ///< Plugin class (RDF data)
	LilvUIs*           uis;            ///< All plugin UIs (RDF data)
	const LilvUI*      ui;             ///< Plugin UI (RDF data)
	SuilInstance*      ui_instance;    ///< Plugin UI instance (shared library)
	SuilHost*          ui_host;        ///< Plugin UI host support
	const LilvNode*    ui_type;        ///< Plugin UI type (unwrapped)
	LilvInstance*      instance;       ///< Plugin instance (shared library)
	Symap*             symap;          ///< Symbol (URI) map
	uint32_t           midi_event_id;  ///< MIDI event class ID in event context
	bool               has_ui;         ///< True iff a control UI is present
	uint32_t           num_ports;      ///< Size of the two following arrays:
	jack_ringbuffer_t* ui_events;     /**< Port events from UI */
	jack_ringbuffer_t* plugin_events; /**< Port events from plugin */
};

struct Lv2Host host;

// Map function for URI map extension.
uint32_t
uri_to_id(LV2_URI_Map_Callback_Data callback_data, const char* map, const char* uri)
{
	return symap_map(((struct Lv2Host*)callback_data)->symap, uri);
}

LV2_URID
map_uri(LV2_URID_Map_Handle handle, const char* uri)
{
	return symap_map(((struct Lv2Host*)handle)->symap, uri);
}

const char*
unmap_uri(LV2_URID_Unmap_Handle handle, LV2_URID urid)
{
	return symap_unmap(((struct Lv2Host*)handle)->symap, urid);
}


static LV2_URI_Map_Feature uri_map = { NULL, &uri_to_id };
static LV2_Feature uri_map_feature = { NS_EXT "uri-map", &uri_map };
static LV2_Feature map_feature       = { LV2_URID__map, NULL };
static LV2_Feature unmap_feature     = { LV2_URID__unmap, NULL };
static LV2_Feature make_path_feature = { LV2_STATE__makePath, NULL };
static LV2_Feature schedule_feature  = { LV2_WORKER__schedule, NULL };
static LV2_Feature log_feature       = { LV2_LOG__log, NULL };

const LV2_Feature* features[] = {
	&uri_map_feature, &map_feature, &unmap_feature,
	&make_path_feature,
	&schedule_feature,
	&log_feature,
	NULL
};

static GList* lilv_world_get_plugins_by_filter (LilvWorld*, bool (*fn)(LilvPlugin*, gpointer), gpointer);
static void   lv2_ui_instantiate               (const char* native_ui_type, void* parent);

#define _LILV_FOREACH(colltype, iter, collection) \
	LilvIter* (iter); \
	for ((iter) = lilv_ ## colltype ## _begin(collection); \
	     !lilv_ ## colltype ## _is_end(collection, iter); \
	     (iter) = lilv_ ## colltype ## _next(collection, iter))

#if 0
#define LILV_FOREACH_(colltype, iter, collection) \
	for (iter = lilv_ ## colltype ## _begin(collection); \
	     !lilv_ ## colltype ## _is_end(collection, iter); \
	     (iter) = lilv_ ## colltype ## _next(collection, iter))
#endif

#if 0
static void
list_plugins(const LilvPlugins* plugins, bool show_names)
{
	printf("lv2 plugins:\n");
	_LILV_FOREACH(plugins, i, plugins) {
		const LilvPlugin* p = lilv_plugins_get(plugins, i);
		if (show_names) {
			LilvNode* n = lilv_plugin_get_name(p);
			printf("  %s\n", lilv_node_as_string(n));
			lilv_node_free(n);
		} else {
			printf("  %s\n", lilv_node_as_uri(lilv_plugin_get_uri(p)));
		}
	}
}
#endif


void
lv2_init()
{
	memset(&host, 0, sizeof(struct Lv2Host));

	_lv2.world = lilv_world_new();
	lilv_world_load_all(_lv2.world);

	host.symap = symap_new();
	uri_map.callback_data = &host;

	host.map.handle  = &host;
	host.map.map     = map_uri;
	map_feature.data = &host.map;

	host.unmap.handle  = &host;
	host.unmap.unmap   = unmap_uri;
	unmap_feature.data = &host.unmap;

	lv2_atom_forge_init(&host.forge, &host.map);

	host.sratom = sratom_new(&host.map);

	host.midi_event_id = uri_to_id(&host, "http://lv2plug.in/ns/ext/event", LV2_MIDI__MidiEvent);

	host.urids.atom_eventTransfer  = symap_map(host.symap, LV2_ATOM__eventTransfer);
	host.urids.log_Trace           = symap_map(host.symap, LV2_LOG__Trace);
	host.urids.midi_MidiEvent      = symap_map(host.symap, LV2_MIDI__MidiEvent);
	host.urids.time_Position       = symap_map(host.symap, LV2_TIME__Position);
	host.urids.time_bar            = symap_map(host.symap, LV2_TIME__bar);
	host.urids.time_barBeat        = symap_map(host.symap, LV2_TIME__barBeat);
	host.urids.time_beatUnit       = symap_map(host.symap, LV2_TIME__beatUnit);
	host.urids.time_beatsPerBar    = symap_map(host.symap, LV2_TIME__beatsPerBar);
	host.urids.time_beatsPerMinute = symap_map(host.symap, LV2_TIME__beatsPerMinute);
	host.urids.time_frame          = symap_map(host.symap, LV2_TIME__frame);
	host.urids.time_speed          = symap_map(host.symap, LV2_TIME__speed);

#if 0
	const LilvPlugins* plugins = lilv_world_get_all_plugins(_lv2.world);
	list_plugins(plugins, false);
#endif
}


void
lv2_destroy()
{
	lilv_world_free(_lv2.world);
}


GtkWidget*
lv2_instantiate(const char* plugin_uri_str)
{
	const LilvPlugins* plugins = lilv_world_get_all_plugins(_lv2.world);

	//const char* plugin_uri_str = "http://linuxsampler.org/plugins/linuxsampler";
	//const char* plugin_uri_str = "http://calf.sourceforge.net/plugins/Compressor";
	LilvNode* plugin_uri = lilv_new_uri(_lv2.world, plugin_uri_str);
	host.plugin = lilv_plugins_get_by_uri(plugins, plugin_uri);
	lilv_node_free(plugin_uri);

	if (!host.plugin) {
		fprintf(stderr, "Failed to find plugin %s.\n", plugin_uri_str);
		return NULL;
	}

#ifdef DEBUG
	LilvNode*   name     = lilv_plugin_get_name(host.plugin);
	const char* name_str = lilv_node_as_string(name);
	dbg(0, "%s loaded ok", name_str);
#endif

	LilvNode* ui_type_uri = lilv_new_uri(_lv2.world, "http://lv2plug.in/ns/extensions/ui#GtkUI");
	LilvUIs* uis = lilv_plugin_get_uis(host.plugin); // FIXME: leak
	const LilvNode* ui_type = NULL;
		_LILV_FOREACH(uis, u, uis) {
			const LilvUI* this_ui = lilv_uis_get(uis, u);
			if (lilv_ui_is_supported(this_ui,
			                         suil_ui_supported,
			                         //native_ui_type,
			                         ui_type_uri,
			                         &ui_type)) {
				// TODO: Multiple UI support
				host.ui = this_ui;
				break;
			}
		}

	if(host.ui){
#if 0
		const LilvNodes* ui_types = lilv_ui_get_classes(host.ui);
		int n_types = lilv_values_size(ui_types);
		dbg(0, "n_gui_types=%i", n_types);
		int i;
		for(i=0;i<n_types;i++){
			LilvNode widget_type_uri = lilv_values_get_at(ui_types, i);
			/*
			if(lilv_value_is_string(v)){
				const char* str = lilv_value_as_string(v);
				dbg(0, "val=%s", str);
			}else{
				dbg(0, "not string");
			}
			*/
			if(!lilv_value_is_uri(widget_type_uri)){
				dbg(0, "not uri");
				continue;
			}

			const char* str = lilv_value_as_uri(widget_type_uri);
			dbg(0, "val=%s", str);

			//old method:
			/*
			lilv_ui_instantiate(LilvPlugin                plugin,
							lilvUI                    ui,
							LV2UI_Write_Function      write_function,
							LV2UI_Controller          controller,
							const LV2_Feature* const* features);
			*/

			//new method:
			LV2UI_Write_Function write_function = NULL;
			LV2UI_Controller controller = NULL;
			const LV2_Feature* const* features = NULL;
			lilvUIInstance ui_instance = slv2_ui_instance_new(host.plugin, ui, widget_type_uri, write_function, controller, features);

			LV2UI_Widget widget = lilv_ui_instance_get_widget(ui_instance);

			return (GtkWidget*)widget;
		}
#endif
		fprintf(stderr, "UI:        %s\n", lilv_node_as_uri(lilv_ui_get_uri(host.ui)));

		host.ui_events     = jack_ringbuffer_create(4096);
		host.plugin_events = jack_ringbuffer_create(4096);
		jack_ringbuffer_mlock(host.ui_events);
		jack_ringbuffer_mlock(host.plugin_events);
	}else{
		dbg(0, "plugin has no gui?");
	}
	return NULL;
}


void
jalv_ui_write(SuilController controller, uint32_t port_index, uint32_t buffer_size, uint32_t protocol, const void* buffer)
{
	struct Lv2Host* const lv2host = (struct Lv2Host*)controller;
	if (!lv2host->has_ui) {
		return;
	}

	if (protocol != 0 && protocol != lv2host->urids.atom_eventTransfer) {
		fprintf(stderr, "UI write with unsupported protocol %d (%s)\n", protocol, symap_unmap(lv2host->symap, protocol));
		return;
	}

	if (port_index >= lv2host->num_ports) {
		fprintf(stderr, "UI write to out of range port index %d\n", port_index);
		return;
	}

	/*
	if (lv2host->opts.dump && protocol == lv2host->urids.atom_eventTransfer) {
		SerdNode s = serd_node_from_string(SERD_BLANK, USTR("msg"));
		SerdNode p = serd_node_from_string(SERD_URI, USTR(NS_RDF "value"));

		const LV2_Atom* atom = (const LV2_Atom*)buffer;
		char*           str  = sratom_to_turtle(lv2host->sratom, &lv2host->unmap, "jalv:", &s, &p, atom->type, atom->size, LV2_ATOM_BODY(atom));
		printf("\n## UI => Plugin (%u bytes) ##\n%s\n", atom->size, str);
		free(str);
	}
	*/
}


/*
 * This is for plugins that are not in the LV2_PATH ?
 */
GtkWidget*
lv2_instantiate_by_path(const char* _bundle_uri)
{
	GtkWidget* widget = NULL;

	//_bundle_uri = "file:///usr/lib/lv2/calf.lv2/";
	dbg(0, "%s", _bundle_uri);

#if 0
	if(!g_setenv("LV2_PATH", bundle_path, true)){
		pwarn("failed to set LV path");
	}
#endif
	//const LilvPlugins* plugins = lilv_world_get_all_plugins(_lv2.world);

	LilvNode* bundle_uri = lilv_new_uri(_lv2.world, _bundle_uri);

	//LilvNodes* nodes = lilv_world_find_nodes(_lv2.world, const LilvNode* subject, const LilvNode* predicate, const LilvNode* object);

	bool filter_by_path(LilvPlugin* plugin, gpointer _test_path)
	{
		//printf("  %s\n", lilv_node_as_uri(lilv_plugin_get_uri(plugin)));
		//const LilvNode* bu = lilv_plugin_get_bundle_uri(plugin);
		const char* bundle_path = lilv_uri_to_path(lilv_node_as_uri(lilv_plugin_get_bundle_uri(plugin)));
		if(!strcmp((char*)_test_path, bundle_path)){
			return true;
		}
		return false;
	}
	GList* pl = lilv_world_get_plugins_by_filter(_lv2.world, filter_by_path, (gpointer)lilv_uri_to_path(_bundle_uri));
	if(!pl){
		dbg(1, "plugin not in LV2_PATH - loading manually...");
		lilv_world_load_bundle(_lv2.world, bundle_uri);
		pl = lilv_world_get_plugins_by_filter(_lv2.world, filter_by_path, (gpointer)lilv_uri_to_path(_bundle_uri));
	}
	if(pl){
		host.plugin = pl->data;
	}else{
		fprintf(stderr, "Failed to find plugin %s.\n", _bundle_uri);
		return NULL;
	}

	LilvNode*   name     = lilv_plugin_get_name(host.plugin);
#ifdef DEBUG
	const char* name_str = lilv_node_as_string(name);
	dbg(0, "%s loaded ok", name_str);
#endif

	lilv_node_free(name);
	lilv_node_free(bundle_uri);

									//set ui types
									{
									LilvNode* ui_type_uri = lilv_new_uri(_lv2.world, "http://lv2plug.in/ns/extensions/ui#GtkUI");
									LilvUIs* uis = lilv_plugin_get_uis(host.plugin); // FIXME: leak
									//const LilvNode* ui_type = NULL;
									//LilvIter* u = NULL;
									_LILV_FOREACH(uis, u, uis) {
										const LilvUI* this_ui = lilv_uis_get(uis, u);
										dbg(1, "found ui type");
										if (lilv_ui_is_supported(this_ui,
																 suil_ui_supported,
																 //native_ui_type,
																 ui_type_uri,
																 &host.ui_type)) {
											// TODO: Multiple UI support
											host.ui = this_ui;
											break;
										}
										else dbg(0, "    not supported");
									}
									//host.ui_type = ui_type;
									}
	if(!host.ui){
		pwarn("no supported ui types");
		return NULL;
	}
	dbg(1,  "host.ui=%p", host.ui_type);

	LilvUIs* uis = lilv_plugin_get_uis(host.plugin);
	if(lilv_nodes_size(uis) > 0){
		printf("  UIs:\n");
		_LILV_FOREACH(uis, i, uis){
			const LilvUI* ui = lilv_uis_get(uis, i);
			host.ui = ui;
			printf("    %s\n", lilv_node_as_uri(lilv_ui_get_uri(ui)));

			const char* binary = lilv_node_as_uri(lilv_ui_get_binary_uri(ui));

			const LilvNodes* types = lilv_ui_get_classes(ui);
			_LILV_FOREACH(nodes, i, types){
				printf("      class:  %s\n", lilv_node_as_uri(lilv_nodes_get(types, i)));
			}
			if(binary) printf("      binary: %s\n", binary);
			printf("      bundle: %s\n", lilv_node_as_uri(lilv_ui_get_bundle_uri(ui)));

			/* Instantiate the plugin */
			host.instance = lilv_plugin_instantiate(host.plugin, song->sample_rate, features);
			if (host.instance) {
				void* parent = NULL; //parent_widget
				const char* native_ui_type = NULL;
				lv2_ui_instantiate(native_ui_type, parent);

				if(host.ui_instance){
					widget = (GtkWidget*)suil_instance_get_widget(host.ui_instance);
				}
			}else{
				pwarn("Failed to instantiate plugin.");
			}
		}
	}
	else dbg(0, "plugin has no ui");
	lilv_uis_free(uis);

	return widget;
}


static void
lv2_ui_instantiate(const char* native_ui_type, void* parent)
{
	host.ui_host = suil_host_new(jalv_ui_write, NULL, NULL, NULL);
	const LV2_Feature parent_feature = {
		LV2_UI__parent, parent
	};
	const LV2_Feature instance_feature = {
		NS_EXT "instance-access", lilv_instance_get_handle(host.instance)
	};
	const LV2_Feature* ui_features[] = {
		&uri_map_feature, &map_feature, &unmap_feature,
		&instance_feature,
		&log_feature,
		&parent_feature,
		NULL
	};

	dbg(0, "%s", lilv_node_as_uri(lilv_plugin_get_uri(host.plugin)));
	dbg(0, "%s", lilv_node_as_uri(lilv_ui_get_uri(host.ui)));
	dbg(1, "%s", lilv_node_as_uri(host.ui_type));
	dbg(1, "%s", lilv_node_as_uri(lilv_ui_get_bundle_uri(host.ui)));
	dbg(1, "%s", lilv_node_as_uri(lilv_ui_get_binary_uri(host.ui)));
	host.ui_instance = suil_instance_new(
		host.ui_host,
		&host,
		"http://lv2plug.in/ns/extensions/ui#GtkUI",
		lilv_node_as_uri(lilv_plugin_get_uri(host.plugin)),
		lilv_node_as_uri(lilv_ui_get_uri(host.ui)),
		lilv_node_as_uri(host.ui_type),
		lilv_uri_to_path(lilv_node_as_uri(lilv_ui_get_bundle_uri(host.ui))),
		lilv_uri_to_path(lilv_node_as_uri(lilv_ui_get_binary_uri(host.ui))),
		ui_features);
}


void
lv2_uninstantiate(const char* uri)
{
	//lilv_ui_instance_free(ui_instance);
}


static GList*
lilv_world_get_plugins_by_filter(LilvWorld* world, bool (*fn)(LilvPlugin*, gpointer), gpointer user_data)
{
	const LilvPlugins* plugins = lilv_world_get_all_plugins(world);
	GList* result = NULL;
	_LILV_FOREACH(plugins, i, plugins) {
		LilvPlugin* p = (LilvPlugin*)lilv_plugins_get(plugins, i);
		if(fn(p, user_data)) result = g_list_append(result, p);
	}
	if(!result) pwarn("not found: %s", (char*)user_data);

	return result;
}

