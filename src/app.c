/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include <glib.h>
#include <glib-object.h>
#include "support.h"
#include "settings.h"
#include "song.h"
#include "transport.h"
#include "panels/plugin.h"
#include "panels/arrange.h"
#include "app.h"

AyyiApp* app = NULL;
SMConfig* config;

typedef struct _AyyiApp AyyiApp;
typedef struct _AyyiAppClass AyyiAppClass;

struct _AyyiAppPrivate
{
    GdkGLConfig*    glconfig;
};

G_DEFINE_TYPE_WITH_PRIVATE (AyyiApp, ayyi_app, G_TYPE_OBJECT)

enum {
	AYYI_APP_DUMMY_PROPERTY
};

static GObject* ayyi_app_constructor (GType, guint n_construct_properties, GObjectConstructParam*);
static void     ayyi_app_finalize    (GObject*);


AyyiApp*
ayyi_app_construct (GType object_type)
{
	return (AyyiApp*) g_object_new (object_type, NULL);
}


AyyiApp*
app_new (void)
{
	app = ayyi_app_construct (AYYI_TYPE_APP);

	app->dnd = (struct AppDnd){
		.file_drag_types_count = 2,
		.file_drag_types = {
			{ "text/uri-list", 0, AYYI_TARGET_URI_LIST },
			{ "text/plain",    0, TARGET_TEXT_PLAIN }
		}
	};

	config = g_new0(SMConfig, 1);

	app->dirs[0] = g_strdup_printf("%s/"USER_CONFIG_DIR, g_get_home_dir());
	app->dirs[1] = strdup("./resources");
	app->dirs[2] = strdup("../resources");

	return app;
}


static GObject*
ayyi_app_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_properties)
{
	GObjectClass* parent_class = G_OBJECT_CLASS (ayyi_app_parent_class);
	GObject* obj = parent_class->constructor (type, n_construct_properties, construct_properties);
	return obj;
}


static void
ayyi_app_class_init (AyyiAppClass* klass)
{
	ayyi_app_parent_class = g_type_class_peek_parent (klass);

	G_OBJECT_CLASS (klass)->constructor = ayyi_app_constructor;
	G_OBJECT_CLASS (klass)->finalize = ayyi_app_finalize;

	g_signal_new ("app_stop_meters", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("app_start_meters", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("styles_available", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("shortcuts_changed", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("focus_changed", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	g_signal_new ("panel_list_changed", AYYI_TYPE_APP, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
ayyi_app_init (AyyiApp* self)
{
	self->priv = ayyi_app_get_instance_private(self);
	self->state = 0;

	snprintf(self->config_filename, 255, "%s/"USER_CONFIG_DIR"/seismix.yml", g_get_home_dir());
}


static void
ayyi_app_finalize (GObject* obj)
{
	G_OBJECT_CLASS (ayyi_app_parent_class)->finalize (obj);
}


void
app_set_state (AppState state)
{
	static int aborting = 0;

	app->state = state;

	switch(state){
		case APP_STATE_STARTED:
			if(song && song->periodic){ g_source_remove(song->periodic); song->periodic = 0; }
			break;
		case APP_STATE_ENGINE_OK:
			am_plugin__sync();
			break;
		case APP_STATE_SONG_LOAD_FAILED:
			{
				Arrange* arrange;
				if((arrange = ARRANGE_FIRST)) arr_cursor_reset(arrange, gtk_widget_get_toplevel(arrange->canvas->widget)->window);
			}
			break;
		case APP_STATE_ABORT:
			transport_disable();
			if(!aborting){
				aborting = TRUE;
				app_quit();
			}
			break;
	}
}


void
app_quit ()
{
	// Though not currently used, we would like the main loop to be still running here.

	app_set_state(APP_STATE_SHUTDOWN);

	window_foreach {
		if(window)
			set_cursor(((GtkWidget*)window)->window, CURSOR_WATCH); // set Busy cursor.
	} end_window_foreach;

	log_print(0, "shutting down...");
	while(gtk_events_pending()) gtk_main_iteration(); // update the display

	if(windows){
		config_save();
		gtk_accel_map_save(config->accels_file);
	}

#ifdef WITH_VALGRIND
	while(windows){
		AyyiWindow* window = windows->data;
		window__destroy_panel (window, window->dock->panels->data);
	}
#endif

	song_unload();
	if(!song_free()) perr ("song free failed.");

	toolbar_class_free();
	plugins_free();

#ifdef WITH_VALGRIND
	am_uninit();
#endif

	printf("exiting...\n");
	exit(0);
}


#ifdef USE_OPENGL
GdkGLConfig*
app_get_glconfig ()
{
	if(!app->priv->glconfig){
		if(!(app->priv->glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_STENCIL | GDK_GL_MODE_DOUBLE))){
			perr ("Cannot initialise gtkglext");
			return NULL;
		}
	}
	return app->priv->glconfig;
}
#endif

bool
app_config_dir_foreach (bool (*fn)(const char*, void*), void* user_data)
{
	for (int i=0;i<G_N_ELEMENTS(app->dirs);i++) {
		if (fn(app->dirs[i], user_data)) return true;
	}
	return false;
}
