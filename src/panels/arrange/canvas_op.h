/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __canvas_op_h__
#define __canvas_op_h__
#include "panels/arrange.h"

#ifdef USE_GNOMECANVAS
#include <libgnomecanvas/libgnomecanvas.h>
#endif

#ifdef USE_CLUTTER
#include <clutter/clutter.h>
#endif

typedef enum
{
    OP_NONE = 0,
    OP_MOVE,
    OP_COPY,
    OP_RESIZE_LEFT,
    OP_RESIZE_RIGHT,
    OP_REPEAT,
    OP_SPLIT,
    OP_DRAW,
    OP_BOX,
    OP_SCROLL,
    OP_EDIT,
    OP_VELOCITY,
    OP_VDIVIDER,
    OP_NOTE_LEFT,
    OP_NOTE_RIGHT,
    OP_NOTE_SELECT,
    OP_AUTO_DRAG,
    N_OP_TYPES
} Optype;


typedef enum
{
    READY_NONE = 0,
    READY_RESIZE_LEFT,
    READY_RESIZE_RIGHT,
} ReadyType;

typedef enum
{
	FEATURE_NONE,
	FEATURE_PART_LEFT,
	FEATURE_PART_RIGHT,
	FEATURE_VEL_AREA,
	FEATURE_VEL_DIVIDER,
	FEATURE_VEL_BAR_TOP,
	FEATURE_NOTE_AREA,
	FEATURE_NOTE,
	FEATURE_NOTE_LEFT,
	FEATURE_NOTE_RIGHT,
	N_FEATURE_TYPES,
} FeatureType;

typedef void   (*Press)  (CanvasOp*, GdkEvent*);
typedef void   (*Motion) (CanvasOp*);
typedef void   (*Finish) (CanvasOp*);

typedef struct _op
{
	Press  press;
	Motion motion;
	Finish finish;
} Op;


struct _CanvasOp
{
    Optype           type;
    Op*              op;

    Arrange*         arrange;
    AMPart*          part;
    LocalPart*       part_local;    // temp part used during copy operations. FIXME this really needs to be a list.

    Ptd              mouse;         // mouse coords in post-zoom world space (NOT screen/viewport coords)
    struct {
        GdkModifierType state;
    }                event;
    Ptd              p1;            // the part boundary - top left.
    Ptd              p2;            // the part boundary - bottom right.
    Ptd              start;         // cursor position at the start of a drag.
    Ptd              bnew;          // mouse coords, bounded (either by canvas or part depending on the operation type).
    Ptd              diff;          // the difference between the cursor and the top left of the part.
                                    // -this should be constant throughout a drag operation, but not for other op types.
    int              xscrolloffset, yscrolloffset;
    Ptd              old_bounded;   // values from the previous callback.
    double           max_movement;  // (x axis) used to detect false moves.

    GList*           note_selection;// TransitionalNote*
                                    // To support selections in multiple parts, maybe make this a list of Parts, each containing a list of notes.
    gint             timer;         // g_timout ref for user interaction, eg for scrolling off window edge.

    ReadyType        ready;

    union {
       struct {
           AGlActor* track;
           BPath     orig[2];
       }             automation;
    }                ft;
};

void         canvas_op__motion              (CanvasOp*);
void         canvas_op__resize_right_motion (CanvasOp*);
void         canvas_op__box_motion          (CanvasOp*);
void         canvas_op__draw_motion         (CanvasOp*);
void         canvas_op__edit_motion         (CanvasOp*);
ArrTrk*      canvas_op__get_track           (CanvasOp*, double py1);

const char*  canvas_op__print_type          (CanvasOp*);

FeatureType  pick_feature_type              (CanvasOp*);
const char*  print_feature_type             (FeatureType);
Optype       optype_from_tooltype           (ToolType);
GList*       canvas_op__get_note_selection  (CanvasOp*, MidiPart*);

bool         canvas_op__is_left_edge        (CanvasOp*);
bool         canvas_op__is_right_edge       (CanvasOp*);

void         canvas_op__check_song_length   (CanvasOp*, double x);


#endif
