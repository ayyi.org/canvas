/*

  Track Control area containing buttons and labels for a single
  arrange-window track.

  Class is based on GtkHBox, and is derived from GtkBox.

  ------------------------------------------------------------

  This file is part of AyyiGtk. http://ayyi.org
  copyright (C) 2004-2020 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ------------------------------------------------------------

  Modified by the GTK+ Team and others 1998-2000.  See the AUTHORS
  file for a list of people on the GTK+ Team.  See the ChangeLog
  files for a list of changes.  These files are distributed with
  GTK+ at ftp://ftp.gtk.org/pub/gtk/. 

*/
#include "global.h"
#ifndef DEBUG_TRACKCONTROL
#include "model/track_list.h"
#include "windows.h"
#include "window.statusbar.h"
#include "panels/arrange.h"
#include "gnome/automation.h"
#include "widgets/trackbutton.h"
#include "widgets/editablelabelbutton.h"
#include "support.h"
#include "gtkmeter.h"
#include "gtkvumeter.h"
#include "part_manager.h"
#include "song.h"
#include "io.h"
#include "style.h"
#include "track_list_box.h"

#include "track_control.h"
#include "track_control_midi.h" //temp!!

extern int         debug;
extern SMConfig*   config;

static void     track_control_finalize             (GObject*);
static void     track_control_set_name             (TrackControl*);
static gboolean track_control_on_buttonpress       (GtkWidget*, GdkEventButton*, TrackControl*);
static void     track_control_size_request         (GtkWidget*, GtkRequisition*);
static void     track_control_size_allocate        (GtkWidget*, GtkAllocation*);
static void     track_control_on_resize            (GtkWidget*, GdkEvent*, gpointer);
static gboolean track_control_drag_highlight_expose(GtkWidget*, GdkEventExpose*, gpointer);
static gboolean track_control_on_chlabel_press     (GtkWidget*, GdkEventButton*, gpointer);
static void     track_control_queue_for_redraw     (TrackControl*);
static gboolean track_control_do_redraw            (TrackControl*);
static void     track_control_mute_toggle          (GtkWidget*, gpointer _tnum);
static gboolean trkctl_track_has_widgets           (const AMTrack*);


G_DEFINE_TYPE (TrackControl, track_control, GTK_TYPE_BOX)

struct _TrackControlPriv
{
	EditableLabelButton* trnamebutton;
	EditableLabelButton* ch_button;
	guint                idle;
};


static void
track_control_class_init (TrackControlClass* class)
{
	GtkWidgetClass* widget_class = (GtkWidgetClass*) class;

	G_OBJECT_CLASS (class)->finalize = track_control_finalize;

	widget_class->size_request = track_control_size_request;
	widget_class->size_allocate = track_control_size_allocate;

	//class->redraw = track_control_redraw;
	class->set_height = track_control_set_height;
}


static void
track_control_init (TrackControl* tc)
{
	tc->priv = g_new0(TrackControlPriv, 1);

	extern void arr_track_pos_update(Arrange*);
	tc->on_resize = arr_track_pos_update; // TODO finish making this a proper dynamic callback set by the host.
}


static void
track_control_finalize (GObject* obj)
{
	TrackControl* tc = (TrackControl*)obj;
	TrackControlPriv* priv = tc->priv;

	ArrTrk* at = tc->atr;
	if(at->tname_changed){
		// refactored. May not be needed now
		if(G_TYPE_CHECK_INSTANCE(priv->trnamebutton))
			g_signal_handler_disconnect((gpointer)priv->trnamebutton, at->tname_changed);
		at->tname_changed = 0;
	}

	if(priv->idle){
		g_source_remove (priv->idle);
		priv->idle = 0;
	}

	g_free(TRACK_CONTROL(obj)->priv);
	G_OBJECT_CLASS (track_control_parent_class)->finalize (obj);
}


void
track_control_disconnect (GtkWidget* widget)
{
	g_return_if_fail(widget);

	TrackControl* tc = (TrackControl*)widget;
	ArrTrk* at = ((TrackControl*)widget)->atr;

	if(at->tname_changed && G_TYPE_CHECK_INSTANCE(widget)) g_signal_handler_disconnect((gpointer)tc->priv->trnamebutton, at->tname_changed);
}


void
track_control_make_widgets(TrackControl* tc)
{
	/*
	 ---hbox track
		+--spacer  (defines track height)
		+--buttons (rec,solo,mute)
		+--vbox
		   +--hbox
		   |  +--event box                 tname_ebox
		   |  |  +--alignment
		   |  |     +--label  (track name)
		   |  +--EditableLabelButton (channel name)
		   |     +--alignment
		   |        +--label
		   +--event box (track resize)
	*/
	TrackControlPriv* _tc = tc->priv;
	ArrTrk* arr_trk = tc->atr;
	AMTrack* tr = arr_trk->track;

	void track_arm_toggle(GtkWidget* widget, gpointer _tr)
	{
		AMTrack* tr = _tr;
		am_track__arm(tr, !am_track__is_armed(tr), NULL, NULL);
	}

	void
	track_solo_toggle(GtkWidget* widget, gpointer _tr)
	{
		// Toggle track solo status in response to a button click

		AMTrack* tr = _tr;
#ifdef DEBUG
		gboolean solod = am_track__is_solod(tr);
		dbg(0, "solo=%i --> %i", solod, !solod);
#endif

		am_track__solo(tr, !am_track__is_solod(tr), NULL, NULL);
	}

	// buttons
	{
		tc->brec = track_button_new("record");
		gtk_box_pack_start(GTK_BOX(tc), tc->brec, FALSE, FALSE, 0);
		g_signal_connect(G_OBJECT(tc->brec), "clicked", G_CALLBACK(track_arm_toggle), tr);
		char name[64]; sprintf(name, "Record Button w=%i", ayyi_panel_get_instance_num((AyyiPanel*)tc->arrange));
		gtk_widget_set_name(tc->brec, name);

		tc->bsolo = track_button_new("solo");
		gtk_box_pack_start(GTK_BOX(tc), tc->bsolo, FALSE, FALSE, 0);
		g_signal_connect(G_OBJECT(tc->bsolo), "clicked", G_CALLBACK(track_solo_toggle), tr);

		tc->bmute = track_button_new("mute");
		gtk_box_pack_start(GTK_BOX(tc), tc->bmute, FALSE, FALSE, 0);
		g_signal_connect(G_OBJECT(tc->bmute), "clicked", G_CALLBACK(track_control_mute_toggle), GINT_TO_POINTER(tc));
	}

	// "label" section

	// vbox for track label and resize handle
	GtkWidget* vbox_label2 = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(tc), vbox_label2, EXPAND_TRUE, FILL_TRUE, 0);

	// hbox for track name and channel name
	GtkWidget* hbox_names = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox_label2), hbox_names, EXPAND_TRUE, FILL_TRUE, 0);

#if 0
	void tnamebox_on_realise(GtkWidget* widget, gpointer user_data)
	{
		GdkColor colour;
		widget_get_bg_colour(widget, &colour, GTK_STATE_NORMAL);
		uint32_t rgb = color_gdk_to_rgba(&colour) >> 8;
		colour.red = 0; colour.blue = 0;
		uint32_t correct_colour = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(widget), "__rgb_colour"));
		dbg(0, "#%06x #%06x", rgb, correct_colour);
		//gtk_widget_modify_bg(widget, GTK_STATE_NORMAL, &colour);
	}
	g_signal_connect((gpointer)tc->tname_ebox, "realize", G_CALLBACK(tnamebox_on_realise), NULL);
#endif

	_tc->trnamebutton = editable_label_button_new(tr->name);
	gtk_widget_set_name((GtkWidget*)_tc->trnamebutton, "track_name_ebox");
	gtk_size_group_add_widget(tc->arrange->tracks.sizegroup, (GtkWidget*)_tc->trnamebutton);
	gtk_box_pack_start(GTK_BOX(hbox_names), (GtkWidget*)_tc->trnamebutton, EXPAND_FALSE, FILL_FALSE, 0);

	// Allow the event box to get focus
	// FIXME box receives focus but doesnt show it
	GValue gval = {0,};
	g_value_init(&gval, G_TYPE_BOOLEAN);
	g_value_set_boolean(&gval, TRUE);
	g_object_set_property(G_OBJECT(_tc->trnamebutton), "can-focus", &gval);

	// track resize event box
	tc->lbl_rsiz = gtk_event_box_new();
	gtk_box_pack_end(GTK_BOX(vbox_label2), tc->lbl_rsiz, FALSE, FALSE, 0);
	gtk_widget_set_size_request(tc->lbl_rsiz, -1, 4);

	void track_control_on_label_changed(GtkWidget* label, const char* new_text, gpointer user_data)
	{
		void on_name_change_done(AyyiIdent obj, GError** error, gpointer data)
		{
			if(!error) shell__statusbar_print(((AyyiPanel*)data)->window, 0, "Track name changed");
		}

		TrackControl* tc = user_data;
		g_return_if_fail(tc);
		PF0;
		am_track__set_name(tc->atr->track, new_text, on_name_change_done, tc->arrange);
	}
	arr_trk->tname_changed = g_signal_connect(G_OBJECT(_tc->trnamebutton), "edited", G_CALLBACK(track_control_on_label_changed), tc);

	// channel box
	// -shows the name of the channel associated with the track.
	// -if the engine has 1:1 relationship between tracks and channels, we show the channel output name instead.
	_tc->ch_button = editable_label_button_new(tr->name);
	gtk_box_pack_start(GTK_BOX(hbox_names), (GtkWidget*)_tc->ch_button, EXPAND_TRUE, FILL_TRUE, 0);
	g_signal_connect((gpointer)_tc->ch_button, "button-press-event", G_CALLBACK(track_control_on_chlabel_press), tc);

	// track select and double click
	g_signal_connect((gpointer)_tc->trnamebutton, "button-press-event", G_CALLBACK(track_control_on_buttonpress), tc);
	// track resize
	g_signal_connect((gpointer)tc->lbl_rsiz, "event", G_CALLBACK(track_control_on_resize), tc->arrange);

	gtk_widget_show_all((GtkWidget*)tc);
}


static void
meterval (Observable* o, AMVal val, gpointer _adj)
{
	gtk_adjustment_set_value(_adj, val.f);
}


void
track_control_make_meter(TrackControl* tc)
{
	ArrTrk* arr_trk = tc->atr;
	AMTrack* tr = tc->atr->track;

	am_track__enable_metering(tr);

	Observable* meter = tr->meterval;
	GtkObject* meter_adj = gtk_adjustment_new(0.0, meter->min.f, meter->max.f, 0.01, 0.1, 0.1);
	observable_subscribe(meter, meterval, meter_adj);

	GtkWidget* vumeter = arr_trk->meter = gtk_meter_new(GTK_ADJUSTMENT(meter_adj), GTK_METER_UP);
	gtk_widget_show(vumeter);
	gtk_meter_set_warn_point(GTK_METER(vumeter), 0.6);
	gtk_box_pack_start(GTK_BOX(tc), vumeter, false, false, 0);
}


void
track_control_remove_meter(TrackControl* tc)
{
	g_return_if_fail(tc);
	ArrTrk* arr_trk = tc->atr;
	if(!arr_trk->meter) return;

	gtk_widget_destroy(arr_trk->meter);
	arr_trk->meter = 0;
}


/*
 *  Add a spacer widget to the force the height.
 */
void
track_control_make_spacer (TrackControl* tc)
{
	ArrTrk* at = tc->atr;

	int height_nz = 0; // non-zoomed track height
	if(ayyi.got_shm){
		height_nz = at->height;
		if(height_nz < 1){
			height_nz = arr_track_min_height(at);
		}
	}else{
		// no song shm
		height_nz = at->height;
	}
	float track_height = height_nz * vzoom(tc->arrange);

	// spacer to force correct track height:
	// note: see tb_hseparator() in support.c
	// actually this method may be better as it may stop gtk trying to resize things.
	char spacer_path[256];
	snprintf(spacer_path, 255, "%s/../%s", config->svgdir, "1px.png");
	tc->fixed  = gtk_fixed_new();
	tc->spacer = gtk_image_new_from_file(spacer_path);
	gtk_fixed_put(GTK_FIXED(tc->fixed), tc->spacer, 0, track_height - 1);
	gtk_widget_show(tc->fixed);
	gtk_widget_show(tc->spacer);
	gtk_box_pack_start(GTK_BOX(tc), tc->fixed, FALSE, FALSE, 0);
}


void
track_control_set_track (TrackControl* tc, ArrTrk* atr)
{
	tc->atr = atr;

	// TODO this is perhaps not completely redundant, though it would be better to embed the TrackControl.
	g_object_set_data(G_OBJECT(tc), "track_idx", GINT_TO_POINTER(am_track_list_position(song->tracks, atr->track))); // embed the trk#.

	if(GTK_WIDGET_REALIZED((GtkWidget*)tc)){
		track_control_redraw(tc);
	}
}


void
track_control_redraw (TrackControl* tc)
{
	track_control_queue_for_redraw (tc);
}


static void
track_control_queue_for_redraw (TrackControl* tc)
{
	TrackControlPriv* priv = tc->priv;
	if(priv->idle) return;

	priv->idle = g_idle_add((GSourceFunc)track_control_do_redraw, tc);
}


static gboolean
track_control_do_redraw (TrackControl* tc)
{
	g_return_val_if_fail(IS_TRACK_CONTROL(tc), FALSE);
	TrackControlPriv* priv = tc->priv;
	g_return_val_if_fail(tc->atr, FALSE);
	AMTrack* tr = tc->atr->track;

	if(!trkctl_track_has_widgets(tr)){ perr ("track has no widgets."); return FALSE; }

	// labels
	track_control_set_name(tc);
	gtk_widget_show((GtkWidget*)priv->trnamebutton->label);
	track_control_set_channel_name(TRACK_CONTROL(tc));

	// FIXME the code below handles mute status. copy it to track_draw_selected() or something.
	//  -it may conflict with track_draw_selected().

	if(am_track__is_muted(tr)){
		g_return_val_if_fail(GTK_IS_WIDGET(tc->bmute), FALSE);
		track_button_set_active(tc->bmute);
	}
	if(am_track__is_solod(tr)){
		g_return_val_if_fail(GTK_IS_WIDGET(tc->bsolo), FALSE);
		track_button_set_active(tc->bsolo);
	}
	if (am_track__is_armed(tr)) track_button_set_active(tc->brec); else track_button_set_not_active(tc->brec);

	priv->idle = 0;
	return FALSE;
}


void
track_control_set_height (TrackControl* tc, int height)
{
	g_return_if_fail(tc);

	if(height < MIN_TRACK_HEIGHT || height > MAX_PART_HEIGHT) pwarn ("requested track height out of range: %i", height);
	height = MIN(height, MAX_PART_HEIGHT);

#ifdef DEBUG
	AMTrack* tr = tc->atr->track;
	dbg (2, "tc=%p t=%i d=%i height=%i", tc, am_track_list_position(song->tracks, tr), track_list__get_display_num(tc->arrange->priv->tracks, tr), height);
#endif

	gtk_fixed_move(GTK_FIXED(tc->fixed), tc->spacer, 0, height - 1);
}


void
track_control_set_tname(TrackControl* tc)
{
	// Update the given arrange window track strip label.

	// note: trkctl_redraw doesnt currently do this.

	g_return_if_fail(tc);

	AMTrack* tr = tc->atr->track;

	char tname[AYYI_NAME_MAX] = {0,};
	if(am_track__get_name(tr, tname)){
		dbg(2, "tnum=%i name=%s", am_track_list_position(song->tracks, tr), tname);

		char buff[AYYI_NAME_MAX + 8];
		snprintf(buff, AYYI_NAME_MAX + 8, "<b>%s</b>", tname);
		gtk_label_set_markup(GTK_LABEL(tc->priv->trnamebutton->label), buff);
	}
}


static void
track_control_set_name(TrackControl* tc)
{
	// Take the engine track_name and set it into the Arrange window track control widget.

	g_return_if_fail(tc->priv->trnamebutton->label);

	AMTrack* tr = tc->atr->track;

	gchar* buf = g_strdup_printf("<b>%s</b>", tr->name);

	gtk_label_set_markup(GTK_LABEL(tc->priv->trnamebutton->label), buf);

	g_free(buf);
}


void
track_control_set_channel_name (TrackControl* tc)
{
	g_return_if_fail(tc);
	AMTrack* tr = tc->atr->track;

	char ch_lbl[256] = "";
	am_track__get_channel_label(tr, ch_lbl);
	gchar* buf = g_strdup_printf("<small>%s</small>", ch_lbl);
	gtk_label_set_markup(GTK_LABEL(tc->priv->ch_button->label), buf);	
	g_free(buf);
}


void
track_control_set_colour(TrackControl* tc, int c_num)
{
	if(c_num < 0) return;

	GdkColor colour;
	am_palette_lookup(song->palette, &colour, c_num);

	GTK_TBUTTON(tc->brec)->colour = colour;
	GTK_TBUTTON(tc->bmute)->colour = colour;
	GTK_TBUTTON(tc->bsolo)->colour = colour;

	gtk_widget_modify_bg((GtkWidget*)tc->priv->trnamebutton, GTK_STATE_NORMAL, &colour);
	gtk_widget_modify_bg((GtkWidget*)tc->priv->ch_button, GTK_STATE_NORMAL, &colour);
	gtk_widget_modify_bg(tc->lbl_rsiz, GTK_STATE_NORMAL, &colour);
}


void
track_control_draw_selected(TrackControl* tc)
{
	// Apply the GTK_STATE_SELECTED colour to all widgets on this track.

	char id_str[64]; ayyi_panel_get_id_string_pretty((AyyiPanel*)tc->arrange, id_str);
	dbg(3, "%s", id_str);
	dbg(2, "%s", tc->atr->track->name);

	g_return_if_fail(tc);
	g_return_if_fail(tc->brec);

	track_button_set_track_selected(tc->brec);
	track_button_set_track_selected(tc->bsolo);
	track_button_set_track_selected(tc->bmute);

	//    GdkColor colour = get_style_bg_color(GTK_STATE_SELECTED);
	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(gtk_widget_get_toplevel(tc->brec)));
	//style->bg[GTK_STATE_NORMAL] = colour;
	style->bg[GTK_STATE_NORMAL] = style->bg[GTK_STATE_SELECTED];
	style->fg[GTK_STATE_NORMAL] = style->fg[GTK_STATE_SELECTED];
	dbg (2, "color=0x%02x%02x%02x", style->bg[GTK_STATE_NORMAL].red >> 8, style->bg[GTK_STATE_NORMAL].green >> 8, style->bg[GTK_STATE_NORMAL].blue >> 8);

	gtk_widget_set_style((GtkWidget*)tc->priv->trnamebutton, style);
	gtk_widget_set_style((GtkWidget*)tc->priv->ch_button, style);
	gtk_widget_set_style(tc->lbl_rsiz,     style);

	g_object_unref(style);
}


void
track_control_draw_unselected(TrackControl* tc)
{
	g_return_if_fail(tc);
	AMTrack* tr = tc->atr->track;
	g_return_if_fail(tr);

	track_button_set_track_unselected(tc->brec);
	track_button_set_track_unselected(tc->bsolo);
	track_button_set_track_unselected(tc->bmute);

	if(am_track__is_muted(tr)) track_control_set_muted(tc);
	else track_control_set_unmuted(tc);
}


void
track_control_set_unmuted(TrackControl* tc)
{
	g_return_if_fail(tc->brec);
	AMTrack* track = tc->atr->track;
	g_return_if_fail(track);

	GtkStyle* style = NULL;
	track_button_set_track_muted(tc->brec,  false);
	track_button_set_track_muted(tc->bsolo, false);
	track_button_set_track_muted(tc->bmute, false);

	if(!arr_track_is_selected(tc->arrange, track)){
		gtk_widget_set_style((GtkWidget*)tc->priv->trnamebutton, style);
		gtk_widget_set_style((GtkWidget*)tc->priv->ch_button,     style);
		gtk_widget_set_style(tc->lbl_rsiz,      style);
	}

	track_button_set_not_active(tc->bmute);
}


void
track_control_set_muted(TrackControl* tc)
{
	// Apply the GTK_STATE_INSENSITIVE colour to all track buttons on this track.

	g_return_if_fail(tc->brec);
	AMTrack* track = tc->atr->track;
	g_return_if_fail(track);

	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(tc->brec));
	style->bg[GTK_STATE_NORMAL] = style->bg[GTK_STATE_INSENSITIVE];
	style->fg[GTK_STATE_NORMAL] = style->fg[GTK_STATE_INSENSITIVE];

	track_button_set_track_muted(tc->brec,  true);
	track_button_set_track_muted(tc->bsolo, true);
	track_button_set_track_muted(tc->bmute, true);

	if(!arr_track_is_selected(tc->arrange, track)){
		gtk_widget_set_style((GtkWidget*)tc->priv->trnamebutton, style);
		gtk_widget_set_style((GtkWidget*)tc->priv->ch_button, style);
		gtk_widget_set_style(tc->lbl_rsiz,     style);
	}

	track_button_set_active(tc->bmute);

	g_object_unref(style);
}


void
track_control_on_style_change(TrackControl* tc)
{
	PF;
	GdkColor colour;
	widget_get_bg_colour(GTK_WIDGET(tc), &colour, GTK_STATE_NORMAL);
}


static gboolean
track_control_on_buttonpress(GtkWidget* widget, GdkEventButton* event, TrackControl* tc)
{
 	// Called (directly) when user clicks on a track in the arrange window.
	// (double clicks are handled by the widget itself)

	// @param widget    - should probably be the event_box as it has the track_idx embedded.
	// @param event     - may be NULL if it is called from another fn rather than from a mouse action.
	// @param user_data - currently NULL.

	g_return_val_if_fail(widget, NOT_HANDLED);
	Arrange* arrange = tc->arrange;
	app->active_panel = &arrange->panel;

	AMTrack* trk = tc->atr->track;
	TrackNum track_idx = am_track_list_position(song->tracks, trk);
	g_return_val_if_fail(track_idx < AM_MAX_TRK, NOT_HANDLED);
	dbg(1, "t=%i %s", track_idx, trk->name);

	// Focus the widget for keyboard operations
	gtk_widget_grab_focus(widget);

	if(event){
		if(event->type != GDK_BUTTON_PRESS) return NOT_HANDLED;

		// right button opens context menu
		if(event->button == 3){
			dbg (1, "right click.");
			if(arrange->tracks.menu){
				// open pop-up menu
				void arr_track_menu_show(Arrange*, TrackNum, GdkEventButton*);
				arr_track_menu_show(arrange, track_idx, event);
			}
			return HANDLED;
		}

		// Check if user has shift held down (for multiple tracks):
		// SHIFT selects a range, CTL adds to selection.
		if (event->state == GDK_SHIFT_MASK) dbg (0, "shift.");
	}

	if(!arr_track_is_selected(arrange, trk)){
		arr_track_select(arrange, trk);
		return HANDLED;
	}

	return NOT_HANDLED;
}


static void
track_control_mute_toggle(GtkWidget* widget, gpointer _tc)
{
	// Toggle track mute status in response to a button click.
	// We cant have 2 colours at once: should the 'selection' color really override the 'mute' color?

	PF;

	TrackControl* tc = (TrackControl*)_tc;

	am_track__mute(tc->atr->track, !am_track__is_muted(tc->atr->track), NULL, NULL);
}


static gboolean
track_control_on_chlabel_press(GtkWidget* widget, GdkEventButton* event, gpointer _tc)
{
	TrackControl* tc = (TrackControl*)_tc;
	g_return_val_if_fail(tc, NOT_HANDLED);

	if(event){
		Arrange* arrange; if( !(arrange = ARRANGE_FIRST) ) return NOT_HANDLED;

		switch(event->button){
			case 3:
				// right button opens context menu
				if(gui_song.ch_menu){
#if 1
					const AMChannel* ch = am_track__lookup_channel(tc->atr->track);
					output_menu_update((AyyiPanel*)arrange, ch);
					output_menu_popup();
#else
					arr_track_ch_menu_update(arrange);
					gtk_menu_popup(GTK_MENU(song->ch_menu), NULL, NULL, NULL, NULL, event->button, (guint32)(event->time));
#endif
				}
				return HANDLED;
			case 1:
				arr_track_select(arrange, tc->atr->track);
				return HANDLED;
				break;
			default:
				dbg (1, "button=%i", event->button);
				break;
		}
	}
	return NOT_HANDLED;
}


void 
track_control_drag_highlight (GtkWidget *widget)
{
	g_return_if_fail(GTK_IS_WIDGET (widget));

	g_signal_connect_after(widget, "expose_event", G_CALLBACK(track_control_drag_highlight_expose), NULL);

	gtk_widget_queue_draw (widget);
}


void 
track_control_drag_unhighlight (GtkWidget *widget)
{
	g_return_if_fail (GTK_IS_WIDGET (widget));

	g_signal_handlers_disconnect_by_func (widget, track_control_drag_highlight_expose, NULL);
  
	gtk_widget_queue_draw (widget);
}


static gboolean
track_control_drag_highlight_expose (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
	gint x, y, width, height;

	if (GTK_WIDGET_DRAWABLE (widget)){
		if (GTK_WIDGET_NO_WINDOW (widget)){
			x = widget->allocation.x;
			y = widget->allocation.y;
			width = widget->allocation.width;
			height = widget->allocation.height;
		} else {
			x = 0;
			y = 0;
			gdk_drawable_get_size (widget->window, &width, &height);
		}

		//gtk_paint_shadow (widget->style, widget->window, GTK_STATE_NORMAL, GTK_SHADOW_OUT, NULL, widget, "dnd", x, y, width, height);

		cairo_t* cr = gdk_cairo_create (widget->window);
		cairo_set_source_rgb (cr, 0.0, 0.0, 0.0); /* black */
		cairo_set_line_width (cr, 4.0);
		cairo_rectangle (cr, x + 0.5, y + 0.5, width - 1, y+ 2.5);
		cairo_stroke (cr);
		cairo_destroy (cr);
	}

	return FALSE;
}


static void
track_control_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
	GtkBox *box;
	GtkBoxChild *child;
	GList *children;
	gint nvis_children;
	gint width;

	box = GTK_BOX (widget);
	requisition->width = 0;
	requisition->height = 0;
	nvis_children = 0;

	children = box->children;
	while (children) {
		child = children->data;
		children = children->next;

		if (GTK_WIDGET_VISIBLE (child->widget))	{
			GtkRequisition child_requisition;

			gtk_widget_size_request (child->widget, &child_requisition);

			if (box->homogeneous) {
				width = child_requisition.width + child->padding * 2;
				requisition->width = MAX (requisition->width, width);
			} else {
				requisition->width += child_requisition.width + child->padding * 2;
			}

			requisition->height = MAX (requisition->height, child_requisition.height);

			nvis_children += 1;
		}
	}

	if (nvis_children > 0) {
		if (box->homogeneous) requisition->width *= nvis_children;
		requisition->width += (nvis_children - 1) * box->spacing;
	}

	requisition->width += GTK_CONTAINER (box)->border_width * 2;
	requisition->height += GTK_CONTAINER (box)->border_width * 2;
}


static void
track_control_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  GtkBox *box;
  GtkBoxChild *child;
  GList *children;
  GtkAllocation child_allocation;
  gint nvis_children;
  gint nexpand_children;
  gint child_width;
  gint width;
  gint extra;
  gint x;
  GtkTextDirection direction;

  box = GTK_BOX (widget);
  widget->allocation = *allocation;

  direction = gtk_widget_get_direction (widget);
  
  nvis_children = 0;
  nexpand_children = 0;
  children = box->children;

  while (children) {
    child = children->data;
    children = children->next;

    if (GTK_WIDGET_VISIBLE (child->widget)) {
      nvis_children += 1;
      if (child->expand) nexpand_children += 1;
    }
  }

  if (nvis_children > 0) {
      if (box->homogeneous)
	{
	  width = (allocation->width -
		   GTK_CONTAINER (box)->border_width * 2 -
		   (nvis_children - 1) * box->spacing);
	  extra = width / nvis_children;
	}
      else if (nexpand_children > 0)
	{
	  width = (gint) allocation->width - (gint) widget->requisition.width;
	  extra = width / nexpand_children;
	}
      else
	{
	  width = 0;
	  extra = 0;
	}

      x = allocation->x + GTK_CONTAINER (box)->border_width;
      child_allocation.y = allocation->y + GTK_CONTAINER (box)->border_width;
      child_allocation.height = MAX (1, (gint) allocation->height - (gint) GTK_CONTAINER (box)->border_width * 2);

      children = box->children;
      while (children)
	{
	  child = children->data;
	  children = children->next;

	  if ((child->pack == GTK_PACK_START) && GTK_WIDGET_VISIBLE (child->widget))
	    {
	      if (box->homogeneous)
		{
		  if (nvis_children == 1)
		    child_width = width;
		  else
		    child_width = extra;

		  nvis_children -= 1;
		  width -= extra;
		}
	      else
		{
		  GtkRequisition child_requisition;

		  gtk_widget_get_child_requisition (child->widget, &child_requisition);

		  child_width = child_requisition.width + child->padding * 2;

		  if (child->expand)
		    {
		      if (nexpand_children == 1)
			child_width += width;
		      else
			child_width += extra;

		      nexpand_children -= 1;
		      width -= extra;
		    }
		}

	      if (child->fill)
		{
		  child_allocation.width = MAX (1, (gint) child_width - (gint) child->padding * 2);
		  child_allocation.x = x + child->padding;
		}
	      else
		{
		  GtkRequisition child_requisition;

		  gtk_widget_get_child_requisition (child->widget, &child_requisition);
		  child_allocation.width = child_requisition.width;
		  child_allocation.x = x + (child_width - child_allocation.width) / 2;
		}

	      if (direction == GTK_TEXT_DIR_RTL)
		child_allocation.x = allocation->x + allocation->width - (child_allocation.x - allocation->x) - child_allocation.width;

	      gtk_widget_size_allocate (child->widget, &child_allocation);

	      x += child_width + box->spacing;
	    }
	}

      x = allocation->x + allocation->width - GTK_CONTAINER (box)->border_width;

      children = box->children;
      while (children)
	{
	  child = children->data;
	  children = children->next;

	  if ((child->pack == GTK_PACK_END) && GTK_WIDGET_VISIBLE (child->widget))
	    {
	      GtkRequisition child_requisition;
	      gtk_widget_get_child_requisition (child->widget, &child_requisition);

              if (box->homogeneous)
                {
                  if (nvis_children == 1)
                    child_width = width;
                  else
                    child_width = extra;

                  nvis_children -= 1;
                  width -= extra;
                }
              else
                {
		  child_width = child_requisition.width + child->padding * 2;

                  if (child->expand)
                    {
                      if (nexpand_children == 1)
                        child_width += width;
                      else
                        child_width += extra;

                      nexpand_children -= 1;
                      width -= extra;
                    }
                }

              if (child->fill)
                {
                  child_allocation.width = MAX (1, (gint)child_width - (gint)child->padding * 2);
                  child_allocation.x = x + child->padding - child_width;
                }
              else
                {
                  child_allocation.width = child_requisition.width;
                  child_allocation.x = x + (child_width - child_allocation.width) / 2 - child_width;
                }

              if (direction == GTK_TEXT_DIR_RTL)
              child_allocation.x = allocation->x + allocation->width - (child_allocation.x - allocation->x) - child_allocation.width;

              gtk_widget_size_allocate (child->widget, &child_allocation);

              x -= (child_width + box->spacing);
          }
       }
    }
}


static void
track_control_on_resize(GtkWidget* widget, GdkEvent* event, gpointer user_data)
{
  static double initial_trk_height, initialrooty, new_track_height_nz, new_track_height_z, old_dy_z;
  double dy_z;
  static int track_num = -1;
  static TrackControl* tc = NULL;
  gdouble rootx, rooty;
  static int dragging;
  int i;
  static int min_track_height;
  GdkCursor *curs;

  //double mouse_x = event->button.x;
  //double mouse_y = event->button.y;
  gdk_event_get_root_coords (event, &rootx, &rooty);

  Arrange* arrange = (Arrange*)user_data;
  windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE);

  switch (event->type)
  {
    case GDK_BUTTON_PRESS:
      switch(event->button.button)
      {
        case 1:
          if (event->button.state & GDK_SHIFT_MASK)
          {
            //gtk_object_destroy(GTK_OBJECT(item));
          }
          else
          {
            initialrooty = rooty;
            int _get_track_num()
            {
              int track_num = -1;
              for(i=0;i<AM_MAX_TRK;i++){
                ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, i);
                if(atr){
                  TrackControl *tc = (TrackControl*)atr->trk_ctl;
                  if(tc && tc->lbl_rsiz == widget){ track_num = i; break; }
                }
              }
              dbg(0, "t=%i", track_num);
              return track_num;
            }
            if((track_num = _get_track_num()) == -1){
              perr ("track_num not set.");
              track_num = 0;
            }
            ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, track_num);
            tc = TRACK_CONTROL(atr->trk_ctl);

            initial_trk_height = atr->height;
            min_track_height   = arr_track_min_height(atr);

           dragging = TRUE;
         }
         break;

         default:
           break;
           }
         break;

       case GDK_MOTION_NOTIFY:
         if (dragging && (event->motion.state & GDK_BUTTON1_MASK)){
           dy_z = rooty - initialrooty;
           if(dy_z==old_dy_z) break; //nothing to do.

           new_track_height_nz = initial_trk_height + dy_z / vzoom(arrange);
           new_track_height_z  = initial_trk_height * vzoom(arrange) + dy_z;
           //printf("new_height_nz=%f %f\n", new_track_height_nz, new_track_height_z);

           if(new_track_height_z < min_track_height){
             new_track_height_nz = min_track_height / vzoom(arrange);
             new_track_height_z  = min_track_height;
           }
           track_list__track_by_index(arrange->priv->tracks, track_num)->height = new_track_height_nz;

           //resize the track_control strip:
           if(IS_TRACK_CONTROL_MIDI(tc)){ //TODO
             track_control_midi_set_height(TRACK_CONTROL_MIDI(tc), new_track_height_z);
           }else{
             track_control_set_height(tc, new_track_height_z);
           }

           tc->on_resize(arrange);

           //update the main canvas:
           arrange->canvas->redraw(arrange);
#ifdef USE_GNOMECANVAS
           automation_view_on_size_change(arrange);
#endif

           char str[64];
           snprintf(str, 64, "%.1f", new_track_height_nz);
           arr_statusbar_printf(arrange, 3, str);

           old_dy_z = dy_z;
         }
       break;

     case GDK_BUTTON_RELEASE:
       curs = gdk_cursor_new(GDK_LEFT_PTR);
       gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
       gdk_cursor_unref(curs);
       dragging = FALSE;

       //resize the canvas so scrollbars are correct:
#ifdef USE_GNOMECANVAS
       gcanvas_scrollregion_update(arrange);
#endif
       track_list_box_on_dim_change(arrange);

       arr_track_set_height(arrange, song->tracks->track[track_num], track_list__track_by_index(arrange->priv->tracks, track_num)->height);
       break;

     case GDK_ENTER_NOTIFY:
       curs = gdk_cursor_new(GDK_DOUBLE_ARROW);
       gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
       gdk_cursor_unref(curs);

       break;
   
     case GDK_LEAVE_NOTIFY:
       if(dragging == FALSE){
         curs = gdk_cursor_new(GDK_LEFT_PTR);
         gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
         gdk_cursor_unref(curs);
       }
       break;
   
     default:
       break;
   }
}


static gboolean
trkctl_track_has_widgets(const AMTrack* trk)
{
	g_return_val_if_fail(trk, false);
	arrange_foreach {
		ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, (AMTrack*)trk);
		g_return_val_if_fail(atr, false);
		g_return_val_if_fail(atr->trk_ctl, false);
#ifndef DEBUG_TRACKCONTROL
		g_return_val_if_fail(TRACK_CONTROL(atr->trk_ctl)->bmute, false);
#endif
	} end_arrange_foreach
	return true;
}

#endif //DEBUG_TRACKCONTROL


#define __TRACK_CONTROL_C__
