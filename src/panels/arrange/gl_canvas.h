/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __gl_canvas_h__
#define __gl_canvas_h__

#define CANVAS_IS_OPENGL (arrange->canvas->type == arr_gl_canvas_get_type())

#ifdef __gl_canvas_priv__

#include <gtk/gtkgl.h>
#include "waveform/ui-typedefs.h"
#include "agl/fbo.h"
#include "arrange/gl_track_control.h"

#define CGL (arrange->canvas->gl)
#define SCENE ((AGlScene*)CGL->actor)
#define vzoom2(ARR) (ARR)->canvas->gl->zoom->value.pt.y

typedef enum {
	ACTOR_TYPE_MAIN = 0,
	ACTOR_TYPE_TC,
	ACTOR_TYPE_TRACKS,
	ACTOR_TYPE_BACKGROUND,
	ACTOR_TYPE_RULER,
	ACTOR_TYPE_HSCROLLBAR,
	ACTOR_TYPE_VSCROLLBAR,
	ACTOR_TYPE_OVERVIEW,
	ACTOR_TYPE_TARGET,
	ACTOR_TYPE_FG,
	ACTOR_TYPE_ISO_FRAME,
	ACTOR_TYPE_ISO_TC,
	ACTOR_TYPE_MAX
} ActorType;

struct _GlCanvas
{
    AGlActor*      actor;       // root actor

    bool           isometric;
    float          iso_factor;  // 1.0 is fully isometric.

    bool           part_background_alpha;

    GtkWidget*     image;       // gl drawing area
    GtkWidget*     table;       // redundant - use canvas->container
    GtkWidget*     iso_button;

    WaveformContext* wfc;

    Observable*    zoom;        // animated zoom - c/w AyyiPanel zoom which is not animated

    GHashTable*    parts;       // type PartActor*
    GList*         local_parts; // transient parts, used for example during copy operations. doesnt belong here ideally as it is relevant to all non retained-mode canvas types.

    AGlActor*      actors[ACTOR_TYPE_MAX];

    bool           is_new;      // false for all realise events except the first

    GList*         transitions; // animations are tracked so they can be cancelled
};

typedef struct {
    AGlActor actor;
    ArrTrk*  atr;
} TrackActor;

typedef struct {
    AGlActor actor;
    AMPart*  part;
    WaveformActor* wf_actor;
} PartActor;

#endif // end __gl_canvas_priv__


void         arr_gl_canvas_init             ();
CanvasType   arr_gl_canvas_get_type         ();
void         arr_gl_canvas_redraw_track     (Arrange*, ArrTrk*);
void         arr_gl_canvas_redraw_part      (Arrange*, AMPart*);
void         arr_gl_canvas_paint_local_part (Arrange*, LocalPart*);
double       arr_gl_get_spp_px              (Arrange*);
AyyiSongPos* arr_gl_px2songpos              (Arrange*, AyyiSongPos*, double px);
double       arr_gl_pos2px_                 (Arrange*, AyyiSongPos*);
bool         arr_gl_pos_is_in_viewport      (Arrange*, AyyiSongPos*);

#endif
