/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __support_c__
#include <sys/stat.h>
#include <math.h>
#include <dirent.h>
#include <fcntl.h>

#include "global.h"

#include <gdk/gdkkeysyms.h>

#include <model/time.h>
#include <model/transport.h>
#include <model/part_manager.h>
#include "waveform/waveform.h"
#include "window.h"
#include "windows.h"
#include "support.h"
#include "song.h"
#include "icon.h"
#include "style.h"
#include "statusbar.h"
#include "shortcuts.h"

extern SMConfig* config;

GtkIconFactory* icon_factory = NULL;

static GList* pixmaps_directories = NULL;

static void k_select_all       (GtkAccelGroup*, gpointer);
static void close_active_panel (GtkAccelGroup*, gpointer);

static const gchar* get_mod_name_shift   ();
static const gchar* get_mod_name_control ();
static const gchar* get_mod_name_alt     ();
static const gchar* get_mod_separator    ();
static const gchar* get_mod_string       (GdkModifierType);


/*
 *  Use this function to set the directory containing installed pixmaps.
 */
void
add_pixmap_directory (const gchar* directory)
{
	pixmaps_directories = g_list_prepend (pixmaps_directories, g_strdup (directory));
}


/*
 *  Apply the palette colour number to a gtk widget.
 */
void
widget_colour_set (GtkWidget* widget, int num)
{
#ifdef DEBUG
	if (GTK_WIDGET_NO_WINDOW(widget)) perr("widget has no window!");
#endif

	GdkColor colour;
	am_palette_lookup(song->palette, &colour, num);
	gtk_widget_modify_bg(widget, GTK_STATE_NORMAL, &colour);
}


void
set_cursor (GdkWindow* window, GdkCursorType type)
{
	if (!window) return;

	if (type) {
		dbg(2, "cursor=%p", app->cursors[type]);
		gdk_window_set_cursor(window, app->cursors[type]);
	} else {
		gdk_window_set_cursor(window, NULL);
	}

	app->cursor_timer.id = 0;
}


void
set_cursor_delayed (GdkWindow* window, GdkCursorType type)
{
	bool set_cursor_delayed_cb(CursorTimer* cursor_timer)
	{
		if (!cursor_timer->id) return false; // cursor change has been cancelled.

		if (cursor_timer->type) {
			GdkCursor* cursor = gdk_cursor_new(cursor_timer->type);
			gdk_window_set_cursor(cursor_timer->window, cursor);
			gdk_cursor_unref(cursor);
		} else {
			gdk_window_set_cursor(cursor_timer->window, NULL);
		}
		return false;
	}

	app->cursor_timer = (CursorTimer){
		.window = window,
		.type = type,
		.id = g_timeout_add(500, (GSourceFunc)set_cursor_delayed_cb, &app->cursor_timer)
	};
}


/*
 *  Return the first dir in the given list that exists.
 *  The list must be null terminated.
 */
const char*
find_path (const char* dirs[])
{
	for (int d=0;dirs[d];d++) {
		if (g_file_test(dirs[d], G_FILE_TEST_IS_DIR)) return dirs[d];
	}
	return dirs[0];
}


/*
 *  Get a list of files and subdirs within $path.
 *  -each file in the returned list must be freed with g_free().
 */
gint
filelist_read (const gchar* path, GList** files, GList** dirs)
{
	DIR* dp;
	struct dirent* dir;
	struct stat ent_sbuf;

	GList* dlist = NULL;
	GList* flist = NULL;

	gchar* pathl = path_from_utf8(path);
	if (!pathl || (dp = opendir(pathl)) == NULL){
		g_free(pathl);
		if (files) *files = NULL;
		if (dirs) *dirs = NULL;
		return FALSE;
	}

	// root dir fix
	if (pathl[0] == '/' && pathl[1] == '\0') {
		g_free(pathl);
		pathl = g_strdup("");
	}

	while ((dir = readdir(dp)) != NULL){
		gchar* name = dir->d_name;
		if (!is_hidden(name)) {
			gchar* filepath = g_strconcat(pathl, "/", name, NULL);
			bool added = false;
			if (stat(filepath, &ent_sbuf) >= 0) {
				if (S_ISDIR(ent_sbuf.st_mode)) {
					if ((dirs) && !(name[0] == '.' && (name[1] == '\0' || (name[1] == '.' && name[2] == '\0')))) {
						dbg (0, "adding file=%s...", filepath);
						dlist = g_list_prepend(dlist, filepath);
						added = true;
					}
				} else {
					if (files) {
						flist = g_list_prepend(flist, filepath);
						added = true;
					}
				}
			}
			if(!added) g_free(filepath);
		}
	}

	closedir(dp);

	g_free(pathl);

	if (dirs) *dirs = dlist;
	if (files) *files = flist;

	return true;
}


gint
is_hidden (const gchar* name)
{
	if (name[0] != '.') return FALSE;
	if (name[1] == '\0' || (name[1] == '.' && name[2] == '\0')) return FALSE;
	return TRUE;
}


void
get_font_string (char* font_string, int size_modifier)
{
	if (app->font_size) {
		snprintf(font_string, 63, "%.59s %.2i", app->font_family, app->font_size + size_modifier);
	} else {
		pwarn ("font size is not set.");
		snprintf(font_string, 63, "San-Serif %i", 10);
	}
}


void
get_style_font ()
{
	g_return_if_fail(windows);

	GtkWidget* window = windows->data;
	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(window));
	PangoFontDescription* font_desc = style->font_desc;

	PangoFontMask mask = pango_font_description_get_set_fields(font_desc);
	if (!(mask & PANGO_FONT_MASK_SIZE)) pwarn ("style font size is not set.\n");

	app->font_size = pango_font_description_get_size(font_desc) / PANGO_SCALE;
	strcpy(app->font_family, pango_font_description_get_family(font_desc));
	if (!app->font_size) pwarn ("failed to get default font size");

	g_object_unref(style);
}


void
get_style_bg_color (GdkColor* colour, GtkStateType state)
{
	g_return_if_fail(colour);

	GtkWidget* widget = windows->data;

	GtkStyle* style = gtk_widget_get_style(widget);

	*colour = style->bg[state];
}


void
get_style_fg_color (GdkColor* colour, GtkStateType state)
{
	widget_get_fg_colour((GtkWidget*)windows->data, colour, state);
}


uint32_t
get_style_bg_color_rgba (GtkStateType state)
{
	GdkColor bg;
	get_style_bg_color(&bg, state);
	bg.red   = MIN((1 << 16) - 1, bg.red);
	bg.green = MIN((1 << 16) - 1, bg.green);
	bg.blue  = MIN((1 << 16) - 1, bg.blue);

	return am_gdk_to_rgba(&bg);
}


uint32_t
get_style_fg_color_rgba (GtkStateType state)
{
	GdkColor bg;
	get_style_fg_color(&bg, state);
	bg.red   = MIN((1 << 16) - 1, bg.red);
	bg.green = MIN((1 << 16) - 1, bg.green);
	bg.blue  = MIN((1 << 16) - 1, bg.blue);

	return am_gdk_to_rgba(&bg);
}


uint32_t
get_style_text_color_rgba (GtkStateType state)
{
	GtkStyle* style = gtk_widget_get_style(windows->data);

	GdkColor c = style->text[state];
	c.red   = MIN((1 << 16) - 1, c.red);
	c.green = MIN((1 << 16) - 1, c.green);
	c.blue  = MIN((1 << 16) - 1, c.blue);

	return am_gdk_to_rgba(&c);
}


void
widget_get_bg_colour (GtkWidget* widget, GdkColor* colour, GtkStateType state)
{
	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));
	colour->red   = style->bg[state].red;
	colour->green = style->bg[state].green;
	colour->blue  = style->bg[state].blue;
	g_object_unref(style);
}


void
widget_get_fg_colour (GtkWidget* widget, GdkColor* colour, GtkStateType state)
{
	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));
	colour->red   = style->fg[state].red;
	colour->green = style->fg[state].green;
	colour->blue  = style->fg[state].blue;
	g_object_unref(style);
}


void
colour_rgba_to_float (AGlColourFloat* colour, uint32_t rgba)
{
	// returned values are in the range 0.0 to 1.0;

	g_return_if_fail(colour);

	colour->r = (float)((rgba & 0xff000000) >> 24) / 0xff;
	colour->g = (float)((rgba & 0x00ff0000) >> 16) / 0xff;
	colour->b = (float)((rgba & 0x0000ff00) >>  8) / 0xff;
}


void
colour_gdk_to_float (AGlColourFloat* c, GdkColor* gdk)
{
	g_return_if_fail(c);

	c->r = ((float)gdk->red)   / 0x10000;
	c->g = ((float)gdk->green) / 0x10000;
	c->b = ((float)gdk->blue)  / 0x10000;
}


uint32_t
colour_lighter_rgba (uint32_t colour, int amount)
{
	// return a colour slightly lighter than the one given

	int fixed = 0x2 * amount; // 0x10 is just visible on black with amount=2
	float multiplier = 1.0;// + amount / 60.0;

	int red   = MIN(((colour & 0xff000000) >> 24) * multiplier + fixed, 0xff);
	/*
	printf("%s(): red: %x %x %x\n", __func__, (colour & 0xff000000) >> 24, 
                     (int)(((colour & 0xff000000) >> 24) * 1.2 + 0x600),
					 red
					 );
	*/
	int green = MIN(((colour & 0x00ff0000) >> 16) * multiplier + fixed, 0xff);
	int blue  = MIN(((colour & 0x0000ff00) >>  8) * multiplier + fixed, 0xff);
	int alpha = colour & 0x000000ff;

	//dbg(0, "%08x --> %08x", colour, red << 24 | green << 16 | blue << 8 | alpha);
	return red << 24 | green << 16 | blue << 8 | alpha;
}


/*
 *  Return a colour slightly lighter than the one given
 *  -amount must be between 0 and 30
 */
uint32_t
colour_darker_rgba (uint32_t colour, int amount)
{
	int fixed = -0x2 * amount;
	float multiplier = 1.0 - amount / 30.0;

	int red   = MIN(((colour & 0xff000000) >> 24) * multiplier + fixed, 0xff);
	int green = MIN(((colour & 0x00ff0000) >> 16) * multiplier + fixed, 0xff);
	int blue  = MIN(((colour & 0x0000ff00) >>  8) * multiplier + fixed, 0xff);
	int alpha = colour & 0x000000ff;

	//printf("%s(): %08x --> %08x\n", __func__, colour, red << 24 | green << 16 | blue << 8 | alpha);
	return red << 24 | green << 16 | blue << 8 | alpha;
}


void
colour_lighter_gdk (GdkColor* colour, int amount)
{
	// @param amount should be in the range 1 - 10.

	g_return_if_fail(colour);

	//hopefully the tweaking on these 2 values is roughly right now. - perhaps a bit extreme on v light colours?
	//-no: probably need to increase the amount for near-black values.
	int fixed = 0x200 * amount;
	float multiplier = 1.0 + amount / 30.0;

	double red   = colour->red;
	//colour->red   = MIN(colour->red   * multiplier + fixed, 0xffff);
	colour->green = MIN(colour->green * multiplier + fixed, 0xffff);
	colour->blue  = MIN(colour->blue  * multiplier + fixed, 0xffff);

	colour->red = MIN((int)(red  * multiplier + fixed), 0xffff);

	dbg(3, "%s", colour_to_string(colour));
}


void
colour_darker_gdk (GdkColor* colour, int amount)
{
	// @param amount should be in the range 1 - 10.

	g_return_if_fail(colour);

	//hopefully the tweaking on these 2 values is roughly right now.
	int fixed = -0x200 * amount;
	float multiplier = 1.0 - amount / 30.0;

#ifdef DEBUG
	int red_orig = colour->red;
#endif
	double red   = colour->red;
	//colour->red   = MIN(colour->red   * multiplier + fixed, 0xffff);
	colour->green = MIN(colour->green * multiplier + fixed, 0xffff);
	colour->blue  = MIN(colour->blue  * multiplier + fixed, 0xffff);

	red = red * multiplier + fixed;
	if (red < 0) red = 0.0;
	//colour->red = MAX(MIN((int)(red  * multiplier + fixed), 0xffff), 0);
	colour->red = red;
	dbg (2, "red=0x%x %.2f --> 0x%x mult=%.2f", red_orig, red, colour->red, multiplier);
}


void
colour_grey_out (GdkColor* colour, GdkColor* bg)
{
	int invert = colour_is_dark(bg) ? 1 : -1;

	#define GREYOUT_FACTOR 4
	colour->red   = CLAMP(bg->red   + (ABS(0x7fff - colour->red  ) / GREYOUT_FACTOR * invert), 0, 0xffff);
	colour->green = CLAMP(bg->green + (ABS(0x7fff - colour->green) / GREYOUT_FACTOR * invert), 0, 0xffff);
	colour->blue  = CLAMP(bg->blue  + (ABS(0x7fff - colour->blue ) / GREYOUT_FACTOR * invert), 0, 0xffff);
}


uint32_t
colour_grey_out_rgba (uint32_t colour)
{
	// increase amount to get more greying
	// 384 is equivalent to opacity of 0.65 (inverted and multiplied by 256)
	#define GREY_AMOUNT 436

	int32_t r = ((((int32_t)((colour & 0xff000000) >> 24)) - 0x80) * 256) / GREY_AMOUNT + 0x80;
	int32_t g = ((((int32_t)((colour & 0x00ff0000) >> 16)) - 0x80) * 256) / GREY_AMOUNT + 0x80;
	int32_t b = ((((int32_t)((colour & 0x0000ff00) >>  8)) - 0x80) * 256) / GREY_AMOUNT + 0x80;
	int32_t a = (colour & 0x000000ff);
	return (r << 24) + (g << 16) + (b << 8) + a;
}


uint32_t
colour_get_offset_rgba (uint32_t colour, int amount)
{
	if (colour_is_dark_rgba(colour))
		return colour_lighter_rgba(colour, amount);
	else
		return colour_darker_rgba(colour, amount);
}


void
colour_get_frame (GdkColor* colour)
{
	if (colour_is_dark(colour))
		colour_lighter_gdk(colour, 2);
	 else
		colour_darker_gdk(colour, 2);
}


gboolean
colour_is_dark (GdkColor* colour)
{
	int average = (colour->red + colour->green + colour->blue ) / 3;
	return (average < 0x8000);
}


gboolean
colour_is_dark_rgba (uint32_t colour)
{
	int r = (colour & 0xff000000) >> 24;
	int g = (colour & 0x00ff0000) >> 16;
	int b = (colour & 0x0000ff00) >>  8;

	int average = (r + g + b ) / 3;
	return (average < 0x80);
}


void
colour_mix (GdkColor* colour1, const GdkColor* colour2, int percentage)
{
	//mix @colour1 and @colour2 and return the result in @colour1.

	colour1->red   = (colour1->red * (100 - percentage) + colour2->red * percentage) / 100;
	colour1->green = (colour1->green * (100 - percentage) + colour2->green * percentage) / 100;
	colour1->blue  = (colour1->blue * (100 - percentage) + colour2->blue * percentage) / 100;
}


uint32_t
colour_mix_rgba (uint32_t a, uint32_t b, float _amount)
{
	int amount = _amount * 0xff;
	int red0 = a                >> 24;
	int grn0 = (a & 0x00ff0000) >> 16;
	int blu0 = (a & 0x0000ff00) >>  8;
	int   a0 = (a & 0x000000ff);
	int red1 = b                >> 24;
	int grn1 = (b & 0x00ff0000) >> 16;
	int blu1 = (b & 0x0000ff00) >>  8;
	int   a1 = (b & 0x000000ff);
	return
		(((red0 * (0xff - amount) + red1 * amount) / 256) << 24) + 
		(((grn0 * (0xff - amount) + grn1 * amount) / 256) << 16) + 
		(((blu0 * (0xff - amount) + blu1 * amount) / 256) <<  8) +
		(a0 * (0xff - amount) + a1 * amount) / 256;
}


const char*
colour_to_string (GdkColor* colour)
{
	static char s[16];
	sprintf(s, "%02x%02x%02x", colour->red >> 8, colour->green >> 8, colour->blue >> 8);
	return s;
}


void
report (int type, char* format, ...)
{
	// this fn is deprecated, as some messages come from the model, eg incoming messages. See log_print() instead which has a gui callback.

	char s[128];
	va_list argp;
	va_start(argp, format);
	vsnprintf(s, 127, format, argp);
	va_end(argp);

	log_print(type, s);

	PF;
	const gchar* stock_id = (type == LOG_FAIL) ? GTK_STOCK_DIALOG_WARNING : GTK_STOCK_APPLY;
	window_foreach {
		statusbar__set_icon(window->statusbar, stock_id);
		if (window->statusbar->widget[STATUSBAR_ICON]) gtk_widget_set_tooltip_text(window->statusbar->widget[STATUSBAR_ICON], s);
	} end_window_foreach
}


void
pixbuf_draw_line_cairo (cairo_t* cr, DRect* pts, double line_width, GdkColor* colour)
{
	if (pts->y1 == pts->y2) return;
	cairo_rectangle(cr, pts->x1, pts->y1, pts->x2 - pts->x1 + 1, pts->y2 - pts->y1 + 1);
	cairo_fill (cr);
	cairo_stroke (cr);
}


void
pixbuf_fill_rect (GdkPixbuf* pixbuf, GdkRectangle* rect, GdkColor* colour)
{
	char src[3];
	src[0] = colour->red   >> 8;
	src[1] = colour->green >> 8;
	src[2] = colour->blue  >> 8;

	guchar* buf = gdk_pixbuf_get_pixels(pixbuf);
	int width = gdk_pixbuf_get_width(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	int x2 = MIN(rect->x + rect->width, width);
	int y2 = MIN(rect->y + rect->height, height);
	int p = 0;
	for (int y=rect->y; y<y2; y++) {
		int x; for(x=rect->x; x<x2; x++){
			p = x * 3 + y * gdk_pixbuf_get_rowstride(pixbuf);
			// TODO its possible last row may be short
			memcpy(&buf[p], src, 3);
		}
	}
}


void
pixbuf_clear_rect (GdkPixbuf* pixbuf, GdkRectangle* rect)
{
	guchar* buf = gdk_pixbuf_get_pixels(pixbuf);
	int x1 = rect->x;
	int width = MIN(rect->width, gdk_pixbuf_get_width(pixbuf) - rect->x);
	int y2 = MIN(rect->y + rect->height, gdk_pixbuf_get_height(pixbuf));
	int p = 0;
	for (int y=rect->y; y<y2; y++){
		p = x1 * 3 + y * gdk_pixbuf_get_rowstride(pixbuf);
		memset(&buf[p], 0, width * 3);
	}
}


/*
 *  Clear the alpha channel, but leave the rgb data intact.
 */
void
pixbuf_clear_alpha (GdkPixbuf* pixbuf, guchar alpha)
{
	if (!gdk_pixbuf_get_has_alpha(pixbuf)){ pwarn("pixbuf doesnt have alpha channel"); return; }
	int n_chans; if ((n_chans = gdk_pixbuf_get_n_channels(pixbuf)) != 4){ pwarn("pixbuf n_chans != 4"); return; }

	guchar* buf = gdk_pixbuf_get_pixels(pixbuf);
	guint rs = gdk_pixbuf_get_rowstride(pixbuf);

	for (int y=0; y<gdk_pixbuf_get_height(pixbuf); y++){
		for (int x=0; x<gdk_pixbuf_get_width(pixbuf); x++){
			*(buf + y * rs + x * n_chans + 3) = alpha;
		}
	}
}


/*
 *  Strip off the "file:" from the beginning of the string:
 */
bool
dnd_get_filename (const char* uri, char* filename)
{
/*
	//check the string starts with "file:":
	if(strstr(u, "file:") != u){
		P_WARN ("unknown string format: '%s'. Ignoring.\n", u);
		return FALSE;
	}
	filename = memcpy(filename, u+7, strlen(u)-5+1); //copy n chars from ct to s, and return s
*/
	GError* error = NULL;

	gchar* f = g_filename_from_uri(uri, NULL, &error);
	P_GERR;
	if (!f) return false;

	strncpy(filename, f, 255);
	g_free(f);

	if (!strlen(filename)) {
#ifdef DEBUG
		pwarn ("empty filename: '%s'.\n", uri);
#endif
		return false;
	}

	return true;
}


/*
 *  Return the index of the named snap mode.
 */
int
snap_mode_lookup (const char* mode_name)
{
	for (int i=0;i<MAX_SNAP;i++) {
		if (!strcmp(snap_modes[i].name, mode_name)) {
			return i;
		}
	}
	return 0;
}


void
print_gerror (GError** error)
{
	//print and free the GEerror
	//-does the same as the P_GERR macro
	//not tested!
	if (*error) {
		printf("%s(): %s\n", __func__, (*error)->message);
		g_error_free(*error);
		*error = NULL;
	}
}


void
accel_map_foreach (gpointer data, const gchar* accel_path, guint accel_key, GdkModifierType accel_mods, gboolean changed)
{
	dbg(0, "%s", accel_path);
}


#define MAX_STOCK 30
static GtkStockItem new_stock_items[MAX_STOCK];

static void
add_stock_item (const char* name, const char* svgfile)
{
	const GtkIconSource* source = gtk_icon_source_new();

	gchar* path = g_strdup_printf("%s/%s.svg", config->svgdir, svgfile);
	gtk_icon_source_set_filename((GtkIconSource*)source, path);
	g_free(path);

	GtkIconSet* icon_set = gtk_icon_set_new();
	gtk_icon_set_add_source(icon_set, source);
	gtk_icon_factory_add(icon_factory, name, icon_set);
}


/*
 *  Allocate memory for stock items. This is neccesary even for 'stock' stock items
 *
 *	@param svgfile - if this is NULL, we use the standard gtk stock item defined by stock_id.
 *
 */
static void
stock_item_add (const char* name, const char* label, AMAccel* accel, const char* svgfile, guchar* stock_id)
{
	// existing stock_items already have an associated key. We seem to ignore that.

	static int idx = 0;
	GtkStockItem* stock_item = &new_stock_items[idx++];
#ifdef DEBUG
	if (idx >= MAX_STOCK) perr("stock_items full!");
#endif

	AMKey* alt_key = &accel->key[1];
	int n_keys = alt_key->code ? 2 : 1;   // accels either have 1 or 2 keys

	if (svgfile) {
		dbg(2, "n_keys=%i", n_keys);
		for (int i=0;i<n_keys;i++) {
			if(i == 1) dbg(2, "adding ALT action: %s", name); //FIXME use diff mem loc
			*stock_item = (GtkStockItem){
				.stock_id = g_strconcat(name, (i == 1) ? " ALT" : NULL, NULL),
				.label    = g_strdup(label),
				.keyval   = accel->key[i].code,
				.modifier = accel->key[i].mask
			};
		}

		add_stock_item(name, svgfile);
	} else {
		// 'stock' gtk item
		if (!gtk_stock_lookup((char*)stock_id, stock_item)) pwarn("stock lookup failed.");

		if (n_keys == 2) {
			GtkStockItem* stock_item = &new_stock_items[idx++];
			dbg(2, "%i: adding ALT action: %s", idx, g_strconcat(name, "-ALT", NULL));
			*stock_item = (GtkStockItem){
				.stock_id = g_strconcat(name, "-ALT", NULL),
				.label    = g_strdup(label),
				.keyval   = accel->key[1].code,
				.modifier = accel->key[1].mask
			};
		}
	}
	accel->stock_item = stock_item;
}


void
global_accels_init ()
{
	// Icon names are currently hardcoded in the app.
	// Strictly speaking we should use gtk_icon_sets and a default icon_factory so that icons can be overridden by the gtk theme.

	app->global_accel_group = gtk_accel_group_new();

	icon_factory = gtk_icon_factory_new();
	gtk_icon_factory_add_default(icon_factory);

	gtk_icon_size_register("32", 32, 32);
	gtk_icon_size_register("48", 48, 48);
	gtk_icon_size_register("64", 64, 64);
	gtk_icon_size_register("96", 96, 96);

	add_stock_item(SM_STOCK_BLANK, "blank");
	add_stock_item(SM_STOCK_POINTER, "pointer");
	add_stock_item(SM_STOCK_AUTOMATION, "automation");
	add_stock_item(SM_STOCK_HISTORY, "history");

	//GtkStockItem (*new_stock_items)[10] = (GtkStockItem (*)[10])g_new0(GtkStockItem, 10);
	//malloc(10 * sizeof(GtkStockItem)); //segfault !!

	//TODO (unfinished) shortcuts that have 2 accel keys have 2 actions. We use the g_object_data to indicate that the 2nd action is not shown seperately the shortcuts window.

	//note: for items below which are existing stock items, the shortcuts are defined twice.
	//      If the stock_item is already used (eg on a menu), the shortcut defined here will not work.
	//      But if it isnt defined elsewhere, the shortcut defined here DOES work.
	//      Qu: can the user change stock_item shortcut keys?

	void
	nudge_left (GtkAccelGroup* accel_group, gpointer user_data, gpointer user_data2)
	{
		AyyiPanel* panel = windows__get_active();
		g_return_if_fail(panel);
		dbg(0, "type=%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));

		AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);
		GList* parts = klass->get_selected_parts(panel);
		if (parts) {
			GList* l = parts;
			for (;l;l=l->next) {
				AMPart* part = l->data;
				#define NUDGE_SNAP TRUE
				am_part__nudge_left(part, NUDGE_SNAP, &song->q_settings[0]);
			}
			g_list_free(parts);
		}
	}

	void
	nudge_right (GtkAccelGroup* accel_group, gpointer user_data, gpointer user_data2)
	{
		AyyiPanel* panel = windows__get_active();
		g_return_if_fail(panel);
		dbg(0, "type=%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));

		AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);
		GList* parts = klass->get_selected_parts(panel);
		if (parts) {
			GList* l = parts;
			for (;l;l=l->next) {
				am_part__nudge_right(((AMPart*)l->data), TRUE, &song->q_settings[0]);
			}
			g_list_free(parts);
		}
	}

	void
	on_song_save_activate (GtkAccelGroup* accel_group, gpointer user_data)
	{
		song_save();
	}

	void
	transport_locate (GtkAccelGroup* accel_group, gpointer unknown, gpointer accel_key)
	{
		// jump to a locator postion.

		int key = GPOINTER_TO_INT(accel_key);
		if (key > 0x30 && key < 0x3a) {
			am_transport_jump(&song->loc[key - 0x30].vals[0].val.sp);
		} else {
			switch (key) {
				case GDK_KP_1:
				case GDK_KP_End:
					am_transport_jump(&song->loc[1].vals[0].val.sp);
					break;
				case GDK_KP_2:
				case GDK_KP_Down:
					am_transport_jump(&song->loc[2].vals[0].val.sp);
					break;
				default:
					dbg (0, "unexpected key: %i", key);
					break;
			}
		}
	}

	void
	transport_loc_left_k (GtkAccelGroup* accel_group, gpointer user_data, gpointer accel_key)
	{
		// locate back a bit

		AyyiSongPos pos;
		am_transport_get_pos_(&pos);
		pos.beat = MAX(pos.beat - 4, 0);

		am_transport_jump(&pos);
	}


	void
	transport_loc_right_k (GtkAccelGroup* accel_group, gpointer user_data, gpointer accel_key)
	{
		// locate forwards a bit
		PF;

		AyyiSongPos pos;
		am_transport_get_pos_(&pos);
		pos.beat = MAX(pos.beat + 4, 0);

		am_transport_jump(&pos);
	}


	void
	kzoom_in_cb (GtkAccelGroup* accel_group, gpointer user_data)
	{
		PF;
		AyyiPanel* panel = app->active_panel;
		if (panel) {
			Ptf zoom = {panel->zoom->value.pt.x * 1.25, panel->zoom->value.pt.y * 1.25};
			observable_point_set(panel->zoom, &zoom.x, &zoom.y);
		}
	}


	void
	kzoom_out_cb (GtkAccelGroup* accel_group, gpointer user_data)
	{
		AyyiPanel* panel = app->active_panel;
		if (panel) {
			Ptf zoom = {panel->zoom->value.pt.x / 1.25, panel->zoom->value.pt.y / 1.25};
			observable_point_set(panel->zoom, &zoom.x, &zoom.y);
		}
	}


	void
	window_new_k (GtkAccelGroup* accel_group, gpointer unknown, int key)
	{
		AyyiPanelClass* class = panel_class_lookup_by_accel(key);
		if (class) {
			window__new_floating(class);
		} else {
			pwarn("!! class not found for accel: %x", key);
		}
	}

	AMAccel key[] = {
		{"Quit",           {{(char)'q',       GDK_CONTROL_MASK},  {0, 0}},               app_quit, NULL        },
		{"Quit (No Save)", {{(char)'q',       GDK_CONTROL_MASK|GDK_SHIFT_MASK}, {0, 0}}, app_quit, NULL,GINT_TO_POINTER(1)},
		{"Close",          {{(char)'w',       GDK_CONTROL_MASK},  {0, 0}}, close_active_panel,    NULL        },
		{"Save",           {{(char)'s',       GDK_CONTROL_MASK},  {0, 0}}, on_song_save_activate,  NULL        },
		{"Cut",            {{(char)'x',       GDK_CONTROL_MASK},  {0, 0}}, NULL},
		{"Select All",     {{(char)'a',       GDK_CONTROL_MASK},  {0, 0}}, k_select_all},
		{"Locate 1",       {{(char)'1',       0               },  {GDK_KP_1, 0}}, transport_locate, NULL, GINT_TO_POINTER(0)},
		//{"Locate 1 KP", &stock_items[3], {{GDK_KP_1,        0               },  {0, 0}}, transport_locate, GINT_TO_POINTER(0)},
		{"Locate 2",       {{(char)'2',       0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(1)},
		{"Locate 2 KP",    {{GDK_KP_2,        0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(1)},
		{"Locate 3 KP",    {{GDK_KP_3,        0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(2)},
		{"Locate 4 KP",    {{GDK_KP_4,        0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(3)},
		{"Locate End",     {{GDK_KP_End,      0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(0)},
		{"Locate Down",    {{GDK_KP_Down,     0               },  {0, 0}}, transport_locate, NULL, GINT_TO_POINTER(1)},
		{"Nudge Left",     {{(char)'[',       0               },  {0, 0}}, nudge_left,       NULL              },
		{"Nudge Right",    {{(char)']',       0               },  {0, 0}}, nudge_right,      NULL              },
		{"Locate Left",    {{GDK_leftarrow,   GDK_CONTROL_MASK},  {0, 0}}, transport_loc_left_k                },
		{"Locate Right",   {{GDK_rightarrow,  GDK_CONTROL_MASK},  {0, 0}}, transport_loc_right_k               },
		{"Zoom In",        {{0x3d/*(char)'='*/,0,             },  {0, 0}}, kzoom_in_cb,      NULL, NULL},
		{"Zoom Out",       {{(char)'-',       0,              },  {0, 0}}, kzoom_out_cb,     NULL, NULL},

		// windows
		// FIXME correct user_data is not getting to callback (when Arrange window is focused, this callback is not used!!).
		{"New Mixer",      {{GDK_F2,          0},  {0, 0}},                window_new_k,     NULL, NULL},
		{"New Transport",  {{GDK_F3,          0},  {0, 0}},                window_new_k,     NULL, NULL/*GINT_TO_POINTER(AYYI_TYPE_TRANSPORT_WIN)*/},
		{"New List",       {{GDK_F4,          0},  {0, 0}},                window_new_k,     NULL, NULL/*GINT_TO_POINTER(AYYI_TYPE_LISTWIN)*/},
		{"New Inspector",  {{GDK_F5,          0},  {0, 0}},                window_new_k,     NULL, NULL/*GINT_TO_POINTER(AYYI_TYPE_INSPECTOR_WIN)*/},
		{"New Event",      {{GDK_F6,          0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Palette",    {{GDK_F8,          0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Pool",       {{GDK_F7,          0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Log",        {{GDK_F9,          0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Plugin",     {{GDK_F10,         0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Midi",       {{GDK_F11,         0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Shortcuts",  {{GDK_F11,         0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Arrange",    {{GDK_F12,         0},  {0, 0}},                window_new_k,     NULL, NULL        },
		{"New Help",       {{GDK_F1,          0},  {0, 0}},                window_new_k,     NULL, NULL        },
	};

	// Transport has its own action group but shares the global accel group
	AMAccel transport_keys[] = {
		{"Play",           {{GDK_KP_Enter,    0},             {0, 0}},   ayyi_transport_play                 },
		{"Stop",           {{0xff9e,          0},             {0, 0}},   ayyi_transport_stop                 }, // keypad zero
		{"Stop 2",         {{GDK_space,       0},             {0, 0}},   ayyi_transport_stop                 },
		{"Rec",            {{GDK_KP_Multiply, 0},             {0, 0}},   ayyi_transport_rec                  },
		{"Cycle",          {{GDK_KP_Divide,   0},             {0, 0}},   am_transport_cycle_toggle           },
	};

	// load user-editable accels file:
	// warning ! the map file overrides the above hardcoded keys, and does work!
	// FIXME the shortcuts window shows the original key, even though this is not the active key.
	if (!g_file_test (config->accels_file, G_FILE_TEST_EXISTS)) log_print(0, "no accels_file found: %s", config->accels_file);
	gtk_accel_map_load(config->accels_file);
#if 0
	gtk_accel_map_foreach_unfiltered(NULL, accel_map_foreach);
#endif

#if 0
	GtkAccelKey accelkey;
	if(gtk_accel_map_lookup_entry("<Global>/Categ/Cut", &accelkey)) dbg(0, "found."); else pwarn("not found.");
#endif

	{
		stock_item_add("Close",       "Mystock Label", &key[ 2], "cross",    NULL);
		stock_item_add("Save",        "Mystock Label", &key[ 3], NULL,       (guchar*)GTK_STOCK_SAVE);
		stock_item_add("Mixer",       "Mystock Label", &key[13], "mixer",    NULL);
		stock_item_add("Transport",   "Mystock Label", &key[14], "tr-play",  NULL);
		stock_item_add("List",        "Mystock Label", &key[15], "list",     NULL);
		stock_item_add("Inspector",   "Mystock Label", &key[16], "mixer",    NULL);
		stock_item_add("Event",       "Mystock Label", &key[17], "event",    NULL);
		stock_item_add("Colour",      "Mystock Label", &key[18], NULL, (guchar*)GTK_STOCK_SELECT_COLOR);
		stock_item_add("Pool",        "Mystock Label", &key[19], "pointer",  NULL);
		stock_item_add("Log",         "Mystock Label", &key[20], "pointer",  NULL);
		stock_item_add("Plugin",      "Mystock Label", &key[21], "plugin",   NULL);
		stock_item_add("Midi",        "Mystock Label", &key[22], "crotchet", NULL);
		stock_item_add("Shortcuts",   "Mystock Label", &key[23], "pointer",  NULL);
		stock_item_add("Spectrogram", "Mystock Label", &key[23], "pointer",  NULL);
		stock_item_add("Filemanager", "Mystock Label", &key[23], "pointer",  NULL);
		stock_item_add("Audio",       "Mystock Label", &key[23], "audio",    NULL);
		stock_item_add("Arrange",     "Mystock Label", &key[24], "arrange",  NULL);
		stock_item_add("Meter",       "Mystock Label", &key[23], "meter",    NULL);
		stock_item_add("Locate 1",    "Mystock Label", &key[ 4], NULL,       (guchar*)GTK_STOCK_JUMP_TO);
		stock_item_add("Locate 2",    "Mystock Label", &key[ 5], NULL,       (guchar*)GTK_STOCK_JUMP_TO);
		stock_item_add("Locate 2 KP", "Mystock Label", &key[ 6], NULL,       (guchar*)GTK_STOCK_JUMP_TO);
		stock_item_add("Locate End",  "Mystock Label", &key[ 7], NULL,       (guchar*)GTK_STOCK_JUMP_TO);
		stock_item_add("Locate Down", "Mystock Label", &key[ 8], NULL,       (guchar*)GTK_STOCK_JUMP_TO);
		stock_item_add("Locate Left", "Mystock Label", &key[11], NULL,       (guchar*)GTK_STOCK_GO_BACK);
		stock_item_add("Locate Right","Mystock Label", &key[12], NULL,       (guchar*)GTK_STOCK_GO_FORWARD);

		stock_item_add("play",        "Mystock Label", &transport_keys[0], "tr_play", NULL);
		stock_item_add("stop",        "Mystock Label", &transport_keys[1], "tr_stop", NULL);
		stock_item_add("cycle",       "Mystock Label", &transport_keys[4], "refresh", NULL);

		gtk_stock_add_static(&new_stock_items[0], 9); //warning ! this must match the number of stock items created.
	}

	// if this is declared before key[], we get a segfault.
	//GtkStockItem (*stock_items)[10] = (GtkStockItem (*)[10])g_new0(GtkStockItem, 10);//allocate array of 10 stock_items.

	GtkStockItem stock_quit; gtk_stock_lookup(GTK_STOCK_QUIT, &stock_quit); key[0].stock_item = &stock_quit;
	key[1].stock_item = &stock_quit;
	//GtkStockItem stock_save; gtk_stock_lookup(GTK_STOCK_SAVE, &stock_save); key[2].stock_item = &stock_save;
	GtkStockItem stock_cut; gtk_stock_lookup(GTK_STOCK_CUT, &stock_cut); key[4].stock_item = &stock_cut;

	make_accels(app->global_accel_group, NULL, key, G_N_ELEMENTS(key), NULL);
	make_accels(app->global_accel_group, shortcuts_get_group("Transport"), transport_keys, G_N_ELEMENTS(transport_keys), NULL);

	// remove gtk trapping of F10 key
	GtkSettings* gtk_settings = gtk_settings_get_for_screen(gdk_screen_get_default());
	g_object_set(gtk_settings, "gtk-menu-bar-accel", NULL, NULL);
}


/*
 *  Add keyboard shortcuts from the key array to the accelerator group.
 *
 *  @param action_group - if NULL, global group is used.
 */
void
make_accels (GtkAccelGroup* accel_group, GimpActionGroup* action_group, AMAccel* keys, int count, gpointer user_dataX)
{
	g_return_if_fail(accel_group);
	g_return_if_fail(keys);

	void add(GtkAccelGroup* accel_group, GimpActionGroup* action_group, char* name, char* postfix, GtkStockItem* stock_item, AMKey* k, gpointer callback, gpointer user_data)
	{
		GClosure* closure = NULL;
		if (callback) {
			closure = g_cclosure_new(G_CALLBACK(callback), user_data, NULL);
			if (k->code) {
				gtk_accel_group_connect(accel_group, k->code, k->mask, GTK_ACCEL_MASK, closure);
			}
		}

		gchar path[64]; sprintf(path, "<%s>/Categ/%s%s", action_group ? gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)) : "Global", name, postfix);
		if (closure) gtk_accel_group_connect_by_path(accel_group, path, closure);

		gtk_accel_map_add_entry(path, k->code, k->mask);

		dbg(2, "path=%s stock_item=%p stock-id=%s", path, stock_item, stock_item? stock_item->stock_id : "gtk-file");
		char label[32] = {0,}; snprintf(label, 32, "%s%s", name, postfix);
		GtkAction* action = gtk_action_new(label, name, "Tooltip", stock_item ? stock_item->stock_id : "gtk-file");
		gtk_action_set_accel_path(action, path);
		gtk_action_set_accel_group(action, accel_group);
		shortcuts_add_action(action, action_group);
	}

	int k;
	for (k=0;k<count;k++) {
		AMAccel* key = &keys[k];

		add(accel_group, action_group, key->name, "", key->stock_item, &key->key[0], key->callback, key->user_data);

		if(key->key[1].code){
			add(accel_group, action_group, key->name, "-ALT", key->stock_item, &key->key[1], key->callback, keys[k].user_data);
		}
	}
}


void
accels_connect (GtkWidget* widget)
{
	PF;
	GtkWindow* window = GTK_WINDOW(gtk_widget_get_toplevel(widget));
	AyyiWindow* w = (AyyiWindow*)window;

	gtk_window_add_accel_group(window, app->global_accel_group);

	if (w->focussed_panel) {
		GtkAccelGroup* accel_group = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(w->focussed_panel))->accel_group;
		if (accel_group) {
			gtk_window_add_accel_group(window, accel_group);
		}

		GList* l = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(w->focussed_panel))->accels;
		for (;l;l=l->next) {
			GtkAccelGroup* g = l->data;
			dbg(0, "adding gl accel group: %p", g);
			gtk_window_add_accel_group(window, g);
		}
	}
}


void
accels_disconnect (GtkWidget* widget)
{
	// Normal keyboard shortcuts need to be disabled when we are "editing".

	GtkWindow* window = GTK_WINDOW(gtk_widget_get_toplevel(widget));
	AyyiWindow* w = (AyyiWindow*)window;

	GSList* connected_accels = gtk_accel_groups_from_object(G_OBJECT(window));
	if (connected_accels) {
		// the GSList should not be freed - it seems to be ref-counted instead.
		gtk_window_remove_accel_group(window, app->global_accel_group);
	}
	else perr ("global accels not connected.");

	if (w->focussed_panel) {
		GtkAccelGroup* accel_group = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(w->focussed_panel))->accel_group;
		if (accel_group) {
			gtk_window_remove_accel_group(window, accel_group);
		}

		GList* l = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(w->focussed_panel))->accels;
		for (;l;l=l->next) {
			GtkAccelGroup* g = l->data;
			gtk_window_remove_accel_group(window, g);
		}
	}
	else pwarn ("failed to disconnect panel accels.");
}


static int prev_keycode = 0;

int
keypress_get_repeat_count (int keycode)
{
	static int count = 0;

	if (keycode == prev_keycode) {
		prev_keycode = keycode;
		return ++count;
	}
	prev_keycode = keycode;
	count = 0;
	return count;
}


void
keypress_reset ()
{
	prev_keycode = 0;
}


gboolean
stop_propogation (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
{
	return HANDLED;
}


static void
k_select_all (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = windows__get_active();
	if (panel) {
		AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel));
		if (panel_class->select_all) panel_class->select_all(panel);
	}
}


void
report_time (struct timeval* time_ref)
{
	struct timeval time_stop;
	gettimeofday(&time_stop, NULL);
	time_t      secs = time_stop.tv_sec - time_ref->tv_sec;
	suseconds_t usec;
	if (time_stop.tv_usec > time_ref->tv_usec) {
		usec = time_stop.tv_usec - time_ref->tv_usec;
	} else {
		secs -= 1;
		usec = time_stop.tv_usec + 1000000 - time_ref->tv_usec;
	}
	printf("%s(): %lu:%06lu\n", __func__, secs, usec);
}


/*
 *   method_string is allocated and must be free'd by caller
 */
const gchar *
vfs_get_method_string (const gchar *substring, gchar **method_string)
{
	const gchar *p;
	char *method;
	
	for (p = substring;
	     g_ascii_isalnum (*p) || *p == '+' || *p == '-' || *p == '.';
	     p++)
		;

	if (*p == ':') {
		/* Found toplevel method specification.  */
		method = g_strndup (substring, p - substring);
		*method_string = g_ascii_strdown (method, -1);
		g_free (method);
		p++;
	} else {
		*method_string = g_strdup ("file");
		p = substring;
	}
	return p;
}


/*  Convert a list of URIs as a string into a GList of EscapedPath URIs.
 *  No unescaping is done.
 *  Lines beginning with # are skipped.
 *  The text block passed in is zero terminated (after the final CRLF)
 *
 *  The returned list must be freed by the user
 */
GList*
uri_list_to_glist (const char* uri_list)
{
	GList* list = NULL;

	while (*uri_list) {
		int length;
		char* linebreak;
		const char* next;

		if ((linebreak = strchr(uri_list, 13))) {
			if (linebreak[1] != 10){
				errprintf ("%s: Incorrect line break in text/uri-list data", __func__);
				return list;
			}

			length = linebreak - uri_list;
			next = linebreak + 2;
		} else {
			if (list){ errprintf ("%s: missing line break in text/uri-list data: %s", __func__, uri_list); return list; }
			// ignore error if first line
			length = strlen(uri_list);
			next = uri_list + length;
		}

		if (length && uri_list[0] != '#')
			list = g_list_append(list, g_strndup(uri_list, length));

		uri_list = next;
	}

	return list;
}


void
uri_list_free (GList* list)
{
	GList* l = list;
	for (;l;l=l->next) {
		if (l->data) g_free(l->data);
	}
	g_list_free(list);
}


/**
 * gimp_strip_uline:
 * @str: underline infested string (or %NULL)
 *
 * This function returns a copy of @str stripped of underline
 * characters. This comes in handy when needing to strip mnemonics
 * from menu paths etc.
 *
 * In some languages, mnemonics are handled by adding the mnemonic
 * character in brackets (like "File (_F)"). This function recognizes
 * this construct and removes the whole bracket construction to get
 * rid of the mnemonic (see bug #157561).
 *
 * Return value: A (possibly stripped) copy of @str which should be
 *               freed using g_free() when it is not needed any longer.
 **/
gchar*
gimp_strip_uline (const gchar* str)
{
  gchar    *escaped;
  gchar    *p;
  gboolean  past_bracket = FALSE;

  if (! str) return NULL;

  p = escaped = g_strdup (str);

  while (*str)
    {
      if (*str == '_')
        {
          /*  "__" means a literal "_" in the menu path  */
          if (str[1] == '_')
            {
             *p++ = *str++;
             *p++ = *str++;
            }

          /*  find the "(_X)" construct and remove it entirely  */
          if (past_bracket && str[1] && *(g_utf8_next_char (str + 1)) == ')')
            {
              str = g_utf8_next_char (str + 1) + 1;
              p--;
            }
          else
            {
              str++;
            }
        }
      else
        {
          past_bracket = (*str == '(');

          *p++ = *str++;
        }
    }

  *p = '\0';

  return escaped;
}

/*  The format string which is used to display modifier names
 *  <Shift>, <Ctrl> and <Alt>
 */
#define GIMP_MOD_NAME_FORMAT_STRING "<%s>"

static const gchar*
get_mod_name_shift ()
{
	static gchar *mod_name_shift = NULL;

	if (!mod_name_shift) {
		GtkAccelLabelClass* accel_label_class = g_type_class_ref (GTK_TYPE_ACCEL_LABEL);
		mod_name_shift = g_strdup_printf (GIMP_MOD_NAME_FORMAT_STRING, accel_label_class->mod_name_shift);
		g_type_class_unref (accel_label_class);
	}

	return (const gchar *) mod_name_shift;
}


static const gchar*
get_mod_name_control ()
{
	static gchar* mod_name_control = NULL;

	if (! mod_name_control) {
		GtkAccelLabelClass* accel_label_class = g_type_class_ref (GTK_TYPE_ACCEL_LABEL);
		mod_name_control = g_strdup_printf(GIMP_MOD_NAME_FORMAT_STRING, accel_label_class->mod_name_control);
		g_type_class_unref (accel_label_class);
	}

	return (const gchar*)mod_name_control;
}


static const gchar*
get_mod_name_alt ()
{
  static gchar *mod_name_alt = NULL;

  if (! mod_name_alt)
    {
      GtkAccelLabelClass *accel_label_class = g_type_class_ref (GTK_TYPE_ACCEL_LABEL);
      mod_name_alt = g_strdup_printf (GIMP_MOD_NAME_FORMAT_STRING, accel_label_class->mod_name_alt);
      g_type_class_unref (accel_label_class);
    }

  return (const gchar *) mod_name_alt;
}


static const gchar*
get_mod_separator ()
{
  static gchar *mod_separator = NULL;

  if (! mod_separator) {
      GtkAccelLabelClass* accel_label_class = g_type_class_ref (GTK_TYPE_ACCEL_LABEL);
      mod_separator = g_strdup (accel_label_class->mod_separator);
      g_type_class_unref (accel_label_class);
    }

  return (const gchar *) mod_separator;
}


static const gchar*
get_mod_string (GdkModifierType modifiers)
{
  static struct
  {
    GdkModifierType  modifiers;
    gchar           *name;
  }
  modifier_strings[] =
  {
    { GDK_SHIFT_MASK,                                    NULL },
    { GDK_CONTROL_MASK,                                  NULL },
    { GDK_MOD1_MASK,                                     NULL },
    { GDK_SHIFT_MASK | GDK_CONTROL_MASK,                 NULL },
    { GDK_SHIFT_MASK | GDK_MOD1_MASK,                    NULL },
    { GDK_CONTROL_MASK | GDK_MOD1_MASK,                  NULL },
    { GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK, NULL }
  };

  gint i;

  for (i = 0; i < G_N_ELEMENTS (modifier_strings); i++)
    {
      if (modifiers == modifier_strings[i].modifiers)
        {
          if (! modifier_strings[i].name)
            {
              GString *str = g_string_new ("");

              if (modifiers & GDK_SHIFT_MASK)
                {
                  g_string_append (str, get_mod_name_shift ());
                }

              if (modifiers & GDK_CONTROL_MASK)
                {
                  if (str->len)
                    g_string_append (str, get_mod_separator ());

                  g_string_append (str, get_mod_name_control ());
                }
              if (modifiers & GDK_MOD1_MASK)
                {
                  if (str->len)
                    g_string_append (str, get_mod_separator ());

                  g_string_append (str, get_mod_name_alt ());
                }

              modifier_strings[i].name = g_string_free (str, FALSE);
            }

          return modifier_strings[i].name;
        }
    }

  return NULL;
}


static void
gimp_substitute_underscores (gchar *str)
{
	gchar* p;

	for (p = str; *p; p++)
		if (*p == '_')
			*p = ' ';
}


gchar*
gimp_get_accel_string (guint key, GdkModifierType modifiers)
{
	gunichar ch;

	GtkAccelLabelClass* accel_label_class = g_type_class_peek (GTK_TYPE_ACCEL_LABEL);

	GString* gstring = g_string_new (get_mod_string (modifiers));

	if (gstring->len > 0)
		g_string_append (gstring, get_mod_separator ());

	ch = gdk_keyval_to_unicode (key);

	if (ch && (g_unichar_isgraph (ch) || ch == ' ') && (ch < 0x80 || accel_label_class->latin1_to_char)) {
		switch (ch) {
			case ' ':
				g_string_append (gstring, "Space");
				break;
			case '\\':
				g_string_append (gstring, "Backslash");
				break;
			default:
				g_string_append_unichar (gstring, g_unichar_toupper (ch));
				break;
		}
	} else {
		gchar* tmp = gtk_accelerator_name (key, 0);

		if (tmp[0] != 0 && tmp[1] == 0)
			tmp[0] = g_ascii_toupper (tmp[0]);

		gimp_substitute_underscores (tmp);
		g_string_append (gstring, tmp);
		g_free (tmp);
	}

	return g_string_free (gstring, FALSE);
}


gboolean
note_is_black (int note)
{
	static int black[] = {1, 3, 6, 8, 10};

	note = note % 12;

	int i; for(i=0;i<5;i++){
		if(note == black[i]) return true;
	}
	return false;
}


int
note_get_white_num (int n)
{
	static int white[] = {0, -1, 1, -1, 2, /*f*/3, -1, 4, -1, /*a*/5, -1, 6};
	return white[n % 12];
}


int
note_get_black_num (int n)
{
	static int black[] = {-1, 1, -1, /*d#*/2, -1, /*f*/-1, 3, -1, 4, /*a*/-1, 5, -1};
	return black[n % 12];
}


const char*
note_format (int note)
{
	static const char format[][4] = {"C", "C#", "D", "Eb", "E", "F", "F#", "G", "Ab", "A", "Bb", "B"};
	return format[note % 12];
}


int
note_get_octave_num (int note)
{
	return note / 12 - 1;
}


/*
 *  Find @old as a child of @box and replace it with @new at the same position.
 */
void
gtk_box_replace (GtkWidget* container, GtkWidget* old, GtkWidget* new)
{
	GtkBox* box = GTK_BOX(container);

	int position = 0;
	bool found = false;
	GList* l = box->children;
	for (;l;l=l->next) {
		if (((GtkBoxChild*)l->data)->widget == old) {
			found = true;
			break;
		}
		position++;
	}
	if(!found){ pwarn("not found"); return; }

	gtk_widget_destroy(old);

	gtk_box_pack_start(box, new, EXPAND_FALSE, FALSE, NO_PADDING);
	gtk_box_reorder_child(box, new, position);
	gtk_widget_show(new);
}


void
show_widget_if (GtkWidget* widget, gboolean show)
{
	if(show) gtk_widget_show(widget);
	else gtk_widget_hide(widget);
}


gboolean
widget_is_in_paned (GtkWidget* widget)
{
	int i = 0;
	GtkWidget* parent = widget;//gtk_widget_get_parent(widget);
	while ((parent = gtk_widget_get_parent(parent))) {
  		if (G_OBJECT_TYPE(parent) == GTK_TYPE_VPANED || G_OBJECT_TYPE(parent) == GTK_TYPE_HPANED) {
    		dbg(2, "is in pane");
			return true;
		}
		if (i++ > 20){ printf("!!\n"); break; }
	}
	return false;
}


GtkWidget*
spacer_new (GtkBox* container, int height)
{
	char path[256];
	snprintf(path, 255, "%s/../1px.png", config->svgdir);

	GtkWidget* spacer_fixed = gtk_fixed_new();
	GtkWidget* spacer_image = gtk_image_new_from_file(path);
	gtk_fixed_put (GTK_FIXED(spacer_fixed), spacer_image, 0, height);
	gtk_box_pack_start(container, spacer_fixed, EXPAND_FALSE, NO_PADDING, 0);

	return spacer_fixed;
}


const char*
get_orientation_string (GtkOrientation i)
{
	static char* strings[] = {"horizontal", "vertical", "error"};
	return strings[MIN(i, 2)];
}


void
print_widget (GtkWidget* widget)
{
	void _print_widget(GtkWidget* widget, int* depth)
	{
		char indent[128];
		snprintf(indent, 127, "%%%is%%s %%s %%i x %%i\n", *depth * 3);
		GtkWidget* item = widget;
		printf(indent, " ", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(item)), gtk_widget_get_name(item), item->allocation.width, item->allocation.height);
	}
	int depth = 0;
	printf("  parent:");
	_print_widget(widget->parent, &depth);
	depth++;
	_print_widget(widget, &depth);
}


void
print_widget_tree (GtkWidget* widget)
{
	UNDERLINE;

	void print_children (GtkWidget* widget, int* depth)
	{
		if(GTK_IS_CONTAINER(widget)){
			GList* children = gtk_container_get_children(GTK_CONTAINER(widget));
			GList* l = children;
			for(;l;l=l->next){
				GtkWidget* child = l->data;
				char indent[128];
				snprintf(indent, 127, "%%%is%%s %%s %%i x %%i\n", *depth * 3);
				printf(indent, " ", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(child)), gtk_widget_get_name(child), child->allocation.width, child->allocation.height);
				if(GTK_IS_CONTAINER(widget)){
					(*depth)++;
					print_children(child, depth);
					(*depth)--;
				}
			}
			g_list_free(children);
		}
	}

	int depth = 0;
	if(GTK_IS_CONTAINER(widget)){
		GList* children = gtk_container_get_children(GTK_CONTAINER(widget));
		if(children){
			print_children(widget, &depth);
			g_list_free(children);
		}
		else dbg(0, "is empty container: %s %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(widget)), gtk_widget_get_name(widget));
	}
	else dbg(0, "widget has no children. %i x %i %i", widget->allocation.width, widget->allocation.height, gtk_widget_get_visible(widget));

	UNDERLINE;
}


/*
 *  CTL-W handler
 */
static void
close_active_panel (GtkAccelGroup* not_used, gpointer not_used_)
{
	if(g_list_length(windows) == 1 && g_list_length(((AyyiWindow*)windows->data)->dock->panels) <= 1) return; // dont close last window.

	AyyiPanel* panel = windows__get_active();
	g_return_if_fail(panel);
	windows__verify_pointer(panel, 0);

#ifdef DEBUG
	windows__print_items();
#endif
	gdl_dock_print_recursive(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(panel)->master));

	// remove a single panel

	dbg(0, "closing panel: '%s' ...", GDL_DOCK_OBJECT(panel)->name);
	window__destroy_panel(panel->window, panel);

	windows__print();
}


/*
 *  Currently, icon can be either a theme icon name, or a stock_id
 */
GtkWidget*
add_menu_items_from_defn (GtkWidget* menu, MenuDef* menu_def, int n, gpointer user_data)
{
	for(int i=0;i<n;i++){
		MenuDef* item = &menu_def[i];
		if(!item->label[0] == '\0'){
			GtkWidget* menu_item = gtk_image_menu_item_new_with_label (item->label);
			gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
			if(item->icon){
				GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, item->icon, 16, 0, NULL);
				if(!pixbuf){
					GtkIconSet* set = gtk_style_lookup_icon_set(gtk_widget_get_style(menu), item->icon);
					pixbuf = gtk_icon_set_render_icon(set, gtk_widget_get_style(menu), GTK_TEXT_DIR_LTR, GTK_STATE_NORMAL, GTK_ICON_SIZE_MENU, menu, NULL);
				}

				GtkWidget* ico = gtk_image_new_from_pixbuf(pixbuf);
				gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(menu_item), ico);
			}
			if(item->callback) g_signal_connect (G_OBJECT(menu_item), "activate", G_CALLBACK(item->callback), user_data);
			if(!item->sensitive) gtk_widget_set_sensitive(GTK_WIDGET(menu_item), false);
		}else{
			GtkWidget* menu_item = gtk_separator_menu_item_new();
			gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
		}
	}

	gtk_widget_show_all (menu);

	return menu;
}


/*
 *  Convenience fn to disconnect a handler by data only. Will warn if exactly n handlers was not disconnected.
 */
void
signal_handler_disconnect_data (gpointer instance, gpointer data, int n_expected, const char* fail_message)
{
	int n;
	if((n = g_signal_handlers_disconnect_matched (instance, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, data)) != n_expected) pwarn("handler disconnection n_disconnected=%i %s", n, fail_message);
}


/*
 *  Find the index of an item in a null terminated array
 */
int
array_index (void** _array, void* item, int item_size)
{
	char** array = (char**)_array;

	int i; for(i=0;array[i*item_size];i++){
		if(&array[i*item_size/sizeof(char*)] == item) return i;
	}
	return -1;
}


/*
 *  Return true if the contents of l1 are different to the contents of l2
 */
bool
list_cmp (GList* l1, GList* l2)
{
	for(; l1 && l2; l1=l1->next,l2=l2->next){
		if(l1->data != l2->data) return true;
	}
	if(l1 || l2) return true;
	return false;
}
