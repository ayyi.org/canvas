/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifdef USE_GNOMECANVAS
#include <libgnomecanvas/libgnomecanvas.h>
#endif

struct _ArrAutoPath
{
    Curve*            curve;
    bool              hidden;
#ifdef USE_GNOMECANVAS
    GnomeCanvasItem*  pathitem;
#endif
};


struct _ArrTrack
{
    // per-window track data.

    Arrange*          arrange;         // parent window.
    AMTrack*          track;
#ifdef DEBUG
    AyyiIdx           shm_idx;         // for checking track pointer.
#endif

    int               y;               // pixels, non-zoomed.
    int               height;          // pixels, non-zoomed.

    GtkWidget*        trk_ctl;         // custom hbox widget holding all track labels and buttons.

    GtkWidget*        meter;

#ifdef USE_GNOMECANVAS
    GnomeCanvasGroup* grp_auto;        // canvas group to hold all the automation curves for this track.
#else
    void*             grp_auto;
#endif
    GPtrArray*        auto_paths;      // array of type ArrAutoPath*

    gulong            tname_changed;   // signal handler id.

    float             peak;            // slowly falling peak level for metering.
};


struct _ArrTrackMidi
{
    ArrTrk            track;

    int               centre_note;     // midi note currently displayed in the middle of the track.
    int               note_height;     // pixels, zoomed - note_height does not scale with track height.
    int               velocity_chart_height;

    int               hover_note;
};

typedef union {
    ArrTrk a;
    ArrTrackMidi m;
} ArrTrackU;

void         arr_track_pos_update           (Arrange*);
void         arr_track_undraw               (Arrange*, ArrTrk*);
bool         arr_track_select               (Arrange*, AMTrack*);
void         arr_track_select_nearest       (Arrange*, TrackDispNum);
bool         arr_track_is_selected          (Arrange*, AMTrack*);
void         arr_track_on_selection_change  (Arrange*);
bool         arr_track_move_to_pos          (Arrange*, TrackDispNum, TrackDispNum new_pos);

void         arr_trkctl_load                (Arrange*);
void         arr_trkctl_redraw              (Arrange*);
void         arr_trkctl_enable              (Arrange*);

void         arr_track_menu_show            (Arrange*, TrackNum, GdkEventButton*);

void         arr_observer__midi_key_scroll  (TrackControlMidi*);

void         arr_track_set_height           (Arrange*, AMTrack*, int height);
int          arr_track_min_height           (ArrTrk*);

GtkWidget*   arr_track_menu_init            (Arrange*);
void         arr_track_ch_menu_update       (Arrange*);

int          arr_track_midi_get_n_notes     (ArrTrackMidi*);
double       arr_track_note_pos_y           (ArrTrk*, MidiNote*);
int          arr_track_get_note_num_from_y  (ArrTrackMidi*, double y);
int          arr_track_get_velocity_from_y  (ArrTrackMidi*, double y);

int          arr_track_count_active_controls(ArrTrk*);
AMiRange     arr_track_get_automation_range (ArrTrk*);
ArrAutoPath* arr_track_find_aap             (ArrTrk*, AMCurve*);

#ifdef __arrange_c__
static void  arr_track_undraw_all           (Arrange*);
#endif

void         tracks_print                   ();

#define AUTO_PATH(I) (atr->auto_paths->len > I ? (ArrAutoPath*)atr->auto_paths->pdata[I] : NULL)


