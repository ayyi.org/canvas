/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#define SAVE_C
#include <sys/stat.h>
#include <math.h>

#include "utils/fs.h"
#include <model/time.h>
#include "model/part_manager.h"
#include "model/track_list.h"
#include "model/channel.h"
#include "windows.h"
#include "support.h"
#include "toolbar.h"
#include "song.h"
#include "part_manager.h"
#include "panels/arrange.h"
#include "panels/mixer.h"
#ifdef USE_GNOMECANVAS
#include "arrange/gnome/part_item.h"
#endif
#ifdef HAVE_YAML_H
#include "yaml/save.h"
#endif
#include "save.h"

#define POINTER_OK_NULL2(A, B) if((unsigned)A < 1024){ perr("bad %s pointer (%p)", A, B); return NULL; }

extern SMConfig* config;
extern int       debug;
extern char*     window_type_strings[];
void             observer__songmap_change();

#ifdef HAVE_YAML_H
extern unsigned char* str_tag;
extern unsigned char* map_tag;
extern yaml_emitter_t emitter;
#endif


static void
config_load_files_yaml (yaml_parser_t* parser, const char* key, gpointer data)
{
	YamlValueHandler handlers[] = {
		{NULL}
	};
	yaml_load_mapping(parser, NULL, handlers, NULL);
}


typedef struct _id_part
{
	uint64_t id;
	AMPart   part;
} IdPart;

static void
config_load_tracks_yaml (yaml_parser_t* parser, const char* key, gpointer data)
{
	// currently only sets the colour.

	void
	config_load_track_yaml (yaml_parser_t* parser, const char* key, gpointer data)
	{
		AyyiTrack* track = g_new0(AyyiTrack, 1);
		YamlValueHandler handlers[] = {
			{"id", yaml_set_uint64, &track->id},
			{"colour", yaml_set_int, &track->colour},
			{NULL, NULL}
		};

		yaml_load_mapping(parser, NULL, handlers, NULL);

		GList** d = data;
		*d = g_list_append(*d, track);
	}

	GList* config_tracks = NULL;
	YamlHandler child_handlers[] = {
		{"", config_load_track_yaml, &config_tracks},
		{NULL, NULL}
	};

	yaml_load_mapping(parser, child_handlers, NULL, NULL);

	GList* l = config_tracks;
	for(;l;l=l->next){
		AyyiTrack* t = l->data;
		if(t->id && (t->colour > -1)){
			AyyiTrack* ayyi_track = ayyi_song__track_lookup_by_id(t->id);
			if(ayyi_track){
				AMTrackType tp = (ayyi_track->flags & master) ? TRK_TYPE_BUS : TRK_TYPE_AUDIO;
				AMTrack* track = am_track_list_find_by_shm_idx(song->tracks, ayyi_track->shm_idx, tp);
				if(track){
					am_track__set_colour(track, t->colour, NULL, NULL);
				}
				else pwarn("track not found. idx=%i id=%"PRIu64, ayyi_track->shm_idx, t->id);
			}
			else if(_debug_) pwarn("saved track not found in current song. %"PRIu64, t->id);
		}

		g_free(t);
	}
	g_list_free(config_tracks);
}


static void
config_load_parts_yaml (yaml_parser_t* parser, const char* key, gpointer data)
{
	// currently only sets the Part colour.

	void config_load_part_yaml (yaml_parser_t* parser, const char* key, gpointer data)
	{
		char* name = NULL;
		IdPart* part = g_new0(IdPart, 1);
		YamlValueHandler handlers[] = {
			{"id", yaml_set_uint64, &part->id},
			{"colour", yaml_set_int, &part->part.bg_colour},
			{"name", yaml_set_string, &name},
			{NULL, NULL}
		};

		yaml_load_mapping(parser, NULL, handlers, NULL);

		strncpy(part->part.name, name, 63);
		g_free(name);

		GList** d = data;
		*d = g_list_append(*d, part);
	}

	GList* config_parts = NULL;
	YamlHandler handlers[] = {
		{"", config_load_part_yaml, &config_parts},
		{NULL, NULL}
	};

	yaml_load_mapping(parser, handlers, NULL, NULL);

	GList* l = config_parts;
	for(;l;l=l->next){
		IdPart* p = l->data;
		if(p->id && p->part.bg_colour > -1){
			AMPart* part = am_partmanager__get_by_id(p->id);
			if(part){
				dbg (2, "setting: part=%s colour=%i", part->name, p->part.bg_colour);
				part->bg_colour = p->part.bg_colour;
				part->fg_colour = am_palette_get_contrasting(song->palette, part->bg_colour);
			} else {
				if(_debug_) pwarn ("part not found in song. Was it deleted? id=%"PRIu64" '%s'", p->id, p->part.name ? p->part.name : "");
			}
		}
		g_free(p);
	}
	g_list_free(config_parts);
}


static void
config_load_songmap_yaml (yaml_parser_t* parser, const char* key, gpointer data)
{
	void
	config_load_map_part_yaml (yaml_parser_t* parser, const char* key, gpointer data)
	{
		char* name = NULL;
		AMPart* part = am_part_new();
		GPos length; songpos_ayyi2gui(&length, &part->length);
		YamlValueHandler handlers[] = {
			{"position", yaml_set_position, &part->start},
			{"length", yaml_set_position, &length},
			{"colour", yaml_set_int, &part->bg_colour},
			{"name", yaml_set_string, &name},
			{NULL, NULL}
		};

		yaml_load_mapping(parser, NULL, handlers, NULL);

		strncpy(part->name, name, 63);
		g_free(name);

		song->map_parts->list = g_list_append(song->map_parts->list, part);
	}

	YamlHandler handlers[] = {
		{"", config_load_map_part_yaml, NULL},
		{NULL}
	};

	am_song__clear_songmap();

	yaml_load_mapping(parser, handlers, NULL, NULL);

	am_song__emit("songmap-change"); //FIXME shouldnt be emitting this here
}


static void
config_load_panels_yaml (yaml_parser_t* parser, const char* key, gpointer data)
{
	void config_load_panel_yaml (yaml_parser_t* parser, const char* key, gpointer data)
	{
		PanelType panel_type = windows__panel_type_from_str(key);
		AyyiPanelClass* k = g_type_class_peek(panel_type);
		if (k->loader.handlers) {
			yaml_load_section2(parser,
				k->loader.handlers,
				k->loader.vhandlers
			);
		} else {
			yaml_load_mapping(parser, NULL, NULL, NULL);
		}
	}

	yaml_load_mapping(parser,
		(YamlHandler[]){
			{"", config_load_panel_yaml,},
			{NULL,}
		},
		NULL,
		NULL
	);
}


#ifdef HAVE_YAML_H
bool
load_gui_song_file (const char* filename)
{
	if(!g_file_test(filename, G_FILE_TEST_EXISTS)){ if(_debug_) log_print(0, "no gui data file for this song."); return FALSE; }

	bool _load_gui_song_file_yaml (FILE* fp, gpointer _)
	{
		AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_ARRANGE);
		k->loader.instances = 0;

		YamlHandler handlers[] = {
			{"files", config_load_files_yaml},
			{"tracks", config_load_tracks_yaml},
			{"parts", config_load_parts_yaml},
			{"songmap", config_load_songmap_yaml},
			{"panels", config_load_panels_yaml},
			{NULL, NULL}
		};
		dbg(2, "handlers[0]=%p %p", handlers[0], handlers[0].key);

		return yaml_load(fp, handlers);
	}

	return with_fp(filename, "rb", _load_gui_song_file_yaml, NULL);
}


bool
save_gui_song_file (const char* filename)
{
	//TODO not sure if this needs to be re-initialised. Probably.
	if(!yaml_emitter_initialize(&emitter)){ perr("failed to initialise yaml writer."); return FALSE; }

	char yaml_filename[256]; snprintf(yaml_filename, 255, "%s.yaml", filename);
	FILE* fp = fopen(yaml_filename, "wb");
	if(!fp){
		log_print(LOG_FAIL, "cannot open config file for writing (%s).", filename);
		return FALSE;
	}
	yaml_emitter_set_output_file(&emitter, fp);
	yaml_emitter_set_canonical(&emitter, FALSE); //needed?

	//stream start
	yaml_event_t event;
	yaml_stream_start_event_initialize(&event, YAML_UTF8_ENCODING);
	if(!yaml_emitter_emit(&emitter, &event)) goto error;
	//document start
	if (!yaml_document_start_event_initialize(&event, NULL, NULL, NULL, 0)) goto error;
	yaml_emitter_emit(&emitter, &event);

	if(!yaml_mapping_start_event_initialize(&event, NULL, (guchar*)"tag:yaml.org,2002:map", 1, YAML_BLOCK_MAPPING_STYLE)) goto error;
	if(!yaml_emitter_emit(&emitter, &event)) goto error;

	//---------------------------------------------------------

	if(!yaml_add_key_value_pair_float("bpm", ((AyyiSongService*)ayyi.service)->song->bpm)) goto error;
	if(!yaml_add_key_value_pair_int("sample_rate", ((AyyiSongService*)ayyi.service)->song->sample_rate)) goto error;
	if(!yaml_add_key_value_pair_int("transport_frame", ((AyyiSongService*)ayyi.service)->song->transport_frame)) goto error;

	map_open("files");
	AyyiFilesource* filesource = NULL;
	while ((filesource = ayyi_song__filesource_next(filesource))) {
		if (!yaml_add_key_value_pair("name", filesource->name)) goto error;
	}
	end_map;

	map_open("tracks");
	AyyiTrack* s_track = NULL;
	char id[64], src[128];
	while ((s_track = ayyi_song__audio_track_next(s_track))) {
		snprintf(id, 63, "%"PRIu64, s_track->id);
		map_open(s_track->name);
		if (!yaml_add_key_value_pair("id", id)) goto error;
		if (!yaml_add_key_value_pair("input_name", s_track->input_name)) goto error;
		if (!yaml_add_key_value_pair("output_name", (char*)ayyi_track__get_output_name(s_track))) goto error;
		if (!yaml_add_key_value_pair_int("colour", s_track->colour)) goto error;
		end_map;
	}
	end_map;

	map_open("parts");
	AyyiRegion* region = NULL;
	int region_count = 0;
	while ((region = ayyi_song__audio_region_next(region))) {
		dbg (2, "%i region=%p", region_count, region);
		int i = ayyi_region__get_pod_index((AyyiRegionBase*)region);
		AMPart* gpart = am_song__get_part_by_region_index(i, AYYI_AUDIO);
		if (!gpart){ dbg (2, "region has no part.\n"); continue; }
		snprintf(id, 63, "%"PRIu64, region->id);
		snprintf(src, 127, "%"PRIu64, region->source0);
		AyyiPlaylist* ap = ayyi_song__playlist_at(region->playlist);
		char* playlist = ap ? ap->name : NULL;
		map_open(id);

		if (!yaml_add_key_value_pair    ("name",     region->name))          goto error;
		if (!yaml_add_key_value_pair    ("id",       id))                    goto error;
		if (!yaml_add_key_value_pair    ("playlist", playlist))              goto error;
		if (!yaml_add_key_value_pair_int("start",    region->start))         goto error;
		if (!yaml_add_key_value_pair_int("length",   region->length))        goto error;
		if (!yaml_add_key_value_pair_int("position", region->position))      goto error;
		if (!yaml_add_key_value_pair_int("flags",    region->flags))         goto error;
		if (!yaml_add_key_value_pair    ("source0",  src))                   goto error;
		if (!yaml_add_key_value_pair_int("channels", region->channels))      goto error;
		if (!yaml_add_key_value_pair_int("colour",   gpart->bg_colour))      goto error;
		end_map;

		region_count++;
	}
	end_map;

	//song map parts:
	map_open("songmap");
	int j = 0;
	GList* l = song->map_parts->list;
	for (;l;l=l->next,j++) {
		AMPart* part = l->data;

		char name[16] = {'\0',};
		sprintf(name, "a%i", j);
		map_open(name);
		if (!yaml_add_key_value_pair     ("name",     part->name))                      goto error;
		if (!yaml_add_key_value_pair_int ("position", ayyi_pos2samples(&part->start)))  goto error;
		if (!yaml_add_key_value_pair_int ("length",   ayyi_pos2samples(&part->length))) goto error;
		if (!yaml_add_key_value_pair_int ("colour",   part->bg_colour))                 goto error;
		end_map;
	}
	end_map;

	map_open("panels");
	char window_name[128];
	panel_foreach {
		ayyi_panel_print_label_safe(panel, window_name);
		dbg(1, "panel=%s", window_name);
		map_open(window_name);

		if (AYYI_IS_ARRANGE(panel)) {
			Arrange* arrange = (Arrange*)panel;
			map_open("tracks");
			char track_name[128];
			for (int t=0;t<am_track_list_count(song->tracks);t++) {
				sprintf(track_name, "track-%02i", t);
				map_open(track_name);
				ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
				if (atr) if(!yaml_add_key_value_pair_int("height", atr->height)) goto error;
				end_map;
			}
			end_map;
		}
		if (AYYI_IS_MIXER_WIN(panel)) {
			char strip_name[128];
			Strip* strip = NULL;
			while ((strip = mixer__strip_next((MixerWin*)panel, strip))) {
				sprintf(strip_name, "strip-%i", strip->strip_num);
				map_open(strip_name);

				if (strip__is_hidden(strip)) {
					if (!yaml_add_key_value_pair_int("hidden", 1)) goto error;
				}
				if (!STRIP_IS_MASTER && !strip__sends_visible(strip)) {
					if (!yaml_add_key_value_pair_int("sends_hidden", 1)) goto error;
				}
				if (strip->colour > -1) {
					if (!yaml_add_key_value_pair_int("colour", strip->colour)) goto error;
				}
				end_map;
			}
		}
		end_map;
	} end_panel_foreach
	end_map;

	end_map;
	end_document;

	yaml_emitter_delete(&emitter);
	fclose(fp);
	dbg(2, "yaml write finished ok.");
	return TRUE;

  error:
	switch (emitter.error){
		case YAML_MEMORY_ERROR:
			fprintf(stderr, "Memory error: Not enough memory for emitting\n");
			break;
		case YAML_WRITER_ERROR:
			fprintf(stderr, "Writer error: %s\n", emitter.problem);
			break;
		case YAML_EMITTER_ERROR:
			log_print(LOG_FAIL, "yaml emitter error: %s", emitter.problem);
			break;
		default:
			fprintf(stderr, "save: yaml internal error: %s\n", emitter.problem);
			break;
	}
	yaml_event_delete(&event);
	yaml_emitter_delete(&emitter);
	fclose(fp);
	return FALSE;
}
#endif


