/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi Project. http://www.ayyi.org           |
* | copyright (C) 2012-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __svgtogglebutton_h__
#define __svgtogglebutton_h__

#include <glib.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

G_BEGIN_DECLS

#define TYPE_SVG_TOGGLE_BUTTON (svg_toggle_button_get_type ())
#define SVG_TOGGLE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SVG_TOGGLE_BUTTON, SvgToggleButton))
#define SVG_TOGGLE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SVG_TOGGLE_BUTTON, SvgToggleButtonClass))
#define IS_SVG_TOGGLE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SVG_TOGGLE_BUTTON))
#define IS_SVG_TOGGLE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SVG_TOGGLE_BUTTON))
#define SVG_TOGGLE_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SVG_TOGGLE_BUTTON, SvgToggleButtonClass))

typedef struct _SvgToggleButton SvgToggleButton;
typedef struct _SvgToggleButtonClass SvgToggleButtonClass;
typedef struct _SvgToggleButtonPrivate SvgToggleButtonPrivate;

struct _SvgToggleButton {
	GtkToggleButton         parent_instance;
	int                     size;
	GtkAction*              on;    // optional action triggered when the button is clicked active
	SvgToggleButtonPrivate* priv;
};

struct _SvgToggleButtonClass {
	GtkToggleButtonClass parent_class;
};


GType            svg_toggle_button_get_type   () G_GNUC_CONST;
GtkWidget*       svg_toggle_button_new        (const char* icon_name);
GtkWidget*       svg_toggle_button_new_full   (const char* svgpath, const char* svgpath2, int size);
SvgToggleButton* svg_toggle_button_construct  (GType);
void             svg_toggle_button_set_size   (SvgToggleButton*, int);
void             svg_toggle_button_set_active (SvgToggleButton*, bool);


G_END_DECLS

#endif
