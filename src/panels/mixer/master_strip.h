/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include "panels/panel.h"
#include "panels/mixer/strip.h"

#define TYPE_MASTER_STRIP (master_strip_get_type ())
#define MASTER_STRIP(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MASTER_STRIP, MasterStrip))
#define MASTER_STRIP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MASTER_STRIP, MasterStripClass))
#define IS_MASTER_STRIP(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MASTER_STRIP))
#define IS_MASTER_STRIP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MASTER_STRIP))
#define MASTER_STRIP_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MASTER_STRIP, MasterStripClass))

typedef struct _MasterStripClass MasterStripClass;
typedef struct _MasterStripPrivate MasterStripPrivate;

struct _MasterStrip {
	AyyiMixerStrip parent_instance;
	MasterStripPrivate * priv;
};

struct _MasterStripClass {
	AyyiMixerStripClass parent_class;
};


MasterStrip* master_strip_new       (AMChannel*, AyyiPanel*);
MasterStrip* master_strip_construct (GType);
gboolean     master_strip__update   (Strip*);

