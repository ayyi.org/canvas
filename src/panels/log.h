/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __log_h__
#define __log_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_LOG_WIN            (ayyi_log_win_get_type ())
#define AYYI_LOG_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_LOG_WIN, AyyiLogWin))
#define AYYI_LOG_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_LOG_WIN, AyyiLogWinClass))
#define AYYI_IS_LOG_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_LOG_WIN))
#define AYYI_IS_LOG_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_LOG_WIN))
#define AYYI_LOG_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_LOG_WIN, AyyiLogWinClass))

#define LOG_FIRST ((LogWindow*)windows__get_first(AYYI_TYPE_LOG_WIN))

typedef struct _AyyiLogWinClass AyyiLogWinClass;

struct _AyyiLogWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiLogWin
{
   AyyiPanel      panel;
   GtkWidget*     textview;
};

GType       ayyi_log_win_get_type ();

void        log_win__init    ();
GtkWidget*  log_win__new     (AyyiWindow*, Size*, GtkOrientation);

G_END_DECLS

#endif
