/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2012-21 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
uniform vec2 modelview;
uniform vec2 translate;

uniform float height;
uniform float level;
uniform float peak;
uniform float peak2;

varying vec2 MCposition;

void main (void)
{
	float mid0 = 2.0 * height / 3.0; // more green than red

	float level2 = level * height;

	if(MCposition.y < level2){
		/* this is the continuous colour gradient version. doesnt look so good at small sizes.
		if(MCposition.y < mid0){
			float red = MCposition.y / mid0;
			float grn = 1.0;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}else{
			float red = 1.0;
			float grn = 1.0 - (MCposition.y - mid0) / mid1;
			gl_FragColor = vec4(red, grn, 0.0, 1.0);
		}
		*/
		if(MCposition.y < mid0){
			gl_FragColor = vec4(0.2, 1.0, 0.2, 1.0); // green
		}else{
			gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0); // yellow
		}
	}else{
		if(MCposition.y < peak){
			gl_FragColor = vec4(0.35);
		}else{
			if(MCposition.y < peak2 && MCposition.y > peak2 - 2.0){
				float red = MCposition.y / height;
				float grn = 1.0 - (MCposition.y) / height;
				gl_FragColor = vec4(red, grn, 0.0, 1.0);
			}else{
				gl_FragColor = vec4(0.0);
			}
		}
	}
}

