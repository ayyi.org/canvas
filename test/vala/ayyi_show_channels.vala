/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShowchannelsClient : Ayyi.SimpleClient
{
	public static int main (string[] args)
	{
		app = new ShowchannelsClient();
		return (int)app.run(((ShowchannelsClient*)app)->on_shm);
	}

	private void on_shm ()
	{
		print("-----------------------------------------------------------------\n");
		print("channels:\n");
		print("  %2s  %16s %s %4s %5s %3s %3s\n", "", "", "n", " lvl", "  pan ", "aux1", "aux2");
		AM.Array<Channel*>* channels = song->channels;
		Model.Iter iter;
		channels->iter_init(&iter);
		Channel* channel = null;
		while((channel = channels->iter_next(&iter)) != null){
			AyyiChannel* ac = AyyiMixer.channel_at(channel->core_index);

			AyyiAux* aux1 = Mixer.get_aux(ac, 0);
			string aux1_level = "%3.2f";
			aux1_level = (aux1 != null) ? aux1_level.printf(aux1->level) : " off";

			AyyiAux* aux2 = Mixer.get_aux(ac, 1);
			string aux2_level = "%3.2f";
			aux2_level = (aux2 != null) ? aux2_level.printf(aux2->level) : " off";

			print("  %2i: %16s %i %.2f %5.2f%s %3s %3s\n", (int)channel->core_index, (string)ac->name, channel->nchans, ac->level, ac->pan, (ac->has_pan != 0) ? " " : "X", aux1_level, aux2_level);
		}

		print("-----------------------------------------------------------------\n");
		print("channel plugins:\n");

		channels->iter_init(&iter);
		channel = null;
		while((channel = channels->iter_next(&iter)) != null){
			AyyiChannel* ac = AyyiMixer.channel_at(channel->core_index);
			for(int i=0;i<PLUGINS_PER_CHANNEL;i++){
				if(ac->plugin[i] != null){
					string active = (ac->plugin[i]->active != 0) ? "A" : " ";
					print("  %2i: %i %s %i\n", (int)channel->core_index, ac->plugin[i]->idx, active, ac->plugin[i]->bypassed);
				}
			}
		}
		print("-----------------------------------------------------------------\n");

		app.quit();
	}
}


