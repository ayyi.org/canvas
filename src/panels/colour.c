/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include <sys/types.h>
#include <gdk/gdkkeysyms.h>

#include "windows.h"
#include "support.h"
#include "style.h"
#include "colour.h"
#include "song.h"

extern int      ayyi_panel_drag_dataget  (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, AyyiPanel*);

static GObject* colour_construct         (GType, guint n_construct_properties, GObjectConstructParam*);
static void     colour_on_ready          (AyyiPanel*);
static void     colour_on_realise        (GtkWidget* window, gpointer);
static void     colour_on_resize         (GtkWidget*, GtkAllocation*, ColourWin*);
static bool     colour_on_motion_event   (GtkWidget*, GdkEventMotion*, gpointer);
static void     colour_on_palette_change (GObject*, gpointer);
static int      colour_drag_dataget      (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, gpointer);
static int      colour_drag_datareceived (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer);
#if 0
static GtkWidget* create_colorselectiondialog();
#endif

G_DEFINE_TYPE (AyyiColourWin, ayyi_colour_win, AYYI_TYPE_PANEL)


void
colour_init()
{
}


static void
ayyi_colour_win_class_init (AyyiColourWinClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS(klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	panel->name           = "Colour";
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->accel          = GDK_F8;
	panel->default_size.x = 400;
	panel->default_size.y = 120;
	panel->on_ready       = colour_on_ready;

	g_object_class->constructor = colour_construct;
}


static void
ayyi_colour_win_init (AyyiColourWin* object)
{
}


static GObject*
colour_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_colour_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_COLOUR_WIN);

	return g_object;
}


static void
colour_on_ready (AyyiPanel* panel)
{
	ColourWin* col_win = (ColourWin*)panel;

	if(col_win->row[0]) return;

	gtk_container_set_border_width(GTK_CONTAINER(panel->vbox), 0);

	GdkColor color;
	int boxes_per_row = 8;
	int r, j, p = 0;
	for(r=0; r<8; r++){
		GtkWidget* h = col_win->row[r] = gtk_hbox_new(NON_HOMOGENOUS, 0); 
		gtk_container_set_border_width(GTK_CONTAINER(h), 0);
		for(j=0;j<boxes_per_row;j++){

			am_rgba_to_gdk(&color, song->palette[p]);

			GtkWidget* e = col_win->box[p] = gtk_event_box_new();
			gtk_widget_modify_bg (GTK_WIDGET(e), GTK_STATE_NORMAL, &color);
			gtk_container_set_border_width(GTK_CONTAINER(e), 0);

			// dnd
			gtk_drag_source_set(e, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE);
			g_signal_connect (G_OBJECT(e), "drag-data-received", G_CALLBACK(colour_drag_datareceived), NULL);
			g_signal_connect (G_OBJECT(e), "drag-data-get",      G_CALLBACK(colour_drag_dataget),      panel);

			gtk_box_pack_start(GTK_BOX(h), e, TRUE, TRUE, NO_PADDING);
			p++;
		}

		// add another to try and smooth out the sizing
		GtkWidget* e = col_win->spacer[r] = gtk_event_box_new();
		gtk_widget_modify_bg (GTK_WIDGET(e), GTK_STATE_NORMAL, &(GdkColor){0,});
		gtk_box_pack_start(GTK_BOX(h), e, TRUE, TRUE, 1);

		panel_pack(h, EXPAND_TRUE);
	}

	g_signal_connect_after ((gpointer)panel->window, "realize", G_CALLBACK(colour_on_realise), NULL);
	g_signal_connect ((gpointer)panel->vbox, "size-allocate", G_CALLBACK(colour_on_resize), col_win);
	g_signal_connect ((gpointer)col_win, "motion-notify-event", (gpointer)colour_on_motion_event, col_win);

	am_song__connect("palette-change", G_CALLBACK(colour_on_palette_change), col_win);
}


/*
 *  Refresh windows following a palette change
 */
void
colour_win_update (ColourWin* colour_win)
{
	PF;
	GdkColor colour;
	for(int i=0;i<AM_MAX_COLOUR;i++){
		am_rgba_to_gdk(&colour, song->palette[i]);
		gtk_widget_modify_bg(colour_win->box[i], GTK_STATE_NORMAL, &colour);
	}
}


static void
colour_on_realise (GtkWidget* window, gpointer user_data)
{
}


static void
colour_on_resize (GtkWidget* widget, GtkAllocation* allocation, ColourWin* win)
{
	// Needed to ensure that the boxes are evenly sized and spaced which gtk doesnt do automaticallly.
	// -still problems? use a GtkSizeGroup...

	if(!windows__verify_pointer((AyyiPanel*)win, AYYI_TYPE_COLOUR_WIN)) return;

	int window_width  = widget->allocation.width;
	int window_height = widget->allocation.height;

	if(window_width != win->box_size.x || window_height != win->box_size.y ){ // anti-feedback-loop check

		float aspect_ratio = window_width / window_height;
		int cells_per_row = 8;
		if(aspect_ratio > 2.0)  cells_per_row = 16;
		if(aspect_ratio > 4.0)  cells_per_row = 32;
		if(aspect_ratio > 12.0) cells_per_row = 64;

		int cell_width = window_width / cells_per_row;
		dbg(2, "win=%p width=%i cellwid=%i", win, window_width, cell_width);
		GtkWidget* cell;
		int r,j;
		for(r=0;r<64/cells_per_row;r++){
			for(j=0;j<cells_per_row;j++){
				cell = win->box[j + cells_per_row * r];
				g_return_if_fail(cell);
				dbg(3, "%i: cell=%p", j, cell);
				gtk_widget_set_size_request(cell, cell_width, -1);

				gtk_widget_reparent(cell, win->row[r]);
			}

			gtk_box_reorder_child(GTK_BOX(win->row[r]), win->spacer[r], -1); // move spacer to end of row.
		}

		for(r=0;r<8;r++) if(r < 64/cells_per_row) gtk_widget_show(win->row[r]); else gtk_widget_hide(win->row[r]);

		win->box_size.x = window_width; // anti-feedback.
		win->box_size.y = window_height;
	}
}


#if 0
static GtkWidget*
create_colorselectiondialog()
{
  GtkWidget *colorselectiondialog1;
  GtkWidget *ok_button1;
  GtkWidget *cancel_button1;
  GtkWidget *help_button1;
  GtkWidget *color_selection1;

  //colorselectiondialog1 = gtk_color_selection_dialog_new (_("Select Color"));
  colorselectiondialog1 = gtk_color_selection_dialog_new ("Select Color");
  gtk_window_set_resizable (GTK_WINDOW (colorselectiondialog1), FALSE);

  ok_button1 = GTK_COLOR_SELECTION_DIALOG (colorselectiondialog1)->ok_button;
  gtk_widget_show (ok_button1);
  GTK_WIDGET_SET_FLAGS (ok_button1, GTK_CAN_DEFAULT);

  cancel_button1 = GTK_COLOR_SELECTION_DIALOG (colorselectiondialog1)->cancel_button;
  gtk_widget_show (cancel_button1);
  GTK_WIDGET_SET_FLAGS (cancel_button1, GTK_CAN_DEFAULT);

  help_button1 = GTK_COLOR_SELECTION_DIALOG (colorselectiondialog1)->help_button;
  gtk_widget_show (help_button1);
  GTK_WIDGET_SET_FLAGS (help_button1, GTK_CAN_DEFAULT);

  color_selection1 = GTK_COLOR_SELECTION_DIALOG (colorselectiondialog1)->colorsel;
  gtk_widget_show (color_selection1);
  gtk_color_selection_set_has_opacity_control (GTK_COLOR_SELECTION (color_selection1), FALSE);

  return colorselectiondialog1;
}
#endif


static void
colour_on_palette_change (GObject* _song, gpointer user_data)
{
	// Any or all of the palette colours may have changed.

	ColourWin* colour_win = user_data;
	g_return_if_fail(colour_win);
	colour_win_update(colour_win);
}


static bool
colour_on_motion_event (GtkWidget* widget, GdkEventMotion* event, gpointer user_data)
{
	return HANDLED;
}


static void
print_drag_actions (GdkDragContext* context)
{
	dbg(0, "%s%s%s%s", context->actions & GDK_ACTION_COPY ? "GDK_ACTION_COPY " : "", 
	                   context->actions & GDK_ACTION_MOVE ? "GDK_ACTION_MOVE ": "",
	                   context->actions & GDK_ACTION_LINK ? "GDK_ACTION_LINK ": "",
	                   context->actions & GDK_ACTION_PRIVATE ? "GDK_ACTION_PRIVATE ": "");

	GdkModifierType state;
	if(gtk_get_current_event_state(&state)) dbg(0, "state=%i", state);
}


static int
colour_drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	PF;

	dbg(1, "suggested_action=%i", (int)drag_context->suggested_action);
	if(_debug_) print_drag_actions(drag_context);

	// ideally, if control is pressed would would never get here, but still...
	if((drag_context->actions & GDK_ACTION_COPY) && !(drag_context->actions & GDK_ACTION_MOVE)){ // CTRL key is pressed.
		dbg(0, "dock operation requested!");
		return ayyi_panel_drag_dataget(widget, drag_context, data, info, time, NULL);
	}

	// assume that all colour windows have the same colours.
	AyyiPanel* panel = user_data;
	g_return_val_if_fail(AYYI_IS_COLOUR_WIN(panel), 0);
	// find our square
	int i; for(i=0;i<255;i++){
		if(((ColourWin*)panel)->box[i] == widget) break;
	}

	char text[16];
	sprintf(text, "colour:%i%c%c", i+1, 13, 10); // 1 based to avoid atoi problems.

	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (guchar*)text, strlen(text));

	return false;
}


static int
colour_drag_datareceived (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	PF;

	return false;
}


