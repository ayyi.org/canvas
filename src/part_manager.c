/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "model/time.h"
#include "model/midi_part.h"
#include "windows.h"
#include "window.statusbar.h"
#include "song.h"
#include "icon.h"
#include "support.h"
#include "part_manager.h"

extern SMConfig* config;

static void part_manager__on_part_change (GObject*, AMPart*, AMChangeType, AyyiPanel*, gpointer);


void
part_manager__init ()
{
	g_signal_connect(song->parts, "item-changed", G_CALLBACK(part_manager__on_part_change), NULL);
}


bool
launch_beatdetector (AMPart* part)
{
	int region_idx = part->ayyi->shm_idx;

	char arg1[128];
	snprintf(arg1, 127, "-r %i", region_idx);
	gchar args[3][128] = {"ayyi_beat_detector", "", ""};
	gchar* argv[3];
	argv[0] = args[0];
	argv[1] = arg1;
	argv[2] = NULL;

	ayyi_launch_by_name("Ayyi Beat Detector", argv);

	return TRUE;
}


static void
part_manager__on_part_change (GObject* _song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	PF;

	// Updating song length disabled.
	// The song length should be explictly set by the user so they can control where playback stops.
#if 0
	void part_check_song_length(AMPart* part)
	{
		//checks the given part to see if it is 'off the end', and if neccesary adjusts the arr canvas dimensions.

		AyyiSongPos part_end; am_part__end_pos(part, &part_end);
		AyyiSongPos song_end = am_object_val(&song->loc[AM_LOC_END]).sp;

#ifdef DEBUG
		char bbst1[32]; ayyi_pos2bbst(&part_end, bbst1);
		char bbst2[32]; ayyi_pos2bbst(&song_end, bbst2);
		dbg(2, "part=%s song=%s", bbst1, bbst2);
#endif

		if(ayyi_pos_is_after(&part_end, &song_end)){
			dbg(0, "changed");
			am_song__set_end(part_end.beat + 1);
		}
	}

	if((change_type & AM_CHANGE_LEN) || (change_type & AM_CHANGE_POS_X)){
		part_check_song_length(part); // check if song length should change.
	}
#endif

	window_foreach {
		shell__statusbar_print(window, 0, "Part %s changed", part->name);
	} end_window_foreach

	//menu_update_history();
	song_get_history(); // temp! this is not an appropriate place for this call
}


