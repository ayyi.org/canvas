/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2010-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 *  For performance reasons the Meter actor is in a separate sub-tree from
 *  the rest of the UI. It shows meters for all visible tracks.
 */

#define __gl_canvas_priv__

#include "global.h"
#include <gtk/gtk.h>
#include "agl/utils.h"
#include "agl/ext.h"
#include "agl/fbo.h"
#include "waveform/actor.h"
#include "model/ayyi_model.h"
#include "arrange.h"
#include "arrange/gl_canvas.h"
#include "arrange/shader.h"
#include "arrange/gl_meter.h"

extern AGlShaderText meter_text;

static unsigned int vbo;

static void meter_set_uniforms ();
static void meter_set_state    (AGlActor*);

#define WIDTH 4.

enum {
  UNIFORM_HEIGHT = 2,
  UNIFORM_LEVEL,
  UNIFORM_PEAK,
  UNIFORM_PEAK2
};

typedef struct {
	AGlShader    shader;
	struct {
		float    height;
		float    level;
		float    peak;
		float    peak2;
	}            uniform;
} MeterShader;

static MeterShader meter_shader = {{NULL, NULL, 0, agl_null_uniforms, meter_set_uniforms, &meter_text}};

static AGl* agl = NULL;


static void
meter_set_uniforms ()
{
	AGlShader* shader = &meter_shader.shader;

	glUniform1f(UNIFORM_PEAK, ((MeterShader*)shader)->uniform.peak);
}


static void
meter_set_size (AGlActor* actor)
{
	Arrange* arrange = actor->root->user_data;

	actor->region = (AGlfRegion){
		.x1 = 0,
		.y1 = arrange->ruler_height,
		.x2 = WIDTH,
		.y2 = ((AGlActor*)actor->root)->region.y2,
	};
}


static void
meter_set_state (AGlActor* actor)
{
	PLAIN_COLOUR2(actor->program) = 0x00ff00ff;

	gl_warn("");
}


AGlActor*
meter_actor_new (GtkWidget* panel)
{
	Arrange* arrange = (Arrange*)panel;

	agl = agl_get_instance();

	if (!vbo) {
		glGenBuffers(1, &vbo);
		agl_use_va (agl->vao);
	}

	float db_xlate (float db) { return MAX(0.0, (db + 70.0) / 70.0); }

	bool meter_paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		TrackList* tl = arrange->priv->tracks;

		agl_translate (actor->program, 0, CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1); // better to scroll the actor

		int n = 0;
		ArrTrk* first = NULL;
		{
			// find first and last onscreen tracks
			AMTrack* track = NULL;
			ArrTrk* atr = NULL;
			while ((atr = track_list__next_visible(tl, atr ? atr->track : NULL)) && (track = atr->track)) {
				float y = atr->y * CGL->zoom->value.pt.y;
				if (y + atr->height * CGL->zoom->value.pt.y < -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1)
					continue;

				if (!first)
					first = atr;
				if (y > actor->region.y2)
					break;
				n++;
			}
		}

		AGlQuadVertex vertices[n];

		struct Item {
			ArrTrk* track;
			float   level;
			float   warn;
		} items[n];

		ArrTrk* atr = first;
		AMTrack* track = first->track;
		// green
		int i = 0;
		do {
			float y = atr->y * CGL->zoom->value.pt.y;
			if (y > actor->region.y2) break;
			float track_height = atr->height * CGL->zoom->value.pt.y;

			items[i] = (struct Item){
				.level = db_xlate(am_track__get_meterlevel(track)) * track_height,
				.warn = track_height * 0.6,
				.track = atr,
			};

			agl_set_quad (&vertices, i,
				(AGlVertex){1.,    y + track_height - MIN(items[i].warn, items[i].level)},
				(AGlVertex){WIDTH, y + track_height}
			);
		} while (++i < n && (atr = track_list__next_visible (tl, atr ? atr->track : NULL)) && (track = atr->track));

		glBindBuffer (GL_ARRAY_BUFFER, vbo);
		glBufferData (GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray (0);
		glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

		glDrawArrays(GL_TRIANGLES, 0, n * AGL_V_PER_QUAD);

		// orange
		int n2 = 0;
		for (i=0;i<n;i++) {
			if (items[i].level > items[i].warn) {
				ArrTrk* atr = items[i].track;
				float y = atr->y * CGL->zoom->value.pt.y;
				float track_height = atr->height * CGL->zoom->value.pt.y;

				agl_set_quad (&vertices, i,
					(AGlVertex){1.,    y + track_height - items[i].level},
					(AGlVertex){WIDTH, y + track_height - items[i].warn}
				);
				n2++;
			}
		}

		if (n2) {
			agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], 0xffaa00ff);
			glBufferData (GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			glDrawArrays(GL_QUADS, 0, n2 * 4);
		}

		return true;
	}

	AGlActor* a = agl_actor__new (AGlActor,
		.name = g_strdup("Meter"),
		.program = (AGlShader*)agl->shaders.plain,
		.region.y1 = arrange->ruler_height,
		.set_size = meter_set_size,
		.set_state = agl->use_shaders ? meter_set_state : NULL,
		.paint = meter_paint
	);

	if (agl->use_shaders) {
		agl_create_program(&meter_shader.shader);
		gl_warn("meter create");
	}

	return a;
}
