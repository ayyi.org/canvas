/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include "gimputils/gimpactiongroup.h"
#include "shortcuts.h"

GimpActionGroup* global_action_group = NULL;
GList* action_groups = NULL;
static gboolean initialised = FALSE;


void
shortcuts__init ()
{
	global_action_group = shortcuts_add_group("Global");
	shortcuts_add_group("Transport"); // this is also global
	shortcuts_add_group("Midi Edit");

	initialised = TRUE;
}


GimpActionGroup*
shortcuts_add_group (const char* name)
{
	gboolean mnemonics = FALSE;
	GimpActionGroupUpdateFunc update_func = NULL;
	const gchar* label = name;

	GimpActionGroup* action_group = gimp_action_group_new(name, label, "gtk-directory", mnemonics, NULL, update_func);
	action_groups = g_list_append(action_groups, action_group);

	static guint idle = 0;
	gboolean shortcuts_changed(gpointer user_data)
	{
		g_signal_emit_by_name(app, "shortcuts-changed");
		idle = 0;
		return G_SOURCE_REMOVE;
	}
	if(!idle) idle = g_idle_add(shortcuts_changed, NULL);

	return action_group;
}


GimpActionGroup*
shortcuts_get_group (const char* name)
{
	GList* l = action_groups;
	for(;l;l=l->next){
		GimpActionGroup* action_group = l->data;
		if(!strcmp(gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)), name)) return action_group;
	}
	dbg(0, "action group not found: %s", name);
	return NULL;
}


void
shortcuts_add_action (GtkAction* action, GimpActionGroup* action_group)
{
	if(!initialised) perr("not initialised.");

	if(!action_group) action_group = global_action_group;

	gtk_action_group_add_action(GTK_ACTION_GROUP(action_group), action);
	dbg(2, "group=%s group_size=%i action=%s", gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)), g_list_length(gtk_action_group_list_actions(GTK_ACTION_GROUP(action_group))), gtk_action_get_name(action));
}

