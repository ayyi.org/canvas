/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShowRouting : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new ShowRouting();

		if(!app.run(((ShowRouting*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		int n_channels = 0;

		print("-----------------------------------------------------------------\n");
		print("channel                    output \n");
		if((bool)AyyiMixer.channel_next(null)){
			AyyiChannel* channel = null;
			while((bool)(channel = AyyiMixer.channel_next(channel))){
				//AyyiTrack* track = AyyiSong.get_audio_track(channel->shm_idx);
				AyyiList* routing = AyyiMixer.channel__get_routing(channel);
				if((bool)routing){
					AyyiList* item  = List.first(routing);
					if((bool)item){
						AyyiConnection* connection = AyyiSong.get_connection(item->id);
						if((bool)connection){
							char b[256];
							char* j = b;
							if(AyyiSong.connection__parse_string(connection, b)){
								print("%i %20s %s%i%s:  %2i %16s %24s %24s\n", channel->shm_idx, (string)channel->name, yellow, channel->n_out, white, connection->shm_idx, (string)b, (string)(j+64), (string)(j+128));
							}else{
								print("%i %20s %i:  %2i %s\n", channel->shm_idx, (string)channel->name, channel->n_out, connection->shm_idx, (string)connection->name);
							}
						}
					}
				}
				else print("%i %20s %s%1i%s:          [no routing]\n", channel->shm_idx, (string)channel->name, yellow, channel->n_out, white);
				n_channels++;
			}
		}
		else print("\tno channels\n");

		app.quit();
	}
}


