/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifdef __audtioner_c__
struct _auditioner
{
	DBusGProxy*    proxy;
};
#endif

void auditioner_connect ();
void auditioner_play    (AMPoolItem*);
void auditioner_stop    (AMPoolItem*);
void auditioner_toggle  (AMPoolItem*);

