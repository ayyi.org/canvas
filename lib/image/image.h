/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2008 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __image_h__
#define __image_h__

void     bloom                 (FT_Bitmap*, int* width);
void     bloom_pixbuf          (GdkPixbuf*, guint x1, guint x2);

void     copy_bitmap_to_pixbuf (guchar* bitmap, GdkPixbuf*, int pitch);

#endif
