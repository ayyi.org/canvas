/*
  copyright (C) 2012 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

varying vec2 MCposition;

void main(void)
{
	//float constructors not available until glsl 1.5
	//float a[7] = float[](1.0, 0.8, 0.6, 0.4, 0.2, 0.0, 0.0);
	float a[7];
	a[0] = 0.9921;
	a[1] = 0.9335;
	a[2] = 0.9492;
	a[3] = 0.9609;
	a[4] = 0.9375;
	a[5] = 0.9414;
	a[6] = 0.9570;
	float b[7];
	b[0] = 0.9609;
	b[1] = 0.9453;
	b[2] = 0.9726;
	b[3] = 0.9687;
	b[4] = 0.9453;
	b[5] = 0.9609;
	b[6] = 0.9726;
	float c[7];
	c[0] = 0.9687;
	c[1] = 0.9492;
	c[2] = 0.9687;
	c[3] = 0.9726;
	c[4] = 0.9531;
	c[5] = 0.9609;
	c[6] = 0.9726;

	float m = mod(MCposition.x, 7.0);
	float n = mod(MCposition.y, 6.0);
	int x = int(m);
	if(n < 2.0)
		gl_FragColor = vec4(a[x], a[x], a[x], 1.0);
	else if(n < 4.0)
		gl_FragColor = vec4(b[x], b[x], b[x], 1.0);
	else
		gl_FragColor = vec4(c[x], c[x], c[x], 1.0);
	/*
	if(m < 2.0)
		gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	else
		gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
	*/
}

