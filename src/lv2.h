/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

void       lv2_init                ();
void       lv2_destroy             ();
GtkWidget* lv2_instantiate         (const char* uri);
GtkWidget* lv2_instantiate_by_path (const char*);
void       lv2_uninstantiate       ();

