/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2006-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "agl/typedefs.h"
#include "model/model_types.h"
#include "gui_types.h"

#define SCROLL__MAX_PX_PER_REPEAT 50
#define SCROLL__INVERSE_ACCEL 60

#define g_free0(A) (A = (g_free(A), NULL))
#ifndef g_list_free0
#define g_list_free0(var) ((var == NULL) ? NULL : (var = (g_list_free (var), NULL)))
#endif
#define g_source_remove0(S) {if(S) g_source_remove(S); S = 0;}
#define gtk_widget_destroy0(A) (A == NULL ? NULL : (gtk_widget_destroy(A), A = NULL))
#define g_hash_table_destroy0(A) (A == NULL ? NULL : (g_hash_table_destroy(A), A = NULL))
#define set_str(P, NAME) ({ if (P) g_free(P); P = NAME; })

void         widget_colour_set        (GtkWidget*, int num);

void         print_arguments          (GtkObject* object);

void         add_pixmap_directory     (const gchar* directory);

void         set_cursor               (GdkWindow*, GdkCursorType);
void         set_cursor_delayed       (GdkWindow*, GdkCursorType);

const char*  find_path                (const char* dirs[]);
gint         filelist_read            (const gchar* path, GList** files, GList** dirs);
gint         is_hidden                (const gchar* name);

void         get_font_string          (char* font_string, int size_modifier);
void         get_style_font           ();
void         get_style_bg_color       (GdkColor*, GtkStateType);
void         get_style_fg_color       (GdkColor*, GtkStateType);
uint32_t     get_style_bg_color_rgba  (GtkStateType);
uint32_t     get_style_fg_color_rgba  (GtkStateType);
uint32_t     get_style_text_color_rgba(GtkStateType);
void         widget_get_fg_colour     (GtkWidget*, GdkColor*, GtkStateType);
void         widget_get_bg_colour     (GtkWidget*, GdkColor*, GtkStateType);
void         colour_rgba_to_float     (AGlColourFloat*, uint32_t rgba);
void         colour_gdk_to_float      (AGlColourFloat*, GdkColor*);
uint32_t     colour_lighter_rgba      (uint32_t colour, int amount);
uint32_t     colour_darker_rgba       (uint32_t colour, int amount);
void         colour_lighter_gdk       (GdkColor*, int amount);
void         colour_darker_gdk        (GdkColor*, int amount);
void         colour_grey_out          (GdkColor*, GdkColor* bg);
uint32_t     colour_grey_out_rgba     (uint32_t colour);
uint32_t     colour_get_offset_rgba   (uint32_t colour, int amount);
void         colour_get_frame         (GdkColor*);
gboolean     colour_is_dark           (GdkColor*);
gboolean     colour_is_dark_rgba      (uint32_t colour);
void         colour_mix               (GdkColor*, const GdkColor*, int percentage);
uint32_t     colour_mix_rgba          (uint32_t, uint32_t, float amount);
const char*  colour_to_string         (GdkColor*);

void         report                   (int type, char* format, ...);

void         pixbuf_draw_line_cairo   (cairo_t*, DRect*, double line_width, GdkColor*);
void         pixbuf_fill_rect         (GdkPixbuf*, GdkRectangle*, GdkColor*);
void         pixbuf_clear_rect        (GdkPixbuf*, GdkRectangle*);
void         pixbuf_clear_alpha       (GdkPixbuf*, guchar alpha);

bool         dnd_get_filename         (const char* uri, char* filename);

int          snap_mode_lookup         (const char* mode_name);

void         print_gerror             (GError**);

void         global_accels_init       ();
void         make_accels              (GtkAccelGroup*, GimpActionGroup*, AMAccel*, int count, gpointer user_data);
void         accels_connect           (GtkWidget*);
void         accels_disconnect        (GtkWidget*);
int          keypress_get_repeat_count(int keycode);
void         keypress_reset           ();
gboolean     stop_propogation         (GtkWidget*, GdkEventButton*, gpointer user_data);

void         report_time              (struct timeval* time_ref);

const gchar* vfs_get_method_string    (const gchar* substring, gchar** method_string);
GList*       uri_list_to_glist        (const char* uri_list);
void         uri_list_free            (GList*);
gchar*       uri_list_to_utf8         (const char* uri_list);

gchar*       gimp_strip_uline         (const gchar* str);
gchar*       gimp_get_accel_string    (guint key, GdkModifierType modifiers);

gboolean     note_is_black            (int);
int          note_get_white_num       (int);
int          note_get_black_num       (int);
const char*  note_format              (int note);
int          note_get_octave_num      (int note);

void         gtk_box_replace          (GtkWidget* box, GtkWidget*, GtkWidget*);
void         show_widget_if           (GtkWidget*, gboolean);
gboolean     widget_is_in_paned       (GtkWidget*);

GtkWidget*   spacer_new               (GtkBox* container, int height);

const char*  get_orientation_string   (GtkOrientation);

void         print_widget             (GtkWidget*);
void         print_widget_tree        (GtkWidget*);

GtkWidget*   add_menu_items_from_defn (GtkWidget* menu, MenuDef*, int, gpointer);

void         signal_handler_disconnect_data(gpointer instance, gpointer data, int, const char* fail_message);

int          array_index              (void** array, void* item, int item_size);

bool         list_cmp                 (GList*, GList*);
