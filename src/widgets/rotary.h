/*
 * Copyright (C) 2008 Tim Orford. Part of the Ayyi project. http://ayyi.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 */
#ifndef __gtk_knob_h__
#define __gtk_knob_h__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include "model/observable.h"

enum{
  TEXT_POSITION_NONE,
  TEXT_POSITION_LEFT,
  TEXT_POSITION_TOP
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_DIAL(obj)          GTK_CHECK_CAST (obj, gtk_knob_get_type (), GtkDial)
#define GTK_DIAL_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_knob_get_type (), GtkDialClass)
#define GTK_IS_DIAL(obj)       GTK_CHECK_TYPE (obj, gtk_knob_get_type ())


typedef struct _GtkDial        GtkDial;
typedef struct _GtkDialClass   GtkDialClass;

struct _GtkDial
{
  GtkWidget  widget;

  gboolean   active;          //normally active, inactive is like GTK_STATE_INSENSITIVE, but still responds to mouse clicks.

  guint      policy : 2;      //update policy (GTK_UPDATE_[CONTINUOUS/DELAYED/DISCONTINUOUS])

  guint8     button;          //button currently pressed or 0 if none

  gint       radius;          //dimensions of dial components

  guint32    timer;           //ID of update timer, or 0 if none

  gfloat     angle;           //current angle
  gfloat     last_angle;

  gfloat     pan_angle;

  gfloat     old_value;       //old values from adjustment stored so we know when something changes
  gfloat     old_lower;
  gfloat     old_upper;

  Observable* value;
  GtkAdjustment* pan;

  gint       in_button;       //the mouse is currently over the widget.
  gboolean   is_pressed;

  GdkPixmap* text_pixmap;     //rendered text to show the current numerical value for the knob.
  gboolean   text_is_visible; //only show the text if this is True.
  int        text_width;
  int        text_height;
  int        text_position;   //either top or left

  int        padding_top;
  int        padding_bottom;

  //int        value_changed_handler;

  void       (*on_right_click)(GtkWidget*, GdkEventButton*);
};

struct _GtkDialClass
{
  GtkWidgetClass parent_class;
  gboolean (* change_value) (GtkDial*, GtkScrollType);
};


GtkType        gtk_knob_get_type               (void);
GtkWidget*     gtk_knob_new                    (Observable*, gboolean active);
#if 0
void           gtk_knob_set_update_policy      (GtkDial*, GtkUpdateType);
#endif

void           gtk_knob_set_adjustment         (GtkDial*, Observable*);

void           gtk_knob_set_showvalue          (GtkWidget*, gboolean show);
gboolean       gtk_knob_get_showvalue          (GtkWidget*);
void           gtk_knob_set_padding            (GtkWidget*, int top, int bottom);

void           gtk_knob_add_pan                (GtkWidget*, GtkAdjustment*);
void           gtk_knob_remove_pan             (GtkWidget*);

void           gtk_knob_set_active             (GtkWidget*, gboolean active);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif
