/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __pool_window_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include "waveform/waveform.h"
#include "waveform/pixbuf.h"
#include <model/time.h>
#include <model/am_palette.h>
#include <model/transport.h>
#include <model/track_list.h>
#include "window.statusbar.h"
#include "windows.h"
#include "support.h"
#include "song.h"
#include "icon.h"
#include "style.h"
#include "../shortcuts.h"
#include "part_manager.h"
#include "auditioner.h"
#include "pool_model.h"

#include "pool.h"

#define POOL_PIXBUF_HEIGHT 32
#define POOL_PIXBUF_MIN_WIDTH 80

static GObject*      pool_construct                (GType, guint n_construct_properties, GObjectConstructParam*);
static void          pool_on_ready                 (AyyiPanel*);
static void          pool_win_destroy              (GtkObject*);
static void          pool_win_finalize             (GObject*);
static void          pool_drag_received            (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer user_data);
static void          pool_drag_dataget             (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, AyyiPanel*);
//static gint          pool_drag_drop                (GtkWidget*, GdkDragContext*, gint x, gint y, guint time, gpointer);
static void          pool_win_on_allocate          (GtkWidget*, GtkAllocation*, gpointer);
static void          pool_win_on_row_selected      (GtkTreeView*, gpointer);
static void          pool_win_on_view_change       (GtkTreeView*, GtkTreeIter*, GtkTreePath*, gpointer);
static gboolean      pool_view_on_button_press     (GtkWidget* treeview, GdkEventButton*, gpointer);
static void          pool_win_update               ();
static void          pool_delete_k                 (GtkWidget* accel_group, gpointer);
static GList*        pool_get_selected_regions     (GtkTreeView*);

static void          pool_win_show_popupmenu       (GtkWidget* treeview, GdkEventButton*, gpointer);
static GtkWidget*    pool_win_menu_init            (GtkTreeView*);

static void          pool_win__add_part_at_current (GtkWidget*, GtkTreeView*);
static void          pool_win__remove_selected_files (GtkWidget*, GtkTreeView*);
static void          pool_win_pixbufs_update       (PoolWindow*);
#ifdef NEVER
static AyyiRegion*   pool_iter_get_fullregion      (GtkTreeIter*);
#endif
static AMPoolItem*   pool_path_get_item            (GtkTreePath*);
static void          pool_view_row_contents_delete (GtkWidget*, GtkTreeView*);

static void          pool_on_part_selection_change (GObject*, AyyiPanel*, PoolWindow*);


static gboolean      pool_is_resizing = FALSE;

extern SMConfig* config;

G_DEFINE_TYPE (AyyiPoolWin, ayyi_pool_win, AYYI_TYPE_PANEL)

/*

local song audiofile data is stored in two object types:
	-ayyi model:
		-PoolItem objects store information about a single audio file.
		-an AMList of PoolItem's is kept in song->pool
	-gui:
		-the gtk_tree_model holds the list of PoolItem's, and also holds a list of regions for each PoolItem.
	 	-it was previously required for each file in the tree_model to have at least one region, but this is no longer the case.

PoolItem lookup:
	we have no simple way of getting from a treeview row to the PoolItem without a lookup.
	one option is to move all local pool data to the treemodel,
	and referencing pool items via the gtk_tree_row_reference using hidden columns.
	Currently we use a hidden column for the file/region index. Getting the PoolItem* is still done as a lookup.

*/


static void
ayyi_pool_win_class_init (AyyiPoolWinClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	g_object_class->finalize = pool_win_finalize;
	g_object_class->constructor = pool_construct;
	object_class->destroy = pool_win_destroy;

	panel->name           = "Pool";
	panel->accel          = GDK_F7;
	panel->on_ready       = pool_on_ready;
	panel->accel_group    = gtk_accel_group_new();
	panel->action_group   = shortcuts_add_group("Pool Mod");
	panel->has_link       = true;
	panel->has_follow     = true;
	panel->has_toolbar    = true;
	panel->has_statusbar  = true;
	panel->default_size   = (Pti){.x = 300, .y = 360};
}


static void
ayyi_pool_win_init (AyyiPoolWin* object)
{
}


static GObject*
pool_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	g_return_val_if_fail(song->pool, NULL);

	GObject* object = G_OBJECT_CLASS(ayyi_pool_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)object, AYYI_TYPE_POOL_WIN);

	return object;
}


static void
pool_win_destroy (GtkObject* object)
{
	PoolWindow* pool = (PoolWindow*)object;
	if(!((AyyiPanel*)object)->destroyed){

		#define parts_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (am_parts, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, pool) != 1) pwarn("handler disconnection");
		parts_handler_disconnect(pool_on_part_selection_change);
		#undef parts_handler_disconnect

		((AyyiPanel*)object)->destroyed = true;
	}

	GTK_OBJECT_CLASS(ayyi_pool_win_parent_class)->destroy(object);
}


static void
pool_win_finalize (GObject* g_object)
{
	G_OBJECT_CLASS(ayyi_pool_win_parent_class)->finalize(g_object);
}


static void
pool_on_ready (AyyiPanel* panel)
{
	/*

	  dock_object
	  +--vbox
	     +--toolbar
	     +--hbox
	     |  +--scrollwin
	     |     +--pool treeview
	     |
	     +--statusbar

	  -poolview scrollbars:
	   we maybe need to do like rox and link the scrollbar to the tree with:
	       gtk_range_set_adjustment(GTK_RANGE(filer_window->scrollbar),
	        gtk_tree_view_get_vadjustment(GTK_TREE_VIEW(view_details)));

	*/

	PoolWindow* pool_win = (PoolWindow*)panel;
	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_POOL_WIN);
	AyyiPanelClass* panel_class = AYYI_PANEL_GET_CLASS(panel);

	if(pool_win->treeview) return;

	//------------------------------------------

	AMAccel mod_keys[] = {
		{"Delete", {{GDK_Delete, 0,}, {0, 0}}, pool_delete_k, NULL, NULL},
	};

	if(k->instances <= 1){
		GtkAccelGroup* mod_accels = gtk_accel_group_new();
		make_accels(mod_accels, panel_class->action_group, mod_keys, G_N_ELEMENTS(mod_keys), NULL);
		panel_class->mod_accels = g_list_append(panel_class->mod_accels, mod_accels);
	}
	gtk_window_add_accel_group(GTK_WINDOW(panel->window), panel_class->mod_accels->data);

	//------------------------------------------

	GtkWidget* dockbar = gdl_dock_bar_new (GDL_DOCK (panel->window->dock));
	gdl_dock_bar_set_style(GDL_DOCK_BAR(dockbar), GDL_DOCK_BAR_BOTH); //GDL_DOCK_BAR_TEXT);
	gdl_dock_bar_set_orientation(GDL_DOCK_BAR(dockbar), GTK_ORIENTATION_HORIZONTAL);

	panel_pack(dockbar, EXPAND_FALSE);

	//------------------------------------------

	GtkWidget* scroll = pool_win->scrollwindow = gtk_scrolled_window_new(NULL, NULL); //adjustments created automatically.
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	GtkWidget* tree = pool_win->treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(gui_pool->treestore));
	gtk_container_add(GTK_CONTAINER(scroll), tree);

	g_signal_connect(G_OBJECT(pool_win), "size-allocate", G_CALLBACK(pool_win_on_allocate), panel);
	g_signal_connect(G_OBJECT(tree), "row-expanded", G_CALLBACK(pool_win_on_view_change), panel);
	g_signal_connect(G_OBJECT(tree), "row-collapsed", G_CALLBACK(pool_win_on_view_change), panel);
	g_signal_connect(G_OBJECT(tree), "cursor-changed", G_CALLBACK(pool_win_on_row_selected), NULL);

	gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(tree)), GTK_SELECTION_MULTIPLE);

#ifdef HAVE_GTK_2_10
	// use treeview lines
	GValue gval = {0,};
	g_value_init(&gval, G_TYPE_CHAR);
#ifdef HAVE_GLIB_2_32
	g_value_set_schar(&gval, '1');
#else
	g_value_set_char(&gval, '1');
#endif
	g_object_set_property(G_OBJECT(tree), "enable-tree-lines", &gval);
#endif

	//pop up menu:
	gboolean pool_on_popupmenu (GtkWidget* treeview, gpointer userdata) { pool_win_show_popupmenu(treeview, NULL, userdata); return HANDLED; }
	g_signal_connect(tree, "button-press-event", (GCallback)pool_view_on_button_press, pool_win);
	g_signal_connect(tree, "popup-menu", (GCallback)pool_on_popupmenu, NULL); // triggered by SHIFT-F10

	//------------------------------------------

	// pool treeview
	//    - col 1: names (file or region) with some lines, 
	//    - col 2: region thumbnails.

	// column 1
	GtkTreeViewColumn* col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Region");
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), col1); // pack treeview-column into the treeview
	GtkCellRenderer* renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE); // pack cellrenderer into treeview-column

	gtk_tree_view_column_add_attribute(col1, renderer1, "text", COL_NAME);

	// column2
	GtkTreeViewColumn* col2 = pool_win->col2 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col2, "Overview");
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), col2);
	GtkCellRenderer* renderer2 = gui_pool->overview_renderer = gtk_cell_renderer_pixbuf_new();
	gtk_tree_view_column_pack_start(col2, renderer2, TRUE);
	g_object_set(G_OBJECT(renderer2), "xalign", 0.0, NULL);

	gtk_tree_view_column_add_attribute(col2, renderer2, "pixbuf", COL_OVERVIEW);

	//---------------------------------------------------------

	pool_win->menu = pool_win_menu_init(GTK_TREE_VIEW(tree));

	//---------------------------------------------------------

	gtk_drag_dest_set(tree, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction)(GDK_ACTION_MOVE | GDK_ACTION_COPY));
	//g_signal_connect(G_OBJECT(view), "drag-motion", G_CALLBACK(pool_drag_motion), NULL);
	g_signal_connect(G_OBJECT(tree), "drag-data-received", G_CALLBACK(pool_drag_received), NULL);

	//set up tree as a dnd source (primarily to drag regions onto the arrange window):
	gtk_drag_source_set(tree, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);
	g_signal_connect (G_OBJECT(tree), "drag-data-get", G_CALLBACK(pool_drag_dataget), panel);

	//---------------------------------------------------------

	panel_pack(scroll, EXPAND_TRUE);

	pool_win_update();

	g_signal_connect(am_parts, "selection-change", G_CALLBACK(pool_on_part_selection_change), pool_win);
}


/*
 *  Update object sizes following a change in window dimensions.
 */
static void
pool_win_on_allocate (GtkWidget* win, GtkAllocation* allocation, gpointer user_data)
{
	static int      width_prev     = 0;
	static gboolean scrollbar_prev = false;
	static gboolean pixbuf_done    = false; // anti-feedback. Ensure we resize only once for each change.

	PoolWindow* pool = (PoolWindow*)user_data;
	if(!windows__verify_pointer((AyyiPanel*)pool, AYYI_TYPE_POOL_WIN)) return;

	dbg(2, "%i %i", allocation->width, allocation->height);
	if(allocation->width){
//		gtk_widget_set_size_request(pool->scrollwindow, allocation->width, allocation->height);
	}

	if(pool_is_resizing) return; //dont redo the pixbufs whilst resizing the treeview.

#ifdef HAVE_GTK_2_10
	// calling of this function should be delayed so that the scrollbars have had time to be created/removed.
	int scrollbar_visible = GTK_WIDGET_VISIBLE(gtk_scrolled_window_get_vscrollbar(GTK_SCROLLED_WINDOW(pool->scrollwindow)));
	dbg (2, "scrollbar_visible=%i.", scrollbar_visible);
#else
	int scrollbar_visible = 0;
#endif

	if((win->allocation.width != width_prev || scrollbar_visible != scrollbar_prev) && !pixbuf_done){

#if 0
		int pool_width = pool->scrollwindow->allocation.width;
		int max_pool_width = win->allocation.width - 10;
		if(pool_width > max_pool_width) gtk_widget_set_size_request(pool->scrollwindow, max_pool_width, -1);
#endif 
		dbg (2, "allocation.width: %i --> %i", width_prev, win->allocation.width);

#if 0
		//get the current sizes:
		GdkRectangle cell_rect;
		gtk_tree_view_get_cell_area(GTK_TREE_VIEW(pool->treeview), NULL, pool->col2, &cell_rect);
#endif

		//resize the pixbufs:
		//drawing is delayed to give time for scrollbars to be drawn 
		g_idle_add((GSourceFunc)pool_win_pixbufs_update, pool);

		pixbuf_done = (win->allocation.width == width_prev);
		width_prev = win->allocation.width;
		scrollbar_prev = scrollbar_visible;
	}

	//after pixbuf widths are made smaller, this reduces the column width. It also causes a size-allocate.
	//gtk_tree_view_columns_autosize(GTK_TREE_VIEW(pool->treeview));
}


//this is a bodge - there is some padding on the cell that i havnt yet worked out:
#define REGION_BORDER 4

/*
 *  Called when the user opens or closes a row in the tree.
 *  This may need a change in the overview size.
 */
static void
pool_win_on_view_change (GtkTreeView* treeview, GtkTreeIter* iter, GtkTreePath* arg2, gpointer user_data)
{
	AyyiPanel* panel = (AyyiPanel*)user_data;
	g_return_if_fail(windows__verify_pointer(panel, AYYI_TYPE_POOL_WIN));
	PoolWindow* pool = (PoolWindow*)panel;

	GdkRectangle cell_rect;
	gtk_tree_view_get_cell_area(GTK_TREE_VIEW(treeview), NULL, pool->col2, &cell_rect);

	int old_pixbuf_width = cell_rect.width;
	int window_width = GTK_WIDGET(panel)->allocation.width; 
	int available_width = window_width - cell_rect.x - REGION_BORDER;

	if(old_pixbuf_width != available_width){
		pool_win_pixbufs_update(pool);
	}
}


static void
pool_win_update ()
{
	// As the treeviews are automatically linked to the model, there currently isnt much to do here.
}


/*
 *  Create new display pixbufs for the given Pool window following window change. 
 *  Any old pixbufs are destroyed.
 */
static void
pool_win_pixbufs_update (PoolWindow* pool)
{
	//overview widths:
	//-they should take up the full width of the visible part of the scrollwindow.
	//-if the scrollwindow is narrow, we give them a fixed width of POOL_PIXBUF_MIN_WIDTH.

	g_return_if_fail(pool->treeview);
	if(!GTK_WIDGET_REALIZED(pool->treeview)) return;

	// calculate the cell dimensions

	GdkRectangle cell_rect;
	gtk_tree_view_get_cell_area(GTK_TREE_VIEW(pool->treeview), NULL, pool->col2, &cell_rect);
	dbg (2, "col2: x=%i wid=%i.", cell_rect.x, cell_rect.width);
	//FIXME the cell width we are getting is too small!!
	//gtk_tree_view_get_visible_rect(GtkTreeView *tree_view, GdkRectangle *visible_rect);

#ifdef HAVE_GTK_2_10
	// calling of this function should be delayed so that the scrollbars have had time to be created/removed.
	int scrollbar_visible = GTK_WIDGET_VISIBLE(gtk_scrolled_window_get_vscrollbar(GTK_SCROLLED_WINDOW(pool->scrollwindow)));
	dbg (2, "scrollbar_visible=%i.", scrollbar_visible);
#else
	int scrollbar_visible = 0;
#endif

	int fix = 5;
	int width = pool->scrollwindow->allocation.width - cell_rect.x - REGION_BORDER - scrollbar_visible*15 - fix;
	if (width > 10000) { perr ("column too wide: %ipx.", width); return; }
	width = MAX(width, POOL_PIXBUF_MIN_WIDTH);
	pool->pixbuf_width = width;

	GtkTreeIter iter, child;  
	GtkTreeModel* treestore = GTK_TREE_MODEL(gui_pool->treestore);
	if (gtk_tree_model_get_iter_first(treestore, &iter)){

		//iterate over each row, updating each overview pixbuf:
		int i;
		int fnum = 0; //peakfile index.
		while(1){
			AMPoolItem* pool_item = g_list_nth_data(song->pool->list, fnum);

			// set the new pixbufs into the store

			// first the pixbuf for the whole file
			pool_win_pixbuf_cell_update(pool, &iter, pool_item, -1);

			// and child rows (regions)
			dbg (2, "updating regions...");
			int regions = gtk_tree_model_iter_n_children(treestore, &iter);
			for(i=0;i<regions;i++){
				if(gtk_tree_model_iter_nth_child(treestore, &child, &iter, i)){
					pool_win_pixbuf_cell_update(pool, &child, pool_item, -1); // -1 means use style colour.
				}
			}

			if(!gtk_tree_model_iter_next(treestore, &iter)) break;
			fnum++;
		}
	}
	gtk_tree_view_columns_autosize(GTK_TREE_VIEW(pool->treeview)); //testing!!
}


#ifdef NEVER
static AyyiRegion*
pool_iter_get_fullregion (GtkTreeIter* iter)
{
	//@param iter: must point to a top level row (file)

	//lets work on the assumption that the fullregion is the first one for the file.

	GtkTreeModel* model = GTK_TREE_MODEL(gui_pool->treestore);

	//int regions = gtk_tree_model_iter_n_children(treestore, &iter);
	if(gtk_tree_model_iter_has_child(model, iter)){
		if(gtk_tree_model_iter_next(model, iter)){
			uint32_t region_idx = 0;
			gtk_tree_model_get(model, iter, COL_AYYI_IDX, &region_idx, -1);
			return ayyi_song__audio_region_at(region_idx);
		}
	}

	return NULL;
}
#endif


/*
 *  Redraws a single overview treecell.
 *
 *  @param palette is temporary until we link parts with regions properly.
 *          (if pallette_num < 0, we use a default colour instead.)
 *  @param iter is the child (region) iter.
 *
 *  Currently the pixbufs are stored in the shared treemodel. This doesnt work properly for multiple windows of different sizes.
 */
void
pool_win_pixbuf_cell_update (PoolWindow* window, GtkTreeIter* iter, AMPoolItem* pool_item, int palette_num)
{
	g_return_if_fail(window);
	g_return_if_fail(pool_item);
	if(pool_item->state != AM_POOL_ITEM_OK) return;

	// attempt to access old pixbuf (is there one?)
	GdkPixbuf* old_pixbuf = NULL;
	AyyiIdx region_idx = 0;
	gtk_tree_model_get(GTK_TREE_MODEL(gui_pool->treestore), iter, COL_OVERVIEW, &old_pixbuf, COL_AYYI_IDX, &region_idx, -1);
	if(old_pixbuf) g_object_unref(old_pixbuf);
	if(pool_model_iter_is_top_level(iter)){
		region_idx = AM_REGION_FULL;
	}else{
		if(region_idx < 0) return; // possibly a local part. nothing to draw.
	}

	AyyiRegion* region = (region_idx == AM_REGION_FULL)
		? NULL
		: ayyi_song__audio_region_at(region_idx);

	AMPart* part = region
		? am_collection_find_by_idx((AMCollection*)song->parts, region_idx)
		: NULL;

	if(region && (region->flags & deleted)){
		pwarn("region is deleted: %i", region_idx);
		return;
	}

	int width  = window->pixbuf_width;
	int height = POOL_PIXBUF_HEIGHT;
	g_return_if_fail(width > 4 && width < 10000);

	GtkStyle* style = gtk_rc_get_style((GtkWidget*)((AyyiPanel*)window)->window);
	GdkColor bg_colour = style->base[GTK_STATE_NORMAL];

	uint32_t fg_colour = palette_num >= 0
		? song->palette[palette_num]
		: part && part->fg_is_set
			? part->fg_colour
			: song->palette[0];

	GdkPixbuf* pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE, BITS_PER_PIXEL, width, height);
#if 0
					void finalize_notify(gpointer data, GObject* was)
					{
						dbg(0, "...");
					}
					g_object_weak_ref((GObject*)pixbuf, finalize_notify, NULL);
#endif

	WfSampleRegion r = {0,};
	if(region_idx == AM_REGION_FULL){
		r.len = waveform_get_n_frames((Waveform*)pool_item);
	}else{
		if (region) {
			r.len = region->length;
			r.start = region->start;
		} else {
			pwarn("cannot get region: %i", region_idx);
			return;
		}
	}

	void pool_win_pixbuf_ready (Waveform* w, GdkPixbuf* pixbuf, gpointer _path)
	{
		GtkTreePath* path = _path;
		GtkTreeIter iter;
		if(gtk_tree_model_get_iter((GtkTreeModel*)gui_pool->treestore, &iter, path)){
			gtk_tree_store_set(gui_pool->treestore, &iter, COL_OVERVIEW, pixbuf, -1);
		}

		// at this pt, refcount should be two, we make it 1 so that pixbuf is destroyed with the row
		g_object_unref(pixbuf);

		gtk_tree_path_free(path);
	}

	dbg(2, "region=%i-->%i len=%"PRIu64, r.start, r.start + r.len, r.len);
	if(r.len){
		GtkTreePath* path = gtk_tree_model_get_path(GTK_TREE_MODEL(gui_pool->treestore), iter);

		waveform_peak_to_pixbuf_async((Waveform*)pool_item, pixbuf, &r, fg_colour, am_gdk_to_rgba(&bg_colour), pool_win_pixbuf_ready, path);
	}
}


/*
 *  Remove all selected files from the pool (first core data, then gui data in the callback)
 *  -currently limited to working on selections in the pool window...
 */
static void
pool_win__remove_selected_files (GtkWidget* widget, GtkTreeView* treeview)
{
	GtkTreeIter iter;
	GtkTreeModel* model = gtk_tree_view_get_model(treeview);

	//what is selected?
	GtkTreeSelection* selection = gtk_tree_view_get_selection(treeview);
	GList* selectionlist = gtk_tree_selection_get_selected_rows(selection, &(model));
	g_return_if_fail(selectionlist);
	dbg (1, "%i rows selected.", g_list_length(selectionlist));

	for(GList* l=selectionlist;l;l=l->next){
		GtkTreePath* treepath_selection = l->data;
		//FIXME get filename or rowreference, and hence idx.......

		// check this is actually a file row, and not a region
		if(gtk_tree_path_get_depth(treepath_selection) != 1){ log_print(0, "Not deleted - selection not a file."); continue; }

		gtk_tree_model_get_iter(model, &iter, treepath_selection);
		gchar* fname;
		gtk_tree_model_get(model, &iter, COL_NAME, &fname, -1);

		AMPoolItem* pool_item = pool_path_get_item(treepath_selection);
		g_return_if_fail(pool_item);
		dbg (0, "file=%s idx=%x", fname, pool_item->pod_index[0]);

		am_song__remove_file(pool_item, NULL, NULL);
	}
	g_list_free(selectionlist);
}


#if 0
static gint
pool_drag_drop(GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, guint time, gpointer user_data)
{
  printf("drop!\n");

  return FALSE;
}
#endif


#ifdef NOT_USED
gboolean
dnd_get_dock_src2 (const char* uri, char* dock_src)
{
  //we expect a string of form: "dock:WINDOW_INSTANCE_STRING"

  dock_src[0] = '\0';

  dbg(2, "uri=%s", uri);

  char* window_type = strstr(uri, ":");
  if(!window_type){ pwarn ("cannot parse uri: '%s'.", uri); return FALSE; }

  window_type += 1;
  if(!window_type){ pwarn ("empty window type: '%s'.", uri); return FALSE; }

  int len = strlen(window_type);
  memcpy(dock_src, window_type, len);
  dock_src[len] = '\0';

  return TRUE;
}
#endif


static void
pool_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
  //this supports:
  //-colouring of overviews.
  //-docking of windows

  //possible data types:
  //1- a palette number from the colour window.
  //2- window instance from another window

  if(!data || data->length < 0){ perr ("no data!"); return; }

  printf("%s(): %s", __func__, data->data); //dont use dbg() here, we want it without extra carriage return.

  AyyiPanel* window = windows__get_panel_from_widget(widget);
  PoolWindow* pool = (PoolWindow*)window;

  switch(info){
    case GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING):
      dbg (0, "GDK_SELECTION_TYPE_STRING");
    case AYYI_TARGET_URI_LIST:
      dbg (3, "TARGET_URI_LIST len=%i", data->length);

      //did we drop on an existing item?
      GtkTreePath* path = NULL;
      gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(widget), x, y, &path, NULL, NULL, NULL);
      {
        //dbg (3, "no treepath - assume new drop...");

        GList* list = uri_list_to_glist((char*)data->data);
        //uri_list_to_utf8(const char *uri_list);
        int i = 0; 
        for(;list;list=list->next){
          char* u = list->data;
          dbg (2, "%i: %s", i, u);
          gchar* method_string;
          vfs_get_method_string(u, &method_string);
          dbg (0, "method=%s", method_string);

          if(!strcmp(method_string, "file")){
             char filename[256];
             if(dnd_get_filename(u, filename)){
               dbg (0, "file=%s", filename);
               pool_import_file(filename, NULL);
             }
          }

          else if(!strcmp(method_string, "colour")){
            if(path){
              //colour: we need the palette number only, which is passed directly.
              int palette_num = atoi((char*)data->data);
              if(!palette_num) { pwarn ("dnd: cannot parse colour data!"); return; }
              palette_num--;//restore zero indexing.

              //get row and childrow numbers:
              gint* indices = gtk_tree_path_get_indices(path);
              int fnum = indices[0]; //file index
              AMPoolItem* pool_item = g_list_nth_data(song->pool->list, fnum);

              GtkTreeIter iter;
              if(gtk_tree_model_get_iter(GTK_TREE_MODEL(gui_pool->treestore), &iter, path)){
                pool_win_pixbuf_cell_update(pool, &iter, pool_item, palette_num);
              }
              else perr ("iter not found.");
            }
            else log_print(0, "background colouring not supported for Pool window.");
          }

          else if(!strcmp(method_string, "dock")){
             ayyi_panel_on_drop_dock_item(window, u);
          }
          i++;
        }
        uri_list_free(list);
      }

      if(path) gtk_tree_path_free(path);
      break;
  }
}


/*
 *  Print to the statusbar of all pool windows.
 */
void
pool_win_statusbar_print_all (int n, char* s)
{
	g_return_if_fail(s);
	AyyiWindowClass* W = g_type_class_peek(AYYI_TYPE_WINDOW);
	if(!W->master_dock) return;

	GdlDockMaster* master = GDL_DOCK_MASTER(GDL_DOCK_OBJECT(W->master_dock)->master);
#if 0
	void callback(GtkWidget* widget, gpointer user_data)
	{
		PF;
	}
	bool include_controller = TRUE;
	gdl_dock_master_foreach_toplevel(master, include_controller, (GFunc)callback, NULL);
#endif

	GList* j = master->toplevel_docks;
	for(;j;j=j->next){
		GdlDockObject* object = GDL_DOCK_OBJECT(j->data);
		GList* sh = windows;
		for(;sh;sh=sh->next){
			AyyiWindow* window = sh->data;
			if((GdlDockObject*)window->dock == object){
				shell__statusbar_print_if_panel(window, n, AYYI_TYPE_POOL_WIN, s);
				break;
			}
		}
	}
}


static void
pool_win__remove_unused_regions (GtkWidget* widget, GtkTreeView* treeview)
{
	AyyiRegion* region = NULL;
	int n = 0;
	while((region = ayyi_song__audio_region_next(region))){
		AMPart* part = am_partmanager__get_by_id(region->id);
		if(!part){
			n++;
			ayyi_song_container_delete_item(&((AyyiSongService*)ayyi.service)->song->audio_regions, (AyyiItem*)region, NULL, NULL);
		}
	}
	log_print(0, "found %i unused regions", n);
}


static MenuDef _menu_def[] = {
    {"Add Part",              G_CALLBACK(pool_win__add_part_at_current),   "zoom_in",    true},
    {"Remove File",           G_CALLBACK(pool_win__remove_selected_files), "cross",      true},
    {"Remove Unused Regions", G_CALLBACK(pool_win__remove_unused_regions), "semiquaver", true},
};


/*
 * Create the context menu for pool window file entries.
 * -there is a menu per window, so we can pass the treeview to the callbacks.
 */
static GtkWidget*
pool_win_menu_init (GtkTreeView* treeview)
{
	g_return_val_if_fail(treeview, NULL);

	return add_menu_items_from_defn(gtk_menu_new(), _menu_def, G_N_ELEMENTS(_menu_def), treeview);
}


static gboolean
pool_view_on_button_press (GtkWidget* treeview, GdkEventButton* event, gpointer user_data)
{
	if(event->type == GDK_BUTTON_PRESS){
		switch(event->button){
			case 3:
				// select row if no row is selected or only one other row is selected

				;GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

				if(gtk_tree_selection_count_selected_rows(selection) <= 1){

					//get tree path for row that was clicked:
					GtkTreePath* path;
					if(gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview), (gint) event->x, (gint) event->y, &path, NULL, NULL, NULL)){
						gtk_tree_selection_unselect_all(selection);
						gtk_tree_selection_select_path(selection, path);
						gtk_tree_path_free(path);
					}
				}

				pool_win_show_popupmenu(treeview, event, user_data);

				return HANDLED;

			case 1:
				;GtkTreePath* path;
				if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview), (gint)event->x, (gint)event->y, &path, NULL, NULL, NULL)) {
  					PoolWindow* pool = (PoolWindow*)windows__get_panel_from_widget(treeview);

					// auditioning
					GdkRectangle rect;
					gtk_tree_view_get_cell_area(GTK_TREE_VIEW(treeview), path, pool->col2, &rect);
					if (((gint)event->x > rect.x) && ((gint)event->x < (rect.x + rect.width))) {

						// overview column
						dbg(1, "overview. column rect: %i %i %i %i", rect.x, rect.y, rect.width, rect.height);

						AMPoolItem* pool_item = pool_path_get_item(path);
						if (pool_item) {
							auditioner_toggle(pool_item);
						}
					}

					gtk_tree_path_free(path);
				}
				break;
			default:
				break;
		}
	}

	return NOT_HANDLED;
}


static void
pool_win_show_popupmenu (GtkWidget* treeview, GdkEventButton* event, gpointer userdata)
{
	PF;

	PoolWindow* window = (PoolWindow*)windows__get_panel_from_widget(treeview);
	g_return_if_fail(window);

	//note: event can be NULL here when called from view_onPopupMenu;
	//      gdk_event_get_time() accepts a NULL argument
	gtk_menu_popup(GTK_MENU(window->menu), 
	               NULL, NULL, NULL, NULL,
	               (event != NULL) ? event->button : 0,
	               gdk_event_get_time((GdkEvent*)event));

	gboolean _update_menu (gpointer _window)
	{
		PF;
		PoolWindow* window = _window;
		GtkMenu* menu = (GtkMenu*)window->menu;
		GList* list = gtk_container_get_children(GTK_CONTAINER(menu));
		GList* l = list;
		int i = 0;
		for(;l;l=l->next){
			GtkWidget* item = l->data;
			switch(i){
				case 0 ... 2:
					;GtkTreeIter iter;
					bool empty = !gtk_tree_model_get_iter_first(GTK_TREE_MODEL(gui_pool->treestore), &iter);
		    		gtk_widget_set_sensitive(item, !empty);
					break;
				default:
					break;
			}
			i++;
		}
		g_list_free(list);

		return G_SOURCE_REMOVE;
	}

	g_timeout_add(10, _update_menu, window);
}


static AMPoolItem*
pool_path_get_item (GtkTreePath* treepath)
{
  //get the pool_item for the given treepath, using the row_reference.

  AMPoolItem* poolitem = NULL;

  //look thru each poolitem to look for the given treepath:
  GList* l = song->pool->list;
  for(;l;l=l->next){
    AMPoolItem* testitem = (AMPoolItem*)l->data;
    GtkTreeRowReference* row_ref = testitem->gui_ref; 
    GtkTreePath* path = gtk_tree_row_reference_get_path(row_ref);

    //get something to compare (using the string may not be the fastest):
    gchar* str1 = gtk_tree_path_to_string(treepath);
    gchar* str2 = gtk_tree_path_to_string(path);

    if(!strcmp(str1, str2)){ 
      poolitem = testitem;
    }
    gtk_tree_path_free(path);

    if(poolitem) break;
  }

  if(!poolitem) pwarn ("poolitem not found!");
  return poolitem;
}


static void
pool_view_row_contents_delete (GtkWidget* not_used, GtkTreeView* treeview)
{
	//a popupmenu menu callback.

	GtkTreeSelection* selection = NULL;
	GtkTreeIter iter;
	GtkTreeModel* model = gtk_tree_view_get_model(treeview);

	//what is selected?:
	//gtk_tree_selection_count_selected_rows();
	selection = gtk_tree_view_get_selection(treeview);
	GList* selectionlist = gtk_tree_selection_get_selected_rows(selection, &(model));
	if(!selectionlist){ perr ("no files selected?"); return; }
	dbg (0, "%i rows selected.", g_list_length(selectionlist));

	GList* l = selectionlist;
	for(;l;l=l->next){
		GtkTreePath* treepath_selection = l->data;
		gtk_tree_model_get_iter(model, &iter, treepath_selection);

		//note: we cant use the row ref here. It can only "go one way"

		//check this is actually a file row, and not a region:
		if(gtk_tree_path_get_depth(treepath_selection) != 1){
			log_print(0, "selection not a file.");
			uint32_t region_index;
			gtk_tree_model_get(model, &iter, COL_AYYI_IDX, &region_index, -1);
			//its possible the region has no parts, in which case it wont get deleted.
			AMPart* part = am_collection_find_by_idx((AMCollection*)song->parts, region_index);
			if(part)
				song_part_delete(part);
			else{
				log_print(0, "Deleting region with no parts...");
				ayyi_song__delete_audio_region(ayyi_song__audio_region_at(region_index));
			}
			continue;
		}

		gchar* fname;
		gtk_tree_model_get(model, &iter, COL_NAME, &fname, -1);

		AMPoolItem* pool_item = pool_path_get_item(treepath_selection);
		g_return_if_fail(pool_item);

		//does this file have any regions?
		GList* regions = pool_item_get_regions(pool_item);
		if(regions){
			log_print(LOG_FAIL, "file has regions - wont delete (%s)", fname);
			g_list_free(regions);
		}else{
			dbg (0, "file=%s", fname);
			am_song__remove_file(pool_item, NULL, NULL); //check this is correct - it was pool_remove_file().
		}
	}
	g_list_free(selectionlist);
}


/*
 *  One or more rows has been selected.
 *  We need to update the global selection info.
 */
static void
pool_win_on_row_selected (GtkTreeView* treeview, gpointer user_data)
{
	//TODO this is connected to "cursor-changed". It might be better to use the tree_selection "changed" signal.
	PF;

	AyyiPanel* window = windows__get_panel_from_widget(GTK_WIDGET(treeview));
	AyyiPanel* p = window;
	if (!p->link) return;

	GtkTreeModel* model = gtk_tree_view_get_model(treeview);
	GtkTreeSelection* selection = gtk_tree_view_get_selection(treeview);

	if (!GTK_IS_TREE_SELECTION(selection)) return;
	//-perhaps selecting row0 on windowcreation will fix this issue....

	int n_selected = gtk_tree_selection_count_selected_rows(selection);
	dbg(1, "selection_count=%i.", n_selected);
	if (!gtk_tree_selection_count_selected_rows(selection)) return;

	GList* rowlist = gtk_tree_selection_get_selected_rows(selection, NULL);
	if (!rowlist) return;

	AyyiIdx selected_file = -1;
	GList* selected_parts = NULL;
	GList* l = rowlist;
	// careful, row list is not NULL terminated
	for (int i=0;i<n_selected;i++) {
		GtkTreePath* path;
		if ((path = l->data)) {
			AMPart* part = NULL;
			GtkTreeIter iter;
			if (gtk_tree_model_get_iter(model, &iter, (GtkTreePath*)(l->data))) {
				AyyiIdx pod_index;
				gtk_tree_model_get(model, &iter, COL_AYYI_IDX, &pod_index, -1);
				if (!pool_model_path_is_top_level(path)) {
					// many regions may have no AMPart.
					part = am_song__get_part_by_region_index(pod_index, AYYI_AUDIO);
					dbg (1, "non toplevel row: got part %p for region_idx=%u", part, pod_index);
					dbg (2, "partlist size: %i", g_list_length(song->parts->list));
				} else {
					selected_file = pod_index;
				}

if (pool_model_path_is_top_level(path) && pod_index < 0) {
	pwarn("failed to get file_idx");
} else {
				pool_win_statusbar_update((PoolWindow*)window, &iter, part, pod_index);
}
			}
			if (part) selected_parts = g_list_prepend(selected_parts, part);
		}
		l = l->next;
	}
	g_list_foreach (rowlist, (gpointer)gtk_tree_path_free, NULL);
	g_list_free (rowlist);

	if (selected_parts) {
		am_collection_selection_replace ((AMCollection*)song->parts, selected_parts, window);
		//ownership of selected_parts is transferred to the model. we do not free it here.
	} else {
		if (selected_file > -1) {
			AMPoolItem* item = am_pool__get_item_from_idx(selected_file);
			if (item)
				am_collection_selection_replace ((AMCollection*)song->pool, g_list_prepend(NULL, item), NULL);
		}
	}
}


void
pool_win_statusbar_update (PoolWindow* pool_window, GtkTreeIter* iter, AMPart* part, AyyiIdx file_index)
{
	//show information about the given row in the pool window statusbar.

	char* msg = NULL;

	if(pool_model_iter_is_top_level(iter)){
		//row is a file, not a region.
		g_return_if_fail(file_index > -1);
		AyyiFilesource* file = ayyi_song__filesource_at(file_index);
		if(file){
			AMPoolItem* pool_item = am_pool__get_item_by_id(file->id);
			if(!pool_item){
				pwarn("pool_item lookup failed for file->id=%"PRIu64, file->id);
#ifdef DEBUG
				am_pool__print();
#endif
				return;
			}
			char bbst[64]; ayyi_samples2bbst(((Waveform*)pool_item)->n_frames, bbst);

			char filename_rhs[128] = " / ";
			if(!ayyi_file_get_other_channel(file, filename_rhs+3, 125)){
				filename_rhs[0] = '\0';
			}

			msg = g_strdup_printf("%s%s / corelength=%u (%"PRIi64") %s %s / idx=%u", file->name, filename_rhs, file->length, ((Waveform*)pool_item)->n_frames, bbst, am_format_channel_width(pool_item->channels), file_index);
		}
	} else if(part){ // a region is selected. We cant show Part information for Regions that dont represent Parts.
		msg = g_strdup_printf("%s length=%u region_start=%u", part->name, ayyi_pos2samples(&part->length), part->region_start);
	} else msg = g_strdup_printf("%s", "");

	shell__statusbar_print_if_panel(((AyyiPanel*)pool_window)->window, 1, AYYI_TYPE_POOL_WIN, msg);

	g_free(msg);
}


static void
pool_drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, AyyiPanel* panel)
{
	// for _outgoing_ drags.
	// we provide the droppee with the either filename or the regionname for the current row, depending on which kind of row it is.

	PF;

	#define ROW_STR_LEN 247
	char text[256], row_str[ROW_STR_LEN] = "";
	gchar* fname = NULL;
	int region_idx = 0;
	GtkTreeIter iter;

	g_return_if_fail(panel);
	PoolWindow* pool_win = (PoolWindow*)panel;

	// which rows are selected?
	GtkTreeModel* model = gtk_tree_view_get_model(GTK_TREE_VIEW(pool_win->treeview));
	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(pool_win->treeview));
	GList* selected_rows = gtk_tree_selection_get_selected_rows(selection, &(model));

	//get the data associated with each of the selected rows:
	GList* row = selected_rows;
	for(; row; row=row->next){
		GtkTreePath* treepath_selection = row->data;
		gtk_tree_model_get_iter(model, &iter, treepath_selection);
		gtk_tree_model_get(model, &iter, COL_NAME, &fname, COL_AYYI_IDX, &region_idx, -1); //get the filename and index for this row.
		dbg(1, "  filename=%s poolitem=%p idx=%i", fname, pool_path_get_item(treepath_selection), region_idx);
		if(pool_model_iter_is_top_level(&iter)){
			snprintf(row_str, ROW_STR_LEN - 1, "file=%i", region_idx);
		}
		else snprintf(row_str, ROW_STR_LEN - 1, "region=%i", region_idx);
	}

	g_list_foreach(selected_rows, (gpointer)gtk_tree_path_free, NULL);
	g_list_free(selected_rows);

	//TODO allow sending of multiple items: append them separated by newlines.
	snprintf(text, 255, "pool:%.240s%c%c", row_str, 13, 10);
	if(fname) g_free(fname);

	//-----------------------------------------------------

	if(!strlen(row_str)){
		//return a dock string instead.
		//-this sets up "internal" docking, which is deprecated as it doesnt show the target box while dragging.
		pwarn("using deprecated internal docking.");
		char win_id[STRLEN_WIN_ID];
		ayyi_panel_get_id_string(panel, win_id);
		snprintf(text, 255, "dock:%s%c%c", win_id, 13, 10);
	}

	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (unsigned char*)text, strlen(text));
}


typedef struct _region
{
  AMPoolItem*     pool_item;
  uint32_t        region_idx;
  AyyiFilesource* file;       //if set, indicates that region_idx is not valid. (would be better to make region_idx a pointer and use the pool_item->pod_idx instead)
} PoolRegion;


/*
 *  Return a newly allocated list of PoolRegion*. Free the list with free_poolregion_list();
 */
static GList*
pool_get_selected_regions (GtkTreeView* treeview)
{
	GList* regions = NULL;

	GtkTreeModel*     model     = gtk_tree_view_get_model(treeview);
	GtkTreeSelection* selection = gtk_tree_view_get_selection(treeview);
	GList*            rowlist   = gtk_tree_selection_get_selected_rows(selection, NULL);

	for(; rowlist; rowlist = rowlist->next){
		GtkTreePath* path = (GtkTreePath*)(rowlist->data);
		GtkTreeIter iter;
		if(gtk_tree_model_get_iter(model, &iter, path)){
			PoolRegion* region = g_new0(PoolRegion, 1);

			region->pool_item = pool_path_get_item(path);

			gtk_tree_model_get(model, &iter, COL_AYYI_IDX, &region->region_idx, -1);

			if(pool_model_iter_is_top_level(&iter)){
				region->file = ayyi_song__filesource_at(region->pool_item->pod_index[0]);
			}
			regions = g_list_append(regions, region);
			//pool_win_statusbar_update((PoolWindow*)windata->data, &iter, gpart, pod_index);
		}
	}

	dbg (1, "%i regions selected.", g_list_length(regions));
	return regions;
}


static void
free_poolregion_list (GList* regions)
{
	GList* l = regions;
	for(;l;l=l->next){
		g_free((PoolRegion*)l->data);
	}
	g_list_free(regions);
}


/*
 *  Add a Part for the currently selected pool window item at current spp and track.
 */
static void
pool_win__add_part_at_current (GtkWidget* widget, GtkTreeView* treeview)
{
	PF;
	GList* regions = pool_get_selected_regions(treeview);
	if(regions){
		AMTrack* tr = am_collection_selection_first(am_tracks);

		GPos pos;
		am_transport_get_pos(&pos);

		GList* l = regions;
		for(;l;l=l->next){
			PoolRegion* region = l->data;

			AMPoolItem* pool_item = region->pool_item;
			AyyiIdent region_ident = {AYYI_OBJECT_AUDIO_PART, region->region_idx};
			AMTrack* trk = tr ? tr : song->tracks->track[0];
			long long len = 0;
			unsigned colour = 0;
			song_part_new(pool_item, region->file ? NULL : &region_ident, &pos, trk, len, NULL, colour, NULL, NULL, NULL);
		}
		free_poolregion_list(regions);
	}
}


static void
pool_delete_k (GtkWidget* accel_group, gpointer data)
{
	PF;
	if(!GTK_IS_ACCEL_GROUP(accel_group)){ perr ("not accel_group!"); return; }

	AyyiPanel* panel = windows__get_active();
	if(AYYI_IS_POOL_WIN(panel)){
		pool_view_row_contents_delete(NULL, (GtkTreeView*)((PoolWindow*)panel)->treeview);
	}
}


static void
pool_on_part_selection_change (GObject* am_song, AyyiPanel* sender, PoolWindow* pool_win)
{
	void pool_win__on_selection_change (AyyiPanel* window)
	{
		// we need to update the window in response to selection changes elsewhere.

		// currently if there is no selection, we clear the pool selection - is this the correct thing to do??

		PoolWindow* pool_win = (PoolWindow*)window;
		AyyiPanel* p = window;
		if(!p->link){ dbg (2, "not linked."); return; }

		if(gui_pool->update_pending) return;

		GtkTreeModel* model = GTK_TREE_MODEL(gui_pool->treestore);

		GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(pool_win->treeview));
		gtk_tree_selection_unselect_all(selection);

		GList* selected = am_parts_selection;
		for(;selected;selected=selected->next){
			AMPart* part = selected->data;
			if(!part->pool_item) continue;

			GtkTreePath* path = pool_model_lookup_by_region((AyyiAudioRegion*)part->ayyi);
			if(path){
				gtk_tree_selection_select_path(selection, path);
				GtkTreeIter iter;
				if(gtk_tree_model_get_iter(model, &iter, path)) pool_win_statusbar_update(pool_win, &iter, part, -1);
				//dbg (0, "num rows selected: %i", gtk_tree_selection_count_selected_rows(selection));
				gtk_tree_path_free(path);
			}else{
				pwarn ("selection not found in pool model: idx=%i id=%"PRIu64, part->ayyi->shm_idx, part->ayyi->id);
#ifdef DEBUG
				am_pool__print();
#endif
			}
		}
	}

	if((AyyiPanel*)pool_win != sender){
		pool_win__on_selection_change((AyyiPanel*)pool_win);
	}
}


