//----------------------------------------------------------------------------
//
//  This file is part of seq24.
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------

#ifndef SEQ24_SEQEDIT
#define SEQ24_SEQEDIT

#include <panels/panel.h>
#include "seq24/sequence.h"

G_BEGIN_DECLS

#define AYYI_TYPE_MIDI_WIN            (ayyi_midi_win_get_type ())
#define AYYI_MIDI_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_MIDI_WIN, AyyiMidiWin))
#define AYYI_MIDI_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_MIDI_WIN, AyyiMidiWinClass))
#define AYYI_IS_MIDI_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_MIDI_WIN))
#define AYYI_IS_MIDI_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_MIDI_WIN))
#define AYYI_MIDI_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_MIDI_WIN, AyyiMidiWinClass))

typedef struct _AyyiMidiWinClass AyyiMidiWinClass;

//temp hacks:
typedef void *perform;
typedef void *mainwid;
typedef GtkWidget *tseqkeys;

struct _AyyiMidiWinClass {
	AyyiPanelClass parent_class;
};

//has a seqroll and piano roll.
struct _AyyiMidiWin
{
    AyyiPanel  panel;

  //private:
 
    GtkWidget  *m_menubar;

    GtkWidget  *m_menu_tools;
    GtkWidget  *m_menu_zoom;
    GtkWidget  *m_menu_snap;
    GtkWidget  *m_menu_note_length;

    //length in measures:
    GtkWidget  *m_menu_length;
    GtkWidget  *m_menu_midich;
    GtkWidget  *m_menu_midibus;
    GtkWidget  *m_menu_data;
    GtkWidget  *m_menu_key;
    GtkWidget  *m_menu_scale;
    GtkWidget  *m_menu_sequences;

    //time signature, beats per measure, beat width:
    GtkWidget  *m_menu_bpm;
    GtkWidget  *m_menu_bw;

    Sequence   *m_seq;
    int         m_pos;
  
    //seqroll    *m_seqroll_wid;
    tseqkeys   *m_seqkeys_wid;
    /*
    seqdata    *m_seqdata_wid;
    seqtime    *m_seqtime_wid;
    seqevent   *m_seqevent_wid;
    */

    GtkWidget  *m_table;
    //GtkWidget  *m_vbox;
    GtkWidget  *m_hbox;
    GtkWidget  *m_hbox2;
    GtkWidget  *m_hbox3;

    //Viewport   *m_keysview;
    //Viewport   *m_rollview;
    //Viewport   *m_dataview;
    //Viewport   *m_timeview;
    //Viewport   *m_eventview;

    GtkWidget *m_vscroll;
    GtkWidget *m_hscroll;

    GtkObject *m_vadjust;
    GtkObject *m_hadjust;
    GtkWidget *m_vscroll_new;
    GtkWidget *m_hscroll_new;
    
    GtkWidget  *m_button_undo;
    
    GtkWidget  *m_button_tools;

    GtkWidget   *m_button_sequence;
    GtkWidget   *m_entry_sequence;
    
    GtkWidget   *m_button_bus;
    GtkWidget   *m_entry_bus;
    
    GtkWidget   *m_button_channel;
    GtkWidget   *m_entry_channel;
    
    GtkWidget   *m_button_snap;
    GtkWidget   *m_entry_snap;
    
    GtkWidget   *m_button_note_length;
    GtkWidget   *m_entry_note_length;
    
    GtkWidget   *m_button_zoom;
    GtkWidget   *m_entry_zoom;
    
    GtkWidget   *m_button_length;
    GtkWidget   *m_entry_length;
    
    GtkWidget   *m_button_key;
    GtkWidget   *m_entry_key;
    
    GtkWidget   *m_button_scale;
    GtkWidget   *m_entry_scale;

    GtkWidget   *m_tooltips;

    GtkWidget   *m_button_data;
    GtkWidget   *m_entry_data;

    GtkWidget   *m_button_bpm;
    GtkWidget   *m_entry_bpm;

    GtkWidget   *m_button_bw;
    GtkWidget   *m_entry_bw;

    GtkWidget   *m_toggle_play;
    GtkWidget   *m_toggle_record;
    GtkWidget   *m_toggle_thru;

    GtkWidget   *m_entry_name;

    /* the zoom 0  1  2  3  4  
                 1, 2, 4, 8, 16 */
    int         m_zoom;
    /*static */int  m_initial_zoom;

    //set snap to in pulses, off = 1
    int         m_snap;
    /*static*/ int  m_initial_snap;

    int         m_note_length;
    /*static*/ int  m_initial_note_length;

    //music scale and key
    int         m_scale;
    /*static */int  m_initial_scale;

    int         m_key;
    /*static*/ int  m_initial_key;

    int         m_sequence;
    /*static*/ int  m_initial_sequence;

    long        m_measures;

    //what is the data window currently editing?
    unsigned char m_editing_status;
    unsigned char m_editing_cc;
};


void seqedit_set_part (MidiWindow*, AMPart*);

void seqedit_set_zoom(int a_zoom);
void seqedit_set_snap(int a_snap);
void seqedit_set_note_length(int a_note_length);

void seqedit_set_bpm(int a_beats_per_measure );
void seqedit_set_bw(int a_beat_width );
void set_measures(int a_length_measures  );
void apply_length(int a_bpm, int a_bw, int a_measures );
long get_measures(void);

void set_midi_channel(int a_midichannel);
void set_midi_bus(int a_midibus);

void set_scale(int a_scale);
void set_key(int a_note);

void seqedit_set_background_sequence(int a_seq);
    
void name_change_callback( void );
void play_change_callback( void );
    void record_change_callback( void );
    void thru_change_callback( void );
    void undo_callback( void );

//void set_data_type(unsigned char a_status, unsigned char a_control = 0 );
void set_data_type(unsigned char a_status, unsigned char a_control); //FIXME init.

void update_all_windows( );

void fill_top_bar(void);
void create_menus(void);

void menu_action_quantise( void );
    
void seqedit_popup_menu(GtkWidget *a_menu);
void popup_event_menu( void );                                                                                                                                                                                                                                                                                                
    void popup_midibus_menu( void );
    void popup_sequence_menu( void );

//GtkWidget* create_menu_image( bool a_state = false );
GtkWidget* create_menu_image(gboolean a_state); //FIXME init.

G_END_DECLS
#endif
