/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "waveform/shader.h"

typedef struct {
	AGlShader    shader;
	struct {
		uint32_t colour;
		uint32_t border_colour;
		bool     selected;
		int      frame;
	}            uniform;
} PartShader;

enum {
   PART_SHADER_WIDTH = 0,
   PART_SHADER_HEIGHT,
   PART_SHADER_MAX
};

typedef struct {
	AGlShader      shader;
	struct {
		AGliPt     centre;
		float      radius;
	}              uniform;
} CircleShader;

#define CIRCLE_COLOUR() \
	(((AGlUniformUnion*)&circle_shader.shader.uniforms[0])->value.i[0])
#define CIRCLE_BG_COLOUR() \
	(((AGlUniformUnion*)&circle_shader.shader.uniforms[1])->value.i[0])

#define H_SCROLLBAR_COLOUR() \
	(((AGlUniformUnion*)&h_scrollbar_shader.shader.uniforms[0])->value.i[0])
#define H_SCROLLBAR_BG_COLOUR() \
	(((AGlUniformUnion*)&h_scrollbar_shader.shader.uniforms[1])->value.i[0])

extern PartShader part_shader;
extern PlainShader plain_shader;
extern AGlShader pattern_shader;
extern CircleShader circle_shader;
extern ScrollbarShader h_scrollbar_shader;
extern ScrollbarShader v_scrollbar_shader;
