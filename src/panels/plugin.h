/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <model/plugin.h>
#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_PLUGIN_WIN            (ayyi_plugin_win_get_type ())
#define AYYI_PLUGIN_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_PLUGIN_WIN, AyyiPluginWin))
#define AYYI_PLUGIN_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_PLUGIN_WIN, AyyiPluginWinClass))
#define AYYI_IS_PLUGIN_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_PLUGIN_WIN))
#define AYYI_IS_PLUGIN_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_PLUGIN_WIN))
#define AYYI_PLUGIN_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_PLUGIN_WIN, AyyiPluginWinClass))

typedef struct _AyyiPluginWinClass AyyiPluginWinClass;

struct _AyyiPluginWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiPluginWin
{
  AyyiPanel     panel;
  GtkTreeStore* treestore;
};

void       plugins_free();
gboolean   pluginbox_on_activate(GtkComboBox*, gpointer data);

GtkWidget* plugin_window_new (AyyiWindow*, Size*, GtkOrientation);

G_END_DECLS
