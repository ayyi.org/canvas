/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2005-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <glib.h>
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "global.h"
#ifndef DEBUG_TRACKCONTROL
#include "model/track.h"
#include "arrange/track.h"
#include "track_control_audio.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

static gpointer track_control_audio_parent_class = NULL;

GType track_control_audio_get_type (void);
enum  {
	TRACK_CONTROL_AUDIO_DUMMY_PROPERTY
};
static GObject*    track_control_audio_constructor (GType, guint, GObjectConstructParam*);


TrackControlAudio*
track_control_audio_construct (GType object_type)
{
	TrackControlAudio* self = g_object_newv (object_type, 0, NULL);
	return self;
}


TrackControlAudio*
track_control_audio_new (ArrTrk* atr, Arrange* arrange, gint spacing)
{
	TrackControlAudio* self = track_control_audio_construct (TYPE_TRACK_CONTROL_AUDIO);
	TrackControl* tc = (TrackControl*)self;
	AMTrack* tr = atr->track;
	dbg(2, "%s", tr->name);

	tc->atr     = atr; 
	tc->redraw  = track_control_redraw;
	tc->arrange = arrange;
	tc->brec    = NULL;
	tc->bmute   = NULL;
	tc->bsolo   = NULL;

	GTK_BOX(tc)->spacing = spacing;

	track_control_set_track(tc, atr);

	track_control_make_spacer (tc);
	track_control_make_widgets(tc);
	track_control_make_meter  (tc);

	track_control_set_colour(tc, am_track__get_colour(tr));

	return self;
}


static GObject*
track_control_audio_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_properties)
{
	TrackControlAudioClass* klass = TRACK_CONTROL_AUDIO_CLASS (g_type_class_peek (TYPE_TRACK_CONTROL_AUDIO));
	GObjectClass* parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	GObject* obj = parent_class->constructor (type, n_construct_properties, construct_properties);

	return obj;
}


static void
track_control_audio_class_init (TrackControlAudioClass* klass)
{
	track_control_audio_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->constructor = track_control_audio_constructor;
}


static void
track_control_audio_instance_init (TrackControlAudio* self)
{
}


GType
track_control_audio_get_type (void)
{
	static GType track_control_audio_type_id = 0;
	if (track_control_audio_type_id == 0) {
		static const GTypeInfo g_define_type_info = { sizeof (TrackControlAudioClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) track_control_audio_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (TrackControlAudio), 0, (GInstanceInitFunc) track_control_audio_instance_init, NULL };
		track_control_audio_type_id = g_type_register_static (TYPE_TRACK_CONTROL, "TrackControlAudio", &g_define_type_info, 0);
	}
	return track_control_audio_type_id;
}


#endif //DEBUG_TRACKCONTROL
