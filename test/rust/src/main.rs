/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2007-2018 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

extern crate libc;
extern crate glib;
extern crate glib_sys;

mod ayyi;

fn main() {
	unsafe { ayyi::am__init(std::ptr::null_mut()); }

	ayyi::connect_all(|error| {

		if !error.is_null() {
			println!("connection error: {:#?}", unsafe { std::ffi::CStr::from_ptr((*(error as *const ayyi::Error)).message)});
			return;
		}

		println!("Audio tracks:");

		let mut iter = ayyi::AMIter{..Default::default()};

		let tracks = unsafe { (*ayyi::song).tracks };
		let mut t = ayyi::_ModelCollection::from(tracks);
		unsafe { ayyi::model_collection_iter_init(& mut ayyi::ModelCollection::from(tracks), & mut iter) }
		loop {
			let track = unsafe { ayyi::model_collection_iter_next(& mut t, & mut iter) as * mut ayyi::AMTrack};
			if (track as ayyi::gpointer).is_null() { break }

			println!("  {:?}", unsafe {std::ffi::CStr::from_ptr((*track).name)});
		}
	});
}
