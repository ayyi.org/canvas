/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2005-2014 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __track_control_h__
#define __track_control_h__

#ifndef DEBUG_TRACKCONTROL

#include <gdk/gdk.h>
#include <gtk/gtkbox.h>

G_BEGIN_DECLS

#define TYPE_TRACK_CONTROL            (track_control_get_type ())
#define TRACK_CONTROL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TRACK_CONTROL, TrackControl))
#define TRACK_CONTROL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TRACK_CONTROL, TrackControlClass))
#define IS_TRACK_CONTROL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TRACK_CONTROL))
#define IS_TRACK_CONTROL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TRACK_CONTROL))
#define TRACK_CONTROL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TRACK_CONTROL, TrackControlClass))

typedef struct _TrackControl       TrackControl;
typedef struct _TrackControlClass  TrackControlClass;
typedef struct _TrackControlPriv   TrackControlPriv;

struct _TrackControl
{
    GtkBox          box;
    ArrTrk*         atr;
    Arrange*        arrange;

    GtkWidget*      bmute;
    GtkWidget*      brec;
    GtkWidget*      bsolo;
    GtkWidget*      fixed;
    GtkWidget*      spacer;
    GtkWidget*      lbl_rsiz;      // resize handle for the track height (event box).

    void (*redraw)    (TrackControl*); //supposed to be done at the class, not object level, but this seems more useful.
    void (*on_resize) (Arrange*);

    TrackControlPriv* priv;
};

struct _TrackControlClass
{
    GtkBoxClass parent_class;

    void (*set_height) (TrackControl*, int);
};


GType   track_control_get_type          (void) G_GNUC_CONST;
void    track_control_set_track         (TrackControl*, ArrTrk*);
void    track_control_redraw            (TrackControl*);
void    track_control_set_height        (TrackControl*, int height);
void    track_control_set_tname         (TrackControl*);
void    track_control_set_channel_name  (TrackControl*);
void    track_control_set_colour        (TrackControl*, int c_num);
void    track_control_draw_selected     (TrackControl*);
void    track_control_draw_unselected   (TrackControl*);
void    track_control_set_unmuted       (TrackControl*);
void    track_control_set_muted         (TrackControl*);
void    track_control_on_style_change   (TrackControl*);

void    track_control_drag_highlight    (GtkWidget*);
void    track_control_drag_unhighlight  (GtkWidget*);

void    track_control_disconnect        (GtkWidget*);

// Internal functions used by derived classes
void    track_control_make_spacer       (TrackControl*);
void    track_control_make_meter        (TrackControl*);
void    track_control_remove_meter      (TrackControl*);
void    track_control_make_widgets      (TrackControl*);

G_END_DECLS

#endif // DEBUG_TRACKCONTROL

#endif
