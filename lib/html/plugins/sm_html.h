/*
  Html rendering plugin using gtkhtml-3
  -------------------------------------

  This file is part of AyyiGtk. http://ayyi.org
  copyright (C) 2004-2009 Tim Orford <tim@orford.org>
  Originally based on code from Devhelp. http://live.gnome.org/devhelp

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#ifdef __dh_html_c__

typedef struct _DhHtml        DhHtml;
typedef struct _DhHtmlClass   DhHtmlClass;
typedef struct _DhHtmlPriv    DhHtmlPriv;

#define DH_TYPE_HTML        (dh_html_get_type ())
#define DH_HTML(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), DH_TYPE_HTML, DhHtml))
#define DH_HTML_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), DH_TYPE_HTML, DhHtmlClass))
#define DH_IS_HTML(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), DH_TYPE_HTML))
#define DH_IS_HTML_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), DH_TYPE_HTML))

struct _DhHtmlPriv {
  GtkWidget    *scrolled_window;
  gchar        *base_url;
};

struct _DhHtml {
  GtkHTML        parent;
  DhHtmlPriv    *priv;
};

struct _DhHtmlClass {
  GtkHTMLClass   parent_class;
  void           (*uri_selected) (DhHtml*, const gchar *uri);
};


#endif //__dh_html_c__
