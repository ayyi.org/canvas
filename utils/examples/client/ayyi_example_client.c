/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
 * A simple tutorial for Ayyi C clients.
 *
 * It demonstrates how to connect to a server, obtain and modify a Song object,
 * and how to be notified of remote model changes.
 * It runs in a shell, and outputs only debugging information, though changes
 * will be visible in other connected clients.
 *
 * For more advanced examples see the test/vala and model/test directories
 */
#include "config.h"
#include <stdlib.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "model/ayyi_model.h"

static void on_new_part  (AMPart*);
static void on_connected ();


int main (int argc, char *argv[])
{
	// Initialise the Ayyi Model library.
	am_init (NULL);

	// Connect to the default Ayyi service.
	// Once connection is complete, execution continues in on_connected().
	am_connect_all(on_connected, NULL);

	g_main_loop_run (g_main_loop_new (NULL, 0));

	return EXIT_SUCCESS;
}


static void
on_connected(GError* error, gpointer user_data)
{
	if (error){
		dbg (0, "server connection failed. %s", error ? error->message : "");
		exit(EXIT_FAILURE);
	}

	// We now have access to the Song object in shared memory
	// and can send and receive messages.

	// Add callbacks for server events.
	// In this example, the Parts 'add' signal will notify us when a new 
	// Part has been created, either by us, or another client.
	g_signal_connect(song->parts, "add", G_CALLBACK(on_new_part), NULL);

	ayyi_discover_clients();

	// get the list of Parts in the current Song

	GList* parts = song->parts->list;
	if (!parts) return;

	// move the first Part 1 bar later

	AMPart* part = parts->data;
	AyyiSongPos position = part->start;

	// offset the position by 4 beats, 0 sub-beats, and 0 ticks
	ayyi_pos_add (&position, &(AyyiSongPos){4, 0, 0});

	// request that the part be moved to the new position
	am_part_move (part, &position, part->track, NULL, NULL);
}


static void
on_new_part(AMPart* new_part)
{
	printf("new part created on server: %s\n", new_part->name);
}


