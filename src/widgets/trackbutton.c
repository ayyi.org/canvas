/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
  Ayyi Track Button
  -----------------

  This is a gtk widget derived from GtkButton.

  Its purpose is to to have a flat button for arrays of buttons which would 
  look ugly with the normal button bevelling.

  Button state:
   -There are 2 Selected states: that of the window Track selection, indicated by background colour,
    and Button Active, indicated by a change of icon.
    (actually similar to GtkTreeView or GtkListView(?))
   -TButton adds an "active" property identical to that in GtkToggleButton.

*/

#include "config.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gtk/gtksignal.h>
#include "debug/debug.h"
#include "gui_types.h"
#include "icon.h"
#include "trackbutton.h"

#define TBUTTON_DEFAULT_SIZE 18
#define GREEN_UNSET 0xfffe

extern SMConfig* config;

extern void get_style_bg_color                (GdkColor*, GtkStateType);
extern void widget_get_bg_colour              (GtkWidget*, GdkColor*, GtkStateType);

static void track_button_class_init           (GtkTButtonClass*);
static void track_button_init                 (TButton*);
static void track_button_destroy              (GtkObject*);
static void track_button_size_request         (GtkWidget*, GtkRequisition*);
static void track_button_realize              (GtkWidget*);
static gint track_button_expose               (GtkWidget*, GdkEventExpose*);
static gint track_button_enter_notify         (GtkWidget*, GdkEventCrossing*);
static gint track_button_leave_notify         (GtkWidget*, GdkEventCrossing*);

static void track_button_update               (TButton*);
static void track_button_real_button_released (GtkButton*);

//local data:
static gpointer* parent_class = NULL;


GType
track_button_get_type ()
{
  static GType type = 0;

  if(!type){
    static const GTypeInfo info =
    {
      sizeof(GtkTButtonClass),
      NULL,
      NULL,
      (GClassInitFunc)track_button_class_init,
      NULL,
      NULL,
      sizeof(TButton),
      0,
      (GInstanceInitFunc)track_button_init,
    };

    type = g_type_register_static(GTK_TYPE_BUTTON, "TButton", &info, 0);
  }

  return type;
}


static void
track_button_class_init (GtkTButtonClass *class)
{
  GtkObjectClass *object_class = (GtkObjectClass*)class;
  GtkWidgetClass *widget_class = (GtkWidgetClass*)class;
  GtkButtonClass *button_class = GTK_BUTTON_CLASS(class);

  parent_class = g_type_class_peek_parent(class);

  object_class->destroy = track_button_destroy;

  widget_class->size_request = track_button_size_request;
  widget_class->realize = track_button_realize;
  widget_class->expose_event = track_button_expose;
  widget_class->enter_notify_event = track_button_enter_notify;
  widget_class->leave_notify_event = track_button_leave_notify;

  button_class->clicked = track_button_real_button_released;  
}


static void
track_button_init (TButton *btn)
{
  btn->active = 0;
  btn->trk_selected = 0;

  btn->colour.red = 0;
  btn->colour.green = GREEN_UNSET;
  btn->colour.blue = 0;
}


GtkWidget*
track_button_new (gchar *icon_name)
{
	TButton* button = g_object_new(track_button_get_type(), NULL);

	button->pixbufs.normal = gtk_icon_theme_load_icon (icon_theme, icon_name, 16, 0, NULL);

	gchar* on_name = g_strdup_printf("%s-on", (char*)icon_name);
	button->pixbufs.active = gtk_icon_theme_load_icon (icon_theme, on_name, 16, 0, NULL);
	g_free(on_name);

	return GTK_WIDGET(button);
}


static void
track_button_destroy (GtkObject *object)
{
  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_TBUTTON(object));

#if 0 //icons are now cached - we dont delete them.
  TButton *btn = GTK_TBUTTON(object);

  if(btn->svg_pixbuf_normal){
      g_object_unref(G_OBJECT(btn->svg_pixbuf_normal));
      btn->svg_pixbuf_normal = NULL;
  }
  if(btn->svg_pixbuf_active){
      g_object_unref(G_OBJECT(btn->svg_pixbuf_active));
      btn->svg_pixbuf_active = NULL;
  }
#endif

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
track_button_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
  TButton* d = GTK_TBUTTON(widget);

  g_return_if_fail(GDK_IS_PIXBUF(d->pixbufs.normal));

  requisition->width = requisition->height = gdk_pixbuf_get_width(d->pixbufs.normal);
}


static void
track_button_realize(GtkWidget *widget)
{
  GdkWindowAttr attributes;
  gint attributes_mask;
  gint border_width;

  GtkButton *button = GTK_BUTTON (widget);
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  border_width = GTK_CONTAINER (widget)->border_width;

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x + border_width;
  attributes.y = widget->allocation.y + border_width;
  attributes.width = widget->allocation.width - border_width * 2;
  attributes.height = widget->allocation.height - border_width * 2;
  attributes.wclass = GDK_INPUT_ONLY;
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_BUTTON_PRESS_MASK |
                GDK_BUTTON_RELEASE_MASK |
                GDK_ENTER_NOTIFY_MASK |
                GDK_LEAVE_NOTIFY_MASK);

  attributes_mask = GDK_WA_X | GDK_WA_Y;

  widget->window = gtk_widget_get_parent_window (widget);
  g_object_ref (widget->window);

  button->event_window = gdk_window_new (gtk_widget_get_parent_window (widget),
                     &attributes, attributes_mask);
  gdk_window_set_user_data (button->event_window, button);

  widget->style = gtk_style_attach (widget->style, widget->window);

  //parent code ends here

  //--------------------------------

  //set the background colour, but only if not already set.
  TButton* tbtn = GTK_TBUTTON(widget);
  if(tbtn->colour.red == 0 && tbtn->colour.green == GREEN_UNSET){
    GdkColor colour;
    //widget_get_bg_colour(gtk_widget_get_toplevel(widget), &colour, GTK_STATE_NORMAL);
    widget_get_bg_colour(widget, &colour, GTK_STATE_NORMAL);
    tbtn->colour = colour;
  }
}


static gint
track_button_expose(GtkWidget *widget, GdkEventExpose *event)
{
  // draw the background, and copy the svg pixbuf to the widgets gdkwindow.
  // note: buttons do not have their own window, we draw on the parent.

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_TBUTTON(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);
  if (event->count > 0) return FALSE;
  TButton *button = GTK_TBUTTON(widget);

  int x      = event->area.x - event->area.x % widget->allocation.width + 1;
  int y      = event->area.y;
  int width  = event->area.width + event->area.x % widget->allocation.width;
  int height = event->area.height;

  if(GTK_WIDGET_DRAWABLE(widget)){
		//set background colour:
		GdkGC *gc = gdk_gc_new(GDK_DRAWABLE(widget->window)); //default settings.
		extern const char*  colour_to_string         (GdkColor*);
		GdkColor bgcolour; GTK_BUTTON(widget)->in_button
		                   ? get_style_bg_color(&bgcolour, GTK_STATE_PRELIGHT)
		                   : (button->trk_selected
		                     ? get_style_bg_color(&bgcolour, GTK_STATE_SELECTED)
		                       : (button->trk_muted
		                         ? widget_get_bg_colour(gtk_widget_get_toplevel(widget), &bgcolour, GTK_STATE_INSENSITIVE)
		                         : (bgcolour = button->colour)));
    gdk_gc_set_rgb_fg_color(gc, &bgcolour);
    gdk_draw_rectangle(GDK_DRAWABLE(widget->window), gc, TRUE, x, y, width, widget->allocation.height);
    g_object_unref(gc);

    GdkPixbuf *pixbuf = (button->active) ? button->pixbufs.active : button->pixbufs.normal;
    g_return_val_if_fail(pixbuf, FALSE);

    int distance_from_widget_top = y - widget->allocation.y;
    // copy the icon onto the pixmap buffer, vertically in the middle:
    int icon_y = (widget->allocation.height - gdk_pixbuf_get_width(pixbuf)) / 2 - distance_from_widget_top;

    if(widget->allocation.y != event->area.y){
      dbg(2, "height=%i area.y=%i alloc.y=%i distance_from_widget_top=%i icon_y=%i", height, y, widget->allocation.y, distance_from_widget_top, icon_y);
    }

    gdk_draw_pixbuf(widget->window,
                    widget->style->black_gc,   //according to api docs, this is ignored.
                    pixbuf,
                    0, 0,                      //src  x,y
                    x, y + icon_y,             //dest x,y
                    -1, -1,                    //width, height (-1 uses pixbuf width).
                    GDK_RGB_DITHER_NONE,
                    0, 0);                     //dither offsets.

    if (GTK_WIDGET_HAS_FOCUS (widget)) {
      GdkRectangle *area = &event->area;
      gtk_paint_focus(widget->style, widget->window, GTK_WIDGET_STATE (widget), area, widget, "button", x, y, width, height);
    }
  }

  return FALSE;
}


static void
track_button_update(TButton *button)
{
  
  g_return_if_fail (button != NULL);
  g_return_if_fail (GTK_IS_TBUTTON (button));

  gtk_widget_queue_draw(GTK_WIDGET(button));
}


void
track_button_set_active(GtkWidget *widget)
{
  // set button state to active/on (the word Active is consistent with its use in GtkToggleButton).

  TButton* button = GTK_TBUTTON(widget);
  button->active = TRUE;  
  track_button_update(button);
}


void
track_button_set_not_active(GtkWidget *widget)
{
  // set the button state to normal/off.

  TButton *button = GTK_TBUTTON(widget);
  button->active = FALSE;  
  track_button_update(button);
}


void
track_button_set_track_selected(GtkWidget *widget)
{
	g_return_if_fail(widget);

	TButton *button = GTK_TBUTTON(widget);
	button->trk_selected = TRUE;
	track_button_update(button);
}


void
track_button_set_track_unselected(GtkWidget *widget)
{
	g_return_if_fail(widget);

	TButton* button = GTK_TBUTTON(widget);
	button->trk_selected = FALSE;
	track_button_update(button);
}


void
track_button_set_track_muted(GtkWidget *widget, gboolean muted)
{
	g_return_if_fail(widget);

	TButton* button = GTK_TBUTTON(widget);
	button->trk_muted = muted;
	track_button_update(button);
}


static void 
track_button_real_button_released(GtkButton *button)
{
  // Attached to the 'pressed'/clicked signal.

  // Note: it appears this function has to return void.

  // Note: button state is set in the fn called by the signal_connected.
}


static gint 
track_button_enter_notify(GtkWidget *widget, GdkEventCrossing *event)
{
  GtkWidget* event_widget = gtk_get_event_widget((GdkEvent*) event);

  if((event_widget == widget) && (event->detail != GDK_NOTIFY_INFERIOR)){
      GTK_BUTTON(widget)->in_button = TRUE;
      TButton *btn = GTK_TBUTTON(widget);
      track_button_update(btn);
  }

  return FALSE;
}


static gint 
track_button_leave_notify(GtkWidget *widget, GdkEventCrossing *event)
{
  TButton* btn = GTK_TBUTTON(widget);

  GtkWidget* event_widget = gtk_get_event_widget((GdkEvent*) event);

  if((event_widget == widget) && (event->detail != GDK_NOTIFY_INFERIOR)){
      GTK_BUTTON(widget)->in_button = FALSE;
      track_button_update(btn);
  }

  return FALSE;
}


