/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <libgnomecanvas/libgnomecanvas.h>

struct _song_overview
{
	GtkWidget*         widget;
	GnomeCanvas*       canvas;
	GnomeCanvasItem*   citem;
};

GtkWidget*       arr_song_overview_new              (Arrange*);
void             arr_song_overview_destroy          (Arrange*);
void             arr_song_overview_queue_for_update ();
void             arr_song_overview_toggle           (Arrange*);

