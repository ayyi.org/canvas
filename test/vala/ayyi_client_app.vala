/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public delegate void ClientDelegateType ();

// this ends up in the vapi file?
//[CCode(cheader_filename = "ayyi_typedefs.h", cheader_filename = "ayyi_client_app.h")]
[CCode(cheader_filename = "ayyi_client_app.h")]


public class Ayyi.SimpleClient : GLib.Object
{
	public static SimpleClient app;
	public AyyiClient* client;
	private MainLoop loop;

	ClientDelegateType user_on_shm;

	public static bool f = false;

	public SimpleClient()
	{
		client = AM.init(null);
		//client.log.to_stdout = true;
	}

	public bool run(ClientDelegateType cb)
	{
		user_on_shm = cb;

		try {
			AM.connect_all(_on_shm);
		} catch(Error e){
			print("server connection failed.\n");
			print("%s\n", e.message);
			return false;
		}

		(app.loop = new MainLoop(null, false)).run();

		return true;
	}

	public void _on_shm()
	{
		if (client->got_shm) {
			user_on_shm();
		}else{
			print("failed to get shm.\n");
			quit();
		}
	}

	/*
	private bool connect_all()
	{
		bool connected = false;

		GLib.Error** error = null;
		if ((connected = AM.connect(client->service, error))){

			int type; for(type=1;type<=2;type++) ayyi_shm_seg_new(0, type);

			AM.get_shm_with_target(client->service, connect_all__on_shm);
		}
		return connected;
	}

	private void connect_all__on_shm()
	{
		if(test != 5) stdout.printf("!!!!! test != 5\n");

		if(service_shm_setup_is_complete()){
			am_song__load();
			on_shm();
		}
	}

	private bool service_shm_setup_is_complete()
	{
		//stdout.printf("shm.song=%p ayyi.service->amixer=%p\n", ayyi.service->song, ayyi.service->amixer);

		return (((bool)client->service->song) && (client->service->amixer != null));
	}
	*/

	public void echo(string str) //TODO varargs
	{
		print(str);
		print("\n");
	}

	public void quit()
	{
		if(loop != null){
			print("\n");
			loop.quit();
		}
		else Timeout.add(500, () => {
			print("\n");
			if(loop != null) loop.quit(); return true;
		});
	}
}


