/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 |  The Tracks actor displays the contents of each track. There is one child actor for each track.
 |  The track controls are not part of this actor
 */

static void gl_tracks_free (AGlActor*);

static AGlActorClass tracks_class = {0, "Tracks", (AGlActorNew*)arr_gl_tracks, gl_tracks_free};


static AGlActor*
arr_gl_tracks (GtkWidget* panel)
{
	bool arr_gl_tracks_paint (AGlActor* actor)
	{
		return true;
	}

	void gl_tracks_set_size (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;

		ArrTrk* atr = track_list__last_visible(arrange->priv->tracks);
		if(atr){
			actor->region.y2 = (atr->y + atr->height) * vzoom2(arrange);
		}
	}

#ifdef USE_FBO
	void arr_gl_tracks_invalidate (AGlActor* actor)
	{
		PF2;
		Arrange* arrange = actor->root->user_data;

		GList* l = CGL->actors[ACTOR_TYPE_TRACKS]->children;
		for(;l;l=l->next){
			AGlActor* actor = l->data;
			actor->cache.valid = false;
		}

		agl_actor__set_size(actor); // maybe resize fbo
	}
#endif

	void gl_tracks_init (AGlActor* actor)
	{
		if(agl->use_shaders && !part_shader.shader.program){
			agl_create_program(&part_shader.shader);
			part_shader.uniform.border_colour = 0x000000aa;
		}
	}

	AGlActor* actor = agl_actor__new(AGlActor,
		.class = &tracks_class,
		.init = gl_tracks_init,
		.paint = arr_gl_tracks_paint,
		.set_size = gl_tracks_set_size,
#ifdef USE_FBO
		.invalidate = arr_gl_tracks_invalidate,
#endif
		.region = (AGlfRegion){0, 0, 128000, 800}
	);

	return actor;
}


static void
gl_tracks_free (AGlActor* actor)
{
	g_free(actor);
}


TrackActor*
arr_gl_tracks_find_track (AGlActor* actor, ArrTrk* track)
{
	for (GList* l=actor->children;l;l=l->next) {
		TrackActor* ta = l->data;
		if (ta->atr == track) {
			return ta;
		}
	}
	return NULL;
}

