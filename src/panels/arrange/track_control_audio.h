/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2005-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __track_control_audio_h__
#define __track_control_audio_h__

#ifndef DEBUG_TRACKCONTROL
#include <glib.h>
#include <gtk/gtk.h>
#include "track_control.h"

G_BEGIN_DECLS


#define TYPE_TRACK_CONTROL_AUDIO (track_control_audio_get_type ())
#define TRACK_CONTROL_AUDIO(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TRACK_CONTROL_AUDIO, TrackControlAudio))
#define TRACK_CONTROL_AUDIO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TRACK_CONTROL_AUDIO, TrackControlAudioClass))
#define IS_TRACK_CONTROL_AUDIO(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TRACK_CONTROL_AUDIO))
#define IS_TRACK_CONTROL_AUDIO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TRACK_CONTROL_AUDIO))
#define TRACK_CONTROL_AUDIO_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TRACK_CONTROL_AUDIO, TrackControlAudioClass))

typedef struct _TrackControlAudio TrackControlAudio;
typedef struct _TrackControlAudioClass TrackControlAudioClass;
typedef struct _TrackControlAudioPrivate TrackControlAudioPrivate;

struct _TrackControlAudio {
	TrackControl              track_control;
	TrackControlAudioPrivate* priv;
};

struct _TrackControlAudioClass {
	TrackControlClass parent_class;
};


GType              track_control_audio_get_type  ();
TrackControlAudio* track_control_audio_new       (ArrTrk*, Arrange*, gint spacing);
TrackControlAudio* track_control_audio_construct (GType);


G_END_DECLS
#endif //DEBUG_TRACKCONTROL

#endif
