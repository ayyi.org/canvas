/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. https://www.ayyi.org          |
* | copyright (C) 2007-2023 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include "debug/debug.h"
#include "model/model_types.h"
#include "model/time.h"
#include "yaml/load.h"


static bool
_yaml_load (yaml_parser_t* parser, YamlHandler handlers[])
{
	int section = 0;

	int safety = 0;
	char key[64] = "";
	bool end = false;
	yaml_event_t event;

	get_expected_event(parser, &event, YAML_STREAM_START_EVENT);
	yaml_event_delete(&event);
	get_expected_event(parser, &event, YAML_DOCUMENT_START_EVENT);
	yaml_event_delete(&event);

	do {
		if (!yaml_parser_parse(parser, &event)) goto error; // Get the next event

		switch (event.type) {
			case YAML_STREAM_START_EVENT:
				perr("unexpected YAML_STREAM_START_EVENT");
				break;
			case YAML_STREAM_END_EVENT:
				end = TRUE;
				dbg(2, "YAML_STREAM_END_EVENT");
				break;
			case YAML_DOCUMENT_START_EVENT:
				dbg(2, "YAML_DOCUMENT_START_EVENT");
				break;
			case YAML_DOCUMENT_END_EVENT:
				end = TRUE;
				dbg(2, "YAML_DOCUMENT_END_EVENT");
				break;
			case YAML_ALIAS_EVENT:
				dbg(0, "YAML_ALIAS_EVENT");
				break;
			case YAML_SCALAR_EVENT:
				//dbg(0, "YAML_SCALAR_EVENT: value=%s %i plain=%i style=%i", event.data.scalar.value, event.data.scalar.length, event.data.scalar.plain_implicit, event.data.scalar.style);

				if(!key[0]){
					// 1st half of a pair
					strncpy(key, (char*)event.data.scalar.value, 63);
				}else{
					// 2nd half of a pair
					dbg(2, "      %s=%s", key, event.data.scalar.value);
					int i = 0;
					YamlHandler* h;
					while((h = &handlers[i]) && h->key ){
						if(!strcmp(h->key, key)){
							h->callback(parser, (char*)event.data.scalar.value, h->data);
							break;
						}
						i++;
					}
					key[0] = '\0';
				}
				break;
			case YAML_SEQUENCE_START_EVENT:
				dbg(2, "YAML_SEQUENCE_START_EVENT");
				break;
			case YAML_SEQUENCE_END_EVENT:
				dbg(2, "YAML_SEQUENCE_END_EVENT");
				break;
			case YAML_MAPPING_START_EVENT:
				if(key[0]){
					if(!section){
						dbg(2, "found section! %s", key);
						int i = 0;
						YamlHandler* h;
						while((h = &handlers[i]) && h->key ){
							if(!strcmp(h->key, key)){
								h->callback(parser, key, h->data);
								break;
							}
							i++;
						}
					}
					else dbg(2, "new section: %s", key);
					key[0] = '\0';
				}
				else dbg(2, "YAML_MAPPING_START_EVENT");
				//dbg(0, "YAML_MAPPING_START_EVENT: anchor=%s tag=%s", event.data.mapping_start.anchor, event.data.mapping_start.tag);
				break;
			case YAML_MAPPING_END_EVENT:
				dbg(2, "YAML_MAPPING_END_EVENT");
				break;
			case YAML_NO_EVENT:
				dbg(0, "YAML_NO_EVENT");
				break;
		}

		yaml_event_delete(&event);

	} while(!end && safety++ < 1024);

	return true;

  error:

	return false;
}


/*
 *  Currently the handlers can match either scalar or mapping types
 */
bool
yaml_load (FILE* fp, YamlHandler handlers[])
{
	g_auto(yaml_parser_t) parser;
	yaml_parser_initialize(&parser);

	yaml_parser_set_input_file(&parser, fp);

	return _yaml_load(&parser, handlers);
}


bool
yaml_load_string (const char* str, YamlHandler handlers[])
{
	g_auto(yaml_parser_t) parser;
	yaml_parser_initialize(&parser);

	yaml_parser_set_input_string(&parser, (guchar*)str, strlen(str));

	return _yaml_load(&parser, handlers);
}


/*
 *  After having entered a new mapping, handle expected scalar and mapping events
 */
bool
yaml_load_mapping (yaml_parser_t* parser, YamlHandler handlers[], YamlValueHandler vhandlers[], gpointer user_data)
{
	int depth = 0;
	char key[64] = "";
	int safety = 0;
	yaml_event_t event;
	do {
		if (!yaml_parser_parse(parser, &event)) goto error;

		switch (event.type) {
			case YAML_SCALAR_EVENT:
				dbg(2, "YAML_SCALAR_EVENT: %s", event.data.scalar.value);

				if (!key[0]) {
					// 1st half of a pair
					g_strlcpy(key, (char*)event.data.scalar.value, 64);
				} else {
					// 2nd half of a pair
					dbg(2, "      %s=%s", key, event.data.scalar.value);
					int i = 0;
					YamlValueHandler* h;
					dbg(2, "        vh=%p %s", &vhandlers[0], &vhandlers[0] ? (&vhandlers[0])->key : "<none>");
					while ((h = &vhandlers[i]) && h->callback) {
						dbg(2, "             key=%s", h->key);
						if (!h->key || !strcmp(h->key, key)) {
							h->callback(&event, key, h->data);
							key[0] = '\0';
							break;
						}
						i++;
					}

					key[0] = '\0';
				}
				break;
			case YAML_MAPPING_START_EVENT:
				depth++;
				if(key[0]){
					dbg(2, "found sub-section: %s", key);
					int i = 0;
					YamlHandler* h;
					while ((h = &handlers[i]) && h->callback ) {
						dbg(2, "             key=%s", h->key);
						if (!h->key || !strlen(h->key) || !strcmp(h->key, key)) { // empty string or NULL is wildcard
							h->callback(parser, key, h->data);
							key[0] = '\0';
							depth--;
							break;
						}
						i++;
					}
					key[0] = '\0';
				}
				else dbg(2, "YAML_MAPPING_START_EVENT");
				break;
			case YAML_MAPPING_END_EVENT:
				dbg(2, "YAML_MAPPING_END_EVENT");
				if(--depth <= 0) goto done;
				break;
			default:
				pwarn("%i", event.type);
				goto error;
		}

		yaml_event_delete(&event);

	} while(/*!end && */safety++ < 1024);
	dbg(0, "end - NEVER GET HERE?");

	return true;

  done:
	yaml_event_delete(&event);
	return true;

  error:
	return false;
}


bool
yaml_load_section2 (yaml_parser_t* parser, YamlHandler** handlers, YamlValueHandler** vhandlers)
{
	// this is exactly the same as above but args are pointers rather than arrays.

	dbg(2, "%p %p", handlers, vhandlers);
	int depth = 0;
	char key[64] = "";
	int safety = 0;
	yaml_event_t event;
	do {
		if (!yaml_parser_parse(parser, &event)) goto error; // Get the next event.

		switch (event.type) {
			case YAML_STREAM_START_EVENT:
				dbg(2, "YAML_STREAM_START_EVENT");
				break;
			case YAML_STREAM_END_EVENT:
				pwarn("unexpected YAML_STREAM_END_EVENT");
				break;
			case YAML_DOCUMENT_START_EVENT:
				dbg(2, "YAML_DOCUMENT_START_EVENT");
				break;
			case YAML_DOCUMENT_END_EVENT:
				pwarn("enexpected YAML_DOCUMENT_END_EVENT");
				break;
			case YAML_ALIAS_EVENT:
				dbg(0, "YAML_ALIAS_EVENT");
				break;
			case YAML_SCALAR_EVENT:
				if(event.data.scalar.anchor) dbg(0, "YAML_SCALAR_EVENT: %s => %s", event.data.scalar.anchor, event.data.scalar.tag);

				if(!key[0]){ //1st half of a pair. We dont yet know what to do with it
					strncpy(key, (char*)event.data.scalar.value, 63);
				}else{
					//2nd half of a pair
					dbg(2, "      %s=%s", key, event.data.scalar.value);
					int i = 0;
					YamlValueHandler* h;
					while((h = vhandlers[i]) && h->key){
						dbg(2, "             key=%s", h->key);
						if(!strcmp(h->key, key)){
							//dbg(3, "            key match");
							h->callback(&event, key, h->data);
							key[0] = '\0';
							break;
						}
						i++;
					}

					key[0] = '\0';
				}
				break;
			case YAML_SEQUENCE_START_EVENT:
				dbg(2, "YAML_SEQUENCE_START_EVENT");
				break;
			case YAML_SEQUENCE_END_EVENT:
				dbg(2, "YAML_SEQUENCE_END_EVENT");
				break;
			case YAML_MAPPING_START_EVENT:
				depth++;
				if(key[0]){
					dbg(2, "found sub-section: %s", key);
					int i = 0;
					YamlHandler* h;
					while((h = handlers[i]) && h->key ){
						dbg(2, "             key=%s", h->key);
						if(!strlen(h->key) || !strcmp(h->key, key)){ //empty string is wildcard
							h->callback(parser, key, h->data);
							key[0] = '\0';
							depth--;
							break;
						}
						i++;
					}
					key[0] = '\0';
				}
				else dbg(2, "YAML_MAPPING_START_EVENT");
				break;
			case YAML_MAPPING_END_EVENT:
				dbg(2, "YAML_MAPPING_END_EVENT");
				if(--depth < -1) pwarn("too many YAML_MAPPING_END_EVENT's.");
				if(depth <= 0) goto done;
				break;
			case YAML_NO_EVENT:
				dbg(0, "YAML_NO_EVENT");
				break;
		}

		yaml_event_delete(&event);

	} while(/*!end && */safety++ < 1024);
	dbg(0, "end - NEVER GET HERE?");

	return TRUE;

  done:
	yaml_event_delete(&event);
	return TRUE;

  error:
	return FALSE;
}


void
yaml_set_string (const yaml_event_t* event, const char* key, gpointer data)
{
	g_return_if_fail(event->type == YAML_SCALAR_EVENT);
	*((char**)data) = g_strdup((char*)event->data.scalar.value);
}


void
yaml_set_int (const yaml_event_t* event, const char* key, gpointer data)
{
	g_return_if_fail(event->type == YAML_SCALAR_EVENT);
	*((int*)data) = atoi((char*)event->data.scalar.value);
}


void
yaml_set_uint64 (const yaml_event_t* event, const char* key, gpointer data)
{
	g_return_if_fail(event->type == YAML_SCALAR_EVENT);
	*((uint64_t*)data) = strtoull((char*)(event->data.scalar.value), NULL, 10);
}


void
yaml_set_position (const yaml_event_t* event, const char* key, gpointer data)
{
	// sets a GPos from a value in samples.

	g_return_if_fail(event->type == YAML_SCALAR_EVENT);

	int samples = atoi((char*)(event->data.scalar.value));

	samples2pos(samples, data);
}
