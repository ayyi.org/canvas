using GLib;
using Gdk;
using Ayyi;

public class Ayyi.SavesongClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		return (int)(app = new SavesongClient()).run(((SavesongClient*)app)->on_shm);
	}

	private void on_shm()
	{
		AM.Song.save((obj, error) => {
			log_print(LogType.OK, "done.");
			quit();
		});
	}
}


