/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __panel_h__
#define __panel_h__

#include <gdl/gdl-dock-item.h>
#include "yaml/load.h"
#include "gui_types.h"
#include "toolbar.h"
#include "window.h"

G_BEGIN_DECLS

#define AYYI_TYPE_PANEL             (ayyi_panel_get_type ())
#define AYYI_PANEL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_PANEL, AyyiPanel))
#define AYYI_PANEL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_PANEL, AyyiPanelClass))
#define AYYI_IS_PANEL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_PANEL))
#define AYYI_IS_PANEL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_PANEL))
#define AYYI_PANEL_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_PANEL, AyyiPanelClass))

typedef struct _AyyiPanelClass AyyiPanelClass;

struct _AyyiPanelClass {
	GdlDockItemClass parent_class;
	char*            name;
	gboolean         has_link;
	gboolean         has_follow;
	gboolean         has_toolbar;
	gboolean         has_menubar;
	gboolean         has_statusbar;

	ConfigParam**    config;

	int              accel;                          //keyboard shortcut to open a new instance.
	GtkAccelGroup*   accel_group;                    //keyboard shortcuts. deprecated by Panel.accels list.
	GimpActionGroup* action_group;
	GList*           accels;                         //GtkAccelGroup*'s - will replace Panel.accel_group
	GList*           mod_accels;                     //GtkAccelGroup*'s for keys that dont need to be disconnected.

	Pti              min_size;
	Pti              default_size;

	struct {
		YamlHandler**      handlers;
		YamlValueHandler** vhandlers;
		int                instances;
	} loader;

	GList*           (*get_selected_parts)       (AyyiPanel*); // returns a newly allocated list of AMPart*.
	void             (*select_all)               (AyyiPanel*);
	void             (*on_link_change)           (AyyiPanel*);
	void             (*on_ready)                 (AyyiPanel*);
	void**           (*get_config)               (AyyiPanel*);

	int              instances;                      //the number of open windows.
};

struct _AyyiPanel {
	GdlDockItem      dock_item;
	AyyiWindow*      window;

	bool             link;
	bool             follow;

	Observable*      zoom;                           // type PtObservable

	int              meter_type;

	GtkWidget*       dnd_ebox;
	GtkWidget*       vbox;
	Toolbar*         toolbar;
	GtkWidget*       toolbar_box[2];
	GtkWidget*       b_toolbar  [MAX_TOOLBAR_BUT];

	gulong           focus_handler;

	bool             size_is_allocated;
	bool             model_is_loaded;                // panel song model objects are loaded. note that this happens some time _after_ AMSong->loaded becomes true.

	bool             destroyed;
#ifdef DEBUG 
	int              instance;                       // tmp 
#endif
	struct {
		gulong       ready_idle;
	}                priv;
};

extern char constructor_colour [16];


GType          ayyi_panel_get_type          ();
void           ayyi_panel_set_name          (AyyiPanel*, PanelType);
void           ayyi_panel_get_id_string     (AyyiPanel*, char* id_str);
void           ayyi_panel_get_id_string_pretty (AyyiPanel*, char* id_str);

void           ayyi_panel_on_new            (AyyiPanel*, Size*, GtkOrientation);
void           ayyi_panel_on_open           (AyyiPanel*);
int            ayyi_panel_get_depth         (AyyiPanel*);
bool           ayyi_panel_is_pressed        (AyyiPanel*);
int            ayyi_panel_get_instance_num  (AyyiPanel*);
void           ayyi_panel_on_focus          (AyyiPanel*);
void           ayyi_panel_on_blur           (AyyiPanel*);
void           ayyi_panel_present           (AyyiPanel*);
bool           ayyi_panel_present_idle      (AyyiPanel*);

void           ayyi_panel_drag_received     (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer user_data);
bool           ayyi_panel_on_drop_dock_item (AyyiPanel*, const char* u);

void           ayyi_panel_print_type        (AyyiPanel*, char*);
void           ayyi_panel_print_label       (AyyiPanel*, char*);
void           ayyi_panel_print_label_safe  (AyyiPanel*, char*);

#define panel_pack(A, B) gtk_box_pack_start (GTK_BOX (panel->vbox), A, B, B, 0)

G_END_DECLS

#endif
