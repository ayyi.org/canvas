/*
 * GtkComboGrid widget for GTK+
 *
 * Copyright (C) 2006 Andrea Zagli <azagli@inwind.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <gdk/gdkkeysyms.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkentry.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtktreeselection.h>
#include <gtk/gtkarrow.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "gtkcombogrid.h"

enum
{
	PROP_0,
	PROP_EDITABLE,
	PROP_TEXT_COLUMN,
	PROP_HEADERS_VISIBLE,
	PROP_GRID_HEIGHT
};

static void     gtk_combo_grid_init         (GtkComboGrid*);

static void     gtk_combo_grid_set_property (GObject*, guint property_id, const GValue*, GParamSpec*);
static void     gtk_combo_grid_get_property (GObject*, guint property_id, GValue*, GParamSpec*);
static void     gtk_combo_grid_set_text_from_grid
                                            (GtkComboGrid*);

static void     gtk_combo_grid_hide_popup   (GtkWidget*);
static gboolean popup_grab_on_window        (GdkWindow*, guint32 activate_time);
static gint     delete_popup                (GtkWidget*, gpointer);
static gint     key_press_popup             (GtkWidget*, GdkEventKey*, gpointer);
static gint     button_press_popup          (GtkWidget*, GdkEventButton*, gpointer);
static void     btn_dropdown_on_toggled     (GtkToggleButton*, gpointer user_data);
static void     treev_on_row_activated      (GtkTreeView*, GtkTreePath*, GtkTreeViewColumn*, gpointer);
//static void     treev_on_realise(GtkWidget* widget, GtkComboGrid *combo_grid);
static void     treev_on_resize             (GtkWidget*, GtkAllocation*, GtkComboGrid*);

struct _GtkComboGridPrivate
{
	GtkWidget *entry;
	GtkWidget *btn_dropdown;
	GtkWidget *w_dropdown;
	GtkWidget *scrolw;
	GtkWidget *treev;

	GtkTreeModel *model;
	GtkTreeSelection *sel;
	void (*update_model)(void*, gpointer);
	gpointer model_arg1;

	gint text_column;
	gint grid_height;
};

G_DEFINE_TYPE_WITH_PRIVATE (GtkComboGrid, gtk_combo_grid, GTK_TYPE_HBOX)

static void
gtk_combo_grid_class_init (GtkComboGridClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->set_property = gtk_combo_grid_set_property;
	object_class->get_property = gtk_combo_grid_get_property;

	g_object_class_install_property (object_class, PROP_EDITABLE,
	                                 g_param_spec_boolean ("editable",
	                                                       "Editable",
	                                                       "Whether the entry is editable.",
	                                                       TRUE,
	                                                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_TEXT_COLUMN,
	                                 g_param_spec_int ("text-column",
	                                                   "Text Column",
	                                                   "A column in the data source model to get the strings from.",
	                                                   0, G_MAXINT, 0,
	                                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_HEADERS_VISIBLE,
	                                 g_param_spec_boolean ("headers-visible",
	                                                       "Headers Visible",
	                                                       "Whether column headers are visible.",
	                                                       TRUE,
	                                                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_GRID_HEIGHT,
	                                 g_param_spec_int ("grid-height",
	                                                   "Grid's height",
	                                                   "The Grid's height.",
	                                                   0, G_MAXINT, 100,
	                                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
}

static void
gtk_combo_grid_init (GtkComboGrid *combo_grid)
{
	GtkComboGridPrivate *priv = combo_grid->priv = gtk_combo_grid_get_instance_private(combo_grid);

	priv->entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (combo_grid), priv->entry, TRUE, TRUE, 0);
	gtk_widget_show (priv->entry);

	priv->btn_dropdown = gtk_toggle_button_new ();
	gtk_box_pack_start (GTK_BOX (combo_grid), priv->btn_dropdown, FALSE, FALSE, 0);
	gtk_widget_show (priv->btn_dropdown);

	g_signal_connect (G_OBJECT (priv->btn_dropdown), "toggled", G_CALLBACK (btn_dropdown_on_toggled), (gpointer)combo_grid);

	GtkWidget *arrow = (GtkWidget *)gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_NONE);
	gtk_container_add (GTK_CONTAINER (priv->btn_dropdown), arrow);
	gtk_widget_show (arrow);

	priv->w_dropdown = gtk_window_new (GTK_WINDOW_POPUP);
	gtk_window_set_resizable (GTK_WINDOW (priv->w_dropdown), FALSE);

#ifdef SHOW_BORDER
	g_object_set (G_OBJECT (priv->w_dropdown),
	              "border-width", 3,
	              NULL);
#endif
	gtk_widget_set_events (priv->w_dropdown, gtk_widget_get_events (priv->w_dropdown) | GDK_KEY_PRESS_MASK);

	g_signal_connect (priv->w_dropdown, "delete_event",       G_CALLBACK (delete_popup), combo_grid);
	g_signal_connect (priv->w_dropdown, "key_press_event",    G_CALLBACK (key_press_popup), combo_grid);
	g_signal_connect (priv->w_dropdown, "button_press_event", G_CALLBACK (button_press_popup), combo_grid);

	priv->scrolw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->scrolw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (priv->w_dropdown), priv->scrolw);
	gtk_widget_show (priv->scrolw);

	priv->treev = gtk_tree_view_new ();
	gtk_container_add (GTK_CONTAINER (priv->scrolw), priv->treev);

	g_signal_connect (G_OBJECT (priv->treev), "row-activated", G_CALLBACK (treev_on_row_activated), combo_grid);
	//g_signal_connect (G_OBJECT (priv->treev), "realize",       G_CALLBACK (treev_on_realise), combo_grid);
	g_signal_connect (G_OBJECT (priv->treev), "size-allocate", G_CALLBACK (treev_on_resize), combo_grid);

	priv->sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treev));
	gtk_tree_selection_set_mode (priv->sel, GTK_SELECTION_SINGLE);

	gtk_widget_show (priv->treev);
}

/**
 * gtk_combo_grid_new:
 *
 * Creates a new #GtkComboGrid object.
 *
 * Returns: The newly created #GtkComboGrid widget.
 */
GtkWidget*
gtk_combo_grid_new ()
{
	GtkWidget *w = GTK_WIDGET (g_object_new (gtk_combo_grid_get_type (), NULL));

	return w;
}

GtkWidget*
gtk_combo_grid_get_entry (GtkComboGrid *combo_grid)
{
	return combo_grid->priv->entry;
}

/**
 * gtk_combo_grid_get_text:
 * @combo_grid: a #GtkComboGrid object.
 *
 * Returns: A pointer to the content of the entry widget as a string.
 */
const gchar*
gtk_combo_grid_get_text (GtkComboGrid *combo_grid)
{
	return gtk_entry_get_text (GTK_ENTRY (combo_grid->priv->entry));
}

void
gtk_combo_grid_set_text(GtkComboGrid *combo_grid, const char* text)
{
	gtk_entry_set_text(GTK_ENTRY (combo_grid->priv->entry), text);
}

/**
 * gtk_combo_grid_set_editable:
 * @combo_grid: a #GtkComboGrid object.
 * @is_editable: TRUE if the user is allowed to edit the text in the widget.
 *
 * Determines if the user can edit the text in the #GtkComboGrid widget or not.
 */
void
gtk_combo_grid_set_editable (GtkComboGrid *combo_grid, gboolean is_editable)
{
	gtk_editable_set_editable (GTK_EDITABLE (combo_grid->priv->entry), is_editable);
}

/**
 * gtk_combo_grid_set_text_column:
 * @combo_grid: a #GtkComboGrid object.
 * @text_column: A column in model to get the strings from.
 *
 * Sets the model column which @combo_grid should use to get strings from to be @text_column.
 */
void
gtk_combo_grid_set_text_column (GtkComboGrid *combo_grid, gint text_column)
{
	combo_grid->priv->text_column = text_column;

	gtk_combo_grid_set_text_from_grid (combo_grid);
}

/**
 * gtk_combo_grid_set_headers_visible:
 * @combo_grid: a #GtkComboGrid object.
 * @headers_visible: TRUE if the headers are visible.
 *
 * Sets the visibility state of the headers.
 */
void
gtk_combo_grid_set_headers_visible (GtkComboGrid *combo_grid, gboolean headers_visible)
{
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (combo_grid->priv->treev), headers_visible);
}

/**
 * gtk_combo_grid_set_model:
 * @combo_grid: a #GtkComboGrid object.
 * @model: a #GtkTreeModel object.
 *
 * Sets the model for a #GtkComboGrid. If the @combo_grid already has a model set, 
 * it will remove it before setting the new model. If model is NULL, then it will 
 * unset the old model.
 */
void
gtk_combo_grid_set_model (GtkComboGrid *combo_grid, GtkTreeModel *model)
{
	GtkComboGridPrivate *priv = combo_grid->priv;

	priv->model = model;

	gtk_tree_view_set_model (GTK_TREE_VIEW(priv->treev), priv->model);
}

void
gtk_combo_grid_set_model_updater (GtkComboGrid *combo_grid, gpointer func, gpointer arg1)
{
	GtkComboGridPrivate *priv = combo_grid->priv;

	priv->update_model = func;
	priv->model_arg1 = arg1;
}

void
gtk_combo_grid_set_height (GtkComboGrid *combo_grid, int popup_height)
{
	combo_grid->priv->grid_height = popup_height;
}

/**
 * gtk_combo_grid_append_column:
 * @combo_grid: a #GtkComboGrid object.
 * @column: a #GtkTreeViewColumn object.
 *
 */
void
gtk_combo_grid_append_column (GtkComboGrid *combo_grid,
                              GtkTreeViewColumn *column)
{	
	gtk_tree_view_append_column (GTK_TREE_VIEW(combo_grid->priv->treev), column);
}

/**
 * gtk_combo_grid_get_active_iter:
 * @combo_grid: a #GtkComboGrid object.
 * @iter: the uninitialized #GtkTreeIter.
 *
 * Sets @iter to point to the current active item, if it exists.
 *
 * Returns: TRUE, if iter was set.
 */
gboolean
gtk_combo_grid_get_active_iter (GtkComboGrid *combo_grid, GtkTreeIter *iter)
{
	return gtk_tree_selection_get_selected (combo_grid->priv->sel, NULL, iter);
}

/* PRIVATE */
static void
gtk_combo_grid_set_property (GObject *object,
                             guint property_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
	GtkComboGrid *combo_grid = (GtkComboGrid *)object;

	GtkComboGridPrivate *priv = combo_grid->priv;

	switch (property_id)
		{
			case PROP_EDITABLE:
				gtk_combo_grid_set_editable (combo_grid, g_value_get_boolean (value));
				break;

			case PROP_TEXT_COLUMN:
				gtk_combo_grid_set_text_column (combo_grid, g_value_get_int (value));
				break;

			case PROP_HEADERS_VISIBLE:
				gtk_combo_grid_set_headers_visible (combo_grid, g_value_get_boolean (value));
				break;

			case PROP_GRID_HEIGHT:
				priv->grid_height = g_value_get_int (value);
				break;

			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
	}
}

static void
gtk_combo_grid_get_property (GObject *object,
                             guint property_id,
                             GValue *value,
                             GParamSpec *pspec)
{
	GtkComboGrid *combo_grid = (GtkComboGrid *)object;

	GtkComboGridPrivate *priv = combo_grid->priv;

	switch (property_id)
		{
			case PROP_EDITABLE:
				g_value_set_boolean (value, gtk_editable_get_editable (GTK_EDITABLE (priv->entry)));
				break;

			case PROP_TEXT_COLUMN:
				g_value_set_int (value, priv->text_column);
				break;

			case PROP_HEADERS_VISIBLE:
				g_value_set_boolean (value, gtk_tree_view_get_headers_visible (GTK_TREE_VIEW (priv->treev)));
				break;

			case PROP_GRID_HEIGHT:
				g_value_set_int (value, priv->grid_height);
				break;

			default:
				G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
				break;
	  }
}

static void
gtk_combo_grid_set_text_from_grid (GtkComboGrid *combo_grid)
{
	GtkComboGridPrivate *priv = combo_grid->priv;

	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (priv->sel, NULL, &iter))
		{
			GValue *src_value = g_malloc0 (sizeof (GValue));
			GValue *dst_value = g_malloc0 (sizeof (GValue));

			g_value_init (dst_value, G_TYPE_STRING);

			gtk_tree_model_get_value (GTK_TREE_MODEL (priv->model), &iter, priv->text_column, src_value);

			if (g_value_transform (src_value, dst_value))
				{
					gtk_entry_set_text (GTK_ENTRY (priv->entry), g_value_get_string (dst_value));
				}
		}
	else
		{
			gtk_entry_set_text (GTK_ENTRY (priv->entry), "");
		}
}

/*
 * callbacks
 */
static void
gtk_combo_grid_hide_popup (GtkWidget *combo_grid)
{
	GtkComboGridPrivate *priv = ((GtkComboGrid*)combo_grid)->priv;

	gtk_widget_hide (priv->w_dropdown);
	gtk_grab_remove (priv->w_dropdown);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->btn_dropdown), FALSE);
	gtk_widget_grab_focus (priv->entry);
}

#ifdef use_treev_on_realise
static void
treev_on_realise (GtkWidget *treeview, GtkComboGrid *combo_grid)
{
	if(treeview->requisition.width < 2){
		//model is empty
		GtkComboGridPrivate *priv = combo_grid->priv;
		if(priv->update_model) priv->update_model(NULL, priv->model_arg1);
	}
}
#endif

#define SCROLLBAR_WIDTH_PLUS 24
static void
treev_on_resize (GtkWidget *treeview, GtkAllocation *allocation, GtkComboGrid *combo_grid)
{
	// adjust the popup window size following change in the model
	//
	// currently the width is oversized to avoid scrollbar problems. Ideally we would check scrollbar status instead.


	GtkComboGridPrivate *priv = combo_grid->priv;

	if(priv->treev->requisition.width > 32){
		//printf("treev_on_resize(): tree_width: requisition=%i allocation=%i\n", priv->treev->requisition.width, priv->treev->allocation.width);
		gtk_widget_set_size_request (priv->w_dropdown, MAX(priv->treev->requisition.width + SCROLLBAR_WIDTH_PLUS, 100), MIN(priv->grid_height, priv->treev->requisition.height + 2));
	}
}

static gboolean
popup_grab_on_window (GdkWindow *window, guint32 activate_time)
{
	if ((gdk_pointer_grab (window, TRUE,
	                       GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
	                       GDK_POINTER_MOTION_MASK,
	                       NULL, NULL, activate_time) == 0))
		{
			if (gdk_keyboard_grab (window, TRUE, activate_time) == 0)
				{
					return TRUE;
				}
			else
				{
					gdk_pointer_ungrab (activate_time);
					return FALSE;
				}
		}

	return FALSE;
}

static gint
delete_popup (GtkWidget *widget, gpointer data)
{
	gtk_combo_grid_hide_popup ((GtkWidget *)data);
	gtk_widget_grab_focus (((GtkComboGrid *)data)->priv->entry);

	return TRUE;
}

static gint
key_press_popup (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	switch (event->keyval){
		case GDK_Escape:
			break;

		case GDK_Return:
		case GDK_KP_Enter:
			gtk_combo_grid_set_text_from_grid ((GtkComboGrid *)data);
			break;

		default:
			return FALSE;
	}

	g_signal_stop_emission_by_name (widget, "key_press_event");
	gtk_combo_grid_hide_popup ((GtkWidget *)data);
	gtk_widget_grab_focus (((GtkComboGrid *)data)->priv->entry);

	return TRUE;
}

/* This function is yanked from gtkcombo.c */
static gint
button_press_popup (GtkWidget *widget,
                    GdkEventButton *event,
                    gpointer user_data)
{
	GtkComboGrid *combo_grid = (GtkComboGrid *)user_data;
	GtkWidget *child = gtk_get_event_widget ((GdkEvent *)event);

	/* We don't ask for button press events on the grab widget, so
	 *  if an event is reported directly to the grab widget, it must
	 *  be on a window outside the application (and thus we remove
	 *  the popup window). Otherwise, we check if the widget is a child
	 *  of the grab widget, and only remove the popup window if it
	 *  is not.
	 */
	if (child != widget)
		{
			while (child)
				{
					if (child == widget) return FALSE;
					child = child->parent;
				}
		}

	gtk_combo_grid_hide_popup (user_data);
	gtk_widget_grab_focus (combo_grid->priv->entry);

	return TRUE;
}

static void
btn_dropdown_on_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
	if (gtk_toggle_button_get_active (togglebutton)){
		GtkComboGrid *combo_grid = (GtkComboGrid *)user_data;
		GtkWidget *combo_widget = GTK_WIDGET (combo_grid);
		GtkComboGridPrivate *priv = combo_grid->priv;

		dbg(1, "popping up...");
		if(priv->update_model) priv->update_model(NULL, priv->model_arg1);

		gint x, y, bheight;
		GtkRequisition req;
		GtkWidget *w_dropdown = priv->w_dropdown;

		/* show dropdown */
		gtk_widget_size_request (w_dropdown, &req);
		gdk_window_get_origin (GDK_WINDOW (GTK_WIDGET (combo_widget)->window), &x, &y);
		x += combo_widget->allocation.x;
		/*y += entry->allocation.y;*/
		//gint bwidth = combo_widget->allocation.width;
		bheight = combo_widget->allocation.height;
		/*x += bwidth - req.width;*/
		y += bheight;
		if (x < 0) x = 0;
		if (y < 0) y = 0;

		gtk_grab_add (w_dropdown);
		gtk_window_move (GTK_WINDOW (w_dropdown), x, y);
#ifdef DROP_DOWN_TAKES_COMBO_WIDTH
		//this is the original widget behaviour, but doesnt work when combox is narrow.
		gtk_widget_set_size_request (priv->w_dropdown, combo_widget->allocation.width, priv->grid_height);
#endif
		gtk_widget_show (w_dropdown);
		gtk_widget_grab_focus (priv->treev);

		popup_grab_on_window (w_dropdown->window, gtk_get_current_event_time ());
	}
}

static void
treev_on_row_activated (GtkTreeView *tree_view,
                        GtkTreePath *path,
                        GtkTreeViewColumn *column,
                        gpointer user_data)
{
	gtk_combo_grid_set_text_from_grid ((GtkComboGrid *)user_data);
	gtk_combo_grid_hide_popup ((GtkWidget *)user_data);
	gtk_widget_grab_focus (((GtkComboGrid *)user_data)->priv->entry);
}
