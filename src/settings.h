/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __settings_h__
#define __settings_h__

#include "panels/panel.h"


bool        config_save                     ();
void        config_load0                    ();
bool        config_load                     ();
int         config_get_window_setting_int   (AyyiPanel*, const char* param);
bool        config_get_window_setting_bool  (AyyiPanel*, const char* param);
double      config_get_window_setting_float (AyyiPanel*, const char* param);
const char* config_get_window_setting_char  (AyyiPanel*, const char* param);
void        config_load_window_instance     (AyyiPanel*);
void        config_make_group_name          (AyyiPanelClass*, int count, char* group_name);
int         config_get_n_windows            (PanelType);
bool        config_log_window_is_open       ();
void        config_new                      ();

#endif
