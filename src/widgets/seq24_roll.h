/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */


#ifndef __GTK_ROLL__
#define __GTK_ROLL__


#include <gdk/gdk.h>
#include <gtk/gtkdrawingarea.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_ROLL                 (gtk_roll_get_type())
#define GTK_ROLL(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_ROLL, GtkRoll))
#define GTK_ROLL_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_ROLL, GtkRollClass))
#define GTK_IS_ROLL(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_ROLL))
#define GTK_IS_ROLL_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_ROLL))
#define GTK_ROLL_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_ROLL, GtkRollClass))


typedef struct _GtkRoll	       GtkRoll;
typedef struct _GtkRollClass   GtkRollClass;


struct _GtkRoll
{
  GtkDrawingArea graph;

  //gint cursor_type;
  //gfloat min_x;
  //gfloat max_x;
  //gfloat min_y;
  //gfloat max_y;
  //gint last;


  GdkWindow   *m_window;
  GdkGC       *m_gc;
  GdkPixmap   *m_pixmap;
  GdkColor     black, white, grey, dk_grey, red;
  GdkColormap *colormap;

  GdkRectangle m_old;
  GdkRectangle m_selected;

  struct _midi_sequence* seq;
  struct _midi_sequence* clipboard;
  perform      *m_perform;
  seqdata      *m_seqdata_widget;  // a "piano event"
  seqevent     *m_seqevent_widget; // a "piano event"
  seqkeys      *m_seqkeys_widget;

  mainwid      *m_mainwid;

  int          m_pos;

  //one pixel == m_zoom ticks
  int          m_zoom;
  int          m_snap;
  int          m_note_length;

  int          m_scale;
  int          m_key;

  int m_window_x, m_window_y;

  //what is the data window currently editing ?
  unsigned char m_status;
  unsigned char m_cc;

  //when highlighting a bunch of events
  gboolean selecting;
  gboolean moving;
  gboolean moving_init;
  gboolean growing;
  gboolean adding;
  gboolean paste;

  //where the dragging started
  int m_drop_x;
  int m_drop_y;
  int m_move_delta_x;
  int m_move_delta_y;
  int m_current_x;
  int m_current_y;

  int m_move_snap_offset_x;

  int m_old_progress_x;

  GtkAdjustment   *m_vadjust;
  GtkAdjustment   *m_hadjust;

  int m_scroll_offset_ticks;
  int m_scroll_offset_key;

  int m_scroll_offset_x;
  int m_scroll_offset_y;

  int m_background_sequence;
  gboolean m_drawing_background_seq;
};


struct _GtkRollClass
{
  GtkDrawingAreaClass parent_class;

  //void (* curve_type_changed) (GtkCurve *curve);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GType		gtk_roll_get_type	(void) G_GNUC_CONST;
GtkWidget*	gtk_roll_new		(
               perform *a_perf,
               struct _midi_sequence* a_seq, int a_zoom, int a_snap,
               seqdata *a_seqdata_wid,
               seqevent *a_seqevent_wid,
               seqkeys *a_seqkeys_wid,
               mainwid *a_mainwid,
               int a_pos,
               GtkAdjustment *a_hadjust,
               GtkAdjustment *a_vadjust
               );

void		gtk_roll_reset		(GtkRoll *roll);

//public functions:
void gtk_roll__redraw(GtkRoll *r);
    void set_zoom(int a_zoom);
    void set_snap(int a_snap);
    void set_note_length(int a_note_length);

    void set_scale(int a_scale);
    void set_key(int a_key);

void gtk_roll_update_sizes();
void gtk_roll_draw_background();
void gtk_roll_draw_events_on_pixmap();
void gtk_roll_draw_selection_on_window(GtkRoll *r);
    int idle_redraw();

    void draw_progress_on_window();
void gtk_roll_set_adding(GtkWidget*, gboolean a_adding);

void start_paste();

void set_background_sequence(gboolean a_state, int a_seq);

void set_data_type(unsigned char a_status, unsigned char a_control);




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_ROLL__ */





#include "seq24/sequence.h"

/*
#define EVENT_STATUS_BIT        0x80
#define EVENT_NOTE_OFF          0x80
#define EVENT_NOTE_ON           0x90
#define EVENT_AFTERTOUCH        0xA0
#define EVENT_CONTROL_CHANGE    0xB0
#define EVENT_PROGRAM_CHANGE    0xC0
#define EVENT_CHANNEL_PRESSURE  0xD0
#define EVENT_PITCH_WHEEL       0xE0
#define EVENT_CLEAR_CHAN_MASK   0xF0
#define EVENT_MIDI_CLOCK        0xF8
#define EVENT_SYSEX             0xF0
#define EVENT_SYSEX_END         0xF7
*/

#define MAXBEATS 0xFFFF
#define PPQN 192

int                 midi_add_note();
void                midiedit_get_selected_box(struct _midi_sequence*, long *a_tick_s, int *a_note_h, long *a_tick_f, int *a_note_l);
int                 midiedit_select_note_events(struct _midi_sequence*, long a_tick_s, int a_note_h, long a_tick_f, int a_note_l);

gboolean            midi_event_is_note_off(midi_event* event);
long                midi_event_get_timestamp(midi_event* event);
gboolean            midi_event_is_linked(midi_event* event);
struct _midi_event* midi_event_get_linked(midi_event* event);
void                midi_event_select(midi_event* event);

