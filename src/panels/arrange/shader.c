/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2015-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include <GL/gl.h>
#include "agl/ext.h"
#include "waveform/utils.h"
#include "waveform/waveform.h"
#include "arrange/shader.h"
#include "arrange/shaders/shaders.c"

static void _part_set_uniforms();
static void _pattern_set_uniforms();
static void _circle_set_uniforms();
static void _h_scrollbar_set_uniforms();


PartShader part_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"width", 1, GL_FLOAT, -1,},
	   {"height", 1, GL_FLOAT, -1,},
	   END_OF_UNIFORMS
	},
	_part_set_uniforms,
	&part_text
}};

AGlShader pattern_shader = {
	.set_uniforms_ = _pattern_set_uniforms,
	&pattern_text
};

CircleShader circle_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   {"bg_colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	_circle_set_uniforms,
	&circle_text
}};

ScrollbarShader h_scrollbar_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   {"bg_colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	_h_scrollbar_set_uniforms,
	&hscrollbar_text
}};


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	AGlUniformInfo* uniform = &shader->uniforms[u];
	if(uniform->value[0] != *prev){
		glUniform1f(uniform->location, uniform->value[0]);
		*prev = uniform->value[0];
	}
}


static void
_part_set_uniforms ()
{
	#define PART_WIDTH 2
	#define PART_HEIGHT 3
	#define PART_SELECTED 4
	#define PART_FRAME 5
	#define PART_COLOUR 6
	#define PART_BORDER_COLOUR 7

	AGlShader* shader = &part_shader.shader;

	static float prev[PART_SHADER_MAX] = {0,};

	float colour[4] = {0.0, 0.0, 0.0, ((float)(part_shader.uniform.colour & 0xff)) / 0x100};
	agl_rgba_to_float(part_shader.uniform.colour, &colour[0], &colour[1], &colour[2]);
	glUniform4fv(PART_COLOUR, 1, colour);

	float border_colour[4] = {0.0, 0.0, 0.0, ((float)(part_shader.uniform.border_colour & 0xff)) / 0x100};
	agl_rgba_to_float(part_shader.uniform.border_colour, &border_colour[0], &border_colour[1], &border_colour[2]);
	glUniform4fv(PART_BORDER_COLOUR, 1, border_colour);

	glUniform1f(PART_SELECTED, ((PartShader*)shader)->uniform.selected);
	glUniform1f(PART_FRAME, ((PartShader*)shader)->uniform.frame);
	for(int i=0;i<PART_SHADER_MAX;i++){
		set_uniform_f(shader, i, &prev[i]);
	}
}


static void
_pattern_set_uniforms ()
{
}


static void
_circle_set_uniforms ()
{
	float centre[2] = {circle_shader.uniform.centre.x, circle_shader.uniform.centre.y};
	glUniform2fv(glGetUniformLocation(circle_shader.shader.program, "centre"), 1, centre);
	glUniform1f(glGetUniformLocation(circle_shader.shader.program, "radius"), circle_shader.uniform.radius);

	agl_set_uniforms ((AGlShader*)&circle_shader);
}


static void
_scrollbar_set_uniforms (ScrollbarShader* scrollbar_shader)
{
	#define CENTRE1 2
	#define CENTRE2 3
	#define RADIUS 4

	float centre1[2] = {scrollbar_shader->uniform.centre1.x, scrollbar_shader->uniform.centre1.y};
	float centre2[2] = {scrollbar_shader->uniform.centre2.x, scrollbar_shader->uniform.centre2.y};
	glUniform2fv(CENTRE1, 1, centre1);
	glUniform2fv(CENTRE2, 1, centre2);
	glUniform1f(RADIUS, scrollbar_shader->uniform.radius);

	agl_set_uniforms ((AGlShader*)scrollbar_shader);
}


static void
_h_scrollbar_set_uniforms ()
{
	_scrollbar_set_uniforms (&h_scrollbar_shader);
}
