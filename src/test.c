/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  This file contains functional tests that run within the application.
 |  They can be run from the help menu.
 |
 */

#define __test_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <model/pool.h>
#include <model/part_manager.h>
#include <model/time.h>
#include <model/transport.h>
#include <model/track_list.h>
#include "windows.h"
#include "support.h"
#include "song.h"
#include "panels/arrange.h"
#include "arrange/automation.h"

extern GHashTable* canvas_types;

#define fail(A, ...) { perr(A, ##__VA_ARGS__); goto abort; }
#define fail_(A, ...) { perr(A, ##__VA_ARGS__); return false; }
#define fail__(A, ...) { perr(A, ##__VA_ARGS__); return; }

gboolean pause_(gpointer _next) { ((AyyiCallback)_next)(NULL); return G_SOURCE_REMOVE; }

static bool fast = true;

bool passed = FALSE;
int  current_test = -1;
static char current_test_name[64];
static int n_passed = 0;
static int n_failed = 0;
static int timer = 0;

typedef void (TestFn)();

static void next_test          ();
static void ensure_blank_track (AyyiFinish, gpointer);

TestFn
	add_track_with_parts,
	test_create_parts,
	test_part_split,
	test_track_add_remove,
	test_add_n_tracks,
	test_move_track,
	test_part_copy,
	test_window_open_close,
	test_canvas_change,
	test_automation,
	test_time,
	test_time2,
	test_track_rename,
	test_zoom;

gpointer tests[] = {
	test_time,
	test_time2,
	//test_canvas_change,
	test_automation,
	test_create_parts,
	add_track_with_parts,
	test_part_split,
	test_track_add_remove,
	test_add_n_tracks,
	test_move_track,
	test_part_copy,
	test_track_rename,
	test_zoom,
	test_window_open_close,
};


static void
test_report ()
{
	if (passed) log_print(LOG_OK, "test completed");
	else        log_print(LOG_FAIL, "test failed.");
}


static void
final_report ()
{
	if (current_test < G_N_ELEMENTS(tests))
		printf("%i", current_test);
	else
		printf("all");
	printf(" tests finished. passed: %s%i%s failed: %s%i%s\n", ayyi_green, n_passed, white, n_failed ? ayyi_red : "", n_failed, white);

	passed = FALSE;
	current_test = -1;
	n_passed = 0;
	n_failed = 0;
}


#define STOP_ON_FAIL

static void
test_finished ()
{
	if(passed) n_passed++; else n_failed++;
#ifdef STOP_ON_FAIL
	if(!passed){
		final_report();
		return;
	}
#endif
	test_report();
	next_test();
}

#define TEST_START \
	log_print(0, "running %s%s%s ...", ayyi_green, __func__, white); \
	g_strlcpy(current_test_name, __func__, 64); \
	gboolean test_timeout (gpointer _user_data) \
	{ \
		TEST_FAIL_NOSTEP("%s: %stimeout%s.\n", current_test_name, ayyi_red, white) \
		return G_SOURCE_REMOVE; \
	} \
	if (timer) perr("timer not cleared"); \
	timer = g_timeout_add(20000, test_timeout, NULL);

#define TEST_COMPLETED \
	{ \
	if(timer) g_source_remove (timer); \
	timer = 0; \
	log_print(0, "test complete"); \
	step = 0; \
	passed = TRUE; \
	test_finished(); \
	return G_SOURCE_REMOVE; \
	}

#define TEST_FAIL(A, ...) \
	({ \
	if(timer) g_source_remove (timer); \
	step = 0; \
	passed = FALSE; \
	printf("%s: %sFAIL%s" A "\n", current_test_name, ayyi_red, white, ##__VA_ARGS__); \
	test_finished(); \
	return G_SOURCE_REMOVE; \
	})

#define TEST_FAIL_NOSTEP(A, ...) \
	{ passed = FALSE; \
	printf(A, ##__VA_ARGS__); \
	test_finished(); \
	return G_SOURCE_REMOVE; }

// for non-timeout tests
#define TEST_COMPLETED_ \
	if(timer) g_source_remove (timer); \
	timer = 0; \
	log_print(0, "test complete"); \
	passed = TRUE; \
	test_finished();

#define TEST_FAIL_(A, ...) \
	{ \
	if(timer) g_source_remove (timer); \
	timer = 0; \
	passed = FALSE; \
	printf(A "\n", ##__VA_ARGS__); \
	test_finished(); \
	return; \
	}

#define FAIL_IF_ERROR \
	if(error && *error) {TEST_FAIL_("%s", (*error)->message);}

#define assert_stop(A, B, ...) \
	{bool __ok_ = (boolp)A; \
	{if(!__ok_) perr(B, ##__VA_ARGS__); } \
	{if(!__ok_) TEST_FAIL("assertion failed"); }}

#define assert(A, B, ...) \
	{bool __ok_ = (boolp)(A); \
	{if(!__ok_) perr(B, ##__VA_ARGS__); } \
	{if(!__ok_) TEST_FAIL_("assertion failed") }}

//----------------------------------------------------------------
// TODO extract modules.c from model/test

#define mono_wav_fixture "mono_0:10.wav"

typedef struct ModuleClosure {
	const char* module_name;
	AyyiFinish  finish;
	gpointer    user_data;
	int         timer;
	void*       module;
} MC;


void
errprintf4 (char* format, ...)
{
	char str[256];

	va_list argp;
	va_start(argp, format);
	vsprintf(str, format, argp);
	va_end(argp);

	printf("%s%s%s\n", ayyi_red, str, white);
}


typedef struct _module_data {
	AyyiFinish  on_finish;
	gpointer    user_data;
	const char* name;
} ModuleData;

#define START_MODULE_B \
	printf("starting module: %s%s%s ...\n", ayyi_green, __func__, white); \
	g_return_if_fail(on_finish); \
	gboolean module_timeout(gpointer _mc) \
	{ \
		MC* mc = _mc; \
		printf("MODULE TIMEOUT! %s\n", mc->module_name); \
		/*FAIL_TEST_TIMER("MODULE TIMEOUT!\n");*/ \
		g_free(mc); \
		return G_SOURCE_REMOVE; \
	} \
	MC* mc = AYYI_NEW(MC, \
		.module_name = __func__, \
		.finish = on_finish, \
	); \
	MC* _mc = mc; /* needed by FINISH_MODULE */ \
	_mc->user_data = _user_data; \
	mc->timer = g_timeout_add(40000, module_timeout, mc);

#define FINISH_MODULE_B(OBJ) \
	{ \
	MC* mc = _mc; \
	if (mc->timer) { \
		g_source_remove(mc->timer); \
		mc->timer = 0; \
	} \
	g_clear_pointer(&mc->module, g_free); \
	mc->finish(OBJ, mc->user_data); \
	g_free(mc); \
	}

#define FAIL_MODULE_IF_ERROR \
	if(error && *error){ g_source_remove(timer); TEST_FAIL_("> %s", (*error)->message); }

static char*
find_wav (const char* wav)
{
	if (wav[0] == '/') {
		return g_strdup(wav);
	}

	g_autofree char* cwd = g_get_current_dir();

	char* filename = g_build_filename(cwd, "../lib/waveform/test/data", wav, NULL);
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		return filename;
	}
	g_free(filename);

	filename = g_build_filename(cwd, "lib/waveform/test/data", wav, NULL);
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		return filename;
	}
	g_free(filename);

	return NULL;
}

void
module__load_wav_fixture (AyyiFinish on_finish, gpointer _user_data, const char* _filename)
{
	START_MODULE_B;

	void module__load_wav_fixture_done (AyyiIdent id, GError** error, gpointer _mc)
	{
		PF;
		MC* mc = _mc;
		FAIL_MODULE_IF_ERROR;
		ModuleData* d = mc->module;

		assert(ayyi_song__have_file(d->name), "file not loaded: %s", d->name);

		dbg(0, "n_files=%i", g_list_length(song->pool->list));
		assert(g_list_length(song->pool->list) > 0, "pool empty");

		AMPoolItem* item = am_pool__get_item_from_idx(id.idx);
		assert(item, "cannot find pool item: id=%i.%i", id.type, id.idx);
		AMPoolItem* pool_item = item;
		assert(pool_item->state == AM_POOL_ITEM_OK, "pool item bad state: %i", pool_item->state);
		assert(pool_item->channels, "pool item has no channels");

		g_free((char*)d->name);

		FINISH_MODULE_B(id)
	}

	char* filename = find_wav(_filename);

	mc->module = AYYI_NEW(ModuleData,
		.on_finish = on_finish,
		.user_data = _user_data,
		.name = filename
	);

	am_song__add_file(filename, module__load_wav_fixture_done, mc);
}


void
module__ensure_pool_item (AyyiFinish on_finish, gpointer _user_data)
{
	START_MODULE_B;

	if (ayyi_song__get_file_count()) {
		FINISH_MODULE_B(AYYI_NULL_IDENT)
		return;
	}

	dbg(0, "wav fixture loading...");

	void on_wav_added (AyyiIdent obj, gpointer _mc)
	{
		PF;
		FINISH_MODULE_B(obj)
	}
	module__load_wav_fixture(on_wav_added, mc, mono_wav_fixture);
}

//----------------------------------------------------------------

bool
test_start ()
{
	PF;
	if(!SONG_LOADED) return false;

	void have_pool_item (AyyiIdent id, gpointer _)
	{
		next_test();
	}
	module__ensure_pool_item(have_pool_item, NULL);

	return true;
}


static void
next_test ()
{
	gboolean run_test (gpointer user_data)
	{
		void (*test)() = tests[current_test];
		test();
		return G_SOURCE_REMOVE;
	}

	printf("\n");
	current_test++;

	if(current_test < G_N_ELEMENTS(tests)){
		g_timeout_add(fast? 1 : 1000, run_test, NULL);
	}
	else final_report();
}


//---------------------------------------------------------------

	typedef struct _closure  Closure;
	typedef void (*OnFinish) (Closure*);
	typedef void (*Task)     (Closure*);

	struct _closure {
		int      i;
		OnFinish next;
		Task*    tasks;
		bool     failed;
		gpointer user_data;
	};

	void tasks_next(Closure* closure)
	{
		closure->i++;
		dbg(0, "          i=%i", closure->i);
		Task next_task = closure->tasks[closure->i];
		if(next_task){
			next_task(closure);
		}else{
			dbg(0, ">>>>>>>>>>>>>> finished");
			TEST_COMPLETED_;
		}
	}

//---------------------------------------------------------------

void
test_create_parts ()
{
	// create 10 parts

	TEST_START;

	gboolean test_create_parts_ (gpointer _closure)
	{
		Closure* c = _closure;
		static int n_parts_added = 0;

		if(c->failed) return G_SOURCE_REMOVE;

		Arrange* arrange = ARRANGE_FIRST;

		am_song__set_end(32);
		if(!arrange->zoom_follow) arr_set_zoom_follow(arrange, true);

		static AyyiHandler p_added; p_added = NULL;

		void next_part (Closure* c)
		{
			AMPoolItem* pool_item = song->pool->list->data; g_return_if_fail(pool_item);
			AMTrack* trk = c->user_data;
			assert(trk, "no empty tracks");
			AMPos length = {4, 0, 0};
			uint64_t length_mu = MIN(pos2mu(&length), ayyi_samples2mu(((Waveform*)pool_item)->n_frames)); //make sure part isnt too long for the pool item
			unsigned colour = 10;

			AMPos start = {n_parts_added * 4 + 4, 0, 0};
			char name[64];
			ayyi_region__make_name_unique(name, "Test Part");
			AyyiSongPos pos;
			songpos_gui2ayyi(&pos, &start);
			am_song__add_part(AYYI_AUDIO, AM_REGION_FULL, pool_item, trk->ident.idx, &pos, length_mu, colour, name, 0, p_added, c);
		}
		typedef void (*AFn) (Closure*);
		static AFn next_part_; next_part_ = next_part;

		void part_added (AyyiIdent obj, GError** error, gpointer c_data)
		{
			gboolean wait (gpointer user_data)
			{
				next_part_(user_data);
				return G_SOURCE_REMOVE;
			}
			PF;
			Closure* c = c_data;
			if(error && *error) c->failed = true;
			FAIL_IF_ERROR
			n_parts_added++;
			if(n_parts_added < 10)
				g_timeout_add(fast ? 1 : 500, wait, c);
			else
				c->next(c); //all parts added
		}
		p_added = part_added;

		next_part(c);

		return G_SOURCE_REMOVE;
	}

	void task1 (Closure* closure)
	{
		g_timeout_add(fast ? 1 : 1000, test_create_parts_, closure);
	}

	void task2 (Closure* closure)
	{
		typedef struct {
			Closure* c;
			void (*done)(GObject*, gpointer);
		} C;

		bool finish (C* c)
		{
			int n = g_signal_handlers_disconnect_by_func (song, c->done, c);
			if(!n) TEST_FAIL_NOSTEP("failed to disconnect transport handler.");
			g_free(c);
			return FALSE;
		}

		void task2_on_play (GObject* object, gpointer _c)
		{
			PF;
			C* c = _c;
			assert(ayyi_transport_is_playing(), "transport not playing");

			((Closure*)c->c)->next(c->c);
			finish(c);
		}

		C* c = AYYI_NEW(C,
			.c = closure,
			.done = task2_on_play,
		);

		am_song__connect("transport-play", G_CALLBACK(c->done), c);

		am_transport_jump(&(AyyiSongPos){12, 0});
		ayyi_transport_stop(NULL, NULL);
		ayyi_transport_play(NULL, NULL);
	}

	void task3 (Closure* closure)
	{
		ayyi_transport_stop();

		gboolean task3_done (gpointer _closure)
		{
			((Closure*)_closure)->next(_closure);
			return G_SOURCE_REMOVE;
		}
		g_timeout_add(fast ? 1 : 250, task3_done, closure);
	}

	static int n_tasks = 3;
	Task* tasks = g_new0(Task, n_tasks + 1);
	tasks[0] = task1;
	tasks[1] = task2;
	tasks[2] = task3;

	void on_have_track (AyyiIdent t, gpointer _c)
	{
		AMTrack* track = am_track_list_find_by_ident(song->tracks, t);
		((Closure*)_c)->user_data = track;

		void tasks_start (Closure* closure)
		{
			closure->tasks[0](closure);
		}

		tasks_start((Closure*)_c);
	}
	ensure_blank_track(on_have_track, AYYI_NEW(Closure,
		.next = tasks_next,
		.tasks = tasks
	));
}


void
test_part_split ()
{
	TEST_START;
	if(!song->parts->list) TEST_FAIL_("no parts");

	static int n_parts; n_parts = g_list_length(song->parts->list);

	void test_part_split_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		if(!error || !*error){
			dbg(0, "n_parts=%i-->%i", n_parts, g_list_length(song->parts->list));
			if(g_list_length(song->parts->list) != n_parts + 1) TEST_FAIL_("incorrect number of parts.");
			TEST_COMPLETED_;
		}else{
			char msg[256]; g_strlcpy(msg, (*error)->message, 256);
			g_error_free(*error);
			*error = NULL;
			TEST_FAIL_("%s", msg);
		}
	}

	bool am_part__is_longer (AMPart* part, AMPos* pos)
	{
		AMPos length; songpos_ayyi2gui(&length, &part->length);
		if(!pos_is_before(&length, pos)){
			dbg(0, "using part: length=%u test=%u", ayyi_pos2samples(&part->length), pos2samples(pos));
			return true;
		}
		return false;
	}

	FilterIterator* i = am_part_iterator_new ((Filter)am_part__is_longer, &((AMPos){0, 1, 0}));
	AMPart* part = (AMPart*)filter_iterator_next(i);
	if(!part) TEST_FAIL_("no long enough parts");
	filter_iterator_unref(i);

	AyyiSongPos length = part->length;
	ayyi_pos_divide(&length, 2);
	ayyi_pos_add(&length, &part->start);

	am_part_split(part, &length, test_part_split_done, NULL);
}


void
add_track_with_parts ()
{
	// create track and put parts on it

	static AMTrack* trk; trk = NULL;

	TEST_START;

	gboolean _add_parts ()
	{
		static int step = 0;
		Arrange* arrange = ARRANGE_FIRST;

		AMPoolItem* pool_item = song->pool->list->data; g_return_val_if_fail(pool_item, false);
		AMPos length = {4,};
		uint64_t length_mu = MIN(pos2mu(&length), ayyi_samples2mu(((Waveform*)pool_item)->n_frames)); //make sure part isnt too long for the pool item
		unsigned colour = 10;

		void part_add_done(AyyiIdent id, GError** error, gpointer _)
		{
			PF;
			if(error) TEST_FAIL_("error adding part");
		}

		switch(step){
			case 0 ... 9:
				assert_stop(trk, "no track");
				song_part_new(pool_item, NULL, &(AMPos){step * 4 + 4, 0}, trk, length_mu, NULL, colour, NULL, part_add_done, NULL);
				arr_zoom_song(arrange);
				break;
			case 10:
				;GList* parts = am_partmanager__get_by_track(trk);
				if(!parts) TEST_FAIL("track has no parts");

				am_transport_jump(&(AyyiSongPos){12,});
				ayyi_transport_play(NULL, NULL);
				break;
			case 11:
				ayyi_transport_stop();
				break;
			/*
			case 7:
				am_song__remove_all_parts(NULL, NULL);
				break;
			*/
			default:
				TEST_COMPLETED;
		}

		step++; return TRUE;
	}

	void track_add_done (AMTrack* tr, GError** error, gpointer _)
	{
		assert(tr && !(error && *error), "track add failed: track=%p error=%p", tr, error);
		trk = tr;

		g_timeout_add(fast ? 1 : 1000, _add_parts, NULL);
	}

	song_add_track(TRK_TYPE_AUDIO, 1, track_add_done, NULL);
}


void
test_track_add_remove ()
{
	//create and destroy a track of each type

	TEST_START;

	static Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) TEST_FAIL_("no Arrange window");

	static int n_tracks = 0;
	static AyyiIdx new_object_idx;
	static ArrTrk* arr_trk = NULL;
	static int iter; iter = 0;
	static void (*do_iter)();

	void step_3_on_delete (AyyiIdent id, GError** error, gpointer _)
	{
		assert(am_track_list_count(song->tracks) == n_tracks, "delete: track count failed. expected=%i got=%i", n_tracks, ayyi_song__get_audio_track_count());

		if(++iter < 2){
			g_timeout_add(fast ? 1: 1000, pause_, do_iter);
		}else{
			TEST_COMPLETED_;
		}
	}

	void step_2_on_add (gpointer _)
	{
		AMPoolItem* pool_item = song->pool->list->data; g_return_if_fail(pool_item);

		assert(arr_track_select(arrange, arr_trk->track), "selection");

		song_del_track(arrange->tracks.selection, step_3_on_delete, NULL);
	}

	void step_1 ()
	{
		void test_on_track_add (AMTrack* new_track, GError** error, gpointer _)
		{
			new_object_idx = new_track->ident.idx;

			assert(am_track_list_count(song->tracks) == n_tracks + 1, "add: count! expected %i, got %i", n_tracks + 1, am_track_list_count(song->tracks));

			AMTrack* trk = (iter % 2)
				? am_track_list_find_by_ident(song->tracks, new_track->ident)
				: am_track_list_find_audio_by_idx(song->tracks, new_object_idx);
			if(!trk) fail__("new track not found with id=%i.%i", new_track->ident.type, new_object_idx);
			if(!(arr_trk = track_list__lookup_track(arrange->priv->tracks, trk))) fail__("gui track not added?");
			automation_view_pan(arr_trk);
			g_timeout_add(fast ? 1 : 2000, pause_, step_2_on_add);
		}

		n_tracks = am_track_list_count(song->tracks);
		song_add_track((iter % 2) ? TRK_TYPE_MIDI : TRK_TYPE_AUDIO, 1, test_on_track_add, NULL);
	}
	do_iter = step_1;

	step_1();
}


void
__add_track (gpointer done, gpointer user_data)
{
	typedef struct {
		void (*callback)(AyyiIdent, gpointer);
		gpointer user_data;
	} C;

	void __add_track_done (AMTrack* new_track, GError** error, gpointer _c)
	{
		C* c = _c;
		AyyiIdx new_object_idx = new_track->ident.idx;

		AMTrack* trk = am_track_list_find_audio_by_idx(song->tracks, new_track->ident.idx);
		if(!trk) fail__("new track not found with idx=%i", new_object_idx);

		c->callback(new_track->ident, c->user_data);

		g_free(c);
	}

	song_add_track(TRK_TYPE_AUDIO, 1, __add_track_done, AYYI_NEW(C,
		.callback = done,
		.user_data = user_data,
	));
}


void
test_add_n_tracks ()
{
	TEST_START;
	static int target_n_tracks = 8;

	typedef struct {
		int n_done;
	} C;

	void _add_track_done(AyyiIdent obj, C* c)
	{
		if(++c->n_done >= target_n_tracks){
			dbg(0, "target reached. Finishing...");
			g_free(c);
			TEST_COMPLETED_;
		}else{
			dbg(0, "%i of %i (%i)", c->n_done, target_n_tracks, am_track_list_count(song->tracks));
			__add_track((gpointer)_add_track_done, c);
		}
	}

	__add_track((gpointer)_add_track_done, g_new0(C, 1));
}


void
test_move_track ()
{
	TEST_START;
	Arrange* arrange = ARRANGE_FIRST;
	TrackList* tl = arrange->priv->tracks;

	int n_tracks = track_list__count_disp_items(tl);
	assert(n_tracks > 2, "not enough tracks: %i", n_tracks);

	ArrTrk* to_move = track_list__track_by_display_index(tl, 2);

	if(!arr_track_move_to_pos(arrange, 2, 1)) TEST_FAIL_("move failed");

	ArrTrk* moved = track_list__track_by_display_index(tl, 1);

	assert(to_move == moved, "track not moved");

	// move it back again
	if(!arr_track_move_to_pos(arrange, 2, 1)) TEST_FAIL_("move failed");

	assert(track_list__track_by_display_index(tl, 2) == to_move, "track not moved");

	TEST_COMPLETED_;
}


static void
find_free_position (AMTrack* track, AyyiSongPos* pos)
{
	int n = 0;
	while(true){
		char bbst[32]; ayyi_pos2bbst(pos, bbst);
		dbg(1, "  %s", bbst);
		FilterIterator* i = am_part_iterator_new ((Filter)am_part__is_at_position, pos);
		AMPart* part = (AMPart*)filter_iterator_next(i);
		if(!part){
			filter_iterator_unref(i);
			break;
		}
		ayyi_pos_add(pos, &(AyyiSongPos){0, 1, 0});
		if(n++ > 1 << 15) break;
	}
}


void
test_part_copy ()
{
	//copy a single part.

	TEST_START;
	static int initial_part_count;
	static int step; step = 0;

	gboolean _test_part_copy ()
	{
		switch(step){
			case 0:
				{
					int track_offset = 0;
					AyyiSongPos start = {0,};
					AMPart* part = song->parts->list->data;
					find_free_position(part->track, &start);

					initial_part_count = g_list_length(song->parts->list);

					if(!song->parts->list) fail("no parts to copy.");
					am_collection_selection_replace ((AMCollection*)song->parts, g_list_append(NULL, part), NULL); //select first part
					song_add_parts_from_selection(NULL, &start, track_offset);
				}
				break;
			case 1:
			case 2:
			case 3:
				break;
			default:
				assert_stop((g_list_length(song->parts->list) == initial_part_count + 1), "wrong Part count.");
				TEST_COMPLETED;
		}

		step++;
		return G_SOURCE_CONTINUE;

	abort:
		step = 0;
		passed = FALSE;
		test_finished();
		return G_SOURCE_REMOVE;
	}
	g_timeout_add(fast ? 1 : 800, _test_part_copy, NULL);
}


void
test_track_rename ()
{
	TEST_START;

	void test_rename_track_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		FAIL_IF_ERROR;

		AMTrack* tr = am_track_list_find_by_ident(song->tracks, id);
		if(!tr) TEST_FAIL_("cant find track");

		AyyiTrack* track = (AyyiTrack*)am_track__get_shared(tr);
		if(!track) TEST_FAIL_("cant find ayyi track");

		//check for playlist mismatch
		bool playlist_valid (AMTrack* tr)
		{
			bool ok = true;
			GList* parts = am_partmanager__get_by_track(tr);
			if(parts){
				GList* l = parts;
				for(;l;l=l->next){
#ifdef OLD
					AyyiRegionBase* part = ((AMPart*)l->data)->ayyi;

					bool found = false;
					AyyiPlaylist* playlist = NULL;
					while((playlist = ayyi_song__playlist_next(playlist))){
						if(!strcmp(playlist->name, part->playlist_name)){ found = true; break; }
					}
					if(!found){ ok = false; break; }
#endif
				}
				g_list_free(parts);
			}
			else pwarn("no parts. nothing to test.");
			return ok;
		}

		if(!playlist_valid(tr)) TEST_FAIL_("playlist invalid for track '%s'", tr->name);

		TEST_COMPLETED_;
	}

	AMTrack* track = am_track_list_get_first_visible(song->tracks, TRK_TYPE_AUDIO);
	char* name = g_strdup_printf("%s-renamed", track->name);
	am_track__set_name(track, name, test_rename_track_done, name);
	g_free(name);
}


void
test_zoom ()
{
	TEST_START;
	Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) TEST_FAIL_("no Arrange windows");

	typedef struct _closure {
		int      iter;
		Arrange* arrange;
		float    zoom;
	} C;

	C* c = AYYI_NEW(C,
		.arrange = arrange,
		.zoom = 0.1
	);

	gboolean on_zoom (gpointer data)
	{
		C* c = data;

		if(c->iter++ > 17){
			arr_zoom_set(c->arrange, 2.0, 2.0);
			TEST_COMPLETED_;
			g_free(c);
			return G_SOURCE_REMOVE;
		}
		c->zoom *= 1.2;
		dbg(1, "zoom=%.2f", c->zoom);
		arr_zoom_set(c->arrange, c->zoom, c->zoom);

		return G_SOURCE_CONTINUE;
	}

	arr_zoom_set(arrange, c->zoom, c->zoom);
	g_timeout_add(fast ? 1 : 600, on_zoom, c);
}


void
test_window_open_close ()
{
	TEST_START;
	static GHashTableIter iter;
	g_hash_table_iter_init (&iter, app->panel_classes);
	static int n_shells; n_shells = g_list_length(windows);
	static AyyiWindow* window = NULL;
	static GtkWidget* panel = NULL;
	static int step; step = 0;

	static int n_windows_done = 0;
	static int n_windows; n_windows = g_hash_table_size(app->panel_classes);
	dbg(0, "n_windows=%i", n_windows);

	// this is a long test, so disable the timeout
	g_source_remove0 (timer);

	void each (gpointer key, gpointer value, gpointer user_data)
	{
		dbg(0, "  %s", g_type_name(*((gint64*)key)));
	}
	g_hash_table_foreach(app->panel_classes, each, NULL);

	gboolean _test_window_open_close ()
	{
		gpointer key, value;

		if(!(step % 2)){
			if(g_hash_table_iter_next(&iter, &key, &value)){
				assert_stop((g_list_length(windows) == n_shells), "number of shells: %i (expected %i)", g_list_length(windows), n_shells)

				// open window
				PanelType panel_type = *((gint64*)key);
				AyyiPanelClass* klass = g_type_class_peek(panel_type);
				dbg(0, "class=%s", klass->name);
				window = window__new_floating(klass);
				panel = window->dock->panels->data;
			}else{
				if(n_windows_done < n_windows) TEST_FAIL("not enough windows done: %i/%i\n", n_windows_done, n_windows);
				else TEST_COMPLETED;
			}
		} else{
			// close window
			assert_stop(window, "no window");
			assert_stop((g_list_length(windows) == n_shells + 1), "number of shells")
			assert_stop(g_list_length(window->dock->panels) == 1, "wrong number of panels");
			assert_stop(panel, "no panel widget");
			assert_stop((panel == (GtkWidget*)window->dock->panels->data), "wrong widget");

			AyyiWindow* f = (AyyiWindow*)gtk_widget_get_toplevel(panel);
			window__destroy_panel(f, AYYI_PANEL(panel));
			windows__print();
			windows__print_items();
			n_windows_done++;
		}
		step++;
		return G_SOURCE_CONTINUE;

	//abort:
		TEST_FAIL(" ");
	}
	g_timeout_add(1000, _test_window_open_close, NULL);
}


void
test_canvas_change ()
{
	TEST_START;

	static Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) TEST_FAIL_("no Arrange windows");

	struct _closure
	{
		Arrange* arrange;
		GHashTableIter iter;
		bool (*next)(struct _closure*);
		int pass;
	};

	bool next_canvas(struct _closure* closure)
	{
		PF;
		gpointer key, value;
		if(g_hash_table_iter_next (&closure->iter, &key, &value)){
			CanvasType canvas_type = *((int*)key);
			arr_set_canvas(closure->arrange, canvas_type);
			g_timeout_add(1500, (GSourceFunc)closure->next, closure);
		}else{
			if(++closure->pass < 2){
				g_hash_table_iter_init (&closure->iter, canvas_types);
				g_timeout_add(1500, (GSourceFunc)closure->next, closure);
			}else{
				#ifdef USE_GNOMECANVAS
				//return to the main canvas type.
				if(!CANVAS_IS_GNOME){
					arr_set_canvas(closure->arrange, gcanvas_get_type());
				}
				#endif

				g_free(closure);
				TEST_COMPLETED_;
			}
		}
		return G_SOURCE_REMOVE;
	}

	struct _closure* closure = g_new0(struct _closure, 1);
	closure->arrange = arrange;
	closure->next = next_canvas;
	g_hash_table_iter_init (&closure->iter, canvas_types);

	next_canvas(closure);
}


void
test_automation ()
{
	TEST_START;
	static Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) TEST_FAIL_("failed");

	gboolean on_1_done(gpointer data)
	{
		TEST_COMPLETED_;
		return G_SOURCE_REMOVE;
	}

	g_timeout_add(fast ? 1 : 1500, on_1_done, arrange);
}


void
test_time ()
{
	TEST_START;
	static Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) TEST_FAIL_("Arrange window");

	uint64_t mu = px2mu_nz(PX_PER_BEAT);
	assert((mu == AYYI_SUBS_PER_BEAT * AYYI_MU_PER_SUB), "mu=%"PRIu64, mu);

#if 0 //these tests fail - off by one
	char bbst[64];
	char bbst2[64];

	AMPos p;
	AMPos p = {0,0,TICKS_PER_SUBBEAT-1};
	mu = px2mu_nz(pos2px_nz(&p));
	int64_t samples = mu2samples(mu);
	if(samples != pos2samples(&p)){ pos2bbst(&p, bbst); pos2bbst(&p, bbst2); perr("%s mu=%Lu samples=%Li!=%i %s", bbst, mu, samples, pos2samples(&p), bbst2); TEST_FAIL_(""); }

	p = (AMPos){1,0,1};
	mu = px2mu_nz(pos2px_nz(&p));
	int64_t samples = mu2samples(mu);
	if(samples != pos2samples(&p)){ pos2bbst(&p, bbst); perr("%s mu=%Lu samples=%Li!=%i", bbst, mu, samples, pos2samples(&p)); TEST_FAIL_(""); }

	p = (AMPos){2,1,0};
	mu = px2mu_nz(pos2px_nz(&p));
	int64_t samples = mu2samples(mu);
	if(samples != pos2samples(&p)){ pos2bbst(&p, bbst); perr("%s mu=%Lu samples=%Li!=%i", bbst, mu, samples, pos2samples(&p)); TEST_FAIL_(""); }
#endif

	static float tempo[4] = {120.0, 60.0, 119.0, 120.0};
	static float zoom[4]  = {  1.0,  1.0,   1.0,   2.0};
	typedef struct {
		int         i;
		float*      tempo;
		float*      zoom;
		AyyiHandler done_one;
	} C;

	void test_px_per_sample (float tempo, AyyiHandler done, gpointer _c)
	{
		void test_tempo_done(AyyiIdent t, GError** error, gpointer _c)
		{
			C* c = _c;

			assert((((AyyiSongService*)ayyi.service)->song->bpm == c->tempo[c->i]), "incorrect tempo");

			arr_set_hzoom((AyyiPanel*)arrange, c->zoom[c->i]);
			assert(hzoom(arrange) == c->zoom[c->i], "zoom not %.2f", c->zoom[c->i]);

			float beats_per_second = c->tempo[c->i] / 60;
			float samples_per_pix = (song->sample_rate / beats_per_second) / (PX_PER_BEAT * c->zoom[c->i]);
			assert((ABS(arr_samples_per_pix(arrange) - samples_per_pix) < 1), "wrong samples_per_pix: %i %.3f", arr_samples_per_pix(arrange), samples_per_pix);

			((C*)_c)->done_one(AYYI_NULL_IDENT, NULL, _c);
		}

		am_song__set_tempo(tempo, test_tempo_done, _c);
	}

	void done_one (AyyiIdent id, GError** error, gpointer _c)
	{
		C* c = _c;
		c->i++;
		if(c->i < G_N_ELEMENTS(tempo)){
			test_px_per_sample(c->tempo[c->i], c->done_one, c);
		}else{
			g_free(c);
			TEST_COMPLETED_;
		}
	}

	test_px_per_sample(120.0, done_one, AYYI_NEW(C,
		.tempo = (float*)tempo,
		.zoom = (float*)zoom,
		.done_one = done_one
	));
}


void
test_time2 ()
{
	// test Arrange function spos2px_nz

	AyyiSongPos pos[] = {{0,}, {1,},        {0,1},                          {0, AYYI_SUBS_PER_BEAT/4}, {0,0,1}};
	double result[]   = {0.0,  PX_PER_BEAT, PX_PER_BEAT/AYYI_SUBS_PER_BEAT, PX_PER_SUBBEAT,            PX_PER_BEAT / AYYI_MU_PER_BEAT};
	int i; for(i=0;i<G_N_ELEMENTS(pos);i++){
		double a = spos2px_nz(&pos[i]);
												dbg(0, "%i %.20f", i, a);
		assert(a == result[i], "%i: expected %f got %f", i, result[i], a);
	}

	TEST_COMPLETED_;
}


void
test_never ()
{
}


static void
ensure_blank_track (AyyiFinish on_done, gpointer user_data)
{
	typedef struct {
		gpointer    user_data;
		AyyiFinish  on_done;
	} C;

	FilterIterator* i = am_track_iterator_new ((Filter)am_track__is_empty, NULL);
	AMTrack* track;
	do{
		track = (AMTrack*)filter_iterator_next(i);
	} while(track && !AM_TRK_IS_AUDIO(track));
	filter_iterator_unref(i);

	if(track){
		on_done(track->ident, user_data);
	}else{
		void ensure_audio_track_done (AyyiIdent obj, const GError* error, gpointer _c)
		{
			C* c = _c;
			c->on_done(obj, c->user_data);
			g_free(c);
		}

		am_song_add_track(TRK_TYPE_AUDIO, "New Track", 1, ensure_audio_track_done, AYYI_NEW(C,
			.on_done = on_done,
			.user_data = user_data
		));
	}
}
