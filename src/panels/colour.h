/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __panels_colour_h__
#define __panels_colour_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_COLOUR_WIN            (ayyi_colour_win_get_type ())
#define AYYI_COLOUR_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_COLOUR_WIN, AyyiColourWin))
#define AYYI_COLOUR_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_COLOUR_WIN, AyyiColourWinClass))
#define AYYI_IS_COLOUR_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_COLOUR_WIN))
#define AYYI_IS_COLOUR_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_COLOUR_WIN))
#define AYYI_COLOUR_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_COLOUR_WIN, AyyiColourWinClass))

typedef struct _AyyiColourWinClass AyyiColourWinClass;

struct _AyyiColourWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiColourWin
{
  AyyiPanel     panel;

  GtkWidget*    box[255];
  GtkWidget*    row[8];
  GtkWidget*    spacer[8];
  Pti           box_size;  //test to prevent window sizing loops.
};

#define colour_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_COLOUR_WIN) continue; \
		AyyiColourWin* colour_win = (AyyiColourWin*)panel;
#define end_colour_foreach end_panel_foreach

GType           ayyi_colour_win_get_type    ();
void            colour_win_update           ();

G_END_DECLS
#endif
