/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __toolbar_h__
#define __toolbar_h__

struct _ToolbarButton
{
    char*         key;
    char*         icon;
    gpointer      callback;
    GtkAction*    action;                // TODO if action used, icon property can be removed
    bool          (*active)(AyyiPanel*); // fn to check to see if the button should be marked 'active'.
};

typedef struct _toolbar
{
    GtkWidget*    b_transport[MAX_TRANSPORT_BUT]; // toolbar transport widgets.
    void*         tr_button_closure[MAX_TRANSPORT_BUT];
    GPtrArray*    closures;
} Toolbar;

typedef struct
{
	ToolbarButton* button;
	AyyiPanel*     panel;
} ButtonContext;

void        toolbar_init           ();
void        toolbar_class_free     ();

Toolbar*    toolbar_new            (AyyiPanel*, GtkWidget* container);
void        toolbar_free           (Toolbar**);
void        request_toolbar_update ();
void        toolbar_enable         (Toolbar*);
void        toolbar_disable        (Toolbar*);
gboolean    toolbar_on_resize      (GtkWidget*, GdkEvent*, gpointer);
void        toolbar_add_separator  (AyyiPanel*, int size);
void        toolbar_show_toggle    (AyyiPanel*);
GtkWidget*  toolbar_add_new_button (AyyiPanel*, ToolbarButton*);

int         toolbar_get_iconsize   (Arrange*);

gboolean    mouseover_enter        (GtkWidget*, GdkEventCrossing*, AyyiPanel*);
int         mouseover_leave        (GtkWidget*, GdkEventCrossing*, AyyiPanel*);

#endif //__toolbar_h__
