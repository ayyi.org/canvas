/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

typedef void (*RenamerCallback) (char*, gpointer);

void     track_rename_popup    (AyyiPanel*, GtkWidget* parent, Pti, gboolean map_type, AMTrack*, RenamerCallback, gpointer);
void     part_rename_popup     (AyyiPanel*, GtkWidget* parent, Pti, gboolean map_type, AMPart*, RenamerCallback, gpointer);

