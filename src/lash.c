/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#include "support.h"
#include "save.h"
#include "lash.h"

static lash_client_t* lash_client = NULL;


gboolean
start_lash(lash_args_t *args)
{
	lash_client = lash_init(args, "Seismix", LASH_Config_Data_Set | LASH_Config_File, LASH_PROTOCOL(2, 0));
	
	if (!lash_client){ /*warnprintf("could not initialise LASH\n");*/ return FALSE; }
	
	lash_event_t *event = lash_event_new_with_type(LASH_Client_Name);
	lash_event_set_string(event, "Seismix");
	lash_send_event(lash_client, event);
	return TRUE;
}

void*
lash_thread_func(void *unused)
{
	lash_event_t* event;
	lash_config_t* config;
	int quit = 0;
	char path[256];

	log_print(LOG_OK, "lash started");
	
	while (!quit) {
		while ((event = lash_get_event(lash_client))) {
			log_print(0, "lash event!");
			switch (lash_event_get_type(event)) {
				case LASH_Save_Data_Set:
					dbg(0, "event received: LASH_Save_Data_Set (ignored)");
					//Tell the client to send all its configuration data to the server with a number of configs. The client must always send a LASH_Save_Data_Set event back to the server when it has finished sending its configs. The event string will always be NULL.

					//lets assume we can save the data in this thread, but let the main thread do the gui stuff.
					g_idle_add(arr_statusbar_print_idle, "Saving song....");
					break;
				case LASH_Save_File:
					dbg(0, "event received: LASH_Save_File.");
					//Tell the client to save all its data to files within a specific directory. The event string will never be NULL and will contain the name of the directory in which the client should save its data. Clients must always send a LASH_Save_File event back to the server when they have finished saving their data. The client should not rely on the directory existing after it has sent its LASH_Save_File event back. It is valid behaviour for a client to save no files within the directory. Files should always be overwritten (ie, using the "w" flag with fopen(),) preferably without user confirmation if you care for their sanity.
					snprintf(path, 255, "%s/gui_data", lash_event_get_string(event));
					dbg(0, "lash path=%s", path);
					if(!save_gui_song_file(path)) log_print(LOG_FAIL, "song save");
					break;
				case LASH_Restore_File:
					dbg(0, "event received: LASH_Restore_File.");
					snprintf(path, 255, "%s/gui_data", lash_event_get_string(event));
					if(!load_gui_song_file(path)) log_print(LOG_FAIL, "song load");
					break;
				case LASH_Quit:
					dbg(0, "event received: LASH_Quit (ignored)");
					//quit = 1;
					break;
				default:
					pwarn("Unhandled LASH event: type %d, '%s''", lash_event_get_type(event), lash_event_get_string(event));
			}
		}
	
		while ((config = lash_get_config(lash_client))) {
			pwarn("Unexpected LASH config: %s", lash_config_get_key(config));
		}
	
		usleep(10000);
	}

	return 0;
}

