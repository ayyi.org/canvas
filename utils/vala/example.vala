/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;
using AM;
using ValaUtils;

public delegate void ClientDelegateType ();

public class Ayyi.ExampleClient : GLib.Object
{
	public AyyiClient* client;
	//public static ExampleClient app;
	public MainLoop loop;

	public static int main(string[] args)
	{
		return (new ExampleClient()).run();
	}

	public ExampleClient ()
	{
		client = AM.init(null);
		//_debug_ = 2;
	}

	public int run ()
	{
		AM.connect_all((error) => {
			if((bool)error){
				print("server connection failed.\n%s\n", error.message);
				return;
			}

			if (!client->got_shm) {
				print("not got shm.\n");
				return;
			}

			ready.begin(error);
		});

		(loop = new MainLoop (null, false)).run();

		return 0;
	}

	private async void ready (Error e)
	{
		print("connected.\n");

		Ident file_id = {-1, -1};
		yield ValaUtils.add_file("/usr/share/sounds/alsa/Front_Center.wav", (id) => {
			print("file added: idx=%u\n", id.idx);
			file_id = id;
		});
		AM.PoolItem* sample = pool__get_item_from_idx(file_id.idx);
		assert((bool)sample);

		Track* track = song->tracks->get_first_visible(TrackType.AUDIO);

		SongPos start = {4, 0, 0};
		Ident part_id = {-1, -1};
		yield ValaUtils.add_part(sample, AM_REGION_FULL, track, &start, "test part", (p_id) => {
			print("part created idx=%u\n", p_id.idx);
			part_id = p_id;
		});

		// move the first Part 1 bar later

		AM.Part* part = song->parts->find_by_ident(part_id);
		assert((bool)part);

		//char bbst[64]; pos2bbst(&part->start, bbst);
		//dbg (0, "from=%s", bbst);

		// define an offset time of 4 beats, 0 sub-beats, and 0 ticks
		SongPos delta = {4, 0, 0};

		// add the offset to the original Part start
		SongPos* position = &part->start;
		ayyi_pos_add(position, &delta);

		// request that the part be moved to the new position
		yield ValaUtils.move_part (part, position, (obj) => {
			print("moved.\n");
		});

		// copy the part by creating a new one passing the old part as a reference
		SongPos start2 = {12, 0, 0};
		yield ValaUtils.add_part(null, part->ident.idx, track, &start2, "copied part", (p_id) => {
			print("copied. idx=%u\n", p_id.idx);
		});

		/*
		show_filesources();
		show_connections();
		show_audio_tracks();
		show_midi_tracks();
		show_channels();
		show_audio_parts();
		show_midi_parts();
		*/

		loop.quit();
	}

	public void on_new_object (dynamic DBus.Object proxy, int object_type, int object_idx, void* data)
	{
	}

	/*
	private void
	show_filesources ()
	{
		print("-----------------------------------------------------------------\n");
		print("files:\n");
		print("  length name\n");
		AyyiFilesource* file = null;
		while((file = AyyiSong.filesource_next(file)) != null){
			print("  %u %s\n", file->length, (string)file->name);
		}
		print("-----------------------------------------------------------------\n");
	}

	private void
	show_connections ()
	{
		print("-----------------------------------------------------------------\n");
		print("connections:\n");
		AyyiConnection* conn = null;
		while((conn = AyyiSong.audio_connection_next(conn)) != null){
			print("  %u %s %s\n", conn->nports, (string)conn->device, (string)conn->name);
		}
		print("-----------------------------------------------------------------\n");
	}

	private void
	show_audio_tracks ()
	{
		print("-----------------------------------------------------------------\n");
		print("audio tracks:\n");
		AyyiTrack* track = null;
		while((track = AyyiSong.audio_track_next(track)) != null){
			print("  %i %s\n", track->shm_idx, (string)track->name);
		}
		print("-----------------------------------------------------------------\n");
	}

	private void
	show_midi_tracks ()
	{
		print("-----------------------------------------------------------------\n");
		print("midi tracks:\n");
		AyyiMidiTrack* track = null;
		while((track = AyyiSong.midi_track_next(track)) != null){
			print("  %i %s\n", track->shm_idx, (string)track->name);
		}
		print("-----------------------------------------------------------------\n");
	}

	private void
	show_channels ()
	{
		print("-----------------------------------------------------------------\n");
		print("channels:\n");
		AyyiChannel* channel = null;
		while((channel = AyyiMixer.channel_next(channel)) != null){
			print("  %i %s\n", channel->shm_idx, (string)channel->name);
		}
		print("-----------------------------------------------------------------\n");
	}


	private void
	show_audio_parts ()
	{
		print("-----------------------------------------------------------------\n");
		print("audio parts:\n");
		print("  idx len   flags name                  playlist\n");
		AyyiRegion* part = null;
		while((part = ayyi_song__audio_region_next(part)) != null){
			//GPos* l = &part->length;
			Ayyi.Playlist* playlist = AyyiSong.playlist_at(part->playlist);
			string playlist_name = ((bool)playlist) ? (string)playlist->name : "";
			print("  %3i %7u %3i %20s %20s\n", part->shm_idx, part->length, part->flags, (string)part->name, playlist_name);
		}
	*/

		/*
		print("pending parts:\n");
		GLib.List<AM.Part>* ll = song->parts_pending;//->first();
		for(;ll!=null;ll=ll->next){
			print("pending %p", (void*)ll->data);
		}
		*/
	/*
		print("-----------------------------------------------------------------\n");
	}


	private void
	show_midi_parts ()
	{
		print("-----------------------------------------------------------------\n");
		print("midi parts:\n");
		print("  idx len   flags name                  playlist\n");
		AyyiMidiRegion* part = null;
		while((part = ayyi_song__midi_region_next(part)) != null){
			Ayyi.Playlist* playlist = AyyiSong.playlist_at(part->playlist);
			string playlist_name = ((bool)playlist) ? (string)playlist->name : "";
			print("  %3i %7u %3i %20s %20s\n", part->shm_idx, part->length, part->flags, (string)part->name, playlist_name);
		}
		print("-----------------------------------------------------------------\n");
	}
	*/
}


