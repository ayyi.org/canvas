/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __gl_canvas_c__
#define __gl_canvas_priv__
#define __arrange_private__

#include "global.h"
#include <math.h>
#define USE_FBO // it is assumed that driver fbo support exists so if defined, will always be used.
#include <gdk/gdkkeysyms.h>
#include "agl/ext.h"
#include "agl/text/pango.h"
#include "agl/gtk.h"
#include "waveform/actor.h"
#include "model/time.h"
#include "model/midi_part.h"
#include "model/pool_item.h"
#include "model/track_list.h"
#include "model/transition_observable.h"
#include "icon.h"
#include "windows.h"
#include "window.statusbar.h"
#include "support.h"
#include "song.h"
#include "arrange.h"
#include "arrange/part.h"
#include "part_manager.h"
#include "../../shortcuts.h"
#include "toolbar.h"
#include "toolbox.h"
#include "gl_canvas_op.h"
#include "arrange/animator.h"
#include "arrange/shader.h"
#include "arrange/gl_meter.h"
#include "arrange/gl_track_control.h"
#include "arrange/view_object.h"

#include "gl_canvas.h"

#define BG_TEXTURE
#define GL_AUTOMATION
#define SHOW_LOG
#undef SHOW_LOG

extern SMConfig* config;

#define GLX_PIXMAP_WORKS 0
#define DIRECT true
#define ISOMETRIC_HEIGHT 100
#define ISOMETRIC_X0 100
#define TRACK_SPACING_Z -50
#define PART_TRANSITION_BORDER 2.0

#define RR  0  // canvas right margin - just for debugging. normally 0
#define RRf 0.0

static gboolean gl_initialised = false;
static CanvasType gl_canvas_type = 0;
#if 0
static int frame_count = 0;
#endif

AGl* agl = NULL;

GtkWidget*      arr_gl_canvas_new          (Arrange*);
static void     arr_gl_class_init          ();
static void     arr_gl_canvas_free         (Arrange*);

// canvas api
static void     arr_gl_get_scroll_offsets  (Arrange*, int* x, int* y);
static void     arr_gl_px_to_model         (Arrange*, Pti, AyyiSongPos*, ArrTrk**);
static uint32_t arr_gl_get_viewport_left   (Arrange*);
static double   arr_gl_pos2px              (Arrange*, GPos*);
static float    arr_gl_samples2px          (Arrange*, uint32_t);
static void     arr_gl_canvas_pick         (Arrange*, double, double, TrackDispNum*, AMPart**);
static void     arr_gl_part_editing_start  (Arrange*, AMPart*);
static void     arr_gl_part_editing_stop   (Arrange*, AMPart*);
static void     arr_gl_get_view_fraction   (Arrange*, double* fx, double* fy);
static void     arr_gl_set_part_selection  (Arrange*);
static bool     arr_gl_part_new            (Arrange*, AMPart*);
static void     arr_gl_on_part_delete      (Arrange*, AMPart*);
static void     arr_gl_get_part_rect       (Arrange*, AMPart*, DRect*);
static void     arr_gl_do_follow           (Arrange*);
static void     arr_gl_scroll_to           (Arrange*, int x, int y);
static void     arr_gl_scroll_to_pos       (Arrange*, GPos*, TrackDispNum);
static void     arr_gl_scroll_left         (Arrange*, int pix);
static void     arr_gl_scroll_right        (Arrange*, int pix);
static void     arr_gl_scroll_up           (Arrange*, int pix);
static void     arr_gl_scroll_down         (Arrange*, int pix);
static void     arr_gl_set_spp             (Arrange*, uint32_t);
static FeatureType arr_gl_pick_feature     (CanvasOp*);
static void     arr_gl_on_song_load        (Arrange*);
static void     arr_gl_on_view_change      (Arrange*, AMChangeType);
static void     arr_gl_on_part_change      (Arrange*, AMPart*);
static void     arr_gl_on_new_track        (Arrange*, ArrTrk*);
static void     arr_gl_on_track_delete     (Arrange*, ArrTrk*);
static void    _arr_gl_on_track_delete     (GObject* _song, AMTrack*, Arrange*);
static void     arr_gl_on_locators_change  (Arrange*);
// end api

static void     arr_gl_canvas_on_realise   (GtkWidget*, gpointer);
static void     arr_gl_canvas_on_unrealise (GtkWidget*, gpointer);
static bool     arr_gl_canvas_expose       (GtkWidget*, GdkEventExpose*, gpointer data);
static void     arr_gl_on_allocate         (GtkWidget*, GtkAllocation*, Arrange*);
static void     arr_gl_on_hscroll          (GtkWidget*, Arrange*);
static void     arr_gl_on_vscroll          (GtkWidget*, Arrange*);
static void     arr_gl_on_zoom             (Observable*, AMVal, gpointer);
static void     arr_gl_on_zoom_frame       (Observable*, AMVal, gpointer);
static void     arr_gl_canvas_redraw_fast  (Arrange*);

static AGlActorNew
#ifdef DEBUG
                arr_gl_frame,
#endif
#ifdef SHOW_LOG
                log_actor,
#endif
#ifndef USE_FBO
                arr_gl_parts,
#endif
                spp_actor,
                arr_gl_tracks;

static AGlActor*arr_gl_track_automation    (ArrTrk*);

static void     gl_set_palette_colour      (int);
#ifdef USE_FBO
#ifdef NOT_USED
static void     fbo_2_png                  (AGlFBO*);
#endif
#endif

static void     arr_gl_setup_view          (Arrange*);
static void     arr_gl_on_iso_button_press (GtkWidget*, ButtonContext*);
static void     arr_gl_on_iso_key_press    (GtkAccelGroup*, gpointer);
static void     arr_gl_enable_isometric    (Arrange*);
static void     arr_gl_disable_isometric   (Arrange*);
static WfViewPort
                arr_gl_get_viewport        (Arrange*);

static void     arr_gl_set_tc_width        (Arrange*, int);

static void     menu_arr_set_gl_canvas     (Arrange*);

#define GL_TRACK_HEIGHT(atr) ((atr->height * (1-c->iso_factor) + ISOMETRIC_HEIGHT * c->iso_factor) * vzoom(arrange))

#define GL_TRACK_HEIGHT2(TCA) ((((AGlActor*)TCA)->region.y2 - ((AGlActor*)TCA)->region.y1) * (1-c->iso_factor) \
                  + (ISOMETRIC_HEIGHT * vzoom(arrange)) * c->iso_factor)

typedef void (*ViewChange) (AGlActor*, AMChangeType);
ViewChange on_change[ACTOR_TYPE_MAX] = {NULL,};

#include "gl_background.c"
#include "gl_part.c"
#include "gl_track.c"
#include "gl_tracks.c"
#include "gl_ruler.c"
#include "gl_button.c"
#include "gl_divider.c"
#include "gl_grid.c"
#include "gl_op.c"
#include "gl_track_automation.c"
#include "gl_spp.c"
#include "gl_overview.c"
#include "gl_scrollbar.c"
#include "gl_target.c"
#include "gl_iso.c"

extern GHashTable* canvas_types;

AMAccel gl_keys[] = {
	{"Isometric",   {{(char)'i',},}, arr_gl_on_iso_key_press, NULL, NULL},
};

#define ROOT() ((AGlRootActor*)CGL->actor)


void
arr_gl_canvas_init ()
{
	gl_canvas_type = arr_register_canvas("gl", arr_gl_canvas_new, menu_arr_set_gl_canvas);
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &gl_canvas_type);

	cc->provides = PROVIDES_METERS | PROVIDES_RULERBAR | PROVIDES_TRACKCTL | PROVIDES_OVERVIEW | PROVIDES_SIZEGROUP;
}


CanvasType
arr_gl_canvas_get_type ()
{
	return gl_canvas_type;
}


static void
arr_gl_class_init ()
{
	if(gl_initialised) return;

	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_ARRANGE);

	GimpActionGroup* ag = shortcuts_add_group("Gl Arrange");

	GtkAccelGroup* g = gtk_accel_group_new();
	make_accels(g, ag, gl_keys, G_N_ELEMENTS(gl_keys), NULL);
	k->accels = g_list_append(k->accels, g);

	agl = agl_get_instance();

	on_change[ACTOR_TYPE_RULER] = arr_gl_ruler_on_view_change;
	on_change[ACTOR_TYPE_OVERVIEW] = arr_gl_overview_on_view_change;

	gl_initialised = true;

#if 0
	gboolean check_framerate(gpointer _)
	{
		printf("n_frames=%.1f\n", ((float)frame_count) / 10.0);
		frame_count = 0;
		return TIMER_CONTINUE;
	}
	g_timeout_add(10000, check_framerate, NULL);
#endif
}


GtkWidget*
arr_gl_canvas_new (Arrange* arrange)
{
	PF;
	AyyiPanel* panel = (AyyiPanel*)arrange;
	AyyiPanelClass* panel_class = g_type_class_peek(AYYI_TYPE_ARRANGE);
	arr_gl_class_init();

	void on_ctl_show (Arrange* arrange, ArrTrk* atr, ArrAutoPath* aap)
	{
		AGlActor* track = (AGlActor*)arr_gl_tracks_find_track (CGL->actors[ACTOR_TYPE_TRACKS], atr);
		AGlActor* actor = agl_actor__find_by_class (track, &track_automation_actor_class);
		if (actor) {
			agl_actor__set_size(track);
		} else {
			AGlActor* a = agl_actor__add_child(track, arr_gl_track_automation(atr));
			agl_actor__set_size(a);
		}
	}

	void on_ctl_hide (Arrange* arrange, ArrAutoPath* aap)
	{
		AGlActor* actor = (AGlActor*)arr_gl_tracks_find_track (CGL->actors[ACTOR_TYPE_TRACKS], arrange->automation.sel.track);

		if (arr_track_count_active_controls(arrange->automation.sel.track)) {
			agl_actor__set_size(actor);
		} else {
			AGlActor* node = agl_actor__find_by_class (actor, &track_automation_actor_class);
			agl_actor__remove_child(node->parent, node);
		}
	}

	void on_pt_add (Arrange* arrange, ArrAutoPath* aap, Curve* curve, int ins_pos)
	{
		AGlActor* actor = (AGlActor*)arr_gl_tracks_find_track (CGL->actors[ACTOR_TYPE_TRACKS], arrange->automation.sel.track);
		agl_actor__invalidate(actor);
	}

	void on_pt_remove (Arrange* arrange, ArrAutoPath* aap, Curve* curve, int new_selection)
	{
		AGlActor* actor = (AGlActor*)arr_gl_tracks_find_track (CGL->actors[ACTOR_TYPE_TRACKS], arrange->automation.sel.track);
		agl_actor__invalidate(actor);
	}

	ArrCanvas* canvas = arrange->canvas = AYYI_NEW(ArrCanvas,
		.type                 = arr_gl_canvas_get_type(),
		.free                 = arr_gl_canvas_free,
		.get_scroll_offsets   = arr_gl_get_scroll_offsets,
		.px_to_model          = arr_gl_px_to_model,
		.get_viewport_left    = arr_gl_get_viewport_left,
		.pick                 = arr_gl_canvas_pick,
		.get_view_fraction    = arr_gl_get_view_fraction,
		.set_part_selection   = arr_gl_set_part_selection,
		.part_new             = arr_gl_part_new,
		.part_delete          = arr_gl_on_part_delete,
		.get_part_rect        = arr_gl_get_part_rect,
		.do_follow            = arr_gl_do_follow,
		.scroll_to            = arr_gl_scroll_to,
		.scroll_to_pos        = arr_gl_scroll_to_pos,
		.scroll_left          = arr_gl_scroll_left,
		.scroll_right         = arr_gl_scroll_right,
		.scroll_up            = arr_gl_scroll_up,
		.scroll_down          = arr_gl_scroll_down,
		.set_spp              = arr_gl_set_spp,
		.pick_feature         = arr_gl_pick_feature,
		.part_editing_start   = arr_gl_part_editing_start,
		.part_editing_stop    = arr_gl_part_editing_stop,

		.on_song_load         = arr_gl_on_song_load,
		.on_view_change       = arr_gl_on_view_change,
		.on_part_change       = arr_gl_on_part_change,
		.on_new_track         = arr_gl_on_new_track,
		.on_track_delete      = arr_gl_on_track_delete,
		.on_locator_change    = arr_gl_on_locators_change,

  		.on_auto_ctl_show     = on_ctl_show,
  		.on_auto_ctl_hide     = on_ctl_hide,
  		.on_auto_pt_add       = on_pt_add,
  		.on_auto_pt_remove    = on_pt_remove,

		.op                   = gl_canvas_op__new(arrange, OP_NONE),

		.h_adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 200.0, 1.0, 10.0, 10.0)),
		.v_adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 200.0, 1.0, 10.0, 10.0)),

		.gl = AGL_NEW(GlCanvas,
			.is_new = true,
			.isometric = false,
			.part_background_alpha = false, // c->isometric
			.iso_factor = 0,
			.zoom = (Observable*)transition_observable (panel->zoom),
			.parts = g_hash_table_new_full(NULL, NULL, NULL, g_free),
		)
	);

	GlCanvas* c = arrange->canvas->gl;

	void _arr_gl_on_song_unload (GObject* _arrange, gpointer _)
	{
		Arrange* arrange = (Arrange*)_arrange;
		if (arrange->canvas) {
			AGlActor* tc = CGL->actors[ACTOR_TYPE_TC];
			while (tc->children)
				agl_actor__remove_child(tc, tc->children->data);

			tc = CGL->actors[ACTOR_TYPE_TRACKS];
			while (tc->children)
				agl_actor__remove_child(tc, tc->children->data);

			CGL->actor->disabled = true;
		}
		arr_gl_canvas_redraw_fast(arrange);
	}
	g_signal_connect(arrange, "song-unload", G_CALLBACK(_arr_gl_on_song_unload), c);

	g_signal_connect(canvas->h_adj, "value-changed", G_CALLBACK(arr_gl_on_hscroll), arrange);
	g_signal_connect(canvas->v_adj, "value-changed", G_CALLBACK(arr_gl_on_vscroll), arrange);

	GtkWidget* table = c->table = canvas->container = gtk_table_new(2, 2, NON_HOMOGENOUS);

	AyyiPanel* existing = windows__find_panel(panel->window, AYYI_TYPE_ARRANGE);
	if (existing == panel) {
		for (GList* l=panel_class->accels;l;l=l->next) {
			GtkAccelGroup* group = l->data;
			gtk_window_add_accel_group(GTK_WINDOW(arrange->panel.window), group);
		}
	}

#ifdef ROTATION_SLIDERS
	// rotate slider 1
	GtkObject* rotate_adj = gtk_adjustment_new (0.0, -10.0, 10.0, 0.1, 1.0, 1.0);
	GtkWidget* rotate_scale = gtk_hscale_new (GTK_ADJUSTMENT (rotate_adj));
	gtk_scale_set_value_pos(GTK_SCALE(rotate_scale), GTK_POS_RIGHT);
	gtk_box_pack_start (GTK_BOX(arrange->sm_window.toolbar_box[1]), rotate_scale, TRUE, TRUE, 0);
	g_signal_connect((gpointer)rotate_scale, "value-changed", G_CALLBACK(gl_on_rotate), arrange);

	// rotate slider 2
	rotate_adj = gtk_adjustment_new (0.0, -10.0, 10.0, 0.1, 1.0, 1.0);
	rotate_scale = gtk_hscale_new (GTK_ADJUSTMENT (rotate_adj));
	gtk_scale_set_value_pos(GTK_SCALE(rotate_scale), GTK_POS_RIGHT);
	gtk_box_pack_start (GTK_BOX(arrange->sm_window.toolbar_box[1]), rotate_scale, TRUE, TRUE, 0);
	g_signal_connect((gpointer)rotate_scale, "value-changed", G_CALLBACK(gl_on_rotate2), arrange);

	// rotate slider 3
	rotate_adj = gtk_adjustment_new (0.0, -10.0, 10.0, 0.1, 1.0, 1.0);
	rotate_scale = gtk_hscale_new (GTK_ADJUSTMENT (rotate_adj));
	gtk_scale_set_value_pos(GTK_SCALE(rotate_scale), GTK_POS_RIGHT);
	gtk_box_pack_start (GTK_BOX(arrange->sm_window.toolbar_box[1]), rotate_scale, TRUE, TRUE, 0);
	g_signal_connect((gpointer)rotate_scale, "value-changed", G_CALLBACK(gl_on_rotate3), arrange);
#endif

	GtkAction* action = gtk_action_new("iso", "iso", "Tooltip", NULL/*SM_STOCK_ISO*/);
	gtk_action_set_icon_name (action, "iso");
	// TODO buttons added with toolbar_add_new_button do not have tooltips
	c->iso_button = toolbar_add_new_button(&arrange->panel, &(ToolbarButton){
		.key      = "iso",
		.action   = action,
		.callback = G_CALLBACK(arr_gl_on_iso_button_press)
	});

	c->image = arrange->canvas->widget = gtk_drawing_area_new();
	gtk_widget_set_can_focus(c->image, true);
	dbg(2, "setting gl_capability context=%p", agl_get_gl_context());
	gtk_widget_set_gl_capability(c->image, app_get_glconfig(), agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);
	gtk_widget_add_events (c->image, GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

	c->actor = agl_new_scene_gtk(c->image);
	SCENE->user_data = arrange;

	c->wfc = wf_context_new(c->actor);

	bool root_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		Arrange* arrange = ((AGlScene*)actor)->user_data;
		GlCanvas* c = arrange->canvas->gl;

		switch (event->type) {
			case GDK_BUTTON_PRESS:
				switch(event->button.button){
					case 3:
						shell__menu_popup(arrange->panel.window, event);
				}
				return AGL_HANDLED; // prevent dock drag operation starting
			case GDK_ENTER_NOTIFY:
				;int T[] = {ACTOR_TYPE_VSCROLLBAR, ACTOR_TYPE_HSCROLLBAR};
				for (int t=0;t<G_N_ELEMENTS(T);t++)
					if (c->actors[T[t]] && c->actors[T[t]]->disabled) {
						c->actors[T[t]]->disabled = false;
						agl_actor__invalidate(c->actors[T[t]]);
					}
				break;
			case GDK_LEAVE_NOTIFY:
				;GdkEventCrossing* e = (GdkEventCrossing*)event;
				if (e->detail == GDK_NOTIFY_ANCESTOR && c->actors[ACTOR_TYPE_VSCROLLBAR]) {
					c->actors[ACTOR_TYPE_VSCROLLBAR]->disabled = true;
					agl_actor__invalidate(c->actors[ACTOR_TYPE_VSCROLLBAR]);

					c->actors[ACTOR_TYPE_HSCROLLBAR]->disabled = true;
					agl_actor__invalidate(c->actors[ACTOR_TYPE_HSCROLLBAR]);
				}
				break;
		}
		return AGL_NOT_HANDLED;
	}

	c->actor->on_event = root_on_event;

	g_signal_connect((gpointer)c->image, "realize", G_CALLBACK(arr_gl_canvas_on_realise), arrange);
	g_signal_connect((gpointer)c->image, "unrealize", G_CALLBACK(arr_gl_canvas_on_unrealise), arrange);
	g_signal_connect((gpointer)c->image, "size-allocate", G_CALLBACK(arr_gl_on_allocate), arrange);
	g_signal_connect(G_OBJECT(c->image), "expose_event", G_CALLBACK(arr_gl_canvas_expose), arrange);

	gtk_table_attach(GTK_TABLE(table), c->image, 0, 1, 0, 1, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);

	gtk_drag_dest_set(GTK_WIDGET(arrange->canvas->widget), GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
	g_signal_connect(G_OBJECT(arrange->canvas->widget), "drag-data-received", G_CALLBACK(arr_canvas_drag_received), NULL);

	((PtObservable*)((AyyiPanel*)arrange)->zoom)->change = CHANGE_BOTH;
	observable_subscribe_with_state(panel->zoom, arr_gl_on_zoom, arrange);

	observable_subscribe_with_state(c->zoom, arr_gl_on_zoom_frame, arrange);

	return arrange->canvas_scrollwin = table;
}


static void
arr_gl_canvas_free (Arrange* arrange)
{
	// Do not free canvas->op here, it is a singleton

	PF;
	GlCanvas* c = arrange->canvas->gl;
	AyyiPanel* panel = (AyyiPanel*)arrange;
	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_ARRANGE);

	// should not be neccesary but just in case...
	#define handler_disconnect(FN, DATA) if(g_signal_handlers_disconnect_matched (c->image, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, DATA) != 1) pwarn("canvas handler disconnection: %s", #FN);
	handler_disconnect(agl_actor__on_event, c->actor);
	#undef handler_disconnect

	g_signal_handlers_disconnect_matched(arrange, G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_DATA, ((AyyiArrangeClass*)k)->song_unload_signal, 0, NULL, NULL, c);
	g_signal_handlers_disconnect_matched(song->tracks, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, _arr_gl_on_track_delete, arrange);

	for (GList* l = c->transitions;l;l=l->next) {
		wf_animation_remove(l->data);
	}
	g_list_free(c->transitions);

	AMTrack* tr;
	ArrTrk* at = NULL;
	while ((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (tr = at->track)) {
		if (tr->meterval)
			observable_unsubscribe (tr->meterval, NULL, at);
	}

	observable_unsubscribe(panel->zoom, arr_gl_on_zoom, arrange);
	g_clear_pointer(&c->zoom, transition_observable_free);

	arrange->canvas->op = NULL;

	if (c->wfc) wf_context_free0(c->wfc);
	g_clear_pointer(&c->actor, agl_actor__free);

	dbg(1, "destroying hashtables... parts.size=%i", g_hash_table_size(c->parts));
	g_hash_table_destroy0(c->parts);

	// TODO check is not attached
	agl_actor__free0(c->actors[ACTOR_TYPE_ISO_FRAME]);
	agl_actor__free0(c->actors[ACTOR_TYPE_ISO_TC]);

	gtk_widget_destroy0(arrange->canvas->widget);
	gtk_widget_destroy0(c->table);
	gtk_widget_destroy0(c->iso_button);
	g_free0(arrange->canvas->gl);
	g_free0(arrange->canvas);

	if(gtk_widget_get_toplevel((GtkWidget*)arrange) == (GtkWidget*)panel->window){ // this prevents gtk throwing a critical error
		for(GList* l=k->accels;l;l=l->next){
			GtkAccelGroup* group = l->data;
			gtk_window_remove_accel_group(GTK_WINDOW(panel->window), group);
		}
	}

	if(!arr_get_n_gl_windows()){
	}
}


static void
arr_gl_setup_view (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;
	g_return_if_fail(c);
	GtkWidget* widget = c->image;

	// setting the viewport affects both the gain and the maximium extent of the screen coords

	int vx = 0/* + (c->isometric ? 100 : 0)*/;
	int vy = 0;
	int vw = widget->allocation.width;
	int vh = widget->allocation.height;
	glViewport(vx, vy, vw, vh);
	dbg (3, "viewport: %i %i %i %i", vx, vy, vw, vh);

	dbg (3, "rot=%.2f %.2f %.2f", rotate[0], rotate[1], rotate[2]);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (c->isometric) {
		glRotatef(rotate[0], 1.0f, 0.0f, 0.0f);
		glRotatef(rotate[1], 0.0f, 1.0f, 0.0f);
		glScalef(1.0f, 1.0f, -1.0f);
	}
}


static void
arr_gl_create_part_wfactor (gpointer key, gpointer value, gpointer _canvas)
{
	PartActor* pa = (PartActor*)value;

	if(PART_IS_AUDIO((AMPart*)key) && !pa->wf_actor){
		AGlActor* wf = gl_part_add_audio_to_wf_canvas(pa);
		agl_actor__add_child((AGlActor*)pa, wf);
	}
}


static void
arr_gl_canvas_on_realise (GtkWidget* widget, gpointer _arrange)
{
	// Note that it can happen that we go through multiple realise/unrealise events when creating a new docked panel.

	// Canvas level objects must not be created until their parent objects are created.
	// ie before gl tracks and gl parts are created, the Panel level objects such as ArrTrk have been created by the Panel.

	PF;
	g_return_if_fail(_arrange);
	Arrange* arrange = _arrange;
	GlCanvas* c = arrange->canvas->gl;

	g_assert(c->wfc); // is created earlier now, and not destroyed on unrealise

	char font_string[64];
	get_font_string(font_string, -3); // TODO why do we have to set this so small to get a reasonable font size?
	agl_set_font_string(font_string);

	// connect signals - must be done _before_ actors are instantiated.
	if(c->is_new){
		g_signal_connect(song->tracks, "delete", G_CALLBACK(_arr_gl_on_track_delete), arrange);

		// we do not connect to song->tracks "added" - is done via canvas->on_new_track
		// we do not connect to song->pool "delete" - we expect a part-delete signal instead
	}

	if(!c->actor->children){
		typedef struct {
			int          type;
			AGlActorNew* new;
		} Item;

		void add_actors (AGlActor* actor, int n, Item actors[n], GlCanvas* c)
		{
			for (int i=0;i<n;i++) {
				Item* item = &actors[i];
				if (item->new) {
					AGlActor* child = agl_actor__add_child(actor, item->new((GtkWidget*)arrange));
					if (item->type > -1) c->actors[item->type] = child; // TODO remove c arg
				}
			}
		}

		Item actors[] = {
			{ACTOR_TYPE_BACKGROUND, arr_background},
#ifdef SHOW_LOG
			{-1,              log_actor},
#endif
#ifdef DEBUG
			{-1,              arr_gl_frame},
#endif
#ifndef USE_FBO
			{-1,              arr_gl_parts            },
#endif
			{-1,              arr_gl_divider          },
			{ACTOR_TYPE_MAIN, arr_gl_op_actor         },
			{ACTOR_TYPE_TC,   track_control_actor_new },
			{-1,              meter_actor_new         },
			{ACTOR_TYPE_RULER,arr_gl_ruler},
		};

		add_actors(c->actor, G_N_ELEMENTS(actors), actors, c);

		// parts and spp are done as a child of the MAIN actor.
		Item actors2[] = {
			{-1,                arr_gl_grid},
			{-1,                spp_actor},
			{ACTOR_TYPE_TRACKS, arr_gl_tracks},
			{ACTOR_TYPE_FG,     arr_gl_foreground},
		};
		add_actors(c->actors[ACTOR_TYPE_MAIN], G_N_ELEMENTS(actors2), actors2, c);

		// actors that are initially inactive
		c->actors[ACTOR_TYPE_ISO_FRAME] = arr_gl_iso_frame((GtkWidget*)arrange);
		c->actors[ACTOR_TYPE_ISO_TC] = arr_gl_iso_tc((GtkWidget*)arrange);

		AGlActor* scrollbar;
		agl_actor__add_child (c->actor, (c->actors[ACTOR_TYPE_HSCROLLBAR] = scrollbar = scrollbar_view(c->actor, AGL_ORIENTATION_HORIZONTAL, arrange->width), scrollbar->z = 100, scrollbar));
		agl_actor__add_child (c->actor, (c->actors[ACTOR_TYPE_VSCROLLBAR] = scrollbar = scrollbar_view(c->actor, AGL_ORIENTATION_VERTICAL, arrange->height), scrollbar->z = 100, scrollbar));

		AGlActor* overview;
		agl_actor__add_child(c->actor, (c->actors[ACTOR_TYPE_OVERVIEW] = overview = arr_gl_song_overview((GtkWidget*)arrange), overview->z = 99, overview));

		CanvasClass* cc = g_hash_table_lookup(canvas_types, &gl_canvas_type);
		if(!(cc->provides & PROVIDES_TRACKCTL)){
			c->actors[ACTOR_TYPE_MAIN]->region.x1 = 0;
			c->actors[ACTOR_TYPE_BACKGROUND]->region.x1 = 0;
		}
	}

	// WaveformActors are currently destroyed in unrealise so need to be recreated here

	// At startup we should not create parts here as they are created via arrange.on_song_load()
	// however it IS needed when the canvas is unrealised then realised again, or when a second window is opened.
	// *** TODO perhaps it is now no longer necc to remove actors on unrealise? ***

	// -if the song is not fully loaded, the arrange tracks may not be created yet, in which case this will be handled later by the song load handler.
	if(((AyyiPanel*)arrange)->model_is_loaded){
		if(!g_hash_table_size(TCTRACKS)){
		}

		if(!CGL->actors[ACTOR_TYPE_TRACKS]->children){
			// iterate over ArrTrk* so that the gl tracks will not be created until after the Panel level objects are done.
			AMTrack* tr;
			ArrTrk* at = NULL;
			while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (tr = at->track)){
				arr_gl_on_new_track(arrange, at);
			}
		}

		if(!g_hash_table_size(c->parts)){
			part_foreach {
				arrange->canvas->part_new(arrange, gpart);
			} end_part_foreach;
		}
	}

	if(gdk_gl_drawable_make_current(ROOT()->gl.gdk.drawable, ROOT()->gl.gdk.context)){

		if(!agl->use_shaders) glUseProgram(0); // is needed though shouldnt be
	}

#ifdef CURIOUS
	// Check settings for our opengl implementation
	GLfloat sizes[2];   // supported line width range
	GLfloat step;       // supported line width increments
	// Get supported line width range and step size
	glGetFloatv(GL_LINE_WIDTH_RANGE, sizes);
	glGetFloatv(GL_LINE_WIDTH_GRANULARITY, &step);
	dbg (1, "GL_LINE_WIDTH_RANGE: %.2f - %.2f  GL_LINE_WIDTH_GRANULARITY: %.2f", sizes[0], sizes[1], step);
#endif

	c->is_new = false;
}


static void
arr_gl_canvas_on_unrealise (GtkWidget* widget, gpointer _arrange)
{
	PF;
	Arrange* arrange = _arrange;

#if 0 // track controls are no longer removed on unrealise
	AGlActor* tc = CGL->actors[ACTOR_TYPE_TC];
	while(tc->children)
		agl_actor__remove_child(tc, tc->children->data);
#endif

	AGlActor* tracks = CGL->actors[ACTOR_TYPE_TRACKS];
	while(tracks->children)
		agl_actor__remove_child(tracks, tracks->children->data);

#if 0 // it should be unneccesary to free the wf_canvas
	GlCanvas* c = arrange->canvas->gl;
	if(c->wfc) wf_canvas_free0(c->wfc);
#endif
}


static void
__arr_gl_allocate_part (gpointer key, gpointer value, gpointer _arrange)
{
	agl_actor__set_size((AGlActor*)value);
}


static void
arr_gl_on_viewport_change (Arrange* arrange)
{
}


static void
arr_gl_on_allocate (GtkWidget* widget, GtkAllocation* allocation, Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

	arr_gl_on_viewport_change(arrange);

	// note that the root actor does not scroll
	c->actor->scrollable.x2 = c->actor->region.x2 = allocation->width;
	c->actor->scrollable.y2 = c->actor->region.y2 = allocation->height;

	agl_actor__set_size(c->actor);
}


/*
 *  This callback fires on zoom step change. See callback below for the animated version
 */
static void
arr_gl_on_zoom (Observable* zoom, AMVal val, gpointer _arrange)
{
	Arrange* arrange = _arrange;
	PtObservable* pt = (PtObservable*)zoom;

	if(pt->change & CHANGE_X){
		// spp does not change?
		float onepx_in_samples = ((float)ayyi_beats2samples(1)) / PX_PER_BEAT;
		CGL->wfc->samples_per_pixel = onepx_in_samples;

		wf_context_set_zoom(CGL->wfc, ((AyyiPanel*)arrange)->zoom->value.pt.x);
	}
}


static void
arr_gl_on_zoom_frame (Observable* observable, AMVal val, gpointer _arrange)
{
	Arrange* arrange = _arrange;

#ifdef USE_FBO
	if(CGL->actors[ACTOR_TYPE_TRACKS]){
		CGL->actors[ACTOR_TYPE_TRACKS]->invalidate(CGL->actors[ACTOR_TYPE_TRACKS]);
		agl_actor__set_size(CGL->actors[ACTOR_TYPE_TRACKS]);
	}
#endif
}


static bool
arr_gl_canvas_expose (GtkWidget* widget, GdkEventExpose* event, gpointer _arrange)
{
	g_return_val_if_fail(_arrange, false);
	Arrange* arrange = _arrange;
	GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

#if 0
	frame_count++;
#endif

	if(gdk_gl_drawable_make_current(ROOT()->gl.gdk.drawable, ROOT()->gl.gdk.context)){
		gl_warn("pre");

		arr_gl_setup_view(arrange);

		GdkColor bg;
		get_style_bg_color(&bg, GTK_STATE_NORMAL);
#ifdef BG_TEXTURE
		glClearColor(0.5 * ((float)bg.red)/0x10000, 0.5 * ((float)bg.green)/0x10000, 0.5 * ((float)bg.blue)/0x10000, 1.0);
#else
		glClearColor(((float)bg.red)/0x10000, ((float)bg.green)/0x10000, ((float)bg.blue)/0x10000, 1.0);
#endif
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//XX no cannot do this, as it removed the rotation set in setup_view
		//glLoadIdentity(); //something in the font rendering is messing with this

		agl_actor__paint(c->actor);

#if USE_SYSTEM_GTKGLEXT
		gdk_gl_drawable_swap_buffers(ROOT()->gl.gdk.drawable);
#else
		gdk_gl_window_swap_buffers(ROOT()->gl.gdk.drawable);
#endif
	}
	else pwarn ("gl_begin failed!");
 
	return true;
}


static void
arr_gl_canvas_redraw_fast (Arrange* arrange)
{
	// This excludes any changes to viewport or dimensions but is still
	// relatively slow due to the fbo's being cleared.
	// For fastest redraw, use gtk_widget_queue_draw()

#ifdef USE_FBO
	CGL->actors[ACTOR_TYPE_TRACKS]->invalidate(CGL->actors[ACTOR_TYPE_TRACKS]);
#endif
	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


void
arr_gl_canvas_redraw_part (Arrange* arrange, AMPart* part)
{
	// This assumes that the part has not moved track. If track has changed
	// you need to also redraw the old track (invalidate cache).

	GlCanvas* c = arrange->canvas->gl;
	g_return_if_fail(part);

	AGlActor* pa = g_hash_table_lookup(c->parts, part);
	g_return_if_fail(pa);

	agl_actor__set_size(pa);
}


void
arr_gl_canvas_redraw_track (Arrange* arrange, ArrTrk* at)
{
	g_return_if_fail(at);
	GlCanvas* c = arrange->canvas->gl;

	// TODO only really need to re-allocate parts on _this_ track
	g_hash_table_foreach(c->parts, __arr_gl_allocate_part, arrange);

#if 0
	TrackControlTrackActor* tcta = g_hash_table_lookup(TRACKS, at->track);
	if(tcta){
		tcta->parts.cache.valid = false;
		agl_actor__invalidate((AGlActor*)tcta);
	}
#endif
}


void
arr_gl_canvas_paint_local_part (Arrange* arrange, LocalPart* lpart)
{
	GlCanvas* c = (GlCanvas*)arrange->canvas->gl;
	AMPart* part = (AMPart*)lpart;
	AMTrack* track = part->track;
	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, track);

	AGliPt p = {
		.x = arr_gl_pos2px_(arrange, &part->start),
		.y = atr->y * vzoom(arrange) - c->actors[ACTOR_TYPE_MAIN]->scrollable.y1
	};

	glTranslatef(p.x, p.y, 0);
	gl_part_paint((AGlActor*)&(PartActor){
		.actor = {
			.root = (AGlScene*)c->actor,
			.region = {
				.x1 = p.x,
				.y1 = p.y,
				.x2 = p.x + arr_gl_pos2px_(arrange, &part->length),
				.y2 = p.y + GL_TRACK_HEIGHT(atr)
			}
		},
		.part = (AMPart*)lpart,
	});
	glTranslatef(-p.x, -p.y, 0);
}


#ifdef DEBUG
static AGlActor*
arr_gl_frame (GtkWidget* panel)
{
	bool arr_gl_draw_frame (AGlActor* actor)
	{
	#undef SHOW_ORIGIN
	#ifdef SHOW_ORIGIN
		agl_enable(0, /* !AGL_ENABLE_TEXTURE_2D */);
		glColor3f(1.0, 0.0, 0.0);

		// show origin
		glLineWidth(1);
		glBegin(GL_LINES);
		glVertex3f( 0, 0, 0 );    glVertex3f(   1,    0, 0 );
		glVertex3f( 0, 0, 0 );    glVertex3f(   0,   -1, 0 );

		// box 320x240
		glVertex3f( 320, -240, 0 ); glVertex3f(320,    0, 0 );
		glVertex3f( 0,   -240, 0 ); glVertex3f(320, -240, 0 );
		glEnd();
		agl_enable(AGL_ENABLE_TEXTURE_2D);
	#endif

	#undef SHOW_PLANES
	#ifdef SHOW_PLANES
		agl_enable(0 /* !AGL_ENABLE_TEXTURE_2D */);
		glColor3f(0.0, 1.0, 0.0);
		int w = 10000;

		// plane at z=0
		//glRectf(-w, -w, w, w);

		//2 more planes facing us, but in front and behind
		glColor3f(1.0, 0.5, 0.0);
		glBegin( GL_QUADS );
		glVertex3f(w, -w, 1.0 ); glVertex3f(w,  0, 1.0);
		glVertex3f(0, -w, 1.0 ); glVertex3f(w, -w, 1.0);
		glEnd();

		glColor3f(0.0, 0.5, 1.0);
		glBegin( GL_QUADS );
		glVertex3f(w, -w, -1.0 ); glVertex3f(w,  0, -1.0);
		glVertex3f(0, -w, -1.0 ); glVertex3f(w, -w, -1.0);
		glEnd();

		//pane at x=0: (havnt actually seen this yet)
		glColor3f(1.0, 1.0, 0.0);
		glBegin( GL_QUADS );
		glVertex3f( 0, w, -w ); glVertex3f(0, w,  0);
		glVertex3f( 0, 0, -w ); glVertex3f(0, w, -w);
		glEnd();

		//pane at y=0: (havnt actually seen this yet)
		glColor3f(0.0, 1.0, 1.0);
		glBegin(GL_QUADS);
		glVertex3f( w, 0, -w ); glVertex3f(w, 0,  0);
		glVertex3f( 0, 0, -w ); glVertex3f(w, 0, -w);
		glEnd();

		agl_enable(AGL_ENABLE_TEXTURE_2D);
	#endif
		return true;
	}

	return agl_actor__new(AGlActor,
		.name = g_strdup("Frame"),
		.paint = arr_gl_draw_frame,
		.region = {0, 0, 1, 1}
	);
}
#endif


#ifndef USE_FBO
static AGlActor*
arr_gl_parts (AyyiPanel* panel)
{
	bool arr_gl_draw_parts (AGlActor* actor)
	{
		// Each part is drawn as several rectangles, one per waveform texture.

		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = arrange->canvas->gl;

		void _each(gpointer key, gpointer value, gpointer arrange)
		{
			PartActor* pa = (PartActor*)value;
			arr_gl_draw_part((Arrange*)arrange, pa);
		}
		g_hash_table_foreach(c->parts, _each, arrange);

		return true;
	}

	return agl_actor__new(AGlActor,
		.name = g_strdup("GlParts"),
		.paint = arr_gl_draw_parts,
		.region = (AGlfRegion){0, ((Arrange*)panel)->ruler_height, 4096, 4096}
	);
}
#endif


static void
arr_gl_canvas_pick (Arrange* arrange, double x, double y, TrackDispNum* track_idx, AMPart** part)
{
	// x and y should be in post-zoom world-space (ie viewport offsets already applied).
	// ie x=0 should always correspond to 01:01:01:000 ?
	// currently y=0 corresponds to the top of the rulerbar, _not_ the top of the first track

	if (track_idx) *track_idx = -1;
	if (part) *part = NULL;
	if (x < 0) return pwarn("coords out of range: %.1f %.1f", x, y);

	CanvasOp* op = arrange->canvas->op;

	if (y - op->yscrolloffset < 0) {
		// rulerbar
	} else {
		if (part) {

			AyyiSongPos pos;
			ArrTrk* track = NULL;
			arrange->canvas->px_to_model(arrange, (Pti){x - CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1, y}, &pos, &track);

			*part = (AMPart*)am_partmanager__find_by_position(track->track, &pos);
			if(track_idx)
				*track_idx = track_list__get_display_num(arrange->priv->tracks, track->track);
		}else{
			TrackDispNum d = arr_px2trk(arrange, y - arrange->ruler_height); // TODO this probably doesnt work in iso mode.
			if(track_idx)
				*track_idx = d;
		}
	}
}


/*
 *  Get the fraction of the whole canvas that is currently visible in the window
 *
 *  This actually works for all canvases, theres no real need for one per canvas type.
 */
static void
arr_gl_get_view_fraction (Arrange* arrange, double* fx, double* fy)
{
	float tot_x_beats = am_object_val(&song->loc[AM_LOC_END]).sp.beat + 4;
	float visible_x_px = arrange->canvas->widget->allocation.width;
	float visible_x_beats = visible_x_px / (PX_PER_BEAT * hzoom(arrange));

	*fx = visible_x_beats / tot_x_beats;

	if(fy){
		ArrTrk* atr = arrange->priv->tracks->track[am_track_list_count(song->tracks) -1];

		float visible_y_px = arrange->canvas->widget->allocation.height;
		float total_y_px = (atr->y + atr->height) * vzoom(arrange) + arrange->ruler_height;

		*fy = visible_y_px / total_y_px;
	}
}


/*
 * Transforms a raw widget pixel coordinate to the song position and track.
 *
 * It is intended to be a replacement for get_scroll_offsets which is not adequate for the transformation for some canvas types
 *
 */
static void
arr_gl_px_to_model (Arrange* arrange, Pti px, AyyiSongPos* pos, ArrTrk** track)
{
	px.x += CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 - arrange->tc_width.val.i;
	px.y += CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1 - arrange->ruler_height;

	arr_px2spos(arrange, pos, px.x);

	if(track){
		TrackDispNum d = arr_px2trk(arrange, px.y);
		dbg(1, "d=%i", d);
		if(d > -1 && d < AM_MAX_TRKX) *track = arrange->priv->tracks->display[d];
	}
}


static void
arr_gl_get_scroll_offsets (Arrange* arrange, int* cx, int* cy)
{
		if(CGL->actors[ACTOR_TYPE_MAIN]){
	*cx = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
	*cy = -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
		}
}


static uint32_t
arr_gl_get_viewport_left (Arrange* arrange)
{
	GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

	return -c->actors[ACTOR_TYPE_MAIN]->scrollable.x1 - (c->isometric ? ISOMETRIC_X0 : 0);
}


/*
 *  Get pixel coords (x1,y1,x2,y2) of the current viewport relative to top left of track1.
 */
static WfViewPort
arr_gl_get_viewport (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;
	AGlActor* scrollable = c->actors[ACTOR_TYPE_MAIN];

	return (WfViewPort){
		.left   = -scrollable->scrollable.x1 - (c->isometric ? ISOMETRIC_X0 : 0),
		.right  = -scrollable->scrollable.x1 + scrollable->region.x2,
		.top    = -scrollable->scrollable.y1,
		.bottom = -scrollable->scrollable.y1 + c->image->allocation.height - arrange->ruler_height,
	};
}


/*
 *  Scroll the arrange canvas to the given absolute canvas position.
 *  -use -1 to keep existing position.
 */
static void
arr_gl_scroll_to (Arrange* arrange, int x, int y)
{
	GlCanvas* c = arrange->canvas->gl;
	if(!c->actors[ACTOR_TYPE_MAIN]) return;
	dbg(1, "x=%i y=%i", x, y);

	AMChangeType change = 0;

	if(x > -1 && x != -c->actors[ACTOR_TYPE_MAIN]->scrollable.x1){
		arrange->canvas->op->mouse.x = -1;

		GtkAdjustment* adj = GTK_ADJUSTMENT(arrange->canvas->h_adj);
		// not using adj->page_size because viewport is different width than the scrollbar
		int page_size = agl_actor__width(c->actors[ACTOR_TYPE_MAIN]);
		double x_ = CLAMP(
			(double)x,
			adj->lower,
			adj->upper > page_size ? adj->upper - page_size : adj->upper
		);
		agl_actor__scroll_to (c->actors[ACTOR_TYPE_MAIN], (AGliPt){x_, -1});
		gtk_adjustment_set_value(adj, x_);
		change |= AM_CHANGE_POS_X;
	}

	if(y > -1 && y != -c->actors[ACTOR_TYPE_MAIN]->scrollable.y1){
		arrange->canvas->op->mouse.y = -1;

		GtkAdjustment* adj = GTK_ADJUSTMENT(arrange->canvas->v_adj);
		double y_ = MIN((double)y, MAX(0, adj->upper - adj->page_size));
		c->actors[ACTOR_TYPE_MAIN]->scrollable.y1 = -y_;
		c->actors[ACTOR_TYPE_TC]->scrollable.y1 = -y_;
		gtk_adjustment_set_value(adj, y_);
		change |= AM_CHANGE_VIEWPORT_Y;
	}

	arr_gl_on_view_change(arrange, change);
}


AyyiSongPos*
arr_gl_px2songpos (Arrange* arrange, AyyiSongPos* pos, double px)
{
	if(px >= 0.0){
		float beats = px / (((float)PX_PER_BEAT) * CGL->zoom->value.pt.x);
		uint64_t mu = beats * AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT;
		ayyi_mu2pos(mu, pos);
	}else{
		*pos = (AyyiSongPos){0,};
	}

	return pos;
}


/*
 *  Convert a song position to a post-zoom world position.
 *  (differs from arr_pos2px in that it takes into account the transient zoom)
 */
static double
arr_gl_pos2px (Arrange* arrange, GPos* pos)
{
	g_return_val_if_fail(arrange, -1.0);
	am_pos_is_valid(pos);

	float ticks_per_beat_f = (float)AYYI_TICKS_PER_BEAT;

	double beats_float = pos->beat + (double)pos->sub / 4.0 + ((double)pos->tick / ticks_per_beat_f);

	return beats_float * PX_PER_BEAT * wf_context_get_zoom(CGL->wfc);
}


/*
 *  Convert a song position to a post-zoom world position.
 *  (differs from arr_pos2px in that it takes into account the transient zoom)
 */
double
arr_gl_pos2px_ (Arrange* arrange, AyyiSongPos* pos)
{
	g_return_val_if_fail(arrange, -1.0);
	ayyi_pos_is_valid(pos);

	double beats_float = pos->beat + (double)pos->sub / ((double)AYYI_SUBS_PER_BEAT) + ((double)pos->mu) / ((double)AYYI_MU_PER_SUB * AYYI_SUBS_PER_BEAT);

	return beats_float * PX_PER_BEAT * wf_context_get_zoom(CGL->wfc);
}


static float
arr_gl_samples2px (Arrange* arrange, uint32_t samples)
{
	float secs = (float)samples / (float)song->sample_rate;
	float beats = secs * BEATS_PER_SECOND;

	return beats * PX_PER_BEAT * wf_context_get_zoom(CGL->wfc);
}


static void
arr_gl_on_hscroll (GtkWidget* widget, Arrange* arrange)
{
	g_return_if_fail(arrange);

	CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 = -gtk_adjustment_get_value(GTK_ADJUSTMENT(widget));
	dbg (2, "x=%.2f", -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1);

	arr_on_view_changed(arrange, AM_CHANGE_POS_X);
}


static void
arr_gl_on_vscroll (GtkWidget* widget, Arrange* arrange)
{
	g_return_if_fail(arrange);
	GlCanvas* c = arrange->canvas->gl;

	GtkAdjustment* scroll_v_adj = GTK_ADJUSTMENT(widget);
	c->actors[ACTOR_TYPE_MAIN]->scrollable.y1 = -floor(gtk_adjustment_get_value(scroll_v_adj));

	arr_gl_on_view_change(arrange, AM_CHANGE_VIEWPORT_Y);
}


static void
arr_gl_on_song_load (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

#if 0
	CGL->actors[ACTOR_TYPE_TC] = agl_actor__replace_child(c->actor, c->actors[ACTOR_TYPE_TC], track_control_actor_new((GtkWidget*)arrange));
#else

	// temporarily using a modified version of agl_actor__replace_child so that the old actor gets freed before the new actor is created (otherwise cannot disconnect signals properly)

	AGlActor* _agl_actor__replace_child (AGlActor* actor, AGlActor* child, AGlActor* new_child)
	{
		GList* l = g_list_find(actor->children, child);
		if(l){
			agl_actor__add_child(actor, new_child);

			// update the children list such that the original order is preserved
			actor->children = g_list_remove(actor->children, new_child);
			GList* j = g_list_find(actor->children, child);
			j->data = new_child;

#if 0
			agl_actor__free(child);
#endif

			return new_child;
		}
		return NULL;
	}

	c->actor->disabled = false;

	if(c->actors[ACTOR_TYPE_TC]){
		GList* placeholder = g_list_find(c->actor->children, c->actors[ACTOR_TYPE_TC]);
		agl_actor__free(c->actors[ACTOR_TYPE_TC]);
		AGlActor* ph = g_new0(AGlActor, 1);
		placeholder->data = ph;
		CGL->actors[ACTOR_TYPE_TC] = _agl_actor__replace_child(c->actor, ph, track_control_actor_new((GtkWidget*)arrange));
		g_free(ph);
	}
#endif

	// Although zoom is not a song property, zoom changes are not currently
	// done when the model is not loaded as there is a risk that state may be undefined.
	arr_gl_on_view_change(arrange, AM_CHANGE_ZOOM_H | AM_CHANGE_ZOOM_V);
}


static void
arr_gl_on_view_change (Arrange* arrange, AMChangeType change_type)
{
#ifdef DEBUG
	dbg(1, "%s%s%s", ayyi_green, am_print_change(change_type), white);
#endif

	GlCanvas* c = arrange->canvas->gl;

	if(!c->actor || !c->actors[ACTOR_TYPE_MAIN]) return;

	if(change_type & (AM_CHANGE_ZOOM_H | AM_CHANGE_ZOOM_V)){
		agl_actor__set_size(c->actors[ACTOR_TYPE_MAIN]);
	}

	if(change_type & AM_CHANGE_WIDTH || change_type & AM_CHANGE_HEIGHT){
		agl_actor__set_size(c->actor);
	}

#ifdef USE_FBO
	void maybe_invalidate_track_caches (Arrange* arrange)
	{
		void __track_invalidate (gpointer key, gpointer value, gpointer _arrange)
		{
			Arrange* arrange = _arrange;
			TrackControlTrackActor* tcta = value;
			g_return_if_fail(tcta);
			ArrTrk* at = tcta->track;
			GlCanvas* c = arrange->canvas->gl;

			TrackActor* ta = arr_gl_tracks_find_track(c->actors[ACTOR_TYPE_TRACKS], at);
			AGlActor* actor = (AGlActor*)ta;

			if(((AGlActor*)ta)->fbo){
				AyyiSongPos track_end_pos, track_start_pos;
				am_track__get_start_end(at->track, &track_start_pos, &track_end_pos);

				iRange track = {
					.start = arr_gl_pos2px_(arrange, &track_start_pos),
					.end   = arr_gl_pos2px_(arrange, &track_end_pos)
				};
				iRange cache = {
					.start = actor->cache.position.x,
					.end   = actor->cache.position.x + ((AGlActor*)ta)->fbo->width
				};
				iRange viewport = {
					.start = -c->actors[ACTOR_TYPE_MAIN]->scrollable.x1,
					.end   = -c->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + agl_actor__width(c->actors[ACTOR_TYPE_MAIN])
				};

				bool fail1 = (MAX(viewport.start, track.start) < cache.start);
				bool fail2 = (MIN(viewport.end,   track.end  ) > cache.end);
				if(fail1) dbg(1, "cache miss (start). invalidating... (%s)", at->track->name);
				if(fail2) dbg(1, "cache miss (end). %i > %i invalidating... (%s)", viewport.end, cache.end, at->track->name);
				if(fail1 || fail2) ((AGlActor*)ta)->cache.valid = false;
			}
		}
		g_hash_table_foreach(TCTRACKS, __track_invalidate, arrange);
	}

	if(change_type & AM_CHANGE_POS_X){
		agl_actor__set_size(c->actors[ACTOR_TYPE_MAIN]);
		arr_gl_on_viewport_change(arrange);
		maybe_invalidate_track_caches(arrange);
	}
#endif

	if(change_type & AM_CHANGE_VIEWPORT_Y){
		// TODO remove dependency on track_list_box
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		GtkAdjustment* adj = gtk_viewport_get_vadjustment(GTK_VIEWPORT(arrange->track_list_box));
		adj->value = c->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
		gtk_adjustment_value_changed(adj);
#endif
	}

	if (change_type & AM_CHANGE_BACKGROUND_COLOUR) {
		dbg(1, "AM_CHANGE_BACKGROUND_COLOUR unimplemented");
	}

	for (int i=0;i<ACTOR_TYPE_MAX;i++) {
		if (c->actors[i] && on_change[i]) on_change[i](c->actors[i], change_type);
	}

	if(change_type & AM_CHANGE_BACKGROUND_COLOUR){
		c->actors[ACTOR_TYPE_BACKGROUND]->colour = arrange->bg_colour;
	}

	if(change_type & AM_CHANGE_SHOW_PART_CONTENTS){
		if(((AyyiPanel*)arrange)->model_is_loaded){
			if(SHOW_PARTCONTENTS){
				g_hash_table_foreach(c->parts, arr_gl_create_part_wfactor, c);
			}else{
				GList* l = CGL->actors[ACTOR_TYPE_TRACKS]->children;
				for(;l;l=l->next){
					TrackActor* ta = l->data;
					GList* p = ((AGlActor*)ta)->children;
					for(;p;p=p->next){
						PartActor* pa = p->data;
						if(pa->wf_actor){
							agl_actor__remove_child((AGlActor*)pa, (AGlActor*)pa->wf_actor);
							pa->wf_actor = NULL;
						}
					}
				}
			}
			agl_actor__invalidate(CGL->actors[ACTOR_TYPE_TRACKS]);
		}
	}

	if(change_type & AM_CHANGE_SHOW_SONG_OVERVIEW){
		agl_actor__set_size(CGL->actors[ACTOR_TYPE_OVERVIEW]);

		if(!SHOW_OVERVIEW){
			CGL->actors[ACTOR_TYPE_HSCROLLBAR]->region = (AGlfRegion){0, 0, 1, 1};
			agl_actor__set_size(CGL->actors[ACTOR_TYPE_HSCROLLBAR]);
		}else{
			CGL->actors[ACTOR_TYPE_HSCROLLBAR]->region = (AGlfRegion){0,};
		}
	}

	if(change_type & AM_CHANGE_PEAK_GAIN){
		if(((AyyiPanel*)arrange)->model_is_loaded){
			if(SHOW_PARTCONTENTS){
				void arr_gl_each_vzoom(gpointer key, gpointer value, gpointer _arrange)
				{
					PartActor* pa = (PartActor*)value;
					wf_actor_set_vzoom(pa->wf_actor, ((Arrange*)_arrange)->peak_gain);
				}
				g_hash_table_foreach(c->parts, arr_gl_each_vzoom, arrange);
			}
			agl_actor__invalidate(CGL->actors[ACTOR_TYPE_TRACKS]);
		}
	}

	gtk_widget_queue_draw(CGL->image);
}


static void
arr_gl_on_locators_change (Arrange* arrange)
{
	agl_actor__invalidate(CGL->actors[ACTOR_TYPE_RULER]);
	agl_actor__invalidate(CGL->actors[ACTOR_TYPE_OVERVIEW]);
}


static void
arr_gl_on_new_track (Arrange* arrange, ArrTrk* at)
{
	void _meter (Observable* o, AMVal level, gpointer _at)
	{
		ArrTrk* at = _at;
		GlCanvas* c = at->arrange->canvas->gl;
		gtk_widget_queue_draw(c->image);
	}

	AGlActor* track = gl_track(at);
	agl_actor__add_child(CGL->actors[ACTOR_TYPE_TRACKS], track);

	// connect meters. only needed when transport is stopped because otherwise screen will be updated anyway.
	am_track__enable_metering(at->track);
	if (at->track->meterval) observable_subscribe(at->track->meterval, _meter, at);
}


static void
arr_gl_on_track_delete (Arrange* arrange, ArrTrk* at)
{
	PF;

	TrackActor* ta = arr_gl_tracks_find_track(CGL->actors[ACTOR_TYPE_TRACKS], at);
	if(ta)
		agl_actor__remove_child(CGL->actors[ACTOR_TYPE_TRACKS], (AGlActor*)ta);
}


static void
_arr_gl_on_track_delete (GObject* _song, AMTrack* tr, Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

	if (tr->meterval)
		observable_unsubscribe(tr->meterval, NULL, track_list__lookup_track(arrange->priv->tracks, tr));

	if (!g_hash_table_remove(TCTRACKS, tr)) pwarn("unable to remove track from hashtable");

	// TODO redraw fast is too heavy - instead, just clear the FBO of the deleted track and queue a redraw
	arr_gl_canvas_redraw_fast(arrange);
}


static bool
arr_gl_part_new (Arrange* arrange, AMPart* part)
{
	PF;
	GlCanvas* c = arrange->canvas->gl;

#if 0
	int num_blocks = pi->waveform->num_peaks / WF_PEAK_TEXTURE_SIZE;
	if(pi->waveform->num_peaks % WF_PEAK_TEXTURE_SIZE) num_blocks++;
	if(!num_blocks) pwarn("num_blocks=0");
#endif

#ifdef TEST_TEXTURE_PERF
	GLboolean resident[num_blocks];
	if(glAreTexturesResident(num_blocks, blocks->texture_name, resident)) dbg (0, "all textures are resident.");
	else {
		dbg (0, "not all textures are resident.");
		int i; for(i=0;i<num_blocks;i++) printf("%i ", resident[i]);
		if(i) printf("\n");
	}
#endif

	g_return_val_if_fail(!g_hash_table_lookup(c->parts, part), false);

	AGlActor* pa = gl_part_new(part);
	g_hash_table_insert(c->parts, part, pa);

	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, part->track);
	g_return_val_if_fail(atr, false);
	AGlActor* ta = (AGlActor*)arr_gl_tracks_find_track(CGL->actors[ACTOR_TYPE_TRACKS], atr);
	g_return_val_if_fail(ta, false);
	agl_actor__add_child(ta, pa);

#if 0
	if(PART_IS_AUDIO(part)){
		dbg(2, "n_channels=%i visible=%i", waveform_get_n_channels(gp->wf_actor->waveform), arr_part_is_in_viewport(arrange, part));
	}
	if(arr_part_is_in_viewport(arrange, part)) gtk_widget_queue_draw(c->image);
#endif

#ifdef DEBUG
	gboolean _verify (gpointer ta)
	{
		#if 0 // ta can become invalid while waiting for idle
		g_assert(gl_track_verify((TrackActor*)ta));
		#endif
		return G_SOURCE_REMOVE;
	}
	g_idle_add(_verify, ta);
#endif

	return true;
}


static void
arr_gl_on_part_delete (Arrange* arrange, AMPart* part)
{
	// delete all the textures used by this part.
	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, (Filter)am_part__has_pool_item, part->pool_item);
	AMPart* other = (AMPart*)filter_iterator_next(i);
	filter_iterator_unref(i);
	if(!other && part->pool_item){
#if 0
		// no, do not do this - we didnt add a reference, so dont remove one.
		g_object_unref(part->pool_item->waveform); 
#endif
	}

#if 1
	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, part->track);
	g_return_if_fail(atr);
	AGlActor* ta = (AGlActor*)arr_gl_tracks_find_track(CGL->actors[ACTOR_TYPE_TRACKS], atr);
	g_return_if_fail(ta);
	PartActor* pa = gl_track_find_part(ta, part);
#else
	PartActor* pa = g_hash_table_lookup(CGL->parts, part);
#endif

	gl_part_remove(pa);
}


static void
arr_gl_set_part_selection (Arrange* arrange)
{
	arr_gl_canvas_redraw_fast(arrange); // need to undraw previous selection TODO how do we know what was the previous selection?

	if(!arrange->part_selection){
		return;
	}

	typedef struct
	{
		Arrange* arrange;
		int      i;
	} C;

	gboolean selection_frame (gpointer _c)
	{
		C* c = _c;

		for(GList* l=c->arrange->part_selection;l;l=l->next){
			arr_gl_canvas_redraw_part(c->arrange, (AMPart*)l->data);
		}

		part_shader.uniform.frame = ++ c->i;

		if(c->i > 5){
			g_free(c);
			return G_SOURCE_REMOVE;
		}
		return G_SOURCE_CONTINUE;
	}

	animator_add(selection_frame, AYYI_NEW(C,
		.arrange = arrange // TODO when arrange is destroyed, this animation must be stopped.
	));
}


static void
arr_gl_on_part_change (Arrange* arrange, AMPart* part)
{
	PF;
	GlCanvas* c = arrange->canvas->gl;

	PartActor* pa = g_hash_table_lookup(c->parts, part);
	if (pa) {
		if (pa->wf_actor)
			wf_actor_set_region(pa->wf_actor, &(WfSampleRegion){part->region_start, part->ayyi->length});
		agl_actor__set_size((AGlActor*)pa);
	}

#ifdef USE_FBO
	c->actors[ACTOR_TYPE_TRACKS]->invalidate(CGL->actors[ACTOR_TYPE_TRACKS]);
#endif
}


/*
 *  Return coords relative to song 0,0 - not current screen coords.
 */
static void
arr_gl_get_part_rect (Arrange* arrange, AMPart* part, DRect* b)
{
	GlCanvas* c = arrange->canvas->gl;
	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, part->track);

	b->x1 = arr_gl_pos2px_(arrange, &part->start);
	b->y1 = atr->y * vzoom(arrange) * (1-c->iso_factor) - c->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
	b->x2 = b->x1 + arr_gl_pos2px_(arrange, &part->length);
	b->y2 = (atr->y + atr->height) * vzoom(arrange) * (1-c->iso_factor) - c->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
}


#if 0
inline int
next_p2 (int a)
{
	// Get The First Power Of 2 >= The Int That We Pass It.

	int rval = 1;
	while(rval < a) rval<<=1;
	return rval;
}


typedef struct _gl_glyph
{
	int width;
	int height;
	unsigned char* buffer;

} glyph;
#endif


static void
arr_gl_on_iso_button_press (GtkWidget* widget, ButtonContext* context)
{
	PF;
	Arrange* arrange = (Arrange*)context->panel;
	GlCanvas* c = arrange->canvas->gl;

	if(c->isometric) arr_gl_disable_isometric(arrange); else arr_gl_enable_isometric(arrange);
	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


static void
arr_gl_on_iso_key_press (GtkAccelGroup* accel_group, gpointer user_data)
{
	PF;
	AyyiPanel* window = app->active_panel;
	char type[AYYI_SHORT_NAME_MAX]; ayyi_panel_print_type(window, type); dbg(0, "active: windowtype=%s", type);
	if(!AYYI_IS_ARRANGE(window)) return;
	Arrange* arrange = (Arrange*)window;
	GlCanvas* c = arrange->canvas->gl;

	if(c->isometric) arr_gl_disable_isometric(arrange); else arr_gl_enable_isometric(arrange);
	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


static void
arr_gl_enable_isometric (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

	c->isometric = TRUE;
	agl_actor__add_child(c->actor, c->actors[ACTOR_TYPE_ISO_FRAME]);
	agl_actor__add_child(c->actor, c->actors[ACTOR_TYPE_ISO_TC]);
	rotation_start(arrange);
}


static void
arr_gl_disable_isometric (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

	c->isometric = FALSE;
	c->actor->children = g_list_remove(c->actor->children, c->actors[ACTOR_TYPE_ISO_FRAME]);
	c->actor->children = g_list_remove(c->actor->children, c->actors[ACTOR_TYPE_ISO_TC]);
	rotation_start(arrange);
}


static void
gl_set_palette_colour (int c_idx)
{
	float r = (song->palette[c_idx] & 0xff000000) >> 24;
	float g = (song->palette[c_idx] & 0x00ff0000) >> 16;
	float b = (song->palette[c_idx] & 0x0000ff00) >>  8;

	glColor3f(r / 0xff, g / 0xff, b / 0xff);
}


static void
arr_gl_do_follow (Arrange* arrange)
{
	GtkAdjustment* adj = arrange->canvas->h_adj;

	WfViewPort viewport = arr_gl_get_viewport(arrange);
	double spp = arr_get_spp_px(arrange);

	if (spp < viewport.left) {
		dbg(2, "paging left...");
		gtk_adjustment_set_value(adj, gtk_adjustment_get_value(adj) - gtk_adjustment_get_page_size(adj));
	} else if (spp > viewport.right) {
		dbg(2, "paging right...");
		gtk_adjustment_set_value(adj, gtk_adjustment_get_value(adj) + gtk_adjustment_get_page_size(adj));
	}
}


static void
arr_gl_scroll_to_pos (Arrange* arrange, GPos* pos, TrackDispNum t)
{
	arr_gl_scroll_to(arrange, arr_pos2px(arrange, pos), t > -1 ? arrange->priv->tracks->track[t]->y * vzoom(arrange) : -1);
}


static void
arr_gl_scroll_left (Arrange* arrange, int pix)
{
	arr_gl_scroll_to(arrange, MAX(0, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 - pix), -1);
}


static void
arr_gl_scroll_right (Arrange* arrange, int pix)
{
	if(pix)
		arr_gl_scroll_to(arrange, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + pix, -1);
}


static void
arr_gl_scroll_up (Arrange* arrange, int pix)
{
	// Scroll up by the number of pixels given.
	// -the scrollbar should move upwards, but objects inside the scrollwin move downwards.

	arr_gl_scroll_to(arrange, -1, MAX(0, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1 - pix));
}


static void
arr_gl_scroll_down (Arrange* arrange, int pix)
{
	arr_gl_scroll_to(arrange, -1, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1 + pix);
}


static void
menu_arr_set_gl_canvas (Arrange* arrange)
{
	CanvasType arr_gl_canvas_get_type();
	arr_set_canvas(arrange, arr_gl_canvas_get_type());
}


#ifdef SHOW_LOG
static Actor*
log_actor (Arrange* arrange)
{
	static AGlFBO* fbo0 = NULL;

	#ifdef USE_FBO
	static void create_fbo()
	{
		AGlFBO* fbo = agl_fbo_new(256, 256, 0, 0);
		fbo0 = fbo;
		return;
	}
	#endif

	void log_actor__init(Actor* actor, Arrange* arrange)
	{
	#ifdef USE_FBO
		if(!fbo0) create_fbo();
	#endif
	}

	void log_actor__paint (Actor* actor, Arrange* arrange)
	{
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

		GdkColor bg;
		get_style_bg_color(&bg, GTK_STATE_NORMAL);
		uint32_t colour = colour_get_offset_rgba(color_gdk_to_rgba(&bg), 10);

		GtkTextBuffer* log = ayyi.log.txtbuf;
		int n_lines = gtk_text_buffer_get_line_count(log);

	#if 0 // draw all text in one go -- is this more efficient than doing separate operations per line?

		int n_visible_lines = 22;
		GtkTextIter start, end;
		gtk_text_buffer_get_iter_at_line(log, &start, MAX(0, n_lines - n_visible_lines));
		gtk_text_buffer_get_end_iter(log, &end);
		gchar* text = gtk_text_buffer_get_text(log, &start, &end, FALSE);

		agl_draw_to_fbo(fbo0) {
			glClearColor(1.0, 1.0, 1.0, 0.0); //background colour must be same as foreground for correct antialiasing
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			agl_print(10, 10, 0, 0xffffffff, text);
		} agl_end_draw_to_fbo;
		g_free(text);

		#ifdef USE_FBO
		// put the log fbo onto the screen
		{
			glColor4f(1.0, 1.0, 1.0, 0.10);
			agl_enable(AGL_ENABLE_TEXTURE_2D | AGL_ENABLE_BLEND);
			glBindTexture(GL_TEXTURE_2D, fbo0->texture);
			glBegin(GL_QUADS);
			double log_scale = 2;
			double top = c->image->allocation.height - fbo0->height * log_scale;
			double bot = c->image->allocation.height;
			double x1 = 0;
			double x2 = fbo0->width * log_scale;
			glTexCoord2d(1.0, 1.0); glVertex2d(x2, top);
			glTexCoord2d(0.0, 1.0); glVertex2d(x1, top);
			glTexCoord2d(0.0, 0.0); glVertex2d(x1, bot);
			glTexCoord2d(1.0, 0.0); glVertex2d(x2, bot);
			glEnd();
		}
		#endif
	#else

		void fbo_print (int x, int y, double scale, uint32_t colour, int alpha, const char* text)
		{
			#ifdef USE_FBO
			agl_draw_to_fbo(fbo0) {
				WfColourFloat fc; colour_rgba_to_float(&fc, colour);

				glClearColor(fc.r, fc.g, fc.b, 0.0); //background colour must be same as foreground for correct antialiasing
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				agl_print(0, 0, 0.0, colour, text);

			} agl_end_draw_to_fbo;

			// put the log fbo onto the screen
			{
				glColor4f(1.0, 1.0, 1.0, alpha / 256.0);
				agl_enable(AGL_ENABLE_TEXTURE_2D | AGL_ENABLE_BLEND);
				glBindTexture(GL_TEXTURE_2D, fbo0->texture);
				glBegin(GL_QUADS);
				double top = y;
				double bot = y + fbo0->height * scale;
				double x1 = x;
				double x2 = x + fbo0->width * scale;
				glTexCoord2d(1.0, 1.0); glVertex2d(x2, top);
				glTexCoord2d(0.0, 1.0); glVertex2d(x1, top);
				glTexCoord2d(0.0, 0.0); glVertex2d(x1, bot);
				glTexCoord2d(1.0, 0.0); glVertex2d(x2, bot);
				glEnd();
			}
			#endif
		}

		int alpha = 0x7f;
		double scale = 1.5;
		int line_height = 14; //TODO
		for (int l=0;l<MIN(20, n_lines - 1);l++) {
			GtkTextIter l1, l2;
			gtk_text_buffer_get_iter_at_line(log, &l1, n_lines -l -2);
			gtk_text_buffer_get_iter_at_line(log, &l2, n_lines -l -1);
			gchar* line = gtk_text_buffer_get_text(log, &l1, &l2, FALSE);
			int y = c->image->allocation.height - 70 - l * line_height * scale;
			if (y < 0) break;
			fbo_print(arrange->tc_width.val.i + 10, y, scale, colour, alpha = alpha - 7, line);
			g_free(line);
		}
	#endif // draw all lines in one go
	}

	return AGL_NEW(Actor,
		.name = g_strdup("Log")
		.init = log_actor__init,
		.paint = log_actor__paint,
	);
}
#endif


static void
arr_gl_set_spp (Arrange* arrange, uint32_t samples)
{
	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


static void
arr_gl_set_tc_width (Arrange* arrange, int width)
{
	GlCanvas* c = arrange->canvas->gl;

	arrange->tc_width.val.i = width;
	c->actors[ACTOR_TYPE_TC]->region.x2 = width;
	c->actors[ACTOR_TYPE_MAIN]->region.x1 = width;
	agl_actor__set_size(c->actor);

	gtk_widget_queue_draw(CGL->image);
}


double
arr_gl_get_spp_px (Arrange* arrange)
{
	GPos pos;
	am_transport_get_pos(&pos);
	return arr_gl_pos2px(arrange, &pos);
}


bool
arr_gl_pos_is_in_viewport (Arrange* arrange, AyyiSongPos* pos)
{
	AGlActor* actor = CGL->actors[ACTOR_TYPE_MAIN];
	float pos_px = arr_gl_pos2px_(arrange, pos);

	return
		pos_px > -actor->region.x1 - (float)actor->scrollable.x1 &&
		pos_px - 0. < actor->region.x1 + agl_actor__width(actor) - (float)actor->scrollable.x1;
}


static FeatureType
arr_gl_pick_feature (CanvasOp* op)
{
	// Note that at present this is not canvas specific and is essentially the same as the implementations for the other canvas types.

	Arrange* arrange = op->arrange;

	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);
	ArrTrackMidi* atm = (ArrTrackMidi*)at;

	int part_height = at->height * vzoom(arrange);
	int vchart_height = atm->velocity_chart_height;
	if (op->diff.y < part_height - vchart_height) {
		dbg(2, "note area... diff=%.2f", op->diff.y);
		bool at_start, at_end;
		if (arr_part_pick_note(arrange, op->part, op->diff, &at_start, &at_end)) {
			dbg(2, "over note! at_start=%i at_end=%i", at_start, at_end);
			if (at_start) {
				return FEATURE_NOTE_LEFT;
			} else if(at_end) {
				return FEATURE_NOTE_RIGHT;
			} else {
				return FEATURE_NOTE;
			}
		}
		else {
			dbg(2, "not note. diff=%.2fx%.2f px1=%.2f", op->diff.x, op->diff.y, op->p1.x);
			return FEATURE_NOTE_AREA;
		}
	} else if(op->diff.y < part_height - vchart_height + 2) {
		dbg(2, "FEATURE_VEL_DIVIDER diff=%.2f", op->diff.y);
		return FEATURE_VEL_DIVIDER;
	} else {
		dbg(2, "velocity zone...");
																			#if 0
		gboolean at_top = FALSE;
		Ptd p = {op->diff.x, op->diff.y - (part_height - vchart_height)};
		if(gnome_canvas_part_pick_velocity_bar(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item), p, &at_top)){
			if(at_top){
				return FEATURE_VEL_BAR_TOP;
			}
		}
																			#endif
		return FEATURE_VEL_AREA;
	}

	return FEATURE_NONE;
}


static void
arr_gl_part_editing_start (Arrange* arrange, AMPart* part)
{
}


static void
arr_gl_part_editing_stop (Arrange* arrange, AMPart* part)
{
}


/* temporary */
#ifdef DEBUG
int
arr_gl_parts_count(Arrange* arrange)
{
	return g_hash_table_size(CGL->parts);
}
#endif


#ifdef NOT_USED
static void
fbo_2_png (AGlFBO* fbo)
{
	int width = agl_power_of_two(fbo->width);
	int height = agl_power_of_two(fbo->height);

	unsigned char buffer[width * height * 4];
	glBindTexture(GL_TEXTURE_2D, fbo->texture);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

	GdkPixbuf* pixbuf = gdk_pixbuf_new_from_data(buffer, GDK_COLORSPACE_RGB, HAS_ALPHA_TRUE, 8, width, height, width*4, NULL, NULL);
	gdk_pixbuf_save(pixbuf, "test.png", "png", NULL, NULL);

	g_object_unref(pixbuf);
}
#endif


