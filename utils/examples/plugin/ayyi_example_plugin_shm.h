/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define CONTAINER_SIZE 128
#define BLOCK_SIZE 128

/*
 *  Struct describing the layout of the data segment created by the
 *  example plugin
 */
typedef struct
{
	// header

	char        service_name[16];
	char        service_type[16];
	int         version;
	void*       owner_shm;         // clients need this so they can calculate address offsets.
	int         num_pages;
	void*       next[1];           // top of allocation.

	// data

	char        hello[8];

} ExampleShm;

