/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __song_h__
#define __song_h__

#include <model/song.h>

#define GDLDOCK_XML

struct _SnapMode {
   int  index;
   char name[32];
   char svg_file[64];
};

#ifdef __song_c__
SnapMode snap_modes[] = {
	{SNAP_OFF,    "snap off",         "snap_free"},
	//{SNAP_NORMAL, "snap normal",      "snap_normal.svg"},
	{SNAP_BAR,    "snap to bar",      "snap2bar"},
	{SNAP_BEAT,   "snap to beat",     "crotchet"},
	{SNAP_16,     "snap to 16th",     "semiquaver"},
	{SNAP_Q,      "snap to quantise", "snap2q"},
};
#else
extern SnapMode snap_modes[];
extern GuiSong gui_song;
#endif

typedef struct config_setting_t config_setting_t;

struct _GuiSong
{
    GtkWidget*              ch_menu;              // Channel selection context menu.
    GtkWidget*              ch_mitem[AM_MAX_CHS]; // individual menu items for channel selection menu.
    char*                   loaded_layout;
};

typedef struct {
   GtkTreeModel* plugin_list;
} ChannelUserData;


void     song_init                     ();
bool     song_free                     ();
bool     song_load                     ();
void     song_load_new                 (const char* filename);
void     song_unload                   ();
void     song_save                     ();

void     song_add_track                (AMTrackType, int n_channels, TrackCallback, gpointer);
void     song_del_track                (GList*, AyyiHandler, gpointer);

void     song_add_parts_from_selection (AyyiPanel*, AyyiSongPos*, int track_offset);
void     song_add_part_from_prototype  (AMPart*);
void     song_part_new                 (AMPoolItem*, AyyiIdent* region, GPos*, AMTrack*, int64_t len, uint32_t* _inset, unsigned int colour, const char* name, AyyiHandler, gpointer);
AyyiAction*
         song_part_delete              (const AMPart*);
void     song_part_selection_repeat    (GList*, GPos* end);

void     recent_songs_update           ();

void     song_get_history              ();

#endif
