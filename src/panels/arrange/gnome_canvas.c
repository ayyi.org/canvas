/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __arrange_private__
#include "global.h"
#include "math.h"
#include <gdk/gdkkeysyms.h>
#include <libgnomecanvas/libgnomecanvas.h>

#include "waveform/waveform.h"
#include "model/time.h"
#include "model/transport.h"
#include "model/pool.h"
#include "model/curve.h"
#include "model/part_manager.h"
#include "model/track_list.h"
#include "windows.h"
#include "window.statusbar.h"
#include "panels/arrange.h"
#include "panels/arrange/part.h"
#include "panels/arrange/part_renamer.h"
#include "panels/arrange/track_control_midi.h"
#include "gnome/automation.h"
#include "pool.h"
#include "song.h"
#include "support.h"
#include "gnome/part_item.h"
#include "map_item.h"
#include "part_manager.h"
#include "gnome/songmap.h"
#include "widgets/time_ruler.h"
#include "gl_canvas.h"
#include "toolbox.h"
#include "arrange/gnome_canvas_op.h"

extern SMConfig*      config;
extern GHashTable*    canvas_types;

static CanvasType gcanvas_type = 0;

GtkWidget*      gcanvas_new                  (Arrange*);
static void     gcanvas_free                 (Arrange*);
static void     gcanvas_redraw               (Arrange*);
static gint     gcanvas_item_on_event        (GtkWidget*,       GdkEvent*, gpointer);
static gint     gcanvas_root_on_event        (GnomeCanvasItem*, GdkEvent*, Arrange*);
static void     gcanvas_root_dblclick        (GdkEvent*);
static gint     gcanvas_trans_item_cb        (GnomeCanvasItem*, GdkEvent*, gpointer);
static void     gcanvas_get_part_rect        (Arrange*, AMPart*, DRect*);
static void     gcanvas_locator_new          (Arrange*, int n);
#ifndef DEBUG_DISABLE_TOOLBAR
static void     gcanvas_part_new_req_from_event(Arrange*, GdkEvent*);
#endif
static void     gcanvas_set_spp_px           (Arrange*, int);
static void     gcanvas_set_spp_samples      (Arrange*, uint32_t);
static void     gcanvas_set_spp              (Arrange*, const GPos*);
static void     gcanvas_redraw_locators      (Arrange*);
static void     gcanvas_on_size_change       (AGlObservable*, AGlVal, gpointer);
static void     gcanvas_on_song_load         (Arrange*);
static void     gcanvas_on_view_change       (Arrange*, AMChangeType);
static void     gcanvas_on_track_selection   (Observable*, AMVal, gpointer);
static void     gcanvas_on_peak_view_toggle  (Arrange*);
static void     gcanvas_on_tool_change       (GObject*, gpointer);
static void     gcanvas_on_colour_change     (Arrange*);
static void     gcanvas_on_new_track         (Arrange*, ArrTrk*);
static void     gcanvas_on_track_delete      (Arrange*, ArrTrk*);
static gint     gcanvas_item_on_scroll       (GnomeCanvasItem*, GdkEvent*, gpointer);
static gint     gcanvas_item_on_mute         (GnomeCanvasItem*, GdkEvent*, gpointer);
static gboolean gcanvas_scroll_mouse_timeout (Pti* origin);
static void     gcanvas_while_recording      (Arrange*);
static void     gcanvas_post_recording       (Arrange*);
static void     gcanvas_record_part_new      (Arrange*, int t);
static void     gcanvas_set_part_selection   (Arrange*);
static void     gcanvas_set_note_selection   (Arrange*, MidiNote*);
static void     gcanvas_part_editing_start   (Arrange*, AMPart*);
static void     gcanvas_part_editing_stop    (Arrange*, AMPart*);
static void     gcanvas_hscroll_cb           (GtkWidget*, gpointer);
static void     gcanvas_vscroll_cb           (GtkWidget*, gpointer);
static void     gcanvas_on_part_change       (Arrange*, AMPart*);
static bool     gcanvas_add_part_item        (Arrange*, AMPart*);
static void     gcanvas_delete_part_item     (Arrange*, AMPart*);
static void     gcanvas_do_follow            (Arrange*);
static void     gcanvas_scroll_to            (Arrange*, int x, int y);
static void     gcanvas_scroll_to_pos        (Arrange*, GPos*, TrackDispNum);
static void     gcanvas_scroll_left          (Arrange*, int pix);
static void     gcanvas_scroll_right         (Arrange*, int pix);
static void     gcanvas_scroll_up            (Arrange*, int pix);
static void     gcanvas_scroll_down          (Arrange*, int pix);
static void     gcanvas_page_left            (Arrange*);
static void     gcanvas_page_right           (Arrange*);
static FeatureType gcanvas_pick_feature      (CanvasOp*);

static void     gcanvas_parts_destroy        (Arrange*);
static void     gcanvas_pick                 (Arrange*, double, double, int* track, AMPart**);
static GnomeCanvasPart* gcanvas_part_item_lookup (Arrange*, AMPart*);
static void     gcanvas_get_scroll_offsets   (Arrange*, int* x, int* y);
static void     gcanvas_px_to_model          (Arrange*, Pti, AyyiSongPos*, ArrTrk**);
static uint32_t gcanvas_get_viewport_left    (Arrange*);
static void     gcanvas_get_view_fraction    (Arrange*, double* x, double* y);

void            gcanvas_zorder_update        (Arrange*);

static void     gcanvas_set_from_menu        (Arrange*);

static void     arr_cyc_fill_update          (Arrange*);

static void     arr_on_note_selection_change (GObject*, AyyiPanel*, GList*, gpointer);
static void     arr_on_parts_change          (GObject*, Arrange*);

static gboolean canvas_on_leave              (GtkWidget*, GdkEventCrossing*, Arrange*);

static gpointer callbacks[] = {
	gcanvas_item_on_event,
	gcanvas_item_on_event,
	gcanvas_item_on_mute,
	gcanvas_item_on_event,
	gcanvas_item_on_scroll,
	gcanvas_item_on_event
};


void
gcanvas_init ()
{
	gcanvas_type = arr_register_canvas("gnome", gcanvas_new, gcanvas_set_from_menu);
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &gcanvas_type);

	cc->provides = 0; // this canvas does not provide a rulerbar or track-list

	// this signal is deprecated - use am_parts item-changed with AM_CHANGE_SELECTION instead
	am_song__connect("note-selection-change", G_CALLBACK(arr_on_note_selection_change), NULL);
}


CanvasType
gcanvas_get_type ()
{
	return gcanvas_type;
}


GtkWidget*
gcanvas_new (Arrange* arrange)
{
	g_automation_init();

	PF;
	arrange->canvas = AYYI_NEW(struct _canvas,
		.gnome                = g_new0(g_canvas, 1),
		.type                 = gcanvas_get_type(),
		.redraw               = gcanvas_redraw,
		.free                 = gcanvas_free,
		.px_to_model          = gcanvas_px_to_model,
		.get_scroll_offsets   = gcanvas_get_scroll_offsets,
		.get_viewport_left    = gcanvas_get_viewport_left,
		.pick                 = gcanvas_pick,
		.scroll_to            = gcanvas_scroll_to,
		.get_view_fraction    = gcanvas_get_view_fraction,
		.on_song_load         = gcanvas_on_song_load,
		.on_view_change       = gcanvas_on_view_change,
		.on_locator_change    = gcanvas_redraw_locators,
		.part_new             = gcanvas_add_part_item,
		.part_delete          = gcanvas_delete_part_item,
		.on_part_change       = gcanvas_on_part_change,
		.get_part_rect        = gcanvas_get_part_rect,
		.set_spp              = gcanvas_set_spp_samples,
		.on_new_track         = gcanvas_on_new_track,
		.on_track_delete      = gcanvas_on_track_delete,
		.while_recording      = gcanvas_while_recording,
		.post_recording       = gcanvas_post_recording,
		.record_part_new      = gcanvas_record_part_new,
		.set_part_selection   = gcanvas_set_part_selection,
		.set_note_selection   = gcanvas_set_note_selection,
		.part_editing_start   = gcanvas_part_editing_start,
		.part_editing_stop    = gcanvas_part_editing_stop,
		.do_follow            = gcanvas_do_follow,
		.scroll_to_pos        = gcanvas_scroll_to_pos,
		.scroll_left          = gcanvas_scroll_left,
		.scroll_right         = gcanvas_scroll_right,
		.scroll_up            = gcanvas_scroll_up,
		.scroll_down          = gcanvas_scroll_down,
		.pick_feature         = gcanvas_pick_feature
	);

	arrange->canvas->op = (CanvasOp*)gnome_canvas_op__new(arrange, OP_NONE);

	g_canvas* g = arrange->canvas->gnome;
	g->spp_pts = gnome_canvas_points_new(2);
	int i; for(i=0;i<AM_MAX_TRK+1;i++) g->tline[i] = NULL;

	double canvas_width = arr_canvas_width(arrange);

	GtkWidget* canvas_widget = arrange->canvas->widget = gnome_canvas_new_aa();
	gtk_widget_set_name(canvas_widget, "Arrange GCanvas");

	// put the canvas in a scrollwindow
	// (canvas has its own scrollbars so we dont add any additional ones.)
	GtkWidget* scrollwin = arrange->canvas_scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrollwin);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gnome_canvas_set_center_scroll_region (GNOME_CANVAS(canvas_widget), FALSE);

	// set scrollable size
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas_widget), 0, 0, canvas_width, 400);

	// set the scroll speed for the scrollbar buttons
	GtkAdjustment* adj  = arrange->canvas->h_adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(scrollwin));
	adj->step_increment = 10.0;
	adj                 = arrange->canvas->v_adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrollwin));
	adj->step_increment = VSCROLL_STEP_INCREMENT;

	gtk_container_add(GTK_CONTAINER(scrollwin), canvas_widget);

	//TODO finish attaching this to canvas h scrollbar to prevent autoscrolling while mouse is pressed.
	gint
	hscroll_buttonpress (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
	{
		((Arrange*)user_data)->pressed = true;
		return NOT_HANDLED;
	}
	gint
	hscroll_buttonrelease (GtkWidget* widget, GdkEventButton* event, gpointer user_data)
	{
		((Arrange*)user_data)->pressed = false;
		return NOT_HANDLED;
	}
	GtkWidget* hzoom_scale = gtk_scrolled_window_get_hscrollbar(GTK_SCROLLED_WINDOW(scrollwin));
	g_signal_connect((gpointer)hzoom_scale, "button-press-event", G_CALLBACK(hscroll_buttonpress), arrange);
	g_signal_connect((gpointer)hzoom_scale, "button-release-event", G_CALLBACK(hscroll_buttonrelease), arrange);

	g_signal_connect (gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(scrollwin)), "value-changed", G_CALLBACK(gcanvas_hscroll_cb), arrange);
	g_signal_connect (gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrollwin)), "value-changed", G_CALLBACK(gcanvas_vscroll_cb), arrange);
	g_signal_connect (canvas_widget, "leave-notify-event", G_CALLBACK(canvas_on_leave), arrange);

	void
	gcanvas_draw (Arrange* arrange)
	{
		ArrCanvas* canvas = arrange->canvas;
		g_canvas* g = canvas->gnome;
		int* track_pos = g->track_pos;

		double canvas_width = arr_canvas_width(arrange);
		double tot_height = arr_canvas_height(arrange);

		GnomeCanvasGroup* rootgroup = gnome_canvas_root(GNOME_CANVAS(canvas->widget));

		// tracks
		int track_count = SONG_LOADED ? am_track_list_count(song->tracks) : AM_MAX_TRK;

		TrackDispNum d;
		for(d=0;d<track_count;d++){
			ArrTrk* at = track_list__track_by_display_index(arrange->priv->tracks, d);
			// dividing lines (at the *top* of the track):
			if(d < 8) dbg (2, "track line: pos=%i", track_pos[d]);
			g->tline[d] = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
						  "outline_color_rgba", 0x22222277, // semi-transparent so it just changes the tone slightly.
						  "x1", 0.0,           "y1", (double)track_pos[d],
						  "x2", canvas_width,  "y2", (double)track_pos[d],
						  "width_units", 1.0,
						  NULL);

			// track automation
			if(at){
				if(SONG_LOADED) automation_track_draw(arrange, at);
			}
		}
		// one more line at the bottom
		g->tline[track_count] = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
									"outline_color_rgba", 0x22222277,
									"x1", 0.0,          "y1", tot_height,
									"x2", canvas_width, "y2", tot_height,
									"width_units", 1.0,
									NULL);
		gnome_canvas_item_lower_to_bottom (g->tline[track_count]);

		// spp
		g->spp = gnome_canvas_item_new(rootgroup, gnome_canvas_line_get_type(),
					 "fill-color-rgba", 0x000000af,
					 "width-pixels", 1,
					 NULL);
		g->spp2 = gnome_canvas_item_new(rootgroup, gnome_canvas_line_get_type(),
					 "fill-color-rgba", 0xffffffaf,
					 "width-pixels", 1,
					 NULL);
		if(!song->periodic) gnome_canvas_item_hide(canvas->gnome->spp); //dont show until transport is enabled.
		if(!song->periodic) gnome_canvas_item_hide(canvas->gnome->spp2);

		// invisible crosshairs
		GnomeCanvasPoints* pts = gnome_canvas_points_new(2);
		pts->coords[0] = pts->coords[2] = 50.0 * 8.0;
		pts->coords[1] = 0.0;     
		pts->coords[3] = 0.0;
		g->xhair       = gnome_canvas_item_new(rootgroup, gnome_canvas_line_get_type(),
					   "fill-color-rgba", 0x7f7f7f7f,
					   "width-pixels", 1,
					   "points", pts, NULL);
		gnome_canvas_item_lower_to_bottom (g->xhair);
		gnome_canvas_item_hide(g->xhair);
		pts->coords[0] = 0.0;
		pts->coords[1] = pts->coords[3] = 0.0;
		pts->coords[2] = (double)canvas_width; //maybe it should be the visible width instead?
		g->yhair = gnome_canvas_item_new(rootgroup, gnome_canvas_line_get_type(), "fill-color-rgba", 0x7f7f7f7f, "width-pixels", 1, "points", pts, NULL);
		gnome_canvas_item_lower_to_bottom(g->yhair);
		gnome_canvas_item_hide(g->yhair);

		// locators
		gcanvas_locator_new(arrange, 1);
		gcanvas_locator_new(arrange, 2);

		// cycle locators fill
		g->cyc_fill = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		  "fill-color-rgba", colour_lighter_rgba(arrange->bg_colour, 2),
		  "x1", song->loc[1].vals[0].val.sp.beat * PX_PER_BEAT, "y1", 0.0,
		  "x2", song->loc[2].vals[0].val.sp.beat * PX_PER_BEAT, "y2", tot_height,
		  "width_units", 2.0,
		  NULL);
		gnome_canvas_item_lower_to_bottom(g->cyc_fill);
		g_signal_connect(G_OBJECT(g->cyc_fill), "event", (GtkSignalFunc)gcanvas_trans_item_cb, arrange);

		// selection drag box
		g->drag_box = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		  "outline_color_rgba", 0xff000077,
		  "fill-color-rgba",    0x00000000,
		  "x1", 0.0,            "y1", 0.0,
		  "x2", 10.0,           "y2", 10.0,
		  NULL);
		gnome_canvas_item_hide(g->drag_box);

		// control points for automation editing
		automation_ctlpts_init(arrange);

		// invisible line for part split operations
		g->split_line = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		  "outline_color_rgba", 0xffffffff,
		  "x1", 0.0,  "y1", 0.0,
		  "x2", 10.0, "y2", 10.0,
		  "width_units", 3.0,
		  NULL);
		gnome_canvas_item_hide(g->split_line);

		// background
		// -need to catch button clicks on the canvas root
		canvas->gnome->bg = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		  "fill-color-rgba", arrange->bg_colour,
		  "x1", 0.0,          "y1", 0.0,
		  "x2", canvas_width, "y2", 1000.0,
		  "width_units", 2.0,
		  NULL);
		gnome_canvas_item_lower_to_bottom(canvas->gnome->bg);
		g_signal_connect(G_OBJECT(canvas->gnome->bg), "event", (GCallback)gcanvas_root_on_event, arrange);

		if(config->arr_background_image && strlen(config->arr_background_image)){
			GdkPixbuf* bg_pixbuf = gdk_pixbuf_new_from_file(config->arr_background_image, NULL);
			gnome_canvas_item_new(rootgroup, gnome_canvas_pixbuf_get_type(),
			"pixbuf", bg_pixbuf,
			"x", 0.0, "y", 0.0,
			"width-set", TRUE,
			"height-set", TRUE,
			"width", (float)gdk_pixbuf_get_width(bg_pixbuf) * 2.0,
			"height", (float)gdk_pixbuf_get_height(bg_pixbuf) * 2.0,
			NULL);
		}

		gnome_canvas_points_unref(pts);
	  
		//----------------------------------------------------------------

		gtk_drag_dest_set(GTK_WIDGET(arrange->canvas->widget), 
						  GTK_DEST_DEFAULT_ALL,           // specifies what gtk will automatically do for us.
						  app->dnd.file_drag_types,       // const GtkTargetEntry *targets, array of target/flags/info
						  app->dnd.file_drag_types_count, // gint n_targets,
						  (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
		g_signal_connect(G_OBJECT(arrange->canvas->widget), "drag-data-received", G_CALLBACK(arr_canvas_drag_received), NULL);

		//----------------------------------------------------------------

#if 0
		if(SONG_LOADED){
			gcanvas_parts_destroy(arrange);
			part_foreach {
				call_i(arrange->canvas->part_new, arrange, gpart);
			} end_part_foreach;
		}
#endif

		dbg (2, "calling redraw... %p type=%i", arrange->canvas->redraw, arrange->canvas->type);
		arrange->canvas->redraw(arrange);
		gcanvas_on_colour_change(arrange);
	}

	gcanvas_draw(arrange);

	void on_transport_cycle (GObject* _song, gpointer _arrange)
	{
		PF;
		Arrange* arrange = _arrange;
		if(CANVAS_IS_GNOME){
			if(ayyi_transport_is_cycling()) gnome_canvas_item_show(arrange->canvas->gnome->cyc_fill);
			else gnome_canvas_item_hide(arrange->canvas->gnome->cyc_fill);
		}
	}
	am_song__connect("transport-cycle", G_CALLBACK(on_transport_cycle), arrange);

	void _on_song_unload (GObject* _arrange, gpointer _)
	{
		Arrange* arr = (Arrange*)_arrange;
		if(arr->canvas) gcanvas_parts_destroy(arr);
	}
	g_signal_connect(arrange, "song-unload", G_CALLBACK(_on_song_unload), g);

	g_signal_connect(arrange, "tool-change", G_CALLBACK(gcanvas_on_tool_change), g);

	g_signal_connect(am_parts, "change", G_CALLBACK(arr_on_parts_change), arrange);

	agl_observable_subscribe (arrange->width, gcanvas_on_size_change, arrange);
	observable_subscribe (am_tracks->selection2, gcanvas_on_track_selection, arrange);

	return scrollwin;
}


static void
gcanvas_free (Arrange* arrange)
{
	g_canvas* g = arrange->canvas->gnome;
	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_ARRANGE);

	part_foreach { arrange->canvas->part_delete(arrange, gpart); } end_part_foreach;

	g_automation_curves_undraw(arrange);
	if(g->auto_.dirty_curves) g_list_free0(g->auto_.dirty_curves);

	for(int t=0;t<AM_MAX_TRK+1;t++) arrange->canvas->gnome->tline[t] = NULL;

	g_signal_handlers_disconnect_by_func(gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(arrange->canvas_scrollwin)), gcanvas_hscroll_cb, arrange);
	g_signal_handlers_disconnect_by_func(gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(arrange->canvas_scrollwin)), gcanvas_vscroll_cb, arrange);
	g_signal_handlers_disconnect_matched(arrange, G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_DATA, ((AyyiArrangeClass*)k)->song_unload_signal, 0, NULL, NULL, g);
	g_signal_handlers_disconnect_matched(arrange, G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_DATA, ((AyyiArrangeClass*)k)->tool_change_signal, 0, NULL, NULL, g);

	#define parts_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (am_parts, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange) != 1) pwarn("part handler disconnection");
	parts_handler_disconnect(arr_on_parts_change);

	agl_observable_unsubscribe (arrange->width, gcanvas_on_size_change, arrange);
	observable_unsubscribe(am_tracks->selection2, gcanvas_on_track_selection, arrange);

	gtk_widget_destroy0(arrange->canvas->widget);
	gtk_widget_destroy0(arrange->canvas_scrollwin);
	if(arrange->canvas->gnome->spp_pts) gnome_canvas_points_free(arrange->canvas->gnome->spp_pts);
	g_free(arrange->canvas->gnome);
	g_free(arrange->canvas);
	arrange->canvas = NULL;
}


static void
gcanvas_redraw(Arrange* arrange)
{
	// Ensures the arrange canvas reflects the current state of the loaded song.
	// note: track control area is not part of the canvas and must be updated separately.

	g_return_if_fail(CANVAS_IS_GNOME);

	ArrCanvas* canvas = arrange->canvas;
	double tot_height = arr_canvas_height(arrange);

	// tracks

	int track_count = SONG_LOADED ? am_track_list_count(song->tracks) : AM_MAX_TRK;

	TrackDispNum d; for(d=0;d<track_count;d++){
		// background lines
		if(!canvas->gnome->tline[d] || !GNOME_IS_CANVAS_ITEM(canvas->gnome->tline[d])){ perr ("track_line: bad widget ref. i=%i", d); continue; }

#if 0 // should only be handled in gcanvas_background_update
		int* track_pos = canvas->gnome->track_pos;
		gnome_canvas_item_set(canvas->gnome->tline[d],
                        "y1", (double)(track_pos[d]),
                        "y2", (double)(track_pos[d]),
                        NULL);
#endif

    if(ayyi.got_shm){
      if(d < am_track_list_count(song->tracks)) gnome_canvas_item_show(canvas->gnome->tline[d]);
      else gnome_canvas_item_hide(canvas->gnome->tline[d]);
    }
  }
  // one more track line
  gnome_canvas_item_set(canvas->gnome->tline[track_count],
                       "y1", tot_height,
                       "y2", tot_height,
                       NULL);

  // Parts
  // -----

  int i = 0;
  GList* l = canvas->gnome->parts;
  if(!l) dbg (2, "canvas partlist empty.");
  for(;l;l=l->next){
    dbg (2, "part %i of %i...", i, g_list_length(song->parts->list));

    GnomeCanvasItem* item = l->data;
    AMPart* gpart = GNOME_CANVAS_PART(item)->gpart;
    g_return_if_fail(gpart);

	g_return_if_fail(am_partmanager__verify_part(gpart)); //tmp!

    gnome_canvas_item_set(item, "track", am_track_list_position(song->tracks, gpart->track), NULL);

    i++;
  }

  GList* parts = arrange->canvas->gnome->parts;
  for(;parts;parts=parts->next){
		GnomeCanvasPart* item = parts->data;
		gnome_canvas_part_clear_pixbuf(item);
	}

	double viewport_height = (double)arrange->canvas_scrollwin->allocation.height;

	arrange->canvas->gnome->spp_pts->coords[1] = 0.0;
	arrange->canvas->gnome->spp_pts->coords[3] = viewport_height + 1000; //y2
	if(SONG_LOADED){
		// needed in case the timer is stopped
		GPos pos; am_transport_get_pos(&pos);
		gcanvas_set_spp(arrange, &pos);
	}

	gcanvas_redraw_locators(arrange);
	gcanvas_background_update(arrange);

	gcanvas_zorder_update(arrange);
}


static void
gcanvas_scroll_up(Arrange* arrange, int pix)
{
  // scroll up by the number of pixels given.
  // -the scrollbar should move upwards, but objects inside the scrollwin move downwards.

  int xscrolloffset, yscrolloffset;
  gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
  gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset - pix);
}


static void
gcanvas_scroll_down(Arrange* arrange, int pix)
{
	// scroll down by the number of pixels given.
	// -the scrollbar should move downwards, but objects inside the scrollwin move upwards.

	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset + pix);
}


static FeatureType
gcanvas_pick_feature (CanvasOp* op)
{
	Arrange* arrange = op->arrange;

	ArrTrk* at = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);
	ArrTrackMidi* atm = (ArrTrackMidi*)at;
	g_return_val_if_fail(at->trk_ctl, FEATURE_NONE);
	if(CANVAS_IS_GNOME){ //TODO remove canvas specific code.
		GnomeCanvasPart* gpart = GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item);
		if(gpart->pixbuf){
#ifndef DEBUG_TRACKCONTROL
			int part_height = gdk_pixbuf_get_height(gpart->pixbuf); //TODO more generic to use track height instead.
			int vchart_height = atm->velocity_chart_height;
			if(op->diff.y < part_height - vchart_height){
				dbg(2, "note area... diff=%.2f", op->diff.y);
				bool at_start, at_end;
				if (arr_part_pick_note(arrange, gpart->gpart, op->diff, &at_start, &at_end)) {
					dbg(2, "over note! at_start=%i at_end=%i", at_start, at_end);
					if(at_start){
						return FEATURE_NOTE_LEFT;
					} else if(at_end){
						return FEATURE_NOTE_RIGHT;
					} else {
						return FEATURE_NOTE;
					}
				}
				else { dbg(2, "not note. diff=%.2fx%.2f px1=%.2f", op->diff.x, op->diff.y, op->p1.x);
				return FEATURE_NONE;
				}
			}else if(op->diff.y < part_height - vchart_height + 2){
				dbg(2, "FEATURE_VEL_DIVIDER diff=%.2f", op->diff.y);
				return FEATURE_VEL_DIVIDER;
			}else{
				dbg(2, "velocity zone...");
				gboolean at_top = FALSE;
				Ptd p = {op->diff.x, op->diff.y - (part_height - vchart_height)};
				if(gnome_canvas_part_pick_velocity_bar(GNOME_CANVAS_PART(((GnomeCanvasOp*)op)->item), p, &at_top)){
					if(at_top){
						return FEATURE_VEL_BAR_TOP;
					}
				}
				return FEATURE_VEL_AREA;
			}
#endif
		}
	}

	return FEATURE_NONE;
}


static void
gcanvas_on_size_change (AGlObservable* o, AGlVal val, gpointer _arrange)
{
	Arrange* arrange = _arrange;

	gnome_canvas_set_scroll_region(GNOME_CANVAS(arrange->canvas->widget), 0.0, 0.0, arr_canvas_width(arrange), arr_canvas_height(arrange));
	songmap_set_size(arrange);
	gcanvas_background_update(arrange);

#ifndef DEBUG_DISABLE_RULERBAR
	time_ruler_update_flags(TIME_RULER(arrange->ruler));
#endif
}


static void
gcanvas_on_song_load (Arrange* arrange)
{
	// Update vertical aspects of the canvas
	// TODO this is the product of refactoring. check if is needed.
	gcanvas_scrollregion_update(arrange);

	// automation
	if(arrange->view_options[SHOW_AUTOMATION].value) automation_show(arrange); else automation_hide(arrange);

	arrange->canvas->redraw(arrange);
}


static void
gcanvas_on_view_change(Arrange* arrange, AMChangeType change_type)
{
	bool need_redraw = false;
	bool automation = false;

	if(change_type & AM_CHANGE_ZOOM_H || change_type & AM_CHANGE_ZOOM_V){
		automation = true;
	}

	if(change_type & AM_CHANGE_BACKGROUND_COLOUR){
		gcanvas_on_colour_change(arrange);
		change_type &= ~AM_CHANGE_BACKGROUND_COLOUR;
	}

	if(change_type & AM_CHANGE_WIDTH || change_type & AM_CHANGE_HEIGHT){
		gcanvas_background_update(arrange);
	}

	if(change_type & AM_CHANGE_SHOW_PART_CONTENTS){
		gcanvas_on_peak_view_toggle(arrange);
	}

	if(change_type) need_redraw = true;

	if(need_redraw){
		gcanvas_scrollregion_update(arrange);
		arrange->canvas->redraw(arrange);
	}

	if(automation) automation_view_on_size_change(arrange);
}


static void
gcanvas_on_track_selection (Observable* o, AMVal val, gpointer arrange)
{
	((Arrange*)arrange)->automation.sel = (AutoSelection){0,};
}


static void
gcanvas_on_peak_view_toggle (Arrange* arrange)
{
	ViewOption* option = &arrange->view_options[SHOW_PART_CONTENTS];
	GList* parts = song->parts->list;
	if(parts){
		for(;parts;parts=parts->next){
			AMPart* gpart = (AMPart*)parts->data;
			GnomeCanvasItem* part = gcanvas_get_part_widget(arrange, gpart);
			if(part) gnome_canvas_item_set(part, "show_contents", option->value, NULL);
		}
	}
}


static void
gcanvas_redraw_locators(Arrange* arrange)
{
  {
    double viewport_height = (double)arrange->canvas_scrollwin->allocation.height;

    GnomeCanvasPoints* pts = gnome_canvas_points_new(2);
    pts->coords[1] = 0.0;
    pts->coords[3] = viewport_height + 1000; //y2
    int i; for(i=1;i<3;i++){
      pts->coords[0] = pts->coords[2] = arr_spos2px(arrange, &song->loc[i].vals[0].val.sp);
      gnome_canvas_item_set(arrange->canvas->gnome->loc[i], "points", pts, NULL);
    }

    gnome_canvas_points_unref(pts);
  }
  arr_cyc_fill_update(arrange);
}


static void
gcanvas_on_tool_change (GObject* _arrange, gpointer _g)
{
	// swap signals on part items

	Arrange* arrange = (Arrange*)_arrange;

	if(!SONG_LOADED) return;

	GtkSignalFunc callback = callbacks[arrange->toolbox.current];

	for(GList* l=song->parts->list;l;l=l->next){
		AMPart* gpart = l->data;
		GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, gpart);
		if(!item){ perr ("no canvas item!"); continue; }
		GnomeCanvasPart* part = GNOME_CANVAS_PART(item);
		dbg (2, "changing signal for part=%p '%s' box=%p id=%"PRIi64, gpart, gpart->name, item, ((AyyiRegionBase*)am_part_get_shared(gpart))->id);

		if(!part->handler_id){ perr ("no Part signal handler!"); continue; }
		dbg (2, "handler_id=%lu", part->handler_id);
		g_signal_handler_disconnect((gpointer)item, part->handler_id);
		part->handler_id = g_signal_connect(GTK_OBJECT(item), "event", callback, arrange);
	}
}


static void
gcanvas_on_colour_change (Arrange* arrange)
{
  g_canvas* g = arrange->canvas->gnome;

  uint32_t rgba = arrange->bg_colour;

  gnome_canvas_item_set(g->bg, "fill-color-rgba", rgba, NULL);

  //set colour of the track dividing lines:

  //work out a colour offset that works for both dark and bright colours:
  //int averagecolor = (color.red + color.green + color.blue)/(256*3);
  //dbg (2, "average_colour=%i offset=%i", averagecolor, offset);
  /*
  int offset = 8;
  //if(averagecolor > 256/2) offset *= -1;
  if(!colour_is_dark_rgba(rgba)) offset *= -1;

  int offset_red = MAX(MIN(colour.red  /256 + offset, 255), 0);
  int offset_grn = MAX(MIN(colour.green/256 + offset, 255), 0);
  int offset_blu = MAX(MIN(colour.blue /256 + offset, 255), 0);

  int linecolour = offset_red*256*256*256 + offset_grn*256*256 + offset_blu*256 + 255;
  */

  uint32_t linecolour = colour_lighter_rgba(rgba, 1);

  for(int t=0;t<AM_MAX_TRK+1;t++){
    if(g->tline[t] && !GNOME_IS_CANVAS_ITEM(g->tline[t])) pwarn("t=%i", t);
    if(g->tline[t]) gnome_canvas_item_set(g->tline[t], "outline_color_rgba", linecolour, NULL);
  }
  gnome_canvas_item_set(g->cyc_fill, "fill-color-rgba", linecolour, NULL);
}


static void
gcanvas_on_new_track (Arrange* arrange, ArrTrk* atr)
{
	TrackNum t = am_track_list_position(song->tracks, atr->track);
	GnomeCanvasGroup* rootgroup = gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget));

	if (!arrange->canvas->gnome->tline[t])
		arrange->canvas->gnome->tline[t] = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		          "outline_color_rgba", 0x22222277,
		          "x1", 0.0,                       "y1", (double)arrange->canvas->gnome->track_pos[t],
		          "x2", arr_canvas_width(arrange), "y2", (double)arrange->canvas->gnome->track_pos[t],
		          "width_units", 1.0,
		          NULL);

	int tb = am_track_list_count(song->tracks); //bottom line.
	if (!arrange->canvas->gnome->tline[tb])
		arrange->canvas->gnome->tline[tb] = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
		          "outline_color_rgba", 0x22222277,
		          "x1", 0.0,                       "y1", (double)arrange->canvas->gnome->track_pos[tb],
		          "x2", arr_canvas_width(arrange), "y2", (double)arrange->canvas->gnome->track_pos[tb],
		          "width_units", 1.0,
		          NULL);

	automation_track_draw(arrange, atr);
}


static void
gcanvas_on_track_delete(Arrange* arrange, ArrTrk* atr)
{
}


static gint
gcanvas_item_on_event (GtkWidget* widget, GdkEvent* event, gpointer _arrange)
{
	// Called following most mouse operations on an Arrange Part.

	Arrange* arrange = _arrange; g_return_val_if_fail(arrange, NOT_HANDLED);
	g_return_val_if_fail(CANVAS_IS_GNOME, NOT_HANDLED);

	GnomeCanvasItem* item = GNOME_CANVAS_ITEM(widget);
	g_canvas* g = arrange->canvas->gnome;

	static Ptd start;            // cursor(?) position before the start of a drag.
	static Pti origin;           // position that scrolling is done relative to.
	                             // -intitially it is the original cursor position.
	CanvasOp* op = arrange->canvas->op;
	GnomeCanvasOp* gop = (GnomeCanvasOp*)op;
	static int resizing;         // 1=moving part front, 2=moving part end.

	static double right;         // the rhs of the part before the start of a resize operation.

	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &op->xscrolloffset, &op->yscrolloffset);

	gdk_window_get_pointer(arrange->canvas->widget->window, &gop->win.x, &gop->win.y, NULL);

	GPos pos; // for general usage.

	GnomeCanvasPart* part = GNOME_CANVAS_PART(item);
	static AMPart* gpart = NULL;
	bool shift   = event->button.state & GDK_SHIFT_MASK;
	bool control = event->button.state & GDK_CONTROL_MASK;

#ifndef DEBUG_DISABLE_TOOLBAR
	if(arrange->toolbox.current == TOOL_MUTE){ perr ("fn not valid for tool TOOL_MUTE."); return -1; }
#endif

	op->mouse.x = event->button.x;
	op->mouse.y = event->button.y;

	// convert cursor position to item relative coords
	gnome_canvas_item_w2i(item->parent, &op->mouse.x, &op->mouse.y);

	// get offset of cursor from part boundary to constrain movement
	gnome_canvas_item_get_bounds(item, &op->p1.x, &op->p1.y, &op->p2.x, &op->p2.y);

	switch (event->type) {
		case GDK_ENTER_NOTIFY:
			// warning: mouse coords seems to be sometimes invalid with this event.
			dbg (2, "GDK_ENTER_NOTIFY...");
			gpart = part->gpart;
			gop->item = item;
			op->part = part->gpart;
			gnome_canvas_op__set_arrange(op, arrange);
#if 0
			if(op->type == OP_NONE && !op->ready){
				dbg(0, "new op from ENTER...");
				gnome_canvas_op__new(OP_NONE); //havin this here is problematic, as mouse is not set. Lets see if we can move it to PRESS
				op->mouse = mouse;
				if(mouse.x < 1) pwarn("ENTER: mouse coords not set!");
				op->press(op_, event);
			}
			else dbg(0, "op->type=%i", op->type);
#endif
			arr_statusbar_printf(arrange, 3, gpart->name);
			break;

		case GDK_BUTTON_PRESS:
			;int optype = OP_NONE;
			dbg (0, "%sGDK_BUTTON_PRESS%s...", bold, white);
			ayyi_panel_on_focus((AyyiPanel*)arrange); //may now be unnecc?
			switch(event->button.button){
				case 3:
					gtk_menu_popup(GTK_MENU(arrange->partmenu), NULL, NULL, NULL, NULL, event->button.button, (guint32)(event->button.time));
					break;
				case 1:
					gtk_window_set_focus(GTK_WINDOW(gtk_widget_get_toplevel(arrange->canvas->widget)), NULL);
					gpart = part->gpart;

					op->mouse.x = gop->win.x + op->xscrolloffset;
					op->mouse.y = gop->win.y + op->yscrolloffset;
					op->op->press(op, event); // TODO this maybe called more than once. we need it here to set diff.
					// if this works out, it replaces READY_VELOCITY
					FeatureType feature = pick_feature_type(op);
					dbg(0, "GDK_BUTTON_PRESS: feature=%s", print_feature_type(feature));
					switch(feature){
						case FEATURE_NOTE_LEFT:
							optype = OP_NOTE_LEFT;
							dbg(0, "new op from PRESS... OP_NOTE_LEFT");
							op->part = part->gpart;
							gnome_canvas_op__new(arrange, OP_NOTE_LEFT);
							gop->item = item;
							op->op->press(op, event);
							break;
						case FEATURE_NOTE_RIGHT:
							if(arrange->part_editing){
								//gdk_window_get_pointer(window->widget->window, &arrange->mouse.x, &arrange->mouse.y, NULL);
								//arr_coords_win_to_canvas(arrange, &mouse.x, &mouse.y);

								optype = OP_NOTE_RIGHT;
								if(op->mouse.x < 1) pwarn("PRESS: mouse coords not set!");
								dbg(0, "new op from PRESS... OP_NOTE_RIGHT");
								op->part = part->gpart;
								gnome_canvas_op__new(arrange, OP_NOTE_RIGHT);
								gop->item = item;
								if(op->mouse.x < 1) pwarn("mouse coords not set!");
								op->op->press(op, event);
							}
							break;
						case FEATURE_NOTE:
							if(arrange->part_editing){
								const MidiNote* note = arr_part_pick_note(arrange, op->part, op->diff, NULL, NULL);
								if(!arr_part_note_is_selected (arrange, gpart, (MidiNote*)note)){
									optype = OP_NOTE_SELECT;
								}else{
									// dont select if already selected.
									optype = OP_EDIT;
								}
							}
							break;
						default:
							break;
					}

					// update Part selection
					if(!arrange->part_editing && !op->ready){
						if (shift){
							if(arrange->part_selection){
								arr_part_selection_add_range(arrange, gpart, (AMPart*)arrange->part_selection->data); //this is relative to first part selected - should be the last?
							}
						} else if (control){
							arr_part_selection_add(arrange, gpart);
						} else {
							if(am_partmanager__is_selected(gpart)){
								// dont change the selection.
							} else {
								// the part grabs the selection
								arr_part_selection_reset(arrange, true);
								arr_part_selection_add(arrange, gpart);
							}
						}
						gcanvas_zorder_update(arrange);
					}

					start = op->mouse;
					// init autoscrolling vars
					origin.x = gop->win.x + op->xscrolloffset;
					origin.y = gop->win.y + op->yscrolloffset;

					//--------------

					if (shift){
					}
					else if(feature){
						switch(feature){
							case FEATURE_VEL_BAR_TOP:
								dbg(0, "setting OP_VELOCITY");
								optype = OP_VELOCITY;
								break;
							case FEATURE_VEL_DIVIDER:
								dbg(0, "setting OP_VDIVIDER");
								optype = OP_VDIVIDER;
								break;
							case FEATURE_NOTE_RIGHT:
								/*
								dbg(0, "setting OP_NOTE_RIGHT");
								op = OP_NOTE_RIGHT;
								//return HANDLED;
								*/
								break;
							case FEATURE_PART_LEFT:
								dbg(0, "PRESS: OP_RESIZE_LEFT.");
								resizing = READY_RESIZE_LEFT;
								optype = OP_RESIZE_LEFT;
								right = op->p2.x;
								break;
							case FEATURE_PART_RIGHT:
								if(control){
									dbg(0, "PRESS: OP_REPEAT.");
									shell__statusbar_print(((AyyiPanel*)arrange)->window, 1, "repeat...");
									optype = OP_REPEAT;
									//this op requires a selection (it doesnt, but there is no harm in making one.)
									if(!arrange->part_selection){
										arr_part_selection_add(op->arrange, op->part);
									}
								}else{
									dbg(0, "PRESS: OP_RESIZE_RIGHT.");
									resizing = READY_RESIZE_RIGHT;
									optype = OP_RESIZE_RIGHT;
								}
								break;
							default:
								break;
						}
					}
					else if (control){
						printf("\n%s(): copying...\n", __func__);
						// select the part first?
						optype = OP_COPY;       //FIXME not necc true!!
					}
#ifndef DEBUG_DISABLE_TOOLBAR
					else if(arrange->toolbox.current == TOOL_SCISSORS){
						optype = OP_SPLIT;
						dbg (0, "buttonpress: starting split...");
						gnome_canvas_item_set(g->split_line, "x1", op->mouse.x, "y1", op->p1.y, "x2", op->mouse.x, "y2", op->p2.y, "outline_color_rgba", 0xffffffff, NULL);
						gnome_canvas_item_raise_to_top(g->split_line);
						gnome_canvas_item_show(g->split_line);
					}
					else if(arrange->toolbox.current == TOOL_PENCIL){
						dbg(0, "starting OP_DRAW...");
						optype = OP_DRAW;
					}
#endif
					else if(arrange->part_editing){
						dbg(2, "starting OP_EDIT...");
						optype = OP_EDIT;
					}
					else{
						// moving
						printf("GDK_BUTTON_PRESS: dragging...\n");
						start = op->mouse;

						optype = OP_MOVE;
					}
					op               = (CanvasOp*)gnome_canvas_op__new(arrange, optype);
					op->part         = part->gpart;
					op->max_movement = 0.0;
					gop->item        = item;
					op->start        = start;
					gop->origin      = origin;
					dbg(0, "new op from PRESS... op=%i '%s'", optype, canvas_op__print_type(op));
					op->op->press(op, event);
					break;

				default:
					break;
			}
			break;

		case GDK_MOTION_NOTIFY:

			switch(op->type){
				case OP_NOTE_RIGHT:
				case OP_VELOCITY:
					break;
				case OP_SPLIT:
					if(op->mouse.x > op->p1.x+1 && op->mouse.x < op->p2.x-1){
						gnome_canvas_item_set(g->split_line, "x1", op->mouse.x, "x2", op->mouse.x, NULL);
						gnome_canvas_item_show(g->split_line);

						arr_snap_px2pos(arrange, &pos, op->mouse.x);
						arr_statusbar_print_pos(arrange, 3, &pos);
					}
					else{
						gnome_canvas_item_hide(g->split_line);
						arr_statusbar_printf(arrange, 3, "");
					}
					break;
				default:
					if ((op->type == OP_MOVE) && (event->motion.state & GDK_BUTTON1_MASK)){
					}
					else if ((op->type == OP_COPY) && (event->motion.state & GDK_BUTTON1_MASK)){
						// dont remove this elseif until the final else is removed
					}
					else if (op->type == OP_RESIZE_LEFT){
						// resizing from the part start

						double new_part_length = right - event->button.x;
						if(new_part_length < 1.0) return -1;
						arr_px2spos(arrange, &gpart->length, new_part_length);

						dbg (2, "resize left: px2=%.1f newlength=%i", op->p2.x, gpart->length.beat);
					}
					else {
						// we are inside a part and not in the middle of any other operation.
						if(op->ready != READY_RESIZE_LEFT && op->ready != READY_RESIZE_RIGHT) resizing = FALSE;
					}
					break;
			}

			op->op->motion(op);
			break;

		case GDK_BUTTON_RELEASE:
			gnome_canvas_item_ungrab(item, event->button.time);

			if(resizing){
				if(op->type == OP_RESIZE_LEFT){
					double orig_part_length_z = arr_spos2px(arrange, &gpart->length);
					arr_px2spos(arrange, &gpart->length, start.x - op->mouse.x + orig_part_length_z);

					arr_snap_px2spos(arrange, &gpart->start, op->mouse.x);

					arr_snap_px2pos(arrange, &pos, op->mouse.x); char bbst[128]; pos2bbst(&pos, bbst); dbg (0, "release: resize left: %s", bbst);

					am_part_trim_left(gpart, NULL, NULL);
				}
				resizing = FALSE;
				arr_cursor_reset(arrange, arrange->canvas->widget->window);

				// dont redraw yet - wait for the callback.
			}
			else if(op->type == OP_SPLIT){
				dbg (2, "split finished.");
				gnome_canvas_item_hide(g->split_line);

				if(op->mouse.x > op->p1.x+1 && op->mouse.x < op->p2.x-1){ //if mouse is outside the part, we consider the operation aborted.
					pwarn("TODO result is snapped, but is not shown...");
					arr_snap_px2pos(arrange, &pos, op->mouse.x);

					AyyiSongPos p;
					songpos_gui2ayyi(&p, &pos);

					am_part_split(gpart, &p, NULL, NULL);
				}
			}

			op->op->finish(op);
			break;

		case GDK_LEAVE_NOTIFY:
			// warning: this gets called on inexplicable occasions
			dbg(2, "GDK_LEAVE_NOTIFY...");
			arr_statusbar_printf(arrange, 3, " ");

			if(!resizing){
				// reset the cursor (currently only really needed if resize handles are active):
				arr_cursor_reset(arrange, arrange->canvas->widget->window);
			} else dbg (0, "LEAVE_NOTIFY: still resizing.");
			break;

		default:
			break;
	}

	return HANDLED;
}


static gint
gcanvas_trans_item_cb(GnomeCanvasItem* item, GdkEvent* event, gpointer data)
{
	// A proxy to pass on events for canvas items that are supposed to be transparent to events.

	Arrange* arrange = data; if (arrange->canvas->type != 1){ perr ("wrong canvas type."); return 0; }

	gcanvas_root_on_event(arrange->canvas->gnome->bg, event, arrange);
	return HANDLED;
}


static gint
gcanvas_root_on_event(GnomeCanvasItem* item, GdkEvent* event, Arrange* arrange)
{
	// Handle mouse events on the arrange canvas background.

	if(!CANVAS_IS_GNOME){ perr ("canvas type!"); return HANDLED; }

	static CanvasOp* op = NULL;
	if(op) gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &op->xscrolloffset, &op->yscrolloffset);
	if(op) gdk_window_get_pointer(arrange->canvas->widget->window, &((GnomeCanvasOp*)op)->win.x, &((GnomeCanvasOp*)op)->win.y, NULL);
	if(op) { op->mouse.x = event->button.x; op->mouse.y = event->button.y; }

	#ifndef DEBUG_DISABLE_TOOLBAR
	if(arrange->toolbox.current == TOOL_SCROLL){ return gcanvas_item_on_scroll(item, event, arrange); }
	#endif

	switch (event->type){

		case GDK_2BUTTON_PRESS:
			gcanvas_root_dblclick(event);
			break;

		case GDK_BUTTON_PRESS:
			dbg (2, "button=%i", event->button.type);
			if(event->button.button == 3){
				AyyiArrangeClass* klass = AYYI_ARRANGE_GET_CLASS(arrange);

				ayyi_panel_on_focus(&arrange->panel);
				// open pop-up menu
#ifndef DEBUG_DISABLE_TOOLBAR
				gtk_menu_popup(GTK_MENU(klass->rootmenu), NULL, NULL, toolbox_position, event, event->button.button, (guint32)(event->button.time));
#endif
			}else{
				// left-button press or middle-button press

				dbg (0, "normal button press...");
				// the background has been clicked on

				// cancel all part selections
				arr_part_selection_reset(arrange, true);
				// clear automation selection
				arrange->automation.sel.subpath = 0;

#ifndef DEBUG_DISABLE_TOOLBAR
				if(arrange->toolbox.current == TOOL_PENCIL){
					gcanvas_part_new_req_from_event(arrange, event);
#else
				if(false){
#endif
				}else{
					GnomeCanvasOp* gop = gnome_canvas_op__new(arrange, OP_BOX);
					op             = (CanvasOp*)gop;
					op->start.x    = event->button.x;
					op->start.y    = event->button.y;
					op->mouse.x    = event->button.x;
					op->mouse.y    = event->button.y;
					gop->origin    = (Pti){gop->win.x + op->xscrolloffset, gop->win.y + op->yscrolloffset};

					op->op->press(op, event);
				}
			}
			gtk_window_set_focus(GTK_WINDOW(gtk_widget_get_toplevel(arrange->canvas->widget)), NULL);

			break;

		case GDK_MOTION_NOTIFY:
			if(op && op->type==OP_BOX && (event->motion.state & GDK_BUTTON1_MASK)){
				op->mouse.x = event->button.x;
				op->mouse.y = event->button.y;
				op->op->motion(op);
			}
			else{
				// normal mouse movement. Put the coords to the statusbar
				char str[64];
				arr_px2bbs(arrange, event->button.x, str);
				shell__statusbar_print(((AyyiPanel*)arrange)->window, 3, str);
			}
#ifndef DEBUG_DISABLE_RULERBAR
			if(arrange->ruler) time_ruler_set_pos(TIME_RULER(arrange->ruler), event->button.x);
#endif
			break;

		case GDK_BUTTON_RELEASE:
			gnome_canvas_item_ungrab(item, event->button.time);
			if(op) op->op->finish(op);

		case GDK_LEAVE_NOTIFY:
			arr_statusbar_printf(arrange, 3, "");
			break;

		default:
			break;
	}

	return HANDLED;
}


static void
gcanvas_root_dblclick(GdkEvent* event)
{
  dbg(0, "FIXME addpart here.");
}


#define LOC_COLOUR 0x3333aa88
static void
gcanvas_locator_new(Arrange* arrange, int n)
{
	ArrCanvas* canvas = arrange->canvas;
	GnomeCanvasPoints* pts = gnome_canvas_points_new(2);

	pts->coords[0] = pts->coords[2] = song->loc[n].vals[0].val.sp.beat * PX_PER_BEAT;
	pts->coords[1] = 0.0;
	pts->coords[3] = canvas->gnome->track_pos[AM_MAX_TRK];

	canvas->gnome->loc[n] = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget)), 
	gnome_canvas_line_get_type(),
		"fill-color-rgba", LOC_COLOUR,
		"width-pixels", 1,
		"points", pts,
		NULL);
	gnome_canvas_item_lower_to_bottom(canvas->gnome->loc[n]);
	gnome_canvas_points_unref(pts);
}


static void
gcanvas_get_part_rect(Arrange* arrange, AMPart* gpart, DRect* b)
{
	//oh dear, we cant quickly get the canvasitem from the gpart...
	//how to add item* arg to a type-agnostic callback?
	//-clutter and gnomecanvas need Item*, but gl canvas doesnt.
	GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, gpart);
	g_return_if_fail(item);
	gnome_canvas_item_get_bounds(item, &b->x1, &b->y1, &b->x2, &b->y2);
}


#ifndef DEBUG_DISABLE_TOOLBAR
static void
gcanvas_part_new_req_from_event(Arrange* arrange, GdkEvent* event)
{
  //request new part using vars taken from a supplied GdkEvent.

  //Used by the pencil tool to create empty Midi parts.

  //currently we use an arbitrary pool_item, but strictly, the part should be empty.

  ASSERT_SONG_SHM;
  PF;

  uint64_t length = bars2mu(8);
  double   x      = ((GdkEventButton*)event)->x;
  double   y      = ((GdkEventButton*)event)->y;
  uint32_t colour = 1;

  TrackDispNum d = arr_px2trk(arrange, (int)y);
  AMTrack* trk = track_list__get_trk_from_display_index(arrange->priv->tracks, d);
  g_return_if_fail(trk);

  AMPoolItem* pool_item = AM_TRK_IS_AUDIO(trk) ? (song->pool->list ? pool_item = song->pool->list->data : NULL) : NULL;

  GPos pos;
  arr_snap_px2pos(arrange, &pos, x);

  song_part_new(pool_item, 0, &pos, trk, length, NULL, colour, NULL, NULL, NULL);
  return;
}
#endif


static void
gcanvas_set_spp_px(Arrange* arrange, int px)
{
	g_canvas* c = arrange->canvas->gnome;
	c->spp_pts->coords[0] = c->spp_pts->coords[2] = px;
	gnome_canvas_item_set(c->spp, "points", c->spp_pts, NULL);

	GnomeCanvasPoints* pts2 = gnome_canvas_points_new(2);
	pts2->coords[0] = c->spp_pts->coords[0] -0.5;
	pts2->coords[1] = c->spp_pts->coords[1];
	pts2->coords[2] = c->spp_pts->coords[2] -0.5;
	pts2->coords[3] = c->spp_pts->coords[3];
	gnome_canvas_item_set(c->spp2, "points", pts2, NULL);
	gnome_canvas_points_free(pts2);

	gnome_canvas_item_show(arrange->canvas->gnome->spp2); //FIXME
}


static void
gcanvas_set_spp_samples(Arrange* arrange, uint32_t samples)
{
	gcanvas_set_spp_px(arrange, arr_samples2px(arrange, samples));
}


static void
gcanvas_set_spp(Arrange* arrange, const GPos* pos)
{
	gcanvas_set_spp_px(arrange, (pos->beat + pos->sub/4.0 + ((float)pos->tick)/AYYI_SUBS_PER_BEAT) * hzoom(arrange) * PX_PER_BEAT);
}


static gint
gcanvas_item_on_mute (GnomeCanvasItem* item, GdkEvent* event, gpointer data)
{
	Arrange* arrange; if (!(arrange = ARRANGE_FIRST)) return false;

	switch (event->type) {
		case GDK_BUTTON_PRESS:
			switch (event->button.button) {
				case 3:
					gtk_menu_popup(GTK_MENU(arrange->partmenu), NULL, NULL, NULL, NULL, event->button.button, (guint32)(event->button.time));
					break;
				case 1:
					am_part_mute_toggle_async(GNOME_CANVAS_PART(item)->gpart);
				default:
					break;
			}
		default:
			break;
	}

	return HANDLED;
}


static gint
gcanvas_item_on_scroll(GnomeCanvasItem* item, GdkEvent* event, gpointer data)
{
	// Handle interactivity for the 'hand'/scrolling tool.
	// -we use the Xwindow coordinates as the canvas coords are not a solid reference while scrolling.
	// -bounds checking is done by the gtk scrollbar set fn, so theres no need for us to do it.
	// FIXME it wont go all the way to the bottom when vscrollbar is visible.

	Arrange* arrange; if( !(arrange = ARRANGE_FIRST) ) return false;

	CanvasOp* op = arrange->canvas->op;

	static Pti origin;            //position that scrolling is done relative to.
	                              //-intitially it is the original cursor position.
	static gint   x, y;           //the value to scroll to.
	gint rootx, rooty;            //the position of the toplevel window in screen coords.

	static gboolean dragging = FALSE;

	static gint win_x=0, win_y=0; //cursor position relative to the window origin.
	gdk_window_get_pointer(arrange->canvas->widget->window, &win_x, &win_y, NULL);

	gint topwinx = 0, topwiny = 0;
	gdk_window_get_pointer(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &topwinx, &topwiny, NULL);

	//we use gdk_window_get_origin(), as it eliminates window furniture
	gdk_window_get_origin(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &rootx, &rooty);

	// how big is the screen?
	GdkScreen* screen = gdk_screen_get_default(); //FIXME
	int screen_h = gdk_screen_get_height(screen);

	int xscrolloffset;
	int yscrolloffset;

	switch (event->type)
	{
		case GDK_BUTTON_PRESS:
			switch(event->button.button){
				case 3:
					gtk_menu_popup(GTK_MENU(arrange->partmenu), NULL, NULL, NULL, NULL, event->button.button, (guint32)(event->button.time));
					break;
				case 1:
					gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
					origin.x = win_x + xscrolloffset;
					origin.y = win_y + yscrolloffset;

					gcanvas_scroll_mouse_timeout(NULL); // reset the scroll speed.

					dragging = TRUE;
				default:
					break;
			}
			break;

		case GDK_MOTION_NOTIFY:
			if(event->motion.state == GDK_CONTROL_MASK) printf("control!\n");

			if(dragging){
				x = origin.x - win_x;
				y = origin.y - win_y;

				// is the mouse at the edge of the window?
				if(!op->timer){
					if(win_x > arrange->canvas_scrollwin->allocation.width - SCROLL_EDGE_WIDTH){
					op->timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)gcanvas_scroll_mouse_timeout, &origin);
				}
				else if(rootx + topwinx < SCROLL_EDGE_WIDTH){
					// at the lhs edge of the screen.
					op->timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)gcanvas_scroll_mouse_timeout, &origin);
				}
				else if(rooty +topwiny < SCROLL_EDGE_WIDTH){
					// at the top edge of the screen.
					op->timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)gcanvas_scroll_mouse_timeout, &origin);
				}
				else if(rooty + topwiny > screen_h - SCROLL_EDGE_WIDTH){
					// at the bottom edge of the screen.
					op->timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)gcanvas_scroll_mouse_timeout, &origin);
				}
				else gcanvas_scroll_mouse_timeout(NULL); //no scrolling needed. Reset the scroll speed.
				}

				arrange->canvas->scroll_to(arrange, x, y);
			}
			break;

		case GDK_BUTTON_RELEASE:
			gnome_canvas_item_ungrab(item, event->button.time);

			g_source_remove0(op->timer);

			dragging = false;
			break;

		default:
			break;
	}

	return HANDLED;
}


static gboolean
gcanvas_scroll_mouse_timeout (Pti* origin)
{
	// Called following the timeout set by the canvas scroll tool when the mouse
	// is at the _edge_of_the_screen_.
	// If the mouse is near the edge, we scroll the window.

	// TODO see time_ruler_mouse_timeout() for a better version of this.

	Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) return false;

	static int scroll_dx = SCROLL_MULTIPLIER; //maybe we could add this as a g_object property of the timer?
	if(!origin){ scroll_dx = SCROLL_MULTIPLIER; return FALSE; } //hack!! passing NULL data resets the scroll speed.

	gboolean stop = TRUE;

	GdkWindow* window = arrange->canvas->widget->window;
	g_return_val_if_fail(window, false);

	gint x=0, y=0;
	gdk_window_get_pointer(window, &x, &y, NULL);

	gint topwinx=0, topwiny=0;
	gdk_window_get_pointer(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &topwinx, &topwiny, NULL);

	gint rootx, rooty;
	gdk_window_get_origin(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &rootx, &rooty);

	//how big is the screen?
	GdkScreen *screen = gdk_screen_get_default(); //this may not be the right screen in a multiscreen setup!!
	int screen_h = gdk_screen_get_height(screen);

	//do the bounds checking again in case the mouse has moved:
	if(x > arrange->canvas_scrollwin->allocation.width - SCROLL_EDGE_WIDTH){
		//we are at the rhs edge.
		arrange->canvas->scroll_left(arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->x = origin->x - scroll_dx/SCROLL_MULTIPLIER;//update startx so we have an up-to-date ref point.
		stop = FALSE;
	}
	else if(rootx + topwinx < SCROLL_EDGE_WIDTH){
		//we are at lhs edge of the screen
		arrange->canvas->scroll_right(arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->x = origin->x + scroll_dx / SCROLL_MULTIPLIER;
		stop = FALSE;
	}
	else if(rooty +topwiny < SCROLL_EDGE_WIDTH){
		//we are at the top edge of the screen.
		gcanvas_scroll_down(arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->y += scroll_dx / SCROLL_MULTIPLIER;
		stop = FALSE;
	}
	else if(rooty + topwiny > screen_h - SCROLL_EDGE_WIDTH){
		//we are at the bottom edge of the screen.
		gcanvas_scroll_up(arrange, scroll_dx / SCROLL_MULTIPLIER);
		origin->y = origin->y - scroll_dx / SCROLL_MULTIPLIER;
		stop = FALSE;
	}
	else scroll_dx = 1;

	// Update the scroll speed
	if(!stop) scroll_dx = MIN((int)(scroll_dx * 1.0 + 1), SCROLL_MAX_SPEED*SCROLL_EDGE_WIDTH);

	if(stop) arrange->canvas->op->timer = 0;

	return !stop;
}


GPos record_start = {0,0,0};

static void
gcanvas_while_recording(Arrange* arrange)
{
	g_return_if_fail(CANVAS_IS_GNOME);

	int t = -1;
	AyyiTrack* track = NULL;
	while((track = ayyi_track__next_armed(track))){
		t = track->shm_idx;
		break;
	}
	g_return_if_fail(t > -1);

	if(!arrange->canvas->gnome->record_part){
		am_transport_get_pos(&record_start);
		gcanvas_record_part_new(arrange, t);
	}else{
		GPos pos;
		am_transport_get_pos(&pos);
		double x = arr_pos2px(arrange, &pos);

		gnome_canvas_item_set(arrange->canvas->gnome->record_part, "x2", x, NULL);
	}
}


static void
gcanvas_record_part_new(Arrange* arrange, int t)
{
	if(!CANVAS_IS_GNOME){ pwarn ("wrong canvas type!"); return; }

	g_canvas* g = arrange->canvas->gnome;
	GnomeCanvasGroup* rootgroup = gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget));

	double offset_y1 = 0; // distance from track top to part top.

	GPos pos;
	am_transport_get_pos(&pos);

	double height = g->track_pos[t+1] - g->track_pos[t] - 2;
	if(height < 2){ perr ("track_height too small! t=%i (%i-%i)", t, g->track_pos[t+1], g->track_pos[t]); return; }

	if(g->record_part) pwarn("already set!");
	g->record_part = gnome_canvas_item_new(rootgroup, gnome_canvas_rect_get_type(),
        "outline_color", "white",
        "fill-color-rgba", 0xff0000ff,
        "x1", (double)arr_pos2px(arrange, &pos),
        "y1", (double)g->track_pos[t] + offset_y1,
        "x2", (double)10 * hzoom(arrange),
        //"y2", height,
        "y2", (double)g->track_pos[t+1],
        "width_units", 2.0,
        NULL);

	dbg(1, "t=%i top=%i", t, g->track_pos[t]);
}


static void
gcanvas_set_part_selection(Arrange* arrange)
{
	g_canvas* g = arrange->canvas->gnome;

	// clear the old selection
	dbg (3, "clearing old selection...");
	GList* l = g->selected_parts;
	for(;l;l=l->next){
		GnomeCanvasItem* item = l->data;
		gnome_canvas_part_set_selected(item, FALSE);
	}

	//g_list_clear(g->selected_parts);
	g->selected_parts = NULL;
	dbg (3, "old selection cleared.");

#if 0
	bool item_valid(Arrange* arrange, GnomeCanvasPart* item)
	{
		GList* l = arrange->canvas->gnome->parts;
		for(;l;l=l->next){
			GnomeCanvasPart* p = l->data;
			if(p == item) return true;
		}
		return false;
	}
#endif

	l = arrange->part_selection;
	while(l){
		AMPart* part = l->data;
		GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, part);
		if(item){
			if(!GNOME_IS_CANVAS_ITEM(item)){ perr ("!GNOME_IS_CANVAS_ITEM %s", part->name); continue; }
			gnome_canvas_part_set_selected(item, TRUE);
			g->selected_parts = g_list_append(g->selected_parts, item);
		}

		l = l->next;
	}
	dbg (1, "n_items_selected=%i", g_list_length(g->selected_parts));

	gcanvas_zorder_update(arrange); //make sure selected parts are fully visible.
}


static void
gcanvas_set_note_selection(Arrange* arrange, MidiNote* note)
{
	if(note){
		AMPart* part = arrange->part_selection->data;
		GnomeCanvasPart* canvas_part = (GnomeCanvasPart*)gcanvas_get_part_widget(arrange, part);
		gnome_canvas_part_set_edit_selection(canvas_part, g_list_append(NULL, note));
	}
}


static void
gcanvas_part_editing_start(Arrange* arrange, AMPart* gpart)
{
	GnomeCanvasPart* canvas_part = (GnomeCanvasPart*)gcanvas_get_part_widget(arrange, gpart);
	gnome_canvas_part_begin_edit(canvas_part);
}


static void
gcanvas_part_editing_stop(Arrange* arrange, AMPart* gpart)
{
	GnomeCanvasPart* canvas_part = (GnomeCanvasPart*)gcanvas_get_part_widget(arrange, gpart);
	gnome_canvas_part_end_edit(canvas_part);
}


void
gcanvas_set_xhairs(Arrange* arrange, double px1, double py1)
{
	// Set the position of the xhairs for a drag operation.
	// -it doesnt hide or unhide them.

	GnomeCanvasPoints* pts = gnome_canvas_points_new(2);
	int xscrolloffset;
	int yscrolloffset;
	double height = arr_canvas_height(arrange) + 1000;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);

	pts->coords[0] = pts->coords[2] = px1;
	pts->coords[1] = 0.0;
	pts->coords[3] = height;
	gnome_canvas_item_set(arrange->canvas->gnome->xhair, "points", pts, NULL);

	pts->coords[0] = (double)xscrolloffset;
	pts->coords[2] = xscrolloffset + arr_canvas_width(arrange);
	pts->coords[1] = pts->coords[3] = py1;
	gnome_canvas_item_set(arrange->canvas->gnome->yhair, "points", pts, NULL);

	gnome_canvas_points_unref(pts);
}


static void
gcanvas_hscroll_cb(GtkWidget* widget, gpointer _arrange)
{
	// Arrange window horizontal scrollbar callback.
	// -is attached to the scrollwin "value-changed" signal.

	Arrange* arrange = (Arrange*)_arrange;
	g_return_if_fail(arrange);

#ifdef USE_GNOMECANVAS
	double x1,cwidth,y1,y2;
	x1=0; y1=0; cwidth=0.0; y2=0;
	gnome_canvas_get_scroll_region(GNOME_CANVAS(arrange->canvas->widget), &x1,&y1,&cwidth,&y2);

	//adjust the rulerbar:
	//-yes it looks like we do have to do this each time.
#if 0
	if(arrange->rulerbar){
		gtk_scrolled_window_set_hadjustment(GTK_SCROLLED_WINDOW(arrange->rulerbar->scrollwindow), GTK_ADJUSTMENT(widget));
	}
#endif
	if(arrange->songmap){
		gtk_scrolled_window_set_hadjustment(GTK_SCROLLED_WINDOW(arrange->songmap), GTK_ADJUSTMENT(widget));
	}
#endif

	arr_on_view_changed(arrange, AM_CHANGE_POS_X); //TODO might be wrong place
}


static void
gcanvas_vscroll_cb(GtkWidget* widget, gpointer _arrange)
{
	// Arrange window vertical scrollbar change callback.

	dbg (3, "val=%.1f", GTK_ADJUSTMENT(widget)->value);

	// scroll the tracklist/control area
	Arrange* arrange = (Arrange*)_arrange;
	GtkAdjustment* canvas_adj = GTK_ADJUSTMENT(widget);
	GtkAdjustment* adj        = gtk_viewport_get_vadjustment(GTK_VIEWPORT(arrange->track_list_box));
	adj->upper = canvas_adj->upper;
	gtk_adjustment_set_value(adj, canvas_adj->value);

#ifdef NO_LONGER_NEEDED
	//dont know why, but this line changes the step_increment so we have to reset it.
	gtk_viewport_set_vadjustment(GTK_VIEWPORT(arrange->track_list_box), GTK_ADJUSTMENT(widget));
	GTK_ADJUSTMENT(widget)->step_increment = VSCROLL_STEP_INCREMENT;
#endif
}


bool
gcanvas_add_part_item(Arrange* arrange, AMPart* part)
{
	// dnd: you cannot make canvas items drop targets. Drops are received at the overall canvas level.

	g_return_val_if_fail(part, false);
	g_return_val_if_fail(arrange, false);
	g_return_val_if_fail(ayyi_pos2mu(&part->length), false);
	g_return_val_if_fail(CANVAS_IS_GNOME, false);
	g_return_val_if_fail(!gcanvas_part_item_lookup(arrange, part), false);
	ArrCanvas* c = arrange->canvas;

	GnomeCanvasGroup* group = gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget));

	g_return_val_if_fail(part->track, false);
	ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, part->track);
	g_return_val_if_fail(atr, false);
	int track_height = atr->height;
	g_return_val_if_fail(track_height > 1, false);

	if (am_palette_is_dark(song->palette, part->bg_colour)) part->fg_colour = 0xffffffff;

	if (_debug_ > 1) am_part__print(part);

	GnomeCanvasItem* item = gnome_canvas_item_new(group, gnome_canvas_part_get_type(),
		"outline_color_rgba", config->part_outline_colour, // TODO fails if we remove this.
		"arrange", arrange,                                // warning, arrange property must be set *before* Part.
		"gpart", part,
		NULL);
	dbg (2, "item=%p type=%s", item, g_type_name(G_TYPE_FROM_INSTANCE(item)));
	c->gnome->parts = g_list_prepend(c->gnome->parts, item);

	GNOME_CANVAS_PART(item)->handler_id = g_signal_connect(GTK_OBJECT(item), "event", (GtkSignalFunc)gcanvas_item_on_event, arrange);

	gcanvas_zorder_update(arrange);

	return true;
}


static void
gcanvas_delete_part_item (Arrange* arrange, AMPart* part)
{
	GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, part);
	if(!item) return;
	dbg (2, "box=%p", item);

	gtk_object_destroy(GTK_OBJECT(item));
	arrange->canvas->gnome->parts = g_list_remove(arrange->canvas->gnome->parts, item);
	arrange->canvas->gnome->selected_parts = g_list_remove(arrange->canvas->gnome->selected_parts, item);
}


static void
gcanvas_post_recording(Arrange* arrange)
{
	if(arrange->canvas->gnome->record_part) gtk_object_destroy(GTK_OBJECT(arrange->canvas->gnome->record_part));
	arrange->canvas->gnome->record_part = NULL;
}


static void
gcanvas_on_part_change(Arrange* arrange, AMPart* part)
{
	GnomeCanvasItem* item = GNOME_CANVAS_ITEM(gcanvas_part_item_lookup(arrange, part));
	if(item){
		gnome_canvas_item_set(item, "gpart", part, NULL); // force a redraw
	}
}


static void
gcanvas_parts_destroy(Arrange* arrange)
{
	GList* l = arrange->canvas->gnome->parts;
	for(;l;l=l->next){
		gtk_object_destroy((GtkObject*)l->data);
	}

	g_list_clear(arrange->canvas->gnome->parts);
}


static void
gcanvas_pick(Arrange* arrange, double x, double y, int* track, AMPart** gpart)
{
	GnomeCanvasItem* item = gnome_canvas_get_item_at(GNOME_CANVAS(arrange->canvas->widget), (double)x, (double)y);
	if(!item) return;
	if(GNOME_IS_CANVAS_PART(item)){
		AMPart* p = GNOME_CANVAS_PART(item)->gpart;
		if(gpart) *gpart = p;
	}
}


void
gcanvas_part_rename_start(Arrange* arrange)
{
	typedef struct {   // only one member, can pass it directly without this struct
		AMPart* part;
	} C;
	C* c = g_new0(C, 1);

	void rename_done(char* new_text, gpointer _c)
	{
		C* c = _c;
		PF;
		am_part_rename(c->part, new_text, NULL, NULL);
		g_free(c);
	}

	void gcanvas_rename_popup_get_position(GnomeCanvasPart* item, gint* _x, gint* _y)
	{
		double px1, py1, px2, py2;
		gnome_canvas_item_get_bounds(GNOME_CANVAS_ITEM(item), &px1, &py1, &px2, &py2);

		*_x = px1, *_y = py2;
	}

	GList* parts = arrange->part_selection;
	dbg (1, "renaming %i parts...", g_list_length(parts));
	c->part = parts->data;
	GnomeCanvasPart* part_item = gcanvas_part_item_lookup(arrange, c->part);
	Pti pos; gcanvas_rename_popup_get_position(part_item, &pos.x, &pos.y);
	part_rename_popup(&arrange->panel, arrange->canvas->widget, pos, FALSE, c->part, rename_done, c);
	for(;parts;parts=parts->next){
	}
	dbg (1, "finished");
}


static GnomeCanvasPart*
gcanvas_part_item_lookup(Arrange* arrange, AMPart* part)
{
	// Find the canvas item for the given part, by looking through the list of canvas items.
	// Note: gcanvas_get_canvaspart_by_part() should be slightly faster, as it only searches Parts.

	GnomeCanvasGroup* root = gnome_canvas_root(GNOME_CANVAS(arrange->canvas->widget));

	GList* parts = root->item_list;
	for(;parts;parts=parts->next){
		GnomeCanvasPart* item = parts->data;
		if(item->gpart == part) return item;
	}
	dbg (1, "not found.");
	return NULL;
}


GnomeCanvasPart*
gcanvas_get_canvaspart_by_part(Arrange* arrange, AMPart* gpart)
{
	if(!CANVAS_IS_GNOME) return NULL;

	GList* l = arrange->canvas->gnome->parts;
	for(;l;l=l->next){
		GnomeCanvasPart* part = l->data;
		if(part->gpart == gpart) return part;
	}
	return NULL;
}


GnomeCanvasItem*
gcanvas_get_part_widget(Arrange* arrange, AMPart* part)
{
	// Find the canvas item for the given part.
	// There may not be an item, eg during song load.

	g_return_val_if_fail(part, NULL);
	if(!CANVAS_IS_GNOME){ perr ("wrong canvas type!"); return NULL; }

	GList* parts = arrange->canvas->gnome->parts;
	for(;parts;parts=parts->next){
		GnomeCanvasPart* item = parts->data;
		if(item->gpart == part) return GNOME_CANVAS_ITEM(item);
	}
	dbg (2, "not found. part=%p part_name='%s' n=%i", part, part->name, g_list_length(arrange->canvas->gnome->parts));
	return NULL;
}


static void
gcanvas_page_left(Arrange* arrange)
{
	//page the arrange canvas left by one page.

	//note: this is currently being used in the case of large jumps, which is not really appropriate.

	if(!((AyyiPanel*)&arrange->panel)->follow) return;

	double overlap = 20.0;      //show some of the previous page to avoid user disorientation.
	                            //-if window is less than this it will page backwards!

	int xscrolloffset;          //pixels
	int yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
	int width = (int)arrange->canvas_scrollwin->allocation.width;

	int dx = -width;
	if(dx < overlap){} //dont use the overlap for very small changes.
	else dx -= overlap;

	xscrolloffset += dx;
	if(xscrolloffset < 0) xscrolloffset = 0;

	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset);

	arr_on_view_changed(arrange, AM_CHANGE_POS_X);
}


static void
gcanvas_page_right(Arrange* arrange)
{
	// Page the arrange canvas right by one page.

	if(!((AyyiPanel*)&arrange->panel)->follow) return;

	double overlap = 20.0;      //show some of the previous page to avoid user disorientation.
	                            //-if window is less than this it will page backwards!

	if(CANVAS_IS_GNOME){
		int xscrolloffset, yscrolloffset; //pixels
		gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);

		xscrolloffset += arr_get_viewport_right_px(arrange) - overlap;

		gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset, yscrolloffset);
	}

	arr_on_view_changed(arrange, AM_CHANGE_POS_X);
}


static void
gcanvas_do_follow(Arrange* arrange)
{
	// Page display if required.

	g_return_if_fail(CANVAS_IS_GNOME);

	if     (arrange->canvas->gnome->spp_pts->coords[0] > arr_get_viewport_right_px(arrange)) gcanvas_page_right(arrange);
	else if(arrange->canvas->gnome->spp_pts->coords[0] < arr_get_viewport_left_px(arrange)) gcanvas_page_left (arrange);
	else dbg (3, "not following... spp=%fpx, left=%f", arrange->canvas->gnome->spp_pts->coords[0], arr_get_viewport_left_px(arrange));
}


void
gcanvas_scroll_to(Arrange* arrange, int x, int y)
{
	// Scroll the arrange canvas to the given absolute canvas position.
	// -use -1 to keep existing position.

	if(x < 0) x = arr_get_viewport_left_px(arrange);

	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), x, y);
}


static void
gcanvas_scroll_to_pos(Arrange* arrange, GPos* pos, TrackDispNum tnum)
{
	// Scroll the arrange canvas to the given song position and track.

	char bbst[64]; pos2bbst(pos, bbst);

	int x = arr_pos2px(arrange, pos); 
	int y = MAX(arrange->canvas->gnome->track_pos[tnum] - 10, 0);
	dbg(2, "x=%i y=%i", x, y);

	double x1, x2, y1, y2;
	gnome_canvas_get_scroll_region(GNOME_CANVAS(arrange->canvas->widget), &x1, &y1, &x2, &y2);
	if(y > y2) pwarn("y out of range: %i > %.1f", y, y2);

	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), x, y);
}


static void
gcanvas_scroll_left(Arrange* arrange, int px)
{
	// Scroll left by the number of pixels given.
	// -the scrollbar should move to the left, but objects inside the scrollwin move to the right.

	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset - px, yscrolloffset);
}


static void
gcanvas_scroll_right(Arrange* arrange, int px)
{
	// Scroll right by the number of pixels given.
	// -the scrollbar should move to the right, but objects inside the scrollwin move to the left.

	GnomeCanvas* w = GNOME_CANVAS(arrange->canvas->widget);

	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(w, &xscrolloffset, &yscrolloffset);
	gnome_canvas_scroll_to(GNOME_CANVAS(arrange->canvas->widget), xscrolloffset + px, yscrolloffset);
}


void
gcanvas_scrollregion_update (Arrange* arrange)
{
	// Update the scrollbars after the canvas size has changed, eg after a zoom.

	if(!CANVAS_IS_GNOME) return;
	g_return_if_fail(arrange->canvas->widget);

	double x1, cwidth, y1, y2;
	gnome_canvas_get_scroll_region(GNOME_CANVAS(arrange->canvas->widget), &x1, &y1, &cwidth, &y2);

	cwidth = arr_canvas_width(arrange);
	double cheight = arr_canvas_height(arrange);

	if (y2 == 0)     perr ("cannot get canvas scroll region height.");
	if (cwidth == 0) perr ("cannot get canvas scroll region width.");
	else {
		gnome_canvas_set_scroll_region (GNOME_CANVAS(arrange->canvas->widget),
		                0.0, 0.0,
		                (double)cwidth, (double)(cheight));
#if 0
		if(arrange->rulerbar)
			gnome_canvas_set_scroll_region (GNOME_CANVAS(arrange->rulerbar->cwidget),
		                0.0, 0.0,
		                (double)cwidth, (double)30.0);
#endif
		if(arrange->songmap)
			gnome_canvas_set_scroll_region (GNOME_CANVAS(arrange->songmap->canvas),
		                0.0, 0.0,
		                (double)cwidth, (double)30.0);
	}

	gcanvas_background_update(arrange);
}


static void
gcanvas_get_scroll_offsets(Arrange* arrange, int* x_offset, int* y_offset)
{
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), x_offset, y_offset);
}


static void
gcanvas_px_to_model(Arrange* arrange, Pti px, AyyiSongPos* pos, ArrTrk** track)
{
	int x, y;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &x, &y);

	arr_snap_px2spos(arrange, pos, px.x + x);

	if(track){
		TrackDispNum d = arr_px2trk(arrange, px.y + y);
		if(d > -1 && d < AM_MAX_TRKX) *track = arrange->priv->tracks->display[d];
	}
}


static uint32_t
gcanvas_get_viewport_left(Arrange* arrange)
{
	int xscrolloffset, yscrolloffset;
	gnome_canvas_get_scroll_offsets(GNOME_CANVAS(arrange->canvas->widget), &xscrolloffset, &yscrolloffset);
	return arr_px2samples(arrange, xscrolloffset);
}


static void
gcanvas_get_view_fraction(Arrange* arrange, double* fx, double* fy)
{
	// get the fraction of the whole canvas that is currently visible in the window

	// whole canvas
	double x1=0, y1=0, x2=0.0, y2=0;
	gnome_canvas_get_scroll_region(GNOME_CANVAS(arrange->canvas->widget), &x1,&y1,&x2,&y2);

	// currently visible
	double visible_x = arrange->canvas_scrollwin->allocation.width;
	double visible_y = arrange->canvas_scrollwin->allocation.height;

	// fraction
	if(fx) *fx = visible_x / (x2 - x1);
	if(fy) *fy = visible_y / (y2 - y1);
}


void
gcanvas_zorder_update(Arrange* arrange)
{
	// Update the z-order of all Arrange canvas items.

	// note: this is a big hack. The z-index of the canvas items is not exposed.
	//       It is currently unknown whether this method imposes any performance penalty.

	if(arrange->canvas->type != 1) return;
	g_canvas* g = arrange->canvas->gnome;

	// xhairs should be in front of all parts except current selection
	gnome_canvas_item_raise_to_top(g->xhair);
	gnome_canvas_item_raise_to_top(g->yhair);

	// bring selected parts to the front
	GList* p = arrange->part_selection;
	for(;p;p=p->next){
		AMPart* gpart = p->data;
		GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, gpart);
		if(item) gnome_canvas_item_raise_to_top(item);
	}

	// automation should be in front of parts
	if(arrange->view_options[SHOW_AUTOMATION].value){
		int t; for(t=0;t<AM_MAX_TRK;t++){
			if(!song->tracks->track[t]) continue;
			if(!track_list__track_by_index(arrange->priv->tracks, t)) continue;
			GnomeCanvasItem* item = GNOME_CANVAS_ITEM(track_list__track_by_index(arrange->priv->tracks, t)->grp_auto);
			if(item) gnome_canvas_item_raise_to_top(item);
		}
	}

	// spp is on top
	gnome_canvas_item_raise_to_top(arrange->canvas->gnome->spp);
	gnome_canvas_item_raise_to_top(arrange->canvas->gnome->spp2);
}


void
gcanvas_background_update(Arrange* arrange)
{
	// make sure that background arrange canvas items are the correct size.


	arrange_verify(arrange);
	g_canvas* g = arrange->canvas->gnome;

	// change length of the track divider lines:
	int t;
	for(t=0;t<=am_track_list_count(song->tracks);t++){
		if(g->tline[t]) gnome_canvas_item_set(g->tline[t], "x2", arr_canvas_width(arrange), NULL);
	}

  //canvas background:
#ifdef SHOW_ACTIVE_AREA
	gnome_canvas_item_set(arrange->canvas->bg,
		"x2", arrange->canvas_width,
		"y2", arr_height(arrange),
		NULL);
#else
	gnome_canvas_item_set(g->bg,
		"x2", (double)MAX(arrange->canvas_scrollwin->allocation.width, arr_canvas_width(arrange)),
		"y2", (double)MAX(arrange->canvas_scrollwin->allocation.height, arr_canvas_height(arrange)),
		NULL);
#endif
}


static void
gcanvas_set_from_menu(Arrange* arrange)
{
	arr_set_canvas(arrange, gcanvas_get_type());
}


void
gcanvas_part_local_draw(GnomeCanvasGroup* group, LocalPart* part)
{
	// Draws a *local* part, eg during a drag operation.
	// Data must be pre-initialised.

	g_return_if_fail(part);
	GCanvasLocalPart* part_ = (GCanvasLocalPart*)part;

	Arrange* arrange = part->arrange;

	g_return_if_fail(part->part.track);
	TrackNum tnum = am_track_list_position(song->tracks, part->part.track);
	g_return_if_fail(!(tnum < 0) || (tnum >= AM_MAX_TRK));

	// set the part start
	GnomeCanvasGroup* pgrp = GNOME_CANVAS_GROUP(gnome_canvas_item_new(group, gnome_canvas_group_get_type(),
  			"x", (double)arr_spos2px(arrange, &part->part.start),
  			"y", (double)(arrange->canvas->gnome->track_pos[tnum]),
  			NULL));
	part_->canvasgroup = GNOME_CANVAS_ITEM(pgrp);
	dbg (2, "x=%.1f", (double)arr_spos2px(arrange, &part->part.start));

	int c_idx;
	int fill_rgba=0, outline_rgba=0;
	double width_pixels;
	if (part->ghost) {
		c_idx = 56;
		fill_rgba    = (song->palette[c_idx] & 0xffffff00) + 100; // semi transparent
		outline_rgba = (song->palette[59   ] & 0xffffff00) + 100;
		width_pixels = 2.0;
	} else {
		c_idx        = part->part.bg_colour;
		fill_rgba    = song->palette[c_idx];
		outline_rgba = song->palette[63   ]; // opaque white
		width_pixels = 1.0;
	}

	ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, tnum);
	g_return_if_fail(atr);
	int track_height =  atr->height;
	if (track_height < 2) { dbg(0, "track_height too small!"); return; }

	part_->box = gnome_canvas_item_new(pgrp, gnome_canvas_rect_get_type(),
		"outline_color_rgba", outline_rgba,
		"fill-color-rgba", fill_rgba,//format eg: 0x202020ff,
		"x1", 0.0, "y1", PART_MARGIN_Y1,
		"x2", (double)MAX(arr_spos2px(arrange, &part->part.length), MIN_PART_WIDTH),
		"y2", (double)track_height * vzoom(arrange) - PART_OFFSET_Y2,
		"width_units", width_pixels,
		NULL
	);
}


static void
arr_cyc_fill_update(Arrange* arrange)
{
  // Update the arrange canvas cycle fill indication following a locator change.

  double left  = arr_spos2px(arrange, &song->loc[1].vals[0].val.sp);
  double right = arr_spos2px(arrange, &song->loc[2].vals[0].val.sp);

  g_canvas* g = arrange->canvas->gnome;
  gnome_canvas_item_set(g->cyc_fill,
      "x1", left,
      "x2", right,
      "y2", (double)arrange->canvas_scrollwin->allocation.height + 1000.0,
	  NULL);
}


static void
arr_on_note_selection_change(GObject* am_song, AyyiPanel* sender, GList* items, gpointer user_data)
{
	// As this is a Selection observation, we only update if the panel is Linked.
	// To determine this, Sender must be supplied.

	g_return_if_fail(sender);
	if(!items){ pwarn("no parts."); return; }

	NoteSelectionListItem* item = items->data; //only 1st item currently supported.
	MidiPart* gpart = item->part;

	arrange_foreach {
		if(arrange == (Arrange*)sender){
			GnomeCanvasPart* part = gcanvas_get_canvaspart_by_part(arrange, (AMPart*)gpart);
			gnome_canvas_part_set_edit_selection(part, gpart->note_selection);
		}

		if(AYYI_IS_ARRANGE(sender)){
		}
	
	} end_arrange_foreach
}


static void
arr_on_parts_change (GObject* am_song, Arrange* arrange)
{
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);

	if(!(cc->provides & PROVIDES_OVERVIEW)){
		arr_song_overview_queue_for_update();
	}
}


static gboolean
canvas_on_leave (GtkWidget* widget, GdkEventCrossing* event, Arrange* arrange)
{
#ifndef DEBUG_DISABLE_RULERBAR
	if(arrange->ruler) time_ruler_set_pos((TimeRuler*)arrange->ruler, -1);
#endif
	return NOT_HANDLED;
}
