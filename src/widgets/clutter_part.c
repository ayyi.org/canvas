/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2007-2012 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#define __clutter_part_c__
#include "global.h"

#include <GL/gl.h>

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <clutter/clutter.h>
#include <math.h>
#include <clutter-gtk/clutter-gtk.h>

#include "waveform/waveform.h"
#include "model/song.h"
#include "model/am_part.h"
#include "model/am_track.h"
#include "model/am_pool_item.h"
#include "widgets/clutter-behaviour-size.h"
#include "pool_model.h"
#include "windows.h"
#include "panels/arrange.h"
#include "panels/arrange/clutter.h"
#include "panels/arrange/part.h"
#include "support.h"
#include "clutter_utils.h"

#include "clutter_part.h"

extern SMConfig* config;

#define PEAKS_PER_BLOCK 256 //defines the texture size
#define CLUTTER_PART_BORDER_WIDTH 1.0
#undef ENABLE_ANIMATION
#define TEXTURE_8BIT

G_DEFINE_TYPE (ClutterPart, clutter_part, CLUTTER_TYPE_GROUP);

#define CLUTTER_PART_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), CLUTTER_TYPE_PART, ClutterPartPrivate))

struct _ClutterPartPrivate
{
	ClutterActor*    label;
#ifdef ENABLE_ANIMATION
	ClutterTimeline* timeline;
	ClutterTimeline* size_timeline;
#endif

	ClutterColor     border_colour;
	ClutterColor     border_colour_selected;
};

enum
{
	PROP_0,
	PROP_LIBRARY
};

#define N_COLS 3

static ClutterGroupClass* parent_class = NULL;

static void clutter_part_allocate (ClutterActor*, const ClutterActorBox*, ClutterAllocationFlags);
static void palette_get_clutter   (ClutterColor*, int idx);
static void colour_rgba_to_clutter(ClutterColor*, uint32_t rgba);

/* GObject Stuff */

static void
clutter_part_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	g_return_if_fail (CLUTTER_IS_PART (object));
	//ClutterPartPrivate* priv = CLUTTER_PART_GET_PRIVATE(object);

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
clutter_part_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	
	g_return_if_fail (CLUTTER_IS_PART (object));

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	} 
}

#if 0
static void
clutter_part_paint (ClutterActor *actor)
{
	ClutterPartPrivate* priv;

	ClutterPart *set = CLUTTER_PART(actor);

	priv = CLUTTER_PART_GET_PRIVATE(set);

	glPushMatrix();
	
	gint i;
	gint len = clutter_group_get_n_children (CLUTTER_GROUP (actor)); 
	for (i = 0; i < len; i++) {
		ClutterActor* child;

		child = clutter_group_get_nth_child (CLUTTER_GROUP(actor), i);
		if (child) {
			clutter_actor_paint (child);
		}
	}

	glPopMatrix();
}
#endif

static void 
clutter_part_dispose (GObject *object)
{
	ClutterPart* self = CLUTTER_PART(object);
#ifdef ENABLE_ANIMATION
	ClutterPartPrivate* priv = self->priv;
#endif

	if(self->waveform) clutter_actor_destroy(self->waveform);
	self->waveform = NULL;

	if(self->border) clutter_actor_destroy((ClutterActor*)self->border);
	self->border = NULL;

#ifdef ENABLE_ANIMATION
	if(priv->timeline) g_object_unref(self->timeline);
	priv->timeline = NULL;
#endif

	G_OBJECT_CLASS (clutter_part_parent_class)->dispose (object);
}

static void 
clutter_part_finalize (GObject *object)
{
	G_OBJECT_CLASS (clutter_part_parent_class)->finalize (object);
}

static void
clutter_part_class_init (ClutterPartClass *klass)
{
	GObjectClass        *gobject_class = G_OBJECT_CLASS (klass);
	ClutterActorClass   *actor_class = CLUTTER_ACTOR_CLASS (klass);
	
	parent_class = CLUTTER_GROUP_CLASS (klass);

	//actor_class->paint           = clutter_part_paint;
	actor_class->allocate       = clutter_part_allocate;

	gobject_class->finalize     = clutter_part_finalize;
	gobject_class->dispose      = clutter_part_dispose;
	gobject_class->get_property = clutter_part_get_property;
	gobject_class->set_property = clutter_part_set_property;	

	g_type_class_add_private (gobject_class, sizeof (ClutterPartPrivate));
}

static void
clutter_part_init (ClutterPart *self)
{
	ClutterPartPrivate* _self = CLUTTER_PART_GET_PRIVATE (self);
	
	//priv->active_set = 0;
	//priv->active_col = 0;

	//ClutterColor c;
	colour_rgba_to_clutter(&_self->border_colour, config->part_outline_colour);
	//ClutterColor colour = { 255, 0, 0x80, 255 };
	//priv->border_colour = colour;
	ClutterColor colour_selected = { 0xff, 0xff, 0xff, 0xff };
	_self->border_colour_selected = colour_selected;
}


ClutterActor*
clutter_part_new (Arrange* arrange, AMPart* part)
{
	ClutterGroup* group = g_object_new(CLUTTER_TYPE_PART, NULL);
	ClutterPart* clutter_part = CLUTTER_PART(group);
	ClutterPartPrivate* priv = CLUTTER_PART_GET_PRIVATE(group);
	clutter_part->arrange = arrange;
	clutter_part->part = part;

	clutter_actor_set_name(CLUTTER_ACTOR(group), "part group");

	TrackDispNum d = arr_t_to_d(arrange, part->track->track_num);
	int part_length = arr_spos2px(arrange, &part->length);
	int part_height = arrange->track_pos[d + 1] - arrange->track_pos[d];
	gint part_x = arr_pos2px(arrange, &part->start);
	gint y = arrange->track_pos[d]/* + arrange->ruler_height*/;

	ClutterColor transparent = { 0, 0, 0, 0x0 };
	ClutterActor* actor = clutter_rectangle_new_with_color(&transparent);
	clutter_part->border = CLUTTER_RECTANGLE(actor);
	clutter_actor_set_size(actor, part_length, part_height -1);
	clutter_actor_set_position(actor, 0, 0);
#ifdef TEXTURE_8BIT
	ClutterColor colour;
	palette_get_clutter(&colour, part->bg_colour);
	clutter_rectangle_set_color(clutter_part->border, &colour);
#endif
	clutter_rectangle_set_border_color(clutter_part->border, &priv->border_colour);
	clutter_rectangle_set_border_width(clutter_part->border, 1);
	clutter_group_add(group, actor);
	dbg(2, "d=%i pos=%i x %i size=%i x %i", d, part_x, y, part_length, part_height);

	if(SHOW_PARTCONTENTS){
		PoolItem* pi = part->pool_item;
		if(!pi->user_data) pi->user_data = g_new0(GuiPoolItem, 1);
		GuiPoolItem* gpi = pi->user_data;
		/*
		GlBlocks* blocks;
		if(pi->gl_blocks){
			blocks = pi->gl_blocks;
		}else{
			blocks = pi->gl_blocks = g_new0(GlBlocks, 1);
		}
		int num_blocks    = part->pool_item->num_peaks / PEAKS_PER_BLOCK;
		if(pi->num_peaks % PEAKS_PER_BLOCK) num_blocks++;
		blocks->size      = num_blocks;
		blocks->texture   = g_new0(ClutterActor*, num_blocks);
		dbg(2, "n_blocks=%i", num_blocks);

		int block_wid = arr_samples2px(arrange, PEAK_RATIO * PEAK_TEXTURE_SIZE);

		int i; for(i=0;i<num_blocks;i++){
				if(i == 1) break;
			gboolean is_last = (i == blocks->size - 1);
		*/

		if(!gpi->texture){
			dbg(0, "");
			AlphaBuf* a = wf_alphabuf_new(pi->waveform, -1, 1.0, FALSE, 0);
			if(a){
				//actor = blocks->texture[i] = clutter_texture_new();
				//blocks->texture[i] = gtk_clutter_texture_new_from_pixbuf(pixbuf);

				gpi->texture = cogl_texture_new_from_data(a->width, a->height, COGL_TEXTURE_NONE, COGL_PIXEL_FORMAT_A_8, COGL_PIXEL_FORMAT_ANY, a->width, a->buf);
				g_return_val_if_fail(gpi->texture, NULL);
			}
		}
		if(gpi->texture){
			//blocks->texture[i] = tex;
			actor = clutter_part->waveform = clutter_texture_new();
#ifdef TEXTURE_8BIT
			CoglMaterial* material = cogl_material_new();
			//CoglHandle material = clutter_texture_get_cogl_material(gpi->texture);
			cogl_material_set_layer(material, 0, gpi->texture);
			/*
			float r, g, b;
			colour_get_float(&song->palette, part->bg_colour, &r, &g, &b, 0xff);
			CoglColor* colour = cogl_color_new(); //TODO free
			//CoglColor colour;
			cogl_color_set_from_4f(colour, r, g, b, 1.0);
			dbg(0, "colour=%.2f %.2f %.2f", cogl_color_get_red(colour), cogl_color_get_green(colour), cogl_color_get_blue(colour));
			cogl_material_set_color(material, colour);
			*/
			clutter_texture_set_cogl_material(CLUTTER_TEXTURE(actor), material);
			clutter_actor_set_opacity(actor, 0x7f);
#else
			clutter_texture_set_cogl_texture(CLUTTER_TEXTURE(actor), gpi->texture);
#endif

			/*
			GError** error = NULL;
			gtk_clutter_texture_set_from_pixbuf(CLUTTER_TEXTURE(actor), pixbuf, error);
			if(error) pwarn("texture!");
			*/
			//my_gtk_clutter_texture_set_from_pixbuf(CLUTTER_TEXTURE(actor), pixbuf);

			clutter_texture_set_sync_size(CLUTTER_TEXTURE(actor), false);

			/*
			if (is_last){
				block_wid = part_length - block_wid * (blocks->size - 1); //last block is smaller
				//tex_pct = blocks->last_fraction - 0.05; //note compensation for 1px rounding error
			}
			*/
			//int tile_width = part_length - (is_last ? block_wid * (blocks->size - 1) : 0);
//			int tile_width = is_last ? block_wid * blocks->last_fraction -1 : block_wid;

			//dbg(0, "setting tile: %i x %i", tile_width, part_height -1);
//			clutter_actor_set_size (actor, tile_width, part_height -1);

			gint x = part_x;// + i * block_wid;
//			if(is_last) dbg(2, "%2i: x=%4i w=%3i", i, x, tile_width);

			clutter_actor_set_position(actor, x, y);

			clutter_group_add(group, actor);
		}
		else pwarn("texture not set.");
	}

	clutter_actor_raise_top(CLUTTER_ACTOR(clutter_part->border));
	clutter_actor_raise_top(CLUTTER_ACTOR(actor)); //tmp - may obscure the border?

	char font_string[256];
	get_font_string(font_string, -2);
	ClutterActor* label = priv->label = clutter_text_new_with_text(font_string, part->name);
	clutter_actor_set_position(label, 0, part_height - 10);
	clutter_group_add(group, label);

	void on_text_changed(ClutterText* label, gpointer user_data)
	{
		dbg(0, "text changed.");
	}
	//clutter_text_set_editable((ClutterText*)label, TRUE);
	g_signal_connect(label, "text-changed", (GCallback)on_text_changed, NULL);

#ifdef ENABLE_ANIMATION
	void part_new__on_frame(ClutterTimeline* timeline, gint frame_num, ClutterPart* part)
	{
		double progress = clutter_timeline_get_progress (timeline);
    	clutter_actor_set_opacity ((ClutterActor*)part, MIN(255, progress * 256));
	}

	priv->timeline = clutter_timeline_new (500);
	g_signal_connect(priv->timeline, "new-frame", G_CALLBACK(part_new__on_frame), clutter_part);
	clutter_timeline_start(priv->timeline);

	priv->size_timeline = clutter_timeline_new(150);
#endif

	return CLUTTER_ACTOR(group);
}


void
clutter_part_redraw(ClutterPart* clutter_part, AMChangeType change_type)
{
	//currently this sets sizes directly from the model, so cannot be used with animation. Perhaps refactor?

	ClutterPartPrivate* _self = CLUTTER_PART_GET_PRIVATE(clutter_part);
	Arrange* arrange = clutter_part->arrange;
	ClCanvas* c = arrange->canvas->cl;
	AMPart* gpart = clutter_part->part;
	if(!gpart->pool_item) return; //midi part.
 
	if(change_type & (CHANGE_LEN | CHANGE_HEIGHT | AM_CHANGE_ZOOM_V | AM_CHANGE_ZOOM_H)){
		g_return_if_fail(gpart->track);
		g_return_if_fail(g_list_find(song->parts->list, gpart));
		TrackDispNum d = arr_t_to_d(arrange, gpart->track->track_num);
		gint part_x = arr_cl_pos2px(arrange, &gpart->start);
		float vscale = c->animation_zoom.y / arrange->vzoom;
		gint y = arrange->track_pos[d] * vscale;
		float part_length = arr_cl_cpos2px(arrange, &gpart->length);
		float part_height = (arrange->track_pos[d + 1] - arrange->track_pos[d]) * vscale;
		clutter_actor_set_size(CLUTTER_ACTOR(clutter_part->border), part_length, part_height -1);
		clutter_actor_set_position(CLUTTER_ACTOR(clutter_part), part_x, y);
		if(gpart->ident.idx == 2) dbg(2, "redrawing... length=%.1f part=%i has_clip=%i", part_length, gpart->ident.idx, clutter_actor_has_clip(CLUTTER_ACTOR(clutter_part)));
		clutter_actor_set_clip(CLUTTER_ACTOR(clutter_part), 0.0, 0.0, part_length, part_height - 1);

		if(change_type & (CHANGE_HEIGHT | AM_CHANGE_ZOOM_V)){
			clutter_actor_set_position(_self->label, 0, part_height - 10);
		}

		if(SHOW_PARTCONTENTS){
			ClutterActor* texture = clutter_part->waveform;
			g_return_if_fail(texture);
			//float block_wid   = arr_samples2px(arrange, PEAK_RATIO * PEAK_TEXTURE_SIZE);
			//int tile_width = is_last ? block_wid * blocks->last_fraction -1 : block_wid;
			//gint x = /*part_x +*/ i * block_wid;
//TODO take trim into account.
			clutter_actor_set_size (texture, arr_cl_samples2px(arrange, gpart->pool_item->samplecount), part_height -1);
			//clutter_actor_set_position(texture, CLUTTER_PART_BORDER_WIDTH, CLUTTER_PART_BORDER_WIDTH);
			clutter_actor_set_position(texture, 0, 0);
			#if 0 //unreliable
			clutter_actor_set_clip(texture, 0.0, 0.0, part_length, part_height - 1);
			#endif
		}
	}
	else dbg(0, "ignoring...");
}


void
clutter_part_resize(ClutterPart* self, float x, float height)
{
	//start an animated resize.

	//ClutterActor* actor = CLUTTER_ACTOR(self);
#ifdef ENABLE_ANIMATION
	ClutterPartPrivate* _self = CLUTTER_PART_GET_PRIVATE(self);

	gdouble __alpha_fn(ClutterAlpha *alpha, gpointer user_data)
	{
		return clutter_timeline_get_progress(clutter_alpha_get_timeline(alpha));
	}

	ClutterAlpha* alpha = clutter_alpha_new_with_func(_self->size_timeline, __alpha_fn/*CLUTTER_EASE_OUT_QUAD how to use this enum ?? */, NULL, NULL);
	clutter_timeline_start(_self->size_timeline);

	Ptf start_size;
	clutter_actor_get_size((ClutterActor*)self, &start_size.x, &start_size.y);
	float part_length = arr_pos2px(self->arrange, &self->gpart->length);

	ClutterBehaviour* b = clutter_behaviour_size_new(alpha, start_size.x, start_size.y, part_length, height);
	clutter_behaviour_apply(b, (ClutterActor*)self);
#endif
}


void
clutter_part_select (ClutterPart* self, gboolean selected)
{
	ClutterPartPrivate* _self = CLUTTER_PART_GET_PRIVATE(self);

	ClutterColor* color = selected ? &_self->border_colour_selected: &_self->border_colour;
	clutter_rectangle_set_border_color(self->border, color);
}


static void
clutter_part_allocate(ClutterActor* self, const ClutterActorBox* box, ClutterAllocationFlags flags)
{
	CLUTTER_ACTOR_CLASS (clutter_part_parent_class)->allocate (self, box, flags);

	//dbg(0, "size=%.1f x %.1f", box->x2 - box->x1, box->y2 - box->y1);
	ClutterPart* clutter_part = CLUTTER_PART(self);
	Arrange* arrange = clutter_part->arrange;

	if(SHOW_PARTCONTENTS){
		ClutterActor* texture = clutter_part->waveform;
		g_return_if_fail(texture);

		Ptf size;
		clutter_actor_get_size((ClutterActor*)self, &size.x, &size.y);
		float part_height = box->y2 - box->y1;

		clutter_actor_set_size(texture, arr_samples2px(arrange, clutter_part->part->pool_item->samplecount), part_height);
		clutter_actor_set_clip(texture, 0.0, 0.0, size.x, part_height);
	}
}


static void
palette_get_clutter(ClutterColor* colour, int idx)
{
	colour->red = song->palette.red[idx];
	colour->green = song->palette.grn[idx];
	colour->blue = song->palette.blu[idx];
	colour->alpha = 0xff;
}


static void
colour_rgba_to_clutter(ClutterColor* colour, uint32_t rgba)
{
	g_return_if_fail(colour);

	colour->red   = (rgba & 0xff000000) >> 24;
	colour->green = (rgba & 0x00ff0000) >> 16;
	colour->blue  = (rgba & 0x0000ff00) >> 8;
	colour->alpha = (rgba & 0x000000ff);
}


