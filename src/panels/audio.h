/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __audio_h__
#define __audio_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_AUDIO_WIN            (ayyi_audio_win_get_type ())
#define AYYI_AUDIO_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_AUDIO_WIN, AyyiAudioWin))
#define AYYI_AUDIO_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_AUDIO_WIN, AyyiAudioWinClass))
#define AYYI_IS_AUDIO_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_AUDIO_WIN))
#define AYYI_IS_AUDIO_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_AUDIO_WIN))
#define AYYI_AUDIO_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_AUDIO_WIN, AyyiAudioWinClass))

typedef struct _AyyiAudioWinClass AyyiAudioWinClass;

struct _AyyiAudioWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiAudioWin
{
	AyyiPanel     panel;
#ifdef USE_VIEW
	WaveformView* waveform;
#else
	WaveformViewPlus* waveform;
#endif
};


void       audio_win_free   (AyyiPanel*);

G_END_DECLS

#endif
