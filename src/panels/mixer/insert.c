/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "model/channel.h"
#include "model/track.h"
#include "model/song.h"
#include "model/plugin.h"
#include "icon.h"
#include "support.h"
#include "src/meter.h"
#include "io.h"
#include "song.h"
#include "widgets/fader_label.h"
#include "widgets/rotary.h"
#include "panels/mixer.h"
#include "panels/mixer/insert.h"

static void     plugin_on_combobox_change    (GtkComboBox*, gpointer);
static void     pluginbox_item_set_sensitive (GtkCellLayout*, GtkCellRenderer*, GtkTreeModel*, GtkTreeIter*, StripInsert*);
static bool     pluginbox_on_key_press       (GtkWidget*, GdkEventKey*, gpointer);

static void     insert_free                  (gpointer);

static gboolean is_separator                 (GtkTreeModel*, GtkTreeIter*, gpointer);


/*
 *  The address of the insert may change. Only access it via the array index.
 */
StripInsert*
insert_new (Strip* strip)
{
	if (!strip->inserts) {
		dbg (2, "init...");
		strip->inserts = g_ptr_array_sized_new(1);
		g_ptr_array_set_free_func(strip->inserts, insert_free);
	}

	StripInsert* new_insert = AYYI_NEW(StripInsert,
		.strip = strip,
		.slot = strip->inserts->len,
	);
	new_insert->combobox = pluginbox_new(strip, new_insert);

	g_ptr_array_add(strip->inserts, new_insert);
	dbg (2, "array length is now: %i", strip->inserts->len);

	if (new_insert->combobox) gtk_box_reorder_child(GTK_BOX(strip->box), new_insert->combobox, 2 + strip->inserts->len);

	return new_insert;
}


void
insert_free (gpointer insert)
{
	g_free(insert);
}


void
insert_hide (StripInsert* insert)
{
	gtk_widget_hide(insert->combobox);
}


GtkWidget*
pluginbox_new (Strip* s, StripInsert* insert)
{
	dbg(2, "insert=%p strip=%p", insert, insert->strip);
	int plugin_slot = s->inserts->len -1; //assume we are appending to the end.

#ifdef OLD
	GtkWidget* ins = gtk_combo_box_entry_new_text();
	GtkComboBox* combo = GTK_COMBO_BOX(ins);

	gtk_combo_box_append_text(combo, NO_PLUGIN); //empty entry at top of list to disable plugin.
	gtk_combo_box_append_text(combo, "bypass");

	if (!song->plugins) dbg (2, "pluglist empty!");

	GList* l = song->plugins;
	for (;l;l=l->next) {
		AMPlugin* plug = l->data;
		gtk_combo_box_append_text(combo, plug->name);

		plugin_shared* plugin_shared = core_plugin_get(plug->shm_num);
		if (plugin_shared) {
			dbg (1, "category=%s", plugin_shared->category); //category = VST, etc
		}
	}

#else
	GtkTreeModel* model = s->channel ? ((ChannelUserData*)s->channel->user_data)->plugin_list : NULL;

	GtkWidget* ins = insert->combobox = model ? gtk_combo_box_new_with_model (model) : gtk_combo_box_new();
	GtkComboBox* combo = GTK_COMBO_BOX(ins);

	GtkCellRenderer* renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "pixbuf", 0, NULL);
	gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (combo), renderer, (GtkCellLayoutDataFunc)pluginbox_item_set_sensitive, insert, NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "text", 1, NULL);
	gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (combo), renderer, (GtkCellLayoutDataFunc)pluginbox_item_set_sensitive, insert, NULL);
	gtk_combo_box_set_row_separator_func (GTK_COMBO_BOX (combo), is_separator, NULL, NULL);

	if(model) pluginbox_set_selection(s, insert);
#endif

#ifdef SET_HEIGHT
	// this tweak is disabled as it doesnt work with clearlooks.
	GValue gval = {0,};
	g_value_init(&gval, G_TYPE_CHAR);
	g_value_set_char(&gval, 17); // sets the height of the arrow box, *not* the entry box.
	g_object_set_property(G_OBJECT(ins1), "height-request", &gval);
#endif

	gtk_widget_show(ins);
	gtk_box_pack_start(GTK_BOX(s->box), ins, FALSE, FALSE, 0);

	gpointer sig_data = GUINT_TO_POINTER(s->strip_num << 8 | plugin_slot);
	insert->changed_handler = g_signal_connect(ins, "changed", G_CALLBACK(plugin_on_combobox_change), sig_data);
	g_signal_connect(G_OBJECT(ins), "key-press-event", G_CALLBACK(pluginbox_on_key_press), sig_data);

#if 0
	GtkWidget* text_box = gtk_bin_get_child(GTK_BIN(ins));
	if(text_box) g_signal_connect(G_OBJECT(text_box), "activate", G_CALLBACK(pluginbox_on_activate), sig_data);
#endif

	g_object_set_data(G_OBJECT(ins), "insert", insert);

	return ins;
}


void
pluginbox_set_selection (Strip* s, StripInsert* insert)
{
	AyyiChannel* ayyi_trk = ayyi_mixer__channel_at_quiet(s->channel->ident.idx);
	g_return_if_fail(ayyi_trk);
	AyyiIdx plugin_num = ayyi_trk->plugin[insert->slot].idx;
	dbg(2, "plug_num=%i", plugin_num);

	GtkTreeModel* model = gtk_combo_box_get_model(GTK_COMBO_BOX(insert->combobox));
	if (!model) gtk_combo_box_set_model(GTK_COMBO_BOX(insert->combobox), model = ((ChannelUserData*)s->channel->user_data)->plugin_list);

	if(plugin_num) plugin_num += INSERT_BOX_LIST_OFFSET + 1;//+1 is for separator
	GtkTreePath* path = gtk_tree_path_new_from_indices(plugin_num, -1);
	if (path) {
		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path)) {
			gchar* plugin_name;
			gtk_tree_model_get(model, &iter, 1, &plugin_name, -1);
			dbg(2, "plug=%s", plugin_name);
			g_strlcpy(insert->previous, plugin_name, 256);
			g_free(plugin_name);

			gtk_combo_box_set_active_iter(GTK_COMBO_BOX(insert->combobox), &iter);
		}

		gtk_tree_path_free(path);
	}
}


void
pluginbox_show_bypassed (Strip* strip, int slot, gboolean bypassed)
{
}


static void
plugin_on_combobox_change (GtkComboBox* combo_box, gpointer data)
{
	// we get here whenever someone types into the box. How do we know if editing finished? check focus.

	// @param user_data - first 2 bytes are plugin slot, 2nd are channel number.
	PF;

	StripInsert* insert = g_object_get_data(G_OBJECT(combo_box), "insert");
	g_return_if_fail(insert);

	char* previous = insert->previous;

	int plugin_slot = GPOINTER_TO_INT(data) & 0xff;
	dbg(1, "plugin_slot=%i", plugin_slot);
	AyyiMixerStrip* strip = (AyyiMixerStrip*)gtk_widget_get_parent(gtk_widget_get_parent(gtk_widget_get_parent((GtkWidget*)combo_box)));
	g_return_if_fail(G_OBJECT_TYPE(strip) == AYYI_TYPE_MIXER_STRIP);
	g_return_if_fail(strip && strip->channel);

	GtkWidget* toplevel = gtk_widget_get_toplevel(GTK_WIDGET(combo_box));
	if (GTK_IS_WINDOW(toplevel)) {
		dbg (1, "toplevel IS a window");
	}
	else dbg (1, "toplevel is NOT a window");
	//GtkWidget* text_box = gtk_bin_get_child(GTK_BIN(combo_box));

	if (GTK_IS_COMBO_BOX(combo_box)) dbg (2, "is combobox."); else dbg (0, "is NOT combobox.");

#ifdef OLD
	char* plugin_name = gtk_combo_box_get_active_text(combo_box);

#else
	gint row = gtk_combo_box_get_active (combo_box);
	GtkTreePath* path = gtk_tree_path_new_from_indices (row, -1);
	//gtk_cell_view_set_displayed_row (cell, path);
	gchar* plugin_name = NULL;
	GtkTreeIter iter;
	GtkTreeModel* model = gtk_combo_box_get_model (combo_box);
	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gtk_tree_model_get(model, &iter, 1, &plugin_name, -1);
	}
	gtk_tree_path_free (path);
#endif

	if (!plugin_name) return;
	gchar* a = plugin_name;
	if (!strcmp(plugin_name, NO_PLUGIN)){
		plugin_name[0] = '\0';                 //send an empty string to indicate NO_PLUGIN.

	} else {

		if (!strcmp(plugin_name, "bypass")){
			dbg (0, "FIXME bypass");
			//pluginbox_show_bypassed(ch, 0, TRUE);
			return;
		}

		if (!strcmp(plugin_name, "Edit")){
			dbg (0, "FIXME - Edit: show plugin window, and dont change selection!");
			g_signal_handler_block(combo_box, insert->changed_handler);
			pluginbox_set_selection(strip, insert); //restore the original combobox selection
			g_signal_handler_unblock(combo_box, insert->changed_handler);
			dbg(0, "previous=%s", previous);
			a = previous;
		}else{
			dbg (0, "text=%s", plugin_name);
			if (!am_plugin__name_valid(plugin_name)) return;
		}
	}

#if 0
	if (!strcmp(plugin_name, previous)) return;
#endif

	dbg(1, "setting plugin: '%s'", a);
	am_plugin__change(strip->channel, plugin_slot, a, NULL, NULL);

	g_strlcpy(previous, a, 256);

	g_free(plugin_name);

	return;
}


static void
pluginbox_item_set_sensitive (GtkCellLayout* cell_layout, GtkCellRenderer* cell, GtkTreeModel* tree_model, GtkTreeIter* iter, StripInsert* insert)
{
	g_return_if_fail(insert && insert->strip);

	gchar* name;
	gtk_tree_model_get(tree_model, iter, 1, &name, -1);

	if(!strcmp(name, "Edit")){
		g_object_set (cell, "sensitive", (gtk_combo_box_get_active(GTK_COMBO_BOX(insert->combobox)) > 0), NULL);
	}else{
		AMPlugin* plugin = am_plugin__get_by_name(name);
		if(plugin){
#if 0
			Channel* channel = insert->strip->channel;
			AyyiPlugin* ayyi_plugin = ayyi_plugin_at(plugin->shm_num);

			gboolean sensitive = (ayyi_plugin->n_inputs == channel->nchans) && (ayyi_plugin->n_outputs == channel->nchans);
#else
			bool sensitive = true;
#endif

			g_object_set (cell, "sensitive", sensitive, NULL);
		}
	}

	g_free(name);
}


static bool
pluginbox_on_key_press (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
	PF;

#if 0
	if( (event->type == GDK_KEY_PRESS) && (event->keyval == 0xFF0D/*GDK_KP_Return*/ ) ){
		return TRUE;
	}
	if(event->type == GDK_KEY_PRESS){
		switch(event->keyval){
		case GDK_Escape:
		printf("%s(): ESCAPE\n", __func__);
		gtk_window_set_focus(GTK_WINDOW(gtk_widget_get_toplevel(widget)), NULL); //FIXME change this so that the edit is aborted.
		break;
		}
	}
#endif
	return NOT_HANDLED;
}


static gboolean
is_separator (GtkTreeModel* model, GtkTreeIter* iter, gpointer data)
{

  GtkTreePath* path = gtk_tree_model_get_path (model, iter);
  gboolean result = gtk_tree_path_get_indices (path)[0] == INSERT_BOX_LIST_OFFSET;
  gtk_tree_path_free (path);

  return result;
}


