/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2007-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
/*
 * Modified by the GTK+ Team and others 1998-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "global.h"
#include "model/time.h"
#include "model/transport.h"
#include "widgets/gtkmaskedentry.h"
#include "windows.h"
#include "support.h"

#include "transport_locators_box.h"

extern SMConfig* config;

static void     locator_display_new                  (GtkWidget*, int loc_num);
static void     transport_locators_box_size_request  (GtkWidget*, GtkRequisition*);
static void     transport_locators_box_size_allocate (GtkWidget*, GtkAllocation*);
static void     transport_locators_box_on_realise    (GtkWidget*);
static gboolean transport_locators_box_on_focus_in   (GtkWidget*, GdkEventFocus*, TransportLocatorsBox*);
static gboolean transport_locators_box_on_focus_out  (GtkWidget*, GdkEventFocus*, TransportLocatorsBox*);
static void     transport_locators_box_on_return     (GtkWidget*, TransportLocatorsBox*);


G_DEFINE_TYPE (TransportLocatorsBox, transport_locators_box, GTK_TYPE_BOX)

static void
transport_locators_box_class_init (TransportLocatorsBoxClass *class)
{
  GtkWidgetClass* widget_class = (GtkWidgetClass*) class;

  widget_class->size_request = transport_locators_box_size_request;
  widget_class->size_allocate = transport_locators_box_size_allocate;
  widget_class->realize = transport_locators_box_on_realise;
}


static void
transport_locators_box_init(TransportLocatorsBox *box)
{
	box->first_realise = true;
}


GtkWidget*
transport_locators_box_new(TransportWin* window)
{
  TransportLocatorsBox* box = g_object_new(TYPE_TRANSPORT_LOCATORS_BOX, NULL);

  box->redraw = transport_locators_box_redraw;

  transport_locators_box_make_widgets(box);

  return GTK_WIDGET(box);
}


void
transport_locators_box_make_widgets(TransportLocatorsBox* box)
{
  // set locators background colour
  box->locators_bg = gtk_event_box_new();
  gtk_container_set_border_width(GTK_CONTAINER(box->locators_bg), 2);

  int i; for(i=0;i<5;i++) locator_display_new(GTK_WIDGET(box), i);

  gtk_widget_show(GTK_WIDGET(box));
  gtk_container_set_border_width(GTK_CONTAINER(box), 4);
  gtk_box_set_spacing(GTK_BOX(box), 4);
  gtk_container_add(GTK_CONTAINER(box->locators_bg), GTK_WIDGET(box));
}


void
transport_locators_box_redraw (TransportLocatorsBox* tc)
{
}


void
transport_locators_box_on_style_change(TransportLocatorsBox* box)
{
  PF;
  //GdkColor colour;
  //widget_get_bg_colour(GTK_WIDGET(box), &colour, GTK_STATE_NORMAL);
}


static void
transport_locators_box_on_realise(GtkWidget* widget)
{
  TransportLocatorsBox* box = TRANSPORT_LOCATORS_BOX(widget);

  ((GtkWidgetClass*)transport_locators_box_parent_class)->realize(widget);

  if(!box->first_realise) return; //FIXME needs to be reset following redock?

  GtkWidget* bg = box->locators_bg;
  GtkStyle* style = gtk_style_copy(gtk_widget_get_style(bg));
  GdkColor bg_color = {0, 0, 0};
  style->bg[GTK_STATE_NORMAL] = bg_color;
  gtk_widget_set_style(bg, style);
  g_object_unref(style);

  style = gtk_style_copy(gtk_widget_get_style(box->text_view[0]));
  GdkColor fg_color = style->base[GTK_STATE_NORMAL];
  style->base[GTK_STATE_NORMAL] = style->text[GTK_STATE_NORMAL];
  style->text[GTK_STATE_NORMAL] = fg_color;
  int i; for(i=0;i<5;i++){
    gtk_widget_set_style(box->text_view[i], style);
  }
  g_object_unref(style);

#ifdef LOC_LBL_LEFT
  style = gtk_style_copy(gtk_widget_get_style(box->label_bg[0]));
  style->bg[GTK_STATE_NORMAL] = style->bg[GTK_STATE_SELECTED];
  for(i=0;i<5;i++){
    gtk_widget_set_style(box->label_bg[i], style);
  }
  g_object_unref(style);
#endif

  box->first_realise = FALSE;
}


static gboolean
transport_locators_box_on_focus_in(GtkWidget *entry, GdkEventFocus *event, TransportLocatorsBox *box)
{
  gtk_window_remove_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(entry)), app->global_accel_group);
  return FALSE;
}


static gboolean
transport_locators_box_on_focus_out(GtkWidget *entry, GdkEventFocus *event, TransportLocatorsBox *box)
{
  // locator numbers: ayyi and g_object-data are 1-indexed, but transport box array is 0-indexed.

  g_return_val_if_fail(GTK_IS_ENTRY(entry), FALSE);
  int loc_num = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entry), "loc_num")) - 1;

  const char* old_text = box->text[loc_num];
  const char* new_text = gtk_entry_get_text(GTK_ENTRY(entry));
  if(!strlen(old_text) && (loc_num < 2)){ perr("old_text not set!"); return FALSE; }
#ifndef NO_LOC_MASK
  // gtk_masked_entry returns the complete string including the mask.
  new_text += 4;
#endif
  if(strcmp(new_text, old_text)){
    dbg (1, "changed! loc_num=%i text: '%s' '%s'", loc_num, old_text, new_text);
    GPos pos;
    if(bbst2pos(new_text, &pos)){
      AyyiSongPos spos;
      am_transport_set_locator_pos(loc_num + 1, songpos_gui2ayyi(&spos, &pos));
      strcpy(box->text[loc_num], new_text);
    }
  }

  gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(entry)), app->global_accel_group);

  return FALSE;
}


static void
transport_locators_box_on_return(GtkWidget *entry, TransportLocatorsBox *box)
{
  PF;
}


static void
locator_display_new(GtkWidget *widget, int loc_num)
{
  // create view of a single locator

#define LOC_LBL_LEFT
#define MASKED_ENTRY
#ifdef MASKED_ENTRY
  #undef LOC_LBL_LEFT
#endif
  TransportLocatorsBox *box = TRANSPORT_LOCATORS_BOX(widget);
  if(loc_num < 0 || loc_num >= AYYI_MAX_LOC){ perr ("loc_num=%i", loc_num); return; }

  char pango_string[64] = {0,};
  snprintf(pango_string, 63, "%.53s bold %.2i", app->font_family, (app->font_size + 2) & 0xff);
  PangoFontDescription* pangofont = pango_font_description_from_string(pango_string);

#ifdef LOC_LBL_LEFT
  GtkWidget* hbox = box->loc_box[loc_num] = gtk_hbox_new(NON_HOMOGENOUS, 2);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(box), hbox, EXPAND_TRUE, FILL_FALSE, 0);

  box->label_bg[loc_num] = gtk_event_box_new();
  gtk_widget_show(box->label_bg[loc_num]);
  gtk_box_pack_start(GTK_BOX(hbox), box->label_bg[loc_num], EXPAND_TRUE, FILL_FALSE, 0);
#endif

  // label for locator
  char label[64];
#ifdef LOC_LBL_LEFT
  snprintf(label, 63, "<b>  %i  </b>", loc_num + 1);
#else
  snprintf(label, 63, "<small>locate %i</small>", loc_num + 1);
#endif
  GtkWidget* loc_lbl = box->label[loc_num] = gtk_label_new(label);
  gtk_misc_set_alignment(GTK_MISC(loc_lbl), 0,0);
  gtk_label_set_markup(GTK_LABEL(loc_lbl), label);
  gtk_widget_show(loc_lbl);

  // locator
#ifdef MASKED_ENTRY
  char mask[64];
  snprintf(mask, 63, "[%i] 000:00:00:000", loc_num + 1);
  GtkWidget* loc = box->text_view[loc_num] = gtk_masked_entry_new_with_mask(mask);

#elif OLD
  GtkWidget* loc = box->text_view[loc_num] = gtk_text_view_new(); 
  GtkTextBuffer* buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(loc));
  transport_win->loc[loc_num] = buf;
  gtk_text_buffer_set_text(buf, "000:00:00:000", -1);
  gtk_widget_set_name(loc, "transport");
  gtk_text_view_set_pixels_above_lines(GTK_TEXT_VIEW(loc), -2);
  gtk_text_view_set_accepts_tab(GTK_TEXT_VIEW(loc), FALSE);
#else
  GtkWidget* loc = box->text_view[loc_num] = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(loc), 14);
  gtk_entry_set_text(GTK_ENTRY(loc), "000:00:00:000");
#endif

  g_object_set_data(G_OBJECT(loc), "loc_num", GINT_TO_POINTER(loc_num + 1));
  gtk_widget_show(loc);
  gtk_widget_modify_font(loc, pangofont);
#ifdef LOC_LBL_LEFT
  gtk_container_add(GTK_CONTAINER(box->label_bg[loc_num]), loc_lbl);
  gtk_box_pack_start(GTK_BOX(hbox), loc,     EXPAND_FALSE, FILL_FALSE, 0);
#else
  gtk_box_pack_start(GTK_BOX(box), loc_lbl, EXPAND_TRUE, FILL_FALSE, 0);
  gtk_box_pack_start(GTK_BOX(box), loc,     EXPAND_FALSE, FILL_FALSE, 0);
#endif
  g_signal_connect((gpointer)loc, "focus-in-event",  G_CALLBACK(transport_locators_box_on_focus_in), GINT_TO_POINTER(loc_num));
  g_signal_connect((gpointer)loc, "focus-out-event", G_CALLBACK(transport_locators_box_on_focus_out), box);
  g_signal_connect((gpointer)loc, "activate", G_CALLBACK(transport_locators_box_on_return), GINT_TO_POINTER(loc_num));
}


void
transport_locators_box_set_text(GtkWidget *widget, int loc_num, const char* text)
{
  // @param text - a normal bbst string.

  TransportLocatorsBox *box = TRANSPORT_LOCATORS_BOX(widget);
  if(!widget) return;

#if OLD
  gtk_text_buffer_set_text(box->text_view[loc_num], text, -1);
#else
  gtk_entry_set_text(GTK_ENTRY(box->text_view[loc_num]), text);
#endif
  strcpy(box->text[loc_num], text);
}


void
transport_locators_box_update_n_visible(GtkWidget *widget)
{
  if(!GTK_WIDGET_REALIZED(widget)) return;

  TransportLocatorsBox *lbox = TRANSPORT_LOCATORS_BOX(widget);

  GtkWidget*
  get_container(GtkWidget* widget)
  {
    //TODO this doesnt really do anything useful - refactor.

    //TransportLocatorsBox is a container!
    int i = 0;
    GtkWidget* container = gtk_widget_get_parent(gtk_widget_get_parent(lbox->locators_bg));
    while(!GTK_IS_CONTAINER(container)){
      container = gtk_widget_get_parent(container);
      if(i++ > 10){ printf("!!\n"); break; }
    }
    return container;
  }

  gint window_height = get_container(widget)->allocation.height;

#ifdef MASKED_ENTRY
  gint child_req = lbox->text_view[0]->requisition.height;
#else
  GtkBox *box = GTK_BOX (widget);

  GList *children = box->children;
  GtkBoxChild *child;
  gint child_req = 30;
  if (children) {
    child = children->data;
    if(child) child_req = GTK_WIDGET(child->widget)->requisition.height;
  }
#endif
  gint n_visible = window_height / child_req;
  dbg(2, "container_height=%i child_req=%i n_visible=%i entry=%i", window_height, child_req, n_visible, lbox->text_view[0]->requisition.height);

  int i; for(i=0;i<5;i++){
#ifdef LOC_LBL_LEFT
    if(i < n_visible) gtk_widget_show(lbox->loc_box[i]); else gtk_widget_hide(lbox->loc_box[i]);
#else
    if(i < n_visible) gtk_widget_show(lbox->text_view[i]); else gtk_widget_hide(lbox->text_view[i]);
#endif
  }
}


static void
transport_locators_box_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
  requisition->width = 0;
  requisition->height = 0;

  // if docked, we adapt to container size and take whatever we are allocated.
  if(widget_is_in_paned(widget)){
    requisition->height = 10;
    dbg(1, "req.height=%i", requisition->height);
  }

  GtkBoxChild *child;
  GtkRequisition child_requisition;
  gint height;

  GtkBox *box = GTK_BOX (widget);
  gint nvis_children = 0;

  GList *children = box->children;
  while (children) {
    child = children->data;
    children = children->next;

    if (GTK_WIDGET_VISIBLE (child->widget)) {
      gtk_widget_size_request (child->widget, &child_requisition);

      if (box->homogeneous) {
        height = child_requisition.height + child->padding * 2;
        requisition->height = MAX (requisition->height, height);
      }
      else {
        requisition->height += child_requisition.height + child->padding * 2;
      }

      requisition->width = MAX (requisition->width, child_requisition.width);

      nvis_children += 1;
    }
  }

  if (nvis_children > 0) {
    if (box->homogeneous) requisition->height *= nvis_children;
    requisition->height += (nvis_children - 1) * box->spacing;
  }

  requisition->width += GTK_CONTAINER (box)->border_width * 2;
  requisition->height += GTK_CONTAINER (box)->border_width * 2;
  requisition->height = 100;
  dbg(1, "req.height=%i", requisition->height);
}


static void
transport_locators_box_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  // Normally gtk box fills all the space at the expense of uniform spacing. We use uniform spacing, so there may be extra space left at the bottom.

  //TransportLocatorsBox *lbox = TRANSPORT_LOCATORS_BOX(widget);
  //dbg(0, "panel_height=%i", GTK_WIDGET(lbox->window)->allocation.height);
  //dbg(0, "hbox_height=%i req=%i", lbox->window->hbox->allocation.height, lbox->window->hbox->requisition.height);
  //dbg(0, "vbox_height=%i req=%i", widget->allocation.height, widget->requisition.height);

  GtkBoxChild *child;
  GtkAllocation child_allocation;

  GtkBox *box = GTK_BOX (widget);
  widget->allocation = *allocation;
  GList *children = box->children;

  transport_locators_box_update_n_visible(widget);

  gint nvis_children = 0;
  while (children){
    child = children->data;
    children = children->next;

    if (GTK_WIDGET_VISIBLE (child->widget)){
      //for some reason, we have invisible labels in the box...
      if(strcmp("GtkLabel", gtk_widget_get_name(child->widget))){
        nvis_children += 1;
      }
    }
  }

  if (nvis_children > 0){
    gint y;
    gint window_height = gtk_widget_get_parent(widget)->allocation.height;
    gint available_height = (window_height/*allocation->height*/ - GTK_CONTAINER (box)->border_width * 2 - (nvis_children - 1) * box->spacing);
    gint child_height = available_height / nvis_children;

    y = allocation->y + GTK_CONTAINER (box)->border_width;
    child_allocation.x = allocation->x + GTK_CONTAINER (box)->border_width;
    child_allocation.width = MAX (1, (gint) allocation->width - (gint) GTK_CONTAINER (box)->border_width * 2);

    children = box->children;
    while (children){
      child = children->data;
      children = children->next;
      if(!strcmp("GtkLabel", gtk_widget_get_name(child->widget))) continue;

      if (GTK_WIDGET_VISIBLE (child->widget)){
        nvis_children -= 1;
        available_height -= child_height;

        if (child->fill){
          child_allocation.height = MAX (1, child_height - (gint)child->padding * 2);
          child_allocation.y = y + child->padding;
        }
        else{
          GtkRequisition child_requisition;

          gtk_widget_get_child_requisition (child->widget, &child_requisition);
          child_allocation.height = child_requisition.height;
          child_allocation.y = y + (child_height - child_allocation.height) / 2;
        }

        gtk_widget_size_allocate (child->widget, &child_allocation);

        y += child_height + box->spacing;
        //printf("y=%i child_size=%i nvis=%i\n", y, child_height, nvis_children);
      }
    }
  }
}

#define __TRANSPORT_LOCATORS_BOX_C__
