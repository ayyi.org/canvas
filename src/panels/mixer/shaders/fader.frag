/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2012-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

uniform vec4 colour;
uniform float height;
uniform float level;
varying vec2 MCposition;

void main(void)
{
	// Markers valid for max gain of 6dB
	float markers[5];
	markers[0] = 0.782; //   0dB
	markers[1] = 0.633; //  -5dB
	markers[2] = 0.510; // -10dB
	markers[3] = 0.325; // -20dB
	markers[4] = 0.121; // -40dB

	vec4 c = vec4(0.0, 0.0, 0.0, 1.0);

	int i; for(i=0;i<5;i++){
		if(abs(MCposition.y - height * (1.0 - markers[i])) < 0.5){
			c = c + vec4(0.4, 0.4, 0.4, 0.0);
			break;
		}
	}

	if(MCposition.y < height * (1.0 - level)){
		gl_FragColor = c + vec4(0.3, 0.3, 0.3, 0.0);
	}else{
		gl_FragColor = c + colour;
	}
}

