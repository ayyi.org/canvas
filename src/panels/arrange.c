/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 *  The main Arrange window
 */

#define __arrange_c__
#define __arrange_private__
#include "global.h"
#include <math.h>
#include <gdk/gdkkeysyms.h>

#include "model/transport.h"
#include "model/curve.h"
#include "model/time.h"
#include "model/pool_item.h"
#include "model/channel.h"
#include "model/connections.h"
#include "model/part_manager.h"
#include "model/track_list.h"
#include "windows.h"
#include "arrange.h"
#include "arrange/part.h"
#include "support.h"
#include "song.h"
#ifdef USE_GNOMECANVAS
#include "arrange/gnome/part_item.h"
#include "arrange/canvas_op.h"
#include "arrange/gnome/songmap.h"
#endif
#include "part_manager.h"
#include "arrange/automation.h"
#include "arrange/toolbox.h"
#include "icon.h"
#include "menu.h"
#include "arrange/gl_canvas.h"
#include "settings.h"
#include "statusbar.h"
#include "widgets/svgbutton.h"
#include "widgets/trackbutton.h"
#include "widgets/ayyi_gtk_spin_button.h"
#ifdef USE_GNOMECANVAS
#include "arrange/track_control_midi.h"
#include "arrange/track_control_audio.h"
#endif
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
#include "arrange/track_list_box.h"
#endif
#include "widgets/time_ruler.h"
#ifdef USE_CLUTTER
#  include "arrange/clutter.h"
#endif

extern SMConfig*  config;

GimpActionGroup*  shortcuts_add_group               (const char* name);
GimpActionGroup*  shortcuts_get_group               (const char* name);

static void       ayyi_arrange_class_init           (AyyiArrangeClass*);
static void       arr_add_load_handlers             (AyyiArrangeClass*);

static GObject*   arr_constructor                   (GType, guint, GObjectConstructParam*);
static void       arr_add_widgets                   (Arrange*);
static void       arr_destroy                       (GtkObject*);
static void       arr_delete                        (Arrange*);
#include "arrange/part.c"
static void       arr_on_size_request               (GtkWidget*, GtkRequisition*);
static void       arr_delete_k                      (GtkWidget*, Arrange*);
static gboolean   arr_on_key_release                (GtkWidget*, GdkEventKey*, gpointer);
static void       arr_on_ready                      (AyyiPanel*);
static void       arr_on_realise                    (GtkWidget*);
//static gboolean   arr_on_configure                  (GtkWidget*, GdkEventConfigure*);
static void       arr_on_allocate                   (GtkWidget*, GtkAllocation*);
static void       arr_on_style_set                  (GtkWidget*, GtkStyle*);
static gboolean   arr_on_key_press                  (GtkWidget*, GdkEventKey*, gpointer);
static void       arr_bsnap_on_click                (GtkWidget*, gpointer);
#ifdef USE_GNOMECANVAS
static void       arr_k_songmap_toggle              (GtkAccelGroup*, gpointer);
static void       arr_add_songmap                   (Arrange*);
#endif
static void       arr_add_rulerbar                  (Arrange*);
static void       arr_qbox_update                   (Arrange*);
static void       arr_part_editing_start            (Arrange*);
static void       arr_part_editing_stop             (Arrange*);
static GList*     arr_get_selected_parts            (AyyiPanel*);
static SignalNfo* arr_signal_new                    (Arrange*, GtkWidget*, const char* detail, GCallback, gpointer);
static void       arr_signals_block                 (Arrange*);
static void       arr_signals_unblock               (Arrange*);
static void       arr_select_all                    (AyyiPanel*);
static void**     arr_get_config                    (AyyiPanel*);
static void       arr_up_k                          (GtkAccelGroup*, Arrange*);
static void       arr_down_k                        (GtkAccelGroup*, Arrange*);
static void       arr_left_k                        (GtkAccelGroup*, Arrange*);
static void       arr_right_k                       (GtkAccelGroup*, Arrange*);
static void       arr_quantise                      (GtkAccelGroup*, gpointer);
static void       arr_set_canvas_type               (AyyiPanel*, const char*);
static void       arr_set_scroll_left               (AyyiPanel*, int);
static void       arr_set_scroll_top                (AyyiPanel*, int);
static void       arr_set_trkctl_width              (AyyiPanel*, int);
static void       arr_set_overview_visible          (AyyiPanel*, int);
static void       arr_set_songmap_visible           (AyyiPanel*, int);
static void       arr_set_partcontents_visible      (AyyiPanel*, int);
static void       arr_set_background_colour         (AyyiPanel*, const char*);
static void       arr_scroll_to_selection           (Arrange*);
static void       arr_scroll_to_track               (Arrange*, AMTrack*);
static GtkWidget* arr_trk_ctl_factory               (Arrange*, ArrTrk*);
static void       arr_songlength_update             (Arrange*);
static void       arr_hscrollbar_update             (Arrange*);
static void       arr_vscrollbar_update             (Arrange*);

static void       arr_on_song_load                  (GObject*, Arrange*);
static void       arr_on_song_unload                (GObject*, Arrange*);
static void       arr_on_track_add                  (GObject*, AMTrack*, gpointer);
static void       arr_on_tracks_change              (GObject*, TrackNum, TrackNum, int change_type, Arrange*);
static void       arr_on_track_change               (GObject*, AMTrack*, AMChangeType, Arrange*);
static void       arr_on_track_delete               (GObject*, AMTrack*, Arrange*);
static void       arr_on_track_selection_change     (Observable*, AMVal, gpointer);
static void       arr_on_part_selection_change      (GObject*, AyyiPanel*, Arrange*);
static void       arr_on_part_change                (GObject*, AMPart*, AMChangeType, AyyiPanel*, Arrange*);
static void       arr_on_song_length_change         (GObject*, Arrange*);
static void       arr_on_start_meters               (GObject*, Arrange*);
static void       arr_on_part_new                   (GObject*, AMPart*, Arrange*);
static void       arr_on_part_delete                (GObject*, AMPart*, Arrange*);
static void       arr_on_songmap_change             (GObject*, Arrange*);
static void       arr_on_locators                   (GObject*, Arrange*);
static void       arr_on_plugin_changed             (GObject*, AMChannel*, gpointer);
static void       arr_on_tracks_change_by_list      (GObject*, GList*, AMChangeType, AyyiPanel*, gpointer);
static void       arr_on_tempo_change               (GObject*, gpointer);
static void       arr_on_focus_changed              (GObject*, gpointer);
static void       arr_on_transport_stop             (GObject*, gpointer);
static void       arr_on_periodic                   (GObject*, gpointer);

static void       arr_peak_view_toggle              (Arrange*);
static void      _arr_song_overview_toggle          (Arrange*);

static void       k_zoom_in_vert                    (GtkAccelGroup*, gpointer);
static void       k_zoom_out_vert                   (GtkAccelGroup*, gpointer);

typedef struct _Arrange AyyiArrange;

static ConfigParam* params[] = {
	&(ConfigParam){"horizontal_zoom",   G_TYPE_FLOAT,  {.f=arr_set_hzoom},             {.f=ARR_MIN_HZOOM}, {.f=ARR_MAX_HZOOM}, {.f=1.0}},
	&(ConfigParam){"vertical_zoom",     G_TYPE_FLOAT,  {.f=arr_set_vzoom},             {.f=ARR_MIN_VZOOM}, {.f=ARR_MAX_VZOOM}, {.f=1.0}},
	&(ConfigParam){"canvas_type",       G_TYPE_STRING, {.c=arr_set_canvas_type},       {.c=""},            {.c=""},            {.c="gl"}},
	&(ConfigParam){"scroll_left",       G_TYPE_INT,    {arr_set_scroll_left},          {0},                {0xfffffff},        {0}},
	&(ConfigParam){"scroll_top",        G_TYPE_INT,    {arr_set_scroll_top},           {0},                {0xfffffff},        {0}},
	&(ConfigParam){"trackctl_width",    G_TYPE_INT,    {arr_set_trkctl_width},         {0},                {480},              {160}},
	&(ConfigParam){"show_overview",     G_TYPE_INT,    {arr_set_overview_visible},     {false},            {true},             {true}},
	&(ConfigParam){"show_songmap",      G_TYPE_INT,    {arr_set_songmap_visible},      {0},                {1},                {1}},
	&(ConfigParam){"show_part_contents",G_TYPE_INT,    {arr_set_partcontents_visible}, {0},                {1},                {1}},
	&(ConfigParam){"background_colour", G_TYPE_STRING, {.c=arr_set_background_colour}, {.c=""},            {.c=""},            {.c="0x00000000"}},
	NULL
};

#include "arrange/canvas.c"
#include "arrange/track.c"
#include "arrange/utils.c"

G_DEFINE_TYPE (AyyiArrange, ayyi_arrange, AYYI_TYPE_PANEL)


static void
ayyi_arrange_class_init (AyyiArrangeClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	AyyiPanelClass* panel_class  = AYYI_PANEL_CLASS (klass);

	panel_class->name           = "Arrange";
	panel_class->has_link       = true;
	panel_class->has_follow     = true;
	panel_class->has_toolbar    = true;
	panel_class->has_menubar    = true;
	panel_class->has_statusbar  = true;
	panel_class->config         = (ConfigParam**)params;
	panel_class->accel          = GDK_F1;
	panel_class->default_size.x = 800;
	panel_class->default_size.y = 360;

	panel_class->on_ready           = arr_on_ready;
	panel_class->get_selected_parts = arr_get_selected_parts;
	panel_class->select_all         = arr_select_all;
	panel_class->get_config         = arr_get_config;

	widget_class->realize         = arr_on_realise;
	//widget_class->configure_event = arr_on_configure;
	widget_class->size_request    = arr_on_size_request;
	widget_class->size_allocate   = arr_on_allocate;
	widget_class->style_set       = arr_on_style_set;

	object_class->destroy         = arr_destroy;

    g_object_class->constructor   = arr_constructor;

	canvas_types = g_hash_table_new(g_int_hash, g_int_equal);

#ifdef USE_GNOMECANVAS
	gcanvas_init();
#endif
#ifdef USE_OPENGL
	arr_gl_canvas_init();
#endif
#ifdef USE_CLUTTER
	arr_cl_init();
#endif

	panel_class->action_group = shortcuts_add_group(panel_class->name);
	GtkAccelGroup* accel_group = panel_class->accel_group = gtk_accel_group_new();

	// TODO finish moving all non-permanent accels to windowclass->mod_accels.
	// TODO these need to include icons for menu items.

	void add_track() { PF; song_add_track(TRK_TYPE_AUDIO, 1, NULL, NULL); }

	AMAccel arr_key[] = {
    {"Select All",    {{(char)'z',       GDK_CONTROL_MASK}, {0, 0}}, arr_select_all,        NULL, NULL},
    {"New Track",     {{(char)'t',       0,              }, {(char)'t',       GDK_CONTROL_MASK}}, add_track, NULL},
    {"Quantise",      {{(char)'q',       0,              }, {0, 0}}, arr_quantise},
    //{"Zoom In Hor", NULL, {{0x3d/*(char)'='*/,GDK_SHIFT_MASK,}, {0, 0}}, kzoom_in_hor_cb,       arrange},
    {"Zoom In Hor",   {{(char)'d',       0,              }, {0, 0}}, kzoom_in_hor_cb,       NULL, NULL},
    {"Zoom Out Hor",  {{(char)'a',       0,              }, {0, 0}}, kzoom_out_hor_cb,      NULL, NULL},
    {"Zoom In Vert",  {{(char)'w',       0,              }, {0, 0}}, k_zoom_in_vert,        NULL, NULL},
    {"Zoom Out Vert", {{(char)'s',       0,              }, {0, 0}}, k_zoom_out_vert,       NULL, NULL},
    {"Zoom H Back",   {{(char)'h',       0,              }, {0, 0}}, arr_zoom_history_back, NULL, NULL},
    {"Zoom To Selectn",{{(char)'y',      0,              }, {0, 0}}, arr_k_zoom_to_selection, NULL, NULL},
    {"Zoom Follow",   {{(char)'f',       0,              }, {0, 0}}, arr_k_zoom_follow,     NULL, NULL},
    {"Peak Gain-",    {{(char)'l',       0,              }, {0, 0}}, arr_peak_gain_dec_k,   NULL, NULL},
    {"Peak Gain+",    {{(char)'p',       0,              }, {0, 0}}, arr_peak_gain_inc_k,   NULL, NULL},
    {"Delete",        {{GDK_Delete,      0,              }, {0, 0}}, arr_delete_k,          NULL, NULL},
    {"Track Up",      {{GDK_uparrow,     0,              }, {0, 0}}, arr_up_k},   // cursor keys are specially trapped by arr_on_key_press 
    {"Track Down",    {{GDK_downarrow,   0,              }, {0, 0}}, arr_down_k,            NULL, NULL},
    {"Sel Left",      {{GDK_leftarrow,}, {GDK_leftarrow, GDK_SHIFT_MASK}}, arr_left_k,      NULL, NULL},
    {"Sel Right",     {{GDK_rightarrow,},{GDK_rightarrow, GDK_SHIFT_MASK}}, arr_right_k,    NULL, NULL},
    {"Scroll Left",   {{(char)',',       0,              }, {0, 0}}, arr_scroll_left_k,     NULL, NULL},
    {"Scroll Right",  {{(char)'.',       0,              }, {0, 0}}, arr_scroll_right_k,    NULL, NULL},
    {"Scroll Up",     {{(char)'>',       0               }, {0, 0}}, arr_scroll_up_k,       NULL, NULL},
    {"Scroll Down",   {{(char)'<',       0               }, {0, 0}}, arr_scroll_down_k,     NULL, NULL},
#ifdef USE_CLUTTER
    {"Clutter",       {{(char)'c',       0,              }, {0, 0}}, arr_k_clutter,         NULL, NULL},
#endif
#ifdef USE_OPENGL
    {"Gl Canvas",     {{(char)'o',       0,              }, {0, 0}}, arr_k_gl_canvas,       NULL, NULL},
#endif
    {"Edit Part",     {{(char)'e',       0,              }, {0, 0}}, arr_k_begin_part_edit, NULL, NULL},
    {"Edit Note Next",{{(char)'n',       0,              }, {0, 0}}, arr_k_next_note,       NULL, NULL},
#ifdef USE_GNOMECANVAS
    {"Auto Pt Next",  {{(char)'b',       0,              }, {0, 0}}, arr_k_next_node,       NULL, NULL},
#endif
	};

	// These accels dont need to be disabled when in text entry mode.
	AMAccel mod_key[] = {
#ifdef USE_GNOMECANVAS
		{"View Songmap",  {{(char)'m',       GDK_CONTROL_MASK}, {0, 0}}, arr_k_songmap_toggle,  NULL, NULL},
#endif
		{"Track Move Up", {{GDK_uparrow,     GDK_CONTROL_MASK}, {0, 0}}, arr_up_k},   // cursor keys are specially trapped by arr_on_key_press 
	};

	AMAccel midi_key[] = {
		{"Edit Part End", {{(char)'f',      0,              }, {0, 0}}, arr_k_end_part_edit,   NULL, NULL},
		{"Select Right",  {{GDK_rightarrow, 0               }, {0, 0}}, arr_k_next_note,       NULL, NULL},
		{"Edit Note Prev",{{GDK_leftarrow,  0               }, {0, 0}}, arr_k_prev_note,       NULL, NULL},
	};

	int k; for(k=0;k<G_N_ELEMENTS(arr_key);k++) arr_key[k].stock_item = NULL;

	klass->midi_accel_group = gtk_accel_group_new();

	make_accels(accel_group, panel_class->action_group, arr_key, G_N_ELEMENTS(arr_key), NULL);
	make_accels(klass->midi_accel_group, shortcuts_get_group("Midi Edit"), midi_key, G_N_ELEMENTS(midi_key), NULL);

	GtkAccelGroup* mod_accels = gtk_accel_group_new();
	make_accels(mod_accels, panel_class->action_group, mod_key, G_N_ELEMENTS(mod_key), NULL);
	panel_class->mod_accels = g_list_append(panel_class->mod_accels, mod_accels);

	arr_add_load_handlers(klass);

	klass->song_unload_signal = g_signal_new ("song_unload", AYYI_TYPE_ARRANGE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	klass->tool_change_signal = g_signal_new ("tool_change", AYYI_TYPE_ARRANGE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	am_song__connect("plugin-change", G_CALLBACK(arr_on_plugin_changed), NULL);
	am_song__connect("tracks-change-by-list", G_CALLBACK(arr_on_tracks_change_by_list), NULL);
	am_song__connect("transport-stop", G_CALLBACK(arr_on_transport_stop), NULL);
	am_song__connect("periodic-update", (GCallback)arr_on_periodic, NULL);
	am_song__connect("tempo-change", G_CALLBACK(arr_on_tempo_change), NULL);

	void arr_on_curve_add (GObject* o, AMCurve* curve, int p, gpointer _arrange)
	{
		arrange_foreach {
			ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, curve->track);
			if (atr) {
#ifdef DEBUG
				ArrAutoPath* aap = arr_track_find_aap (atr, curve);
				g_return_if_fail(!aap);
#endif
				if (curve == curve->track->bezier.vol) {
					// if the curve was added locally it will already exist in the current ArrTrk.
					if (!AUTO_PATH(VOL)) {
						if (atr->auto_paths->len <= VOL)
							g_ptr_array_set_size(atr->auto_paths, VOL + 1);
						atr->auto_paths->pdata[VOL] = AYYI_NEW(ArrAutoPath, .curve = curve);
					}

					arrange->canvas->on_auto_ctl_show(arrange, atr, AUTO_PATH(VOL));
				}
			}
		} end_arrange_foreach;
	}

	g_signal_connect(song, "curve-add", G_CALLBACK(arr_on_curve_add), NULL);

	klass->view_option_toggle[SHOW_PART_CONTENTS] = arr_peak_view_toggle;
#ifdef USE_GNOMECANVAS
	klass->view_option_toggle[SHOW_AUTOMATION] = automation_view_toggle;
	klass->view_option_toggle[SHOW_SONG_MAP] = arr_song_map_toggle;
#endif
	klass->view_option_toggle[SHOW_SONG_OVERVIEW] = _arr_song_overview_toggle;

	bool get_value (AyyiWindow* window, gpointer view_type)
	{
		Arrange* arrange = (Arrange*)windows__find_panel(window, AYYI_TYPE_ARRANGE);
		if(arrange){
			return arrange->view_options[GPOINTER_TO_INT(view_type)].value;
		}
		return true;
	}

	void toggle (AyyiWindow* window, gpointer view_type)
	{
		Arrange* arrange = (Arrange*)windows__find_panel(window, AYYI_TYPE_ARRANGE);
		if(arrange){
			AYYI_ARRANGE_GET_CLASS(arrange)->view_option_toggle[GPOINTER_TO_INT(view_type)](arrange);
		}
	}

	menu_add_view_item("Automation", get_value, toggle, GINT_TO_POINTER(SHOW_AUTOMATION));
#ifdef USE_GNOMECANVAS
	menu_add_view_item("Song map", get_value, toggle, GINT_TO_POINTER(SHOW_SONG_MAP));
#endif
	menu_add_view_item("Part contents", get_value, toggle, GINT_TO_POINTER(SHOW_PART_CONTENTS));
	menu_add_view_item("Song Overview", get_value, toggle, GINT_TO_POINTER(SHOW_SONG_OVERVIEW));

	g_signal_connect(app, "focus-changed", (GCallback)arr_on_focus_changed, NULL);
}


static void
ayyi_arrange_init (AyyiArrange* a)
{
	a->zoom_history = NULL;
	a->zoom_focus = ZOOM_FOCUS_CENTRE;
	a->peak_gain = 1.0;
#if 0
	arrange->offset_beats_x = 0; //not yet being used.
#endif
	a->ruler_height = 28; // warning: the time_ruler widget will override this for large font sizes.
	a->text_height = 5;   // was 11;
	a->part_selection = NULL;
	a->automation.sel.track = 0;
	a->automation.sel.subpath = 0;
	a->part_fades_show = true;
	a->tracks.selection = NULL;
	a->tracks.menu = NULL;
}


static GObject*
arr_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	// construct_params are: name, master, orientation

	AYYI_DEBUG printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_arrange_parent_class)->constructor(type, n_construct_properties, construct_param);
	Arrange* arrange = (Arrange*)g_object;
	AyyiPanel* panel = (AyyiPanel*)g_object;

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_ARRANGE);

#ifdef DEBUG
	gtk_widget_set_name(panel->vbox, "arrange-vbox");
#endif

	// h zoom of 1.0 corresponds to PX_PER_BEAT pixels per beat.
	panel->zoom->min.pt = (Ptf){ARR_MIN_HZOOM, ARR_MIN_VZOOM};
	panel->zoom->max.pt = (Ptf){ARR_MAX_HZOOM, ARR_MAX_VZOOM};

	void arr_on_scrollable_width (AGlObservable* o, AGlVal val, gpointer _arrange)
	{
		Arrange* arrange = _arrange;

		arrange->priv->canvas_size.x = 0;
		arr_hscrollbar_update(arrange);
	}

	void arr_on_scrollable_height (AGlObservable* o, AGlVal val, gpointer _arrange)
	{
		Arrange* arrange = _arrange;

		arrange->priv->canvas_size.y = 0;
		arr_vscrollbar_update(arrange);
	}

	arrange->width = agl_observable_new();
	arrange->height = agl_observable_new();
	agl_observable_subscribe (arrange->width, arr_on_scrollable_width, arrange);
	agl_observable_subscribe (arrange->height, arr_on_scrollable_height, arrange);

	arrange->priv = AYYI_NEW(ArrangePriv,
		.tracks = track_list__new(arrange),
	);

	arrange->partmenu = part_menu_new(arrange);

	arr_signals_block(arrange);

	return g_object;
}


static void
arr_add_widgets (Arrange* arrange)
{
	void arr_add_toolbar (Arrange* arrange)
	{
		// Toolbar - arrange-specific additions

		AyyiPanel* panel = &arrange->panel;
		Arrange* a = arrange;
		ArrangePriv* _arr = arrange->priv;

		toolbar_add_separator(panel, 15);

		// tempo box
		{
			a->tempobox = (GtkWidget*)ayyi_gtk_spin_button_new(song->tempo);

			bool _accels_disconnect(GtkWidget* widget, GdkEvent* event, gpointer user_data){ accels_disconnect(widget); return NOT_HANDLED; }
			bool _accels_connect   (GtkWidget* widget, GdkEvent* event, gpointer user_data){ accels_connect   (widget); return NOT_HANDLED; }
			arr_signal_new(arrange, a->tempobox, "focus-in-event",  G_CALLBACK(_accels_disconnect), NULL);
			arr_signal_new(arrange, a->tempobox, "focus-out-event", G_CALLBACK(_accels_connect),    NULL);

			gtk_widget_set_sensitive(a->tempobox, FALSE);

			gtk_box_pack_start(GTK_BOX(panel->toolbar_box[0]), a->tempobox, FALSE, FALSE, 0);

			void on_tempo_adj_changed2(Observable* tempo, AMVal value, gpointer _arrange)
			{
			}
			observable_subscribe(song->tempo, on_tempo_adj_changed2, arrange);
		}

		toolbar_add_separator(panel, 15);

		// snap button
		{
			_arr->bsnap = svgbutton_new();
			int i; for(i=0;i<MAX_SNAP;i++) svgbutton_add_state(SVGBUTTON(_arr->bsnap), snap_modes[i].name, snap_modes[i].svg_file);
			gtk_widget_show(_arr->bsnap);
			gtk_box_pack_start(GTK_BOX(panel->toolbar_box[0]), _arr->bsnap, FALSE, FALSE, 0);
			g_signal_connect((gpointer)_arr->bsnap, "clicked", G_CALLBACK(arr_bsnap_on_click), arrange);
			g_signal_connect(G_OBJECT(_arr->bsnap), "enter-notify-event", G_CALLBACK(mouseover_enter), SVGBUTTON(_arr->bsnap)->mouseover_text);
			g_signal_connect(G_OBJECT(_arr->bsnap), "leave-notify-event", G_CALLBACK(mouseover_leave), panel);
		}

		// quantise selector (values are filled in later by q_song_init())
		{
			arrange->q_combo = gtk_combo_box_new_text();
			gtk_widget_set_size_request(arrange->q_combo, 70, -1);
			gtk_box_pack_start(GTK_BOX(panel->toolbar_box[0]), arrange->q_combo, FALSE, FALSE, 0);
		}

		toolbar_add_separator(panel, 15);

		toolbox_new(arrange, panel->toolbar_box[0]);

		toolbar_add_separator(panel, 20);

		// zoom sliders
		{
			a->hzoom_adj = gtk_adjustment_new (hzoom_get_adj_val(arrange), ARR_MIN_HZOOM, 101.0, 0.1, 1.0, 1.0);
			a->hzoom_scale = gtk_hscale_new (GTK_ADJUSTMENT (a->hzoom_adj));
			gtk_scale_set_value_pos(GTK_SCALE(a->hzoom_scale), GTK_POS_RIGHT);
			gtk_box_pack_start (GTK_BOX(panel->toolbar_box[0]), a->hzoom_scale, TRUE, TRUE, 0);
			g_signal_connect((gpointer)a->hzoom_scale, "value-changed", G_CALLBACK(arr_hzoom_slider_cb), NULL);

			a->vzoom_adj = gtk_adjustment_new (1.0, 0.0, 31.0, 0.1, 1.0, 1.0);
			a->vzoom_scale = gtk_hscale_new (GTK_ADJUSTMENT (a->vzoom_adj));
			gtk_scale_set_value_pos(GTK_SCALE(a->vzoom_scale), GTK_POS_RIGHT);
			gtk_box_pack_start (GTK_BOX(panel->toolbar_box[0]), a->vzoom_scale, TRUE, TRUE, 0);
			g_signal_connect((gpointer)a->vzoom_scale, "value-changed", G_CALLBACK(arr_vzoom_slider_cb), NULL);
		}

		// resize eventbox underneath the toolbar
		GtkWidget* tb_size_ev = gtk_event_box_new();
		panel_pack(tb_size_ev, EXPAND_FALSE);
		gtk_widget_set_size_request(tb_size_ev, -1, 2);
		g_signal_connect(tb_size_ev, "event", G_CALLBACK(toolbar_on_resize), NULL);
	}

	AyyiPanel* panel = &arrange->panel;
	Arrange* a = arrange;
	ArrangePriv* _arr = arrange->priv;

	#define IS_BUILT (arrange->priv->hpaned)
	if(IS_BUILT) return;

	arr_add_toolbar(arrange);

	// HPaned to hold the track ctl list and arrange canvas
	_arr->hpaned = gtk_hpaned_new();
	gtk_paned_set_position(GTK_PANED(_arr->hpaned), config_get_window_setting_int(panel, "trackctl_width"));
	panel_pack(_arr->hpaned, EXPAND_TRUE);

	a->priv->vbox_rhs = gtk_vbox_new(FALSE, 0);

	GtkWidget* vbox_lhs = gtk_vbox_new(FALSE, 0); // vbox containing track control area and padding above it.
	{
		gtk_paned_add1(GTK_PANED(_arr->hpaned), vbox_lhs);

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		// 'track control' or 'track list' area (label, mute, solo etc):

		// a viewport for the track controls (follows the scrolling of the rhs scrollwindow)
		// TODO why doesnt scrollwheel work on this? - TrackListBox probably needs to register for scroll events
		arrange->track_list_box = (GtkWidget*)track_list_box_new(arrange);

		void
		on_tc_vadj_changed(GtkWidget* widget, gpointer _arrange)
		{
			// workaround to re-set the correct trk_ctl scroll position after it inexplicably gets set to 0.

			Arrange* arrange = (Arrange*)_arrange;
			if(GTK_ADJUSTMENT(widget)->value != arrange->canvas->v_adj->value){
				gtk_adjustment_set_value(GTK_ADJUSTMENT(widget), arrange->canvas->v_adj->value);
			}
		}
		g_signal_connect (((GtkViewport*)arrange->track_list_box)->vadjustment, "value-changed", G_CALLBACK(on_tc_vadj_changed), arrange);

		//put a spacer at the bottom to line the control area up with the scrollbars:
		GtkWidget* bot_sp = gtk_event_box_new (); //use cheaper widget?
		gtk_widget_show(bot_sp);
		gtk_widget_set_size_request(bot_sp, -1, 15);
		gtk_box_pack_end(GTK_BOX(vbox_lhs), bot_sp, EXPAND_FALSE, FILL_FALSE, 0);

		arrange->tracks.sizegroup = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
#endif
	}

	// Because signal handlers are called in the order they are added, it is important that handlers are added in the correct order.
	// -for "add" events, arrange song event handlers must be added _before_ canvas handlers so that the canvas can access arrange-level objects such as ArrTrk.
	// (for "remove" events the situation is reversed)
	// TODO move other "add" handlers here also.
	g_signal_connect(song->tracks, "add", G_CALLBACK(arr_on_track_add), arrange);

	//-------------------------------------------------------------

#ifdef USE_GNOMECANVAS
	if(SHOW_SONGMAP) arr_add_songmap(arrange);
#endif

	dbg(2, "canvas_type=%i", arrange->canvas_type);
	arr_set_canvas(arrange, arrange->canvas_type);

	if(GTK_WIDGET_REALIZED(arrange)){
		//get_style_font();
	}

	//------------------------------------------------------

#ifdef USE_GNOMECANVAS
	a->canvas->vtab[0] = arrange->songmap ? arrange->songmap->height : 0; // bottom of songmap.
#else
	a->canvas->vtab[0] = 0;
#endif
	a->canvas->vtab[1] = a->canvas->vtab[0] + arrange->ruler_height;      // bottom of rulerbar.

	// Add some padding to the left of the rulerbar / top of the lhs trkctl area:
	// -this only does vertical positioning, not horizontal.
	GtkWidget* hbox_tkctl_top = arrange->priv->tcl_top_space = gtk_hbox_new(NON_HOMOGENOUS, 0);

	GtkWidget* ev_tkctl_top = gtk_event_box_new(); //TODO now we have an event box, we dont need the above hbox.
	gtk_widget_set_size_request(ev_tkctl_top, -1, a->canvas->vtab[1]);
	gtk_container_add(GTK_CONTAINER(ev_tkctl_top), hbox_tkctl_top);
	gtk_box_pack_start(GTK_BOX(vbox_lhs), ev_tkctl_top, FALSE, FALSE, 0);

	// load panel settings again (for settings that need the canvas to be present)
	config_load_window_instance(panel);

	bool tcl_top_on_event (GtkWidget* widget, GdkEventButton* event, Arrange* arrange)
	{
		switch (event->type){
			case GDK_BUTTON_PRESS:
				dbg (2, "button=%i", event->type);
				if(event->button == 3){
					ayyi_panel_on_focus(&arrange->panel);
					shell__menu_popup(arrange->panel.window, (GdkEvent*)event);
				}
				break;
			default:
				return NOT_HANDLED;
		}
		return HANDLED;
	}
	g_signal_connect(G_OBJECT(ev_tkctl_top), "button-press-event", G_CALLBACK(tcl_top_on_event), arrange);
  
	char spacer_path[256];
	snprintf(spacer_path, 255, "%s/../%s", config->svgdir, "1px.png");

	#define TKCTL_RSIZE_WID 2
	GtkWidget* fixed_topleft = gtk_fixed_new();
	gtk_box_pack_start(GTK_BOX(hbox_tkctl_top), fixed_topleft, FALSE, FALSE, 0);

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	gtk_box_pack_start (GTK_BOX(vbox_lhs), arrange->track_list_box, EXPAND_TRUE, FILL_TRUE, 0);
#endif

	//-------------------------------------------------------------

	gtk_paned_add2(GTK_PANED(_arr->hpaned), _arr->vbox_rhs);

	arrange->tracks.menu = arr_track_menu_init(arrange);

	// If there is an existing Arrange panel, we can copy settings from it
	Arrange* template_panel = (Arrange*)windows__get_first(AYYI_TYPE_ARRANGE);
	if(template_panel == arrange) template_panel = NULL;

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	if(CANVAS_IS_GNOME){
		gtk_widget_grab_focus(arrange->track_list_box);
	}else{
		gtk_widget_grab_focus(panel->b_toolbar[0]); // note setting focus on toolbar disables track-selection keys
	}
#else
	gtk_widget_grab_focus(panel->b_toolbar[0]);
#endif

	gtk_widget_set_size_request(panel->toolbar_box[0], 20, -1);
	gtk_widget_set_size_request(panel->toolbar_box[1], 20, -1);

	arr_zoom_set(arrange, hzoom(arrange), vzoom(arrange)); // display values that were set by config loader

	ayyi_panel_on_open(panel);
	app->latest_arrange = arrange;

	if(song->loaded){
		arr_on_song_load((GObject*)song, arrange);
	}

	am_song__connect("song-load", G_CALLBACK(arr_on_song_load), arrange);
	am_song__connect("song-unload", G_CALLBACK(arr_on_song_unload), arrange);
	am_song__connect("tracks-change", G_CALLBACK(arr_on_tracks_change), arrange);
	am_song__connect("song-length-change", G_CALLBACK(arr_on_song_length_change), arrange);
	am_song__connect("locators-change", G_CALLBACK(arr_on_locators), arrange);
	am_song__connect("songmap-change", G_CALLBACK(arr_on_songmap_change), arrange);
	g_signal_connect(song->tracks, "delete", G_CALLBACK(arr_on_track_delete), arrange); // connected after canvas handlers. Canvas handlers will get the old arrange state. Handlers that want the new state can be run via canvas->on_track_delete()
	g_signal_connect(song->tracks, "change", G_CALLBACK(arr_on_track_change), arrange);
	g_signal_connect(am_parts, "item-changed", G_CALLBACK(arr_on_part_change), arrange);
	// TODO add handlers should be earlier so as to be before the canvas handlers
	g_signal_connect(am_parts, "add", G_CALLBACK(arr_on_part_new), arrange);
	g_signal_connect(am_parts, "delete", G_CALLBACK(arr_on_part_delete), arrange);
	g_signal_connect(am_parts, "selection-change", G_CALLBACK(arr_on_part_selection_change), arrange);

	g_signal_connect(panel, "key-press-event", G_CALLBACK(arr_on_key_press), NULL);
	g_signal_connect(panel, "key-release-event", G_CALLBACK(arr_on_key_release), NULL);

	g_signal_connect(app, "app-start-meters", G_CALLBACK(arr_on_start_meters), arrange);

	observable_subscribe_with_state(am_tracks->selection2, arr_on_track_selection_change, arrange);

	void arr_on_zoom (Observable* zoom, AMVal val, gpointer _panel)
	{
		AyyiPanel* panel = _panel;
		Arrange* arrange = (Arrange*)panel;

		ZoomType zoom_type = ((PtObservable*)panel->zoom)->change;

		// Update zoom bars
		{
			// block the value-changed signal to prevent this fn from being called again.
			gulong id1 = g_signal_handler_find(arrange->hzoom_scale, G_SIGNAL_MATCH_FUNC, 0, 0, 0, arr_hzoom_slider_cb, NULL);
			gulong id2 = g_signal_handler_find(arrange->vzoom_scale, G_SIGNAL_MATCH_FUNC, 0, 0, 0, arr_vzoom_slider_cb, NULL);

			if(id1){
				g_signal_handler_block(arrange->hzoom_scale, id1);
				g_signal_handler_block(arrange->vzoom_scale, id2);
			}
		#ifdef DEBUG
			else perr ("zoombar handlerid not found.");
		#endif

			// signal blocking is not effective, so we also check if value has changed
			if(GTK_ADJUSTMENT(arrange->hzoom_adj)->value != hzoom_get_adj_val(arrange)){
				gtk_adjustment_set_value(GTK_ADJUSTMENT(arrange->hzoom_adj), hzoom_get_adj_val(arrange));
				gtk_adjustment_value_changed(GTK_ADJUSTMENT(arrange->hzoom_adj));
			}
			if(GTK_ADJUSTMENT(arrange->vzoom_adj)->value != vzoom_get_adj_val(arrange)){
				gtk_adjustment_set_value(GTK_ADJUSTMENT(arrange->vzoom_adj), vzoom_get_adj_val(arrange));
				gtk_adjustment_value_changed(GTK_ADJUSTMENT(arrange->vzoom_adj));
			}
			g_signal_handler_unblock(arrange->hzoom_scale, id1);
			g_signal_handler_unblock(arrange->vzoom_scale, id2);
		}

		if (zoom_type & CHANGE_X) {
			arrange->priv->canvas_size.x = 0;
			agl_observable_set_int (arrange->width, 0);
		}

		// TODO check this, possibly it should only be set for user activated changes
		arr_zoom_history_update(arrange);

		if (zoom_type & CHANGE_Y) {
			arrange->priv->canvas_size.y = 0;
			agl_observable_set_int (arrange->height, arrange->height->value.i + 1);

			if(hzoom(arrange) > ARR_MAX_HZOOM -0.1) arr_statusbar_printf(arrange, 1, "at max zoom");
			dbg(2, "vzoom=%.2f", vzoom(arrange));
			arr_track_pos_update(arrange);
		}

		// TODO this most likely should become redundant
		arr_on_view_changed(arrange,
			((zoom_type & CHANGE_X) ? AM_CHANGE_ZOOM_H : 0) |
			((zoom_type & CHANGE_Y) ? AM_CHANGE_ZOOM_V : 0)
		);
	}

	observable_subscribe_with_state(panel->zoom, arr_on_zoom, arrange);
}


static void
arr_destroy (GtkObject* object)
{
	PF;

	Arrange* arrange = (Arrange*)object;
	AyyiPanel* panel = (AyyiPanel*)object;

	if (panel->destroyed) goto out;

	// disconnect signals
	for (int t=0;t<AM_MAX_TRK;t++) {
		dbg(3, "t=%i", t);
		ArrTrk* trk = track_list__track_by_index(arrange->priv->tracks, t);
		if (trk && trk->trk_ctl) {
#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
			track_control_disconnect(trk->trk_ctl);
#endif
#endif
		}
	}
	am_song__disconnect(arrange, arr_on_song_load);
	am_song__disconnect(arrange, arr_on_song_unload);
	am_song__disconnect(arrange, arr_on_tracks_change);
	am_song__disconnect(arrange, arr_on_song_length_change);
	am_song__disconnect(arrange, arr_on_locators);
	am_song__disconnect(arrange, arr_on_songmap_change);

	#define parts_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (am_parts, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange) != 1) pwarn("part handler disconnection");
	parts_handler_disconnect(arr_on_part_new);
	parts_handler_disconnect(arr_on_part_delete);
	parts_handler_disconnect(arr_on_part_selection_change);
	parts_handler_disconnect(arr_on_part_change);
	#undef parts_handler_disconnect
	#define tracks_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (am_tracks, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange) != 1) pwarn("track handler disconnection");
	tracks_handler_disconnect(arr_on_track_add);
	tracks_handler_disconnect(arr_on_track_delete);
	tracks_handler_disconnect(arr_on_track_change);
	#undef tracks_handler_disconnect
	#define ap_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (app, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange) != 1) pwarn("ap handler disconnection");
	ap_handler_disconnect(arr_on_start_meters);
	#undef ap_handler_disconnect

	observable_unsubscribe(panel->zoom, NULL, arrange);
	observable_unsubscribe(am_tracks->selection2, NULL, arrange);

	for (int t=0;t<AM_MAX_TRK;t++) {
		ArrTrk* atr = arrange->priv->tracks->track[t];
		if (atr) arr_track_undraw (arrange, atr);
	}

	if (arrange->canvas) call(arrange->canvas->free, arrange);
#ifdef USE_GNOMECANVAS
	arrange->songmap = NULL; //free'd automatically by its container parent
	if (arrange->song_overview.widget) arr_song_overview_destroy(arrange);
#endif

	g_clear_pointer(&arrange->width, agl_observable_free);
	g_clear_pointer(&arrange->height, agl_observable_free);

	track_list__free(arrange->priv->tracks);
	g_free(arrange->priv);

#ifndef DEBUG_DISABLE_TOOLBAR
	toolbox_free(arrange);
#endif

	g_list_free_full (g_steal_pointer(&arrange->signals), g_free);
	g_list_free_full (g_steal_pointer(&arrange->zoom_history), g_free);

	if (app->latest_arrange == arrange) app->latest_arrange = NULL;

	((AyyiPanel*)object)->destroyed = true;
  out:
	GTK_OBJECT_CLASS(ayyi_arrange_parent_class)->destroy(object);
}


static void
arr_on_ready (AyyiPanel* panel)
{
	// the intention is that widgets are not shown until the panel is 'ready'.

	Arrange* arrange = (Arrange*)panel;

	arr_add_widgets(arrange);

	g_return_if_fail(gtk_widget_get_realized((GtkWidget*)panel));

	if(SONG_LOADED) arr_signals_unblock(arrange);
	if(SONG_LOADED) gtk_widget_set_sensitive(arrange->tempobox, TRUE);

	toolbox_change_tool(arrange, TOOL_DEFAULT); // so the widget gets painted properly.
#ifdef USE_GNOMECANVAS
	gcanvas_scrollregion_update(arrange);
#endif

	AyyiWindow* window = (AyyiWindow*)gtk_widget_get_toplevel((GtkWidget*)arrange);
	if(window->menu.window){
		menu_update_options();
	}

	AMTrack* first = am_collection_selection_first(am_tracks);
	if(!arrange->tracks.selection && first) arr_update_track_selection(arrange, first);
}


void
arr_apply_song_prefs (Arrange* arrange)
{
	g_return_if_fail(arrange);

	AyyiArrangeClass* klass = AYYI_ARRANGE_GET_CLASS(arrange);
	if(klass && klass->song_storage){
		int inst = ayyi_panel_get_instance_num((AyyiPanel*)arrange);
		ArrangeSongStorage* settings = g_list_nth_data(klass->song_storage, inst - 1);
		if(settings){
			for(int i=0;i<am_track_list_count(song->tracks);i++){
				dbg(2, "height=%i", settings->height[i]);
				if(settings->height[i]){
					track_list__track_by_index(arrange->priv->tracks, i)->height = settings->height[i];
					if(arrange->canvas) arr_on_view_changed(arrange, AM_CHANGE_HEIGHT | AM_CHANGE_ZOOM_H);
				}
			}
		}
	}
}


void
arr_set_canvas (Arrange* arrange, CanvasType nnn)
{
	ArrangePriv* a = arrange->priv;
#ifdef USE_GNOMECANVAS
	AyyiPanel* panel = (AyyiPanel*)arrange;
#endif
	g_return_if_fail(nnn);
	PF2;

	if(arrange->canvas){
		if(nnn == arrange->canvas->type) return;

		call(arrange->canvas->free, arrange); // destroy the old canvas
	}

	CanvasClass* cc = g_hash_table_lookup(canvas_types, &nnn);
	g_return_if_fail(cc);
	GtkWidget* canvas = call(cc->canvas_new, arrange);
	g_return_if_fail(canvas);

	if(canvas){
		gtk_widget_show_all(canvas);

		if(cc->provides & PROVIDES_RULERBAR){
			if(arrange->ruler){ gtk_widget_destroy(arrange->ruler); arrange->ruler = NULL; }
		}else{
			if(!arrange->ruler) arr_add_rulerbar(arrange);
		}

		if(cc->provides & PROVIDES_TRACKCTL){
			gtk_box_pack_start(GTK_BOX(((AyyiPanel*)arrange)->vbox), canvas, EXPAND_TRUE, TRUE, 0);
			if(cc->provides & PROVIDES_TRACKCTL){ // the above may have triggered a canvas realise and it may not be possible after all to provide track_control
				gtk_box_reorder_child(GTK_BOX(((AyyiPanel*)arrange)->vbox), canvas, 2);
				gtk_widget_hide(a->hpaned);
				gtk_widget_set_no_show_all(a->hpaned, true);
			}
		}else{
			arr_trkctl_enable(arrange);
		}

		if(arrange->view_options[SHOW_SONG_OVERVIEW].value){
			if(cc->provides & PROVIDES_OVERVIEW){
#ifdef USE_GNOMECANVAS
				if(arrange->song_overview.widget) gtk_widget_hide(arrange->song_overview.widget);
#endif
			}else{
#ifdef USE_GNOMECANVAS
				if(arrange->song_overview.widget){
					gtk_widget_show(arrange->song_overview.widget);
				}else{
					panel_pack(arr_song_overview_new(arrange), EXPAND_FALSE);
				}
#endif
			}
		}

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		if(cc->provides & PROVIDES_TRACKCTL){
			if(arrange->tracks.sizegroup){
				g_object_unref(arrange->tracks.sizegroup);
				arrange->tracks.sizegroup = NULL;
			}
		}else{
			if(!arrange->tracks.sizegroup)
				arrange->tracks.sizegroup = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
		}
#endif
	}

	gboolean on_widgets (gpointer _arrange)
	{
		Arrange* arrange = _arrange;
		g_return_val_if_fail(arrange->canvas, G_SOURCE_REMOVE);

		gtk_drag_dest_set(GTK_WIDGET(arrange->canvas->widget), GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
		g_signal_handlers_disconnect_matched(G_OBJECT(arrange->canvas->widget), G_SIGNAL_MATCH_FUNC, 0, 0, NULL, arr_canvas_drag_received, NULL);
		g_signal_connect(G_OBJECT(arrange->canvas->widget), "drag-data-received", G_CALLBACK(arr_canvas_drag_received), NULL);

		return G_SOURCE_REMOVE;
	}

	if(cc->provides & ASYNC_CREATION){
		// Clutter widget creation is async.
		// FIXME dont use a timer. if window is closed before timeout, will segfault.
		g_timeout_add(1500, on_widgets, (gpointer)arrange);
	}else{
		on_widgets(arrange);
	}

#ifdef USE_GNOMECANVAS
	dbg(2, "provides=%i", cc->provides);
	if (cc->provides & PROVIDES_METERS) {
		dbg(2, "canvas has meters");
		ArrTrk* at;
		for (int t=0;(at=a->tracks->track[t]) && t < AM_MAX_TRK;t++) {
			if (at->trk_ctl) track_control_remove_meter((TrackControl*)at->trk_ctl);
		}
	} else {
		ArrTrk* at;
		for (int t=0;(at=a->tracks->track[t]) && t < AM_MAX_TRK;t++) {
			if(at->trk_ctl && !at->meter) track_control_make_meter((TrackControl*)at->trk_ctl);
		}
	}
#endif

	arrange->canvas->type = nnn;
}


double
arr_canvas_width (Arrange* arrange)
{
	ArrangePriv* arr = arrange->priv;

	if(!arr->canvas_size.x){
		// note, in order to see parts past the song end, this would need to be changed
		arr->canvas_size.x = arr_beats2px(arrange, am_object_val(&song->loc[AM_LOC_END]).sp.beat) + ARR_SONG_END_MARGIN;
	}
	return arr->canvas_size.x;
}


int
arr_canvas_height (Arrange* arrange)
{
	ArrangePriv* arr = arrange->priv;

	if(!arr->canvas_size.y){
		#define CANVAS_BOTTOM_BORDER 15.0

		int y = 0;
		ArrTrk* at = NULL;
		while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL))){
			y += at->height * vzoom(arrange);
		}

		arr->canvas_size.y = y + CANVAS_BOTTOM_BORDER;
	}
	return arr->canvas_size.y;
}


void
arr_set_zoom_follow (Arrange* arrange, bool follow)
{
	dbg(0, "follow=%i", follow);

	if((arrange->zoom_follow = follow)){
		arr_songlength_update(arrange);
	}
}


void
arr_on_view_changed (Arrange* arrange, AMChangeType change)
{
	ArrangePriv* arr = arrange->priv;

	g_return_if_fail(arrange->canvas);

	// Intended to handle position, size and magnification changes.

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
#endif

	if(change & AM_CHANGE_LEN){
		agl_observable_set_int (arrange->width, 0);
		arr_songlength_update(arrange);
	}

	if(change & AM_CHANGE_POS_X){
#ifndef DEBUG_DISABLE_RULERBAR
		if(arrange->ruler) time_ruler_set_start((TimeRuler*)arrange->ruler, arrange->canvas->get_viewport_left(arrange));
#endif
#ifdef USE_GNOMECANVAS
		if(!(cc->provides & PROVIDES_OVERVIEW)){
			arr_song_overview_queue_for_update();
		}
#endif

#ifdef USE_OPENGL
		// update songmap. TODO remove canvastype check
		if(CANVAS_IS_OPENGL){
#ifdef USE_GNOMECANVAS
			GtkAdjustment* songmap_adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(arrange->songmap));
			gtk_adjustment_set_value(songmap_adj, arrange->canvas->h_adj->value);
#endif
		}
#endif
	}

	if (change & AM_CHANGE_ZOOM_V || change & AM_CHANGE_HEIGHT) {
		agl_observable_set_int (arrange->height, arrange->height->value.i + 1);

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		if(!(cc->provides & PROVIDES_TRACKCTL)){
			track_list_box_on_dim_change(arrange);
		}
#endif
	}

	if(change & AM_CHANGE_HEIGHT){
		arr_track_pos_update(arrange);
		arr->canvas_size.y = 0;
	}

	if((change & AM_CHANGE_POS_X) || (change & AM_CHANGE_POS_Y)){
	}

	if((change & AM_CHANGE_ZOOM_H) || (change & AM_CHANGE_ZOOM_V)){
		if(change & AM_CHANGE_ZOOM_H){
#ifdef USE_GNOMECANVAS
			songmap_redraw(arrange);
			if(!(cc->provides & PROVIDES_OVERVIEW)){
				arr_song_overview_queue_for_update();
			}
#endif

#ifndef DEBUG_DISABLE_RULERBAR
			if(arrange->ruler) time_ruler_set_zoom((TimeRuler*)arrange->ruler, arr_samples_per_pix(arrange), arrange->canvas->get_viewport_left(arrange));
#endif
		}
		if(change & AM_CHANGE_ZOOM_V){

			// scroll up needed?
			int p = arrange->canvas->v_adj->value;
			if(p > 0){
				if(p + arr_canvas_viewport_size(arrange).height > arr_canvas_height(arrange)){
					arrange->canvas->scroll_to(arrange, -1, MAX(0, (int)arr_canvas_height(arrange) - arr_canvas_viewport_size(arrange).height));
				}
			}

#ifdef USE_GNOMECANVAS
			if(!(cc->provides & PROVIDES_OVERVIEW)){
				arr_song_overview_queue_for_update();
			}
#endif
		}
	}

	arrange->canvas->on_view_change(arrange, change);
}


void
arr_toolbar_update (Arrange* arrange)
{
	// Update toolbar items specific to the arrange window - NOT standard or transport items.

	/*  snap is no longer a toggle button.......
	GtkStyle* style = NULL;

	if(song->snap_mode == SNAP_BAR){ //hmmm, why isnt snap an arrange property?
	style = gtk_style_copy(gtk_widget_get_style(arrange->priv->bsnap));
	style->bg[GTK_STATE_NORMAL] = style->bg[GTK_STATE_SELECTED];
	style->fg[GTK_STATE_NORMAL] = style->fg[GTK_STATE_SELECTED];
	}
	gtk_widget_set_style(arrange->priv->bsnap, style);

	if(style) g_object_unref(style);
	*/
}


void
arr_while_recording (Arrange* arrange)
{
	if(arrange->canvas->while_recording) arrange->canvas->while_recording(arrange);
}


void
arr_post_recording (Arrange* arrange)
{
	if(arrange->canvas->post_recording) arrange->canvas->post_recording(arrange);
}


static void
arr_songlength_update (Arrange* arrange)
{
	// Update the canvas size following a change in the song length (as given by song->loc[END]).

	g_return_if_fail(AYYI_IS_ARRANGE(arrange));

	if(am_object_val(&song->loc[AM_LOC_END]).sp.beat > AYYI_MAX_BEATS){
		pwarn ("song too long (%i beats).", am_object_val(&song->loc[AM_LOC_END]).sp.beat);
		am_object_val(&song->loc[AM_LOC_END]).sp.beat = AYYI_MAX_BEATS;
	}

	arrange->priv->canvas_size.x = 0;
	arrange->canvas->h_adj->upper = arr_canvas_width(arrange);

	if(arrange->zoom_follow){
		arr_zoom_to_song(arrange);
	}
}


static void
arr_delete_k (GtkWidget* accel_group, Arrange* arrangeX)
{
	Arrange* arrange = (Arrange*)app->active_panel;
	g_return_if_fail(arrange);

	arr_delete(arrange);
}


void
arr_delete (Arrange* arrange)
{
	g_return_if_fail(AYYI_IS_ARRANGE(arrange));
	PF;

	if(arrange->part_selection){
		GList* parts = g_list_copy(arrange->part_selection);
		dbg (1, "%sdeleting %i parts....%s", yellow, g_list_length(parts), white);
		GList* p = parts;
		for(;p;p=p->next) song_part_delete(p->data);
		g_list_free(parts);
	}else{
		g_return_if_fail(arrange->tracks.selection);
		song_del_track(arrange->tracks.selection, NULL, NULL);
	}
}


static void
arr_on_allocate (GtkWidget* widget, GtkAllocation* allocation)
{
	/*
	 * warning: this gets called a lot.
	 */

	dbg(2, "window req: width=%i width_alloc=%i", widget->requisition.height, widget->allocation.height);

	((GtkWidgetClass*)ayyi_arrange_parent_class)->size_allocate(widget, allocation);

	Arrange* arrange = (Arrange*)widget;
	AyyiPanel* panel = (AyyiPanel*)widget;

	if(!IS_BUILT) return;

	arrange->canvas->on_view_change(arrange, AM_CHANGE_WIDTH | AM_CHANGE_HEIGHT);
#ifdef USE_GNOMECANVAS
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
	if(!(cc->provides & PROVIDES_OVERVIEW)){
		arr_song_overview_queue_for_update();
	}
#endif

	// toolbar - how many rows does it need?
	int width1 = toolbar_get_iconsize(arrange) * 24/* approx */ + 100;
	if(panel->dnd_ebox->allocation.width < width1){
		// move the zoom bars to the 2nd row
		gtk_widget_reparent(arrange->hzoom_scale, panel->toolbar_box[1]);
		gtk_widget_reparent(arrange->vzoom_scale, panel->toolbar_box[1]);
	}
	else if(panel->dnd_ebox->allocation.width > width1 + 16){ // hysteresis added to prevent toggle loop
		// move the zoom bars back to the 1st row
		gtk_widget_reparent(arrange->hzoom_scale, panel->toolbar_box[0]);
		gtk_widget_reparent(arrange->vzoom_scale, panel->toolbar_box[0]);
	}

	arr_hscrollbar_update(arrange);
	arr_vscrollbar_update(arrange);
}


static void
arr_on_realise (GtkWidget* widget)
{
	dbg(2, "widget=%s name='%s'", gtk_widget_get_name(widget), GDL_IS_DOCK_OBJECT(widget) ? GDL_DOCK_OBJECT(widget)->name : "");
	g_return_if_fail(AYYI_IS_ARRANGE(widget));

#if 0
	Arrange* arrange = (Arrange*)widget;

	if(SONG_LOADED) arr_signals_unblock(arrange);
	if(SONG_LOADED) gtk_widget_set_sensitive(arrange->tempobox, TRUE);

	toolbox_change_tool(arrange, TOOL_DEFAULT);     //so the widget gets painted properly.
#endif

	((GtkWidgetClass*)ayyi_arrange_parent_class)->realize(widget);
}


static void
arr_on_size_request (GtkWidget* widget, GtkRequisition* requisition)
{
	dbg(2, "req=%i x %i.", requisition->width, requisition->height);

	((GtkWidgetClass*)ayyi_arrange_parent_class)->size_request(widget, requisition);

#ifdef USE_CLUTTER
	//    ...needs sorting but is needed for Clutter.
	static int tmp = 0;
	if(GTK_WIDGET_REALIZED(widget)) tmp++;
	else tmp = 0;
	if(tmp > 1){
		gtk_widget_set_size_request(widget, 50, 50);
	}
#endif
}


static void
arr_on_style_set (GtkWidget* widget, GtkStyle* style)
{
}


static void
arr_on_song_load (GObject* _song, Arrange* arrange)
{
	PF;
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);

#ifdef USE_GNOMECANVAS
	if(!(cc->provides & PROVIDES_OVERVIEW)){
		arr_song_overview_queue_for_update();
	}
#endif

	track_list__update_all_pointers(arrange->priv->tracks);
	arr_track_pos_update(arrange);

	automation_on_song_load(arrange);

	arr_zoom_history_clear(arrange);
	arr_signals_unblock(arrange);

#if 0
	if(arrange->rulerbar) rulerbar_redraw(arrange->rulerbar);
#endif

	arr_update_track_selection(arrange, am_collection_selection_first(am_tracks));
	arr_trkctl_load(arrange);
	if(!(cc->provides & PROVIDES_TRACKCTL)){
		arr_trkctl_redraw(arrange); //needed to update button state

		//is currently probably needed to set outer widget size
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		track_list_box_on_dim_change(arrange);
#endif
	}
	arr_track_on_selection_change(arrange); // TODO is product of refactoring. check is needed

	arr_on_view_changed(arrange, AM_CHANGE_LEN | AM_CHANGE_WIDTH | AM_CHANGE_ZOOM_H | AM_CHANGE_HEIGHT | AM_CHANGE_ZOOM_H | AM_CHANGE_BACKGROUND_COLOUR);

	svgbutton_set_state_n(SVGBUTTON(arrange->priv->bsnap), song->snap_mode);
	arr_qbox_update(arrange);

	ArrTrk* at = NULL;
	while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL))){
		call(arrange->canvas->on_new_track, arrange, at);
	}

	void arr_canvas_parts_new (Arrange* arrange)
	{
#ifdef DEBUG
#ifdef USE_OPENGL
		extern int arr_gl_parts_count(Arrange*);
		if(CANVAS_IS_OPENGL) g_return_if_fail(!arr_gl_parts_count(arrange));
#endif
#endif

		part_foreach {
			dbg (2, "part foreach...");
			if(arrange->canvas->part_new) arrange->canvas->part_new(arrange, gpart);
		} end_part_foreach;
	}
	arr_canvas_parts_new(arrange);

	gtk_widget_set_sensitive(arrange->tempobox, TRUE);

	// TODO can we apply a cursor to the whole toplevel window?:
	if(arrange->canvas->widget) arr_cursor_reset(arrange, gtk_widget_get_toplevel(arrange->canvas->widget)->window);

	((AyyiPanel*)arrange)->model_is_loaded = true;
	printf("%ssong loaded%65s\n", white__r, white);

	call(arrange->canvas->on_song_load, arrange);
}


static void
arr_on_song_unload (GObject* _song, Arrange* arrange)
{
	g_signal_emit_by_name (arrange, "song-unload");

	// TODO move this into signal handlers for the above signal ?
	arr_track_undraw_all(arrange); // remove any existing gui track widgets. 
}


static GList*
arr_get_selected_parts (AyyiPanel* panel)
{
	// TODO use an iterator to avoid copying the list.

	return g_list_copy(((Arrange*)panel)->part_selection);
}


static void
arr_quantise (GtkAccelGroup* accel_group, gpointer user_data)
{
	// Quantise selected parts.

	PF;
	Arrange* arrange; if(!(arrange = ARRANGE_FIRST)) return;

	am_partlist_quantize(arrange->part_selection, NULL, NULL);
}


static void
arr_bsnap_on_click (GtkWidget* widget, gpointer _arrange)
{
	// Clicking the snap button steps to the next of the available snap modes.

	PF;
	Arrange* arr = (Arrange*)_arrange;

	//what is the buttons state?
	char* state_name = svgbutton_get_state(SVGBUTTON(widget));
	int mode = snap_mode_lookup(state_name);
	dbg (1, "state='%s' mode=%i", state_name, mode);

	song->snap_mode = mode;

	arr_statusbar_printf(arr, 1, SVGBUTTON(arr->priv->bsnap)->mouseover_text);
}


static gboolean
arr_on_key_press (GtkWidget* panel, GdkEventKey* event, gpointer user_data)
{
	// Trap keys that gtk likes to grab, eg cursor keys.
	// Currently used to change tracks.

	// TODO get a better understanding of what the cursor keys are supposed to do in gtk.
	// They change the focus, but in what order?

	int handled = false;

	Arrange* arrange = (Arrange*)panel;

	GtkWindow* window = GTK_WINDOW(gtk_widget_get_toplevel(panel));
	GtkWidget* focused = gtk_window_get_focus(window);

	if(!focused || focused == (GtkWidget*)arrange || focused == arrange->canvas->widget
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
			 || gtk_widget_is_ancestor(focused, arrange->track_list_box)
#endif
			){
		switch(event->keyval){
			case GDK_Return:
				dbg (0, "return!");
				break;
			case GDK_Up:
				dbg (3, "uparrow!");
				gtk_accel_groups_activate(G_OBJECT(window), GDK_uparrow, event->state);
				handled = true; //we dont want focus to change.
				break;
			case GDK_Down:
				gtk_accel_groups_activate(G_OBJECT(window), GDK_downarrow, event->state);
				handled = true;
				break;
			case GDK_Left:
				gtk_accel_groups_activate(G_OBJECT(window), GDK_leftarrow, event->state);
				handled = true;
				break;
			case GDK_Right:
				gtk_accel_groups_activate(G_OBJECT(window), GDK_rightarrow, event->state);
				handled = true;
				break;
			case GDK_KP_1:
			case GDK_KP_End:
				if(gtk_accel_groups_activate(G_OBJECT(window), 65436, event->state)){
					handled = true;
				}
#ifdef DEBUG
				else dbg (0, "GDK_KP_1: not handled.");
#endif
				break;
			case GDK_KP_2:
			case GDK_KP_Down:
				if(gtk_accel_groups_activate(G_OBJECT(window), GDK_KP_2, event->state)){
					handled = true;
				}
#ifdef DEBUG
				else dbg (0, "GDK_KP_2: not handled.");
#endif
				break;
			case GDK_Escape:
				dbg(1, "Esc");
				if(gtk_accel_groups_activate(G_OBJECT(window), 'f', event->state)){
					handled = true;
				}
#ifdef DEBUG
				else dbg (0, "edit end (f): not handled.");
#endif
				break;
			case GDK_Shift_L:
				dbg(1, "SHIFT");
				break;
			case GDK_Control_L:
				// TODO move this to a handler for the zoom tool.
				dbg(1, "CTL");
				break;
			default:
				dbg(2, "keyval=%i %x", event->keyval, event->keyval);
				break;
		}
	}
#if 0
	else print_widget(focused);
#endif

	return handled;
}


static gboolean
arr_on_key_release (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
{
	keypress_reset();

	switch(event->keyval){
		case GDK_Shift_L:
			break;
		case GDK_Control_L:
			break;
		default:
			break;
	}
	return NOT_HANDLED;
}


static SignalNfo*
arr_signal_new (Arrange* arrange, GtkWidget* widget, const char* detail, GCallback callback, gpointer user_data)
{
	// Signal wrapper to allow disabling of functions when song is not loaded.

	SignalNfo* sig = AYYI_NEW(SignalNfo,
		.widget     = widget,
		.callback   = callback,
		.user_data  = user_data,
		.handler_id = g_signal_connect(widget, detail, callback, user_data)
	);
	g_strlcpy(sig->detail, detail, 32);

	dbg (3, "connecting '%s': --> id=%u", detail, sig->handler_id);

	arrange->signals = g_list_append(arrange->signals, sig);

	return sig;
}


static void
arr_signals_block (Arrange* arrange)
{
	GList* l = arrange->signals;
	for(;l;l=l->next){
		SignalNfo* signal = l->data;
		g_signal_handler_block(signal->widget, signal->handler_id);
		signal->blocked = true;
	}
}


static void
arr_signals_unblock (Arrange* arrange)
{
	// Can be called multiple times, irrespective of whether the signal were previously blocked.

	GList* list = arrange->signals;
	for (;list;list=list->next){
		SignalNfo* signal = list->data;
		if (signal->blocked) g_signal_handler_unblock(signal->widget, signal->handler_id);
	}
}


#if 0
void
arr_foreach(void(*function)(Arrange*), gpointer data)
{
	GList* l = windows__get_by_type(AYYI_TYPE_ARRANGE);
	if(l){
		for(;l;l=l->next){
			Arrange* arr = (Arrange*)l->data;

			function(arr);
		}
		g_list_free(l);
	}
}
#endif


#ifdef USE_GNOMECANVAS
static void
arr_k_songmap_toggle (GtkAccelGroup* accel_group, gpointer data)
{
	Arrange* arrange = (Arrange*)windows__get_active();

	show_widget_if((GtkWidget*)arrange->songmap, !GTK_WIDGET_VISIBLE((GtkWidget*)arrange->songmap));
}


void
arr_song_map_toggle (Arrange* arrange)
{
	if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;

	SHOW_SONGMAP = !SHOW_SONGMAP;
	if(SHOW_SONGMAP && !arrange->songmap) arr_add_songmap(arrange);
	show_widget_if((GtkWidget*)arrange->songmap, SHOW_SONGMAP);
}
#endif


static void
arr_peak_view_toggle (Arrange* arrange)
{
	SHOW_PARTCONTENTS = !SHOW_PARTCONTENTS;

	arrange->canvas->on_view_change(arrange, AM_CHANGE_SHOW_PART_CONTENTS);
}


static void
_arr_song_overview_toggle (Arrange* arrange)
{
	SHOW_OVERVIEW = !SHOW_OVERVIEW;

	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
	if(cc->provides & PROVIDES_OVERVIEW){
		arrange->canvas->on_view_change(arrange, AM_CHANGE_SHOW_SONG_OVERVIEW);
	}else{
#ifdef USE_GNOMECANVAS
		arr_song_overview_toggle(arrange);
#endif
	}
}


static void
arr_add_rulerbar (Arrange* arrange)
{
#ifndef DEBUG_DISABLE_RULERBAR
#ifdef USE_GNOMECANVAS
	int position = arrange->songmap ? 1 : 0;
#else
	int position = 0;
#endif

	void text_out(AyyiPanel* panel, const char* format, ...)
	{
		va_list args;
		va_start(args, format);
		gchar* s = g_strdup_vprintf(format, args);
        shell__statusbar_print(panel->window, 2, s);
		g_free(s);
		va_end(args);
	}

	gtk_box_pack_start(GTK_BOX(arrange->priv->vbox_rhs), arrange->ruler = time_ruler_new(arrange), FALSE, FALSE, 0);
	sw_format f = {0, 44100};
	time_ruler_set_format((TimeRuler*)arrange->ruler, &f);
	((TimeRuler*)arrange->ruler)->text_out = text_out;
	gtk_box_reorder_child(GTK_BOX(arrange->priv->vbox_rhs), arrange->ruler, position);
	gtk_widget_show(arrange->ruler);

	void arr_on_layout_change(Arrange* arrange)
	{
		// respond to changes in widget sizes or packing order.

	#ifdef USE_GNOMECANVAS
		int height = arrange->canvas->vtab[1] = arrange->songmap->height + arrange->ruler_height;
	#else
		int height = arrange->canvas->vtab[1] = arrange->ruler_height;
	#endif
		gtk_widget_set_size_request(arrange->priv->tcl_top_space, -1, height);
	}

	void arr_on_ruler_realise (GtkWidget* widget, gpointer _arr)
	{
		Arrange* arrange = _arr;
		TimeRuler* time_ruler = TIME_RULER(widget);
		if(time_ruler->height != time_ruler->arrange->ruler_height){
			arrange->ruler_height = time_ruler->height;
			arr_on_layout_change(arrange);
		}
	}
	g_signal_connect(G_OBJECT(arrange->ruler), "realize", G_CALLBACK(arr_on_ruler_realise), arrange);
#endif
}


#ifdef USE_GNOMECANVAS
static void
arr_add_songmap (Arrange* arrange)
{
	GtkWidget* songmap = songmap_new(arrange);

	gtk_box_pack_start(GTK_BOX(arrange->priv->vbox_rhs), songmap, FALSE, FALSE, 0);
	gtk_box_reorder_child(GTK_BOX(arrange->priv->vbox_rhs), songmap, 0);

	void arr_on_map_part_new(SongMap* songmap, AMPart* part, gpointer user_data)
	{
		Arrange* arrange = user_data;
		map_part_new(arrange, part);
		songmap_redraw(arrange);
	}

	// TODO remove signal on destroy
	g_signal_connect(G_OBJECT(arrange->songmap), "new-part", G_CALLBACK(arr_on_map_part_new), arrange);
}
#endif


static void
arr_qbox_update (Arrange* arrange)
{
	// Load the settings into arrange page drop down selector:

	GtkComboBox* combo = GTK_COMBO_BOX(arrange->q_combo);
	int size = Q_MAX;
	int i; for(i=0;i<size;i++) gtk_combo_box_remove_text(combo, 0);
	for(i=0;i<Q_MAX;i++){
		gtk_combo_box_append_text(combo, song->q_settings[i].name);
	}
	gtk_combo_box_set_active(combo, 0);
}


#ifdef USE_GNOMECANVAS
static void
_on_midi_note_rollover (GtkWidget* widget, int note_num)
{
	AyyiPanel* panel = windows__get_panel_from_widget(widget);
	if (panel) {
		shell__statusbar_print(panel->window, 2, "%s", note_format(note_num));
	}
}
#endif


static GtkWidget*
arr_trk_ctl_factory (Arrange* arrange, ArrTrk* atr)
{
	g_return_val_if_fail(atr, NULL);
	AMTrack* tr = atr->track;
	dbg(2, "tr=%s", tr->name);
	switch (tr->type){
		case TRK_TYPE_MIDI:
#ifdef DEBUG_TRACKCONTROL
			atr->trk_ctl = gtk_button_new();
#else
#ifdef USE_GNOMECANVAS
			atr->trk_ctl = track_control_midi_new(atr, 0);
			g_signal_connect(((TrackControlMidi*)atr->trk_ctl)->keys, "change_highlight", G_CALLBACK(_on_midi_note_rollover), NULL);
#endif
#endif
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
			gtk_box_pack_start(GTK_BOX(((TrackListBox*)arrange->track_list_box)->vbox), GTK_WIDGET(atr->trk_ctl), EXPAND_FALSE, FILL_FALSE, 0);
#endif
			break;
		case TRK_TYPE_AUDIO:
#ifdef DEBUG_TRACKCONTROL
			atr->trk_ctl = gtk_button_new();
#else
#ifdef USE_GNOMECANVAS
			atr->trk_ctl = (GtkWidget*)track_control_audio_new(atr, arrange, 0);
#endif
#endif
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
			gtk_box_pack_start(GTK_BOX(((TrackListBox*)arrange->track_list_box)->vbox), GTK_WIDGET(atr->trk_ctl), EXPAND_FALSE, FILL_FALSE, 0);
#endif
			break;
		default:
			pwarn("cant make track_control for NULL track type.");
			break;
	}

	return atr->trk_ctl;
}


static void
arr_part_editing_start (Arrange* arrange)
{
	ArrCanvas* c = arrange->canvas;

	arrange->part_editing = true;

	if(c->part_editing_start)
		c->part_editing_start(arrange, (AMPart*)arrange->part_selection->data);

	gtk_window_add_accel_group(GTK_WINDOW(arrange->panel.window), AYYI_ARRANGE_GET_CLASS(arrange)->midi_accel_group);
	g_object_set_data(G_OBJECT(AYYI_ARRANGE_GET_CLASS(arrange)->midi_accel_group), "sm_window", arrange);
}


static void
arr_part_editing_stop (Arrange* arrange)
{
	ArrCanvas* c = arrange->canvas;

	if(!arrange->part_selection) return;
	if(!arrange->part_editing) return;

	arrange->part_editing = false;
	AMPart* part = arrange->part_selection->data;
	if(c->part_editing_stop) c->part_editing_stop(arrange, part);
	gtk_window_remove_accel_group(GTK_WINDOW(arrange->panel.window), AYYI_ARRANGE_GET_CLASS(arrange)->midi_accel_group);
}


static void
arr_on_tracks_change (GObject* _song, AMTrackNum t_num1, AMTrackNum t_num2, int change_type, Arrange* arrange)
{
	dbg(2, "t1=%i t2=%i", t_num1, t_num2);
	g_return_if_fail(t_num1 >= 0 && t_num2 <= AM_MAX_TRK);

	if((change_type & AM_CHANGE_SOLO)){
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, (int)arr_t_to_d(arrange->priv->tracks, t_num1));
		if(atr && atr->trk_ctl){
			GtkWidget* button = TRACK_CONTROL(atr->trk_ctl)->bsolo;
			g_return_if_fail(button);
			if(am_track__is_solod(song->tracks->track[t_num1])) track_button_set_active(button); else track_button_set_not_active(button);
		}
		#endif
		#endif
	}

	if((change_type & AM_CHANGE_TRACK)){
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		TrackNum t; for(t=t_num1;t<=t_num2;t++){
			ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, (int)arr_t_to_d(arrange->priv->tracks, t));
			track_control_set_track(TRACK_CONTROL(atr->trk_ctl), atr);
		}
		#endif
		#endif
	}

	if ((change_type & AM_CHANGE_HEIGHT)) {
		dbg(2, "height changed: %.2f", track_list__track_by_index(arrange->priv->tracks, t_num1)->height * vzoom(arrange));
		arr_track_pos_update(arrange);
		for (AMTrackNum t=t_num1;t<=t_num2;t++) {
			#ifndef DEBUG_TRACKCONTROL
			CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
			if (cc->provides & PROVIDES_TRACKCTL) {
				arr_on_view_changed(arrange, AM_CHANGE_ZOOM_V);
			} else {
				#ifdef USE_GNOMECANVAS
				ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, song->tracks->track[t]);
				track_control_set_height(TRACK_CONTROL(atr->trk_ctl), atr->height * vzoom(arrange));
				#endif
			}
			#endif
		}
		#ifdef USE_GNOMECANVAS
		if (CANVAS_IS_GNOME) arrange->canvas->redraw(arrange);
		automation_view_on_size_change(arrange);
		#endif
	}

	if ((change_type & AM_CHANGE_NAME)) {
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);
		if (!(cc->provides & PROVIDES_TRACKCTL)) {
			TrackNum t; for(t=t_num1;t<=t_num2;t++){
				ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, (int)arr_t_to_d(arrange->priv->tracks, t));
				if(atr){
					TrackControl* tc = TRACK_CONTROL(atr->trk_ctl);
					if(tc) track_control_set_tname(tc);
				}
			}
		}
		#endif
		#endif
	}

	#ifdef USE_GNOMECANVAS
	if((change_type & AM_CHANGE_OUTPUT)){
		dbg(2, "output changed...");
		int t; for(t=t_num1;t<=t_num2;t++){
			if(track_list__track_by_index(arrange->priv->tracks, t)){
				TrackControl* tc = TRACK_CONTROL(track_list__track_by_index(arrange->priv->tracks, t)->trk_ctl);
				if(tc) track_control_set_channel_name (tc);
			}
		}
	}
	#endif

	if((change_type & AM_CHANGE_ARMED)){
		dbg(1, "ARMED changed.");
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		bool armed = am_track__is_armed(song->tracks->track[t_num1]);
		if(((CanvasClass*)g_hash_table_lookup(canvas_types, &arrange->canvas->type))->provides & PROVIDES_TRACKCTL){
			arr_on_view_changed(arrange, AM_CHANGE_ARMED);
		}else{
			GtkWidget* button = TRACK_CONTROL(track_list__track_by_index(arrange->priv->tracks, t_num1)->trk_ctl)->brec;
			g_return_if_fail(button);
			if(armed){
				track_button_set_active(button);
				arr_statusbar_printf(arrange, 1, "track armed");
			} else {
				track_button_set_not_active(button);
				arr_statusbar_printf(arrange, 1, "track disarmed");
			}
		}
		#endif
		#endif
	}

	if((change_type & AM_CHANGE_MUTE)){
		AMTrack* track = song->tracks->track[t_num1];
		g_return_if_fail(track);
		dbg(2, "MUTE changed. muted=%i", am_track__is_muted(track));
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		ArrTrk* at = track_list__track_by_index(arrange->priv->tracks, t_num1);
		if(at && at->trk_ctl){
			GtkWidget* button = TRACK_CONTROL(at->trk_ctl)->bmute;
			g_return_if_fail(button);

			if(am_track__is_muted(track))
				track_control_set_muted(TRACK_CONTROL(at->trk_ctl));
			else
				track_control_set_unmuted(TRACK_CONTROL(at->trk_ctl));
		}
		#endif
		#endif
	}

	// TODO this also needs to be in arr_on_tracks_change_by_list() ?
	if((change_type & AM_CHANGE_COLOUR)){
		//apply colour to the track buttons:
		#ifndef DEBUG_TRACKCONTROL
		#ifdef USE_GNOMECANVAS
		int t; for(t=t_num1;t<=t_num2;t++){
			ArrTrk* at = track_list__track_by_index(arrange->priv->tracks, t);
			TrackControl* tc = TRACK_CONTROL(at->trk_ctl);
			if(tc){
				track_control_set_colour(tc, am_track__get_colour(song->tracks->track[t]));
			}
		}
		#endif
		#endif
	}
}


static void
arr_on_track_change (GObject* _collection, AMTrack* track, AMChangeType change_type, Arrange* arrange)
{
	dbg(1, "...");
}


static void
arr_on_track_add (GObject* _song, AMTrack* tr, gpointer user_data)
{
	// TODO can be called multiple times in quick succession. Make an idle fn? Maybe just 2nd half?

	void arr_on_new_track (Arrange* arrange, ArrTrk* atr, bool is_new)
	{
		PF;

		automation_setup(atr);

		if (!(((CanvasClass*)g_hash_table_lookup(canvas_types, &arrange->canvas->type))->provides & PROVIDES_TRACKCTL)) {
			arr_trk_ctl_factory(arrange, atr);
		}

		call(arrange->canvas->on_new_track, arrange, atr);
	}

	if (!SONG_LOADED) return;

	dbg(2, "tr=%s", tr->name);

	arrange_foreach {
		CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);

		bool is_new = !track_list__lookup_track(arrange->priv->tracks, tr);

		ArrTrk* at = is_new ? track_list__add(arrange->priv->tracks, tr) : track_list__lookup_track(arrange->priv->tracks, tr);

		if (!is_new) {
			dbg(2, "...already have ArrTrk.");
			if(!(cc->provides & PROVIDES_TRACKCTL) && !at->trk_ctl){
				pwarn("  ... but NO trk_ctl! is this a bug? check arrange->track array...");
				arr_on_new_track(arrange, at, is_new);
			}
		} else {
			dbg(2, "at not found. creating...");
			arr_on_new_track(arrange, at, is_new);
		}

		arr_track_pos_update(arrange); //must be done before the zoom
		if (arrange->zoom_follow) arr_zoom_to_song(arrange);
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
		if(!(cc->provides & PROVIDES_TRACKCTL)){
			arr_trkctl_redraw(arrange);
			track_list_box_on_dim_change(arrange);
		}
#endif
		arr_track_on_selection_change(arrange); // TODO is product of refactoring. check is needed
#ifdef USE_GNOMECANVAS
		if(CANVAS_IS_GNOME){
			// TODO this is the product of refactoring. check if is needed.
			// update vertical aspects of the canvas:
			gcanvas_scrollregion_update(arrange);
			arrange->canvas->redraw(arrange); // Redraw canvas to match the increased vertical size.
		}
#endif

	} end_arrange_foreach
}


/*
 *  @param tracks - list of tracks (AMTrack*) that have been deleted.
 */
static void
arr_on_track_delete (GObject* _tracks, AMTrack* tr, Arrange* arrange)
{
	/*
	 * What exactly is the state here?
	 *
	 * AMTrack: AMTrack is still valid and present in the song->tracks array
	 *
	 * ArrTrk:  Is present at start but not at end
	 *
	 * track pointer lists ?
	 *          Indexes may have changed, so objects should be referenced by direct pointers, not by index.
	 */
	PF;

#ifdef DEBUG
	if(_debug_) printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> (Arrange TRACK OBSERVER START)\n\n");
#endif

	// Get references to old objects before the lists are updated.
	ArrTrk* atr = NULL;
	TrackDispNum d = -1;
	for(TrackNum t=0;t<AM_MAX_TRK;t++){
		ArrTrk* _atr = arrange->priv->tracks->track[t];
		if(_atr){
			if(_atr->track == tr){
				atr = _atr;
				d = arr_t_to_d(arrange->priv->tracks, t);
				break;
			}
		}
	}

	/*
	 *  Should this be run before or after canvas update?
	 *  -ideally would make no difference (ie canvas update should not access the pointers)
	 */
	track_list__remove(arrange->priv->tracks, atr);
	arr_track_pos_update(arrange);

	arrange->canvas->on_track_delete(arrange, atr);


	// Is the deleted track selected? if so select the nearest.
	if(g_list_find(arrange->tracks.selection, tr)){

		// its doubtful that this is reliable. Better to change the selection before the delete.

		if(d > -1) arr_track_select_nearest(arrange, d); 
	}

	arr_track_undraw(arrange, atr);

#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
	CanvasClass* cc = g_hash_table_lookup(canvas_types, &arrange->canvas->type);

	if(!(cc->provides & PROVIDES_TRACKCTL)){
		arr_trkctl_redraw(arrange);
		track_list_box_on_dim_change(arrange); // yes, this is still needed else the trkctl becomes too short
	}
#endif
	arr_track_on_selection_change(arrange); // TODO is product of refactoring. check is needed (and see arr_track_select_nearest above)

#ifdef USE_GNOMECANVAS // for backwards compatiblility
	if(CANVAS_IS_GNOME){
		// TODO this is the product of refactoring. check if is needed.
		// update vertical aspects of the canvas:
		gcanvas_scrollregion_update(arrange);

		arrange->canvas->redraw(arrange);
	}
#endif

	// prevent track being reused later
	atr->track = NULL;
#ifdef DEBUG
	atr->shm_idx = -1;
#endif

#ifdef DEBUG
	if(_debug_) printf("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< (Arrange TRACK OBSERVER END)\n");
#endif
}


static void
arr_on_track_selection_change (Observable* o, AMVal val, gpointer _arrange)
{
	PF;
	g_return_if_fail(SONG_LOADED); // error. should not be emitted

	Arrange* arrange = _arrange;
	g_return_if_fail(arrange);

	if(!((AyyiPanel*)arrange)->link) return;

	arr_update_track_selection(arrange, am_collection_selection_first(am_tracks));
}


static void
arr_statusbar_update_selection (Arrange* arrange)
{
	int selection_count = g_list_length(am_parts_selection);
	if(selection_count == 1){
		arr_statusbar_printf(arrange, 2, "Part %i selected.", ((AMPart*)arrange->part_selection->data)->ayyi->shm_idx);
	}else{
		arr_statusbar_printf(arrange, 2, "%i parts selected.", selection_count);
	}
}


static void
arr_on_part_selection_change (GObject* am_song, AyyiPanel* sender, Arrange* arrange)
{
	// Note: when linked, the selection list is just an alias to the song list.

	PF;
	if((AyyiPanel*)arrange != sender){
		if(((AyyiPanel*)arrange)->link){
			dbg(1, "linked. n_selected=%i", g_list_length(am_parts_selection));
			// TODO -perhaps use GList** ?
			arrange->part_selection = am_parts_selection;

			call(arrange->canvas->set_part_selection, arrange);

			arr_scroll_to_selection(arrange);

			arr_statusbar_update_selection(arrange);
		}
	}
}


static void
arr_scroll_to_selection (Arrange* arrange)
{
	// Currently only handles horizontal scrolling, though to save having 2 separate scrolls it might be better to merge arr_scroll_to_track into here.

	if(arrange->part_selection && ((AyyiPanel*)arrange)->follow){
		AyyiSongPos start, end;
		TrackNum track_top, track_bottom;
		int selection_start = 10000000, selection_end = 0;
		am_partmanager__find_boundary(arrange->part_selection, &start, &end, &track_top, &track_bottom);
		selection_start = arr_spos2px(arrange, &start);
		selection_end   = arr_spos2px(arrange, &end);

		if((selection_start < selection_end) && !ayyi_panel_is_pressed((AyyiPanel*)arrange)){

			AGliRegion c;
			arrange->canvas->get_scroll_offsets(arrange, &c.x1, &c.y1);
			c.x2 = c.x1 + (GTK_WIDGET(arrange->canvas->widget))->allocation.width;
			c.y2 = c.y1 + (GTK_WIDGET(arrange->canvas->widget))->allocation.height;
			dbg (1, "canvas=%i-->%i selection=%i-->%i", c.x1, c.x2, selection_start, selection_end);

			Pti scroll = {-1, -1};
			bool off_left = selection_end < c.x1;
			bool off_right = selection_start > c.x2;

			TrackDispNum d = arr_px2trk(arrange, c.y2);
			if(d > -1){
				ArrTrk* at = arrange->priv->tracks->display[d];
				ArrTrk* at1 = track_list__track_by_index(arrange->priv->tracks, track_top);
				TrackNum t = am_track_list_position(song->tracks, at->track);
				bool off_bottom = track_top > t;
				if(off_bottom) scroll.y = at1->y;
			}

			if(off_left || off_right){ // only scroll if it is completely offscreen
				// scrolling needed
				int spare_space = arrange->canvas->widget->allocation.width - (selection_end - selection_end);
				int left_margin = MIN(200, spare_space / 2);
				scroll.x = MAX(0, selection_start - left_margin);
				dbg(1, "scrolling to: %i margin=%i", selection_start - left_margin, left_margin);
			}

			if(scroll.x != -1 || scroll.y != -1){
				arrange->canvas->scroll_to(arrange, scroll.x, scroll.y);
			}
			else dbg(2, "not offscreen");
		}
	}
}


void
arr_scroll_to_track (Arrange* arrange, AMTrack* track)
{
	g_return_if_fail(track->visible);
	ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, track);
	g_return_if_fail(at);

#ifdef DEBUG
	ArrTrk* last_track = track_list__last_visible(arrange->priv->tracks);
#endif

	// Do we need to scroll the window?
	// note: the canvas sets the adjustment Upper value to match the canvas size in pixels.
	Size viewport = arr_canvas_viewport_size(arrange);
	//viewport consists of: top_hidden + visible + bottom_hidden.
#ifdef DEBUG
	double total_height   = (last_track->y + last_track->height) * vzoom(arrange);
#endif
	double visible_height = viewport.height;
	double vp_top         = arrange->canvas->v_adj->value;
	double vp_bottom      = vp_top + visible_height;
	if(visible_height < 2.0) return;

	int track_top = at->y * vzoom(arrange);
	int track_bot = (at->y + at->height) * vzoom(arrange);
	dbg (2, "vptop=%.1f trk.y=%i adj=%.1f total=%.1f visibleheight=%.1f upper=%.1f", vp_top, track_top, gtk_adjustment_get_value(arrange->canvas->v_adj), total_height, visible_height, arrange->canvas->v_adj->upper);
	// alternatively, we could check the trk_ctl->window's ?

	if(track_top < vp_top){
		// scroll up
		dbg (2, "autoscrolling canvas up! top=%.1f t.y=%i", vp_top, track_top);
		arrange->canvas->scroll_to(arrange, -1, track_top);
	}
	else if(track_bot > vp_bottom){
		// scroll down
		//dbg (2, "autoscrolling canvas down... %i > %.1f t=%i %.1f --> %.1f", track_bot, vp_bottom, arr_t_to_d(arrange->priv->tracks, track->track_num), vp_top, track_bot - visible_height);
		#define EX 10
		arrange->canvas->scroll_to(arrange, -1, track_bot - visible_height + EX);
	}
	else dbg(2, "no scrolling needed.");
}


static void
arr_hscrollbar_update (Arrange* arrange)
{
	float visible_x_px = arrange->canvas->widget->allocation.width;
	float total_x_px = arr_canvas_width(arrange);

	GtkAdjustment* h_adj = arrange->canvas->h_adj;
	h_adj->lower = 0;
	h_adj->upper = total_x_px;
	h_adj->step_increment = visible_x_px / 100;
	h_adj->page_increment = visible_x_px;
	h_adj->page_size = visible_x_px;

	gtk_adjustment_changed(h_adj);

#if 0
	show_widget_if(c->hscrollbar, fx < 1.0);
#endif
}


static void
arr_vscrollbar_update (Arrange* arrange)
{
	if(!arrange->canvas) return;

	float visible_y_px = arrange->canvas->widget->allocation.height;
	float total_y_px = arr_canvas_height(arrange) + arrange->ruler_height;

	GtkAdjustment* v_adj = arrange->canvas->v_adj;
	v_adj->lower = 0;
	v_adj->upper = total_y_px;
	v_adj->step_increment = visible_y_px / 64;
	v_adj->page_increment = visible_y_px;
	v_adj->page_size = visible_y_px;

	gtk_adjustment_changed(v_adj);
}


static void
arr_on_part_change (GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, Arrange* arrange)
{
	dbg (2, "redrawing arrange part...");
	arrange->canvas->on_part_change(arrange, part);

	if((change_type & AM_CHANGE_NAME)){
#ifdef USE_GNOMECANVAS
		if(CANVAS_IS_GNOME){
			GnomeCanvasItem* item = gcanvas_get_part_widget(arrange, part);
			if(item) gnome_canvas_item_set(item, "label", part->name, NULL);
		}
#endif
	}

#if 0
	if((change_type & CHANGE_COLOUR)){
	}
#endif
}


static void
arr_on_songmap_change (GObject* _song, Arrange* arrange)
{
#ifdef USE_GNOMECANVAS
	songmap_set_parts(arrange);
#endif
}


static void
arr_on_song_length_change (GObject* _song, Arrange* arrange)
{
	arr_songlength_update(arrange);
}


static void
arr_on_start_meters (GObject* app, Arrange* arrange)
{
#ifdef USE_GNOMECANVAS
	if(CANVAS_IS_GNOME) gnome_canvas_item_show(arrange->canvas->gnome->spp);
#endif
}


static void
arr_on_part_new (GObject* _song, AMPart* part, Arrange* arrange)
{
	g_return_if_fail(AYYI_IS_ARRANGE(arrange));

	arr_part_widgets_new(arrange, part);
}


static void
arr_on_part_delete (GObject* _parts, AMPart* part, Arrange* arrange)
{
	PF;
	arr_part_selection_remove(arrange, part);

	arrange->canvas->part_delete(arrange, part);
}


static void
arr_on_locators (GObject* _song, Arrange* arrange)
{
	if(arrange->canvas) call(arrange->canvas->on_locator_change, arrange);
#ifndef DEBUG_DISABLE_RULERBAR
	if(arrange->ruler) time_ruler_update_flags((TimeRuler*)arrange->ruler);
#endif
}


static void
arr_on_tracks_change_by_list (GObject* _song, GList* tracks, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	// @param tracks - list of AMTrack*.

	// pre-display updates
	if ((change_type & AM_CHANGE_HEIGHT || change_type & AM_CHANGE_TRACK)) {
		arrange_foreach {
			arr_track_pos_update(arrange);
		} end_arrange_foreach
	}

	GList* l = tracks;
	for (;l;l=l->next) {
#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		AMTrack* trk = l->data;
#endif

		if ((change_type & AM_CHANGE_TRACK)) {
#ifdef USE_GNOMECANVAS
			arrange_foreach {
				ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, trk);
				if (at->trk_ctl) track_control_set_track(TRACK_CONTROL(at->trk_ctl), at);
			} end_arrange_foreach
#endif
			change_type = change_type | AM_CHANGE_HEIGHT;
		}
#endif

		if ((change_type & AM_CHANGE_HEIGHT)) {
#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
			arrange_foreach
			ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, trk);
			//dbg(1, "height changed: t=%i d=%i height=%.2f at=%p tcl=%p", t, arr_t_to_d(arrange->priv->tracks, t), at->height * vzoom(arrange), at, at->trk_ctl);
			if(at->trk_ctl) track_control_set_height(TRACK_CONTROL(at->trk_ctl), at->height * vzoom(arrange));
			end_arrange_foreach
#endif
#endif
		}
	}

	if ((change_type & AM_CHANGE_NAME)) {
#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		arrange_foreach {
			for (l=tracks;l;l=l->next) {
				AMTrack* trk = l->data;
				track_control_set_tname(TRACK_CONTROL(track_list__lookup_track(arrange->priv->tracks, trk)->trk_ctl));
			}
		} end_arrange_foreach
#endif
#endif
	}

	if ((change_type & AM_CHANGE_OUTPUT)) {
#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		arrange_foreach {
			for (l=tracks;l;l=l->next) {
				AMTrack* trk = l->data;
				track_control_set_channel_name (TRACK_CONTROL(track_list__lookup_track(arrange->priv->tracks, trk)->trk_ctl));
			}
		} end_arrange_foreach
#endif
#endif
	}

	if ((change_type & AM_CHANGE_HEIGHT || change_type & AM_CHANGE_TRACK)) {
#ifdef USE_GNOMECANVAS
		arrange_foreach {
			if (CANVAS_IS_GNOME) arrange->canvas->redraw(arrange);
			automation_view_on_size_change(arrange);
		} end_arrange_foreach
#endif
	}

	arrange_foreach {
		arrange->canvas->on_view_change(arrange, change_type);
	} end_arrange_foreach
}


static void
arr_on_plugin_changed (GObject* _song, AMChannel* ch, gpointer user_data)
{
	// Reset the track menu

	arrange_foreach {
		g_object_set_data(G_OBJECT(arrange->tracks.menu), "for_track_idx", NULL);
	} end_arrange_foreach
}


static void
arr_on_tempo_change (GObject* _song, gpointer user_data)
{
	arrange_foreach {
		if(arrange->ruler) time_ruler_set_zoom((TimeRuler*)arrange->ruler, arr_samples_per_pix(arrange), arrange->canvas->get_viewport_left(arrange));

		/*
			-what happens when the tempo changes? options:
				1) bbt scale (hzoom) stays the same - this means the part scale will change, as will the ruler time scale
				2) part display stays the same - bbt scale and hzoom change **** <--- this one is correct

			-need to calculate the new hzoom value.
			-we know how many samples are supposed to be displayed
				-how do we know this? the old zoom is based on old tempo and old bbt-zoom.
			 --> samples/px
			     --> beats/px


		*/

		if(!arrange->canvas) continue;

		int n_px = arrange->canvas->widget->allocation.width;
		int n_samples = n_px * arr_samples_per_pix(arrange); //samples_per_pix is fixed - it doesnt change when tempo changes.

		/*
		float new_unzoomed_px = samples2px_nz((uint64_t)n_samples); // the number of unzoomed pixels for the new tempo.
		float hzoom = n_px / new_unzoomed_px;
		*/
		float beats_per_second = ((AyyiSongService*)ayyi.service)->song->bpm / 60.0;
		float n_seconds = n_samples / song->sample_rate;
		float n_beats = n_seconds * beats_per_second;
		float hzoom = n_px / (n_beats * PX_PER_BEAT);

#if 0
		float n_beats = arr_px2beat(arrange, n_px); //cannot use this, as its based on old hzoom, not tempo
		float samples2secs = 1 / song->sample_rate;
		float n_secs_per_px = arr_samples_per_pix(arrange)/* also based on old zoom */ * samples2secs;
		float n_seconds = n_px * n_secs_per_px;
		float beats_per_px = n_beats / n_px;
		float hzoom = 1 / (beats_per_px * PX_PER_BEAT);
#endif
		dbg(1, "%.1f hzoom: %.3f --> %.3f samples_per_pix=%u n_beats=%.3f", ((AyyiSongService*)ayyi.service)->song->bpm, hzoom(arrange), hzoom, arr_samples_per_pix(arrange), n_beats);

		arr_zoom_set(arrange, hzoom, vzoom(arrange));
	} end_arrange_foreach
}


static void
arr_on_focus_changed (GObject* _ap, gpointer _)
{
	if(AYYI_IS_ARRANGE(app->active_panel)) app->latest_arrange = (Arrange*)app->active_panel;
}


static bool recording;

static void
arr_on_transport_stop (GObject* _song, gpointer user_data)
{
	arrange_foreach {
		if(recording) arr_post_recording(arrange);
	} end_arrange_foreach

	recording = false;
}


static void
arr_on_periodic (GObject* _song, gpointer user_data)
{
	static int filter = 0;
	if(!filter){
		if(ayyi_transport_is_recording()) recording = TRUE;
	}
	if(filter++ > 6) filter = 0;

	// move the arrange page spp cursor line
	arr_set_spp_samples(ayyi_transport__get_frame());

	arrange_foreach {
		if (ayyi_transport_is_playing() && arrange->panel.follow){
			if(!ayyi_panel_is_pressed((AyyiPanel*)arrange)){
				g_return_if_fail(arrange->canvas);
				call(arrange->canvas->do_follow, arrange);
			}
		}

		if(ayyi_transport_is_recording()){
			arr_while_recording(arrange);
		}
	} end_arrange_foreach
}


void**
arr_get_config (AyyiPanel* panel)
{
	// Returned value must be g_free'd

	Arrange* arrange = (Arrange*)panel;
	static char value[16] = {0,};
	static char* v;

	static int cx, cy; arrange->canvas->get_scroll_offsets(arrange, &cx, &cy);

	void* b[] = {
		&panel->zoom->value.pt.x,
		&vzoom(arrange),
		&((CanvasClass*)g_hash_table_lookup(canvas_types, &arrange->canvas->type))->name,
		&cx,
		&cy,
#if 0
		(g_hash_table_lookup(canvas_types, &arrange->canvas->type)->provides & PROVIDES_TRACKCTL) ? &arrange->tc_width.val.i : &arrange->track_list_box->allocation.width;
#else
		&arrange->tc_width.val.i,
#endif
		&arrange->view_options[SHOW_SONG_OVERVIEW].value,
#ifdef USE_GNOMECANVAS
		&arrange->view_options[SHOW_SONG_MAP].value,
#else
		NULL,
#endif
		&arrange->view_options[SHOW_PART_CONTENTS].value,
		(snprintf(value, 15, "0x%08x", arrange->bg_colour), v = value, &v)
	};

	void** config = g_malloc0(sizeof(void*) * G_N_ELEMENTS(params));
	memcpy(config, b, sizeof(void*) * G_N_ELEMENTS(b));

	return config;
}


static void
arr_add_load_handlers (AyyiArrangeClass* klass)
{
	void config_load_arrange_tracks (yaml_parser_t* parser, const char* key, gpointer data)
	{
		AyyiArrangeClass* arrange_class = g_type_class_peek(AYYI_TYPE_ARRANGE);
		((AyyiPanelClass*)arrange_class)->loader.instances++;

		struct _M
		{
			ArrangeSongStorage* storage;
			int i;
		} m = {
			.storage = g_new0(ArrangeSongStorage, 1)
		};

		void config_load_arrange_track (yaml_parser_t* parser, const char* key, gpointer data)
		{
			struct _M* m = data;

			YamlValueHandler vhandlers[] = {
				{"height", yaml_set_int, &m->storage->height[m->i]},
				{NULL}
			};
			yaml_load_mapping(parser, NULL, vhandlers, NULL);

			m->i++;
		}

		YamlHandler handlers[] = {
			{"", config_load_arrange_track, &m},
			{NULL}
		};
		yaml_load_mapping(parser, handlers, NULL, NULL);

		arrange_class->song_storage = g_list_append(arrange_class->song_storage, m.storage);

		AyyiPanel* arrange = windows__get_from_instance(AYYI_TYPE_ARRANGE, ((AyyiPanelClass*)arrange_class)->loader.instances);
		if (arrange)
			arr_apply_song_prefs((Arrange*)arrange);
		else
			pwarn("no such instance: %i", ((AyyiPanelClass*)arrange_class)->loader.instances);
	}

	YamlHandler _handlers[] = {
		{"tracks", config_load_arrange_tracks, NULL},
		{NULL}
	};

	// Convert to pointer array
	YamlHandler** handlers = (YamlHandler**)g_malloc0(sizeof(YamlHandler) * 2);
	for(int i=0;i<2;i++){
		handlers[i] = g_new0(YamlHandler, 1);
		memcpy((YamlHandler*)handlers[i], &_handlers[i], sizeof(YamlHandler));
	}

#if 0
	YamlValueHandler vhandlers[] = {
		{NULL}
	};
#endif
	YamlValueHandler* vhandlers = g_malloc0(1 * sizeof(YamlValueHandler));

	((AyyiPanelClass*)klass)->loader.handlers = handlers;
	((AyyiPanelClass*)klass)->loader.vhandlers = (YamlValueHandler**)vhandlers;
}


static void
k_zoom_in_vert (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	if(panel){
		float zoom = panel->zoom->value.pt.y * 1.25;
		observable_point_set(panel->zoom, NULL, &zoom);
	}
}


static void
k_zoom_out_vert (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;
	if(panel){
		float zoom = panel->zoom->value.pt.y / 1.25;
		observable_point_set(panel->zoom, NULL, &zoom);
	}
}


