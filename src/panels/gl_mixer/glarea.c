/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "agl/utils.h"
#include "gtkglext-1.0/gtk/gtkgl.h"
#include "glarea.h"

extern GdkGLConfig* app_get_glconfig ();

#define DIRECT TRUE

static gpointer gl_area_parent_class = NULL;

enum  {
	GL_AREA_0_PROPERTY
};

static void gl_area_realize   (GtkWidget*);
static void gl_area_unrealize (GtkWidget*);
static bool gl_area_event     (GtkWidget*, GdkEvent*);


GlArea*
gl_area_construct (GType object_type)
{
	return (GlArea*) g_object_new (object_type, NULL);
}


GlArea*
gl_area_new (void)
{
	return gl_area_construct (TYPE_GL_AREA);
}


static void
gl_area_realize (GtkWidget* widget)
{
	gtk_widget_set_gl_capability(widget, app_get_glconfig(), agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);
	GTK_WIDGET_CLASS(gl_area_parent_class)->realize (widget);
}


static void
gl_area_unrealize (GtkWidget* widget)
{
	PF;
	GTK_WIDGET_CLASS(gl_area_parent_class)->unrealize (widget);
}


static void
gl_area_class_init (GlAreaClass* klass)
{
	gl_area_parent_class = g_type_class_peek_parent (klass);
	((GtkWidgetClass *) klass)->realize = (void (*) (GtkWidget *)) gl_area_realize;
	((GtkWidgetClass *) klass)->unrealize = (void (*) (GtkWidget *)) gl_area_unrealize;
	((GtkWidgetClass *) klass)->event = (gboolean (*) (GtkWidget *, GdkEvent*)) gl_area_event;
}


static void
gl_area_instance_init (GlArea* self)
{
}


GType
gl_area_get_type (void)
{
	static volatile gsize gl_area_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&gl_area_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (GlAreaClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) gl_area_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (GlArea), 0, (GInstanceInitFunc) gl_area_instance_init, NULL };
		GType gl_area_type_id;
		gl_area_type_id = g_type_register_static (GTK_TYPE_DRAWING_AREA, "GlArea", &g_define_type_info, 0);
		g_once_init_leave (&gl_area_type_id__volatile, gl_area_type_id);
	}
	return gl_area_type_id__volatile;
}


static bool
gl_area_event (GtkWidget* widget, GdkEvent* event)
{
	g_return_val_if_fail (event, FALSE);

	return agl_actor__on_event(((GlArea*)widget)->scene, event);
}

