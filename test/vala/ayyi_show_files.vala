/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;
using AM;

public class Ayyi.ShowfilesClient : Ayyi.SimpleClient
{
	private static bool debug = true;

	public static int main (string[] args)
	{
		app = new ShowfilesClient();

		if(!app.run(ShowfilesClient.on_shm)) return 1;

		return 0;
	}

	private static void on_shm ()
	{
		print("-----------------------------------------------------------------\n");
		print("audio files:\n");
		if(AyyiSong.filesource_next(null) != null){
			if(debug){
				print("  idx %10s %10s %20s orig\n", "id", "length", "name");
			}else{
				print("  idx %10s name\n", "id");
			}
			AyyiFilesource* file = null;
			while((file = AyyiSong.filesource_next(file)) != null){
				if(debug){
					print("  %3i %10" + uint64.FORMAT + " %10i %20s %s\n", file->shm_idx, file->id, file->length, (string)file->name, (string)file->original_name);
				}else{
					print("  %3i %10" + uint64.FORMAT + " %s\n", file->shm_idx, file->id, (string)file->name);
				}
			}

			print("---\n");
			AM.PoolItem* pool_item = null;
			Model.Iter i;
			song->pool->iter_init (&i);
			print("  %1s %s %s\n", "c", "s", "name");
			while((pool_item = (AM.PoolItem*)song->pool->iter_next(&i)) != null){
				print("  %i %i %s\n", pool_item->channels, pool_item->state, (string)pool_item->leafname);
			}
		}
		else print("\tno audio files\n");
		print("-----------------------------------------------------------------\n");

		app.quit();
	}
}


