/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 |  UI Unit Tests
 |  -------------
 |
 |  These tests dont require connecting to any Ayyi services.
 |  All tests run synchronously without callbacks.
 |
 |  See also lib/ayyi/model/test/unit_test.c
 |
 */

#define __main_c__

#include "config.h"
#include "model/ayyi_model.h"
#include "lib/test/runner.h"

static void app_init ();

TestFn test_ayyi_list,
	test_mu_gpos_conversion,
	test_gpos_to_samples,
	test_samples_to_gpos,
	test_pos_to_gpos,
	test_gpos_divide,
	test_fader_law;

#define STOP FALSE;
#define OK 0
#define FAIL 1

gpointer tests[] = {
	test_ayyi_list,
	test_mu_gpos_conversion,
	test_gpos_to_samples,
	test_samples_to_gpos,
	test_pos_to_gpos,
	test_fader_law,
};

#define MAX_RECENT_SONGS 4
typedef struct _canvas_op canvas_op;

typedef struct
{
   int        state;

   bool       dbus;

   char       config_filename[256];
   GKeyFile*  key_file;

   char       recent_songs[MAX_RECENT_SONGS][256];
} App;

App app = {0,};


int
setup ()
{
	am_init(NULL);
	app_init();

	TEST.n_tests = G_N_ELEMENTS(tests);

	return 0;
}


void
teardown ()
{
}


static void
app_init ()
{
	if (!((AyyiSongService*)ayyi.service)->song) {
		((AyyiSongService*)ayyi.service)->song = AYYI_NEW(struct _shm_song,
			.bpm = 120,
			.sample_rate = 44100,
		);
	}

	ayyi.log.to_stdout = true;
}


void
test_ayyi_list ()
{
	// AyyiList's live only in shared memory, so can't be tested here.

	START_TEST;
	FINISH_TEST;
	return;

	AyyiList* ayyi_list_prepend (AyyiList* l, int i)
	{
		AyyiList* new = g_new0(AyyiList, 1);
		new->id = i;
		return new;
	}
	int id = 99;
	AyyiList* l = ayyi_list_prepend(NULL, id);
	if (l->id != id) FAIL_TEST("");
	if (!(ayyi_list__length(l) == 1)) FAIL_TEST("");
}


void
test_mu_gpos_conversion ()
{
	START_TEST;
	bool passed = true;

	#define CORRECTION_PER_BEAT 768

	uint64_t mu_beat = ((uint64_t)AYYI_TICKS_PER_BEAT) * ((uint64_t)AYYI_MU_PER_TICK) + CORRECTION_PER_BEAT;
	if (mu_beat != AYYI_MU_PER_BEAT) {
		dbg(0, "            !! %Lu != %Lu", mu_beat, ((uint64_t)AYYI_MU_PER_BEAT));
		FAIL_TEST("");
	}

	typedef struct {
		GPos     d;
		uint64_t mu;
	} F;

	F d[] = {{{0,0,0}}, {{0,0,1}}, {{0,1,0}}, {{0,1,1}}, {{0,1,2}}, {{0,1,3}}, {{0,3,383}}, {{1,0,0},AYYI_MU_PER_BEAT}, {{3,3,383}}, {{4,0,1}}, {{64,0,0},64*AYYI_MU_PER_BEAT}, {{1024,0,1}}};

	for (int i=0;i<G_N_ELEMENTS(d);i++) {
		F* f = &d[i];
		uint64_t mu = pos2mu(&f->d);
		mu += f->d.beat * CORRECTION_PER_BEAT;
		if (f->mu && mu != f->mu) dbg(0, "            !! %Lu != %Lu", mu, f->mu);
		GPos out; mu2pos(mu, &out);

		char bbst1[32]; pos2bbst(&f->d, bbst1);
		char bbst2[32]; pos2bbst(&out, bbst2);
		am_pos_is_valid(&out);
		if (pos_cmp(&f->d, &out)) {
			ayyi_log_print(LOG_FAIL, "test %i", i);
			passed = false;
		}
		dbg(1, "%s --> %s mu=%Li", bbst1, bbst2, mu);
	}

	assert(passed, "not passed");

	FINISH_TEST;
}


void
test_samples_to_gpos ()
{
	//GPos is lower resolution that samples, so translating samples -> gpos -> samples is not possible with accuracy.

	START_TEST;
	bool pass = true;
	static int64_t accuracy;
	float tempo[] = {60.0, 120.0, 240.0};
	uint32_t cases[] = {0, 1, 10, 22050, 44099, 44100, 44101};

	for (int t=0;t<G_N_ELEMENTS(tempo);t++) {
		((AyyiSongService*)ayyi.service)->song->bpm = tempo[t];
		accuracy = ayyi_samples2mu(1);

		for (int i=0;i<G_N_ELEMENTS(cases);i++) {
			uint32_t in = cases[i];
			GPos pos;
			samples2pos(in, &pos);
			uint32_t out = pos2samples(&pos);

			char bbst[64]; pos2bbst(&pos, bbst);

			int diff = ABS(out - in);
			if(diff > accuracy / 2){
				perr("in=%u out=%u %s err=%i", in, out, bbst, diff);
				pass = false;
			}
			else ayyi_log_print(LOG_OK, "%5u %s", in, bbst);
		}
	}
	assert(pass, "not passed");

	FINISH_TEST;
}


void
test_pos_to_gpos ()
{
	//TODO this gives useful output but does not check for errors.
	START_TEST;
	bool pass = true;
	static int64_t accuracy;
	accuracy = ayyi_samples2mu(1);
	AyyiSongPos cases[] = {{0,0,0}, {0,0,1}, {0,0,accuracy}, {0, 0, AYYI_MU_PER_SUB/2}, {0,0,AYYI_MU_PER_SUB-1}, {0,1,0}, {0,1,1}, {0,1,2}, {0,1,3}, {0,AYYI_SUBS_PER_BEAT/2,0}, {0,AYYI_SUBS_PER_BEAT-1,AYYI_MU_PER_SUB-1}, {1,0,0}, {3,3,383}, {4,0,1}, {127,0,0}, {128,0,0}, {511,0,0}};
	for (int i=0;i<G_N_ELEMENTS(cases);i++) {
		AyyiSongPos* in = &cases[i];
		char bbst1[64]; ayyi_pos2bbst(in, bbst1);
		GPos pos;
		songpos_ayyi2gui(&pos, in);
		char bbst2[64]; pos2bbst0(&pos, bbst2);
		dbg(0, "                                  %s %s", bbst1, bbst2);
		//if(in->beat != pos.beat) perr("!!! beat: %i!=%i", in->beat, pos.beat);
	}
	assert(pass, "not passed");

	FINISH_TEST;
}


void
test_gpos_to_samples ()
{
	START_TEST;
	assert(((AyyiSongService*)ayyi.service)->song, "song");

	bool pass = true;
	GPos cases[] = {/*{-4, 0, 0}, */{0, 0, 0}, {0, 0, 1}, {0, 0, 96}, {0, 0, 97}, {0, 3, 383}, {1, 1, 0}, {7, 0, 0}, {9, 0, 0}, {16, 0, 0}};
	for (int i=0;i<G_N_ELEMENTS(cases);i++) {
		GPos in = cases[i];
		uint32_t samples = pos2samples(&in);
		GPos out; samples2pos(samples, &out);

		char bbst1[64]; pos2bbst(&in, bbst1);
		char bbst2[64]; pos2bbst(&out, bbst2);
		if (!pos_cmp(&in, &out)) {
			ayyi_log_print(0, "%s %5u", bbst1, samples);
		} else {
			perr("%s != %s %5u", bbst1, bbst2, samples);
			pass = false;
		}
	}
	assert(pass, "");
	FINISH_TEST;
}


void
test_gpos_divide ()
{
	START_TEST;
	bool pass = true;
	int n = 2;
	GPos cases[]   = {{0, 0, 2}, {0, 2, 0}, {2, 0, 0}, {0, 1, 0},                          {1, 0,               0}};
	GPos results[] = {{0, 0, 1}, {0, 1, 0}, {1, 0, 0}, {0, 0, (AYYI_TICKS_PER_SUBBEAT/n)}, {0, SUBS_PER_BEAT/n, 0}};
	for (int i=0;i<G_N_ELEMENTS(cases);i++) {
		GPos pos = cases[i];
		pos_divide(&pos, n);

		char bbst0[64]; pos2bbst(&cases[i], bbst0);
		char bbst1[64]; pos2bbst(&pos, bbst1);
		char bbst2[64]; pos2bbst(&results[i], bbst2);
		if (!pos_cmp(&pos, &results[i])) {
			dbg(2, "%s --> %s", bbst0, bbst1);
		} else {
			perr("%s != %s", bbst1, bbst2);
			pass = false;
		}
	}
	assert(pass, "not passed");

	FINISH_TEST;
}


void
test_fader_law ()
{
	extern double ayyi_automation_xlate_out (double val); // convert pixel value to gain

	START_TEST;

	float cases[] = {0, 100, 50};
	float results[] = {2, 0, 0.299605};

	for (int i=0;i<G_N_ELEMENTS(cases);i++) {
		float result = ayyi_automation_xlate_out(cases[i]);
		assert(ABS(result - results[i]) < 0.00001, "case=%f: expected %f, got %f", cases[i], results[i], result);
	}

	FINISH_TEST;
}

