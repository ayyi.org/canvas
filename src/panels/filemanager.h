/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __panels_filemanager_h__
#define __panels_filemanager_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_FILE_PANEL            (ayyi_file_panel_get_type ())
#define AYYI_FILE_PANEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_FILE_PANEL, AyyiFilePanel))
#define AYYI_FILE_PANEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_FILE_PANEL, AyyiFilePanelClass))
#define AYYI_IS_FILE_PANEL(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_FILE_PANEL))
#define AYYI_IS_FILE_PANEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_FILE_PANEL))
#define AYYI_FILE_PANEL_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_FILE_PANEL, AyyiFilePanelClass))

typedef struct _AyyiFilePanelClass AyyiFilePanelClass;

#define filemanager_foreach GList* swindows = windows__get_by_type(TYPE_FILE_PANEL); if(swindows){ for(;swindows;swindows=swindows->next){ AyyiFilePanel* filemanager = (AyyiFilePanel*)swindows->data;
#define end_filemanager_foreach } g_list_free(swindows); }

struct _AyyiFilePanelClass {
	AyyiPanelClass parent_class;
};

struct _AyyiFilePanel {
	AyyiPanel      panel;
};

void file_panel_init ();

G_END_DECLS

#endif
