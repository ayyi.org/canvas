/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#if (defined __cl_canvas_c__ || defined __clutter_part_c__ )

#include "canvas_op.h"

struct _cl_canvas
{
	ClutterActor*    stage;
	ClutterActor*    viewport;
	ClutterTimeline* timeline;
	ClutterTimeline* stage_zoom_timeline;
	ClutterTimeline* stage_scroll_timeline;

	GList*           parts;                 // ClutterPart*
	GList*           selected_parts;        // ClutterPart*. Stored so that they can be easily unselected.

	GtkWidget*       table;
	GtkWidget*       hscrollbar;
	GtkWidget*       vscrollbar;
	GtkWidget*       event_box;

	Rectangle        visible;

	Ptd              animation_zoom;
	Ptd              animation_scroll;
};

struct _clutter_canvas_op
{
	CanvasOp         canvas;
	ClutterActor*    item;
};
#endif

#define CANVAS_IS_CLUTTER (arrange->canvas->type == arr_cl_get_type())


CanvasType  arr_cl_init                ();
CanvasType  arr_cl_get_type            ();
GtkWidget*  arr_cl_canvas_new          (Arrange*);
void        arr_cl_canvas_show         (Arrange*);
int         arr_cl_pos2px              (Arrange*, GPos*);
int         arr_cl_cpos2px             (Arrange*, AyyiSongPos*);
double      arr_cl_samples2px          (Arrange*, uint32_t);

void        arr_k_clutter              (GtkAccelGroup*, gpointer);

