/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, version 3.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */


#ifndef __GTK_KEYS_H__
#define __GTK_KEYS_H__


#include <gdk/gdk.h>
#include <gtk/gtkdrawingarea.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_KEYS                 (gtk_keys_get_type())
#define GTK_KEYS(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_KEYS, GtkKeys))
#define GTK_KEYS_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_KEYS, GtkKeysClass))
#define GTK_IS_KEYS(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_KEYS))
#define GTK_IS_KEYS_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_KEYS))
#define GTK_KEYS_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_KEYS, GtkKeysClass))


typedef struct _GtkKeys	       GtkKeys;
typedef struct _GtkKeysClass   GtkKeysClass;

struct _GtkKeys
{
  GtkDrawingArea graph;

  gint           cursor_type;

  gfloat         min_x;
  gfloat         max_x;
  gfloat         min_y;
  gfloat         max_y;

  GdkPixmap*     pixmap;

  GtkAdjustment* vadjust;
  GdkGC*         gc;
  int            num_keys;
  int            note_height; //average space used in an octave. Because of overlap, the actual height of any key is bigger than this.
  GdkColor       black, white, grey;
  int            highlighted_note;
};

struct _GtkKeysClass
{
  GtkDrawingAreaClass parent_class;

  void (*change_highlight)(GtkKeys*, int);
  void (*change_note_height)(GtkKeys*, int);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
};


GType       gtk_keys_get_type             (void) G_GNUC_CONST;
GtkWidget*  gtk_keys_new                  (void*, GtkAdjustment*);
void        gtk_keys_reset                (GtkKeys*);
void        gtk_keys_set_highlighted_note (GtkKeys*, int note);
void        gtk_keys_set_note_height      (GtkKeys*, int height);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_KEYS_H__ */
