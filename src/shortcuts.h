/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

void             shortcuts__init();
GimpActionGroup* shortcuts_add_group(const char* name);
GimpActionGroup* shortcuts_get_group(const char* name);
void             shortcuts_add_action(GtkAction*, GimpActionGroup*);

