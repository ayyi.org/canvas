/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __windows_h__
#define __windows_h__

#include <gdl/gdl.h>
#include <panels/panel.h>
#include "model/observable.h"

extern GList* windows; // list of AyyiWindow*

#define STRLEN_WIN_ID 16


struct _ConfigWindow // Used for both windows and Panels
{
  PanelType      type;
  char*          config_group;
  int            width;
  int            height;
  int            x;
  int            y;
  bool           maximised;
  int            depth;
  int            docktree[16];
  GtkOrientation orientation;
  GList*         params;       // ConfigParamValue*
  GList*         child_panels; // recursive list of ConfigWindow*
};


void             windows_init                   ();
void             windows__add_type              (PanelType);

AyyiWindow*      windows__open_new_window       ();

AyyiPanel*       windows__get_first             (PanelType);
GList*           windows__get_by_type           (PanelType);
int              windows__get_n_instances       (PanelType);
AyyiPanel*       windows__get_panel_from_widget (GtkWidget*);
AyyiPanel*       windows__get_active            ();
AyyiPanel*       windows__find_panel            (AyyiWindow*, PanelType);
PanelType        windows__panel_type_from_str   (const char*);
bool             windows__verify_pointer        (AyyiPanel*, PanelType);
AyyiPanel*       windows__get_from_instance_string(const char* str);
AyyiPanel*       windows__get_from_instance     (GType, int instance);
void             windows__ensure                (PanelType);

GHashTable*      windows__new_count_table       ();
int              windows__count_table_add       (GHashTable*, PanelType);

AyyiPanelClass*  panel_class_lookup_by_accel    (int);

void             windows__print                 ();
void             windows__print_dock            ();
void             windows__print_dock_item       (GdlDockItem*, const char* text);
void             windows__print_dock_object     (GdlDockObject*, const char* text);


void             windows__print_items           ();

#define window_foreach \
	GList* shells = windows; if(shells){ for(;shells;shells=shells->next){ AyyiWindow* window = shells->data;
#define end_window_foreach }}

#define panel_foreach \
	GList* _l = windows; \
	for(;_l;_l=_l->next){ \
		AyyiWindow* window = _l->data; \
		GList* s = window->dock->panels; \
		for(;s;s=s->next){ \
			AyyiPanel* panel = s->data;

#define end_panel_foreach }}

#endif
