/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __shortcuts_c__
#include <gdk/gdkkeysyms.h>
#include "global.h"
#include "toolbar.h"
#include "gimputils/gimpactiongroup.h"
#include "widgets/gimpcellrendereraccel.h"
#include "../shortcuts.h"
#include "shortcuts.h"

extern GList* action_groups;

extern gchar*   gimp_strip_uline        (const gchar* str);

static GtkTreeStore* accel_store;

static GObject* shortcuts_construct     (GType, guint n_construct_properties, GObjectConstructParam*);
static void     shortcuts_on_ready      (AyyiPanel*);

static void shortcuts_model_update      ();
static void shortcuts_win__on_realise   (GtkWidget*, gpointer);
static void shortcuts_win__on_allocate  (GtkWidget*, GtkAllocation*, gpointer);
static void shortcuts_win__accel_edited (GimpCellRendererAccel*, const char* path_string, gboolean delete, guint accel_key, GdkModifierType, GtkTreeView*);

G_DEFINE_TYPE (AyyiShortcutsWin, ayyi_shortcuts_win, AYYI_TYPE_PANEL)

// see gimp/app/widgets/gimpactionview.c

enum
{
  COLUMN_ACTION,
  COLUMN_STOCK_ID,
  COLUMN_LABEL,
  COLUMN_NAME,
  COLUMN_ACCEL_KEY,
  COLUMN_ACCEL_MASK,
  COLUMN_ACCEL_KEY2,
  COLUMN_ACCEL_MASK2,
  COLUMN_MENU_ITEM,
  SHORTCUTS_N_COLUMNS
};


static void
ayyi_shortcuts_win_class_init (AyyiShortcutsWinClass *klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	panel->name           = "Shortcuts";
	panel->has_link       = true;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F12;
	panel->default_size.x = 280;
	panel->default_size.y = 350;
	//panel->new            = shortcuts_win__new;
	panel->on_ready       = shortcuts_on_ready;

	g_object_class->constructor = shortcuts_construct;

	void shortcuts_changed(gpointer user_data){ shortcuts_model_update(); }
	g_signal_connect(app, "shortcuts-changed", (GCallback)shortcuts_changed, NULL);

	accel_store = gtk_tree_store_new (SHORTCUTS_N_COLUMNS, GTK_TYPE_ACTION, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_UINT, GDK_TYPE_MODIFIER_TYPE, G_TYPE_UINT, GDK_TYPE_MODIFIER_TYPE, GTK_TYPE_MENU_ITEM);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(accel_store), COLUMN_NAME, GTK_SORT_ASCENDING);
}


static void
ayyi_shortcuts_win_init (AyyiShortcutsWin* object)
{
}


static GObject*
shortcuts_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_shortcuts_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_SHORTCUTS_WIN);

	return g_object;
}


static void
shortcuts_win__on_realise (GtkWidget* window, gpointer user_data)
{
}


static void
shortcuts_win__on_allocate (GtkWidget* window, GtkAllocation* allocation, gpointer user_data)
{
	// remove size forcing
	static bool first_time = TRUE;
	if(first_time &&  GTK_WIDGET_REALIZED(window)){
		gtk_widget_set_size_request(window, 20, 20);
		first_time = FALSE;
	}
}


static void
shortcuts_on_ready (AyyiPanel* panel)
{
	ShortcutsWin* self = (ShortcutsWin*)panel;

	if(self->treeview) return;

	GtkWidget* scr_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scr_win), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	panel_pack(scr_win, EXPAND_TRUE);
	gtk_widget_show (scr_win);

	// set up a two column view of the model in browse selection mode
	self->treeview = GTK_TREE_VIEW(gtk_tree_view_new_with_model (GTK_TREE_MODEL(accel_store)));
#ifdef HAVE_GTK_2_10
	SHOW_TREEVIEW_LINES(self->treeview);
#endif
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (self->treeview)), GTK_SELECTION_BROWSE);

	GtkTreeViewColumn* column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Action");

	GtkCellRenderer* cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, cell, FALSE);
	gtk_tree_view_column_set_attributes (column, cell, "stock-id", COLUMN_STOCK_ID, NULL);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_attributes (column, cell, "text", COLUMN_LABEL, NULL);

	gtk_tree_view_append_column (self->treeview, column);

	//----------------------

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Shortcut");

	cell = gimp_cell_renderer_accel_new ();
	cell->mode = GTK_CELL_RENDERER_MODE_EDITABLE;
	GTK_CELL_RENDERER_TEXT (cell)->editable = TRUE;
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_attributes (column, cell, "accel-key", COLUMN_ACCEL_KEY, "accel-mask", COLUMN_ACCEL_MASK, NULL);
	gtk_tree_view_column_set_resizable(column, TRUE);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width(column, 100);
	gtk_tree_view_append_column (self->treeview, column);

	g_signal_connect (cell, "accel-edited", G_CALLBACK (shortcuts_win__accel_edited), self->treeview);

	//----------------------

	// 2nd shortcut

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Alt Shortcut");

	cell = gimp_cell_renderer_accel_new ();
	cell->mode = GTK_CELL_RENDERER_MODE_EDITABLE;
	GTK_CELL_RENDERER_TEXT (cell)->editable = TRUE;
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_attributes (column, cell, "accel-key", COLUMN_ACCEL_KEY2, "accel-mask", COLUMN_ACCEL_MASK2, NULL);
	gtk_tree_view_column_set_resizable(column, TRUE);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width(column, 100);
	gtk_tree_view_append_column (self->treeview, column);

	//----------------------

	GtkCellRenderer* renderer = gtk_cell_renderer_text_new ();
	g_object_set (G_OBJECT (renderer), "xalign", 0.0, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (self->treeview), -1, "Accel", renderer, "text", COLUMN_ACCEL_KEY, NULL);
	gtk_tree_view_column_set_sort_column_id (gtk_tree_view_get_column (GTK_TREE_VIEW (self->treeview), 1), COLUMN_LABEL);

	//----------------------

	gtk_container_add (GTK_CONTAINER (scr_win), GTK_WIDGET(self->treeview));
	gtk_widget_show (GTK_WIDGET(self->treeview));

	g_signal_connect((gpointer)panel->window, "realize", G_CALLBACK(shortcuts_win__on_realise), NULL);
	g_signal_connect((gpointer)panel->window, "size-allocate", G_CALLBACK(shortcuts_win__on_allocate), NULL);

	shortcuts_model_update();
}


#if 0
static void
accel_map_add(gpointer user, const gchar* accel_path, guint accel_key, guint accel_mods, gboolean changed)
{
  GtkTreeIter iter;
  gchar* accel_name;

  gtk_list_store_append (accel_store, &iter);
  if (accel_key) accel_name = gtk_accelerator_name (accel_key, accel_mods);
  else           accel_name = "";

  gtk_list_store_set (accel_store, &iter, 0, accel_path, 1, accel_name, -1);
  if (accel_key) g_free(accel_name);
}
#endif


static void
shortcuts_model_update ()
{
	gtk_tree_store_clear (accel_store);
	//gtk_accel_map_foreach (NULL, accel_map_add);
	GtkTreeStore* store = accel_store;

	for(GList* l=action_groups;l;l=l->next){
		GimpActionGroup* action_group = l->data;
		dbg(2, "group=%s", gtk_action_group_get_name(GTK_ACTION_GROUP(action_group)));

		GtkTreeIter group_iter;
		gtk_tree_store_append(accel_store, &group_iter, NULL);
		gtk_tree_store_set (store, &group_iter,
			COLUMN_STOCK_ID, action_group->stock_id,
			COLUMN_LABEL,    action_group->label,
			-1);

		GList* actions = gtk_action_group_list_actions (GTK_ACTION_GROUP(action_group));
		//actions = g_list_sort (actions, (GCompareFunc) gimp_action_name_compare);
		for(GList* a = actions; a; a=a->next){
			GtkAction* action = a->data;
			const gchar* name = gtk_action_get_name (action);

			const gchar*    accel_path = gtk_action_get_accel_path(action);
			guint           accel_key  = 0;
			GdkModifierType accel_mask = 0;
			GtkAccelKey key;
			if(accel_path){
				if(gtk_accel_map_lookup_entry(accel_path, &key)){
					accel_key  = key.accel_key;
					accel_mask = key.accel_mods;
					dbg(3, "accel_path=%s key=%i", accel_path, accel_key);
				}
				else dbg(0, "entry not found (%s).", accel_path);
			}
			else dbg(0, "failed to get accel_path. action=%s", name);

			gchar* stock_id;
			gchar* label;
			g_object_get (action, "stock-id", &stock_id, "label", &label, NULL);
			gchar* stripped = gimp_strip_uline (label);

			GtkTreeIter action_iter;
			gtk_tree_store_append (store, &action_iter, &group_iter);

			gtk_tree_store_set (store, &action_iter,
				COLUMN_ACTION,     action,
				COLUMN_STOCK_ID,   stock_id,
				COLUMN_LABEL,      stripped,
				COLUMN_NAME,       name,
				COLUMN_ACCEL_KEY,  accel_key,
				COLUMN_ACCEL_MASK, accel_mask,
				//COLUMN_MENU_ITEM,  menu_item,
				-1);
			g_free (stock_id);
			g_free (label);
			g_free (stripped);
		}
		g_list_free(actions);
	}
}


static void
shortcuts_win__conflict_confirm (GtkTreeView* view, GtkAction *action, guint accel_key, GdkModifierType  accel_mask, const gchar *accel_path)
{
  dbg(0, "FIXME");
#if 0
  GimpActionGroup *group;
  GimpMessageBox  *box;
  gchar           *label;
  gchar           *stripped;
  gchar           *accel_string;
  ConfirmData     *confirm_data;
  GtkWidget       *dialog;

  g_object_get (action,
                "action-group", &group,
                "label",        &label,
                NULL);

  stripped = gimp_strip_uline (label);
  g_free (label);

  accel_string = gimp_get_accel_string (accel_key, accel_mask);

  confirm_data = g_new0 (ConfirmData, 1);

  confirm_data->accel_path = g_strdup (accel_path);
  confirm_data->accel_key  = accel_key;
  confirm_data->accel_mask = accel_mask;

  dialog =
    gimp_message_dialog_new (_("Conflicting Shortcuts"),
                             GIMP_STOCK_WARNING,
                             gtk_widget_get_toplevel (GTK_WIDGET (view)), 0,
                             gimp_standard_help_func, NULL,

                             GTK_STOCK_CANCEL,         GTK_RESPONSE_CANCEL,
                             _("_Reassign shortcut"),  GTK_RESPONSE_OK,

                             NULL);

  g_signal_connect (dialog, "response",
                    G_CALLBACK (gimp_action_view_conflict_response),
                    confirm_data);

  box = GIMP_MESSAGE_DIALOG (dialog)->box;

  gimp_message_box_set_primary_text (box,
                                     _("Shortcut \"%s\" is already taken "
                                       "by \"%s\" from the \"%s\" group."),
                                     accel_string, stripped, group->label);
  gimp_message_box_set_text (box,
                             _("Reassigning the shortcut will cause it "
                               "to be removed from \"%s\"."),
                             stripped);

  g_free (stripped);
  g_free (accel_string);

  g_object_unref (group);

  gtk_widget_show (dialog);
#endif
}

static void
shortcuts_win__accel_edited (GimpCellRendererAccel *accel,
                             const char            *path_string,
                             gboolean               delete,
                             guint                  accel_key,
                             GdkModifierType        accel_mask,
                             GtkTreeView           *view)
{
	GtkTreeModel* model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
	if (! model) return;

	GtkTreePath* path = gtk_tree_path_new_from_string (path_string);

	GtkTreeIter iter;
	if (gtk_tree_model_get_iter (model, &iter, path)) {
		GtkAction* action;
		GtkActionGroup* group;

		gtk_tree_model_get (model, &iter, COLUMN_ACTION, &action, -1);

		if (!action) goto done;

		g_object_get (action, "action-group", &group, NULL);

		if (!group) {
			g_object_unref (action);
			goto done;
		}

		gchar* accel_path = g_strdup_printf ("<Actions>/%s/%s", gtk_action_group_get_name (group), gtk_action_get_name (action));
		dbg(1, "accel_path=%s", accel_path);

		if (delete) {
			if (!gtk_accel_map_change_entry (accel_path, 0, 0, FALSE)) {
				g_message ("Removing shortcut failed.");
			}
		}
		else if (! accel_key) {
			g_message ("Invalid shortcut.");
		}
		else {
			dbg(0, "changing shortcut...");
			if (!gtk_accel_map_change_entry (accel_path, accel_key, accel_mask, FALSE)) {
				GtkAction   *conflict_action = NULL;
				GtkTreeIter  iter;
				gboolean     iter_valid;

				// look through the model to see if the key is already used
				for (iter_valid = gtk_tree_model_get_iter_first (model, &iter); iter_valid; iter_valid = gtk_tree_model_iter_next (model, &iter)) {
					GtkTreeIter child_iter;
					gboolean    child_valid;

					for (child_valid = gtk_tree_model_iter_children (model, &child_iter, &iter); child_valid; child_valid = gtk_tree_model_iter_next (model, &child_iter)) {
						guint           child_accel_key;
						GdkModifierType child_accel_mask;

						gtk_tree_model_get (model, &child_iter, COLUMN_ACCEL_KEY, &child_accel_key, COLUMN_ACCEL_MASK, &child_accel_mask, -1);

						if (accel_key  == child_accel_key && accel_mask == child_accel_mask) {
							gtk_tree_model_get (model, &child_iter, COLUMN_ACTION, &conflict_action, -1);
							break;
						}
					}

					if (conflict_action) break;
				}

				if (conflict_action && conflict_action != action) {
					shortcuts_win__conflict_confirm (view, conflict_action, accel_key, accel_mask, accel_path);
					g_object_unref (conflict_action);
				}
				else if (conflict_action != action) {
					g_message ("Changing shortcut failed.");
				}
			}
		}

		g_free (accel_path);
		g_object_unref (group);
		g_object_unref (action);
	}

  done:

	gtk_tree_path_free (path);
}
