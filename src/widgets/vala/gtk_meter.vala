/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
 
using GLib;
using Gtk;
 
public class GtkMeter : Gtk.Widget {

	Gtk.Adjustment adj;

	public GtkMeter(Gtk.Adjustment _adj, int direction)
	{
		adj = new Gtk.Adjustment(0.0, 0.0, 100.0, 1.0, 10.0, 10.0);
	}

	public override void realize ()
	{
	}
}

