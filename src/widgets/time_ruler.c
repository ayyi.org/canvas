/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2008-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 * time_ruler, modified from GtkHRuler
 * by Conrad Parker 2000 for Sweep.
 */

/*
 * GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */


#include <config.h>
#ifndef DEBUG_DISABLE_RULERBAR
#include <math.h>
#include "debug/debug.h"
#include "ayyi/ayyi_utils.h"
#include "model/model_types.h"
#include "model/time.h"
#include "model/utils.h"
#include "model/transport.h"
#include "gui_types.h"
#include "windows.h"
#include "window.statusbar.h"
#include "support.h"
#include "panels/arrange.h"
#include "src/song.h"

void        am_transport_set_locator_pos   (int loc_num, AyyiSongPos*);
extern void am_transport_jump              (AyyiSongPos*);
extern void am_song__set_end               ();
void        set_cursor                     (GdkWindow*, GdkCursorType);
extern void get_font_string                (char*, int);

#include "time_ruler.h"

#define FONT_SIZE            -2
#define TEXT_PADDING_Y        1
#define MINIMUM_INCR          5
#define MAXIMUM_SUBDIVIDE     5
#define MAXIMUM_SCALES        21

#define ROUND(x) ((int) ((x) + 0.5))
#define LOWER_PX arr_samples2px(time_ruler->arrange, ruler->lower)
#define FLAG_DIRECTION(i) ((i == 2) ? GTK_DIR_LEFT : GTK_DIR_RIGHT)
#define FLAG_SIZE 10
static inline gboolean
is_over_loc(TimeRuler* time_ruler, int loc_num, int x)
{
	GtkRuler *ruler = GTK_RULER (time_ruler);
	int lower_px = LOWER_PX;
	return (x > 3 + time_ruler->locators[loc_num] - lower_px - FLAG_SIZE/2 - (FLAG_DIRECTION(loc_num)==GTK_DIR_LEFT?5:0)) && (x < 3 + time_ruler->locators[loc_num] - lower_px + FLAG_SIZE/2 - (FLAG_DIRECTION(loc_num)==GTK_DIR_LEFT?5:0));
}
//still needed for end-flag.
#define IS_OVER_LOC(LOC) \
    (x > 3 + LOC - lower_px - 5 - (FLAG_DIRECTION(LOC)==GTK_DIR_LEFT?5:0)) && (x < 3 + LOC - lower_px + FLAG_SIZE/2 - (FLAG_DIRECTION(LOC)==GTK_DIR_LEFT?5:0))
//TODO merge these 3 versions of IS_OVER_LOC - how to do end-flag?
#define IS_OVER_LOC2(LOC) \
    (x > 3 + time_ruler->locators[LOC] - lower_px - FLAG_SIZE/2 - (FLAG_DIRECTION(LOC)==GTK_DIR_LEFT?5:0)) && (x < 3 + time_ruler->locators[LOC] - lower_px + FLAG_SIZE/2 - (FLAG_DIRECTION(LOC)==GTK_DIR_LEFT?5:0))


struct _scroll_op
{
	guint        timer;
	int          speed;
	int          time;                // counts the number of consequtive window scrolls in order to set the speed.
};


int dragging_loc_num = -1;

static void     time_ruler_class_init       (TimeRulerClass*);
static void     time_ruler_init             (TimeRuler*);
static void     time_ruler_on_realise       (GtkWidget*);
static void     time_ruler_on_allocate      (GtkWidget*, GtkAllocation*);
static gint     time_ruler_button_press     (GtkWidget*, GdkEventButton*);
static gint     time_ruler_button_release   (GtkWidget*, GdkEventButton*);
static gint     time_ruler_motion_notify    (GtkWidget*, GdkEventMotion*);
static gint     time_ruler_leave_notify     (GtkWidget*, GdkEventCrossing*);
static void     time_ruler_draw_ticks       (GtkRuler*);
static void     time_ruler_draw_pos         (GtkRuler*);
static void     time_ruler_draw_flags       (TimeRuler*);
static void     time_ruler_draw_flag        (cairo_t*, int, GtkDirectionType);

static gboolean time_ruler_mouse_timeout    (gpointer);
static void     time_ruler_start_mouse_timer(TimeRuler*);
static void     time_ruler_stop_timer       (TimeRuler*);

static void     snprint_bbst                (char*, int len, GPos*);
static int      snprint_time                (gchar*, gint n, sw_time_t);
static void     time_ruler_default_text_out (AyyiPanel*, const char* format, ...);

G_DEFINE_TYPE (TimeRuler, time_ruler, GTK_TYPE_RULER)


static void
time_ruler_class_init (TimeRulerClass *klass)
{
  GtkWidgetClass *widget_class = (GtkWidgetClass*) klass;
  GtkRulerClass *ruler_class = (GtkRulerClass*) klass;

  widget_class->realize = time_ruler_on_realise;
  widget_class->size_allocate = time_ruler_on_allocate;
  widget_class->button_press_event = time_ruler_button_press;
  widget_class->button_release_event = time_ruler_button_release;
  widget_class->motion_notify_event = time_ruler_motion_notify;
  widget_class->leave_notify_event = time_ruler_leave_notify;

  //this draws the semi-permanent features onto the backing pixmap
  ruler_class->draw_ticks = time_ruler_draw_ticks;

  //this overlays transient features directly onto the window
  ruler_class->draw_pos = time_ruler_draw_pos;
}

static gfloat ruler_scale[MAXIMUM_SCALES] =
{ 0.001, 0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1,
  2.5, 5, 10, 15, 30, 60, 300, 600, 1800, 3600, 18000, 36000 };

static gint subdivide[MAXIMUM_SUBDIVIDE] = { 1, 2, 5, 10, 100 };

static void
time_ruler_init (TimeRuler *time_ruler)
{
  GtkWidget *widget = GTK_WIDGET (time_ruler);

  time_ruler->height = 28;
  time_ruler->samplerate = 44100;
  time_ruler->dragging = FALSE;

  widget->requisition.width = widget->style->xthickness * 2 + 1;

  gtk_widget_add_events(widget, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_LEAVE_NOTIFY_MASK);
}

GtkWidget*
time_ruler_new (Arrange *arrange)
{
  GtkWidget *widget = GTK_WIDGET (g_object_new (time_ruler_get_type (), NULL));
  TimeRuler *time_ruler = TIME_RULER(widget);
  time_ruler->arrange = arrange;
  time_ruler->text_out = time_ruler_default_text_out;

  void on_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer _ruler)
  {
		TimeRuler *ruler = _ruler;

		double ruler_width = arr_px2samples(ruler->arrange, ((GtkWidget*)ruler)->allocation.width);
		gdouble lower = ruler->arrange->canvas->get_viewport_left(ruler->arrange);
		gdouble upper = lower + ruler_width;
		gdouble max_size = 1600;
		dbg(2, "upper=%.2f", upper);
		gtk_ruler_set_range((GtkRuler*)ruler, lower, upper, ((GtkRuler*)ruler)->position, max_size);
  }
  g_signal_connect(G_OBJECT(arrange), "size-allocate", G_CALLBACK(on_allocate), widget);

  return widget;
}

static gint
time_ruler_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
  #define SCROLL_MASK 5
  #define SCROLL_DELTA 3
  enum {
    F_NONE,
    F_LOC,
  };
  gint x;
  //gint feature = F_NONE;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TIME_RULER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  TimeRuler *time_ruler = TIME_RULER(widget);
  GtkRuler *ruler = GTK_RULER (widget);

  if (event->is_hint)
    gdk_window_get_pointer (widget->window, &x, NULL, NULL);
  else
    x = event->x;

  ruler->position = ruler->lower + ((ruler->upper - ruler->lower) * x) / widget->allocation.width;

  if (ruler->backing_store) gtk_ruler_draw_pos (ruler);

  if (time_ruler->dragging && dragging_loc_num > -1) {
    //feature = F_LOC;
    time_ruler->locators[dragging_loc_num] = x + LOWER_PX;
    gtk_widget_queue_draw(GTK_WIDGET(ruler));
    arr_bbst2statusbar(time_ruler->arrange, (double)x, NULL);

    //scrolling needed?
    if ((x < SCROLL_MASK) || (x > widget->allocation.width - SCROLL_MASK)) {
      time_ruler_start_mouse_timer(time_ruler);
    }
  }
  else {
    //set cursor
    int cursor = CURSOR_NORMAL;
    int lower_px = LOWER_PX;
    int i; for (i=0;i<AYYI_MAX_LOC;i++) {
      //if (IS_OVER_LOC(time_ruler->locators[i])) {
      if (IS_OVER_LOC2(i)) {
        //feature = F_LOC;
        cursor = CURSOR_HAND_CLOSE;
		time_ruler->text_out(&time_ruler->arrange->panel, "%s", am_locator_name(i));
        break;
      }
      else
		time_ruler->text_out(&time_ruler->arrange->panel, "");
    }
    set_cursor(widget->window, cursor);
  }

  arr_bbst2statusbar(time_ruler->arrange, (double)x, NULL);

  return FALSE;
}

static gint
time_ruler_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  time_ruler_set_pos((TimeRuler*)widget, -1);
  return NOT_HANDLED;
}

static void
time_ruler_on_realise (GtkWidget *widget)
{
	TimeRuler *time_ruler = TIME_RULER(widget);

	((GtkWidgetClass*)time_ruler_parent_class)->realize(widget);

	int get_layout_height(GtkWidget* widget)
	{
		char font_string[64];
		get_font_string(font_string, FONT_SIZE);
		PangoFontDescription* d = pango_font_description_from_string(font_string);

		gint font_size = pango_font_description_get_size(d) / PANGO_SCALE + 2;
		pango_font_description_set_size(d, (font_size) * PANGO_SCALE);

		PangoLayout *layout = gtk_widget_create_pango_layout (widget, "012456789");
		PangoRectangle ink_rect;
		pango_layout_get_extents (layout, &ink_rect, NULL);
		// The logical_rect doesnt seem to reflect the fact that we have reduced the size of the font.
		// in this particular case, the ink_rect seems to accurately reflect what is rendered.
		return PANGO_PIXELS (ink_rect.height);
	}
	int digit_height = get_layout_height(widget);

	if(digit_height > (time_ruler->height - TEXT_PADDING_Y - 6) / 2){
		time_ruler->height = 2 * (digit_height + TEXT_PADDING_Y + 8);
		dbg(1, "font is too big for box - increasing box height to: %i", time_ruler->height);
		gtk_widget_set_size_request(widget, -1, time_ruler->height);
	}

	widget->requisition.height = time_ruler->height;
}


static void
time_ruler_on_allocate (GtkWidget* widget, GtkAllocation* allocation)
{
	TimeRuler *time_ruler = TIME_RULER(widget);

	((GtkWidgetClass*)time_ruler_parent_class)->size_allocate(widget, allocation);

	gtk_widget_set_size_request(widget, allocation->width, time_ruler->height);
}


static gint
time_ruler_button_press (GtkWidget *widget, GdkEventButton *event)
{
  TimeRuler *time_ruler = TIME_RULER(widget);
  GtkRuler *ruler = GTK_RULER (time_ruler);

  GdkModifierType state;
  int x, y;

  gdk_window_get_pointer (event->window, &x, &y, &state);
  gtk_widget_grab_focus (widget);

  int lower_px = LOWER_PX;

  for (int i=0;i<AYYI_MAX_LOC;i++) {
    //if(IS_OVER_LOC(time_ruler->locators[i])){
    if (is_over_loc(time_ruler, i, x)) {
      dbg(0, "flag! (%i)", i);
      time_ruler->dragging = TRUE;
      dragging_loc_num = i;
      gtk_grab_add (widget);
      break;
    }
  }

  if(!(dragging_loc_num > -1)){
    //check end-flag
    if(IS_OVER_LOC(time_ruler->end_loc)){
      dbg(0, "end flag!");
      time_ruler->dragging = TRUE;
      dragging_loc_num = AYYI_MAX_LOC;
      gtk_grab_add (widget);
    }
  }

  return TRUE;
}

static gint
time_ruler_button_release (GtkWidget *widget, GdkEventButton *event)
{
	PF;
	TimeRuler *time_ruler = TIME_RULER(widget);
	GtkRuler *ruler = GTK_RULER(time_ruler);

	if(time_ruler->dragging){
		gint x = event->x;
		AyyiSongPos pos;
		arr_snap_px2spos(time_ruler->arrange, &pos, x + LOWER_PX);
		if(dragging_loc_num == AYYI_MAX_LOC){
			dbg(0, "end flag dropped!");
			am_song__set_end((am_object_val(&song->loc[AM_LOC_END]).sp = pos).beat);
		}else{
			am_transport_set_locator_pos(dragging_loc_num, &pos);
		}
		//window__statusbar_print(&time_ruler->arrange->sm_window, 1, "");
		log_print(0, "locator moved");
	}else{
		AyyiSongPos pos;
		arr_snap_px2spos(time_ruler->arrange, &pos, event->x + LOWER_PX);
		am_transport_jump(&pos);

		shell__statusbar_print((AyyiWindow*)gtk_widget_get_toplevel((GtkWidget*)time_ruler->arrange), 1, "");
	}

	time_ruler->dragging = FALSE;
	dragging_loc_num = -1;
	time_ruler_stop_timer(time_ruler);

	gtk_grab_remove (widget);

	return TRUE;
}

static void
time_ruler_draw_ticks (GtkRuler *ruler)
{
	gint i;
	gint ideal_length;
	gdouble lower, upper;             // Upper and lower limits, in ruler units
	gdouble increment, abs_increment; // Number of pixels per unit
	gint scale;                       // Number of units per major unit
	gdouble subd_incr;
	gdouble start, end, cur;
#define UNIT_STR_LEN 32
	gchar unit_str[UNIT_STR_LEN];
	gint digit_height;
	//gint digit_offset;
	gint pos;

	g_return_if_fail (ruler);
	g_return_if_fail (GTK_IS_TIME_RULER (ruler));

	if (!GTK_WIDGET_DRAWABLE (ruler)) return;

	GtkWidget* widget = GTK_WIDGET (ruler);
	TimeRuler* time_ruler = TIME_RULER(ruler);

	gint ythickness = 0;//widget->style->ythickness;

	gint width = widget->allocation.width;
	gint height = (widget->allocation.height - ythickness * 2) / 2;

	PangoLayout *layout = gtk_widget_create_pango_layout (widget, "012456789");

	PangoRectangle logical_rect, ink_rect;
	{
		char font_string[64]; get_font_string(font_string, FONT_SIZE);
		PangoFontDescription *font_desc = pango_font_description_from_string(font_string);
		pango_layout_set_font_description(layout, font_desc);

		pango_layout_get_extents (layout, &ink_rect, &logical_rect);
  
		digit_height = PANGO_PIXELS (MAX(ink_rect.height, logical_rect.height)) + 2;
		//digit_offset = ink_rect.y;
	}
	int layout_offset = PANGO_PIXELS(ink_rect.y) - 1; //empirically determined in absence of documentation

	//background:   
	gtk_paint_box (widget->style, ruler->backing_store,
		 GTK_STATE_NORMAL, GTK_SHADOW_NONE, 
		 NULL, widget, "time_ruler",
		 0, 0, 
		 widget->allocation.width, widget->allocation.height);

	cairo_t *cr = gdk_cairo_create (ruler->backing_store);
	gdk_cairo_set_source_color (cr, &widget->style->fg[widget->state]);

	upper = ruler->upper / time_ruler->samplerate;
	lower = ruler->lower / time_ruler->samplerate;
	/*
	if(1 || ayyi_panel_get_instance_num((AyyiPanel*)time_ruler->arrange) == 1){
		dbg(0, "lowerpx=%.2f ruler->lower=%.2f (%p) realised=%i", lower, ruler->lower, &ruler->lower, GTK_WIDGET_REALIZED(widget));
	}
	*/

	if ((upper - lower) == 0) goto out;

	increment = (gdouble) width / (upper - lower);
	abs_increment = (gdouble) fabs((double)increment);

	/* determine the scale
	 *  We calculate the text size as for the vruler instead of using
	 *  text_width = gdk_string_width(font, unit_str), so that the result
	 *  for the scale looks consistent with an accompanying vruler
	 */
	scale = ceil (ruler->max_size / TIME_RULER(ruler)->samplerate);
	snprint_time (unit_str, UNIT_STR_LEN, (sw_time_t)scale);
	/* snprint_time_smpte (unit_str, UNIT_STR_LEN, (sw_time_t)scale, 10.0);*/

	gint text_width = strlen (unit_str) * digit_height + 1;

	for (scale = 0; scale < MAXIMUM_SCALES; scale++)
		if (ruler_scale[scale] * abs_increment > 2 * text_width)
			break;

  if (scale == MAXIMUM_SCALES) scale = MAXIMUM_SCALES - 1;

  /* drawing starts here */
  gint length = 0;
  for (i = MAXIMUM_SUBDIVIDE - 1; i >= 0; i--) {
    subd_incr = (gdouble) ruler_scale[scale] / (gdouble) subdivide[i];
    if (subd_incr * fabs(increment) <= MINIMUM_INCR) continue;

    /* Calculate the length of the tickmarks. Make sure that
     * this length increases for each set of ticks
     */
      ideal_length = height / (i + 1) - 1;
      if (ideal_length > ++length) length = ideal_length;

      if (lower < upper) {
        start = floor (lower / subd_incr) * subd_incr;
        end   = ceil  (upper / subd_incr) * subd_incr;
      }
      else {
        start = floor (upper / subd_incr) * subd_incr;
        end   = ceil  (lower / subd_incr) * subd_incr;
      }
  
      for (cur = start; cur <= end; cur += subd_incr) {
        pos = ROUND ((cur - lower) * increment);

        cairo_rectangle (cr, 
                         pos, height + ythickness - length, 
                         1,   length);

        /* draw label */ 
        if (i == 0) {
#if 1
          snprint_time (unit_str, UNIT_STR_LEN, (sw_time_t)cur);
#else
          snprint_time_smpte (unit_str, UNIT_STR_LEN, (sw_time_t)cur, 10.0);
#endif
          pango_layout_set_text (layout, unit_str, -1);

          gtk_paint_layout (widget->style,
                  ruler->backing_store,
                  GTK_WIDGET_STATE (widget),
                  FALSE,
                  NULL,
                  widget,
                  "vruler",
                  pos + 2,
                  TEXT_PADDING_Y - layout_offset,
                  layout);
      }
    }
  }

  //--------------------------------------------------

  //bars and beats:

  Arrange* arrange = time_ruler->arrange;

  QMap* q = &song->q_settings[Q_EXPONENTIAL];

  int width_samples = ruler->upper - ruler->lower;
  int start_px = arr_samples2px(arrange, ruler->lower);
  //lets say we ideally want one tick every 3 pixels...
  #define MIN_TICK_SPACING 3
  uint32_t samples_per_px = arr_samples_per_pix(time_ruler->arrange);
  GPos interval; samples2pos(MIN_TICK_SPACING * samples_per_px, &interval);
  char interval_bbst[64]; pos2bbst(&interval, interval_bbst);
  q_to_next(&interval, q);

  //if not zoomed in, reduce tick height
  int reduction = 0;
  gint zoom = (width << 16)/ width_samples;
  if(zoom > 1){
    if(zoom > 8){
      if(zoom > 36){
        reduction = 1;
      }
      else reduction = 2;
    }
    else reduction = 3;
  }

  enum {
    TICK_SMALL,
    TICK_MEDIUM,
    TICK_LARGE,
    TICK_FULL,
  };
  #define MAX_LENS 7
  int lens[MAX_LENS] = {2, 4, 8, 13, 13, 13, 13};
  int previous_x = -1000;
  GPos pos_; samples2pos(ruler->lower, &pos_);
  q_to_nearest(&pos_, &song->q_settings[Q_16]);
  //the above quantise is inadequate - quantise some more:
  if(zoom < 32) pos_.sub = 0;                    //quantise to beat.
  if(zoom <  8) pos_.beat = 4 * (pos_.beat / 4); //quantise to bar.

  GPos max_pos; samples2pos(ruler->upper, &max_pos);

  char interval_bbst2[64]; pos2bbst(&interval, interval_bbst2);
  char bbst1[64]; pos2bbst(&pos_, bbst1);
  dbg(2, "start=%u=%s interval=%s rdn=%i zoom=%i", start_px, bbst1, interval_bbst2, reduction, zoom);

  //int g = 0; //debugging
  while(pos_is_before(&pos_, &max_pos)){

    int len_i = 0;
    if(!pos_.tick){
      if(!pos_.sub){
        if(!(pos_.beat % 4)){
          if(!(pos_.beat % 16)){
            if(!(pos_.beat % 64)){
              len_i = 6;
            }
            else len_i = 5;
          }
          else len_i = 4;
        }
        else len_i = 3;
      }
      else len_i = 2;
    }
    else len_i = 1;
    len_i -= reduction;
    int len = CLAMP(lens[len_i], 0, MAX_LENS-1);

#if 0
    if(g < 16) printf("%i:%i ", (pos_.sub), len); fflush(stdout);
    if(g == 16) printf("\n");
#endif

    double x = arr_pos2px(arrange, &pos_) - start_px;

    if((len_i > 2) && (x > previous_x + 50)){
      snprint_bbst(unit_str, UNIT_STR_LEN, &pos_);
      pango_layout_set_text (layout, unit_str, -1);
      pango_layout_get_extents (layout, NULL, &logical_rect);
      gtk_paint_layout (widget->style,
                  ruler->backing_store,
                  GTK_WIDGET_STATE (widget),
                  FALSE,
                  NULL,
                  widget,
                  "vruler",
                  x + 2,
                  height + TEXT_PADDING_Y - layout_offset,
                  layout);
      previous_x = x;
    }

    cairo_rectangle (cr, 
                     x, height * 2, 
                     1, -len);
    pos_add(&pos_, &interval);
    //g++;
  }

  //--------------------------------------------------

  cairo_fill (cr);
out:
  cairo_destroy (cr);
  g_object_unref (layout);

  time_ruler_draw_flags((TimeRuler*)widget);
}

static void
time_ruler_draw_pos (GtkRuler *ruler)
{
  g_return_if_fail (ruler != NULL);
  g_return_if_fail (GTK_IS_TIME_RULER (ruler));
  TimeRuler* time_ruler = TIME_RULER(ruler);

  if (GTK_WIDGET_DRAWABLE (ruler)) {
    GtkWidget *widget = GTK_WIDGET (ruler);

    GdkColor red = {0, 0xffff, 0, 0};
    GdkGC *gc = gdk_gc_new(GDK_DRAWABLE(widget->window));
    gdk_gc_set_rgb_fg_color(gc, &red);

    gint ythickness = widget->style->ythickness;
    gint width = widget->allocation.width;
    gint height = widget->allocation.height - ythickness * 2;

    gint bs_width = 1;
    gint bs_height = height;

    if (bs_height > 0) {
      /*  If a backing store exists, restore the ruler  */
      if (ruler->backing_store && ruler->non_gr_exp_gc)
        gdk_draw_drawable (ruler->widget.window,
          ruler->non_gr_exp_gc,
          ruler->backing_store,
          ruler->xsrc, ruler->ysrc,
          ruler->xsrc, ruler->ysrc,
          bs_width, bs_height);

      gfloat increment = (gfloat) width / (ruler->upper - ruler->lower);

      gint x = ROUND ((ruler->position - ruler->lower) * increment);
      gint y = 0;//(height + bs_height) / 2 + ythickness;

      if (!time_ruler->dragging)
        gdk_draw_line (widget->window, gc,
                       x, y,
                       x, y + bs_height);
      ruler->xsrc = x;
      ruler->ysrc = y;
      g_object_unref(gc);
    }
  }
}


static void
time_ruler_draw_flags(TimeRuler* time_ruler)
{
  GtkRuler *ruler = GTK_RULER (time_ruler);
  GtkWidget *widget = GTK_WIDGET(time_ruler);

  int lower_px = arr_samples2px(time_ruler->arrange, ruler->lower);

  cairo_t *cr = gdk_cairo_create (ruler->backing_store);
  gdk_cairo_set_source_color (cr, &widget->style->fg[widget->state]);

  DColour r = {1.0, 0.0, 0.0};
  DColour g = {0.0, 1.0, 0.0};
  DColour b = {0.0, 0.0, 1.0};
  DColour *colours[] = {&r, &b, &g, &b, &b, &b, &b, &b, &b, &b, &b, &r, &b, &b, &b};

  int i; for(i=1;i<AYYI_MAX_LOC;i++){
    if(song->loc[i].vals[0].val.sp.beat > -1 && (i == 1 || i == 2)){
      DColour *c = colours[i];
      cairo_set_source_rgb(cr, c->r, c->g, c->b);
      time_ruler_draw_flag(cr, time_ruler->locators[i] - lower_px, FLAG_DIRECTION(i));
    }
  }
  //dbg(0, "end=%i x=%.2f\n", song->end / 4, time_ruler->end_loc - ruler->lower / time_ruler->samplerate);

  //end flag:
  DColour *c = &r;
  cairo_set_source_rgb(cr, c->r, c->g, c->b);
  time_ruler_draw_flag(cr, time_ruler->end_loc - lower_px, GTK_DIR_LEFT);

  cairo_destroy (cr);
}


void
time_ruler_set_format (TimeRuler* time_ruler, sw_format* f)
{
  time_ruler->samplerate = f->rate;
}

void
time_ruler_set_start(TimeRuler* time_ruler, double start)
{
  //move start point, preserving scale.
  //@start - viewport left edge measured in samples.

  GtkRuler* ruler = (GtkRuler*)time_ruler;
  ruler->upper += (start - ruler->lower);
  ruler->lower = start;
/*
  if(1 || ayyi_panel_get_instance_num((AyyiPanel*)time_ruler->arrange) == 1){
    dbg(0, "ruler->lower=%.2f (%p) realised=%i", ruler->lower, &ruler->lower, GTK_WIDGET_REALIZED(GTK_WIDGET(time_ruler)));
  }
*/
  gtk_widget_queue_draw(GTK_WIDGET(ruler));
}

void
time_ruler_set_pos(TimeRuler* time_ruler, int x)
{
  //set the location of the position indicator.
  dbg(2, "x=%i", x);

  GtkRuler* ruler = (GtkRuler*)time_ruler;
  ruler->position = arr_px2samples(time_ruler->arrange, x);
  gtk_widget_queue_draw(GTK_WIDGET(ruler));
}

void
time_ruler_set_zoom(TimeRuler* time_ruler, double samples_per_px, double start)
{
  //the widget quantizes its scaling, so this might not match up with the rest of the window.

  GtkWidget* widget = GTK_WIDGET(time_ruler);
  GtkRuler* ruler = (GtkRuler*)time_ruler;

  time_ruler_update_flags(time_ruler);

  if(!GTK_WIDGET_REALIZED(widget)) return;

  ruler->lower = start;
  ruler->upper = ruler->lower + samples_per_px * widget->allocation.width;

  gtk_widget_queue_draw(widget);
}

void
time_ruler_update_flags(TimeRuler* time_ruler)
{
  //is called following a zoom change, but _not_ for scroll changes.

  GtkWidget* widget = GTK_WIDGET(time_ruler);

  time_ruler->locators[0] = -100;
  int i; for(i=1;i<AYYI_MAX_LOC;i++){
    time_ruler->locators[i] = arr_spos2px(time_ruler->arrange, &song->loc[i].vals[0].val.sp);
    //if(i<2) dbg(0, "%i: %i %.2f", i, song->loc[i].beat, ruler->lower);
  }
  time_ruler->end_loc = arr_spos2px(time_ruler->arrange, &am_object_val(&song->loc[AM_LOC_END]).sp);

  gtk_widget_queue_draw(widget);
}

static void
time_ruler_draw_flag(cairo_t *cr, int x, GtkDirectionType direction)
{
  //printf("%s(): x=%i\n", __func__, x);
  if(x < -FLAG_SIZE) return;
  int x1 = (direction == GTK_DIR_RIGHT) ? x + FLAG_SIZE : x - FLAG_SIZE + 1;
  int x2 = (direction == GTK_DIR_RIGHT) ? -FLAG_SIZE : FLAG_SIZE + 1;
  cairo_move_to (cr, x1, 0);
  cairo_rel_line_to (cr, x2, 0);
  cairo_rel_line_to (cr, 0, FLAG_SIZE);
  cairo_close_path (cr);
  cairo_fill (cr);
}

static void
time_ruler_get_mouse_position(TimeRuler* time_ruler, Pti* left, Pti* right)
{
  //returns the distance from top left corner, and bottom right corner.

  GdkWindow* window = time_ruler->arrange->canvas->widget->window;
  g_return_if_fail(window);

  gint y = 0;
  gdk_window_get_pointer(window, &left->x, &y, NULL);

  int width = GTK_WIDGET(time_ruler)->allocation.width;
  right->x = left->x - width;
}

static gboolean
time_ruler_mouse_timeout(gpointer data)
{
  gboolean dont_stop = FALSE;
  TimeRuler* time_ruler = data;
  GtkRuler* ruler = data;
  GtkWidget* widget = data;
  Arrange* arrange = time_ruler->arrange;
  RulerScrollOp* scroll_op = time_ruler->scroll_op;
  int strength = 0;

  Pti mouse_pos, right = {0};
  time_ruler_get_mouse_position(time_ruler, &mouse_pos, &right);

  if (mouse_pos.x < SCROLL_MASK){
    call(arrange->canvas->scroll_left, arrange, scroll_op->speed/* / SCROLL_MULTIPLIER*/);
    dont_stop = TRUE;
    strength = -mouse_pos.x + SCROLL_EDGE_WIDTH + 1;

  } else if (right.x > -SCROLL_EDGE_WIDTH) {
    // Increase size of canvas:
    // -careful the marker doesnt go out of sync with song->end.
    time_ruler->locators[dragging_loc_num] = widget->allocation.width - 5 + LOWER_PX;

    time_ruler->scroll_op->time = MIN(time_ruler->scroll_op->time + 1, MAX_SCROLL_SPEED);

    arrange->canvas->scroll_right(arrange, scroll_op->speed/* / SCROLL_MULTIPLIER*/);
    dont_stop = TRUE;
    strength = right.x + SCROLL_EDGE_WIDTH;
  }
  else time_ruler->scroll_op->speed = 0;

  int target_speed = (strength  + 2) / 4;
  int difference = target_speed - scroll_op->speed;
  int dx = 0;
  if     (difference > 0) dx = MAX(difference / 50, +1); //speeding up
  else if(difference < 0) dx = MIN(difference / 10, -1); //slowing down
  int speed = scroll_op->speed + dx;

  //if(dont_stop) scroll_op->speed = MIN((int)(scroll_op->speed + 1 * (strength/8)), MAX_SCROLL_SPEED);
  if(dont_stop) scroll_op->speed = MIN(speed, MAX_SCROLL_SPEED);
  else          scroll_op->timer = 0;
  dbg(2, "scrollspeed=%i strength=%i", scroll_op->speed, strength);
  return dont_stop;
}


static void
time_ruler_start_mouse_timer(TimeRuler* time_ruler)
{
  if(!time_ruler->scroll_op) time_ruler->scroll_op = g_new0(RulerScrollOp, 1);
  //dbg(1, "scroll_op=%p", time_ruler->scroll_op);

  //Pti origin; //position that scrolling is done relative to.
  if(!time_ruler->scroll_op->timer) time_ruler->scroll_op->timer = g_timeout_add(SCROLL_TIMEOUT_MS, (gpointer)time_ruler_mouse_timeout, time_ruler);
}


static void
time_ruler_stop_timer(TimeRuler* time_ruler)
{
  PF;
  RulerScrollOp* scroll_op = time_ruler->scroll_op;

  if(scroll_op){
    if(scroll_op->timer) g_source_remove(scroll_op->timer);
    scroll_op->timer = 0;
    scroll_op->speed = 1;
    scroll_op->time  = 0;
  }
}


static void
snprint_bbst(char* str, int len, GPos* pos)
{
  int bars  = pos->beat / 4;
  int beats = pos->beat % 4;
  if(pos->sub) snprintf(str, len-1, "%i:%i:%i", bars, beats, pos->sub);
  else if(beats) snprintf(str, len-1, "%i:%02i", bars, beats);
  else snprintf(str, len-1, "%i", bars);
}


static void
time_ruler_default_text_out(AyyiPanel* panel, const char* format, ...)
{
  //user needs to override
}


//-----------------------------------------------------

//from sweep print.c

/*
 * Print a time in the format HH:MM:SS.sss
 */
static int
snprint_time (gchar * s, gint n, sw_time_t time)
{
  int hrs, min;
  sw_time_t sec;
  char * sign;

  sign = (time < 0.0) ? "-" : "";

  if (time < 0.0) time = -time;

  hrs = (int) (time/3600.0);
  min = (int) ((time - ((sw_time_t)hrs * 3600.0)) / 60.0);
  sec = time - ((sw_time_t)hrs * 3600.0)- ((sw_time_t)min * 60.0);

  /* XXX: %02.3f workaround */
  if (sec < 10.0) {
    //return snprintf (s, n, "%s%02d:%02d:0%2.3f", sign, hrs, min, sec);
    return snprintf (s, n, "%s%02d:0%2.3f", sign, min, sec);
  } else {
    return snprintf (s, n, "%s%02d:%02d:%02.3f", sign, hrs, min, sec);
  }
}

#endif //DEBUG_DISABLE_RULERBAR
