/**
 * @file htmlview.c implementation of the item view interface for HTML rendering
 * 
 * Copyright (C) 2006-2007 Lars Lindner <lars.lindner@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#include <config.h>
#include <string.h>
#include <gtk/gtk.h>
#include "debug/debug.h"

#include "html/html_plugin.h"

#include "ui_htmlview.h"
#include "htmlview.h"
#include "render.h"

// FIXME: namespace clash of LifereaHtmlView *htmlview and htmlView_priv 
// clearly shows the need to merge htmlview.c and src/ui/ui_htmlview.c,
// maybe with a separate a HTML cache object...

extern htmlviewPluginPtr htmlviewPlugin;

static struct htmlView_priv 
{
	GHashTable	*chunkHash;	/**< cache of HTML chunks of all displayed items */
	GSList		*orderedChunks;	/**< ordered list of chunks */
	//nodePtr		node;		/**< the node whose items are displayed */
	guint		missingContent;	/**< counter for items without content */
} htmlView_priv;

typedef struct htmlChunk 
{
	gulong 		id;	/**< item id */
	gchar		*html;	/**< the rendered HTML (or NULL if not yet rendered) */
	time_t		date;	/**< date as sorting criteria */
} *htmlChunkPtr;

#if 0
static void
htmlview_chunk_free (htmlChunkPtr chunk) 
{
	g_free (chunk->html);
	g_free (chunk);
}

static gint
htmlview_chunk_sort (gconstpointer a,
                     gconstpointer b) 
{
	return (((htmlChunkPtr)a)->date) - (((htmlChunkPtr)b)->date);
}
#endif

void 
htmlview_init (void) 
{
	htmlView_priv.chunkHash = NULL;
	htmlView_priv.orderedChunks = NULL;
	//htmlview_clear ();
}

#if 0
void
htmlview_clear (void) 
{
	if (htmlView_priv.chunkHash)
		g_hash_table_destroy (htmlView_priv.chunkHash);

	GSList	*iter = htmlView_priv.orderedChunks;
	while (iter)
	{
		htmlview_chunk_free (iter->data);
		iter = g_slist_next (iter);
	}

	if (htmlView_priv.orderedChunks)
		g_slist_free (htmlView_priv.orderedChunks);
	
	htmlView_priv.chunkHash = g_hash_table_new (g_direct_hash, g_direct_equal);
	htmlView_priv.orderedChunks = NULL;
	htmlView_priv.missingContent = 0;
}
#endif

void
htmlview_update (LifereaHtmlView *htmlview, guint mode) 
{
	PF;

	GString		*output;
	gchar		*baseURL = NULL;

#if 0
	if (!htmlView_priv.node)
	{
		dbg(0, "clearing HTML view as nothing is selected");
		liferea_htmlview_clear (htmlview);
		return;
	}
#endif

	/* determine base URL */
	switch (mode) {
		default:
			//baseURL = (gchar *) node_get_base_url (htmlView_priv.node);
			dbg(0, "FIXME baseurl");
			break;
	}

	if (baseURL) baseURL = g_markup_escape_text (baseURL, -1);
		
	output = g_string_new (NULL);
	htmlview_start_output (htmlview, output, baseURL, TRUE, TRUE);

	htmlview_finish_output (output);

	dbg (0, "writing %d bytes to HTML view", strlen (output->str));
	htmlview_write (htmlview, output->str, baseURL);
	
	g_string_free (output, TRUE);
	g_free (baseURL);
}
