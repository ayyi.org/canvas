/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

typedef struct
{
    AGlActor      actor;
    GlMixerStrip* strip;
} Label;


static AGlActor*
label_new (GlMixerStrip* strip)
{
	bool label__paint (AGlActor* actor)
	{
		Label* label = (Label*)actor;
		AMChannel* channel = ((Label*)actor)->strip->channel;

		if (label->strip->selected) {
			GdkColor* color = &actor->root->gl.gdk.widget->style->bg[GTK_STATE_SELECTED];
			agl_set_colour_uniform(&agl->shaders.plain->uniforms[PLAIN_COLOUR], am_gdk_to_rgba(color));
			agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));
		}

		AyyiChannel* ac = ayyi_mixer__channel_at(channel->ident.idx);
		if (ac) {
			agl_print(1, 2, 0, 0x000000ff, "%s", ac->name);
		}
		return true;
	}

	void aux__set_size (AGlActor* actor)
	{
		actor->region.x2 = agl_actor__width(actor->parent);
	}

	bool aux__on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		return NOT_HANDLED;
	}

	void label__init (AGlActor* actor)
	{
	}

	AGlActor* actor = (AGlActor*)agl_actor__new(Label,
		.actor = {
			.name     = g_strdup("Label"),
			.program  = agl->shaders.plain,
			.region   = (AGlfRegion){0, 0, 30, 20},
			.init     = label__init,
			.paint    = label__paint,
			.set_size = aux__set_size,
			.on_event = aux__on_event
		},
		.strip = strip
	);

	return actor;
}


