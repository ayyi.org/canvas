/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. httpd://ayyi.github.io/samplecat/    |
 | copyright (C) 2020-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "utils/fs.h"
#include "runner.h"
#include "yaml/load.h"
#include "yaml/save.h"

TestFn test_load, test_save_empty;
void test_save ();

gpointer tests[] = {
	test_load,
	test_save_empty,
	test_save,
};

void
setup ()
{
	TEST.n_tests = G_N_ELEMENTS(tests);
}


void
teardown ()
{
}


void
test_load ()
{
	START_TEST;

	static bool have_section = false;
	static bool have_subsection = false;
	static bool have_bad_property = false;
	static char* property1_value = NULL;
	static char* property2_value = NULL;

	void bad_property (const yaml_event_t* event, const char*, gpointer user_data)
	{
		have_bad_property = true;
	}

	void subsection (yaml_parser_t* parser, const char*, gpointer user_data)
	{
		have_subsection = true;

		yaml_load_mapping(parser,
			NULL,
			(YamlValueHandler[]){
				{"property-2", yaml_set_string, &property2_value},
				{NULL}
			},
			user_data
		);
	}

	void section (yaml_parser_t* parser, const char*, gpointer user_data)
	{
		have_section = true;

		yaml_load_mapping(parser,
			(YamlHandler[]){
				{NULL, subsection, user_data},
				{NULL}
			},
			(YamlValueHandler[]){
				{"property-1", yaml_set_string, &property1_value},
				{"property-2", bad_property, user_data},
				{NULL}
			},
			user_data
		);
	}

	bool fn (FILE* fp, gpointer _master)
	{
		return yaml_load(fp, (YamlHandler[]){
			{"section", section, NULL},
			{NULL}
		});
	}

	if (!with_fp("data/1.yaml", "rb", fn, NULL)) {
		FAIL_TEST("error loading");
	}

	assert(have_section, "expected section to be found");
	assert(have_subsection, "expected sub-section to be found");
	assert(property1_value && !strcmp(property1_value, "value-1"), "expected property-1 to be set");
	assert(property2_value && !strcmp(property2_value, "value-2"), "expected property-2 to be set");
	assert(!have_bad_property, "expected bad property not to be found");

	FINISH_TEST;
}


static bool
files_match (const char* path)
{
	g_autofree gchar* contents1;
	g_file_get_contents ("/tmp/test.yaml", &contents1, NULL, NULL);

	g_autofree gchar* contents2;
	g_file_get_contents (path, &contents2, NULL, NULL);

	return !strcmp(contents1, contents2);
}


void
test_save_empty ()
{
	START_TEST;

	bool fn (FILE* fp, gpointer _master)
	{
		g_auto(yaml_emitter_t) emitter;
		g_auto(yaml_event_t) event;

		yaml_start(fp);

		end_map;
		end_document;

		return true;

	  error:
	  out:
		return false;
	}

	assert(with_fp("/tmp/test.yaml", "wb", fn, NULL), "fp");
	assert(files_match ("data/empty.yaml"), "mismatch");

	FINISH_TEST;
}


yaml_emitter_t emitter;
void
test_save ()
{
	START_TEST;

	bool fn (FILE* fp, gpointer _master)
	{
		g_auto(yaml_event_t) event;

		yaml_start(fp);

		{
			map_open("root");
			{
				map_open("section");
				if (!yaml_add_key_value_pair("property-1", "value-1")) goto error;
				{
					map_open("sub-section");
					if (!yaml_add_key_value_pair("property-2", "value-2")) goto error;
					end_map;
				}
				end_map;
			}
			end_map;
		}

		end_map;
		end_document;

		return true;

	  error:
	  out:
		return false;
	}

	assert(with_fp("/tmp/test.yaml", "wb", fn, NULL), "fp");
	assert(files_match ("data/1.yaml"), "mismatch");

	FINISH_TEST;
}
