/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2008 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#include "ayyi/ayyi_dbus.h"

G_BEGIN_DECLS

typedef struct _ExampleDbus ExampleDbus;
typedef struct _ExampleApplicationClass ExampleDbusClass;

#define EXAMPLE_DBUS_TYPE_APPLICATION              (example_dbus_get_type ())
#define EXAMPLE_DBUS_APPLICATION(object)           (G_TYPE_CHECK_INSTANCE_CAST((object), EXAMPLE_DBUS_TYPE_APPLICATION, ExampleDbus))
#define EXAMPLE_DBUS_APPLICATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), EXAMPLE_DBUS_TYPE_APPLICATION, ExampleDbusClass))
#define EXAMPLE_DBUS_IS_APPLICATION(object)        (G_TYPE_CHECK_INSTANCE_TYPE((object), EXAMPLE_DBUS_TYPE_APPLICATION))
#define EXAMPLE_DBUS_IS_APPLICATION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), EXAMPLE_DBUS_TYPE_APPLICATION))
#define EXAMPLE_DBUS_APPLICATION_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), EXAMPLE_DBUS_TYPE_APPLICATION, ExampleDbusClass))

#define EXAMPLE_DBUS_APP                           (example_dbus_get_instance ())

struct _ExampleDbus {
	GObject base_instance;
	
	DBusGConnection *connection;
	gpointer         local_shm; //temporary. c file cannot get properties of c++ class?
};

struct _ExampleApplicationClass {
	GObjectClass base_class;
};

GType           example_dbus_get_type           (void);
ExampleDbus    *example_dbus_get_instance       (void);
gboolean        example_dbus_register_service   (ExampleDbus*);
void            example_dbus_register_signals   ();

gboolean        example_dbus_emit_changed       (ExampleDbus*, GError**);

//the dbus services:
gboolean        example_dbus_get_shm            (ExampleDbus*, const char *_name, guint *address, GError **error);

void            example_dbus_req_signal         (ExampleDbus*);

GQuark          example_dbus_error_quark        ();

G_END_DECLS
