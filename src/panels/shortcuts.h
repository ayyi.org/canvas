/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __shortcuts_h__
#define __shortcust_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_SHORTCUTS_WIN            (ayyi_shortcuts_win_get_type ())
#define AYYI_SHORTCUTS_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_SHORTCUTS_WIN, AyyiShortcutsWin))
#define AYYI_SHORTCUTS_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_SHORTCUTS_WIN, AyyiShortcutsWinClass))
#define AYYI_IS_SHORTCUTS_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_SHORTCUTS_WIN))
#define AYYI_IS_SHORTCUTS_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_SHORTCUTS_WIN))
#define AYYI_SHORTCUTS_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_SHORTCUTS_WIN, AyyiShortcutsWinClass))

typedef struct _AyyiShortcutsWinClass AyyiShortcutsWinClass;
typedef struct _AyyiShortcutsWin      AyyiShortcutsWin;

struct _AyyiShortcutsWinClass {
    AyyiPanelClass parent_class;
};

struct _AyyiShortcutsWin {
    AyyiPanel          panel;
  
    GtkTreeView*       treeview;
};

G_END_DECLS

#endif
