/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __toolbox_h__
#define __toolbox_h__

typedef struct
{
    char*         key;
    char*         stock_id;
    GdkCursorType cursor;
    GtkAction*    action;
    bool          (*active)(AyyiPanel*); // fn to check to see if the button should be marked 'active'.
} ToolboxButton;

void  toolbox_new         (Arrange*, GtkWidget* container);
void  toolbox_free        (Arrange*);
void  toolbox_change_tool (Arrange*, int tooltype);
void  toolbox_position    (GtkMenu*, gint*, gint*, gboolean*, gpointer);

#endif
