/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib.h>
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"


#define AYYI_TYPE_HSCALE (ayyi_hscale_get_type ())
#define AYYI_HSCALE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_HSCALE, AyyiHScale))
#define AYYI_HSCALE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_HSCALE, AyyiHScaleClass))
#define AYYI_IS_HSCALE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_HSCALE))
#define AYYI_IS_HSCALE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_HSCALE))
#define AYYI_HSCALE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_HSCALE, AyyiHScaleClass))

typedef struct _AyyiHScale AyyiHScale;
typedef struct _AyyiHScaleClass AyyiHScaleClass;
typedef struct _AyyiHScalePrivate AyyiHScalePrivate;

struct _AyyiHScale {
	GtkHScale parent_instance;
	AyyiHScalePrivate * priv;
};

struct _AyyiHScaleClass {
	GtkHScaleClass parent_class;
};


static gpointer ayyi_hscale_parent_class = NULL;

GType ayyi_hscale_get_type (void) G_GNUC_CONST;
enum  {
	AYYI_HSCALE_0_PROPERTY
};
AyyiHScale* ayyi_hscale_new (GtkAdjustment* adj);
AyyiHScale* ayyi_hscale_construct (GType object_type, GtkAdjustment* adj);


AyyiHScale*
ayyi_hscale_construct (GType object_type, GtkAdjustment* adj)
{
	g_return_val_if_fail (adj != NULL, NULL);

	AyyiHScale* self = (AyyiHScale*) g_object_new (object_type, NULL);
	gtk_range_set_adjustment ((GtkRange*) self, adj);

	return self;
}


AyyiHScale*
ayyi_hscale_new (GtkAdjustment* adj)
{
	return ayyi_hscale_construct (AYYI_TYPE_HSCALE, adj);
}


static void
ayyi_hscale_class_init (AyyiHScaleClass* klass)
{
	ayyi_hscale_parent_class = g_type_class_peek_parent (klass);
}


static void
ayyi_hscale_instance_init (AyyiHScale* self)
{
}


GType
ayyi_hscale_get_type (void)
{
	static volatile gsize ayyi_hscale_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&ayyi_hscale_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (AyyiHScaleClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) ayyi_hscale_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (AyyiHScale), 0, (GInstanceInitFunc) ayyi_hscale_instance_init, NULL };
		GType ayyi_hscale_type_id;
		ayyi_hscale_type_id = g_type_register_static (GTK_TYPE_HSCALE, "AyyiHScale", &g_define_type_info, 0);
		g_once_init_leave (&ayyi_hscale_type_id__volatile, ayyi_hscale_type_id);
	}
	return ayyi_hscale_type_id__volatile;
}
