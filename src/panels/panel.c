/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include "gdl/gdl-dock.h"
#include "gdl/gdl-dock-paned.h"
#include "support.h"
#include "window.h"
#include "windows.h"
#include "toolbar.h"
#include "settings.h"
#include "panel.h"

extern SMConfig* config;

static void     ayyi_panel_class_init         (AyyiPanelClass*);
static void     ayyi_panel_init               (AyyiPanel*); 
static GObject* ayyi_panel_constructor        (GType, guint, GObjectConstructParam*);
static void     ayyi_panel_set_property       (GObject*, guint prop_id, const GValue*, GParamSpec*);
static void     ayyi_panel_get_property       (GObject*, guint prop_id, GValue*, GParamSpec*);
static void     ayyi_panel_finalize           (GObject*);
static void     ayyi_panel_on_realise         (GtkWidget*, gpointer);
static bool     ayyi_panel_on_focus_in        (GtkWindow*, GdkEventFocus*, AyyiPanel*);

static bool     dnd_box__on_button_press      (GtkWidget*, GdkEventButton*, AyyiPanel*);
static bool     dnd_box__on_motion            (GtkWidget*, GdkEventMotion*, AyyiPanel*);
static void     dnd_box__on_drag_end          (GtkWidget*, GdkDragContext*, AyyiPanel*);

int             ayyi_panel_drag_dataget       (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, AyyiPanel*);

extern void     gdl_dock_item_drag_start      (GdlDockItem*);
extern void     gdl_dock_master_drag_end      (GdlDockItem*, gboolean cancelled, gpointer);

G_DEFINE_TYPE (AyyiPanel, ayyi_panel, GDL_TYPE_DOCK_ITEM)

char constructor_colour [16] = "\x1b[38;5;159m";


static void
ayyi_panel_class_init (AyyiPanelClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	klass->has_menubar = false;
	klass->min_size.x  = 20;
	klass->min_size.y  = 20;

    g_object_class->constructor = ayyi_panel_constructor;
	g_object_class->finalize = ayyi_panel_finalize;
	g_object_class->set_property = ayyi_panel_set_property;
	g_object_class->get_property = ayyi_panel_get_property;

	//widget_class->show = ayyi_panel_show;
	//widget_class->hide = ayyi_panel_hide;

	void toolbar_toggle (AyyiWindow* window, gpointer _)
	{
		AyyiPanel* panel = app->active_panel;
		if(panel){
			toolbar_show_toggle(panel);
		}
	}

	bool toolbar_get_value (AyyiWindow* window, gpointer view_type)
	{
		/*
		int i; for(i=0;i<view_items->len;i++){
			ViewItem* item = &g_array_index(view_items, ViewItem, i);
			if(item->toggle == toolbar_toggle){
				dbg(0, "TODO disable toolbar item");
				break;
			}
		}
		*/

		if(app->active_panel && app->active_panel->toolbar_box[0]){
			menu_view_item_enable(0, true);
			return gtk_widget_get_visible(app->active_panel->toolbar_box[0]);
		}else{
			menu_view_item_enable(0, false);
		}

		return true;
	}
	menu_add_view_item("Toolbar", toolbar_get_value, toolbar_toggle, NULL);
}


static void
ayyi_panel_init (AyyiPanel* panel)
{
	// called before the init of the child panel-type
	// Gobject properties are not yet set.

#ifdef HAVE_GTK_2_18
	gtk_widget_set_can_focus((GtkWidget*)panel, true);
#endif
}


static GObject*
ayyi_panel_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	GObject* g_object = G_OBJECT_CLASS(ayyi_panel_parent_class)->constructor(type, n_construct_properties, construct_param);
	AyyiPanel* panel = (AyyiPanel*)g_object;

	AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(g_object));
	panel_class->instances++;

	panel->follow = panel_class->has_follow;
	panel->link   = true;

	panel->zoom = observable_new_point(1.0, 1.0);

	#define VBOX_PADDING 0 // some windows need 0, most probably need 2
	panel->vbox = gtk_vbox_new(NON_HOMOGENOUS, VBOX_PADDING);

	GtkWidget* dnd_ebox = panel->dnd_ebox = gtk_event_box_new();
	gtk_drag_dest_set(dnd_ebox, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
	gtk_container_add(GTK_CONTAINER(panel), dnd_ebox);
												// todo why special handling? see below
												//gtk_container_add(GTK_CONTAINER(dnd_ebox), panel->vbox);

#ifdef USE_HELP
	extern GType ayyi_help_win_get_type();
	if(G_OBJECT_TYPE(panel) != ayyi_help_win_get_type()){ //tmp. need to work out how to do drag docking with html windows.
#endif
		g_signal_connect(G_OBJECT(dnd_ebox), "button-press-event", G_CALLBACK(dnd_box__on_button_press), panel);
#ifdef USE_HELP
	}
#endif

	g_signal_connect(G_OBJECT(dnd_ebox), "motion-notify-event", G_CALLBACK(dnd_box__on_motion), panel);
	//g_signal_connect(G_OBJECT(dnd_ebox), "drag-begin", G_CALLBACK(window__on_drag_begin), window);
	///g_signal_connect(G_OBJECT(dnd_ebox), "drag-motion", G_CALLBACK(window__on_drag_motion), window);
	g_signal_connect(G_OBJECT(dnd_ebox), "drag-end", G_CALLBACK(dnd_box__on_drag_end), panel);

	gtk_drag_source_set(dnd_ebox, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);
	//g_signal_connect (G_OBJECT(dnd_ebox), "drag-data-get", G_CALLBACK(ayyi_panel_drag_dataget), window);

	gtk_container_add(GTK_CONTAINER(dnd_ebox), panel->vbox);

	if(panel_class->has_toolbar) panel->toolbar = toolbar_new(panel, panel->vbox);

#if 0
	GdlDockObject* dock_object = (GdlDockObject*)g_object;
	GdlDockMaster* dock_master = (GdlDockMaster*)dock_object->master;
	if(dock_master){
		GList* l = dock_master->toplevel_docks;
		if(g_list_length(l) != 1) pwarn("expected 1 toplevel dock"); // this assumption is incorrect: during layout loading, widgets are created before their parents.
		GdlDock* dock = l->data;
	}
#endif

	// temporary - decide where/when to call panel->ready
	gboolean idle_ready (gpointer _panel)
	{
		AyyiPanel* panel = _panel;
		g_return_val_if_fail(panel->window, G_SOURCE_REMOVE);
		AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(_panel));

		if(panel_class->on_ready) panel_class->on_ready((AyyiPanel*)_panel);

		ayyi_panel_on_open(panel);

		panel->priv.ready_idle = 0;
		return G_SOURCE_REMOVE;
	}
	panel->priv.ready_idle = g_idle_add(idle_ready, g_object);

	g_object_set(g_object, "preferred-width", panel_class->default_size.x, "preferred-height", panel_class->default_size.y, NULL);

	g_signal_connect(G_OBJECT(panel), "focus-in-event", G_CALLBACK(ayyi_panel_on_focus_in), panel);
	g_signal_connect(G_OBJECT(panel), "realize", G_CALLBACK(ayyi_panel_on_realise), panel);

	return g_object;
}


#if 0
int start_x, start_y;

				static gboolean
				dnd_box__on_button_press(GtkWidget* widget, GdkEventButton* event, AyyiPanel* panel)
				{
				  PF;

				  //set correct Panel focus when an empty area is clicked.
				  GtkWidget* parent = gtk_widget_get_parent(widget);
				  gtk_widget_grab_focus(parent);

				  //just testing...
				  panel_foreach {
					gtk_drag_dest_unset(panel->dnd_ebox);
					gtk_drag_source_unset(panel->dnd_ebox);
				  } end_panel_foreach

				  start_x = event->x;
				  start_y = event->y;
				  GDL_DOCK_ITEM_SET_FLAGS (GDL_DOCK_ITEM(panel), GDL_DOCK_IN_PREDRAG);
				  return HANDLED;
				}


						static gboolean
						dnd_box__on_motion(GtkWidget* widget, GdkEventMotion* event, AyyiPanel* panel)
						{
						  GdlDockItem* item = GDL_DOCK_ITEM(panel);
						  if (GDL_DOCK_ITEM_IN_PREDRAG (item)) {
							if (gtk_drag_check_threshold (widget, start_x, start_y, event->x, event->y)) {
							  GDL_DOCK_ITEM_UNSET_FLAGS (item, GDL_DOCK_IN_PREDRAG);

							  dbg(0, "drag threshold reached. calling drag_start...");
							  //item->dragoff_x = item->_priv->start_x;
							  //item->dragoff_y = item->_priv->start_y;
							  gdl_dock_item_drag_start(item);
							}
						  }

						  return NOT_HANDLED;
						}
#endif
void
ayyi_panel_on_new (AyyiPanel* panel, Size* size, GtkOrientation orientation)
{
	// for internal use by AyyiPanel implementations only

	// param size: optional - if not present, the default size for the panel class will be used.

	// note: we dont show the panel yet. Wait until resizing is done.

	/*

	GtkWindow
	+--GdlDock
	   +--AyyiPanel
	      +--GtkEventBox  dnd_ebox
	         +--GtkVBox

	*/

																				// TODO this now only sets the size
																				//      -it should be run only if not in a dock layout.

	AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel));

	int width = 0;
	int height = 0;
	if(size && size->width)             { width = size->width;                 height = size->height; }
	else if(panel_class->default_size.x){ width = panel_class->default_size.x; height = panel_class->default_size.y; }

	if(width || height) gtk_widget_set_size_request((GtkWidget*)panel, width, height);

	g_object_set(panel, "preferred-width", width, NULL);
}


void
ayyi_panel_set_name (AyyiPanel* panel, PanelType dyn_type)
{
	AyyiPanelClass* klass = g_type_class_peek(dyn_type);

#if 0 // using DockObject->name seems to be problematic as GdlDock has some unexplained concept and use of 'named items'
	GdlDockObject* dock_object = (GdlDockObject*)panel;

	//dont add a number to the string for the first window instance.
	dock_object->name = klass->instances > 1
		? g_strdup_printf("%s %i", klass->name, klass->instances)
		: g_strdup_printf("%s", klass->name);
#else
	GValue value = {0,};
	g_value_init(&value, G_TYPE_STRING);
	g_value_set_string(&value, klass->name);
	g_object_set_property((GObject*)panel, "long-name", &value);
	g_value_unset(&value);
#endif
}


void
ayyi_panel_get_id_string (AyyiPanel* panel, char* id_str)
{
	snprintf(id_str, STRLEN_WIN_ID-1, "%s %i", AYYI_PANEL_GET_CLASS(panel)->name, ayyi_panel_get_instance_num(panel));
}


void
ayyi_panel_get_id_string_pretty (AyyiPanel* panel, char* id_str)
{
	//dont add a number to the string for the first window instance.
	int instance = ayyi_panel_get_instance_num(panel);
	if(instance > 1) snprintf(id_str, STRLEN_WIN_ID-1, "%s %i", AYYI_PANEL_GET_CLASS(panel)->name, instance);
	else             snprintf(id_str, STRLEN_WIN_ID-1, "%s", AYYI_PANEL_GET_CLASS(panel)->name);
}


#if 0
void
ayyi_panel__get_id_string_pretty_by_type(PanelType dyn_type, int instance, char* id_str)
{
	AyyiPanelClass* klass = g_type_class_peek(dyn_type);

	// dont add a number to the string for the first window instance.
	if(instance > 1) snprintf(id_str, STRLEN_WIN_ID-1, "%s %i", klass->name, instance);
	else             snprintf(id_str, STRLEN_WIN_ID-1, "%s", klass->name);
}
#endif


void
ayyi_panel_on_open (AyyiPanel* panel)
{
	// set the size and position of the window, and a few misc things.

	if(config->windows){
		AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);
		char group_name[64] = {'\0'};

#if 0
		//set bg colour as debugging aid

		if(window->dnd_ebox){
			GtkStyle* style = NULL;
			style = gtk_style_copy(gtk_widget_get_style(window->widget));
			style->bg[GTK_STATE_NORMAL] = style->bg[GTK_STATE_SELECTED];
			style->fg[GTK_STATE_NORMAL] = style->fg[GTK_STATE_SELECTED];
			gtk_widget_set_style(window->dnd_ebox, style);
			if(style) g_object_unref(style);
		}
#endif

		config_make_group_name(klass, klass->instances-1, group_name);
		dbg (2, "group_name=%s", group_name);
	}
	gtk_widget_show_all(gtk_widget_get_toplevel((GtkWidget*)panel));

	request_toolbar_update();
}


static void
ayyi_panel_on_realise (GtkWidget* widget, gpointer data)
{
	AyyiPanel* panel = AYYI_PANEL(widget);
	if(!panel->size_is_allocated && GTK_WIDGET_REALIZED(panel)){
		AyyiPanelClass* panel_class = AYYI_PANEL_GET_CLASS(panel);
		dbg(2, "setting minimum size: %i", panel_class->min_size.x);
		int width = panel_class->min_size.x;
#if 0
		GtkWidget* parent = gtk_widget_get_parent(widget);
		if(G_OBJECT_TYPE(parent) == GDL_TYPE_DOCK_PANED || G_OBJECT_TYPE(parent) == GDL_TYPE_DOCK || G_OBJECT_TYPE(parent) == GTK_TYPE_HPANED){
			width = widget->requisition.width;
		}
		dbg(0, "name=%s width=%i", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(parent)), width);
#endif
		//if(G_OBJECT_TYPE(parent) != GTK_TYPE_HPANED)
		gtk_widget_set_size_request(widget, width, panel_class->min_size.y);
		panel->size_is_allocated = true;
		g_signal_handlers_disconnect_by_func(G_OBJECT(widget), ayyi_panel_on_realise, NULL);
	}
}


int
ayyi_panel_get_depth (AyyiPanel* panel)
{
	int depth = 0;
	int recursion = 0;
	GtkWidget* parent = (GtkWidget*)panel;
	while((parent = gtk_widget_get_parent((GtkWidget*)parent))){
		dbg(4, "parent=%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(parent)));
		GType type = G_OBJECT_TYPE(parent);
		if(G_OBJECT_TYPE(parent) == GDL_TYPE_DOCK){ dbg(4, "  is GdlDock"); break; }
		if(type != GDL_TYPE_DOCK_PANED) depth++;
		g_return_val_if_fail(recursion++ < 16, 0);
	}
	return depth;
}


bool
ayyi_panel_is_pressed (AyyiPanel* panel)
{
	if(panel != app->active_panel) return false; //not tested

	GdkModifierType mask;
	gdk_window_get_pointer(((GtkWidget*)panel)->window, NULL, NULL, &mask);
	return (mask & GDK_BUTTON1_MASK) >> 8;
}


/*
 *  @return: instance number is 1-based, not zero-based.
 */
int
ayyi_panel_get_instance_num (AyyiPanel* target_panel)
{
	g_return_val_if_fail(target_panel, 0);

	GType target_type = G_OBJECT_TYPE(target_panel);
	int instance = 0;

	panel_foreach {
		if(G_OBJECT_TYPE(panel) == target_type){
			instance++;
			if(panel == target_panel) return instance;
		}
	} end_panel_foreach

#ifdef DEBUG
	perr ("window not found. type=%li %s", target_type, G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(target_panel)));
#endif
	return 0;
}


void
ayyi_panel_print_type (AyyiPanel* panel, char* str)
{
	// gives the verbose panel-type string for the given Panel.

	if(AYYI_IS_PANEL(panel)) g_strlcpy(str, AYYI_PANEL_GET_CLASS(panel)->name, AYYI_SHORT_NAME_MAX);
}


void
ayyi_panel_print_label (AyyiPanel* panel, char* str)
{
	// prints the type and instance number.

	if(!panel){ str[0] = '\0'; return; }

	char type[AYYI_SHORT_NAME_MAX];
	ayyi_panel_print_type(panel, type);

	AyyiPanelClass* panel_class = AYYI_PANEL_GET_CLASS(panel);
	if(panel_class->instances < 2){
		strcpy(str, type);
	}else{
		snprintf(str, 63, "%s [%i]", type, ayyi_panel_get_instance_num(panel));
	}
}


void
ayyi_panel_print_label_safe (AyyiPanel* panel, char* str)
{
	// prints the type and instance number.
	// used when saving.

	dbg(0, "%stype=%i %s %s", ayyi_green, G_OBJECT_TYPE(panel), AYYI_PANEL_GET_CLASS(panel)->name, white);

	char type[AYYI_SHORT_NAME_MAX];
	ayyi_panel_print_type(panel, type);

	if (AYYI_PANEL_GET_CLASS(panel)->instances < 2) {
		strcpy(str, type);
	} else {
		snprintf(str, 63, "%s-%i", type, ayyi_panel_get_instance_num(panel));
	}
}


static void
ayyi_panel_set_property (GObject* g_object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	AyyiPanel* object = AYYI_PANEL (g_object);

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}


static void
ayyi_panel_get_property (GObject* g_object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	AyyiPanel* object = AYYI_PANEL (g_object);

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}


static void
ayyi_panel_finalize (GObject* g_object)
{
	dbg(1, "%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(g_object)));

	AyyiPanel* panel = (AyyiPanel*)g_object;
	g_return_if_fail (g_object != NULL && GDL_IS_DOCK_ITEM (g_object));

	g_source_remove0(panel->priv.ready_idle);
	g_clear_pointer(&panel->zoom, observable_free);

	if(panel->toolbar) toolbar_free(&panel->toolbar);

	panel->destroyed = true;

	G_OBJECT_CLASS(ayyi_panel_parent_class)->finalize(g_object);
}


static bool
ayyi_panel_on_focus_in (GtkWindow* window, GdkEventFocus* event, AyyiPanel* panel)
{
#if 0
	char type[AYYI_SHORT_NAME_MAX]; ayyi_panel_print_type(panel, type); dbg(0, "windowtype=%s", type);
#endif
	return NOT_HANDLED;
}


void
ayyi_panel_on_focus (AyyiPanel* panel)
{
	PF;
	g_return_if_fail(panel);
#ifdef DEBUG
	if(!windows__verify_pointer(panel, 0)) return;
#endif
	AyyiWindow* window = panel->window;
	if(panel == window->focussed_panel && panel == app->active_panel) return;

	gboolean emit_changed (gpointer _window)
	{
		AyyiWindow* window = _window;

		if(g_list_index(windows, window) > -1){ // check window has not been removed while waiting

			if(window->shell_menus) menu_sync_panel_items(window);

			g_signal_emit_by_name(app, "focus-changed");
		}
		return G_SOURCE_REMOVE;
	}

	GObjectClass* panel_class = G_OBJECT_GET_CLASS(panel);
	if(!window->focussed_panel || (panel_class != G_OBJECT_GET_CLASS(window->focussed_panel)) || !app->active_panel || (panel_class != G_OBJECT_GET_CLASS(app->active_panel))){
		app->active_panel = panel;

		dbg(2, "focus changed! %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));
		if(window->focussed_panel){
			ayyi_panel_on_blur(window->focussed_panel);
		}

		window->focussed_panel = panel;

		GtkAccelGroup* accel_group = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(panel))->accel_group;
		if(accel_group){
			gtk_window_add_accel_group((GtkWindow*)window, accel_group);
		}

		g_idle_add(emit_changed, window);
	}

	window->focussed_panel = panel;
	app->active_panel = panel;

	return;
}


void
ayyi_panel_on_blur (AyyiPanel* panel)
{
	GtkAccelGroup* accel_group = ((AyyiPanelClass*)G_OBJECT_GET_CLASS(panel))->accel_group;
	if(accel_group){
		GtkWindow* window = (GtkWindow*)panel->window;
		gtk_window_remove_accel_group(window, accel_group);
	}
	panel->window->focussed_panel = NULL;
}


/*
 *  Show the window (raise to top, etc)
 */
void
ayyi_panel_present (AyyiPanel* panel)
{
	g_return_if_fail(panel);

	gtk_window_present(GTK_WINDOW(panel->window));
	ayyi_panel_on_focus(panel);
}


bool
ayyi_panel_present_idle (AyyiPanel* panel)
{
	// presents the window after other drawing operations are complete.

	ayyi_panel_present(panel);
	return false;
}


static int start_x, start_y;

static bool
dnd_box__on_button_press (GtkWidget* widget, GdkEventButton* event, AyyiPanel* panel)
{
	PF;

	// set correct Panel focus when an empty area is clicked.
	GtkWidget* parent = gtk_widget_get_parent(widget);
	gtk_widget_grab_focus(parent);

	if(event->type == GDK_2BUTTON_PRESS){
		return HANDLED;
	}

	// just testing...
	panel_foreach {
		gtk_drag_dest_unset(panel->dnd_ebox);
		gtk_drag_source_unset(panel->dnd_ebox);
	} end_panel_foreach

	start_x = event->x;
	start_y = event->y;
	GDL_DOCK_ITEM_SET_FLAGS (GDL_DOCK_ITEM(panel), GDL_DOCK_IN_PREDRAG);

	return HANDLED;
}


static void
dnd_box__on_drag_end (GtkWidget* widget, GdkDragContext* drag_context, AyyiPanel* panel)
{
	PF;
	gboolean cancelled = FALSE;
	gdl_dock_master_drag_end(GDL_DOCK_ITEM(panel), cancelled, GDL_DOCK_OBJECT(panel)->master);
}


static bool
dnd_box__on_motion (GtkWidget* widget, GdkEventMotion* event, AyyiPanel* panel)
{
	GdlDockItem* item = GDL_DOCK_ITEM(panel);
	if (GDL_DOCK_ITEM_IN_PREDRAG (item)) {
		if (gtk_drag_check_threshold (widget, start_x, start_y, event->x, event->y)) {
			GDL_DOCK_ITEM_UNSET_FLAGS (item, GDL_DOCK_IN_PREDRAG);

			dbg(0, "drag threshold reached. calling drag_start...");
			gdl_dock_item_drag_start(item);
		}
	}

	return NOT_HANDLED;
}


void
ayyi_panel_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	// currently this only handles docking.

	if(!data || data->length < 0){ pwarn("no data!"); return; }

	dbg(0, "%s", data->data);

	if(info == GPOINTER_TO_INT(GDK_SELECTION_TYPE_STRING)) printf(" type=string.\n");

	AyyiPanel* window = windows__get_panel_from_widget(widget);

	GList* list = uri_list_to_glist((gchar*)data->data);
	int i = 0;
	for(; list; list = list->next){
		char* u = list->data;
		dbg (0, "%i: %s\n", i, u);

		gchar* method_string;
		vfs_get_method_string(u, &method_string);
		if(!strcmp(method_string, "dock")){
			ayyi_panel_on_drop_dock_item(window, u);
		}
		else dbg(0, "unexpected dnd method: %s.", method_string);
		i++;
	}
	uri_list_free(list);

	return;
}


static bool
dnd_get_dock_src (const char* uri, char* dock_src)
{
	// expect a string of form: "dock:WINDOW_INSTANCE_STRING"

	dock_src[0] = '\0';

	dbg(2, "uri=%s", uri);

	char* window_type = strstr(uri, ":");
	if(!window_type){ pwarn ("cannot parse uri: '%s'.", uri); return FALSE; }

	window_type += 1;
	if(!window_type){ pwarn ("empty window type: '%s'.", uri); return FALSE; }

	int len = strlen(window_type);
	memcpy(dock_src, window_type, len);
	dock_src[len] = '\0';

	return true;
}


bool
ayyi_panel_on_drop_dock_item (AyyiPanel* panel, const char* u)
{
	perr("NEVER GET HERE");

	// This is somewhat deprecated now, as gdl handles the dnd drops.

	pwarn("should be deprecated. Still used for internal dock drop ops, eg Pool treeview to Arrange canvas.");
	PF;

	GdlDockItem* window__get_dock_item (AyyiPanel* src)
	{
		// warning! dont think this will work any longer

		dbg(2, "getting dock item for: %i", ayyi_panel_get_instance_num(src));

		char win_id[STRLEN_WIN_ID];
		ayyi_panel_get_id_string(src, win_id);

		GdlDockItem* dock_item = NULL;
		GList* i = gdl_dock_get_named_items(src->window->dock);
		if(i){
			for(;i;i=i->next){
				GdlDockItem* item = i->data;
				GdlDockObject* object = &item->object;
				dbg(3, "  item: %s", object->name);
				if(!strcmp(object->name, win_id)){
					dock_item = item;
					break;
				}
			}
			g_list_free(i);
		}
		if(!dock_item) pwarn("failed");
		return dock_item;
	}

	char dock_src[32];
	if(dnd_get_dock_src(u, dock_src)){
		AyyiPanel* src = windows__get_from_instance_string(dock_src);
		windows__verify_pointer(src, 0);
		//dbg (0, "src_window=%s %s", dock_src, window_classes[src->type].name);
		if(src){
			dbg(0, "redocking '%s' to the %s window...", dock_src, AYYI_PANEL_GET_CLASS(panel)->name);

			if(src->window->dock){
				GdlDockItem* src_dock_item = window__get_dock_item(src);

				AyyiWindow* window = panel->window;
				if(window->dock && src_dock_item){
					/*

					The 'proper' gdl drag fns are tied to the gdl-master object via signals.
					If we want to use them, we have to let gdl handle the events.

					What is the correct method for 'chaining-up' drag targets? the docs dont say.
					Note that gdl doesnt use drag-data-received signal, it uses drag-end instead.
					We cant call it directly without making it non-static.
					Maybe we should make a new object which replaces gdl-master, or import it into sm_window.

					/TODO we *can* connect to the layout-changed signal to resize our panes.

					*/

					windows__print_dock_item(src_dock_item, "src");

					//------------------------------------------------------------------------

					char win_id[32];
					ayyi_panel_get_id_string(panel, win_id);

					// get placeholders
					char placeholder_name[32];
					for(int i=1;i<5;i++){
						snprintf(placeholder_name, 31, "%.10s ph%i", win_id, i);
#ifdef DEBUG
						GdlDockPlaceholder* ph = gdl_dock_get_placeholder_by_name(panel->window->dock, placeholder_name);
						if(ph){
							GtkContainer* container = &(GDL_DOCK_OBJECT(ph))->container;
							GList* ph_children = gtk_container_get_children(container);
							dbg(0, "  placeholder=%p n_children=%i", ph, g_list_length(ph_children));
						} else dbg(0, "no placeholder '%s'", placeholder_name);
#endif
					}

					GList* children = gtk_container_get_children (GTK_CONTAINER(window->dock));
					dbg(0, "dest dock has: n_children=%i", g_list_length(children));
					for(GList* l=children;l;l=l->next){
						if(!GDL_IS_DOCK_OBJECT(l->data)){ dbg(0, "  not dock object."); continue; }
#ifdef DEBUG
						GdlDockObject* object = GDL_DOCK_OBJECT(l->data);
						dbg(0, "  %s %s", object->name ? object->name : "<Unnamed>", object->long_name);

						//windows__print_dock_object(object, "child");
						//gdl_dock_item_print(GDL_DOCK_ITEM(object));
#endif
					}

					gdl_dock_object_dock(GDL_DOCK_OBJECT(window->dock), GDL_DOCK_OBJECT(src_dock_item), GDL_DOCK_LEFT, NULL);

					//testing...
					g_signal_emit_by_name (AYYI_WINDOW_GET_CLASS(window)->master_dock, "layout-changed");
				}
				else pwarn("cannot dock to window. No dest dock found.");
			}
			else pwarn("panel->dock is not set for source.");
		}
	}
	return true;
}


int
ayyi_panel_drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, AyyiPanel* sm_window)
{
	//for _outgoing_ drags.

	PF;

	char text[64];
	gboolean data_sent = FALSE;

	if(!windows__verify_pointer(sm_window, 0)) return data_sent;

	char id_str[STRLEN_WIN_ID];
	ayyi_panel_get_id_string(sm_window, id_str);
	snprintf(text, 63, "dock:%s%c%c", id_str, 13, 10);

	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (guchar*)text, strlen(text));
	data_sent = TRUE;
	return 0;
}


#ifdef NEVER
static void
window__on_drag_begin (GtkWidget* widget, GdkDragContext* drag_context, AyyiPanel* panel)
{
	panel_foreach {
		gtk_drag_dest_unset(window->dnd_ebox);
		gtk_drag_source_unset(window->dnd_ebox);
	} end_panel_foreach;
	gdl_dock_master_drag_begin(GDL_DOCK_ITEM(panel->dock_item), GDL_DOCK_OBJECT(panel->dock_item)->master);
}


static gboolean
window__on_drag_motion (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, guint time, AyyiPanel* panel)
{
	GdlDockItem* item = GDL_DOCK_ITEM(panel->dock_item);
	gpointer master = GDL_DOCK_OBJECT(panel->dock_item)->master;
	gint win_x, win_y;
	gdk_window_get_origin (widget->window, &win_x, &win_y);
	gdl_dock_master_drag_motion(item, x + win_x, y + win_y, master);
	return NOT_HANDLED;
}


static void
window__on_dock_drag_begin (GdlDockItem* item, gpointer data)
{
}
#endif


