/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2012-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

uniform vec4 colour;
uniform float width;
uniform float value;
varying vec2 MCposition;

void main(void)
{
	if(MCposition.x + 4.0 < width * value || MCposition.x - 4.0 > width * value){
		gl_FragColor = colour / 1.5;
	}else{
		gl_FragColor = colour;
	}
}

