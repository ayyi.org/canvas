/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include <gmodule.h>
#include <ayyi/ayyi.h>
#include <ayyi/ayyi_client.h>
#include <model/model_types.h>
#include <model/song.h>
#include "demo.h"

#define APPLICATION_SERVICE_NAME "org.ayyi.exampleplugin.ApplicationService"
#define DBUS_APP_PATH            "/org/ayyi/exampleplugin/Exampleplugin"
#define DBUS_INTERFACE           "org.ayyi.exampleplugin.Application"

#define PLUGIN_TYPE_A 1
#define PLUGIN_API_VERSION 1

#define DECLARE_PLUGIN(plugin) \
	G_MODULE_EXPORT AyyiPluginPtr plugin_get_info() { \
		return &plugin; \
	}

#define DECLARE_DEMO_PLUGIN(plugin) \
	G_MODULE_EXPORT demoPluginPtr example_plugin_get_info() { \
		return &plugin; \
	}

static void ui_demo_run (AyyiPluginPtr);

static struct DemoPlugin exampleplugin_info = {
	.api_version = 1,
	.name        = "Name",
	.run         = ui_demo_run,
};

static struct _AyyiPlugin pi = {
	PLUGIN_API_VERSION,
	"Ayyi UI Demo",
	PLUGIN_TYPE_A,
	APPLICATION_SERVICE_NAME,
	DBUS_APP_PATH,
	DBUS_INTERFACE,
	&exampleplugin_info
};

DECLARE_PLUGIN(pi);
DECLARE_DEMO_PLUGIN(exampleplugin_info);

static void
ui_demo_run (AyyiPluginPtr plugin)
{
	dbg(0, "RUN");

	void on_load (AyyiIdent _, GError** e, gpointer user_data)
	{
		void on_clear (AyyiIdent id, GError** error, gpointer _)
		{
			printf("Demo finished\n");
		}
		am_song__clear_tracks(on_clear, NULL);
	}

	char* demo_session = g_strdup_printf("%s/docs/"PACKAGE_NAME"/demo_session", g_get_home_dir());
	if(strcmp(((AyyiSongService*)ayyi.service)->song->path, demo_session)){
		dbg(0, "need to change session");

		am_song__load_new(demo_session, on_load, NULL);
	} else {
		on_load(AYYI_NULL_IDENT, NULL, NULL);
	}
	g_free(demo_session);
}

