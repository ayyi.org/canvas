using GLib;
using Gdk;
using Ayyi;

[CCode(cheader_filename = "ayyi_client.h")]

public class Ayyi.AddmiditrackClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new AddmiditrackClient();

		if(!app.run(((AddmiditrackClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		string name = "Midi Track";
		char* n = AyyiSong.container_next_name(&((SongService*)ayyi.service)->song->midi_tracks, name);
		AM.Song.add_track(TrackType.MIDI, n, 1, (obj, error) => {
			log_print(LogType.OK, "done.");
			print("ok. track count: %i", AyyiSong.get_playlist_count());
			app.quit();
		});
	}
}


