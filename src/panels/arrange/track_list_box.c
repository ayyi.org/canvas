/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "global.h"
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include "model/track_list.h"
#include "support.h"
#include "windows.h"
#include "track_control.h"
#include "track_list_box.h"

struct _TrackListBoxPrivate {
	GtkImage* image1;
};

G_DEFINE_TYPE_WITH_PRIVATE (TrackListBox, track_list_box, GTK_TYPE_VIEWPORT)
enum  {
	TRACK_LIST_BOX_DUMMY_PROPERTY
};

static TrackNum track_drag_highlight = -1; // track that is currently highlighted, or -1 for none.

static void         track_list_box_finalize    (GObject*);
static void         trkctl_drag_begin          (GtkWidget*, GdkDragContext*, gpointer);
static bool         trkctl_on_motion           (GtkWidget*, GdkEventMotion*, gpointer);
static void         trkctl_drag_dataget        (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint info, guint time, TrackListBox*);
static void         track_drag_received        (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, Arrange*);
static bool         track_drag_motion          (GtkWidget*, GdkDragContext*, gint x, gint y, guint time, Arrange*);
static TrackDispNum track_drag_get_dest_track  (GtkWidget*, Arrange*, int y);
static void         tracklist_drag_unhighlight (Arrange*);


TrackListBox*
track_list_box_construct (GType object_type)
{
	TrackListBox* self = (TrackListBox*) g_object_new (object_type, NULL);
	GtkViewport* viewport = (GtkViewport*)self;

	// for vertical scrolling, use canvas adjustment instead
	gtk_viewport_set_hadjustment(viewport, (GtkAdjustment*)gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0));
	gtk_viewport_set_vadjustment(viewport, (GtkAdjustment*)gtk_adjustment_new(100.0, 0.00, 1.0, 0.01, 0.1, 0.1));
	gtk_viewport_set_shadow_type(viewport, GTK_SHADOW_NONE);

	// event box covering the whole track list to catch drag events.
	// TODO widget should just register for events instead
	self->ev = gtk_event_box_new();
	gtk_container_add(GTK_CONTAINER(self), self->ev);

	// vbox to hold all tracks. Inside the viewport. 
	gtk_widget_show(self->vbox = gtk_vbox_new (NON_HOMOGENOUS, 0));
	gtk_container_add(GTK_CONTAINER(self->ev), self->vbox);

	// set up widget as a dnd source (primarily for track reordering):
	gtk_drag_source_set(self->ev, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);
	gtk_drag_dest_set(self->ev, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));

	return self;
}


TrackListBox*
track_list_box_new (Arrange* arrange)
{
	TrackListBox* box = track_list_box_construct (TYPE_TRACK_LIST_BOX);
	box->arrange = arrange;

	g_signal_connect(G_OBJECT(box->ev), "motion-notify-event", G_CALLBACK(trkctl_on_motion), NULL);
	g_signal_connect(G_OBJECT(box->ev), "drag-data-get", G_CALLBACK(trkctl_drag_dataget), box);
	g_signal_connect(G_OBJECT(box->ev), "drag-data-received", G_CALLBACK(track_drag_received), arrange);
	g_signal_connect(G_OBJECT(box->ev), "drag-begin", G_CALLBACK(trkctl_drag_begin), arrange);
	g_signal_connect(G_OBJECT(box->ev), "drag-motion", G_CALLBACK(track_drag_motion), arrange);
	g_signal_connect(G_OBJECT(box->ev), "button-press-event", G_CALLBACK(stop_propogation), arrange);
	g_signal_connect(G_OBJECT(box->ev), "button-release-event", G_CALLBACK(stop_propogation), arrange);

	return box;
}


static void
track_list_box_class_init (TrackListBoxClass* klass)
{
	track_list_box_parent_class = g_type_class_peek_parent (klass);
	G_OBJECT_CLASS (klass)->finalize = track_list_box_finalize;
}


static void
track_list_box_init (TrackListBox* self)
{
	self->priv = track_list_box_get_instance_private(self);
	self->priv->image1 = NULL;
}


static void
track_list_box_finalize (GObject* obj)
{
	G_OBJECT_CLASS (track_list_box_parent_class)->finalize (obj);
}


void
track_list_box_on_dim_change (Arrange* arrange)
{
	// Redraw the arrange page track control area (lhs) for zoom changes etc.
	// -it only deals with changes in dimensions, not state changes.

	int y = 0;
	AMTrack* tr;
	ArrTrk* at = NULL;
	while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (tr = at->track)){
		// note: because relative track heights need to be preserved, we *dont* want to change the 
		//       underlying track height (track[t]->height).
		//       Only crop the zoomed size.
		int h_zoomed = MIN(MAX_TRACK_HEIGHT, at->height * vzoom(arrange));

#ifndef DEBUG_TRACKCONTROL
#ifdef USE_GNOMECANVAS
		if(at->trk_ctl) track_control_set_height(TRACK_CONTROL(at->trk_ctl), h_zoomed);
#endif
#endif

		y += h_zoomed;
	}

	gtk_widget_set_size_request(((TrackListBox*)arrange->track_list_box)->vbox, -1, y + 20/*vbox must fill all space of viewport otherwise the area wont be drawn*/); // move!
}


static void
trkctl_drag_begin (GtkWidget* widget, GdkDragContext* drag_context, gpointer _)
{
	TrackListBox* box = (TrackListBox*)widget;

	box->drag_begin_y = 0; // forces it to be set later.
}


static bool
trkctl_on_motion (GtkWidget* widget, GdkEventMotion* event, gpointer _)
{
	// warning: the event relates to the top x-window at start of drag, not @widget.

#if 0
	if(windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)){
		//arrange->mouse.x = event->x;
		//arrange->mouse.y = (int)event->y_root - arrange->canvas_scrollwin->allocation.y;
		//printf("%i(%i(%i)) ", arrange->mouse.y, (int)event->y, arrange->canvas_scrollwin->allocation.y); fflush(stdout);
		//dbg(0, "root=%.2f", event->y_root);
	}
#endif
	return HANDLED; // why?
}


static void
trkctl_drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, TrackListBox* box)
{
	// @widget should be the event box behind the whole track control area.

	int y = box->drag_begin_y;
	TrackDispNum track_num = arr_px2trk(box->arrange, y); // note that y must be in *canvas* coords.

	char text[128];
	sprintf(text, "track:%i%c%c", track_num + 1, 13, 10); // 1 based to avoid atoi problems.
	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (guchar*)text, strlen(text));
	dbg(2, "y=%i %s", y, text);
}


static void
track_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, Arrange* arrange)
{
	// Possible data types:
	//   1- a palette number from the colour window.
	//   2- a track number for track reordering.

	// @param widget is the 'drop' widget, not the source. It should be the tracklist ebox.

	// TODO change cursor

	if(data == NULL || data->length < 0){ perr ("dnd: no data!"); return; }

	dbg(0, "dnd: %s", data->data);

	switch(data->data[0]){
		case 'c':
			// need the palette number only - "colour:N"
			;int palette_num = atoi((char*)data->data + 7);
			if(!palette_num){ perr ("dnd: colour: cannot parse data! (%s)", data->data); return; }
			palette_num--; // restore zero indexing.

			TrackDispNum t = track_drag_get_dest_track(widget, arrange, y);
			if(t >= 0) am_track__set_colour(song->tracks->track[t], palette_num, NULL, NULL);
			break;
		case 't':
			if(!windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE)) return;
			int track_num = atoi((char*)data->data + 6);
			if(!track_num){ perr ("dnd: track: cannot parse data! (%s)", data->data); return; }
			track_num--;

			TrackDispNum new_track = track_drag_get_dest_track(widget, arrange, y);
			if(new_track != track_num) arr_track_move_to_pos(arrange, track_num, new_track);

			tracklist_drag_unhighlight(arrange);
			break;
		default:
			break;
	}

	return;
}


bool
track_drag_motion (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, guint time, Arrange* arrange)
{
	if(!((TrackListBox*)arrange->track_list_box)->drag_begin_y) ((TrackListBox*)arrange->track_list_box)->drag_begin_y = y; //FIXME this is unreliable as cursor may have already moved to a different track. Set it in the drag-begin or button-press.
	arrange->mouse.x = y + widget->allocation.x;
	arrange->mouse.y = y + widget->allocation.y;

	TrackDispNum dest_track = track_drag_get_dest_track(widget, arrange, y);
	if(dest_track < 0) return NOT_HANDLED;

	if(dest_track != track_drag_highlight){
		// unhighlight previously highlighted track:
		tracklist_drag_unhighlight(arrange);

		gtk_drag_unhighlight(widget); //prevent viewport being highlighted - nasty! TODO

		track_drag_highlight = dest_track;

		ArrTrk* dest = track_list__track_by_display_index(arrange->priv->tracks, dest_track);
		if(dest){
			GtkWidget* tc = dest->trk_ctl;
			if(tc){
				#ifdef USE_GNOME_CANVAS
				track_control_drag_highlight (tc);
				#endif
				return HANDLED;
			}
			else return NOT_HANDLED;
		}
		else return NOT_HANDLED;
	}
	return HANDLED;
}


static void
tracklist_drag_unhighlight (Arrange* arrange)
{
	if(track_drag_highlight >= 0){
#ifdef USE_GNOME_CANVAS
		ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, track_drag_highlight);
		if(atr){
			GtkWidget* highlighted = atr->trk_ctl;
			if(highlighted) track_control_drag_unhighlight(highlighted);

			track_drag_highlight = -1;
		}
#endif
	}
}


static TrackDispNum
track_drag_get_dest_track (GtkWidget* event_box, Arrange* arrange, int y)
{
	int viewport_y = gtk_viewport_get_vadjustment(GTK_VIEWPORT(arrange->track_list_box))->value;
	ArrTrk* top = arr_px2track(arrange, viewport_y);
	g_return_val_if_fail(top, 0);
	int viewport_y_q = top->y * vzoom(arrange); // viewport top quantised to nearest track.

	int new_track = arr_px2trk(arrange, event_box->allocation.y + viewport_y_q + y);
	//dbg(0, "y=%i vpy=%i wy=%i", y, viewport_y, widget->allocation.y);
	if(new_track >= am_track_list_count(song->tracks)) return -1;
	return new_track;
}

#endif
