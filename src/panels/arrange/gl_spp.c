/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

static AGlActor*
spp_actor (GtkWidget* panel)
{
	bool spp_actor__paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;

		double spp = arr_gl_get_spp_px(arrange);

		agl_rect(spp, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1, 1, actor->region.y2);

		// mouse cursor on rulerbar (is not done by rulerbar so as to allow caching)
		CanvasOp* op = arrange->canvas->op;
		if (op->mouse.x > -1) {
			agl_rect(op->mouse.x, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1, 1, -agl_actor__height(CGL->actors[ACTOR_TYPE_RULER]));
		}

		return true;
	}

	void spp_state (AGlActor* actor)
	{
		SET_PLAIN_COLOUR (agl->shaders.plain, 0xff00007f);
	}

	void arr_gl_spp_size (AGlActor* actor)
	{
		actor->region = (AGlfRegion){
			.x1 = 0,
			.y1 = 0,
			.x2 = ((AGlActor*)actor->root)->region.x2,
			.y2 = ((AGlActor*)actor->root)->region.y2,
		};
	}

	return agl_actor__new (AGlActor,
		.name = g_strdup("SPP"),
		.program = (AGlShader*)agl->shaders.plain,
		.set_state = spp_state,
		.set_size = arr_gl_spp_size,
		.paint = spp_actor__paint
	);
}
