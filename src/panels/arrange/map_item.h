/*
  This file is part of the Ayyi Project. http://www.ayyi.org
  copyright (C) 2004-2009 Tim Orford <tim@orford.org>

 * Distribulted under the GPL LIcense.
 * 
 * You should have received a copy of the GNU General Public
 * License; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
  @NOTATION@
 */
/* Polygon item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 *
 * Author: Federico Mena <federico@nuclecu.unam.mx>
 *         Rusty Conover <rconover@bangtail.net>
 */

#ifndef GNOME_CANVAS_MAP_H
#define GNOME_CANVAS_MAP_H


#include <libgnomecanvas/gnome-canvas.h>
#include <libgnomecanvas/gnome-canvas-shape.h>
#include <libgnomecanvas/gnome-canvas-path-def.h>

G_BEGIN_DECLS


/* Polygon item for the canvas.  A polygon is a bit different from rectangles and ellipses in that
 * points inside it will always be considered "inside", even if the fill color is not set.  If you
 * want to have a hollow polygon, use a line item instead.
 *
 * The following object arguments are available:
 *
 * name			type			read/write	description
 * ------------------------------------------------------------------------------------------
 * points		GnomeCanvasPoints*	RW		Pointer to a GnomeCanvasPoints structure.
 *								This can be created by a call to
 *								gnome_canvas_points_new() (in gnome-canvas-util.h).
 *								X coordinates are in the even indices of the
 *								points->coords array, Y coordinates are in
 *								the odd indices.
 */

#define GNOME_TYPE_CANVAS_MAP             (gnome_canvas_map_get_type ())
#define GNOME_CANVAS_MAP(obj)             (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_MAP, GnomeCanvasMap))
#define GNOME_CANVAS_MAP_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_MAP, GnomeCanvasMapClass))
#define GNOME_IS_CANVAS_MAP(obj)          (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_MAP))
#define GNOME_IS_CANVAS_MAP_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_MAP))
#define GNOME_CANVAS_MAP_GET_CLASS(obj)   (GTK_CHECK_GET_CLASS ((obj), GNOME_TYPE_CANVAS_MAP, GnomeCanvasMapClass))


//typedef struct _GnomeCanvasMap GnomeCanvasMap;
typedef struct _GnomeCanvasMapClass GnomeCanvasMapClass;

#define PART_OUTLINE_WIDTH 2.0

struct _GnomeCanvasMap
{
	GnomeCanvasShape    item;                  // parent widget
	GnomeCanvasPathDef* path_def;
	GnomeCanvasPoints*  pts;

	GdkPixbuf*          pixbuf;
	cairo_t*            cr;                    // should always be valid if Part is midi, and there is a pixbuf.
	cairo_surface_t*    surface;               // TODO remove this.
	GArray*             tiles;

	AMPart*             gpart;
	Arrange*            arrange;               // parent window.
	double              transient_offset_left;
	int                 min_height;

	gboolean            editing;
	int                 velocity_bar_width;

	gulong              box_handler_id;        // gsignal handler for mouse operations.
	gulong              peakdata_ready_handler;

	GdkPixbuf*          label_pixbuf;          // the visible part of the rendered text as used by the canvas.

	gpointer            private;
};

struct _GnomeCanvasMapClass {
	GnomeCanvasShapeClass parent_class;
	uint32_t              edit_colour;
};

void    gnome_canvas_map_set_selected(GnomeCanvasItem*, gboolean selected);
double  gnome_canvas_map_get_x       (GnomeCanvasMap*);
double  gnome_canvas_map_get_right   (GnomeCanvasMap*);


/* Standard Gtk function */
GType gnome_canvas_map_get_type (void) G_GNUC_CONST;

G_END_DECLS
#endif

//-------------------------------------------------------

//copy of gnome-canvas-shape-private.h:
#ifndef GNOME_CANVAS_SHAPE_PRIVATE_H
#define GNOME_CANVAS_SHAPE_PRIVATE_H

/* Bpath item type for GnomeCanvas widget
 *
 * GnomeCanvas is basically a port of the Tk toolkit's most excellent canvas widget.  Tk is
 * copyrighted by the Regents of the University of California, Sun Microsystems, and other parties.
 *
 * Copyright (C) 1998,1999 The Free Software Foundation
 *
 * Authors: Federico Mena <federico@nuclecu.unam.mx>
 *          Raph Levien <raph@acm.org>
 *          Lauris Kaplinski <lauris@ariman.ee>
 */

#include <gdk/gdk.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_vpath_dash.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libgnomecanvas/gnome-canvas.h>

#include <libgnomecanvas/gnome-canvas-path-def.h>

G_BEGIN_DECLS

typedef struct _GnomeCanvasShapePrivGdk GnomeCanvasShapePrivGdk;
typedef struct _GCBPDrawCtx GCBPDrawCtx;

/* Per canvas private structure, holding necessary data for rendering
 * temporary masks, which are needed for drawing multipart bpaths.
 * As canvas cannot multithread, we can be sure, that masks are used
 * serially, also one set of masks per canvas is sufficent to guarantee,
 * that masks are created on needed X server. Masks grow as needed.
 * Full structure is refcounted in Bpath implementation
 */

struct _GCBPDrawCtx {
	gint refcount;

	GnomeCanvas * canvas;

	gint width;
	gint height;

	GdkBitmap * mask;
	GdkBitmap * clip;

	GdkGC * clear_gc;
	GdkGC * xor_gc;
};

/* Per Bpath private structure, holding Gdk specific data */

struct _GnomeCanvasShapePrivGdk {
	gulong fill_pixel;		/* Color for fill */
	gulong outline_pixel;		/* Color for outline */

	GdkBitmap *fill_stipple;	/* Stipple for fill */
	GdkBitmap *outline_stipple;	/* Stipple for outline */

	GdkGC * fill_gc;		/* GC for filling */
	GdkGC * outline_gc;		/* GC for outline */

	gint len_points;		/* Size of allocated points array */
	gint num_points;		/* Gdk points in canvas coords */
	GdkPoint * points;		/* Ivariant: closed paths are before open ones */
	GSList * closed_paths;		/* List of lengths */
	GSList * open_paths;		/* List of lengths */

	GCBPDrawCtx * ctx;		/* Pointer to per-canvas drawing context */
};

struct _GnomeCanvasShapePriv {
	GnomeCanvasPathDef * path;      /* Our bezier path representation */

	gdouble scale;			/* CTM scaling (for pen) */

	guint fill_set : 1;		/* Is fill color set? */
	guint outline_set : 1;		/* Is outline color set? */
	guint width_pixels : 1;		/* Is outline width specified in pixels or units? */

	double width;			/* Width of outline, in user coords */

	guint32 fill_rgba;		/* Fill color, RGBA */
	guint32 outline_rgba;		/* Outline color, RGBA */

	GdkCapStyle cap;		/* Cap style for line */
	GdkJoinStyle join;		/* Join style for line */
	ArtWindRule wind;		/* Winding rule */
	double miterlimit;		/* Miter limit */

	ArtVpathDash dash;		/* Dashing pattern */

	ArtSVP * fill_svp;		/* The SVP for the filled shape */
	ArtSVP * outline_svp;		/* The SVP for the outline shape */

	GnomeCanvasShapePrivGdk * gdk;	/* Gdk specific things */
};

G_END_DECLS

#endif
