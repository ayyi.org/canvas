/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 */
#ifndef __arrange_h__
#define __arrange_h__

#include "agl/typedefs.h"
#include "agl/observable.h"
#include "windows.h"
#include "window.h"
#include "panels/panel.h"
#include "arrange/track.h"
#include "arrange/track_list.h"
#ifdef USE_GNOMECANVAS
#include "arrange/overview.h"
#include "arrange/gnome_canvas.h"
#endif
#include "arrange/toolbox.h"

G_BEGIN_DECLS

#define AYYI_TYPE_ARRANGE            (ayyi_arrange_get_type ())
#define AYYI_ARRANGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_ARRANGE, AyyiArrange))
#define AYYI_ARRANGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_ARRANGE, AyyiArrangeClass))
#define AYYI_IS_ARRANGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_ARRANGE))
#define AYYI_IS_ARRANGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_ARRANGE))
#define AYYI_ARRANGE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_ARRANGE, AyyiArrangeClass))

typedef struct _AyyiArrangeClass AyyiArrangeClass;
typedef struct _ArrangePriv ArrangePriv;

typedef void (*ArrangeFn) (Arrange*);

#define PX_PER_BEAT 8.0
#define PX_PER_SUBBEAT 2.0

#define MIN_TRACK_HEIGHT 8         // see also arr_track_min_height() which takes button height into account.
#define MAX_TRACK_HEIGHT 1024      // arbitrary
#define TICKS_PER_PX 192.0         // 4 * 384 / PX_PER_BEAT (384 steps per subbeat)

#define MIN_PART_WIDTH 4.0
#define PART_OFFSET_Y2 1.0         // distance from part bottom to track bottom
#define PART_BORDER    1.0         // distance from the edge of the part to the useful 'internal' space.
                                   // dont know atm if one should assume this is the same as the canvas line-width used.
#define ARR_SONG_END_MARGIN 40.0   // visible space after the end marker.
#define ARR_MAX_HZOOM 200.0        // this value is too big for the gnome canvas.
#define ARR_MAX_VZOOM 16.0         // currently limited by fixed buffer allocation in gnomecanvas overview pixbuf.
#define ARR_MIN_HZOOM 0.1
#define ARR_MIN_VZOOM 0.1
#define ARR_MIN_NOTE_DISPLAY_LENGTH 5
#define ARR_PICK_SIZE 3

#define MAX_SCROLL_SPEED 300       // max no of pixels per scroll for autoscroll operations.
#define VSCROLL_STEP_INCREMENT 2.0

#define SHOW_AUTO (arrange->view_options[SHOW_AUTOMATION].value)
#define SHOW_SONGMAP arrange->view_options[SHOW_SONG_MAP].value
#define SHOW_PARTCONTENTS arrange->view_options[SHOW_PART_CONTENTS].value
#define SHOW_OVERVIEW arrange->view_options[SHOW_SONG_OVERVIEW].value

#define MAX_CANVAS_TYPES 4

// mouse tool types:
typedef enum {
  TOOL_DEFAULT = 0,
  TOOL_PENCIL,
  TOOL_MUTE,
  TOOL_MAG,
  TOOL_SCISSORS,
  TOOL_SCROLL,
  TOOL_MAX
} ToolType;

#include "arrange/canvas_op.h"

typedef enum {
  ZOOM_FOCUS_LEFT,
  ZOOM_FOCUS_CENTRE,
  ZOOM_FOCUS_SPP,
  ZOOM_FOCUS_SELECTION,
} ZoomFocus;

enum {
  SHOW_AUTOMATION = 0,
#ifdef USE_GNOMECANVAS
  SHOW_SONG_MAP,
#endif
  SHOW_PART_CONTENTS,
  SHOW_SONG_OVERVIEW,
  MAX_VIEW_OPTIONS
};

struct _ViewOption {
   bool              value;
};

typedef struct
{
  double h;
  double v;
} ZoomSetting;

typedef struct {
   ArrTrk*          track;              // the track that was last edited for automation.
   AMTrackNum       tnum;
   AyyiAutoType     type;               // the curve that is currently or previously was being edited.
   int              subpath;            // the current subpath being editing, or 0 if none.
   bool             is_line;            // if true, the line is selected, as opposed to the control point.
} AutoSelection;

struct _Arrange
{
  AyyiPanel          panel;
  ArrangePriv*       priv; 

  ArrCanvas*         canvas;
  CanvasType         canvas_type;       // used by the config loader to set the type before the canvas object is created.

  AGlObservable*     width;             // empty event stream for scrollable width
  AGlObservable*     height;            // empty event stream for scrollable height. use arr_canvas_height() to get the derived value

  GtkWidget*         canvas_scrollwin;  // the outer widget. For canvases that provide their own srollbars, it will not actually be a scrollwindow widget.
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
  GtkWidget*         track_list_box;    // track control viewport.
#endif

  uint32_t           bg_colour;         // 0xrrggbbaa.

  ViewOption         view_options[MAX_VIEW_OPTIONS];
  gboolean           part_fades_show;   // true if part fade-in and fade-out lines are displayed.
  bool               zoom_follow;

#ifdef USE_GNOMECANVAS
  song_overview      song_overview;     // pane showing whole song.
#endif

  // menus
  GtkWidget*         partmenu;          // Part popup context menu.
  struct {
    EditMenu         canvas_type[MAX_CANVAS_TYPES]; // part of tcl context menu
  }                  menu;

  ConfigParamValue   tc_width;          // maybe make this an array

  GtkWidget**        b_transport;       // transport buttons.
  GtkWidget*         tempobox;          // GtkSpinButton.
  GtkWidget*         q_combo;

  GtkObject*         hzoom_adj;
  GtkObject*         vzoom_adj;
  GtkWidget*         hzoom_scale;
  GtkWidget*         vzoom_scale;
  float              peak_gain;

  struct {
    GtkWidget*       buttons[TOOL_MAX];
    ToolType         current;           // the currently selected tool, eg: TOOL_PENCIL.
  }                  toolbox;

  // rulerbar
  GtkWidget*         ruler;
  float              ruler_height;

  SongMap*           songmap;

  GList*             zoom_history;
  ZoomFocus          zoom_focus;

  GList*             part_selection;     // list of AMPart*. When window is linked, this list will be the same song->part_selection. 
  gboolean           part_editing;       // set when in midi edit mode.

  // automation editing
  struct {
    AutoSelection    sel;
    AutoSelection    hover;
    GtkWidget*       menu;
  }                  automation;

  Pti                mouse;              // mouse coords before a menu selection (window coords).

  struct {
#if defined(USE_GNOMECANVAS) || defined(USE_CLUTTER)
    GtkSizeGroup*   sizegroup;           // ensure track labels are all the same width.
#endif
    GList*          selection;           // list of AMTrack*.
                                         // -if we need a single track, use the last selected (last in list).
                                         // -however because of eg Record fn, it is somewhat Song-wide
                                         //  and not just related to the arrange window.   <---- no - changing this.
    GtkWidget*      menu;                // context menu for track area.
  }                 tracks;

  int               text_height;         // optimum height of Part label text in pixels (some may be smaller).

  GList*            signals;             // signals to block when the user interface is inactive.

  bool              pressed;             // used to prevent autoscrolling during mouse ops.
};

struct _AyyiArrangeClass
{
    AyyiPanelClass parent_class;

    GList*         song_storage;         // type ArrangeSongStorage*

    guint          song_unload_signal;
    guint          tool_change_signal;

    ArrangeFn      view_option_toggle[MAX_VIEW_OPTIONS];

    GtkAccelGroup* midi_accel_group;

    ToolboxButton* toolbox;

    GtkWidget*     rootmenu;             // canvas_root popup context menu.
};

struct _ArrangePriv
{
    TrackList*         tracks;

    AGliPt             canvas_size;      // size of total scrollable area - height is equivalent to the (zoomed) position of the bottom of the last track plus some empty space.
                                         // It is a derived value and is invalidated by setting to zero. Use arr_canvas_width() and arr_canvas_height() to get the correct value.

    GtkWidget*         hpaned;
    GtkWidget*         vbox_rhs;         // vbox used to hold the main canvas.
    GtkWidget*         tcl_top_space;    // hbox above the track control area.

    GtkWidget*         bsnap;
};

typedef struct _ArrangeSongStorage
{
	int height[AM_MAX_TRK];
} ArrangeSongStorage;


typedef enum {
	PROVIDES_RULERBAR  = 1 << 0,
	PROVIDES_METERS    = 1 << 1,
	PROVIDES_TRACKCTL  = 1 << 2,
	PROVIDES_OVERVIEW  = 1 << 3,
	PROVIDES_SIZEGROUP = 1 << 4,
	ASYNC_CREATION     = 1 << 5,
} CanvasProvides;


typedef struct _canvas_class
{
	const char*    name;
	NewCanvas      canvas_new;
	void           (*menu_callback)(Arrange*);
	CanvasProvides provides;
} CanvasClass;


struct _canvas
{
  CanvasType       type;
  GtkWidget*       widget;
  GtkWidget*       container;            // GtkTable - used by GlCanvas
  GtkAdjustment*   h_adj;
  GtkAdjustment*   v_adj;                // can be used to observe 2 things: the scroll position, and the total scrollable height
  int              vtab[2];
#ifdef USE_GNOMECANVAS
  g_canvas*        gnome;
#endif
  GlCanvas*        gl;
#ifdef USE_CLUTTER
  ClCanvas*        cl;
#endif

  CanvasOp*        op;

#ifdef USE_GNOMECANVAS
  void           (*redraw)              (Arrange*); // TODO remove
#endif
  void           (*free)                (Arrange*);
  void           (*px_to_model)         (Arrange*, Pti, AyyiSongPos*, ArrTrk**);       // Transform a widget pixel coordinate (e.g. as generated by an widget event) to the song position and track. Note that get_scroll_offsets() is not adequate for the transformation for some canvas types.
  void           (*get_scroll_offsets)  (Arrange*, int* cx, int* cy); // deprecated - use canvas->h_adj->value and canvas->v_adj->value instead
  uint32_t       (*get_viewport_left)   (Arrange*);
  void           (*get_view_fraction)   (Arrange*, double* fx, double* fy);
  void           (*pick)                (Arrange*, double, double, TrackDispNum* track, AMPart**); // TODO should probably be removed (why?)
  bool           (*part_new)            (Arrange*, AMPart*);
  void           (*part_delete)         (Arrange*, AMPart*);
  void           (*get_part_rect)       (Arrange*, AMPart*, DRect*);
  void           (*set_spp)             (Arrange*, uint32_t);
  void           (*while_recording)     (Arrange*);
  void           (*post_recording)      (Arrange*);
  void           (*record_part_new)     (Arrange*, int t);
  void           (*set_part_selection)  (Arrange*);
  void           (*set_note_selection)  (Arrange*, MidiNote*);
  void           (*part_editing_start)  (Arrange*, AMPart*);
  void           (*part_editing_stop)   (Arrange*, AMPart*);
  void           (*do_follow)           (Arrange*);
  void           (*scroll_to)           (Arrange*, int x, int y);
  void           (*scroll_to_pos)       (Arrange*, GPos*, TrackDispNum);
  void           (*scroll_left)         (Arrange*, int pix);
  void           (*scroll_right)        (Arrange*, int pix);
  void           (*scroll_up)           (Arrange*, int pix);
  void           (*scroll_down)         (Arrange*, int pix);
  FeatureType    (*pick_feature)        (CanvasOp*);

  void           (*on_song_load)        (Arrange*);                    // notify the canvas when panel->model_is_loaded becomes set.
  void           (*on_view_change)      (Arrange*, AMChangeType);
  void           (*on_part_change)      (Arrange*, AMPart*);
  void           (*on_new_track)        (Arrange*, ArrTrk*);
  void           (*on_track_delete)     (Arrange*, ArrTrk*);
  void           (*on_locator_change)   (Arrange*);

  void           (*on_auto_ctl_show)    (Arrange*, ArrTrk*, ArrAutoPath*);
  void           (*on_auto_ctl_hide)    (Arrange*, ArrAutoPath*);
  void           (*on_auto_pt_add)      (Arrange*, ArrAutoPath*, Curve*, int position);
  void           (*on_auto_pt_remove)   (Arrange*, ArrAutoPath*, Curve*, int position);
};


#define ARRANGE_FIRST ((Arrange*)windows__get_first(AYYI_TYPE_ARRANGE))

#define arrange_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_ARRANGE) continue; \
		Arrange* arrange = (Arrange*)panel;
#define end_arrange_foreach end_panel_foreach

#define arrange_verify(A) windows__verify_pointer((AyyiPanel*)A, AYYI_TYPE_ARRANGE)


GType            dh_html_get_type            ();
GType            ayyi_arrange_get_type       ();

void             arr_apply_song_prefs        (Arrange*);

CanvasType       arr_register_canvas         (const char* name, NewCanvas, ArrangeFn menu_callback);
CanvasType       arr_lookup_canvas_type      (const char*);
void             arr_canvas_foreach          (void (*fn)(CanvasType, CanvasClass*, gpointer), gpointer user_data);
void             arr_set_canvas              (Arrange*, CanvasType);

int              arr_canvas_height           (Arrange*);
double           arr_canvas_width            (Arrange*); // the scrollable width, not the viewport. depends on the song length.

void             arr_set_zoom_follow         (Arrange*, bool);

void             arr_on_view_changed         (Arrange*, AMChangeType);
void             arr_toolbar_update          (Arrange*);

void             arr_while_recording         (Arrange*);
void             arr_post_recording          (Arrange*);

void             arr_song_map_toggle         (Arrange*);

#define hzoom(ARR) (((AyyiPanel*)ARR)->zoom->value.pt.x)
#define vzoom(ARR) (((AyyiPanel*)ARR)->zoom->value.pt.y)

#include "arrange/utils.h"

G_END_DECLS

#endif
