/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2014-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

typedef struct
{
    AGlActor actor;
    float    level;
} Meter;


static AGlActor*
meter_new (AMChannel* channel)
{
	bool meter__paint (AGlActor* actor)
	{
		Meter* meter = (Meter*)actor;

		agl_set_colour_uniform(&agl->shaders.plain->uniforms[PLAIN_COLOUR], 0x99ff999f);
		agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));

		agl_set_colour_uniform(&agl->shaders.plain->uniforms[PLAIN_COLOUR], 0x99ff999f);
		float y = agl_actor__height(actor) * (1.0 - ((96 + meter->level) / 96.0));
		agl_rect(0, y, agl_actor__width(actor), agl_actor__height(actor) - y);

		return true;
	}

	void meter__set_size (AGlActor* actor)
	{
	}

	bool meter__on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		return NOT_HANDLED;
	}

	AGlActor* actor = (AGlActor*)agl_actor__new(Meter,
		.actor = {
			.name     = g_strdup("Meter"),
			.program  = agl->shaders.plain,
			.region   = (AGlfRegion){0, 40, 15, 160},
			.paint    = meter__paint,
			.set_size = meter__set_size,
			.on_event = meter__on_event
		}
	);

	return actor;
}


void
meter_set_channel (AGlActor* actor, AMChannel* channel)
{
}

