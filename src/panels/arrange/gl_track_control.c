/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2010-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __gl_canvas_priv__

#include "global.h"
#include "agl/utils.h"
#include "agl/text/pango.h"
#include "model/track_list.h"
#include "song.h"
#include "io.h"
#include "support.h"
#include "arrange.h"
#include "arrange/gl_canvas.h"
#include "arrange/shader.h"
#include "arrange/gl_track_control.h"
#include "arrange/part_renamer.h"
#include "arrange/gl_button.h"

extern AGlActor* gl_track (ArrTrk*);

#define TCTRACKS ((TrackControlActor*)c->actors[ACTOR_TYPE_TC])->tracks

#define BG_OPACITY 0.75

typedef void (*ButtonAction)   (AGlActor*, gpointer);
typedef bool (*ButtonGetState) (AGlActor*, gpointer);

extern AGlActor*     gl_button (int, ButtonAction, ButtonGetState, gpointer);
extern AGlShaderText meter_text;

typedef enum {
	OBJ_RESIZE = 0,
	OBJ_DRAG,
	OBJ_MAX
} Objs;

typedef struct
{
	AGlActor*    pressed;
	struct {
		int      y;
	}            orig;
	struct {
		ArrTrk*  atr;
	}            new;
} Drag;
static Drag drag;

static ViewObjectClass drag_class = {0,};
static ViewObjectClass resize_class = {
	.cursor = CURSOR_V_DOUBLE_ARROW,
};

static void track_control_set_state         (AGlActor*);
static void track_control_actor_layout      (Observable*, AMVal, gpointer);

static AGl* agl = NULL;

#include "gl_track_control_track.c"


static void
track_control_set_size (AGlActor* actor)
{
}


static void
track_control_set_state (AGlActor* actor)
{
}


static void
track_control_set_state_gl1 (AGlActor* actor)
{
}


static bool
track_control_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	switch (event->type){
		case GDK_2BUTTON_PRESS:
			dbg(1, "double click");
			am_song_add_track(0, NULL, 0, NULL, NULL);
			return HANDLED;
		default:
			break;
	}
	return NOT_HANDLED;
}


AGlActor*
track_control_actor_new (GtkWidget* panel)
{
	Arrange* arrange = (Arrange*)panel;

	/*
	 *  Fbo caching:
	 *
	 *  Each track is cached separately to an fbo.
	 *
	 *  The track controls are not cached with the rest of the track as there are many cases
	 *  where the controls need to be redrawn but the (potentially complex) track itself is unchanged.
	 *
	 */
	agl = agl_get_instance();

	bool track_control_paint (AGlActor* actor)
	{
#if 0
		Arrange* arrange = actor->root->user_data;

		GdkColor default_bg;
		GdkColor selected_bg;
		widget_get_bg_colour((GtkWidget*)arrange, &default_bg, GTK_STATE_NORMAL);
		widget_get_bg_colour((GtkWidget*)arrange, &selected_bg, GTK_STATE_SELECTED);
		((TrackControlActor*)actor)->default_bg = color_gdk_to_rgba(&default_bg);
		((TrackControlActor*)actor)->selected_bg = color_gdk_to_rgba(&selected_bg);
#endif
		char font_string[64];

		get_font_string(font_string, -3);
		agl_set_font_string(font_string);

		return true;
	}

#if 0
	bool track_control_track_on_event(Actor* actor, Arrange* arrange, GdkEvent* event, Pti xy)
	{
		return actor__on_viewobject_event(actor, arrange, event, xy);
	}
#endif

	void on_tracks_change (GObject* _song, TrackNum t_num1, TrackNum t_num2, AMChangeType change_type, Arrange* arrange)
	{
		GlCanvas* c = arrange->canvas->gl;

		if ((change_type & AM_CHANGE_NAME)) {
			gtk_widget_queue_draw(CGL->image);
		}

		if ((change_type & AM_CHANGE_HEIGHT)) {
			for (TrackNum t=t_num1;t<=t_num2;t++) {
				ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
				if (atr) {
					TrackControlTrackActor* tcta = g_hash_table_lookup(TCTRACKS, atr->track);
					((AGlActor*)tcta)->set_size(((AGlActor*)tcta));
				}
			}
		}

		if ((change_type & (AM_CHANGE_MUTE | AM_CHANGE_SOLO | AM_CHANGE_ARMED | AM_CHANGE_NAME | AM_CHANGE_OUTPUT | AM_CHANGE_COLOUR))) {
			for (TrackNum t=t_num1;t<=t_num2;t++) {
				ArrTrk* atr = track_list__track_by_index(arrange->priv->tracks, t);
				if (atr && atr->track->visible) {
					TrackControlTrackActor* tcta = g_hash_table_lookup(TCTRACKS, atr->track);
					for (GList* l=((AGlActor*)tcta)->children;l;l=l->next) {
						agl_actor__invalidate((AGlActor*)l->data);
					}
					if (change_type & AM_CHANGE_NAME || change_type & AM_CHANGE_OUTPUT) {
						gl_track_control_track_set_text(tcta);
					}
				}
			}
		}
	}

	void gl_tracks_on_change_by_list (GObject* _song, GList* tracks, AMChangeType change_type, AyyiPanel* originator, Arrange* arrange)
	{
		PF;
		TrackList* tl = arrange->priv->tracks;
		GlCanvas* c = arrange->canvas->gl;

		void rebuild_track_list (Arrange* arrange, AGlActor* actor)
		{
			// if tracks have been re-ordered the actor child list has to be rebuilt to preserve order by display

			TrackControlTrackActor* find_ta (AGlActor* actor, ArrTrk* atr)
			{
				for(GList* l=actor->children;l;l=l->next){
					TrackControlTrackActor* a = l->data;
					if(a->track == atr) return a;
				}
				return NULL;
			}

			GList* l = NULL;
			TrackDispNum d; for (d=0; d<AM_MAX_TRK && tl->display[d] && tl->display[d]->track->type; d++){
				ArrTrk* atr = tl->display[d];
				if(d == 0 && !atr->track->visible) continue; // why master in tl list?
				TrackControlTrackActor* ta = find_ta(actor, atr);
				l = g_list_append(l, ta);
			}
			g_list_free(actor->children);
			actor->children = l;
		}

		for(GList* l=tracks;l;l=l->next){
			AMTrack* track = l->data;

			if((change_type & AM_CHANGE_TRACK)){ // track reordering
				ArrTrk* atr = track_list__lookup_track(tl, track);
				TrackControlTrackActor* tcta = g_hash_table_lookup(TCTRACKS, atr->track);

				((AGlActor*)tcta)->region.y1 = atr->y * CGL->zoom->value.pt.y;
				((AGlActor*)tcta)->region.y2 = (atr->y + atr->height) * CGL->zoom->value.pt.y;

				rebuild_track_list(arrange, ((AGlActor*)tcta)->parent);
			}
		}
		gtk_widget_queue_draw(CGL->image);
	}

	void _on_track_add (GObject* _song, AMTrack* tr, Arrange* arrange)
	{
		GlCanvas* c = arrange->canvas->gl;

		ArrTrk* at = track_list__lookup_track(arrange->priv->tracks, tr);
		g_return_if_fail(at);

		AGlActor* actor = track_control_track_actor_new(arrange, at);
		g_hash_table_insert(TCTRACKS, tr, actor);
		agl_actor__add_child(CGL->actors[ACTOR_TYPE_TC], actor);
	}

	void _on_track_delete (GObject* _tracks, AMTrack* track, Arrange* arrange)
	{
		PF;

		AGlActor* find_track_actor (Arrange* arrange, ArrTrk* at)
		{
			AGlActor* tc = CGL->actors[ACTOR_TYPE_TC];
			for(GList* l=tc->children;l;l=l->next){
				TrackControlTrackActor* ta = (TrackControlTrackActor*)l->data;
				if(ta->track == at) return (AGlActor*)ta;
			}
			pwarn("not found");
			return NULL;
		}

		ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, track);
		g_return_if_fail(atr);
		AGlActor* child = find_track_actor(arrange, atr);
		g_return_if_fail(child);
		agl_actor__remove_child(CGL->actors[ACTOR_TYPE_TC], child);
		dbg(1, "actor removed ok");

		track_control_actor_layout(CGL->zoom, (AMVal){0,}, arrange);
	}

	void gl_track_control_on_selection_change (Observable* _, AMVal val, gpointer _arrange)
	{
		Arrange* arrange = _arrange;
		AGlActor* tc = CGL->actors[ACTOR_TYPE_TC];

#ifdef AGL_ACTOR_RENDER_CACHE
		GList* l = tc->children;
		for(;l;l=l->next){
			((AGlActor*)l->data)->cache.valid = false;
		}
#endif
		gtk_widget_queue_draw(tc->root->gl.gdk.widget);
	}

	void track_control_free (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		TrackControlActor* tca = (TrackControlActor*)actor;

		int n, i = 0;
		#define song_handler_disconnect(FN) if((n = g_signal_handlers_disconnect_matched (song, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange)) != 1){ pwarn("song handler disconnection (%i) n=%i", i, n); } i++;
		song_handler_disconnect(on_tracks_change);
		song_handler_disconnect(gl_tracks_on_change_by_list);
		#undef song_handler_disconnect

		#define tracks_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (am_tracks, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, arrange) != 1) pwarn("track handler disconnection");
		tracks_handler_disconnect(_on_track_add);
		tracks_handler_disconnect(_on_track_delete);
		#undef tracks_handler_disconnect

		g_hash_table_remove_all(tca->tracks); // ensure any free functions are run
		g_hash_table_destroy0(tca->tracks);

		g_free(actor);
	}


	void track_control_init (AGlActor* actor)
	{
		TrackControlActor* tca = (TrackControlActor*)actor;
		Arrange* arrange = ((AGlActor*)tca)->root->user_data;

		tca->default_bg = get_style_bg_color_rgba(GTK_STATE_NORMAL);
		((TrackControlActor*)actor)->selected_bg = get_style_bg_color_rgba(GTK_STATE_SELECTED);

		if(((AyyiPanel*)arrange)->model_is_loaded && !g_hash_table_size(tca->tracks)){
												dbg(0, "B");
			// iterate over ArrTrk* so that the gl tracks will not be created until after the Panel level objects are done.
			AMTrack* tr;
			ArrTrk* at = NULL;
			while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (tr = at->track)){
				AGlActor* track = gl_track(at);
				agl_actor__add_child(CGL->actors[ACTOR_TYPE_TRACKS], track);
			}
		}

		observable_subscribe_with_state(am_tracks->selection2, gl_track_control_on_selection_change, arrange); // unsubscribed in arr_destroy
	}

	static AGlActorClass tc_class = {0, "TrackControl", (AGlActorNew*)track_control_actor_new};
	tc_class.free = track_control_free;

	TrackControlActor* a = AGL_NEW(TrackControlActor,
		.actor = {
			.class     = &tc_class,
			.init      = track_control_init,
			.set_size  = track_control_set_size,
			.set_state = agl->use_shaders ? track_control_set_state : track_control_set_state_gl1,
			.paint     = track_control_paint,
			.on_event  = track_control_on_event,
			.region    = (AGlfRegion){0, arrange->ruler_height, arrange->tc_width.val.i, 2048}
		},
		.tracks = g_hash_table_new_full(NULL, NULL, NULL, NULL),
	);
	CGL->actors[ACTOR_TYPE_TC] = (AGlActor*)a; // must be set immediately, before usage

	if(((AyyiPanel*)arrange)->model_is_loaded){
		AMTrack* track = NULL;
		ArrTrk* at = NULL;
		while((at = track_list__next_visible(arrange->priv->tracks, at ? at->track : NULL)) && (track = at->track)){
			AGlActor* tcta = track_control_track_actor_new(arrange, at);
			g_hash_table_insert(a->tracks, at->track, tcta);
			agl_actor__add_child((AGlActor*)a, tcta);
		}
		gtk_widget_queue_draw(CGL->image);
	}

	am_song__connect("tracks-change", G_CALLBACK(on_tracks_change), arrange);
	am_song__connect("tracks-change-by-list", G_CALLBACK(gl_tracks_on_change_by_list), arrange);
	g_signal_connect(song->tracks, "add", G_CALLBACK(_on_track_add), arrange);
	g_signal_connect(song->tracks, "delete", G_CALLBACK(_on_track_delete), arrange);

	observable_subscribe_with_state(CGL->zoom, track_control_actor_layout, arrange);

	return (AGlActor*)a;
}


static void
track_control_actor_layout (Observable* observable, AMVal val, gpointer _arrange)
{
	Arrange* arrange = _arrange;

	AGlActor* tc = CGL->actors[ACTOR_TYPE_TC];
	if(tc){
		agl_actor__set_size(tc);

		float y = 0;
		for(GList* l=tc->children;l;l=l->next){
			TrackControlTrackActor* ta = (TrackControlTrackActor*)l->data;

			int h = ta->track->height * CGL->zoom->value.pt.y;
			// note ta->track->y is not used here in order to handle transient sizes during track resize.
			((AGlActor*)ta)->region.y1 = y; // note no scroll offset. scrolling done by container.
			((AGlActor*)ta)->region.y2 = ((AGlActor*)ta)->region.y1 + h;

			y += h;
		}
	}
}

