/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "agl/utils.h"
#include "gtkglext-1.0/gtk/gtkgl.h"
#include "glpanel.h"

extern GdkGLConfig* app_get_glconfig ();

#define DIRECT TRUE

enum  {
	gl_panel_0_PROPERTY
};

static GObject* gl_panel_construct (GType type, guint, GObjectConstructParam*);
static void     gl_panel_destroy   (GtkObject*);
static void     gl_panel_realize   (GtkWidget*);
static void     gl_panel_unrealize (GtkWidget*);
static void     gl_panel_on_resize (GtkWidget*, GtkAllocation*);

G_DEFINE_TYPE (GlPanel, gl_panel, AYYI_TYPE_PANEL)


static void
gl_panel_class_init (GlPanelClass* klass)
{
	gl_panel_parent_class = g_type_class_peek_parent (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	g_object_class->constructor = gl_panel_construct;
	((GtkObjectClass *) klass)->destroy = gl_panel_destroy;
	((GtkWidgetClass *) klass)->realize = (void (*) (GtkWidget *)) gl_panel_realize;
	((GtkWidgetClass *) klass)->unrealize = (void (*) (GtkWidget *)) gl_panel_unrealize;
	((GtkWidgetClass *) klass)->size_allocate = gl_panel_on_resize;
}


static GObject*
gl_panel_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	GObject* object = G_OBJECT_CLASS(gl_panel_parent_class)->constructor(type, n_construct_properties, construct_param);
	AyyiPanel* panel = (AyyiPanel*)object;
	GlPanel* glpanel = (GlPanel*)object;

	glpanel->area = gtk_drawing_area_new();
	gtk_widget_set_can_focus(glpanel->area, true);
	gtk_widget_add_events (glpanel->area, GDK_EXPOSURE_MASK | GDK_FOCUS_CHANGE_MASK | GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);
	gtk_box_pack_start (GTK_BOX(panel->vbox), glpanel->area, FALSE, TRUE, 0);

	void gl_panel_on_area_resize (GtkWidget* widget, GtkAllocation* allocation, gpointer _glpanel)
	{
		GlPanel* panel = (GlPanel*)_glpanel;

		((AGlActor*)panel->scene)->region = (AGlfRegion){0, 0, allocation->width, allocation->height};
		((AGlActor*)panel->scene)->scrollable = (AGliRegion){0, 0, allocation->width, allocation->height};
	}
	g_signal_connect(G_OBJECT(glpanel->area), "size-allocate", G_CALLBACK(gl_panel_on_area_resize), glpanel);

	return object;
}


static void
gl_panel_destroy (GtkObject* object)
{
	GlPanel* glpanel = (GlPanel*)object;

	if (glpanel->scene) agl_actor__free0(glpanel->scene);

	GTK_OBJECT_CLASS (gl_panel_parent_class)->destroy (object);
}


static void
gl_panel_realize (GtkWidget* widget)
{
	GlPanel* panel = (GlPanel*)widget;
	gtk_widget_set_gl_capability(panel->area, app_get_glconfig(), agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);

	panel->scene = (AGlScene*)agl_new_scene_gtk(panel->area);

	g_signal_connect(G_OBJECT(panel->area), "expose-event", (void*)agl_actor__on_expose, panel->scene);

	GTK_WIDGET_CLASS(gl_panel_parent_class)->realize (widget);
}


static void
gl_panel_unrealize (GtkWidget* widget)
{
	GTK_WIDGET_CLASS(gl_panel_parent_class)->unrealize (widget);
}


static void
gl_panel_init (GlPanel* self)
{
}


static void
gl_panel_on_resize (GtkWidget* widget, GtkAllocation* allocation)
{
	GlPanel* panel = (GlPanel*)widget;

	if(widget->allocation.width != allocation->width || widget->allocation.height != allocation->height){
		agl_actor__set_size((AGlActor*)panel->scene);
	}

	GTK_WIDGET_CLASS(gl_panel_parent_class)->size_allocate (widget, allocation);
}
