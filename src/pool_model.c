/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */
/*

  20120730:
  * added extra keying column containing the AyyiId to handle local Parts that dont have a shm idx.
    -the original key based on shm idx has not been removed due to (unverified) performance concerns.

*/
#define __pool_model_c__
#include "global.h"
#include <model/time.h>
#include <model/song.h>
#include "window.h"
#include "window.statusbar.h"
#include "windows.h"
#include "part_manager.h"
#include "song.h"
#include "panels/pool.h" //TODO
#include "pool_model.h"

struct _foreach
{
	AyyiIdx          region_idx;
	char             path[64];
};

struct _id_foreach
{
	uint64_t         region_id;
	char             path[64];
};

static GtkTreeModel* pool_create_model               ();
static bool          pool_model_add_item             (AMPoolItem*);
static void          pool_model_update_old           ();
static void          pool_model_update               ();
static bool          pool_model_get_iter_by_poolitem (GtkTreeIter*, AMPoolItem*);
static bool          pool_model_lookup_by_region_id  (uint64_t, char* path_str);
static bool          pool_model_lookup_by_region_idx (AyyiIdx region_idx, char* path_str);
static void          pool_model_regions_add          (AMPoolItem*, GtkTreeIter*);
static void          pool_model_add_region           (AyyiRegion*, GtkTreeIter*);
static void          pool_tree_save_expanded_state   ();
static void          pool_tree_restore_expanded_state();
static gboolean      pool_model_remove_item          (AMPoolItem*);
static gboolean      pool_model_item_del             (AMPoolItem*);
static void         _pool_model_update_item          (AMPoolItem*);
static void          pool_model_update_region        (AyyiRegion*);
static void          pool_model_on_change_old        ();
static gboolean      pool_model_row_is_id            (GtkTreeModel*, GtkTreePath*, GtkTreeIter*, struct _id_foreach** row);
static gboolean      pool_model_row_is_index         (GtkTreeModel*, GtkTreePath*, GtkTreeIter*, struct _foreach** row_num);

// signal handlers
static void         _pool_model_on_parts_change      (GObject*, gpointer);
static void         _pool_model_on_change_old        (GObject*, gpointer);
static void          pool_model_on_delete            (GObject*, AMPoolItem*, gpointer);
static void          pool_model_on_item_change       (GObject*, AMPoolItem*, gpointer);
static void          pool_model_on_part_add          (GObject*, AMPart*, gpointer);
static void          pool_model_on_region_delete     (GObject*, AyyiIdx, gpointer);
static void          pool_model_on_part_delete       (GObject*, AMPart*, gpointer);
static void          pool_model_on_part_change       (GObject*, AMPart*, AMChangeType, AyyiPanel*, gpointer);
static void          pool_model_on_song_unload       (GObject*, gpointer);


void
pool_init ()
{
	if(!song->pool){ perr("am_pool must be initialised first!"); return; }

	((AMCollection*)song->pool)->user_data = AYYI_NEW(UISongPool,
		.treestore = GTK_TREE_STORE(pool_create_model())
	);

	void pool_model_on_add (GObject* _pool, AMPoolItem* item, gpointer user_data)
	{
		g_return_if_fail(item);
		pool_model_add_item(item);

		window_foreach {
			shell__statusbar_print(window, 1, "File added");
		} end_window_foreach
	}

	g_signal_connect(song->pool, "change", G_CALLBACK(_pool_model_on_change_old), NULL);
	g_signal_connect(song->pool, "add", G_CALLBACK(pool_model_on_add), NULL);
	g_signal_connect(song->pool, "delete", G_CALLBACK(pool_model_on_delete), NULL);

	am_song__connect("pool-item-change", G_CALLBACK(pool_model_on_item_change), NULL);
	am_song__connect("region-delete", G_CALLBACK(pool_model_on_region_delete), NULL); // region-delete is only emitted for non-Part regions.
	am_song__connect("song-unload", G_CALLBACK(pool_model_on_song_unload), NULL);

	g_signal_connect(song->parts, "add", G_CALLBACK(pool_model_on_part_add), NULL);
	g_signal_connect(song->parts, "delete", G_CALLBACK(pool_model_on_part_delete), NULL);
	g_signal_connect(song->parts, "item-changed", G_CALLBACK(pool_model_on_part_change), NULL);
	g_signal_connect(song->parts, "change", G_CALLBACK(_pool_model_on_parts_change), NULL);
}


void
pool_free ()
{
	// only done on quit

	if(app->state == APP_STATE_ABORT) return;

	if(song->pool){
		if(gui_pool){
			g_object_unref0(gui_pool->treestore);
			g_clear_pointer(&((AMCollection*)song->pool)->user_data, g_free);
		}
	}
}


static GtkTreeModel*
pool_create_model ()
{
	// create a tree data store to hold info on files and regions.
	// -this treemodel is per-song, not per-window.

	GtkTreeStore* treestore = gtk_tree_store_new(NUM_COLS, G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_INT, G_TYPE_UINT64);

	return GTK_TREE_MODEL(treestore);
}


static void
_pool_model_on_parts_change (GObject* object, gpointer user_data)
{
	PF;
#if 1
	g_return_if_reached(); //looks like signal is never emitted.
#else
	pool_model_update();
#endif
}


static void
_pool_model_on_change_old (GObject* object, gpointer user_data)
{
	PF;
	pool_model_on_change_old();
}


static void
pool_model_on_change_old ()
{
	static guint update_id = 0;
	if(update_id) return;

	gboolean
	pool_model_update_idle()
	{
		pool_model_update_old();

		update_id = 0; //show update queue has been cleared.
		return gui_pool->update_pending = false;  //dont run again.
	}

	update_id = g_idle_add((GSourceFunc)pool_model_update_idle, NULL);

	gui_pool->update_pending = true;
}


static void
pool_model_on_delete (GObject* object, AMPoolItem* item, gpointer user_data)
{
	// An item has been removed from the model. Remove it from the gui also.

	PF;
	g_return_if_fail(item);

	pool_model_remove_item(item);
}


static void
_pool_model_update_item (AMPoolItem* item)
{
	// Update regions. For now we just delete the old region tree items and remake them from scratch.

	GtkTreeStore* treestore = gui_pool->treestore;
	GtkTreePath* treepath = gtk_tree_row_reference_get_path(item->gui_ref);
	GtkTreeIter iter;
	gtk_tree_model_get_iter(GTK_TREE_MODEL(treestore), &iter, treepath);

	gtk_tree_store_set(treestore, &iter, COL_AYYI_IDX, item->pod_index[0], COL_AYYI_ID, item->source_id[0], -1); //TODO other columns may also have changed?

	// redraw the file overview:
	// TODO treeview should connect to row-changed instead
	// TODO move the pixbuf out of the model. it is different for each view.
	pool_foreach {
		pool_win_pixbuf_cell_update(pool_window, &iter, item, -1);
		break;
	} end_pool_foreach;

	// delete old regions:
	int region_count = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(treestore), &iter);
	if(region_count){
		GtkTreeIter region_iter;
		gtk_tree_model_iter_children(GTK_TREE_MODEL(treestore), &region_iter, &iter); //get first child region.
		while(gtk_tree_store_remove(treestore, &region_iter)){
			//above fn reduces refcount of pixbuf by one.
			//TODO check ref count is 1 here.
		}
	}
	else dbg (2, "pool item has no existing regions: %s.", item->filename); //this is no longer considered to be an error.

	pool_model_regions_add(item, &iter);

	gtk_tree_path_free(treepath);
}


void
pool_model_update_now ()
{
	pool_model_update();
}


static void
pool_model_update_old ()
{
	//forces gui pool data to be in sync with core pool data.
	//-this is a "catch-all" fn, and is not supposed to be fast.

	//-currently referencing files by their filesystem name.

	//FIXME regions need to be updated as well as files (partly done).

	if(SONG_LOADED) pwarn("use of this fn when the song is loaded is deprecated.");
	PF;

	pool_tree_save_expanded_state();

	for(int i=0;i<g_list_length(song->pool->list);i++){
		AMPoolItem* item = g_list_nth_data(song->pool->list, i);
		g_return_if_fail(item);

		dbg (2, "%i: file=%s rowref=%p", i, item->filename, item->gui_ref);

		int found = pool_item_in_core(item);

		if(!item->gui_ref){
			dbg(0, "%i: item not in gui model.", i);
			pool_model_add_item(item);
			continue;
		}

		if(found){
			_pool_model_update_item(item);

		}else{ 
			if(item->state != AM_POOL_ITEM_PENDING){
				dbg (0, "file=%s: not found in core. deleting... FIXME very deprecated. We dont want to be here.", item->filename);
				if(pool_model_item_del(item)){ //the gui entry is not in the core. Delete it.
					i--;                         //list indexing has changed following deletion from the list.
				}
			}
		}
	}

	//add missing files to the gui:
	dbg (2, "adding files to gui...");

	//make new pool_items for filesources that are not already in the pool.
	AyyiFilesource* filesource = NULL;
	while((filesource = ayyi_song__filesource_next(filesource))){
		if(filesource && !am_pool__get_item_by_id(filesource->id)){
			if(SONG_LOADED){
				perr("found file not in song->pool !! "); //shouldnt happen - model is supposed to maintain the pool.
			}
			am_pool__add_ayyi_file(filesource);
		}
	}

	dbg (1, "new pool size: %i", g_list_length(song->pool->list));
	char msg[256];
	snprintf(msg, 255, "%i files", g_list_length(song->pool->list));
	pool_win_statusbar_print_all(1, msg);

	pool_tree_restore_expanded_state();

	dbg(2, "*** TODO selection indication not done? check...");
}


/*
 *  Forces gui pool data to be in sync with core pool data.
 *  This is a "catch-all" fn, and is not supposed to be fast.
 *
 *  It is assumed that the gui pool does not contain stale items
 *  Any no-longer-current items must be previously cleared.
 *  Similarly, items newly added to am_pool must be signalled separately.
 */
static void
pool_model_update ()
{
	//-currently referencing files by their filesystem name.

	//FIXME regions need to be updated as well as files (partly done).

	pool_tree_save_expanded_state();

	GList* l = song->pool->list;
	for(;l;l=l->next){
		AMPoolItem* item = l->data;
		dbg (2, "  file=%s rowref=%p", item->filename, item->gui_ref);

		if(!item->gui_ref){
			dbg(2, " item not in gui model.");
			pool_model_add_item(item);
			continue;
		}

		_pool_model_update_item(item);
	}

	dbg (1, "new pool size: %i", g_list_length(song->pool->list));
	char msg[256];
	snprintf(msg, 255, "%i files", g_list_length(song->pool->list));
	pool_win_statusbar_print_all(1, msg);

	pool_tree_restore_expanded_state();

	dbg(2, "*** TODO selection indication not done? check...");
}


void
pool_model_update_item (AMPoolItem* pool_item)
{
	_pool_model_update_item(pool_item);
}


static void
pool_model_update_region (AyyiRegion* region)
{
	// The properties of the region have changed.
	// -the region must already exist in the pool.

	if (gui_pool->update_pending) return; //this fn wont work if the pool is not up to date.

	char path_str[128];
	if (pool_model_lookup_by_region_id(region->id, path_str)) {
		GtkTreeIter iter;
		if (gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL(gui_pool->treestore), &iter, path_str)) {
			AMPart* part = (AMPart*)am_collection_find_by_id (am_parts, region->id);
			g_return_if_fail(part);
			pool_foreach {
				pool_win_pixbuf_cell_update (pool_window, &iter, part->pool_item, part->bg_colour);
				pool_win_statusbar_update   (pool_window, &iter, part, -1);
			} end_pool_foreach
		}
	}
	else pwarn ("not found. region_idx=%i", region->shm_idx);
}


void
pool_model_clear ()
{
	if(!song->pool->list) return;

	while((song->pool->list)){ //careful; list gets modified inside the loop
		pool_model_item_del(song->pool->list->data);
	}
}


static gboolean
pool_model_remove_item (AMPoolItem* item)
{
	// Remove from model
	GtkTreeIter iter;
	GtkTreeStore* treestore = gui_pool->treestore;

	g_return_val_if_fail(item->gui_ref, false);

	GtkTreePath* treepath = gtk_tree_row_reference_get_path(item->gui_ref);
	g_return_val_if_fail(treepath, false);

	dbg(4, "accessing iter...");
	if(!gtk_tree_model_get_iter(GTK_TREE_MODEL(treestore), &iter, treepath)){
		perr ("failed to get iter.");
		return false; /* path describes a non-existing row - should not happen */
	}
	gtk_tree_path_free(treepath);

	if(pool_model_iter_is_top_level(&iter)){
		// note: tree children get removed automatically.
		GtkTreeIter child;
		if(gtk_tree_model_iter_children(GTK_TREE_MODEL(treestore), &child, &iter)){
			dbg(0, "child found");
		}
	}

	gtk_tree_store_remove(treestore, &iter);

	item->gui_ref = NULL;

	return true;
}


static gboolean
pool_model_item_del (AMPoolItem* item)
{
	// Removes a pool item from the client. Removes it from the pool list and also free's the item.

	// warning! this removes am objects directly, which is not the normal course of events.

	dbg (0, "removing item from gui pool: %s", item->filename);

	if(!pool_model_remove_item(item)) return false;

	dbg(3, "freeing pool_item...");
	g_object_unref(item);
	am_pool__remove(item);
	return true;
}


static gboolean
pool_model_row_is_id (GtkTreeModel* model, GtkTreePath* path, GtkTreeIter* iter, struct _id_foreach** row)
{
	// we are looking for the row with a particular region_id.
	// @return: TRUE if the given path/iter contain the region_id given in row->region_id.
	// the path and iter seem to become invalid, so we return a path string in row->path.

	g_return_val_if_fail(row, false);
	g_return_val_if_fail(model, false);
	g_return_val_if_fail(row[0], false);
	uint64_t row_region_id = 0;

	if(pool_model_iter_is_top_level(iter)) return false; //is a file, not a region.

	gtk_tree_model_get(model, iter, COL_AYYI_ID, &row_region_id, -1);
	dbg(3, "model_get: '%s' row_region_id=%Lu", gtk_tree_path_to_string(path), row_region_id);
	if(row_region_id == row[0]->region_id){
		dbg(2, "  found! row_region_id=%Lu path='%s'", row_region_id, gtk_tree_path_to_string(path));
		char* path_str = gtk_tree_path_to_string(path);
		strncpy(row[0]->path, path_str, 63);
		g_free(path_str);
		return true;
	}
	else return false;
}


static gboolean
pool_model_row_is_index (GtkTreeModel* model, GtkTreePath* path, GtkTreeIter* iter, struct _foreach** row)
{
	// we are looking for the row with a particular region_idx.
	// @return: TRUE if the given path/iter contain the region_idx given in row->region_idx.
	// the path and iter seem to become invalid, so we return a path string in row->path.

	g_return_val_if_fail(row, false);
	g_return_val_if_fail(model, false);
	g_return_val_if_fail(row[0], false);
	int row_region_idx = -1;

	if(pool_model_iter_is_top_level(iter)) return false; // is a file, not a region.

	gtk_tree_model_get(model, iter, COL_AYYI_IDX, &row_region_idx, -1);
	dbg(3, "model_get: '%s' row_region_idx=%u", gtk_tree_path_to_string(path), row_region_idx);
	if(row_region_idx == row[0]->region_idx){
		dbg(2, "  found! row_region_idx=%u path='%s'", row_region_idx, gtk_tree_path_to_string(path));
		char* path_str = gtk_tree_path_to_string(path);
		strncpy(row[0]->path, path_str, 63);
		g_free(path_str);
		return true;
	}
	else return false;
}


gboolean
pool_model_iter_is_top_level (GtkTreeIter* iter)
{
	gboolean ret = false;

	GtkTreePath* path = NULL;
	if((path = gtk_tree_model_get_path(GTK_TREE_MODEL(gui_pool->treestore), iter))){
		if(gtk_tree_path_get_depth(path) == 1)
			ret = true;
		gtk_tree_path_free(path);
	}
	return ret;
}


gboolean
pool_model_path_is_top_level (GtkTreePath* path)
{
	return (gtk_tree_path_get_depth(path) == 1);
}


static bool
pool_model_lookup_by_region_id (uint64_t region_id, char* path_str)
{
	struct _id_foreach r = {region_id, ""};
	struct _id_foreach* row = &r;

	gtk_tree_model_foreach(GTK_TREE_MODEL(gui_pool->treestore), (GtkTreeModelForeachFunc)(pool_model_row_is_id), &row);
	if(strlen((&row)[0]->path)){
		dbg (2, "  found! path='%s'", (&row)[0]->path);
		g_strlcpy(path_str, (&row)[0]->path, 63);
		return true;
	}

	dbg (2, "region id not found in pool modeli: %Lu", region_id);
	return false;
}


static bool
pool_model_lookup_by_region_idx (AyyiIdx region_idx, char* path_str)
{
	struct _foreach r = {region_idx, ""};
	struct _foreach* row = &r;

	gtk_tree_model_foreach(GTK_TREE_MODEL(gui_pool->treestore), (GtkTreeModelForeachFunc)(pool_model_row_is_index), &row);
	if(strlen((&row)[0]->path)){
		dbg (2, "  found! path='%s'", (&row)[0]->path);
		g_strlcpy(path_str, (&row)[0]->path, 63);
		return true;
	}

	dbg (2, "region idx not found in pool model. %i", region_idx);

	return false;
}


static void
pool_model_add_region (AyyiRegion* region, GtkTreeIter* parent_iter)
{
	dbg(2, "idx=%i", region->shm_idx);

	// check the region
	AMPart* part = am_song__get_part_by_region_index(region->shm_idx, AYYI_AUDIO);
	if(part && (part->status == PART_BAD)){ pwarn ("skipping bad part..."); return; }

	char name[64];
	snprintf(name, 64, "%s", region->name);

	GtkTreeStore* treestore = gui_pool->treestore;
	GtkTreeIter child;
	gtk_tree_store_append(treestore, &child, parent_iter);
	gtk_tree_store_set(treestore, &child,
	                   COL_NAME, name,
	                   COL_AYYI_IDX, region->shm_idx,
	                   COL_AYYI_ID, region->id,
	                   -1);
}


static void
pool_tree_save_expanded_state()
{
	// Iterate over the pool treeviews checking the Expanded setting.

	GtkTreeModel* treemodel = GTK_TREE_MODEL(gui_pool->treestore);
	GtkTreeIter iter;
	pool_foreach {
		if(gtk_tree_model_get_iter_first(treemodel, &iter)){

			GtkTreePath* path;
			pool_window->expanded_list = NULL;
			do{
				if((path = gtk_tree_model_get_path(treemodel, &iter))){
					gboolean expanded = gtk_tree_view_row_expanded(GTK_TREE_VIEW(pool_window->treeview), path);
					if(expanded){
						GtkTreeRowReference* ref = gtk_tree_row_reference_new(treemodel, path);
						pool_window->expanded_list = g_list_append(pool_window->expanded_list, ref);
					}
					gtk_tree_path_free(path);
				}
			} while(gtk_tree_model_iter_next(treemodel, &iter));
		}
	} end_pool_foreach;
}


static void
pool_tree_restore_expanded_state()
{
	// Restore tree expanded state

	GtkTreePath* path;
	pool_foreach {

		GList* refs = pool_window->expanded_list;
		if(!refs) return;
		for(;refs;refs=refs->next){
			GtkTreeRowReference* ref = refs->data;
			if(!gtk_tree_row_reference_valid(ref)){ pwarn ("invalid row ref!"); continue; }
			if((path = gtk_tree_row_reference_get_path(ref))){
				gtk_tree_view_expand_row(GTK_TREE_VIEW(pool_window->treeview), path, FALSE);
				gtk_tree_path_free(path);
			}
			gtk_tree_row_reference_free(ref);
		}
		g_list_free(pool_window->expanded_list);
		pool_window->expanded_list = NULL;
	} end_pool_foreach;
}


/*
 *  Add treestore items for each region of the given pool_item.
 *  The pixbuf is created later.
 *
 *  Note that we store the region index and not the AMPart*, as not all regions have Parts, eg, the part can be deleted.
 */
static void
pool_model_regions_add (AMPoolItem* pool_item, GtkTreeIter* iter)
{
	// get a list of regions for this file
	GList* regions = pool_item_get_regions(pool_item);
	dbg (2, "%s: region count: %i", pool_item->leafname, g_list_length(regions));

	GList* l = regions;
	for(;l;l=l->next){
		AyyiRegion* region = l->data;
		pool_model_add_region(region, iter);
	}

	g_list_free(regions);
}


static bool
pool_model_add_item (AMPoolItem* pool_item)
{
	// add a file and associated regions to the pool model.

	GtkTreeStore* treestore = gui_pool->treestore;

	// add a top level File entry
	AyyiIdx file_idx = pool_item->pod_index[0];
	dbg(2, "fidx=%i", file_idx);

	char name[256]; pool_item_get_display_name(pool_item, name);

	GtkTreeIter iter;
	gtk_tree_store_append(treestore, &iter, NULL);
	gtk_tree_store_set(treestore, &iter, COL_NAME, name, COL_AYYI_IDX, file_idx, COL_AYYI_ID, pool_item->source_id[0], -1);

	pool_model_regions_add(pool_item, &iter);

	pool_foreach {
		pool_win_pixbuf_cell_update(pool_window, &iter, pool_item, -1);
		break;
	} end_pool_foreach;

	// Store a permanent reference to the new row (iters and rownums are not permanent)
	// -note: we cant access this ref later via the tree, we can only use it to access the tree.
	GtkTreePath* treepath = gtk_tree_model_get_path   (GTK_TREE_MODEL(treestore), &iter);
	pool_item->gui_ref    = gtk_tree_row_reference_new(GTK_TREE_MODEL(treestore), treepath);
	gtk_tree_path_free(treepath);

	return true;
}


GtkTreePath*
pool_model_lookup_by_region (AyyiAudioRegion* region)
{
	// returned path must be free'd.

	struct _id_foreach* row = &(struct _id_foreach){region->id, ""};

	gtk_tree_model_foreach((GtkTreeModel*)gui_pool->treestore, (GtkTreeModelForeachFunc)(pool_model_row_is_id), &row);

	return strlen((&row)[0]->path)
		? gtk_tree_path_new_from_string((&row)[0]->path)
		: NULL;
}


AMPoolItem*
pool_import_file (char* fname, AMPart* prototype_part)
{
	// setup and request a new audio file to be added to songcore.

	// if the file exists, and is not already in the song, we create a new pool_item, and add it to the song.

	// @param fname          - should be a local filename, not a uri. Can be absolute, or relative to the song audio directory.
	// @param prototype_part - if this is set, a part is created later based on this prototype. Optional.
	// @param prototype_part - optional template for a new part to be created containing the new file. Fn takes ownership.

	PF;
	ASSERT_SONG_SHM_RET_FALSE;
	if(!strlen(fname)){ perr ("no filename!"); return NULL; }

	g_return_val_if_fail(strlen(fname) <= AYYI_FILENAME_MAX, NULL);

	AMPoolItem* pool_item = NULL;

	typedef struct {
		bool     make_new_part;
		AMPart*  prototype_part;
	} C;

	C* c = AYYI_NEW(C,
		.make_new_part  = prototype_part ? true : false, // make a new part for this file.
		.prototype_part = prototype_part
	);

	void song_add_file_done(AyyiIdent id, GError** error, gpointer user_data)
	{
		g_return_if_fail(id.type);
		C* c = user_data;

		if(!*error){
			AMPoolItem* pool_item = am_pool__get_item_from_idx(id.idx);
			if(pool_item){

				if(!strlen(pool_item->leafname)){ log_print(LOG_FAIL, "file not added! pool item has no name."); goto out; }
				dbg (0, "pool_item='%s' idx=%u", pool_item->leafname, id.idx);
				if(c->make_new_part){
					//Add a new part for the new file:
					song_add_part_from_prototype(c->prototype_part);
				}
			}

			log_print(0, "new file added");
			if(!pool_item->peakfile_valid) pool_item_load_peak(pool_item, 0); // FIXME move to model
			//pool_model_on_change_old();
		}

		out:
		;window_foreach {
			if(window->statusbar->progress) gtk_widget_hide(window->statusbar->progress);
		} end_window_foreach;
	}

	shell__statusbar_print(NULL, 0, "importing file...");

	//if filename is not an absolute path, prepend the base audio directory:
	char* fullpath = (fname[0] != '/')
		? g_build_filename(song->audiodir, fname, NULL)
		: g_build_filename(fname, NULL);

	if(!g_file_test(fullpath, G_FILE_TEST_EXISTS)){
		pwarn ("file doesnt exist! (%s)\n", fullpath);
		log_print(LOG_FAIL, "File import aborted. File doesnt exist.");
		goto out;
	}

	if(ayyi_song__have_file(fname)){
		//FIXME we need to make sure this is the *relative* path.
		log_print(LOG_FAIL, "Not importing: file '%s' is already in the pool.\n", fname);
		goto out;
	}

	if((pool_item = am_song__add_file(fname, song_add_file_done, c))){
		if(prototype_part) prototype_part->pool_item = pool_item;
	}

  out:
	g_free(fullpath);
	return pool_item;
}


static void
pool_model_on_item_change (GObject* o, AMPoolItem* pool_item, gpointer user_data)
{
	PF;
	{
		GtkTreeIter iter;
		if(pool_model_get_iter_by_poolitem(&iter, pool_item)){
			gtk_tree_store_set(gui_pool->treestore, &iter, COL_AYYI_IDX, pool_item->pod_index[0], COL_AYYI_ID, pool_item->source_id[0], -1); //TODO other columns may also have changed?
		}
	}

	FilterIterator* i = filter_iterator_new (0, NULL, NULL, am_parts, (Filter)am_part__has_pool_item, pool_item);
	AMPart* part;
	while((part = (AMPart*)filter_iterator_next(i))){
		char path_str[128];
  		if(pool_model_lookup_by_region_idx(part->ident.idx, path_str)){
			pool_model_update_region((AyyiRegion*)part->ayyi);
		}else{
			GtkTreeIter iter;
			pool_model_get_iter_by_poolitem(&iter, pool_item);

			pool_model_add_region((AyyiRegion*)part->ayyi, &iter);
		}
	}
	filter_iterator_unref(i);
}


static bool
pool_model_get_iter_by_poolitem (GtkTreeIter* iter, AMPoolItem* pool_item)
{
	g_return_val_if_fail(pool_item, false);
	g_return_val_if_fail(gtk_tree_row_reference_valid(pool_item->gui_ref), false);

	GtkTreePath* treepath = gtk_tree_row_reference_get_path(pool_item->gui_ref);
	if(treepath){
		gtk_tree_model_get_iter(GTK_TREE_MODEL(gui_pool->treestore), iter, treepath);
		gtk_tree_path_free(treepath);
		return true;
	}
	return false;
}


static void
pool_model_on_part_add (GObject* _parts, AMPart* part, gpointer data)
{
	g_return_if_fail(part);

	AMPoolItem* pool_item = part->pool_item;
	if(pool_item){
		g_return_if_fail(pool_item->gui_ref);

		GtkTreeIter parent_iter;
		if(pool_model_get_iter_by_poolitem(&parent_iter, pool_item)){
			pool_model_add_region((AyyiRegion*)part->ayyi, &parent_iter);
		}
	}
}


static void
pool_model_on_region_delete (GObject* o, AyyiIdx region_idx, gpointer user_data)
{
	PF;

	char path_str[128];
	if(pool_model_lookup_by_region_idx(region_idx, path_str)){
    	GtkTreeIter iter;
	    if(gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL(gui_pool->treestore), &iter, path_str)){
			gtk_tree_store_remove(gui_pool->treestore, &iter);
		}
	}
	else dbg(0, "not found (%i)", region_idx);
}


static void
pool_model_on_part_delete (GObject* _parts, AMPart* part, gpointer data)
{
	dbg(1, "%p waveform=%p", part->pool_item, part->pool_item ? part->pool_item : NULL);

	GtkTreePath* path = pool_model_lookup_by_region((AyyiAudioRegion*)part->ayyi);
	if(path){
		GtkTreeIter iter;
		if(gtk_tree_model_get_iter((GtkTreeModel*)gui_pool->treestore, &iter, path)){
			pool_foreach {
				pool_win_statusbar_update(pool_window, &iter, part, part->ayyi->shm_idx);
			} end_pool_foreach;
		}
		gtk_tree_path_free(path);
	}
}


static void
pool_model_on_part_change (GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	if(PART_IS_AUDIO(part)) pool_model_update_region((AyyiRegion*)part->ayyi);
}


static void
pool_model_on_song_unload (GObject* _song, gpointer data)
{
	gtk_tree_store_clear(gui_pool->treestore);
}


void
pool_model_print ()
{
	// see also am_pool__print

	GtkTreeModel* treemodel = GTK_TREE_MODEL(gui_pool->treestore);
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_first(treemodel, &iter)){

		GtkTreePath* path;
		do{
			if((path = gtk_tree_model_get_path(treemodel, &iter))){
  				gchar* fname = NULL;
  				AyyiIdx region_idx = 0;
    			gtk_tree_model_get(treemodel, &iter, COL_NAME, &fname, COL_AYYI_IDX, &region_idx, -1);
				printf("  %i %s\n", region_idx, fname);
				gtk_tree_path_free(path);
			}
		} while(gtk_tree_model_iter_next(treemodel, &iter));
	}
}


/*
 *  str is the split drag string - ie for a single item
 */
void
pool_get_dropdata (const char* str, PoolDrop* data)
{
	*data = (PoolDrop){ .region = -1 };

	if(strlen(str) < 6) return;

	const char* poolname = str + 5;
	dbg (1, "poolname=%s", poolname);

	// drop may be either a region or a file index. Format is: "region=X" or "file=Y".
	char* index;
	if((index = strstr(poolname, "file="))){
		index += strlen("file=");
		AyyiIdx file_idx = atoi(index);
		dbg (1, "found file_idx");
		data->file = am_pool__get_item_from_idx(file_idx);
		if(!data->file){ pwarn("couldnt get pool_item."); log_print(LOG_FAIL, "couldnt get pool_item."); }

	}else if((index = strstr(poolname, "region="))){
		index += strlen("region=");
		AyyiIdx region_idx = atoi(index);
		dbg (1, "region_idx found!");
		data->region = region_idx;
	}
	else pwarn ("no index found.");
}

