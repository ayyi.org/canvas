/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2020-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include <glib.h>
#include <gtk/gtk.h>
#include "panels/panel.h"
#include "agl/gtk.h"

G_BEGIN_DECLS


#define TYPE_GL_PANEL            (gl_panel_get_type ())
#define GL_PANEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GL_PANEL, GlPanel))
#define GL_PANEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_GL_PANEL, GlPanelClass))
#define IS_GL_PANEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GL_PANEL))
#define IS_GL_PANEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_GL_PANEL))
#define GL_PANEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GL_PANEL, GlPanelClass))

typedef struct _GlPanel        GlPanel;
typedef struct _GlPanelClass   GlPanelClass;
typedef struct _GlPanelPrivate GlPanelPrivate;

struct _GlPanel {
    AyyiPanel       parent_instance;
    GlPanelPrivate* priv;
    GtkWidget*      area;
    AGlScene*       scene;
};

struct _GlPanelClass {
	AyyiPanelClass parent_class;
};


GType gl_panel_get_type (void) G_GNUC_CONST;


G_END_DECLS
