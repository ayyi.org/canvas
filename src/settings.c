/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "global.h"
#include <sys/stat.h>
#include <glib/gstdio.h>
#include "gdl/gdl-dock-layout.h"
#include "utils/fs.h"
#include "windows.h"
#include "song.h"
#include "support.h"
#include "panels/log.h"
#ifdef HAVE_YAML_H
#include "yaml/load.h"
#include "yaml/save.h"
#endif
#include "settings.h"

extern SMConfig* config;
extern GHashTable* canvas_types;


#ifdef HAVE_YAML_H
static ConfigWindow* config_get_window   (AyyiPanel*);
static void          config_load_windows (yaml_parser_t*, const char*, gpointer);
static bool          parse_panel         (yaml_parser_t*, yaml_event_t*, ConfigWindow*, const char*);
#else
static void          config_set_float    (char* group_name, const char* param, double value);
static void          config_set_int      (char* group_name, const char* param, int value);
#endif
static bool          create_config_dir   ();

#ifdef HAVE_YAML_H
yaml_emitter_t emitter;

#endif

#ifdef XXX
		static void gdl_dock_layout_foreach_object_save (GdlDockObject* object, gpointer user_data)
		{
			struct {
				yaml_event_t* event;
				GHashTable*   placeholders;
			} *info = user_data, info_child;

			guint n_props, i;
			GValue attr = {0,};

			g_return_if_fail (object != NULL && GDL_IS_DOCK_OBJECT (object));

			map_open_(info->event, gdl_dock_object_nick_from_type (G_TYPE_FROM_INSTANCE (object)));

			// get object exported attributes
			GParamSpec** props = g_object_class_list_properties (G_OBJECT_GET_CLASS (object), &n_props);
			g_value_init (&attr, GDL_TYPE_DOCK_PARAM);
			for (i = 0; i < n_props; i++) {
				GParamSpec* p = props [i];

				if (p->flags & GDL_DOCK_PARAM_EXPORT) {
					GValue v = {0,};

					// export this parameter
					// get the parameter value
					g_value_init (&v, p->value_type);
					g_object_get_property (G_OBJECT (object), p->name, &v);

					/*
					// only save the object "name" if it is set
					//   (i.e. don't save the empty string)
					if (strcmp (p->name, GDL_DOCK_NAME_PROPERTY) || g_value_get_string (&v)) {
						if (g_value_transform (&v, &attr))
							xmlSetProp (node, BAD_CAST p->name, BAD_CAST g_value_get_string (&attr));
					}
					*/

					// free the parameter value
					g_value_unset (&v);
				}
			}
			g_value_unset (&attr);
			g_free (props);

			end_map_(info->event);

			/*
			info_child = *info;
			info_child.where = node;

			// save placeholders for the object
			if (info->placeholders && !GDL_IS_DOCK_PLACEHOLDER (object)) {
				GList *lph = g_hash_table_lookup (info->placeholders, object);
				for (; lph; lph = lph->next)
					gdl_dock_layout_foreach_object_save (GDL_DOCK_OBJECT (lph->data), (gpointer) &info_child);
			}

			// recurse the object if appropiate
			if (gdl_dock_object_is_compound (object)) {
				gtk_container_foreach (GTK_CONTAINER (object), (GtkCallback) gdl_dock_layout_foreach_object_save, (gpointer) &info_child);
			}
			*/
			return;

		  error:
			switch (emitter.error){
				case YAML_MEMORY_ERROR:
					fprintf(stderr, "Memory error: Not enough memory for emitting\n");
					break;
				case YAML_WRITER_ERROR:
					fprintf(stderr, "Writer error: %s\n", emitter.problem);
					break;
				case YAML_EMITTER_ERROR:
					log_print(LOG_FAIL, "yaml emitter error: %s", emitter.problem);
					break;
				default:
					fprintf(stderr, "save: yaml internal error: %s\n", emitter.problem);
					break;
			}
			/*
			yaml_event_delete(&event);
			yaml_emitter_delete(&emitter);
			fclose(fp);
			*/
		}
#endif


static void
save_layout ()
{
	AyyiWindowClass* W = g_type_class_peek(AYYI_TYPE_WINDOW);

	GdlDockLayout* layout = gdl_dock_layout_new(W->master_dock);

	gdl_dock_layout_attach(layout, (GdlDockMaster*)GDL_DOCK_OBJECT(W->master_dock)->master);
	gdl_dock_layout_save_layout(layout, 0);

#ifdef HAVE_YAML_H
	g_autofree gchar* fname = g_strdup_printf("%s/"USER_CONFIG_DIR"/layout.yaml", g_get_home_dir());
#else
#ifdef GDLDOCK_XML
	g_autofree gchar* fname = g_strdup_printf("%s/"USER_CONFIG_DIR"/layout.xml", g_get_home_dir());
#endif
#endif
	if (gdl_dock_layout_save_to_file(layout, fname)) {
		dbg(1, "save done");
	}
	else pwarn("save error");
}


static bool
_config_save (FILE* fp, gpointer _)
{
	if (app->state == APP_STATE_ABORT){ dbg(0, "not saving."); return FALSE; }

	PF;
	#define MAX_WIN_COUNT 9 //cannot save more than this number of windows per type.

	if (!create_config_dir()) return false;

	char value[256];
#ifdef HAVE_YAML_H
	g_auto(yaml_event_t) event;
	yaml_start(fp);

	snprintf(value, 255, "0x%08x", config->part_outline_colour);
	if(!yaml_add_key_value_pair("part_outline_colour", value)) goto error;
	snprintf(value, 255, "0x%08x", config->part_outline_colour_selected);
	if(!yaml_add_key_value_pair("part_outline_colour_selected", value)) goto error;
	if(config->arr_background_image && !yaml_add_key_value_pair("background_image", config->arr_background_image)) goto error;
	if(!yaml_add_key_value_pair_int("snap_mode", song->snap_mode)) goto error;
	if(!yaml_add_key_value_pair("icon_theme", config->icon_theme)) goto error;

	map_open("recent"); // should really be an array
	for (int i=0;i<MAX_RECENT_SONGS;i++) {
		if (!app->recent_songs[i][0]) break;

		snprintf(value, 255, "%i", i);
		if (!yaml_add_key_value_pair(value, app->recent_songs[i])) goto error;
	}
	end_map;

	map_open("windows");

	AyyiWindow* window = (AyyiWindow*)windows->data;
	AYYI_DEBUG gdl_dock_print_recursive(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(window->dock)->master));

	AyyiDockInfo* info[32] = {0,};
	gdl_dock_get_layout_info(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(window->dock)->master), (void*)info);

#ifdef DEBUG
	char* dock_info_to_string (AyyiDockInfo* nfo)
	{
		static char str[32] = "";
		for (int i=0;i<G_N_ELEMENTS(nfo->k);i++) {
			sprintf(str+i, "%i", nfo->k[i]);
		}
		return str;
	}
#endif

	void dock_info_free ()
	{
		PF;
		int idx = 0;
		while (info[idx]) {
			g_free(info[idx]);
			idx++;
		}
	}

	char group_name [64];
	gint width, height, x, y;
	GHashTable* window_count = windows__new_count_table();
	AyyiDockInfo* nfo;
	for(int i=0; (nfo=info[i]); i++){
		dbg(1, "i=%i", i);
		AyyiWindow* window = (AyyiWindow*)gtk_widget_get_toplevel(GTK_WIDGET(nfo->object));
		if(!window->dock->panels){ perr ("empty shell."); continue; }

		// currently the shell is named after its first child panel.
		AyyiPanel* first = (AyyiPanel*)window->dock->panels->data;
		PanelType dtype = G_OBJECT_TYPE(first);
		int count = GPOINTER_TO_INT(g_hash_table_lookup(window_count, &dtype));
		dbg(1, "first=%p type=%i count=%i", first, dtype, count);
		config_make_group_name(AYYI_PANEL_GET_CLASS(first), count, group_name);
		dbg(1, "saving group: %s", group_name);

		map_open(group_name);

		gtk_window_get_size(GTK_WINDOW(window), &width, &height);
		if(!yaml_add_key_value_pair_int("width", width)) goto error;
		if(!yaml_add_key_value_pair_int("height", height)) goto error;

		gtk_window_get_position(GTK_WINDOW(window), &x, &y);
		if(!yaml_add_key_value_pair_int("x", x)) goto error;
		if(!yaml_add_key_value_pair_int("y", y)) goto error;

		GdkWindowState state = gdk_window_get_state(GTK_WIDGET(window)->window);
		gboolean maximised = state & GDK_WINDOW_STATE_MAXIMIZED;
		dbg(1, "maximised=%i", maximised);
		if(!yaml_add_key_value_pair_int("window_state", maximised)) goto error;
		if(!yaml_add_key_value_pair_bool("maximised", maximised)) goto error;

		// child panels.
		// -currently we use the same naming convention as for parent windows.
		if(window->dock->panels){
			map_open("panels");
			char panel_name [64];

			for(i++;(nfo=info[i]);i++){
				GdlDockObject* object = nfo->object;
				if(G_OBJECT_TYPE(object) == GDL_TYPE_DOCK_PANED) continue;
				if(G_OBJECT_TYPE(object) == GDL_TYPE_DOCK) break; // top level - start a new window section

				dbg(1, "  %i: depth=%i %s", i, nfo->depth, dock_info_to_string(nfo));
				AyyiPanel* panel = AYYI_PANEL(object);
				g_return_val_if_fail(panel, false);
				snprintf(panel_name, 63, "%s", object->long_name ? object->long_name : "<Unnamed>");

				//window_count [panel->type] ++;
				PanelType dtype = G_OBJECT_TYPE(panel);
				int n = windows__count_table_add(window_count, dtype);
				if(n > MAX_WIN_COUNT) continue;

				map_open(panel_name);

#ifndef GDLDOCK_XML
				if (!yaml_add_key_value_pair_int("width", GTK_WIDGET(object)->allocation.width)) goto error;
				if (!yaml_add_key_value_pair_int("height", GTK_WIDGET(object)->allocation.height)) goto error;
				if (!yaml_add_key_value_pair_int("x", GTK_WIDGET(object)->allocation.x)) goto error;
				if (!yaml_add_key_value_pair_int("y", GTK_WIDGET(object)->allocation.y)) goto error;
				int depth = ayyi_panel_get_depth(panel);
				if (!yaml_add_key_value_pair_int("depth", depth)) goto error;
				if (!yaml_add_key_value_pair("dock", dock_info_to_string(nfo))) goto error;

				GdlDockObject* parent = gdl_dock_object_get_parent_object(object);
				if (parent) {
					if (!GDL_IS_DOCK_CLASS(G_OBJECT_GET_CLASS(parent))) {
						//possibly a shell with only one panel.
						GtkOrientation orientation;
						g_object_get((GdlDockItem*)parent, "orientation", &orientation, NULL);
						if (!yaml_add_key_value_pair("orientation", get_orientation_string(orientation))) goto error;
					}
				} else pwarn("no parent");
#endif

				AyyiPanelClass* k = (AyyiPanelClass*)G_OBJECT_GET_CLASS(panel);
				if (k->get_config) {
					void** vals = k->get_config(panel);
					ConfigParam** panel_config = k->config;
					int p; ConfigParam* param; for(p=0;(param=panel_config[p]);p++){
						if(vals[p]){
							switch(param->utype){
								case G_TYPE_FLOAT:
									dbg(1, "    %s=%.2f", param->name, *(float*)vals[p]);
									if(!yaml_add_key_value_pair_float(param->name, *(float*)vals[p])) goto error;
									break;
								case G_TYPE_INT:
									dbg(1, "    %s=%i", param->name, *(int*)vals[p]);
									if(!yaml_add_key_value_pair_int(param->name, *(int*)vals[p])) goto error;
									break;
								default:
									dbg(1, "    %s=%s", param->name, *(char**)vals[p]);
									if(!yaml_add_key_value_pair(param->name, *(char**)vals[p])) goto error;
									break;
							}
						}
					}
					g_free(vals);
				}

				end_map;
			}

			end_map; // end panels
        }

		end_map; // end window

		dbg(2, "map ended");
	}
	end_map; // end windows
	dock_info_free();
	g_hash_table_destroy(window_count);

	end_map;

	end_document;
	yaml_emitter_delete(&emitter);
	dbg(1, "yaml write finished ok.");

#ifdef GDLDOCK_XML
	save_layout();
#endif

#ifdef HAVE_YAMLGLIB
#ifdef DEBUG
	GString* sb = g_string_new("");
#endif
	GError* error = NULL;
	#ifdef locallibyamlglib
	#include "libyaml-glib/src/libyaml-glib-1.0.h"
	GYAMLWriter* writer = g_yaml_writer_new(NULL);
	GError* error = NULL;
	//g_yaml_writer_stream_object (writer, G_OBJECT(song->controller), sb, &error);
	//g_yaml_writer_stream_object (writer, G_OBJECT(app.master_dock), sb, &error);
	//g_yaml_writer_stream_object (writer, G_OBJECT(song->tracks), sb, &error);

	#else
	#if 0
	#include "yaml_writer.h"

	printf("\n");
	dbg(0, "eventsize=%i", sizeof(yaml_event_t));
	printf("\n");

	YAMLWriter* writer = yaml_writer_new(NULL);
	yaml_writer_stream_object (writer, G_OBJECT(GDL_DOCK_OBJECT(app.master_dock)->master), sb, &error);
	//yaml_writer_stream_object (writer, G_OBJECT(song->controller), sb, &error);
	#endif
	#endif

	if(error){
		pwarn("%s", error->message);
	} else dbg(1, "no errors");

	if(_debug_){
		printf("\n");
		dbg(1, "yaml=%s", sb->str);
		printf("\n");
	}
	g_string_free(sb, true);
#endif

	return true;

  error:
	switch (emitter.error){
		case YAML_MEMORY_ERROR:
			fprintf(stderr, "Memory error: Not enough memory for emitting\n");
			break;
		case YAML_WRITER_ERROR:
			fprintf(stderr, "Writer error: %s\n", emitter.problem);
			break;
		case YAML_EMITTER_ERROR:
			log_print(LOG_FAIL, "yaml emitter error: %s", emitter.problem);
			break;
		default:
			fprintf(stderr, "Internal error\n");
			break;
	}
	yaml_emitter_delete(&emitter);
  out:
	return false;

#endif // HAVE_YAML

	return true;
}


bool
config_save ()
{
	char* tmp = g_strdup_printf("%s/imgview.yaml", g_get_tmp_dir());

	bool ok = with_fp(tmp, "wb", _config_save, NULL);

	if(ok){
		if(g_rename (tmp, app->config_filename)){
			pwarn("failed to save config");
			ok = false;
		}
	}
	g_free(tmp);

	return ok;
}


void
config_load0 ()
{
	add_pixmap_directory(PACKAGE_DATA_DIR "/" PACKAGE "/pixmaps");

	static const char* dirs[] = {IMAGES_DIR, "../pixmaps", "pixmaps", NULL};
	const char* image_dir = find_path(dirs);
	dbg (2, "image_dir=%s", image_dir);

	g_autofree gchar* cwd = g_get_current_dir();

	config->arr_background_image = NULL;
	config->svgdir = (image_dir[0] == '/') ? g_strdup_printf("%s/svg", image_dir) : g_strdup_printf("%s/%s/svg", cwd, image_dir);
	config->accels_file = g_strdup_printf("%s/"USER_CONFIG_DIR"/seismix.keys", g_get_home_dir());
#ifdef USE_LV2
	config->plugindir = g_strdup_printf(PACKAGE_LIB_DIR);
#endif
}


static void
load_recent (yaml_parser_t* parser, const char* name, gpointer _)
{
	void recent_item (const yaml_event_t* event, const char* key, gpointer _)
	{
		char* value = (char*)event->data.scalar.value;

		for (int i=0;i<MAX_RECENT_SONGS;i++) {
			char* item = app->recent_songs[i];
			if (!item[0]) {
				g_strlcpy(item, value, 256);
				break;
			}
			if (!strcmp(item, value)) break;
		}
	}


	yaml_load_mapping(parser,
		NULL,
		(YamlValueHandler[]){
			{NULL, recent_item},
			{NULL}
		},
		NULL
	);
}


static bool
_config_load (FILE* fp, gpointer _)
{
	void load_part_outline (yaml_parser_t* parser, const char* value, gpointer _)
	{
		long long colour = strtoll(value, NULL, 16);
		config->part_outline_colour = colour;
	}


	void load_part_outline_selected (yaml_parser_t* parser, const char* value, gpointer _)
	{
		long long colour = strtoll(value, NULL, 16);
		config->part_outline_colour_selected = colour;
	}


	void load_snap_mode (yaml_parser_t* parser, const char* value, gpointer _)
	{
		guint snap_mode = atoi(value);
		if(snap_mode < MAX_SNAP) config->snap_mode = snap_mode;
	}

	void load_background_image (yaml_parser_t* parser, const char* value, gpointer _)
	{
		config->arr_background_image = g_strdup(value);
	}

	void load_icon_theme (yaml_parser_t* parser, const char* value, gpointer _)
	{
		config->icon_theme = g_strdup(value);
	}

	bool ok = yaml_load(fp, (YamlHandler[]){
		{"part_outline_colour", load_part_outline},
		{"part_outline_colour_selected", load_part_outline_selected},
		{"snap_mode", load_snap_mode},
		{"background_image", load_background_image},
		{"icon_theme", load_icon_theme},
		{"recent", load_recent},
		{"windows", config_load_windows},
		{NULL}
	});

	if (ok) log_print(LOG_OK, "config loaded");

	return ok;
}


bool
config_load ()
{
	bool try_config (const char* dir, void* user_data)
	{
		g_autofree char* file = g_strdup_printf("%s/seismix.yml", dir);

		return with_fp(file, "rb", _config_load, NULL);
	}

	if (app_config_dir_foreach(try_config, NULL)) {
		return true;
	}

	ayyi_log_print(LOG_FAIL, "cannot open config file (%s)", app->dirs[0]);

	return false;
}


static ConfigParam*
window_class__config_lookup (AyyiPanelClass* class, const char* param)
{
	dbg(2, "%s", param);

	int i = 0;
	ConfigParam* p = NULL;
	while((p = class->config[i++])){
		if(!strcmp(param, p->name)) return p;
	}

	pwarn("not found: %s", param);
	return NULL;
}


/*
 *  Return the value of a single config setting.
 *  -used for settings that cannot be stored at window creation time.  XX no longer needed - width & height?
 *
 *  note: it is marginally more efficient to apply all settings at once in window initialiser.
 */
int
config_get_window_setting_int (AyyiPanel* panel, const char* param)
{
	AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);

	if(config->windows){
		ConfigWindow* window_settings = config_get_window(panel);
		if(window_settings){
			GList* l = window_settings->params;
			for(;l;l=l->next){
				ConfigParamValue* pval = l->data;
				ConfigParam* i = pval->param;
				int v = pval->val.i;
				if(!strcmp(i->name, param)){
					dbg(2, "%s=%i", i->name, v);
					return CLAMP(v, i->min.i, i->max.i);
				}
			}
		}
	}

	// not found, so return the default value
	ConfigParam* setting = window_class__config_lookup(klass, param);
	return setting
		? setting->dfault.i
		: (dbg(2, "%s: using default: %i", param, setting->dfault), 0);
}


bool
config_get_window_setting_bool (AyyiPanel* panel, const char* param)
{
	AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);

	if(config->windows){
		ConfigWindow* window_settings = config_get_window(panel);
		g_return_val_if_fail(window_settings, false);

		for(GList* l=window_settings->params;l;l=l->next){
			ConfigParamValue* pval = l->data;
			ConfigParam* i = pval->param;
			bool v = pval->val.b;
			if(!strcmp(i->name, param)){
				dbg(2, "%s=%i", i->name, v);
				return v;
			}
		}
	}

	// not found, so return the default value.
	ConfigParam* setting = window_class__config_lookup(klass, param);
	if(setting) dbg(2, "%s: using default: %i", param, setting->dfault);
	return setting ? setting->dfault.b : 0;
}


double
config_get_window_setting_float (AyyiPanel* panel, const char* param)
{
	AyyiPanelClass* klass = AYYI_PANEL_GET_CLASS(panel);

	if(config->windows){
		ConfigWindow* window_settings = config_get_window(panel);
		if(window_settings){
			for(GList* l=window_settings->params;l;l=l->next){
				ConfigParamValue* pval = l->data;
				ConfigParam* f = pval->param;
				if(!strcmp(f->name, param)){
					float v = pval->val.f;
					dbg(2, "%s=%.2f", f->name, v);
					return CLAMP(v, f->min.f, f->max.f);
				}
			}
		}
	}

	// not found, so return the default value.
	ConfigParam* setting = window_class__config_lookup(klass, param);
	if(!setting) pwarn("%s: cannot get default. setting not found", param);
	return setting ? setting->dfault.f : 0;
}


const char*
config_get_window_setting_char (AyyiPanel* panel, const char* param)
{
	if(config->windows){
		ConfigWindow* window_settings = config_get_window(panel);
		if(window_settings){
			GList* l = window_settings->params;
			for(;l;l=l->next){
				ConfigParamValue* pval = l->data;
				ConfigParam* f = pval->param;
				if(!strcmp(f->name, param)){
					dbg(2, "%s=%s", f->name, pval->val.c);
					return pval->val.c;
				}
			}
		}
	}

	// no existing setting - return the default value
	ConfigParam* setting = window_class__config_lookup(AYYI_PANEL_GET_CLASS(panel), param);

	return setting
		? setting->dfault.c
		: (pwarn("%s: cannot get default. setting not found", param), NULL);
}


#ifdef HAVE_YAML_H
static void yaml_window_set_width      (ConfigWindow* window, GValue* width ){ window->width  = g_value_get_int(width);  }
static void yaml_window_set_height     (ConfigWindow* window, GValue* height){ window->height = g_value_get_int(height); }
static void yaml_window_set_x          (ConfigWindow* window, GValue* x     ){ window->x      = g_value_get_int(x);      }
static void yaml_window_set_y          (ConfigWindow* window, GValue* y     ){ window->y      = g_value_get_int(y);      }
static void yaml_window_set_depth      (ConfigWindow* window, GValue* depth ){ window->depth  = g_value_get_int(depth);  }
static void yaml_window_set_orientation(ConfigWindow* window, GValue* o     ){ window->orientation = !strcmp(g_value_get_string(o), "vertical") ? GTK_ORIENTATION_VERTICAL : GTK_ORIENTATION_HORIZONTAL; }
static void yaml_window_set_docktree   (ConfigWindow* window, GValue* d     ){
	const gchar* s = g_value_get_string(d);
	int i=0; for(;i<16;i++){
		gchar* c = g_strndup(s+i, 1);
		window->docktree[i] = atoi(c);
		g_free(c);
	}
}
static void yaml_window_set_maximised (ConfigWindow* window, GValue* o     ){ window->maximised = g_value_get_boolean(o); dbg(2, "maximised=%i", window->maximised); }

struct {char* name; int type; void (*set)(ConfigWindow*, GValue*); } yaml_windows[] = {
	{"width",       G_TYPE_INT,    yaml_window_set_width      },
	{"height",      G_TYPE_INT,    yaml_window_set_height     },
	{"x",           G_TYPE_INT,    yaml_window_set_x          },
	{"y",           G_TYPE_INT,    yaml_window_set_y          },
	{"depth",       G_TYPE_INT,    yaml_window_set_depth      },
	{"orientation", G_TYPE_STRING, yaml_window_set_orientation},
	{"dock",        G_TYPE_STRING, yaml_window_set_docktree   },
	{"maximised",   G_TYPE_BOOLEAN,yaml_window_set_maximised  },
};


static bool
parse_params (yaml_parser_t* parser, ConfigWindow* panel)
{
	// A new window or panel has been started with YAML_MAPPING_START_EVENT.
	// Returns true after getting YAML_MAPPING_END_EVENT.

	AyyiPanelClass* klass = g_type_class_peek(panel->type);
	ConfigParam** params = klass->config;

	char key[64] = {0,};
	yaml_event_t event;

	while(yaml_parser_parse(parser, &event)){
		switch(event.type){
			case YAML_SCALAR_EVENT:
				if(!key[0]){
					g_strlcpy(key, (char*)event.data.scalar.value, 64);
				}else{
					bool found = false;
					GValue gval = {0,};
					// first check generic config-params:
					for(int i=0;i<G_N_ELEMENTS(yaml_windows);i++){
						if(!strcmp(yaml_windows[i].name, key)){
							found = true;
							switch(yaml_windows[i].type){
								case G_TYPE_INT:
									g_value_init(&gval, G_TYPE_INT);
									g_value_set_int(&gval, atoi((char*)event.data.scalar.value));
									break;
								case G_TYPE_STRING:
									g_value_init(&gval, G_TYPE_STRING);
									g_value_set_string(&gval, (char*)event.data.scalar.value);
									break;
								case G_TYPE_BOOLEAN:
									;gboolean set = !strcmp("true", (char*)event.data.scalar.value);
									g_value_init(&gval, G_TYPE_BOOLEAN);
									g_value_set_boolean(&gval, set);
									break;
							}
							yaml_windows[i].set(panel, &gval);
							break;
						}
					}
					if(!found && params){
						// check window-specific config-params:
						int i = 0;
						ConfigParam* p = NULL;
						bool found2 = false;
						while((p = params[i++])){
							if(!strcmp(key, p->name)){
								dbg(2, "  %s --> %s", p->name, event.data.scalar.value);
								AMVal val;
								switch(p->utype){
									case G_TYPE_STRING:
										val.c = g_strdup((char*)event.data.scalar.value);
										dbg(2, "char type: %s=%s", p->name, val.c);
										break;
									case G_TYPE_INT:
										val.i = atoi((char*)event.data.scalar.value);
										dbg(2, "int type: %s=%i", p->name, val.i);
										break;
									default:
										val.f = atof((char*)event.data.scalar.value);
										break;
								}
								panel->params = g_list_append(panel->params, AYYI_NEW(ConfigParamValue,
									.param = p,
									.val = val
								));
								found2 = true;
								break;
							}
							if(i > 100) break;
						}
						if(!found2 && strcmp(key, "window_state") && _debug_) pwarn("unhandled: %p: %s", panel, key);
					}
					key[0] = '\0';
				}
				break;
			case YAML_MAPPING_START_EVENT:
				if(!strcmp(key, "panels")){
					yaml_event_delete(&event);
					get_expected_event(parser, &event, YAML_SCALAR_EVENT);
					g_strlcpy(key, (char*)event.data.scalar.value, 64);
					yaml_event_delete(&event);
					get_expected_event(parser, &event, YAML_MAPPING_START_EVENT);
				}
				parse_panel(parser, &event, panel, key);
				key[0] = '\0';
				break;
			case YAML_MAPPING_END_EVENT:
				goto out;
		}
		yaml_event_delete(&event);
	}
	return false;

  out:
	yaml_event_delete(&event);
	return true;
}


static bool
parse_panel (yaml_parser_t* parser, yaml_event_t* event, ConfigWindow* parent, const char* name)
{
	g_return_val_if_fail(parent, NULL);

	ConfigWindow* panel = g_new0(ConfigWindow, 1);
	parent->child_panels = g_list_append(parent->child_panels, panel);

	if((panel->type = windows__panel_type_from_str(name))){
		return parse_params(parser, panel);
	}else{
		pwarn ("bad config file. unknown window type. %s", name);
		// TODO need to go to next map-end
	}

	return panel;
}


static void
config_load_windows (yaml_parser_t* parser, const char* name, gpointer _)
{
	// At this point in the parsing, we have just found a "windows" map.
	// The section is a dictionary of panels, so the first event should be a YAML_SCALAR_EVENT

	int num_windows = 0;
	config->windows = g_array_sized_new(ZERO_TERMINATED, TRUE, sizeof(ConfigWindow), 4);

	char key[64] = {0,};
	int depth = 0;
	yaml_event_t event;
	while(yaml_parser_parse(parser, &event)){
		switch (event.type) {
			case YAML_SCALAR_EVENT:
				dbg(2, "YAML_SCALAR_EVENT: value='%s' %i plain=%i style=%i", event.data.scalar.value, event.data.scalar.length, event.data.scalar.plain_implicit, event.data.scalar.style);

				g_strlcpy(key, (char*)event.data.scalar.value, 64);
				break;
			case YAML_MAPPING_START_EVENT:
				depth++;
				if(key[0]){
					dbg(2, "key=%s", key);
					switch(depth){
						case 1:
							g_array_set_size(config->windows, ++num_windows);
							dbg(2, "YAML_MAPPING_START_EVENT: new toplevel window: '%s' arraysize=%i type=", key, config->windows->len);
							ConfigWindow* window = &g_array_index(config->windows, ConfigWindow, num_windows - 1);
							window->x = window->y = -1;
							if((window->type = windows__panel_type_from_str(key)))
								parse_params(parser, window);
							else
								pwarn ("unknown window type in config file: %s", key);

							break;
					}
					key[0] = '\0';
				}
				else pwarn("mapping event has no name. depth=%i", depth);
				break;
			case YAML_MAPPING_END_EVENT:
				dbg(2, "YAML_MAPPING_END_EVENT");
				if(--depth < 0){
					dbg(2, "done. n windows found: %i", num_windows);
					goto out;
				}
				break;
			default:
				pwarn("unexpected parser event type: %i", event.type);
				break;
		}
		yaml_event_delete(&event);
	}
  out:
	yaml_event_delete(&event);
}
#endif //HAVE_YAML_H


/*
 *  Get config info specific to an individual instance
 */
void
config_load_window_instance (AyyiPanel* panel)
{
#ifdef HAVE_YAML_H
	AyyiPanelClass* klass = g_type_class_peek(G_OBJECT_TYPE(panel));

	ConfigParam** params = (ConfigParam**)klass->config;
	if(params){
		for(int i=0;params[i];i++){
			ConfigParam* param = params[i];

			switch(param->utype){
				case G_TYPE_INT:
					;int ival = config_get_window_setting_int(panel, param->name);
					dbg(2, "int value found: %s %i", param->name, ival);
					if(param->set.i) param->set.i(panel, ival);
					break;
				case G_TYPE_STRING:
					;const char* cval = config_get_window_setting_char(panel, param->name);
					dbg(2, "char value found: %s %s", param->name, cval);
					if(param->set.c) param->set.c(panel, cval);
					break;
				case G_TYPE_BOOLEAN:
					;bool bval = config_get_window_setting_int(panel, param->name);
					if(param->set.i) param->set.i(panel, bval);
					break;
				default:
					;float fval = config_get_window_setting_float(panel, param->name); // unneccesarily slow
					dbg(2, "value found: %s %f", param->name, fval);
					if(param->set.f) param->set.f(panel, fval);
					break;
			}
		}
	}
#endif
}


/*
 *  Return true if the config file reports at least one open log window.
 */
bool
config_log_window_is_open ()
{
	ConfigWindow* window;
	if(!config->windows) return false;

	for(int w=0;w<config->windows->len;w++){
		window = &g_array_index(config->windows, ConfigWindow, w);
		if(window->type == AYYI_TYPE_LOG_WIN) return true;
	}

	return false;
}


#if HAVE_YAML_H
static ConfigWindow*
config_get_window (AyyiPanel* panel)
{
	// config_window doesnt have instance_num prop. do we need to add it?

	g_return_val_if_fail(config->windows, NULL);

	GType d = G_OBJECT_TYPE(panel);
	int w;
	for(w=0;w<config->windows->len;w++){
		ConfigWindow* window = &g_array_index(config->windows, ConfigWindow, w);
		if(window->child_panels){
			GList* l = window->child_panels;
			for(;l;l=l->next){
				ConfigWindow* child = l->data;
				if(child->type == d) return child;
			}
		}
	}

	if(_debug_) pwarn("no config found for window->type=%s n_windows=%i", AYYI_PANEL_CLASS(g_type_class_peek(d))->name, config->windows->len);
	return NULL;
}
#endif


void
config_make_group_name (AyyiPanelClass* class, int count, char* group_name)
{
	g_return_if_fail(class);

	snprintf(group_name, 63, "%s %i", class->name, count + 1);
}


int
config_get_n_windows (PanelType type)
{
	// Return the number of windows in the config for the specified type
	// or if not specified, the total number of windows.

	// TODO this is only really useful if there is only one shell.

	int n = 0;

	int w;
	for(w=0;w<config->windows->len;w++){
		ConfigWindow* window = &g_array_index(config->windows, ConfigWindow, w);
		if(window->type == type || !type) n++;
	}
	return n;
}


#ifndef HAVE_YAML_H
static void
config_set_int (char* group_name, const char* param, int value)
{
	static char str[256];
	snprintf(str, 255, "%i", value);
	g_key_file_set_value(app.key_file, group_name, param, str);
}


static void
config_set_float (char* group_name, const char* param, double value)
{
	static char str[256];
	snprintf(str, 255, "%.1f", value);
	g_key_file_set_value(app.key_file, group_name, param, str);
}
#endif


void
config_new ()
{
#if 0
	//g_key_file_has_group(GKeyFile *key_file, const gchar *group_name);

	GError* error = NULL;
	char data[256 * 256];
	sprintf(data,
		"#this is a Seismix application config file.\n"
		"[Seismix]\n"
	);

	if(!g_key_file_load_from_data(app->key_file, data, strlen(data), G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, &error)){
		perr ("error creating new key_file from data. %s", error->message);
		g_error_free(error);
		error = NULL;
		return;
	}

	log_print(0, "new config file created.");
#endif
}


static bool
create_config_dir ()
{
	gboolean ret = false;

	gchar* path = g_build_filename(g_get_home_dir(), USER_CONFIG_DIR, NULL);
	if(file_exists(path)) ret = true;
	else {
		if(!g_mkdir_with_parents(path, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP)) ret = true;
		else pwarn("cannot create config dir: %s", path);
	}
	if(!ret) pwarn("cannot access config dir: %s", path);
	g_free(path);

	return ret;
}


#if 0 // TODO a GType so that we can save colours like 0x777777ff as numbers instead of strings.
GType
colour32_get_type (void)
{
	static GType spec_type = 0;

	if(!spec_type){
		static const GTypeInfo type_info = {
			sizeof (GParamSpecClass),
			NULL, NULL,
			(GClassInitFunc)gimp_param_unit_class_init,
			NULL, NULL,
			sizeof(GimpParamSpecUnit),
			0, NULL, NULL
		};

		spec_type = g_type_register_static (G_TYPE_PARAM_INT, "Colour32", &type_info, 0);
	}

	return spec_type;
}
#endif
