/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
/*
 *  svgbutton.c - multi-state button with svg images.
 *
 */
#include "config.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "gtk/gtksignal.h"
#include "gtk/gtktogglebutton.h"
#include "debug/debug.h"
#include "ayyi/ayyi_utils.h"
#include "gui_types.h"
#include "svgbutton.h"
#include "icon.h" //needed for icon cache.

struct _SvgButtonState
{
   char*      name;
   char*      icon_path;
   GtkWidget* image;
};

static void svgbutton_class_init    (SvgButtonClass*);
static void svgbutton_init          (SvgButton*);
static void svgbutton_finalize      (GObject*);
static void svgbutton_clicked       (GtkButton*);
static void svgbutton_set_state     (SvgButton*, SvgButtonState*);

static GtkButtonClass* parent_class = NULL;

G_DEFINE_TYPE (SvgButton, svgbutton, GTK_TYPE_BUTTON)


static void
svgbutton_class_init (SvgButtonClass* class)
{
	GObjectClass* gobject_class = G_OBJECT_CLASS(class);
	GtkButtonClass* button_class = GTK_BUTTON_CLASS(class);

	parent_class = g_type_class_peek_parent(class);

	button_class->clicked = svgbutton_clicked;

	gobject_class->finalize = svgbutton_finalize;

	class->svgbutton = NULL;
}


static void
svgbutton_init (SvgButton* button)
{
	button->size = 16;
}


static void
svgbutton_finalize (GObject* object)
{
	SvgButton* button = SVGBUTTON(object);

	g_return_if_fail(IS_SVGBUTTON(object));

	for(GList* l=button->state_list;l;l=l->next){
		SvgButtonState* state = l->data;

		g_clear_pointer(&state->image, g_object_unref);
		g_clear_pointer(&state->name, g_free);
		g_clear_pointer(&state->icon_path, g_free);
	}
#ifdef HAVE_GTK_2_22
	g_list_free_full(button->state_list, g_free);
#else
	g_list_free(button->state_list);
#endif
	button->state_list = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}


GtkWidget*
svgbutton_new ()
{
	return (GtkWidget*)g_object_new (TYPE_SVGBUTTON, NULL);
}


char*
svgbutton_get_state (SvgButton* button)
{
	return button->current_state->name;
}


void
svgbutton_set_state_n (SvgButton* button, int n)
{
	SvgButtonState* state = g_list_nth_data(button->state_list, n);
	if(state) svgbutton_set_state(button, state);
	else pwarn("bad state requested: state=%i", n);
}


static void
svgbutton_set_state (SvgButton* button, SvgButtonState* state)
{
	//TODO it might be better to just swap the pixbuf rather than swapping the GtkImage

	if(!state) return;
	g_return_if_fail(state->image);

	GtkContainer* container = GTK_CONTAINER(button);

	button->mouseover_text = state->name;

	//add the image to the button
	GList* children;
	if((children = gtk_container_get_children(container))){
		gtk_container_remove(container, children->data);
		g_list_free(children);
	}
	gtk_container_add(container, state->image);

	button->current_state = state;
}


void
svgbutton_add_state (SvgButton* button, const char* state_name, const char* icon_path)
{
	GdkPixbuf* pixbuf = gtk_icon_theme_load_icon (icon_theme, icon_path, 16, 0, NULL);

	SvgButtonState* state = AYYI_NEW(SvgButtonState,
		.name = g_strdup(state_name),
		.icon_path = g_strdup(icon_path),
		.image = g_object_ref_sink(gtk_image_new_from_pixbuf(pixbuf)) // ref_sink fixes refcounting issues when unparented
	);

	if(pixbuf) g_object_unref(pixbuf);
	gtk_widget_show(state->image);

	button->state_list = g_list_append(button->state_list, state);

	// if the button doesnt yet have a state, set it
	GList* children = NULL;
	if(!(children = gtk_container_get_children(GTK_CONTAINER(button))))
		svgbutton_set_state(button, state);
	else
		g_list_free(children);
}


void
svgbutton_set_size (SvgButton* button, int size)
{
	if(size < 4 || size > 800){ pwarn("bad size (%i).\n", size); return; }

	if(size == button->size) return;

	button->size = size;

	for(GList* l=button->state_list;l;l=l->next){
		SvgButtonState* state = l->data;

		GdkPixbuf* pixbuf = gtk_icon_theme_load_icon (icon_theme, state->icon_path, 16, 0, NULL);
		gtk_image_set_from_pixbuf((GtkImage*)state->image, pixbuf);
		g_object_unref(pixbuf);
	}
}


static void
svgbutton_clicked (GtkButton* button)
{
	SvgButton* svgbutton = SVGBUTTON(button);

	// switch to the next state
	GList* list = g_list_find(svgbutton->state_list, svgbutton->current_state);
	GList* next = list->next ? list->next : svgbutton->state_list;
	svgbutton_set_state(svgbutton, next->data);
}
