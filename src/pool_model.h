/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <model/pool.h>
#ifdef USE_CLUTTER
#include <clutter/clutter.h>
#endif

#if defined(__pool_model_c__) || defined(__pool_window_c__)
enum
{
	COL_NAME = 0,
	COL_OVERVIEW,
	COL_AYYI_IDX,  // G_TYPE_INT - either file index or region index depending on tree depth.
	COL_AYYI_ID,   // G_TYPE_UINT64 - added for local objects that dont have shm idx - can this replace COL_AYYI_IDX ?
	NUM_COLS
};

typedef struct
{
    // This is per _song, and is shared by all pool windows.

    GtkTreeStore*      treestore;
    GtkCellRenderer*   overview_renderer;
    bool               update_pending;
} UISongPool;
#endif

#define gui_pool ((UISongPool*)((AMCollection*)song->pool)->user_data)

struct _gui_pool_item
{
#ifdef USE_CLUTTER
	CoglHandle texture;
#endif
};

typedef struct
{
    AMPoolItem* file;
    AyyiIdx     region;
} PoolDrop;

void            pool_init                     ();
void            pool_free                     ();
void            pool_model_update_item        (AMPoolItem*);
void            pool_model_update_now         ();
void            pool_model_clear              ();

GtkTreePath*    pool_model_lookup_by_region   (AyyiAudioRegion*);

gboolean        pool_model_iter_is_top_level  (GtkTreeIter*);
gboolean        pool_model_path_is_top_level  (GtkTreePath*);

AMPoolItem*     pool_import_file              (char* uri, AMPart*);

void            pool_model_print              ();

void            pool_get_dropdata             (const char*, PoolDrop*);
