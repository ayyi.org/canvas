/*
 * This file is part of AyyiGtk.
 * Copyright (C) 2008 Tim Orford <tim@orford.org>
 */

#ifndef __TEXT_FX_H__
#define __TEXT_FX_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define TEXT_FX(obj)          GTK_CHECK_CAST (obj, text_fx_get_type (), TextFx)
#define TEXT_FX_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, text_fx_get_type (), TextFxClass)
#define IS_TEXT_FX(obj)       GTK_CHECK_TYPE (obj, text_fx_get_type ())


typedef struct _TextFx        TextFx;
typedef struct _TextFxClass   TextFxClass;
typedef struct _TextFxPriv    TextFxPriv;

struct _TextFx
{
  GtkWidget widget;

  guint8      button;          //button currently pressed or 0 if none

  guint32     timer;           //ID of update timer, or 0 if none

  gint        in_button;       //the mouse is currently over the widget.

  int         text_width;
  int         text_height;
  int         font_size;       //pts

  int         padding_top;

#if 0
  int         val;             //the number to display.
  int         n_chars;         //string is padded with zeros up to this length.
#endif
  char*       str;             //the string to display - will probably replace Val above.

  Glyph**     src_bufs;        //pre-rendered characters
  GdkPixbuf*  pixbuf;

  TextFxPriv* priv;
};

struct _TextFxClass
{
  GtkWidgetClass parent_class;
};


GtkType        text_fx_get_type               ();
GtkWidget*     text_fx_new                    (Glyph**);
GtkAdjustment* text_fx_get_adjustment         (TextFx*);
void           text_fx_set_update_policy      (TextFx*, GtkUpdateType);

void           text_fx_set_string             (GtkWidget*, const char* str);

void           text_fx_set_font               (TextFx*, const char* font);
void           text_fx_set_padding            (GtkWidget*, int top, int bottom);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __TEXT_FX_H__ */
