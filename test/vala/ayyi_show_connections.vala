/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

static int n_channels = 0;
static bool inputs_only = false;
const OptionEntry[] entries = 
{
	{ "channels", 'c', 0, OptionArg.INT, ref n_channels, "only show connections with this number of channels (mono or stereo)", "N" },
	{ "inputs", 'i', 0, OptionArg.NONE, ref inputs_only, "only show input connections", "N" },
	{ null }
};

public class Ayyi.ShowConnections : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		OptionContext context = new OptionContext("Ayyi Set Routing");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}
		stdout.printf("channels=%i\n", n_channels);

		app = new ShowConnections();

		if(!app.run(((ShowConnections*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		int n_connections = 0;

		if(!(bool)AyyiSong.audio_connection_next(null)){
			print("\tno audio connections\n");
			app.quit();
			return;
		}

		/*
		stdout.printf("-------------------------------------------------------------------------\n");
		stdout.printf("connections:\n");
		//TODO why we get item 0?
		if((bool)AyyiSong.audio_connection_next(null)){
			stdout.printf("  idx  flg n %24s\n", "name");
			AyyiConnection* conn = null;
			char b[256];
			char* j = b;
			while((bool)(conn = AyyiSong.audio_connection_next(conn))){
				if(AyyiSong.connection__parse_string(conn, b)){
					stdout.printf("  %3i %4i %i %24s %22s %22s\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)b, (string)(j+64), (string)(j+128));
				}else{
					stdout.printf("  %3i %4i %i %24s (parse error)\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)conn->name);
				}

				n_connections++;
			}
		}
		else stdout.printf("\tno audio connections\n");
		*/

		stdout.printf("-----------------------------------------------------------------------------------\n");
		stdout.printf("input connections:\n");
		stdout.printf("  %3s %4s %1s\n", "idx", "fl", "n");
		char b[256];
		char* j = b;
		AyyiConnection* conn = null;
		while((bool)(conn = AyyiSong.next_input_connection(conn, n_channels))){
			if(AyyiSong.connection__parse_string(conn, b)){
				stdout.printf("  %3i %4i %i %24s %10s %22s %22s\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)b, conn->device, (string)(j+64), (string)(j+128));
			}else{
				stdout.printf("  %3i %4i %i %24s %10s (parse error)\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)conn->name, conn->device);
			}
		}

		if(!inputs_only){
			stdout.printf("-----------------------------------------------------------------------------------\n");
			stdout.printf("output connections:\n");
			conn = null;
			while((bool)(conn = AyyiSong.next_output_connection(conn, n_channels))){
				if(AyyiSong.connection__parse_string(conn, b)){
					stdout.printf("  %3i %4i %i %24s %10s %22s %22s\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)b, conn->device, (string)(j+64), (string)(j+128));
				}else{
					stdout.printf("  %3i %4i %i %24s %10s (parse error)\n", conn->shm_idx, conn->flags, (int)conn->nports, (string)conn->name, conn->device);
				}
			}
		}

		stdout.printf("-----------------------------------------------------------------------------------\n");

		stdout.printf("total connections: %i\n", n_connections);

		app.quit();
	}
}


