/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

enum {
   TR_REC,
   TR_REW,
   TR_STOP,
   TR_PLAY,
   TR_FF,
   TR_CYC
};

extern GArray* tr_array;   // array holding info on icons and callbacks for transport buttons.

struct _TransportButton
{
   char*     desc;
   char*     stock_id;
   gpointer  callback;    // fn to call when button is activated.
   bool      (*active)(); // fn to check to see if the button should be marked 'active'.
   gpointer  mouseover;   // fn to call when mouse enter-notify events.
};

void transport__init     ();
void transport_enable    ();
void transport_disable   ();
