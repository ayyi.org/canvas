/*
 * +----------------------------------------------------------------------+
 * | This file is part of the Ayyi project. http://www.ayyi.org           |
 * | copyright (C) 2014-2021 Tim Orford <tim@orford.org>                  |
 * +----------------------------------------------------------------------+
 * | This program is free software; you can redistribute it and/or modify |
 * | it under the terms of the GNU General Public License version 3       |
 * | as published by the Free Software Foundation.                        |
 * +----------------------------------------------------------------------+
 *
 */

#include "src/style.h"

// Although 10dB gain is desirable, the fader law implement below is not
// suitable, so 6dB gain is used for now
#define GAIN_MAX_6dB 2.0
#define GAIN_MAX_10dB 3.5

typedef struct {
	AGlActor     actor;
	Observable*  level;
} Fader;

static void fader_free (AGlActor*);

static AGlActorClass fader_class = {0, "Fader", NULL, fader_free};

typedef struct {
	AGlShader    shader;
	struct {
		float    level;
		float    height;
		uint32_t colour;
	}            uniform;
} FaderShader;


static void fader_set_uniforms ();

static FaderShader fader_shader = {{NULL, NULL, 0, agl_null_uniforms, fader_set_uniforms, &fader_text}};

static void
fader_set_uniforms ()
{
	float colour[4] = {0.0, 0.0, 0.0, ((float)(fader_shader.uniform.colour & 0xff)) / 0x100};
	agl_rgba_to_float(fader_shader.uniform.colour, &colour[0], &colour[1], &colour[2]);
	glUniform4fv(glGetUniformLocation(fader_shader.shader.program, "colour"), 1, colour);

	glUniform1f(glGetUniformLocation(fader_shader.shader.program, "height"), fader_shader.uniform.height);
	glUniform1f(glGetUniformLocation(fader_shader.shader.program, "level"), fader_shader.uniform.level);
}


struct Grab
{
    int offset;
} grab;


static double
gain_to_fader_position (double g)
{
	if (g == 0.0) {
		return 0.0;
	}

	g = g * 2.0 / GAIN_MAX_6dB;
	return pow ((6.0 * log (g) / log (2.0) + 192.0) / 198.0, 8.0);
}


static double
fader_position_to_gain (double y)
{
	if (y == 0.0) {
		return 0.0;
	}

	y = exp (((pow (y, 1.0 / 8.0) * 198.0) - 192.0) / 6.0 * log (2.0));

	return y * GAIN_MAX_6dB / 2.0;
}


#ifdef UNUSED
#ifdef DEBUG
static void
test_position()
{
	float in[]  = {1.0};
	float out[] = {0.782};
	for(int i=0;i<G_N_ELEMENTS(in);i++){
		float a = in[i];
		float b = gain_to_fader_position(a);
		float c = fader_position_to_gain(b);
		dbg(0, "%f -> %f -> %f", a, b, c);
		if(ABS(b - out[i]) > 0.001) gerr("%i: expected %f, got %f", i, out[i], b);
	}
}
#endif
#endif


static AGlActor*
fader_new (AMChannel* channel)
{
	bool gl_fader__paint (AGlActor* actor)
	{
		Fader* fader = (Fader*)actor;

		fader_shader.uniform.level = gain_to_fader_position (fader->level->value.f); // 0.0 --> 1.0
		fader_shader.uniform.height = agl_actor__height(actor);
		agl_use_program((AGlShader*)&fader_shader);
		agl_rect(0, 0, agl_actor__width(actor), agl_actor__height(actor));

		agl_print(-10, -10, 0, 0xffffffff, "%.1f", am_gain2db(fader->level->value.f));

		return true;
	}

	void gl_fader_set_size (AGlActor* actor)
	{
		actor->region.x2 = agl_actor__width(actor->parent);
	}

	bool gl_fader__on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		void fader_send (AGlActor* actor, int y)
		{
			Fader* fader = (Fader*)actor;

			float val = ((float)agl_actor__height(actor) - y) / (float)agl_actor__height(actor);
#ifdef DEBUG
			float pos = val;
#endif

			val = fader_position_to_gain(val);
#ifdef DEBUG
			if(val != fader->level->value.f)
				dbg(1, "pos=%.2f gain=%.2f %.2fdB", pos, val, am_gain2db(val));
#endif
			observable_float_set(fader->level, val);
		}

		Fader* fader = (Fader*)actor;

		switch (event->type){
			case GDK_SCROLL:;
				GdkEventScroll* scroll = (GdkEventScroll*)event;
				float db = am_gain2db(fader->level->value.f) + (scroll->direction == GDK_SCROLL_UP ? 1.0 : -1.0);
				switch(scroll->direction){
					case GDK_SCROLL_UP:
					case GDK_SCROLL_DOWN:
						fader_send(actor, agl_actor__height(actor) * (1.0 - gain_to_fader_position(am_db2gain(db))));
						return AGL_HANDLED;
				}
				break;
			case GDK_ENTER_NOTIFY:
				break;
			case GDK_LEAVE_NOTIFY:
				break;
			case GDK_BUTTON_PRESS:
				switch(event->button.button){
					case 1:
						grab.offset = xy.y - (1.0 - gain_to_fader_position(fader->level->value.f)) * agl_actor__height(actor);
						if(ABS(grab.offset) > 10) grab.offset = 0;
						agl_actor__grab(actor);
						return AGL_HANDLED;
				}
				break;
			case GDK_MOTION_NOTIFY:

				if(actor_context.grabbed == actor){
					fader_send(actor, xy.y - grab.offset);
				}
				break;
			case GDK_BUTTON_RELEASE:
				switch(event->button.button){
					case 1:
						fader_send(actor, xy.y - grab.offset);
						return AGL_HANDLED;
				}
				break;
		}
		return NOT_HANDLED;
	}

	void fader_init (AGlActor* actor)
	{
		Fader* fader = (Fader*)actor;

		if(agl->use_shaders){
			if(!fader_shader.shader.program)
				agl_create_program((AGlShader*)&fader_shader);
			GdkColor* color = &actor->root->gl.gdk.widget->style->bg[GTK_STATE_SELECTED];
			fader_shader.uniform.colour = am_gdk_to_rgba(color);
		}

		void fader_on_level_change (Observable* o, AMVal level, gpointer _fader)
		{
			agl_actor__invalidate((AGlActor*)_fader);
		}

		observable_subscribe(fader->level, fader_on_level_change, fader);
	}

	Fader* fader = agl_actor__new(Fader,
		.actor = {
			.class    = &fader_class,
			.region   = (AGlfRegion){0, 0, 20, 160},
			.init     = fader_init,
			.paint    = agl->use_shaders ? gl_fader__paint : agl_actor__null_painter,
			.set_size = gl_fader_set_size,
			.on_event = gl_fader__on_event
		},
		.level = channel->level
	);

	return (AGlActor*)fader;
}


static void
fader_free (AGlActor* actor)
{
	Fader* fader = (Fader*)actor;

	observable_unsubscribe(fader->level, NULL, fader);

	g_free(actor);
}

