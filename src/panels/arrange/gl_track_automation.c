/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "agl/behaviours/vbo.h"

/*
 * Currently using straight lines.
 *
 * Although curves are more useful, they are not supported by the mixer
 * so would need to be rendered first. They would need to be stored independently
 * by the gui and hence would be lost in a multi-client situation.
 */
#undef ENABLE_CURVES

extern AGlMaterialClass aaline_class;

typedef struct {GLfloat x, y, z;} Pt3f;
typedef struct {Pt3f pt1, ctl1, ctl2, pt2;} Bp;

#define VBO ((VboBehaviour*)actor->behaviours[0])
#define VBO2 ((VboBehaviour*)actor->behaviours[1])
#define EXTRA_START_SEGMENT 1

typedef struct {
    AGlActor        actor;
    int             n_controls;
    int*            controls;      // index into vertex array for each parameter type, eg volume, pan
} TrackAutomationActor;

static void arr_gl_automation_free               (AGlActor*);
static void arr_gl_track_automation_set_vertices (AGlActor*);

static AGlActorClass track_automation_actor_class = {0, "Automation", (AGlActorNew*)arr_gl_track_automation, arr_gl_automation_free};


AGlActorClass*
arr_gl_track_automation_get_class ()
{
	if (!track_automation_actor_class.behaviour_classes[0]) {
		agl_actor_class__add_behaviour(&track_automation_actor_class, vbo_behaviour_get_class());
		agl_actor_class__add_behaviour(&track_automation_actor_class, vbo_behaviour_get_class());
	}

	return &track_automation_actor_class;
}


static AGlActor*
arr_gl_track_automation (ArrTrk* atr)
{
	arr_gl_track_automation_get_class ();

	bool arr_gl_track_automation_draw (AGlActor* actor)
	{
		TrackAutomationActor* a = (TrackAutomationActor*)actor;
		Arrange* arrange = actor->root->user_data;

		ArrTrk* atr = ((TrackActor*)actor->parent)->atr;
		AMTrack* track = atr->track;

		int n_controls = arr_track_count_active_controls(atr);
		if (a->n_controls != n_controls) {
			g_clear_pointer(&a->controls, g_free);
			int n_vertices = 0;

			if (n_controls) {
				a->controls = g_malloc0(sizeof(int) * n_controls);

				for (AyyiAutoType c=0;c<atr->auto_paths->len;c++) {
					a->controls[c] = n_vertices;

					if (AUTO_PATH(c) && !AUTO_PATH(c)->hidden) {
						AMCurve* cv = ((AMCurve**)&track->bezier)[c];
						n_vertices += cv->len;
					}
				}

				g_clear_pointer(&(VBO->data), g_free);
				VBO->data = g_malloc0(sizeof(AGlTQuad) * n_vertices);

				g_clear_pointer(&(VBO2->data), g_free);
				VBO2->data = g_malloc0(sizeof(AGlQuadVertex) * n_vertices);
			}

			VBO->len = n_vertices * sizeof(AGlTQuad) / sizeof(AGlQuadVertex);
			VBO->valid = false;

			VBO2->len = n_vertices;
			VBO2->valid = false;

			a->n_controls = n_controls;
		}

		if (!VBO->valid) arr_gl_track_automation_set_vertices (actor);

		glBindBuffer (GL_ARRAY_BUFFER, VBO->vbo);
		glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, 0, NULL);

		agl_scale (actor->program, wf_context_get_zoom(CGL->wfc), 1./*(float)atr->height*/);
		agl_translate_abs (actor->program, builder()->offset.x / wf_context_get_zoom(CGL->wfc), builder()->offset.y);

		glDrawArrays (GL_TRIANGLES, 0, VBO->len * AGL_V_PER_QUAD / 2);

		// hover
		AutoSelection* hover = &arrange->automation.hover;
		if (hover->track == atr) {
			if (hover->is_line) {
				agl->shaders.alphamap->uniform.fg_colour = 0xffffffff;
				((AGlShader*)agl->shaders.alphamap)->set_uniforms_((AGlShader*)agl->shaders.alphamap);

				int subpath = hover->subpath + EXTRA_START_SEGMENT;
				int bytes_offset = (a->controls[hover->type] + subpath) * sizeof(AGlTQuad);
				glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, 0, GINT_TO_POINTER(bytes_offset));
				glDrawArrays (GL_TRIANGLES, 0, AGL_V_PER_QUAD);
			}
		}

		// points
		glBindBuffer (GL_ARRAY_BUFFER, VBO2->vbo);
		glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		SET_PLAIN_COLOUR(agl->shaders.plain, 0xff0000ff);
		agl_use_program((AGlShader*)agl->shaders.plain);
		agl_scale (actor->program, 1., 1.);
		glDrawArrays (GL_TRIANGLES, 0, VBO2->len * AGL_V_PER_QUAD);

		if (hover->track == atr) {
			if (!hover->is_line) {
				SET_PLAIN_COLOUR(agl->shaders.plain, 0xffffffff);
				((AGlShader*)agl->shaders.plain)->set_uniforms_((AGlShader*)agl->shaders.plain);

				int bytes_offset = (a->controls[hover->type] / 2 + hover->subpath) * sizeof(AGlQuadVertex);
				glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, GINT_TO_POINTER(bytes_offset));
				glDrawArrays (GL_TRIANGLES, 0, AGL_V_PER_QUAD);
			}
		}

		agl_scale (actor->program, 1., 1.);
		agl_translate (actor->program, 0., 0.);

		return true;
	}

	void arr_gl_track_automation_set_state (AGlActor* actor)
	{
		agl_use_va (agl->vao);
		agl->shaders.alphamap->uniform.fg_colour = actor->colour;
		agl_use_material(agl->aaline);
	}

	void automation_set_size (AGlActor* actor)
	{
		actor->region = (AGlfRegion){
			.x1 = 0,
			.y1 = 0,
			.x2 = agl_actor__width(actor->parent),
			.y2 = agl_actor__height(actor->parent),
		};

		VBO->valid = false;
		VBO2->valid = false;
		actor->cache.valid = false;
	}

	void arr_gl_track_automation_init (AGlActor* actor)
	{
		if (!agl->aaline) agl->aaline = agl_aa_line_new();
	}

	return (AGlActor*)agl_actor__new (TrackAutomationActor, {
		.class = &track_automation_actor_class,
		.init = arr_gl_track_automation_init,
		.program = (AGlShader*)agl->shaders.alphamap,
		.colour = 0x33ff33ff,
		.paint = arr_gl_track_automation_draw,
		.set_size = automation_set_size,
		.set_state = arr_gl_track_automation_set_state,
	});
}


static void
arr_gl_automation_free (AGlActor* actor)
{
	TrackAutomationActor* a = (TrackAutomationActor*)actor;

	g_clear_pointer(&a->controls, g_free);
	g_free(actor);
}


static void
arr_gl_track_automation_set_vertices (AGlActor* actor)
{
	TrackAutomationActor* a = (TrackAutomationActor*)actor;
	Arrange* arrange = actor->root->user_data;
	ArrTrk* atr = ((TrackActor*)actor->parent)->atr;
	AMTrack* track = atr->track;

	for (AyyiAutoType c=0;c<AUTO_MAX;c++) {
		ArrAutoPath* aap = AUTO_PATH(c);
		if (!aap || aap->hidden) continue;

		AMCurve* cv = ((AMCurve**)&track->bezier)[c];
		BPath* path = cv->path;

		// TODO make xrange bigger if rendering to cache (actor->parent->cache.size_request)
		iRange xrange = {0, arr_px2samples(arrange, actor->region.x2 - actor->scrollable.x1)};

		AGlfPt scale = {
			.x = samples2px_nz(1),
			.y = vzoom(arrange) * atr->height
		};
		#define Y_TO_PX(Y) (scale.y * (100. - Y) / 100.)

		BPath* p = (BPath*)&path[0];

		BPath prev_path0 = {CURVETO, .x3 = p->x3 - 20.0, p->y3};
		BPath* pp = &prev_path0;
		for (int i=0;i<cv->len;i++) {
			p = (BPath*)&path[i];
#ifdef DEBUG
			if (p->code == CURVETO) pwarn("curve not supported! straight line expected.");
#endif
			if ((p->x3 >= xrange.start) && (pp->x3 <= xrange.end)) { // curve segment is visible.

				agl_aa_line_set_line ((AGlTQuad(*)[])VBO->data,
					a->controls[c] + i,
					(AGlfPt){pp->x3 * scale.x, Y_TO_PX(pp->y3)},
					(AGlfPt){p->x3 * scale.x, Y_TO_PX(p->y3)}
				);

				{
					float x = p->x3 * scale.x * wf_context_get_zoom(CGL->wfc);
					float y = Y_TO_PX(p->y3);

					agl_set_quad (VBO2->data,
						a->controls[c] / 2 + i,
						(AGlVertex){x - 2., y - 2.},
						(AGlVertex){x + 2., y + 2.}
					);
				}

				pp = p;
			}
		}
	}

	glBindBuffer (GL_ARRAY_BUFFER, VBO->vbo);
	glBufferData (GL_ARRAY_BUFFER, sizeof(AGlQuadVertex) * VBO->len, VBO->data, GL_STATIC_DRAW);
	VBO->valid = true;

	glBindBuffer (GL_ARRAY_BUFFER, VBO2->vbo);
	glBufferData (GL_ARRAY_BUFFER, sizeof(AGlQuadVertex) * VBO2->len, VBO2->data, GL_STATIC_DRAW);
	VBO2->valid = true;
}
