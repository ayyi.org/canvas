/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include <config.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

#include <glib-object.h>
#include "debug/debug.h"
#include "model/model_types.h"
#include "model/song.h"
#include "support.h"
#include "icon.h"
#include "glpanel.h"
#include "statusbar.h"
#include "windows.h"
#include "window.h"

Observable* windows_ = NULL;

extern void song_save   ();
extern bool config_save ();
extern void app_quit    ();

enum  {
	AYYI_WINDOW_0_PROPERTY,
	AYYI_WINDOW_NUM_PROPERTIES
};

enum  {
	AYYI_WINDOW_PANEL_ADDED_SIGNAL,
	AYYI_WINDOW_PANEL_PARENTED_SIGNAL,
	AYYI_WINDOW_NUM_SIGNALS
};
static guint    ayyi_window_signals[AYYI_WINDOW_NUM_SIGNALS] = {0};

static gpointer ayyi_window_parent_class = NULL;

static GObject*    ayyi_window_constructor    (GType, guint n_construct_properties, GObjectConstructParam*);
static void        ayyi_window_finalize       (GObject*);
static GType       ayyi_window_get_type_once  (void);

static AyyiWindow* window__new_from_dock      (GdlDock*);
static void        window__on_dock_item_added (GdlDockMaster*, gpointer, gpointer);
static void        window__add_panel          (AyyiWindow*, AyyiPanel*);
static void        window__remove_panel       (AyyiWindow*, AyyiPanel*);
static void        window__free               (AyyiWindow*);
static gint        window__on_close_request   (GtkWidget*, GdkEvent*, AyyiWindow*);
static void        window__on_progress        (GObject*, double, AyyiWindow*);
static AyyiWindow* window__lookup_by_dock     (GdlDock*);
static GList*      window__get_dock_panels    (GdlDock*);
static void        window__add_panels_for_dock(AyyiWindow*, GdlDock*);
static void        panel__on_reparented       (GdlDockItem*, GdlDockObject*, GdlDockPlacement*, GValue*, gpointer data);


static void
ayyi_window_class_init (AyyiWindowClass* klass, gpointer klass_data)
{
	ayyi_window_parent_class = g_type_class_peek_parent (klass);

	G_OBJECT_CLASS (klass)->constructor = ayyi_window_constructor;
	G_OBJECT_CLASS (klass)->finalize = ayyi_window_finalize;

	ayyi_window_signals[AYYI_WINDOW_PANEL_ADDED_SIGNAL] = g_signal_new ("panel-added", AYYI_TYPE_WINDOW, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);
	ayyi_window_signals[AYYI_WINDOW_PANEL_PARENTED_SIGNAL] = g_signal_new ("panel-parented", AYYI_TYPE_WINDOW, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);

	windows_ = array_observable_new();
	klass->instances = (ArrayObservable*)windows_;
}


AyyiWindow*
ayyi_window_construct (GType object_type)
{
	AyyiWindow* window = (AyyiWindow*) g_object_new (object_type, NULL);

	AyyiWindowClass* W = AYYI_WINDOW_GET_CLASS(window);

	if(!windows_->value.p){
		g_signal_connect(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(W->master_dock)->master), "dock-item-added", G_CALLBACK(window__on_dock_item_added), NULL);
	}

	gtk_widget_show(window->vbox = gtk_vbox_new(0, 0));
	gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(window->vbox));

	am_song__connect("progress", G_CALLBACK(window__on_progress), window);

	g_signal_connect(G_OBJECT(window), "delete-event", G_CALLBACK(window__on_close_request), window);

	array_observable_add(windows_, window);

	return window;
}


AyyiWindow*
ayyi_window_new ()
{
	return ayyi_window_construct (AYYI_TYPE_WINDOW);
}


static GObject*
ayyi_window_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_properties)
{
	AyyiWindow* window = (AyyiWindow*)G_OBJECT_CLASS (ayyi_window_parent_class)->constructor (type, n_construct_properties, construct_properties);

	return (GObject*)window;

}


static void
ayyi_window_instance_init (AyyiWindow* self, gpointer klass)
{
}


static void
ayyi_window_finalize (GObject* obj)
{
	AyyiWindow* window = G_TYPE_CHECK_INSTANCE_CAST (obj, AYYI_TYPE_WINDOW, AyyiWindow);

	shell_menu_free(window);

	array_observable_remove(windows_, window);

	g_signal_handlers_disconnect_matched (song, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, window__on_progress, window);

	G_OBJECT_CLASS (ayyi_window_parent_class)->finalize (obj);
}


static GType
ayyi_window_get_type_once ()
{
	static const GTypeInfo g_define_type_info = { sizeof (AyyiWindowClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) ayyi_window_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (AyyiWindow), 0, (GInstanceInitFunc) ayyi_window_instance_init, NULL };
	GType ayyi_window_type_id = g_type_register_static (GTK_TYPE_WINDOW, "AyyiWindow", &g_define_type_info, 0);
	return ayyi_window_type_id;
}


GType
ayyi_window_get_type ()
{
	static volatile gsize ayyi_window_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&ayyi_window_type_id__volatile)) {
		GType ayyi_window_type_id = ayyi_window_get_type_once ();
		g_once_init_leave (&ayyi_window_type_id__volatile, ayyi_window_type_id);
	}
	return ayyi_window_type_id__volatile;
}


/*
 *  This is for new items. Is only called once for each panel.
 */
static void
window__on_dock_item_added (GdlDockMaster* master, gpointer item, gpointer _)
{
	g_return_if_fail(item);

	AyyiWindow* window = NULL;

	dbg(1, "-> item added! %s", ((GdlDockObject*)item)->name);

	GtkWidget* top = gtk_widget_get_toplevel((GtkWidget*)item);

	if(GDL_IS_DOCK(item)){
		window = window__lookup_by_dock((GdlDock*)item);
		if(GTK_IS_WINDOW(top)){
			window__add_panels_for_dock (window, (GdlDock*)item);
		}else{
			if(!window){
				AyyiWindow* window = window__new_from_dock((GdlDock*)item);
				window__add_panels_for_dock (window, (GdlDock*)item);
			}
		}
	}else if(GDL_IS_DOCK_PANED(item)){
		// ignore these - wait for the top level dock

	}else{
		// why checking the parent?
		GType parent_type = G_TYPE_FROM_CLASS(g_type_class_peek_parent(G_OBJECT_GET_CLASS(item)));
		if(parent_type == AYYI_TYPE_PANEL){
			AyyiPanel* panel = item;

			GtkWidget* top = gtk_widget_get_toplevel((GtkWidget*)item);
			if(GDL_IS_DOCK(top)){
				// dock is not parented

				window = window__lookup_by_dock((GdlDock*)top);
				if(!window){
					window = window__new_from_dock((GdlDock*)top);
					gtk_widget_show_all((GtkWidget*)window);

					GList* panels = gtk_container_get_children((GtkContainer*)top);
					if(panels){
						AyyiPanel* panel = panels->data;
						if(((GdlDockObject*)panel)->long_name)
							gtk_window_set_title(GTK_WINDOW(window), ((GdlDockObject*)panel)->long_name);
						g_list_free(panels);
					}
				}
			}
			else if(GTK_IS_WINDOW(top))
				window = (AyyiWindow*)top;
			else if(GDL_IS_DOCK_PANED(top))
				// dont do anything until the dock is parented to the window
				return;
			else
				dbg(0, "unexpected item type");

			window__add_panel(window, panel);
		}
	}
}


AyyiWindow*
window__new_floating (AyyiPanelClass* class)
{
	void window__add_panel_to_dock (AyyiWindow* window, AyyiPanel* panel, GdlDockPlacement placement, GdlDockObject* parent)
	{
		// Add the new dock item to the widget tree

		if(parent) g_return_if_fail(GDL_IS_DOCK_OBJECT(parent));

		dbg(2, "name=%s parent=%s", GDL_DOCK_OBJECT(panel)->name, parent ? GDL_DOCK_OBJECT(parent)->name : "<empty>");
		if(parent && GDL_IS_DOCK_ITEM(parent)){
			dbg(3, "path1............");

			// can happen if docking of previous panels failed.
			g_return_if_fail(gdl_dock_object_get_parent_object(parent));

			GDL_DOCK_OBJECT_CLASS(AYYI_PANEL_GET_CLASS(panel))->dock (parent, GDL_DOCK_OBJECT(panel), placement, NULL);
		}
		else if(parent && (g_list_length(window->dock->panels) > 1)){
			dbg(2, "path2............");
			gdl_dock_item_dock2(parent, GDL_DOCK_ITEM(panel), placement);
		}
		else{
			dbg(2, "path3...........");
			gdl_dock_add_item (GDL_DOCK(window->dock), GDL_DOCK_ITEM(panel), placement);
		}
	}

	AyyiWindow* window = windows__open_new_window();
	AyyiPanel* panel = g_object_new(G_OBJECT_CLASS_TYPE(class), NULL);

	g_signal_emit(window, ayyi_window_signals[AYYI_WINDOW_PANEL_ADDED_SIGNAL], 0, panel);
	window__add_panel(window, panel);

	GtkOrientation orientation = GTK_ORIENTATION_HORIZONTAL;
	GdlDockPlacement placement = (orientation == GTK_ORIENTATION_VERTICAL) ? GDL_DOCK_BOTTOM : GDL_DOCK_RIGHT;
	GdlDockObject* parent = NULL;
	window__add_panel_to_dock(window, panel, placement, parent);

	ayyi_panel_on_new(panel, NULL, GTK_ORIENTATION_HORIZONTAL); // set size
									// TODO this is now also called in an idle
	if(AYYI_PANEL_GET_CLASS(panel)->on_ready) AYYI_PANEL_GET_CLASS(panel)->on_ready(panel);

#ifdef DEBUG
	if(_debug_) windows__print_items();
#endif

	return window;
}


/*
 *  Update application level state following a user drag and drop operation
 */
static AyyiWindow*
window__new_from_dock (GdlDock* dock)
{
	PF;
	AyyiWindow* window = (AyyiWindow*)gdl_dock_get_window(dock);
	window->dock = dock;

	if(!((GtkWidget*)dock)->parent){
		pwarn("dock has no parent - NEVER GET HERE");
		gtk_box_pack_start(GTK_BOX(window->vbox), (GtkWidget*)dock, EXPAND_TRUE, FILL_TRUE, 0);
	}

	void dock_finalize_notify (gpointer _window, GObject* was)
	{
		// the window may have been closed outside of our control.
		// In this case it is important to imediately remove all invalid references

		PF;
		AyyiWindow* window = _window;

		while(window->dock->panels)
			window__remove_panel(window, window->dock->panels->data);
		window__free(window);
	}
	g_object_weak_ref((GObject*)dock, dock_finalize_notify, window);

	return window;
}


static void
window__add_panel (AyyiWindow* window, AyyiPanel* panel)
{
	g_return_if_fail(window);

	dbg(1, "adding: %s to: %s", ((GdlDockObject*)panel)->long_name, ((GdlDockObject*)window->dock)->long_name);

	g_signal_emit(window, ayyi_window_signals[AYYI_WINDOW_PANEL_PARENTED_SIGNAL], 0, panel);

	AyyiPanelClass* panel_class = AYYI_PANEL_CLASS(G_OBJECT_GET_CLASS(panel));
	if(panel_class->has_statusbar && !window->statusbar){
		window->statusbar = (Statusbar*)statusbar__new(window);
		gtk_box_pack_end((GtkBox*)window->vbox, (GtkWidget*)window->statusbar, EXPAND_FALSE, FILL_FALSE, 0);
	}

	// add menu if panel type requests it, or is the first window
	if(!window->menu.window && (((ArrayObservable*)windows_)->array->len == 1 || panel_class->has_menubar)){
		GtkWidget* menu = shell_menu_new(window);
		if(menu){
			gtk_box_pack_start(GTK_BOX(window->vbox), menu, EXPAND_FALSE, FILL_FALSE, 0);
			gtk_box_reorder_child(GTK_BOX(window->vbox), menu, 0);
		}
	}

	g_signal_connect (GDL_DOCK_OBJECT(panel), "dock", G_CALLBACK (panel__on_reparented), panel);

	void panel__on_parent_set (GtkWidget* widget, GtkWidget* old_parent, gpointer user_data)
	{
		// during a move, a widget is first removed, then re-added

#ifdef DEBUG
		GdlDock* find_parent_dock (GtkWidget* widget)
		{
			while((widget = gtk_widget_get_parent(widget))){
				if(GDL_IS_DOCK(widget)) return (GdlDock*)widget;
			}
			return NULL;
		}
#endif

		AyyiWindow* lookup_shell_by_widget (GtkWindow* window)
		{
			GList* l = windows;
			for(;l;l=l->next){
				AyyiWindow* w = l->data;
				if((GtkWindow*)w == window) return w;
			}
			return NULL;
		}

		gboolean on_parented_idle_add_to_window (gpointer _widget)
		{
			GtkWidget* widget = _widget;
			g_return_val_if_fail(AYYI_IS_PANEL(widget), G_SOURCE_REMOVE);

			AyyiWindow* window = lookup_shell_by_widget((GtkWindow*)gtk_widget_get_toplevel(widget->parent));
			if(!window){
				GdlDockObject* dock_parent = gdl_dock_object_get_parent_object((GdlDockObject*)widget);
				if(GDL_IS_DOCK(dock_parent)){
					pwarn("NEVER GET HERE floating window with no window");
					// it looks like a new floating window has been created that does not yet have a shell
					window = window__new_from_dock(GDL_DOCK(dock_parent));
				}
			}
			if(window){
				dbg(2, "has parent: window=%s", ((GdlDockObject*)window->dock)->long_name);
				window__add_panel(window, (AyyiPanel*)widget);
			}
			else dbg(0, "*** shell not found. cannot add. toplevel=%s parent_dock=%p", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(gtk_widget_get_toplevel(widget->parent))), find_parent_dock(widget));

			return G_SOURCE_REMOVE;
		}

		if(widget->parent){
			// parent is likely to be a GtkPaned or some other container widget
			dbg(2, "has parent: widget=%s parent=%s (%s)", ((GdlDockObject*)widget)->long_name, GDL_IS_DOCK_OBJECT(widget->parent) ? ((GdlDockObject*)widget->parent)->long_name : "", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(widget->parent)));

			// sometimes has a root window at this point, sometimes not.
			GtkWidget* toplevel = gtk_widget_get_toplevel(widget->parent);
			if(GTK_IS_WINDOW(toplevel)){
				on_parented_idle_add_to_window(widget);
			}else{
				g_idle_add(on_parented_idle_add_to_window, widget);
			}

		}else{
			// for example toplevel window has been closed
			dbg(2, "widget does not have parent: %s", ((GdlDockObject*)widget)->long_name);
			AyyiWindow* old_shell = lookup_shell_by_widget((GtkWindow*)gtk_widget_get_toplevel(old_parent));
			if(old_shell){
				window__remove_panel(old_shell, (AyyiPanel*)widget);
				((AyyiPanel*)widget)->window = NULL;
			}
		}
	}
	// parent-set is a GtkWidget signal sent when the panel is parented
	g_signal_connect (panel, "parent-set", G_CALLBACK (panel__on_parent_set), panel);

	void window_on_panel_finalize (gpointer _window, GObject* was)
	{
		PF;
		AyyiWindow* window = _window;

		if(was == (GObject*)window->focussed_panel){
			window->focussed_panel = NULL;
		}

		if(g_list_find(windows, window))
			window__remove_panel(window, (AyyiPanel*)was);
	}
	g_object_weak_ref((GObject*)panel, window_on_panel_finalize, window);
}


static void
window__remove_panel (AyyiWindow* window, AyyiPanel* panel)
{
#if 0 // objects may be been destroyed. careful trying to access them
	dbg(1, "shell=%s panel=%s", GDL_DOCK_OBJECT(shell->dock)->long_name, GDL_DOCK_OBJECT(panel)->long_name);
#endif

	PF;
	g_return_if_fail(panel);
	g_return_if_fail(g_list_find(windows, window));

	if(window->focussed_panel == panel)
		ayyi_panel_on_blur(window->focussed_panel);
}


static void
window__free (AyyiWindow* window)
{
	PF;
	g_return_if_fail(window);
	g_return_if_fail(g_list_find(windows, window));

	windows = g_list_remove(windows, window);
}


void
window__destroy_panel (AyyiWindow* window, AyyiPanel* panel)
{
	PF;
	g_return_if_fail(panel);

	dbg(1, "destroying panel widget... %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));

	if(window->dock->panels)
		gtk_widget_destroy(GTK_WIDGET(panel));
	else
		gtk_widget_destroy((GtkWidget*)window);
}


void
window__add_panel_from_menu (AyyiWindow* window, AyyiPanelClass* class)
{
	AyyiWindowClass* W = AYYI_WINDOW_GET_CLASS(window);

	AyyiPanel* parent = window->focussed_panel;
	if(!parent){ // sometimes there genuinely can be no focussed widget
		parent = window->dock->panels->data;
	}

	AyyiPanel* panel = g_object_new(G_OBJECT_CLASS_TYPE(class), NULL);

	gdl_dock_master_add(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(W->master_dock)->master), (GdlDockObject*)panel);
	GdlDockObject* item = (GdlDockObject*)parent;

	GtkAllocation allocation;
	gtk_widget_get_allocation((GtkWidget*)item, &allocation);
	GdlDockPlacement placement = allocation.width > allocation.height ? GDL_DOCK_RIGHT : GDL_DOCK_BOTTOM;

	GDL_DOCK_OBJECT_GET_CLASS(item)->dock(item, (GdlDockObject*)panel, placement, NULL);

	am_promise_resolve(GDL_DOCK_ITEM(panel)->parented, &(PromiseVal){0});
	g_signal_emit(window, ayyi_window_signals[AYYI_WINDOW_PANEL_ADDED_SIGNAL], 0, panel);
	window__add_panel (window, panel);
}


/*
 *  Return TRUE if the window contains a visible panel of the given type.
 */
bool
window__has_panel_type (AyyiWindow* window, PanelType type)
{
	int get_page (GdlDockNotebook* container, GdlDockItem* child)
	{
		// return the position of the child within a notebook.

		gboolean found = false;
		int n = 0;
		GList* children = gtk_container_get_children(GTK_CONTAINER(container));
		if(children){
			GList* i = children;
			for(;i;i=i->next){
				GtkWidget* w = i->data;
				if(w == (GtkWidget*)child) {found = true; break; };
				n++;
			}
			g_list_free(children);
		}
		return found ? n : -1;
	}

	panel_foreach {
		if(G_OBJECT_TYPE(panel) == type){
			GdlDockItem* item = (GdlDockItem*)panel;
			GdlDockObject* parent = gdl_dock_object_get_parent_object(GDL_DOCK_OBJECT(item));
			if(GDL_IS_DOCK_NOTEBOOK(parent)){
				int visible_tab;
				g_object_get(parent, "page", &visible_tab, NULL);
				dbg(2, "is notebook! page=%i panel=%i", visible_tab, get_page(GDL_DOCK_NOTEBOOK(parent), item));
				if(get_page(GDL_DOCK_NOTEBOOK(parent), item) != visible_tab) dbg(0, "not visible!");
				if(get_page(GDL_DOCK_NOTEBOOK(parent), item) != visible_tab) return false;
			}
			return true;
		}
	} end_panel_foreach;

	return false;
}


/*
 *  Return a newly allocated list of GdlDockItem's.
 */
GList*
window__get_dock_items (AyyiWindow* window)
{
	// -perhaps gdl-dock has a better way of doing this?

	GList* ret = NULL;
	GList* l = gdl_dock_get_named_items(window->dock); // this gets dock items for ALL docks, but only if they are named.
	for(GList* i=l;i;i=i->next){
		GdlDockItem* di = i->data;
		int depth = 0;
		//gdl_dock_item_print(GDL_DOCK_ITEM(di));
		GdlDockObject* parent = gdl_dock_object_get_parent_object(GDL_DOCK_OBJECT(di));
		depth++;
		if(GDL_IS_DOCK_ITEM(parent)){
			parent = gdl_dock_object_get_parent_object(parent);
			depth++;
			if(!GDL_IS_DOCK(parent)){
				if(GDL_IS_DOCK_ITEM(parent)){

					parent = gdl_dock_object_get_parent_object(parent);
					depth++;
					if(GDL_IS_DOCK_ITEM(parent)){
						parent = gdl_dock_object_get_parent_object(parent);
						depth++;
						if(!GDL_IS_DOCK(parent)){
							pwarn("(%s) parent4 is not Dock! parent=%p parent->name=%s", GDL_DOCK_OBJECT(di)->name, parent, parent->name);
						}
						else dbg(2, "item is at depth: %i", depth);
						//TODO we quickly get to this level of nestedness - need to add more recursion.
					}
				}
			}
		}
		else if(GDL_IS_DOCK_MASTER(parent)){
			dbg(0, "parent is dock-master.");
			continue;
		}
		else if(GDL_IS_DOCK_PLACEHOLDER(parent)){
			dbg(0, "parent is placeholder.");
			continue;
		}
		else if(GDL_IS_DOCK(parent)){
		}
		else if(!parent){
			pwarn("parent null. scheduled for deletion? (item=%s)", GDL_DOCK_OBJECT(di)->name);
			continue;
		}
		else {
			pwarn("unknown parent type. item=%s parent=%s", GDL_DOCK_OBJECT(di)->name, parent ? parent->name : "");
			continue;
		}

		dbg(2, "parent='%s'", parent ? parent->name : "");
		if(parent == GDL_DOCK_OBJECT(window->dock)) ret = g_list_append(ret, di);
	}
	g_list_free(l);
	dbg(2, "n_items=%i", g_list_length(ret));

	return ret;
}


/*
 *  Callback for button press on toplevel window.
 *  Connected to toplevel window close buttons.
 */
static gint
window__on_close_request (GtkWidget* widget, GdkEvent* event, AyyiWindow* window)
{
	PF;

	if(g_list_length(windows) == 1){
		song_save();
		config_save();
	}

	GList* panels = g_list_copy(window->dock->panels);

	dbg(1, "shell has %i panels", g_list_length(panels));
	if(panels){
		GList* l = panels;
		for(;l;l=l->next){
			AyyiPanel* panel = l->data;
			window__destroy_panel(window, panel);
		}
		g_list_free(panels);
	}

	PF_DONE;

#ifdef DEBUG
	if(_debug_ > 1) windows__print_items();
#endif

	if(!windows) app_quit();

	return NOT_HANDLED;
}


/*
 *  Updates shell/panel properties following a re-dock
 *
 *  @param requestor - the item that has been reparented.
 */
static void
panel__on_reparented (GdlDockItem* item, GdlDockObject* requestor, GdlDockPlacement* position, GValue* other_data, gpointer _panel)
{
	AyyiPanel* dest_panel = (AyyiPanel*)_panel;
	AyyiPanel* src_panel = (AyyiPanel*)requestor;

#ifdef DEBUG
	AyyiWindow* dest_window =
#endif
	dest_panel->window = (AyyiWindow*)gtk_widget_get_toplevel((GtkWidget*)item);
	dbg(0, "item='%s' requestor='%s' dest_window='%s'", GDL_DOCK_OBJECT(item)->long_name, requestor->long_name, GDL_DOCK_OBJECT(dest_window->dock)->long_name);

	// update all dock_item lists
	AyyiWindow* src_window = src_panel ? src_panel->window : NULL;
	dbg(2, "src_panel='%s'", src_panel ? GDL_DOCK_OBJECT(src_window)->long_name: "");
	if(src_window)
		window__remove_panel(src_window, AYYI_PANEL(requestor));
}


static void
window__on_progress (GObject* _song, double val, AyyiWindow* window)
{
	// FIXME using progressbar can cause windows to become wider (eg pool window)

	g_return_if_fail(window);

	if(window->statusbar->progress && ((GtkWidget*)window)->allocation.width > 480){
		if(!GTK_WIDGET_VISIBLE(window->statusbar->progress)) gtk_widget_show(window->statusbar->progress);
		gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(window->statusbar->progress), val);
	}
}


/*
 *  Normally the window can be obtained using gtk_widget_get_toplevel()
 *  but during object creation the dock may not be parented
 */
static AyyiWindow*
window__lookup_by_dock (GdlDock* dock)
{
	for(GList* l=windows;l;l=l->next){
		if(((AyyiWindow*)l->data)->dock == dock) return l->data;
	}
	return NULL;
}


/*
 *  Return newly allocated list of AyyiPanel*
 */
static GList*
window__get_dock_panels (GdlDock* dock)
{
	typedef struct {
		GList* panels;
	} C;

	C c = {0,};

	void each_ (GdlDockObject* object, gpointer _c)
	{
		C* c = _c;
		//windows__print_dock_object(object, "  ");
		GType parent_type = G_TYPE_FROM_CLASS(g_type_class_peek_parent(G_OBJECT_GET_CLASS(object)));
		if(parent_type == AYYI_TYPE_PANEL || parent_type == TYPE_GL_PANEL){
			c->panels = g_list_append(c->panels, object);
		}

		if(gdl_dock_object_is_compound (object)){
			gtk_container_foreach (GTK_CONTAINER (object), (GtkCallback)each_, c);
		}
	}
	each_((GdlDockObject*)dock, &c);

	return c.panels;
}


static void
window__add_panels_for_dock (AyyiWindow* window, GdlDock* dock)
{
	GList* panels = window__get_dock_panels(dock);
	for(GList* l=panels;l;l=l->next){
		g_signal_emit(window, ayyi_window_signals[AYYI_WINDOW_PANEL_ADDED_SIGNAL], 0, l->data);
		window__add_panel(window, l->data);
	}
	g_list_free(panels);
}

