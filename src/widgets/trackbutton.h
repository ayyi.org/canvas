/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __gtk_tbutton_h__
#define __gtk_tbutton_h__

#include <gtk/gtk.h>

enum{
  TEXT_POSITION_NONE,
  TEXT_POSITION_LEFT,
  TEXT_POSITION_TOP
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



#define GTK_TBUTTON(obj)          GTK_CHECK_CAST (obj, track_button_get_type(), TButton)
#define GTK_TBUTTON_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, track_button_get_type(), GtkSkelClass)
#define GTK_IS_TBUTTON(obj)       GTK_CHECK_TYPE (obj, track_button_get_type())


typedef struct _TButton         TButton;
typedef struct _GtkTButtonClass GtkTButtonClass;

struct _TButton
{
  GtkButton button;

  struct {
    GdkPixbuf* normal;
    GdkPixbuf* active;
  }         pixbufs;

  gboolean  active;       // button toggle state: 0 normal, 1 active.
  gboolean  trk_selected; // true if the widget's parent row is selected.
  gboolean  trk_muted;

  GdkColor  colour;       // the colour of the parent Track/row - defaults to GTK_STATE_NORMAL colour.
};

struct _GtkTButtonClass
{
  GtkButtonClass parent_class;
};


GtkWidget* track_button_new                  (gchar* svg_file_name);
GtkType    track_button_get_type             (void);
void       track_button_set_active           (GtkWidget*);
void       track_button_set_not_active       (GtkWidget*);
void       track_button_set_track_selected   (GtkWidget*);
void       track_button_set_track_unselected (GtkWidget*);
void       track_button_set_track_muted      (GtkWidget*, gboolean);
#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TBUTTON_H__ */
