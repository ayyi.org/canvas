using GLib;
using Gdk;
using Ayyi;

static int file_idx = 0;
static string[] files = null;
static bool force = false;
const OptionEntry[] entries = 
{
	{ "force", 'f', 0, OptionArg.NONE, ref force, "delete any parts using the file", "N" },
	{ ""/*G_OPTION_REMAINING*/, 0, 0, OptionArg.FILENAME_ARRAY, ref files, null, "FILE" },
	{ null }
};

public class Ayyi.DeleteAudioFileClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		file_idx = -1;

		OptionContext context = new OptionContext("Ayyi Delete File");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}
		if(!(bool)files){
			print("file not specified\n");
			return 1;
		}
		file_idx = files[0].to_int();
		print("deleting file %i ...\n", file_idx);

		app = new DeleteAudioFileClient();

		if(!app.run(((DeleteAudioFileClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		AyyiFilesource* file = AyyiSong.get_filesource(file_idx);

		if(!(bool)file){
			print("file not found\n");
			print_file_numbers();
			abort();
		}

		Model.PoolItem* pool_item = AM.pool__get_item_from_idx(file_idx);
		assert((bool)pool_item);

		//check for parts
		FilterIterator<AM.Part*> i = new FilterIterator<AM.Part*>((Model.Collection*)song->parts, (_part)=>{
			AM.Part* part = _part;
			if(part->pool_item == pool_item) print("  %s\n", (string)part->name);
			return part->pool_item == pool_item;
		});
		//TODO use AM.part__has_pool_item as below
		//     -in order to do this, we need to stop vala using the user_data for 'this' - change Filter definition to be use_target=false?
		//FilterIterator<AM.Part*> i = new FilterIterator<AM.Part*>((Model.Collection*)song->parts, (Filter)AM.part__has_pool_item);

		AM.Part* part = (AM.Part*)i.next();
		if((bool)part){
			if(force){
				print("TODO --force\n");
				abort();
			}else{
				print("file is in use. Cannot remove. Use --force to override\n");
				abort();
			}
			return;
		}

		print("deleting: %s\n", (string)file->name);

		AyyiSong.delete_item(&((SongService*)ayyi.service)->song->filesources, (Ayyi.Item*)file, (obj, error) => {
			log_print(LogType.OK, "done.");
			quit();
		});
	}

	private void print_file_numbers()
	{
		print("available parts:\n");

		AyyiRegion* part = null;
		while((part = ayyi_song__audio_region_next(part)) != null){
			print("%i ", part->shm_idx);
		}
		print("\n");
	}

	public void abort()
	{
		log_print(LogType.FAIL, "aborted.");
		app.quit();
	}
}


