/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __arr_utils_h__
#define __arr_utils_h__

typedef double ArrZoomedCanvasDouble; // a whole-canvas coordinate - ie a screen coordinate with scroll-position added.

ArrTrk*        arr_px2track                (Arrange*, int y);
TrackDispNum   arr_px2trk                  (Arrange*, int y);

double         arr_px2bar                  (Arrange*, double px);
double         arr_px2beat                 (Arrange*, double px);
float          arr_mu2px                   (Arrange*, long long mu);
double         arr_beats2px                (Arrange*, int beats);
double         arr_subs2px                 (Arrange*, int subs);
double         arr_samples2px              (Arrange*, unsigned long long samples);
double         arr_pos2px                  (Arrange*, GPos*);
double         arr_spos2px                 (Arrange*, AyyiSongPos*);
uint64_t       arr_px2mu                   (Arrange*, double px_zoomed);
int32_t        arr_px2samples              (Arrange*, double px_zoomed);
uint32_t       arr_samples_per_pix         (Arrange*);
double         samples2px_nz               (uint64_t samples);
uint32_t       px2samples_nz               (double px_nonzoomed);
uint64_t       px2mu_nz                    (double px_nonzoomed);
void           px2pos_nz                   (GPos*, double px);
double         pos2px_nz                   (GPos*);
double         spos2px_nz                  (AyyiSongPos*);
double         beats2px_nz                 (int beats);

double         arr_snap                    (Arrange*, double px);
void           arr_snap_px2pos             (Arrange*, GPos*, double px);
void           arr_snap_px2spos            (Arrange*, AyyiSongPos*, double px);
void           arr_px2pos                  (Arrange*, GPos*, double px);
void           arr_px2spos                 (Arrange*, AyyiSongPos*, double px);

int            arr_px2tick_rel             (Arrange*, double px);
void           arr_px2bbs                  (Arrange*, ArrZoomedCanvasDouble, char*);
void           arr_px2bbst                 (Arrange*, ArrZoomedCanvasDouble, char*);
double         arr_px2beat_nearest         (Arrange*, double px);
int            arr_px2subbeat_rel          (Arrange*, double px);
int            arr_px2subbeat_rel_nearest  (Arrange*, double px);

void           arr_bbst2statusbar          (Arrange*, double x, const char*);
void           arr_statusbar_printf        (Arrange*, int n, char* fmt, ...);
void           arr_statusbar_print_pos     (Arrange*, int n, GPos*);
void           arr_statusbar_print_xpos    (Arrange*, int n, double);
gboolean       arr_statusbar_print_idle    (gpointer data);

Size           arr_canvas_viewport_size    (Arrange*);
int            arr_get_viewport_left_px    (Arrange*);
double         arr_get_viewport_right_px   (Arrange*);
bool           arr_part_is_in_viewport     (Arrange*, AMPart*);

void           arr_zoom_to                 (Arrange*, double x1, double y1, double x2, double y2);
void           arr_zoom_to_1               (Arrange*, AyyiSongPos* x1, AyyiSongPos* x2, TrackDispNum, TrackDispNum, Pti* padding);
void           arr_zoom_to_song            (Arrange*);
void           arr_zoom_set                (Arrange*, float hzoom, float vzoom);
void           arr_set_hzoom               (AyyiPanel*, float);
void           arr_set_vzoom               (AyyiPanel*, float);
double         get_hzoom_from_adj          (double adj_val);
double         hzoom_get_adj_val           (Arrange*);
double         vzoom_get_adj_val           (Arrange*);
void           arr_zoom_history_back       (GtkWidget*, gpointer);
void           arr_zoom_to_selection       (Arrange*);
void           arr_scroll_left_k           (GtkWidget*, gpointer pix);
void           arr_scroll_right_k          (GtkWidget*, gpointer pix);
void           arr_scroll_up_k             (GtkWidget*, gpointer pix);
void           arr_scroll_down_k           (GtkWidget*, gpointer pix);
void           arr_zoom_song               (Arrange*);

void           arr_set_background          (Arrange*, int palette_num);

double         arr_get_spp_px              (Arrange*);

#ifdef __arrange_private__
void           arr_part_selection_reset    (Arrange*, bool);
void           arr_part_selection_replace  (Arrange*, GList*, bool);
void           arr_part_selection_add      (Arrange*, AMPart*);
#endif
void           arr_part_selection_add_range(Arrange*, const AMPart* new_part, const AMPart* old_part);
void           arr_part_selection_remove   (Arrange*, AMPart*);
bool           arr_part_is_selected        (Arrange*, const AMPart*);
void           arr_parts_select_by_track   (Arrange*, TrackDispNum);

int            arr_coords_get_note         (Arrange*, int x, int y);

void           kzoom_in_hor_cb             (GtkAccelGroup*, gpointer);
void           kzoom_out_hor_cb            (GtkAccelGroup*, gpointer);

void           arr_peak_gain_inc           (Arrange*);
void           arr_peak_gain_dec           (Arrange*);

void           arr_k_next_note             (GtkAccelGroup*, Arrange*);
void           arr_k_prev_note             (GtkAccelGroup*, Arrange*);

int            arr_get_n_gl_windows        ();

double         arr_get_selection_size      (Arrange*, GList* selectionlist, int* n_tracks, double* xpos, int* t0);

GtkWidget*     ch_menu_init                ();

void           arr_cursor_reset            (Arrange*, GdkWindow*);

void           arr_canvas_drag_received    (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer);

void           arr_set_song_title          ();

#define BEATS_PER_SECOND (((AyyiSongService*)ayyi.service)->song->bpm / 60.0)

#endif
