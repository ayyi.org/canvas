/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __menu_h__
#define __menu_h__

typedef struct _ShellMenus ShellMenus;
typedef struct _EditMenu EditMenu;

GtkWidget*    shell_menu_new           (AyyiWindow*);
GtkWidget*    shell_context_menu_new   (AyyiWindow*);
void          shell_menu_free          (AyyiWindow*);

void          shell__menu_popup        (AyyiWindow*, GdkEvent*);

GtkWidget*    menu_separator_new       (GtkWidget* container);

void          menu_add_view_item       (const char*, bool (*value)(AyyiWindow*, gpointer), AyyiWindowFn toggle, gpointer);
void          menu_sync_panel_items    (AyyiWindow*);
void          menu_view_item_enable    (int, bool);

void          menu_update_recent_songs ();
void          menu_update_options      ();
void          menu_update_history      (const char*);

struct _EditMenu {
    GtkWidget*   radio_item;
    gulong       radio_id;             //signal handler id's.
};

#endif
