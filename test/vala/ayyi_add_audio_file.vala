/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

using GLib;
using Gdk;
using Ayyi;

static string[] files = null;
static int ff = 0;
const OptionEntry[] entries = 
{
	{ ""/*G_OPTION_REMAINING*/, 0, 0, OptionArg.FILENAME_ARRAY, ref files, null, "FILE" },
	{ null }
};

public class Ayyi.AddFile : Ayyi.SimpleClient
{
	public static int main (string[] args)
	{
		OptionContext context = new OptionContext("");
		context.set_help_enabled(true);
		context.add_main_entries(entries, null);
		try {
			context.parse(ref args);
		} catch (OptionError e) {
			print ("%s\n", e.message);
			print ("Use '--help' to see available command line options.\n");
			return 1;
		}

		ayyi.log.to_stdout = true;

		if (files == null) {
			print("no files specified\n");
			return 1;
		}

		if (files != null) {
			string cwd = Environment.get_current_dir();

			char* file;
			for (int i = 0; (file = files[i]) != null; i++) {
				print ("Adding file: %s\n", (string)file);
				if (!Path.is_absolute((string)file)) {
					files[i] = cwd + "/" + (string)file;
				}
			}
		}

		app = new AddFile();
		/*
		if(false) app.client->debug = 1;
		*/

		if (!app.run(((AddFile*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm ()
	{
		add_file(files[0]);
	}

	void add_file (string file)
	{
		AM.Song.add_file(file, (ident, error) => {
			if (error != null && (*((Error**)error)) != null) {
				log_print(LogType.FAIL, "file not added: %i %s", error->code, error->message);
			} else {
				log_print(LogType.OK, "file added");
			}

			string next_file = files[++ff];
			if (next_file != null)
				add_file(next_file);
			else
				quit();
		});
	}
}
