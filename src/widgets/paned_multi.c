/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2007 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * Modified by the GTK+ Team and others 1997-2000.
 */

/*
   a container widget for holding multiple independently resizable vertical strips
   -------------------------------------------------------------------------------

   ***** currently not being used due to a bug where some child widgets are not shown ******

		TODO check this advice:
			> 1. If you want the child to be completely inside the green window,
			>    Try gtk_widget_set_parent_window(button, green_gdk_window);
			>
			> 2. If the child has a gdk window, call gdk_window_raise on it.

   sizing rules:
	-no autosizing of children. Application will set the child widths.

*/

//GETTEXT_PACKAGE is not defined
#define _(A) A
#define P_(A) A
#define I_(A) A

#include <config.h>
#include "paned_multi.h"
#include "gtk/gtkbindings.h"
#include "gtk/gtksignal.h"
#include "gdk/gdkkeysyms.h"
#include "gtk/gtkwindow.h"
#include "gtk/gtkmain.h"
#include "gtk/gtkmarshalers.h"
#include "gtk/gtkprivate.h"
#include "gtk/gtkalias.h"

#include "gtk/gtkmarshalers.c" //maybe move this?
//GtkContainerClass* paned_multi_parent_class;

#include "ayyi_utils.h"

enum {
  PROP_0,
  PROP_POSITION,
  //PROP_POSITION_SET,
  PROP_MIN_POSITION,
  PROP_MAX_POSITION
};

enum {
  CHILD_PROP_0,
  CHILD_PROP_RESIZE,
  CHILD_PROP_SHRINK
};

enum {
  CYCLE_CHILD_FOCUS,
  TOGGLE_HANDLE_FOCUS,
  MOVE_HANDLE,
  CYCLE_HANDLE_FOCUS,
  ACCEPT_POSITION,
  CANCEL_POSITION,
  LAST_SIGNAL
};

static void     paned_multi_set_property          (GObject*, guint prop_id, const GValue *value, GParamSpec *pspec);
static void     paned_multi_get_property          (GObject*, guint prop_id, GValue *value, GParamSpec *pspec);
static void     paned_multi_set_child_property    (GtkContainer*, GtkWidget *child, guint property_id, const GValue *value, GParamSpec *pspec);
static void     paned_multi_get_child_property    (GtkContainer*, GtkWidget *child, guint property_id, GValue *value, GParamSpec *pspec);
static void     paned_multi_finalize              (GObject*);
static void     paned_multi_realize               (GtkWidget*);
static void     paned_multi_unrealize             (GtkWidget*);
static void     paned_multi_map                   (GtkWidget*);
static void     paned_multi_unmap                 (GtkWidget*);
static gboolean paned_multi_expose                (GtkWidget*, GdkEventExpose*);
static gboolean paned_multi_enter                 (GtkWidget*, GdkEventCrossing*);
static gboolean paned_multi_leave                 (GtkWidget*, GdkEventCrossing*);
static gboolean paned_multi_button_press          (GtkWidget*, GdkEventButton*);
static gboolean paned_multi_button_release        (GtkWidget*, GdkEventButton*);
static gboolean paned_multi_motion                (GtkWidget*, GdkEventMotion*);
static gboolean paned_multi_focus                 (GtkWidget*, GtkDirectionType);
static gboolean paned_multi_grab_broken           (GtkWidget*, GdkEventGrabBroken*);
static void     paned_multi_add_internal          (GtkContainer*, GtkWidget*);
static void     paned_multi_remove                (GtkContainer*, GtkWidget*);
static void     paned_multi_forall                (GtkContainer*, gboolean include_internals, GtkCallback, gpointer callback_data);
static void     paned_multi_set_focus_child       (GtkContainer*, GtkWidget *child);
static void     paned_multi_set_saved_focus       (PanedMulti*, GtkWidget*);
static void     paned_multi_set_first_paned       (PanedMulti*, PanedMulti *first_paned);
static void     paned_multi_set_last_children_focus(PanedMulti*, GtkWidget*);
static void     paned_multi_set_last_child_n_focus(PanedMulti*, GtkWidget*, int n);
static gboolean paned_multi_cycle_child_focus     (PanedMulti*, gboolean reverse);
static gboolean paned_multi_cycle_handle_focus    (PanedMulti*, gboolean reverse);
static gboolean paned_multi_move_handle           (PanedMulti*, GtkScrollType);
static gboolean paned_multi_accept_position       (PanedMulti*);
static gboolean paned_multi_cancel_position       (PanedMulti*);
static gboolean paned_multi_toggle_handle_focus   (PanedMulti*);

//static GType    paned_multi_child_type            (GtkContainer*);
static void     paned_multi_grab_notify           (GtkWidget*, gboolean was_grabbed);

static int      find_child_widget                 (PanedMulti*, GtkWidget *child);
static gboolean window_is_handle                  (PanedMulti*, GdkWindow*);
static GdkWindow* handle_lookup                   (PanedMulti*, GdkWindow*);

struct _PanedMultiPrivate
{
  GtkWidget*  saved_focus;
  PanedMulti* first_paned;
  guint32     grab_time;
};

G_DEFINE_ABSTRACT_TYPE (PanedMulti, paned_multi, GTK_TYPE_CONTAINER)

static guint signals[LAST_SIGNAL] = { 0 };

static void
add_tab_bindings (GtkBindingSet* binding_set, GdkModifierType modifiers)
{
  gtk_binding_entry_add_signal (binding_set, GDK_Tab, modifiers, "toggle_handle_focus", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_KP_Tab, modifiers,	"toggle_handle_focus", 0);
}

static void
add_move_binding (GtkBindingSet* binding_set, guint keyval, GdkModifierType mask, GtkScrollType scroll)
{
  gtk_binding_entry_add_signal (binding_set, keyval, mask,
				"move_handle", 1,
				GTK_TYPE_SCROLL_TYPE, scroll);
}

static void
paned_multi_class_init (PanedMultiClass *class)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;
  PanedMultiClass *paned_class;
  GtkBindingSet *binding_set;

  object_class = (GObjectClass *) class;
  widget_class = (GtkWidgetClass *) class;
  container_class = (GtkContainerClass *) class;
  paned_class = (PanedMultiClass *) class;

  object_class->set_property = paned_multi_set_property;
  object_class->get_property = paned_multi_get_property;
  object_class->finalize = paned_multi_finalize;

  widget_class->realize = paned_multi_realize;
  widget_class->unrealize = paned_multi_unrealize;
  widget_class->map = paned_multi_map;
  widget_class->unmap = paned_multi_unmap;
  widget_class->expose_event = paned_multi_expose;
  widget_class->focus = paned_multi_focus;
  widget_class->enter_notify_event = paned_multi_enter;
  widget_class->leave_notify_event = paned_multi_leave;
  widget_class->button_press_event = paned_multi_button_press;
  widget_class->button_release_event = paned_multi_button_release;
  widget_class->motion_notify_event = paned_multi_motion;
  widget_class->grab_broken_event = paned_multi_grab_broken;
  widget_class->grab_notify = paned_multi_grab_notify;
  
  container_class->add = paned_multi_add_internal;
  container_class->remove = paned_multi_remove;
  container_class->forall = paned_multi_forall;
  //container_class->child_type = paned_multi_child_type;
  container_class->set_focus_child = paned_multi_set_focus_child;
  container_class->set_child_property = paned_multi_set_child_property;
  container_class->get_child_property = paned_multi_get_child_property;

  paned_class->cycle_child_focus = paned_multi_cycle_child_focus;
  paned_class->toggle_handle_focus = paned_multi_toggle_handle_focus;
  paned_class->move_handle = paned_multi_move_handle;
  paned_class->cycle_handle_focus = paned_multi_cycle_handle_focus;
  paned_class->accept_position = paned_multi_accept_position;
  paned_class->cancel_position = paned_multi_cancel_position;

  /*
  g_object_class_install_property (object_class,
				   PROP_POSITION,
				   g_param_spec_int ("position",
						     P_("Position"),
						     P_("Position of paned separator in pixels (0 means all the way to the left/top)"),
						     0,
						     G_MAXINT,
						     0,
						     GTK_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_POSITION_SET,
				   g_param_spec_boolean ("position-set",
							 P_("Position Set"),
							 P_("TRUE if the Position property should be used"),
							 FALSE,
							 GTK_PARAM_READWRITE));
  */
  gtk_widget_class_install_style_property (widget_class,
					   g_param_spec_int ("handle-size",
							     P_("Handle Size"),
							     P_("Width of handle"),
							     0,
							     G_MAXINT,
							     5,
							     GTK_PARAM_READABLE));
  /**
   * PanedMulti:min-position:
   *
   * The smallest possible value for the position property. This property is derived from the
   * size and shrinkability of the widget's children.
   *
   * Since: 2.4
   */
  /*
  g_object_class_install_property (object_class,
				   PROP_MIN_POSITION,
				   g_param_spec_int ("min-position",
						     P_("Minimal Position"),
						     P_("Smallest possible value for the \"position\" property"),
						     0,
						     G_MAXINT,
						     0,
						     GTK_PARAM_READABLE));
  */

  /**
   * PanedMulti:max-position:
   *
   * The largest possible value for the position property. This property is derived from the
   * size and shrinkability of the widget's children.
   *
   * Since: 2.4
   */
  /*
  g_object_class_install_property (object_class,
				   PROP_MAX_POSITION,
				   g_param_spec_int ("max-position",
						     P_("Maximal Position"),
						     P_("Largest possible value for the \"position\" property"),
						     0,
						     G_MAXINT,
						     G_MAXINT,
						     GTK_PARAM_READABLE));
  */

/**
 * PanedMulti:resize:
 *
 * The "resize" child property determines whether the child expands and 
 * shrinks along with the paned widget.
 * 
 * Since: 2.4 
 */
  /*
  gtk_container_class_install_child_property (container_class,
					      CHILD_PROP_RESIZE,
					      g_param_spec_boolean ("resize", 
								    P_("Resize"),
								    P_("If TRUE, the child expands and shrinks along with the paned widget"),
								    TRUE,
								    GTK_PARAM_READWRITE));
  */

/**
 * PanedMulti:shrink:
 *
 * The "shrink" child property determines whether the child can be made 
 * smaller than its requisition.
 * 
 * Since: 2.4 
 */
  gtk_container_class_install_child_property (container_class,
					      CHILD_PROP_SHRINK,
					      g_param_spec_boolean ("shrink", 
								    P_("Shrink"),
								    P_("If TRUE, the child can be made smaller than its requisition"),
								    TRUE,
								    GTK_PARAM_READWRITE));

  signals [CYCLE_CHILD_FOCUS] =
    g_signal_new (I_("cycle_child_focus"),
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (PanedMultiClass, cycle_child_focus),
		  NULL, NULL,
		  _gtk_marshal_BOOLEAN__BOOLEAN,
		  G_TYPE_BOOLEAN, 1,
		  G_TYPE_BOOLEAN);

  signals [TOGGLE_HANDLE_FOCUS] =
    g_signal_new (I_("toggle_handle_focus"),
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (PanedMultiClass, toggle_handle_focus),
		  NULL, NULL,
		  _gtk_marshal_BOOLEAN__VOID,
		  G_TYPE_BOOLEAN, 0);

  signals[MOVE_HANDLE] =
    g_signal_new (I_("move_handle"),
		  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                  G_STRUCT_OFFSET (PanedMultiClass, move_handle),
                  NULL, NULL,
                  _gtk_marshal_BOOLEAN__ENUM,
                  G_TYPE_BOOLEAN, 1,
                  GTK_TYPE_SCROLL_TYPE);

  signals [CYCLE_HANDLE_FOCUS] =
    g_signal_new (I_("cycle_handle_focus"),
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (PanedMultiClass, cycle_handle_focus),
		  NULL, NULL,
		  _gtk_marshal_BOOLEAN__BOOLEAN,
		  G_TYPE_BOOLEAN, 1,
		  G_TYPE_BOOLEAN);

  signals [ACCEPT_POSITION] =
    g_signal_new (I_("accept_position"),
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (PanedMultiClass, accept_position),
		  NULL, NULL,
		  _gtk_marshal_BOOLEAN__VOID,
		  G_TYPE_BOOLEAN, 0);

  signals [CANCEL_POSITION] =
    g_signal_new (I_("cancel_position"),
		  G_TYPE_FROM_CLASS (object_class),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (PanedMultiClass, cancel_position),
		  NULL, NULL,
		  _gtk_marshal_BOOLEAN__VOID,
		  G_TYPE_BOOLEAN, 0);

  binding_set = gtk_binding_set_by_class (class);

  /* F6 and friends */
  gtk_binding_entry_add_signal (binding_set, GDK_F6, 0,              "cycle_child_focus", 1, G_TYPE_BOOLEAN, FALSE);
  gtk_binding_entry_add_signal (binding_set, GDK_F6, GDK_SHIFT_MASK, "cycle_child_focus", 1, G_TYPE_BOOLEAN, TRUE);

  /* F8 and friends */
  gtk_binding_entry_add_signal (binding_set, GDK_F8, 0,	             "cycle_handle_focus", 1, G_TYPE_BOOLEAN, FALSE);
  gtk_binding_entry_add_signal (binding_set, GDK_F8, GDK_SHIFT_MASK, "cycle_handle_focus", 1, G_TYPE_BOOLEAN, TRUE);
 
  add_tab_bindings (binding_set, 0);
  add_tab_bindings (binding_set, GDK_CONTROL_MASK);
  add_tab_bindings (binding_set, GDK_SHIFT_MASK);
  add_tab_bindings (binding_set, GDK_CONTROL_MASK | GDK_SHIFT_MASK);

  /* accept and cancel positions */
  gtk_binding_entry_add_signal (binding_set, GDK_Escape,   0,        "cancel_position", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Return,   0,        "accept_position", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_KP_Enter, 0,        "accept_position", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_space,    0,        "accept_position", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_KP_Space, 0,        "accept_position", 0);

  /* move handle */
  add_move_binding (binding_set, GDK_Left,         0,                GTK_SCROLL_STEP_LEFT);
  add_move_binding (binding_set, GDK_KP_Left,      0,                GTK_SCROLL_STEP_LEFT);
  add_move_binding (binding_set, GDK_Left,         GDK_CONTROL_MASK, GTK_SCROLL_PAGE_LEFT);
  add_move_binding (binding_set, GDK_KP_Left,      GDK_CONTROL_MASK, GTK_SCROLL_PAGE_LEFT);

  add_move_binding (binding_set, GDK_Right,        0,                GTK_SCROLL_STEP_RIGHT);
  add_move_binding (binding_set, GDK_Right,        GDK_CONTROL_MASK, GTK_SCROLL_PAGE_RIGHT);
  add_move_binding (binding_set, GDK_KP_Right,     0,                GTK_SCROLL_STEP_RIGHT);
  add_move_binding (binding_set, GDK_KP_Right,     GDK_CONTROL_MASK, GTK_SCROLL_PAGE_RIGHT);

  add_move_binding (binding_set, GDK_Up,           0,                GTK_SCROLL_STEP_UP);
  add_move_binding (binding_set, GDK_Up,           GDK_CONTROL_MASK, GTK_SCROLL_PAGE_UP);
  add_move_binding (binding_set, GDK_KP_Up,        0,                GTK_SCROLL_STEP_UP);
  add_move_binding (binding_set, GDK_KP_Up,        GDK_CONTROL_MASK, GTK_SCROLL_PAGE_UP);
  add_move_binding (binding_set, GDK_Page_Up,      0,                GTK_SCROLL_PAGE_UP);
  add_move_binding (binding_set, GDK_KP_Page_Up,   0,                GTK_SCROLL_PAGE_UP);

  add_move_binding (binding_set, GDK_Down,         0,                GTK_SCROLL_STEP_DOWN);
  add_move_binding (binding_set, GDK_Down,         GDK_CONTROL_MASK, GTK_SCROLL_PAGE_DOWN);
  add_move_binding (binding_set, GDK_KP_Down,      0,                GTK_SCROLL_STEP_DOWN);
  add_move_binding (binding_set, GDK_KP_Down,      GDK_CONTROL_MASK, GTK_SCROLL_PAGE_DOWN);
  add_move_binding (binding_set, GDK_Page_Down,    0,                GTK_SCROLL_PAGE_RIGHT);
  add_move_binding (binding_set, GDK_KP_Page_Down, 0,                GTK_SCROLL_PAGE_RIGHT);

  add_move_binding (binding_set, GDK_Home,         0,                GTK_SCROLL_START);
  add_move_binding (binding_set, GDK_KP_Home,      0,                GTK_SCROLL_START);
  add_move_binding (binding_set, GDK_End,          0,                GTK_SCROLL_END);
  add_move_binding (binding_set, GDK_KP_End,       0,                GTK_SCROLL_END);

  g_type_class_add_private (object_class, sizeof (PanedMultiPrivate));  
}

/*
static GType
paned_multi_child_type (GtkContainer *container)
{
  // whats this for? its a gtkcontainer function

  //if (!PANED_MULTI (container)->child1 || !PANED_MULTI (container)->child2)
  if(1)
    return GTK_TYPE_WIDGET;
  else
    return G_TYPE_NONE;
}
*/

static void
paned_multi_init (PanedMulti *paned)
{
  GTK_WIDGET_SET_FLAGS (paned, GTK_NO_WINDOW | GTK_CAN_FOCUS);

  paned->children = g_ptr_array_new();
  //paned->child1 = NULL;
  //paned->child2 = NULL;
  //paned->handle = NULL;
  paned->xor_gc = NULL;
  paned->cursor_type = GDK_CROSS;
  
  //paned->handle_pos.width = 5;
  //paned->handle_pos.height = 5;
  //paned->position_set = FALSE;
  paned->last_allocation = -1;
  paned->in_drag = FALSE;

  paned->priv = G_TYPE_INSTANCE_GET_PRIVATE (paned, TYPE_PANED_MULTI, PanedMultiPrivate);
  //paned->last_child1_focus = NULL;
  //paned->last_child2_focus = NULL;
  paned->in_recursion = FALSE;
  paned->handle_prelit = FALSE;
  paned->original_position = -1;
  
  //paned->handle_pos.x = -1;
  //paned->handle_pos.y = -1;

  paned->drag_pos = -1;
}

static void
paned_multi_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  PanedMulti *paned = PANED_MULTI (object);
  
  switch (prop_id)
    {
    case PROP_POSITION:
      paned_multi_set_position (paned, g_value_get_int (value));
      break;
    /*
    case PROP_POSITION_SET:
      paned->position_set = g_value_get_boolean (value);
      gtk_widget_queue_resize (GTK_WIDGET (paned));
      break;
    */
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
paned_multi_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  PanedMulti *paned = PANED_MULTI (object);
  
  switch (prop_id)
    {
    case PROP_POSITION:
      g_value_set_int (value, paned_multi_get_child_size(0));
      break;
    /*
    case PROP_POSITION_SET:
      g_value_set_boolean (value, paned->position_set);
      break;
    */
    case PROP_MIN_POSITION:
      g_value_set_int (value, paned->min_position);
      break;
    case PROP_MAX_POSITION:
      g_value_set_int (value, paned->max_position);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
paned_multi_set_child_property (GtkContainer *container, GtkWidget *child, guint property_id, const GValue *value, GParamSpec *pspec)
{
#if 0
  PanedMulti *paned = PANED_MULTI (container);
  gboolean old_value, new_value;

  //g_assert (child == paned->child1 || child == paned->child2);

  new_value = g_value_get_boolean (value);
  switch (property_id)
    {
    case CHILD_PROP_RESIZE:
      if (child == paned->child1)
      {
        old_value = paned->child1_resize;
        paned->child1_resize = new_value;
      }
      else
      {
        old_value = paned->child2_resize;
        paned->child2_resize = new_value;
      }
      break;
    case CHILD_PROP_SHRINK:
      if (child == paned->child1)
      {
        old_value = paned->child1_shrink;
        paned->child1_shrink = new_value;
      }
      else
	{
	  old_value = paned->child2_shrink;
	  paned->child2_shrink = new_value;
	}
      break;
    default:
      GTK_CONTAINER_WARN_INVALID_CHILD_PROPERTY_ID (container, property_id, pspec);
      old_value = -1; /* quiet gcc */
      break;
    }
  if (old_value != new_value)
    gtk_widget_queue_resize (GTK_WIDGET (container));
#endif
}

static void
paned_multi_get_child_property (GtkContainer *container, GtkWidget *child, guint property_id, GValue *value, GParamSpec *pspec)
{
#if 0
  PanedMulti *paned = PANED_MULTI (container);

  g_assert (child == paned->child1 || child == paned->child2);
  
  switch (property_id)
    {
    case CHILD_PROP_RESIZE:
      if (child == paned->child1)
	g_value_set_boolean (value, paned->child1_resize);
      else
	g_value_set_boolean (value, paned->child2_resize);
      break;
    case CHILD_PROP_SHRINK:
      if (child == paned->child1)
	g_value_set_boolean (value, paned->child1_shrink);
      else
	g_value_set_boolean (value, paned->child2_shrink);
      break;
    default:
      GTK_CONTAINER_WARN_INVALID_CHILD_PROPERTY_ID (container, property_id, pspec);
      break;
    }
#endif
}

static void
paned_multi_finalize (GObject *object)
{
  PanedMulti *paned = PANED_MULTI (object);
  
  paned_multi_set_saved_focus (paned, NULL);
  paned_multi_set_first_paned (paned, NULL);

  g_ptr_array_free(paned->children, TRUE);

  G_OBJECT_CLASS (paned_multi_parent_class)->finalize (object);
}

static void
paned_multi_realize (GtkWidget *widget)
{
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  widget->window = gtk_widget_get_parent_window (widget);
  g_object_ref (widget->window);

  PanedMulti *paned = PANED_MULTI (widget);
#ifdef DRAW_GDKWIN
  GdkWindowAttr attributes;
  gint attributes_mask;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.wclass = GDK_INPUT_ONLY;
  attributes.x      = widget->allocation.x;//paned->handle_pos.x;
  attributes.y      = widget->allocation.y;  //paned->handle_pos.y;
  attributes.width  = widget->allocation.width; //paned->handle_pos.width;
  attributes.height = widget->allocation.height;//paned->handle_pos.height;
  attributes.cursor = gdk_cursor_new_for_display (gtk_widget_get_display (widget), paned->cursor_type);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_ENTER_NOTIFY_MASK |
			    GDK_LEAVE_NOTIFY_MASK |
			    GDK_POINTER_MOTION_MASK |
			    GDK_POINTER_MOTION_HINT_MASK);
  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_CURSOR;

  //note: 1st arg is parent:
  GdkWindow* window = /*paned->handle = */gdk_window_new (widget->window, &attributes, attributes_mask);
  //widget->window = window; // segfault
  //gdk_window_set_user_data (paned->handle, paned);
  gdk_window_set_user_data (window, paned);
  gdk_cursor_unref (attributes.cursor);
  gdk_window_show (window);
#endif

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL); //testing

  //if (paned->child1 && GTK_WIDGET_VISIBLE (paned->child1) &&
  //    paned->child2 && GTK_WIDGET_VISIBLE (paned->child2))
  //  gdk_window_show (paned->handle);
}

static void
paned_multi_unrealize (GtkWidget *widget)
{
  PanedMulti *paned = PANED_MULTI (widget);

  if (paned->xor_gc)
    {
      g_object_unref (paned->xor_gc);
      paned->xor_gc = NULL;
    }

  int i; for (i=0;i<paned->children->len;i++) {
    paned_child* child = g_ptr_array_index(paned->children, i);
    if (child->handle) {
      gdk_window_set_user_data (child->handle, NULL);
      gdk_window_destroy (child->handle);
      child->handle = NULL;
    }
  }

  paned_multi_set_last_children_focus (paned, NULL);
  //paned_multi_set_last_child2_focus (paned, NULL);
  paned_multi_set_saved_focus (paned, NULL);
  paned_multi_set_first_paned (paned, NULL);
  
  if (GTK_WIDGET_CLASS (paned_multi_parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (paned_multi_parent_class)->unrealize) (widget);
}

static void
paned_multi_map (GtkWidget *widget)
{
  PanedMulti *paned = PANED_MULTI (widget);

  int i; for (i=0;i<paned->children->len;i++) {
    //paned_child* child = g_ptr_array_index(paned->children, i);
    dbg(2, "showing handle %i... TEMP DISABLED", i);
    //gdk_window_show (child->handle);
  }

  GTK_WIDGET_CLASS (paned_multi_parent_class)->map (widget);
}

static void
paned_multi_unmap (GtkWidget *widget)
{
  PanedMulti *paned = PANED_MULTI (widget);
    
  int i; for (i=0;i<paned->children->len;i++) {
    paned_child* child = g_ptr_array_index(paned->children, i);
    gdk_window_hide (child->handle);
  }

  GTK_WIDGET_CLASS (paned_multi_parent_class)->unmap (widget);
}

static gint my_gtk_container_expose (GtkWidget *widget, GdkEventExpose *event); //tmp
static gboolean
paned_multi_expose (GtkWidget *widget, GdkEventExpose *event)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);

  if (GTK_WIDGET_VISIBLE (widget) && GTK_WIDGET_MAPPED (widget)
      //&& paned->child1 && GTK_WIDGET_VISIBLE (paned->child1)
      //&& paned->child2 && GTK_WIDGET_VISIBLE (paned->child2)
     ) {
      GtkStateType state;
      
      if (gtk_widget_is_focus (widget))
        state = GTK_STATE_SELECTED;
      else if (paned->handle_prelit)
        state = GTK_STATE_PRELIGHT;
      else
        state = GTK_WIDGET_STATE (widget);
      
#ifdef DRAW_GDKWIN
      int i; for(i=0;i<paned->children->len;i++){
        paned_child* pc = ((paned_child*)g_ptr_array_index(paned->children, i));
        dbg(2, "painting handle %i at %i...", i, pc->handle_pos.x);
        gtk_paint_handle (widget->style, widget->window,
			state, GTK_SHADOW_NONE,
			&pc->handle_pos, widget, "paned",
			pc->handle_pos.x,     pc->handle_pos.y,
			pc->handle_pos.width, pc->handle_pos.height,
			paned->orientation);
      }
#endif
  }

  GList* children = gtk_container_get_children(GTK_CONTAINER(widget));
  for(;children;children=children->next){
    GtkWidget* child = children->data;
    GdkWindow* win = child->window;
    gint x, y, w, h;
    gdk_window_get_geometry(win, &x, &y, &w, &h, NULL);
    printf("*");
    dbg(2, "realised=%i x=%i vis=%i", GTK_WIDGET_REALIZED(child), x, gdk_window_is_visible(win));

    //testing:
    //gtk_widget_send_expose (child, event);
  }
  printf("\n");

  /* Chain up to draw children */
#define TYU
#ifdef TYU
  my_gtk_container_expose(widget, event);
#else
  GTK_WIDGET_CLASS (paned_multi_parent_class)->expose_event (widget, event);
#endif

  return FALSE;
}

/*
static gboolean
is_rtl (PanedMulti *paned)
{
  if (paned->orientation == GTK_ORIENTATION_VERTICAL &&
      gtk_widget_get_direction (GTK_WIDGET (paned)) == GTK_TEXT_DIR_RTL)
    {
      return TRUE;
    }

  return FALSE;
}
*/

static void
update_drag (PanedMulti *paned)
{
  gint pos;
  gint handle_size;
  gint size;
  
  if (paned->orientation == GTK_ORIENTATION_HORIZONTAL)
    gtk_widget_get_pointer (GTK_WIDGET (paned), NULL, &pos);
  else
    gtk_widget_get_pointer (GTK_WIDGET (paned), &pos, NULL);

  pos -= paned->drag_pos;

  /*if (is_rtl (paned))
    {
      gtk_widget_style_get (GTK_WIDGET (paned), "handle-size", &handle_size, NULL);
      
      size = GTK_WIDGET (paned)->allocation.width - pos - handle_size;
    }
  else*/
    {
      size = pos;
    }

  size -= GTK_CONTAINER (paned)->border_width;
  
  size = CLAMP (size, paned->min_position, paned->max_position);

  if (size != paned_multi_get_child_size(0)) paned_multi_set_position (paned, size);
}

static gboolean
paned_multi_enter (GtkWidget *widget, GdkEventCrossing *event)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);
  
  if (paned->in_drag)
    update_drag (paned);
  else
    {
      //find_child_widget(PanedMulti *paned, GtkWidget *child);
      paned_child* pc = g_ptr_array_index(paned->children, 0);

      paned->handle_prelit = TRUE;
      gtk_widget_queue_draw_area (widget,
				  pc->handle_pos.x,
				  pc->handle_pos.y,
				  pc->handle_pos.width,
				  pc->handle_pos.height);
    }
  
  return TRUE;
}

static gboolean
paned_multi_leave (GtkWidget *widget, GdkEventCrossing *event)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);
  
  if (paned->in_drag)
    update_drag (paned);
  else
    {
      paned_child* pc = g_ptr_array_index(paned->children, 0);

      paned->handle_prelit = FALSE;
      gtk_widget_queue_draw_area (widget,
				  pc->handle_pos.x,
				  pc->handle_pos.y,
				  pc->handle_pos.width,
				  pc->handle_pos.height);
    }

  return TRUE;
}

static gboolean
paned_multi_focus (GtkWidget *widget, GtkDirectionType direction)

{
  /* This is a hack, but how can this be done without
   * excessive cut-and-paste from gtkcontainer.c?
   */

  GTK_WIDGET_UNSET_FLAGS (widget, GTK_CAN_FOCUS);
  gboolean retval = (* GTK_WIDGET_CLASS (paned_multi_parent_class)->focus) (widget, direction);
  GTK_WIDGET_SET_FLAGS (widget, GTK_CAN_FOCUS);

  return retval;
}

static gboolean
paned_multi_button_press (GtkWidget *widget, GdkEventButton *event)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);

  GdkWindow* handle = handle_lookup(paned, event->window);
  if(handle) dbg(0, "handle!");

  if (!paned->in_drag &&
      //(event->window == paned->handle) && (event->button == 1))
      (window_is_handle(paned, event->window)) && (event->button == 1))
    {
      dbg(0, "starting drag...");
      /* We need a server grab here, not gtk_grab_add(), since
       * we don't want to pass events on to the widget's children */
      if (gdk_pointer_grab (handle, FALSE,
			    GDK_POINTER_MOTION_HINT_MASK
			    | GDK_BUTTON1_MOTION_MASK
			    | GDK_BUTTON_RELEASE_MASK
			    | GDK_ENTER_NOTIFY_MASK
			    | GDK_LEAVE_NOTIFY_MASK,
			    NULL, NULL,
			    event->time) != GDK_GRAB_SUCCESS)
        return FALSE;

      paned->in_drag = TRUE;
      paned->priv->grab_time = event->time;

      if (paned->orientation == GTK_ORIENTATION_HORIZONTAL)
        paned->drag_pos = event->y;
      else
        paned->drag_pos = event->x;
      
      return TRUE;
    }

  return FALSE;
}

static gboolean
paned_multi_grab_broken (GtkWidget *widget, GdkEventGrabBroken *event)
{
  PanedMulti *paned = PANED_MULTI (widget);

  paned->in_drag = FALSE;
  paned->drag_pos = -1;
  //paned->position_set = TRUE;

  return TRUE;
}

static void
stop_drag (PanedMulti *paned)
{
  paned->in_drag = FALSE;
  paned->drag_pos = -1;
  //paned->position_set = TRUE;
  gdk_display_pointer_ungrab (gtk_widget_get_display (GTK_WIDGET (paned)), paned->priv->grab_time);
}

static void
paned_multi_grab_notify (GtkWidget *widget, gboolean was_grabbed)
{
  PanedMulti *paned = PANED_MULTI (widget);

  if (!was_grabbed && paned->in_drag) stop_drag (paned);
}

static gboolean
paned_multi_button_release (GtkWidget *widget, GdkEventButton *event)
{
  PF;
  PanedMulti *paned = PANED_MULTI (widget);

  if (paned->in_drag && (event->button == 1))
    {
      stop_drag (paned);
      return TRUE;
    }

  return FALSE;
}

static gboolean
paned_multi_motion (GtkWidget *widget, GdkEventMotion *event)
{
  PanedMulti *paned = PANED_MULTI (widget);
  
  if (paned->in_drag)
    {
      update_drag (paned);
      return TRUE;
    }
  
  return FALSE;
}

void
paned_multi_add (PanedMulti *paned, GtkWidget *widget)
{
  paned_multi_pack (paned, widget, TRUE);
}

void
paned_multi_pack (PanedMulti *paned, GtkWidget *child, gboolean shrink) //FIXME remove shrink arg?
{
  g_return_if_fail (IS_PANED_MULTI (paned));
  g_return_if_fail (GTK_IS_WIDGET (child));
  GtkWidget* widget = GTK_WIDGET(paned);

  paned_child* pc = g_new0(paned_child, 1);
  pc->widget = child;
  pc->size   = child->requisition.width; //**cough**

  /*
  {
    GdkWindowAttr attributes;
    gint attributes_mask;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.wclass      = GDK_INPUT_ONLY;
    attributes.x           = pc->handle_pos.x;
    attributes.y           = pc->handle_pos.y;
    attributes.width       = pc->handle_pos.width;
    attributes.height      = pc->handle_pos.height;
    attributes.cursor      = gdk_cursor_new_for_display (gtk_widget_get_display (widget), paned->cursor_type);
    attributes.event_mask  = gtk_widget_get_events (widget);
    attributes.event_mask |= (GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_ENTER_NOTIFY_MASK |
			    GDK_LEAVE_NOTIFY_MASK |
			    GDK_POINTER_MOTION_MASK |
			    GDK_POINTER_MOTION_HINT_MASK);
    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_CURSOR;
    pc->handle = gdk_window_new (GTK_WIDGET(paned)->window, &attributes, attributes_mask);
    gdk_window_set_user_data (pc->handle, paned);
    gdk_cursor_unref (attributes.cursor);
    gdk_window_show (pc->handle);
  }
  */

  pc->handle = NULL; //note !! currently handles do not have their own window.
  pc->handle_pos.width = 5;
  pc->handle_pos.height = 5;
  pc->handle_pos.x = -1;
  pc->handle_pos.y = -1;

  g_ptr_array_add(paned->children, pc);
  dbg(0, "size=%i", paned->children->len);

  //if (!paned->child1) {
  //    paned->child1 = child;
  //    paned->children_resize = resize;
      paned->children_shrink = shrink;

  //}
  gtk_widget_set_parent (child, GTK_WIDGET(paned));
  gtk_widget_show(child);
}

#if 0
void
paned_multi_pack2 (PanedMulti  *paned,
		 GtkWidget *child,
		 gboolean   resize,
		 gboolean   shrink)
{
  g_return_if_fail (IS_PANED_MULTI (paned));
  g_return_if_fail (GTK_IS_WIDGET (child));

  if (!paned->child2)
    {
      paned->child2 = child;
      paned->child2_resize = resize;
      paned->child2_shrink = shrink;

      gtk_widget_set_parent (child, GTK_WIDGET (paned));
    }
}
#endif


static void
paned_multi_add_internal (GtkContainer *container, GtkWidget *widget)
{
  g_return_if_fail (IS_PANED_MULTI (container));

  PanedMulti *paned = PANED_MULTI (container);

  //if (!paned->child1)
    paned_multi_add (paned, widget);
  //else if (!paned->child2)
  //  paned_multi_add2 (paned, widget);
  //else
  //  g_warning ("PanedMulti cannot have more than 2 children\n");
}

static void
paned_multi_remove (GtkContainer *container, GtkWidget *widget)
{
  PanedMulti *paned = PANED_MULTI (container);
  gboolean was_visible = GTK_WIDGET_VISIBLE (widget);

  int i = find_child_widget(paned, widget);
  if (i > -1) {
    if (g_ptr_array_remove(paned->children, widget)) {
    //if (paned->child1 == widget) {
      gtk_widget_unparent (widget);

      //paned->child1 = NULL;

      if (was_visible && GTK_WIDGET_VISIBLE (container)) gtk_widget_queue_resize (GTK_WIDGET (container));
    }
    else dbg(0, "widget not found");
  }
  /*
  else if (paned->child2 == widget)
    {
      gtk_widget_unparent (widget);

      paned->child2 = NULL;

      if (was_visible && GTK_WIDGET_VISIBLE (container))
	gtk_widget_queue_resize (GTK_WIDGET (container));
    }
  */
}

static void
paned_multi_forall (GtkContainer *container, gboolean include_internals, GtkCallback callback, gpointer callback_data)
{
  g_return_if_fail (callback != NULL);

  PanedMulti *paned = PANED_MULTI (container);

  if(!paned->children) { gerr("children null"); return; }
  if(!paned->children->len) return;
  int i; for(i=0;i<paned->children->len;i++){
    paned_child* pc = g_ptr_array_index(paned->children, i);
    GtkWidget* widget = ((paned_child*)g_ptr_array_index(paned->children, i))->widget;
    //GtkWidget* child2 = ((paned_child*)g_ptr_array_index(paned->children, 1))->widget;

    if (widget) (*callback) (widget, callback_data);
    //if (child2) (*callback) (child2, callback_data);
  }
}

/**
 * paned_multi_get_position:
 * @paned: a #PanedMulti widget
 * 
 * Obtains the position of the divider between the two panes.
 * 
 * Return value: position of the divider
 **/
gint
paned_multi_get_position (PanedMulti *paned)
{
  g_return_val_if_fail (IS_PANED_MULTI (paned), 0);

  return paned_multi_get_child_size(0);
}

/**
 * paned_multi_set_position:
 * @paned: a #PanedMulti widget
 * @position: pixel position of divider, a negative value means that the position
 *            is unset.
 * 
 * Sets the position of the divider between the two panes.
 **/
void
paned_multi_set_position (PanedMulti *paned, gint position)
{
  g_return_if_fail (IS_PANED_MULTI (paned));

  GObject *object = G_OBJECT (paned);
  
  if (position >= 0) {
      /* We don't clamp here - the assumption is that
       * if the total allocation changes at the same time
       * as the position, the position set is with reference
       * to the new total size. If only the position changes,
       * then clamping will occur in paned_multi_compute_position()
       */

      ((paned_child*)g_ptr_array_index(paned->children, 0))->size = position;
      //paned->position_set = TRUE;
    }
  /*
  else
    {
      paned->position_set = FALSE;
    }
  */

  g_object_freeze_notify (object);
  //g_object_notify (object, "position");
  //g_object_notify (object, "position-set");
  g_object_thaw_notify (object);

  gtk_widget_queue_resize (GTK_WIDGET (paned));
}

/**
 * paned_multi_get_child1:
 * @paned: a #PanedMulti widget
 * 
 * Obtains the first child of the paned widget.
 * 
 * Return value: first child, or %NULL if it is not set.
 *
 * Since: 2.4
 **/
GtkWidget *
paned_multi_get_child (PanedMulti *paned, int n)
{
  g_return_val_if_fail (IS_PANED_MULTI (paned), NULL);

  return g_ptr_array_index(paned->children, n);
}

/**
 * paned_multi_get_child2:
 * @paned: a #PanedMulti widget
 * 
 * Obtains the second child of the paned widget.
 * 
 * Return value: second child, or %NULL if it is not set.
 *
 * Since: 2.4
 **/
#if 0
GtkWidget *
paned_multi_get_child2 (PanedMulti *paned)
{
  g_return_val_if_fail (IS_PANED_MULTI (paned), NULL);

  return paned->child2;
}
#endif

/**
 * @allocation: space available for child widgets (the container dimension minus all handle and border space).
 *
 * called by derived widget implementations.
 **/
void
paned_multi_compute_position (PanedMulti *paned, gint allocation, gint child1_req, gint child2_req)
{
  g_return_if_fail (IS_PANED_MULTI (paned));
  dbg(0, "req0=%i req1=%i", child1_req, child2_req);

  gint old_position     = paned_multi_get_child_size(0);
  gint old_min_position = paned->min_position;
  gint old_max_position = paned->max_position;

  paned->min_position = paned->children_shrink ? 0 : child1_req;

  paned->max_position = allocation;
  if (!paned->children_shrink) paned->max_position = MAX (1, paned->max_position - child2_req);
  paned->max_position = MAX (paned->min_position, paned->max_position);

  paned_child* child0 = g_ptr_array_index(paned->children, 0);
  /*
  if (!paned->position_set) {
    if (paned->children_resize && !paned->children_resize)
        paned->child1_size = MAX (0, allocation - child2_req);
    else if (!paned->children_resize && paned->children_resize)
        paned->child1_size = child1_req;
    else if (child1_req + child2_req != 0)
        paned->child1_size = allocation * ((gdouble)child1_req / (child1_req + child2_req)) + 0.5;
    else
        paned->child1_size = allocation * 0.5 + 0.5;
  }
  else*/ {
      /* If the position was set before the initial allocation.
       * (paned->last_allocation <= 0) just clamp it and leave it.
       */
    if (paned->last_allocation > 0) {
	  child0->size = allocation * ((gdouble) child0->size / (paned->last_allocation)) + 0.5;
	}
    else child0->size = child1_req;
  }

  child0->size = CLAMP (child0->size, paned->min_position, paned->max_position);

  //if (paned->child1) gtk_widget_set_child_visible (paned->child1, paned->child1_size != 0);
  
  //if (paned->child2) gtk_widget_set_child_visible (paned->child2, paned->child1_size != allocation); 

  g_object_freeze_notify (G_OBJECT (paned));
  //if (paned->child1_size  != old_position)     g_object_notify (G_OBJECT (paned), "position");
  //if (paned->min_position != old_min_position) g_object_notify (G_OBJECT (paned), "min-position");
  //if (paned->max_position != old_max_position) g_object_notify (G_OBJECT (paned), "max-position");
  g_object_thaw_notify (G_OBJECT (paned));

  paned->last_allocation = allocation;
}

static void
paned_multi_set_saved_focus (PanedMulti *paned, GtkWidget *widget)
{
  if (paned->priv->saved_focus) {
    g_object_remove_weak_pointer (G_OBJECT (paned->priv->saved_focus), (gpointer *)&(paned->priv->saved_focus));
  }

  paned->priv->saved_focus = widget;

  if (paned->priv->saved_focus) {
    g_object_add_weak_pointer (G_OBJECT (paned->priv->saved_focus), (gpointer *)&(paned->priv->saved_focus));
  }
}

static void
paned_multi_set_first_paned (PanedMulti *paned, PanedMulti *first_paned)
{
  if (paned->priv->first_paned)
    g_object_remove_weak_pointer (G_OBJECT (paned->priv->first_paned), (gpointer *)&(paned->priv->first_paned));

  paned->priv->first_paned = first_paned;

  if (paned->priv->first_paned)
    g_object_add_weak_pointer (G_OBJECT (paned->priv->first_paned), (gpointer *)&(paned->priv->first_paned));
}

//this may not be needed - use child_n focu instead?
static void
paned_multi_set_last_children_focus (PanedMulti *paned, GtkWidget *widget)
{
#if 0
  if (paned->last_child1_focus)
    g_object_remove_weak_pointer (G_OBJECT (paned->last_child1_focus), (gpointer *)&(paned->last_child1_focus));

  paned->last_child1_focus = widget;

  if (paned->last_child1_focus)
    g_object_add_weak_pointer (G_OBJECT (paned->last_child1_focus), (gpointer *)&(paned->last_child1_focus));
#endif
}

static void
paned_multi_set_last_child_n_focus (PanedMulti *paned, GtkWidget *widget, int n)
{
#if 0
  if (paned->last_child2_focus)
    g_object_remove_weak_pointer (G_OBJECT (paned->last_child2_focus), (gpointer *)&(paned->last_child2_focus));

  paned->last_child2_focus = widget;

  if (paned->last_child2_focus)
    g_object_add_weak_pointer (G_OBJECT (paned->last_child2_focus), (gpointer *)&(paned->last_child2_focus));
#endif
}

static GtkWidget *
paned_get_focus_widget (PanedMulti *paned)
{
  GtkWidget *toplevel = gtk_widget_get_toplevel (GTK_WIDGET (paned));
  if (GTK_WIDGET_TOPLEVEL (toplevel)) return GTK_WINDOW (toplevel)->focus_widget;

  return NULL;
}

static void
paned_multi_set_focus_child (GtkContainer *container, GtkWidget    *focus_child)
{
#if 0
  g_return_if_fail (IS_PANED_MULTI (container));

  PanedMulti *paned = PANED_MULTI (container);
 
  if (focus_child == NULL)
    {
      GtkWidget *last_focus;
      GtkWidget *w;
      
      last_focus = paned_get_focus_widget (paned);

      if (last_focus)
	{
	  /* If there is one or more paned widgets between us and the
	   * focus widget, we want the topmost of those as last_focus
	   */
	  for (w = last_focus; w != GTK_WIDGET (paned); w = w->parent)
	    if (IS_PANED_MULTI (w))
	      last_focus = w;
	  
	  if (container->focus_child == paned->child1)
	    paned_multi_set_last_child_n_focus (paned, last_focus, 0);
	  else if (container->focus_child == paned->child2)
	    paned_multi_set_last_child_n_focus (paned, last_focus, 1);
	}
    }

  if (GTK_CONTAINER_CLASS (paned_multi_parent_class)->set_focus_child)
    (* GTK_CONTAINER_CLASS (paned_multi_parent_class)->set_focus_child) (container, focus_child);
#endif
}

static void
paned_multi_get_cycle_chain (PanedMulti *paned, GtkDirectionType direction, GList **widgets)
{
#if 0
  GtkContainer *container = GTK_CONTAINER (paned);
  GtkWidget *ancestor = NULL;
  GList *temp_list = NULL;
  GList *list;

  if (paned->in_recursion) return;

  g_assert (widgets != NULL);

  if (paned->last_child1_focus && !gtk_widget_is_ancestor (paned->last_child1_focus, GTK_WIDGET (paned)))
    {
      paned_multi_set_last_child_n_focus (paned, NULL, 0);
    }

  if (paned->last_child2_focus && !gtk_widget_is_ancestor (paned->last_child2_focus, GTK_WIDGET (paned)))
    {
      paned_multi_set_last_child_n_focus (paned, NULL, 1);
    }

  if (GTK_WIDGET(paned)->parent) ancestor = gtk_widget_get_ancestor (GTK_WIDGET (paned)->parent, TYPE_PANED_MULTI);

  /* The idea here is that temp_list is a list of widgets we want to cycle
   * to. The list is prioritized so that the first element is our first
   * choice, the next our second, and so on.
   *
   * We can't just use g_list_reverse(), because we want to try
   * paned->last_child?_focus before paned->child?, both when we
   * are going forward and backward.
   */
  if (direction == GTK_DIR_TAB_FORWARD)
    {
      if (container->focus_child == paned->child1)
	{
	  temp_list = g_list_append (temp_list, paned->last_child2_focus);
	  temp_list = g_list_append (temp_list, paned->child2);
	  temp_list = g_list_append (temp_list, ancestor);
	}
      else if (container->focus_child == paned->child2)
	{
	  temp_list = g_list_append (temp_list, ancestor);
	  temp_list = g_list_append (temp_list, paned->last_child1_focus);
	  temp_list = g_list_append (temp_list, paned->child1);
	}
      else
	{
	  temp_list = g_list_append (temp_list, paned->last_child1_focus);
	  temp_list = g_list_append (temp_list, paned->child1);
	  temp_list = g_list_append (temp_list, paned->last_child2_focus);
	  temp_list = g_list_append (temp_list, paned->child2);
	  temp_list = g_list_append (temp_list, ancestor);
	}
    }
  else
    {
      if (container->focus_child == paned->child1)
	{
	  temp_list = g_list_append (temp_list, ancestor);
	  temp_list = g_list_append (temp_list, paned->last_child2_focus);
	  temp_list = g_list_append (temp_list, paned->child2);
	}
      else if (container->focus_child == paned->child2)
	{
	  temp_list = g_list_append (temp_list, paned->last_child1_focus);
	  temp_list = g_list_append (temp_list, paned->child1);
	  temp_list = g_list_append (temp_list, ancestor);
	}
      else
	{
	  temp_list = g_list_append (temp_list, paned->last_child2_focus);
	  temp_list = g_list_append (temp_list, paned->child2);
	  temp_list = g_list_append (temp_list, paned->last_child1_focus);
	  temp_list = g_list_append (temp_list, paned->child1);
	  temp_list = g_list_append (temp_list, ancestor);
	}
    }

  /* Walk the list and expand all the paned widgets. */
  for (list = temp_list; list != NULL; list = list->next)
    {
      GtkWidget *widget = list->data;

      if (widget)
	{
	  if (IS_PANED_MULTI (widget))
	    {
	      paned->in_recursion = TRUE;
	      paned_multi_get_cycle_chain (PANED_MULTI (widget), direction, widgets);
	      paned->in_recursion = FALSE;
	    }
	  else
	    {
	      *widgets = g_list_append (*widgets, widget);
	    }
	}
    }

  g_list_free (temp_list);
#endif
}

static gboolean
paned_multi_cycle_child_focus (PanedMulti *paned, gboolean  reversed)
{
  GtkDirectionType direction = reversed? GTK_DIR_TAB_BACKWARD : GTK_DIR_TAB_FORWARD;

  /* ignore f6 if the handle is focused */
  if (gtk_widget_is_focus (GTK_WIDGET (paned))) return TRUE;
  
  /* we can't just let the event propagate up the hierarchy,
   * because the paned will want to cycle focus _unless_ an
   * ancestor paned handles the event
   */
  GList *cycle_chain = NULL;
  paned_multi_get_cycle_chain (paned, direction, &cycle_chain);

  GList *list;
  for (list = cycle_chain; list != NULL; list = list->next)
    if (gtk_widget_child_focus (GTK_WIDGET (list->data), direction))
      break;

  g_list_free (cycle_chain);
  
  return TRUE;
}

static void
get_child_panes (GtkWidget *widget, GList **panes)
{
  if (IS_PANED_MULTI (widget))
    {
      PanedMulti *paned = PANED_MULTI (widget);
      
      //get_child_panes (paned->child1, panes);
      *panes = g_list_prepend (*panes, widget);
      int i; for(i=0;i<paned->children->len;i++){
        get_child_panes (paned_multi_get_child_widget(i), panes);
      }
    }
  else if (GTK_IS_CONTAINER (widget))
    {
      gtk_container_foreach (GTK_CONTAINER (widget), (GtkCallback)get_child_panes, panes);
    }
}

static GList *
get_all_panes (PanedMulti *paned)
{
  PanedMulti *topmost = NULL;
  GList *result = NULL;
  GtkWidget *w;
  
  for (w = GTK_WIDGET (paned); w != NULL; w = w->parent) {
    if (IS_PANED_MULTI (w)) topmost = PANED_MULTI (w);
  }

  g_assert (topmost);

  get_child_panes (GTK_WIDGET (topmost), &result);

  return g_list_reverse (result);
}

static void
paned_multi_find_neighbours (PanedMulti *paned, PanedMulti **next, PanedMulti **prev)
{
  GList *all_panes;
  GList *this_link;

  all_panes = get_all_panes (paned);
  g_assert (all_panes);

  this_link = g_list_find (all_panes, paned);

  g_assert (this_link);
  
  if (this_link->next) *next = this_link->next->data;
  else                 *next = all_panes->data;

  if (this_link->prev) *prev = this_link->prev->data;
  else                 *prev = g_list_last (all_panes)->data;

  g_list_free (all_panes);
}

static gboolean
paned_multi_move_handle (PanedMulti *paned, GtkScrollType  scroll)
{
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    {
      gint old_position;
      gint new_position;
      gint increment;
      
      enum {
        SINGLE_STEP_SIZE = 1,
        PAGE_STEP_SIZE   = 75
      };
      
      new_position = old_position = paned_multi_get_position (paned);
      increment = 0;
      
      switch (scroll)
	{
	case GTK_SCROLL_STEP_LEFT:
	case GTK_SCROLL_STEP_UP:
	case GTK_SCROLL_STEP_BACKWARD:
	  increment = - SINGLE_STEP_SIZE;
	  break;
	  
	case GTK_SCROLL_STEP_RIGHT:
	case GTK_SCROLL_STEP_DOWN:
	case GTK_SCROLL_STEP_FORWARD:
	  increment = SINGLE_STEP_SIZE;
	  break;
	  
	case GTK_SCROLL_PAGE_LEFT:
	case GTK_SCROLL_PAGE_UP:
	case GTK_SCROLL_PAGE_BACKWARD:
	  increment = - PAGE_STEP_SIZE;
	  break;
	  
	case GTK_SCROLL_PAGE_RIGHT:
	case GTK_SCROLL_PAGE_DOWN:
	case GTK_SCROLL_PAGE_FORWARD:
	  increment = PAGE_STEP_SIZE;
	  break;
	  
	case GTK_SCROLL_START:
	  new_position = paned->min_position;
	  break;
	  
	case GTK_SCROLL_END:
	  new_position = paned->max_position;
	  break;

	default:
	  break;
	}

      if (increment)
	{
	  //if (is_rtl (paned)) increment = -increment;
	  
	  new_position = old_position + increment;
	}
      
      new_position = CLAMP (new_position, paned->min_position, paned->max_position);
      
      if (old_position != new_position)	paned_multi_set_position (paned, new_position);

      return TRUE;
    }

  return FALSE;
}

static void
paned_multi_restore_focus (PanedMulti *paned)
{
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    {
      if (paned->priv->saved_focus &&
	  GTK_WIDGET_SENSITIVE (paned->priv->saved_focus))
	{
	  gtk_widget_grab_focus (paned->priv->saved_focus);
	}
      else
	{
	  /* the saved focus is somehow not available for focusing,
	   * try
	   *   1) tabbing into the paned widget
	   * if that didn't work,
	   *   2) unset focus for the window if there is one
	   */
	  
	  if (!gtk_widget_child_focus (GTK_WIDGET (paned), GTK_DIR_TAB_FORWARD))
	    {
	      GtkWidget *toplevel = gtk_widget_get_toplevel (GTK_WIDGET (paned));
	      
	      if (GTK_IS_WINDOW (toplevel))
		gtk_window_set_focus (GTK_WINDOW (toplevel), NULL);
	    }
	}
      
      paned_multi_set_saved_focus (paned, NULL);
      paned_multi_set_first_paned (paned, NULL);
    }
}

static gboolean
paned_multi_accept_position (PanedMulti *paned)
{
  PF;
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    {
      paned->original_position = -1;
      paned_multi_restore_focus (paned);

      return TRUE;
    }

  return FALSE;
}


static gboolean
paned_multi_cancel_position (PanedMulti *paned)
{
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    {
      if (paned->original_position != -1)
      {
	    paned_multi_set_position (paned, paned->original_position);
	    paned->original_position = -1;
      }

      paned_multi_restore_focus (paned);
      return TRUE;
    }

  return FALSE;
}

static gboolean
paned_multi_cycle_handle_focus (PanedMulti *paned, gboolean  reversed)
{
#if 0
  PanedMulti *next, *prev;
  
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    {
      PanedMulti *focus = NULL;

      if (!paned->priv->first_paned)
	{
	  /* The first_pane has disappeared. As an ad-hoc solution,
	   * we make the currently focused paned the first_paned. To the
	   * user this will seem like the paned cycling has been reset.
	   */
	  
	  paned_multi_set_first_paned (paned, paned);
	}
      
      paned_multi_find_neighbours (paned, &next, &prev);

      if (reversed && prev &&
	  prev != paned && paned != paned->priv->first_paned)
	{
	  focus = prev;
	}
      else if (!reversed && next &&
	       next != paned && next != paned->priv->first_paned)
	{
	  focus = next;
	}
      else
	{
	  paned_multi_accept_position (paned);
	  return TRUE;
	}

      g_assert (focus);
      
      paned_multi_set_saved_focus (focus, paned->priv->saved_focus);
      paned_multi_set_first_paned (focus, paned->priv->first_paned);
      
      paned_multi_set_saved_focus (paned, NULL);
      paned_multi_set_first_paned (paned, NULL);
      
      gtk_widget_grab_focus (GTK_WIDGET (focus));
      
      if (!gtk_widget_is_focus (GTK_WIDGET (paned)))
	{
	  paned->original_position = -1;
	  focus->original_position = paned_multi_get_position (focus);
	}
    }
  else
    {
      GtkContainer *container = GTK_CONTAINER (paned);
      PanedMulti *focus;
      PanedMulti *first;
      PanedMulti *prev, *next;
      GtkWidget *toplevel;

      paned_multi_find_neighbours (paned, &next, &prev);

      if (container->focus_child == paned->child1)
	{
	  if (reversed)
	    {
	      focus = prev;
	      first = paned;
	    }
	  else
	    {
	      focus = paned;
	      first = paned;
	    }
	}
      else if (container->focus_child == paned->child2)
	{
	  if (reversed)
	    {
	      focus = paned;
	      first = next;
	    }
	  else
	    {
	      focus = next;
	      first = next;
	    }
	}
      else
	{
	  /* Focus is not inside this paned, and we don't have focus.
	   * Presumably this happened because the application wants us
	   * to start keyboard navigating.
	   */
	  focus = paned;

	  if (reversed)
	    first = paned;
	  else
	    first = next;
	}

      toplevel = gtk_widget_get_toplevel (GTK_WIDGET (paned));

      if (GTK_IS_WINDOW (toplevel))	paned_multi_set_saved_focus (focus, GTK_WINDOW (toplevel)->focus_widget);
      paned_multi_set_first_paned (focus, first);
      focus->original_position = paned_multi_get_position (focus); 

      gtk_widget_grab_focus (GTK_WIDGET (focus));
   }
#endif
  return TRUE;
}

static gboolean
paned_multi_toggle_handle_focus (PanedMulti *paned)
{
  /* This function/signal has the wrong name. It is called when you
   * press Tab or Shift-Tab and what we do is act as if
   * the user pressed Return and then Tab or Shift-Tab
   */
  if (gtk_widget_is_focus (GTK_WIDGET (paned)))
    paned_multi_accept_position (paned);

  return FALSE;
}

static int
find_child_widget(PanedMulti *paned, GtkWidget *child)
{
  int i; for(i=0;i<paned->children->len;i++){
    if(((paned_child*)g_ptr_array_index(paned->children, i))->widget == child) return i;
  }
  pwarn("child widget not found.");
  return -1;
}

static gboolean
window_is_handle(PanedMulti *paned, GdkWindow* window)
{
  int x = 0;
  int i; for(i=0;i<paned->children->len;i++){
    paned_multi_get_child(paned, i);
#ifdef HANDlES_HAVE_GDK_WINDOW
    if(paned_multi_get_handle(i) == window) return TRUE;
#endif
    x += 1;
    dbg(0, "  window=%p %p", paned_multi_get_handle(i), window);
  }
  dbg(0, "not handle");
  return FALSE;
}


static GdkWindow*
handle_lookup(PanedMulti *paned, GdkWindow* window)
{
  int i; for(i=0;i<paned->children->len;i++){
    if(paned_multi_get_handle(i) == window) return window;
  }
  return NULL;
}


#define __PANED_MULTI_C__
#include "gtk/gtkaliasdef.c"

//these fns from gtkcontainer are for testing only:

/**
 * gtk_container_propagate_expose:
 * @container: a #GtkContainer
 * @child: a child of @container
 * @event: a expose event sent to container
 *
 * When a container receives an expose event, it must send synthetic
 * expose events to all children that don't have their own #GdkWindows.
 * This function provides a convenient way of doing this. A container,
 * when it receives an expose event, calls gtk_container_propagate_expose() 
 * once for each child, passing in the event the container received.
 *
 * gtk_container_propagate_expose() takes care of deciding whether
 * an expose event needs to be sent to the child, intersecting
 * the event's area with the child area, and sending the event.
 * 
 * In most cases, a container can simply either simply inherit the
 * ::expose implementation from #GtkContainer, or, do some drawing 
 * and then chain to the ::expose implementation from #GtkContainer.
 **/
static void
my_gtk_container_propagate_expose (GtkContainer *container,	GtkWidget *child, GdkEventExpose *event)
{
  GdkEvent *child_event;

  g_return_if_fail (GTK_IS_CONTAINER (container));
  g_return_if_fail (GTK_IS_WIDGET (child));
  g_return_if_fail (event != NULL);

  g_assert (child->parent == GTK_WIDGET (container));
  
  if (GTK_WIDGET_DRAWABLE (child) &&
      GTK_WIDGET_NO_WINDOW (child) &&
      (child->window == event->window))
  {
      child_event = gdk_event_new (GDK_EXPOSE);
      child_event->expose = *event;
      g_object_ref (child_event->expose.window);

      child_event->expose.region = gtk_widget_region_intersect (child, event->region);
      if (!gdk_region_empty (child_event->expose.region))
      {
        gdk_region_get_clipbox (child_event->expose.region, &child_event->expose.area);
        dbg(0, "sending expose...");
        gtk_widget_send_expose (child, child_event);
      }
      else dbg(0, "region empty!");
      gdk_event_free (child_event);
  }
  else dbg(0, "ignoring... nowin=%i ourwin=%i childw=%p evw=%p", GTK_WIDGET_NO_WINDOW(child), child->window == event->window, child->window, event->window);
}

static void
my_gtk_container_expose_child (GtkWidget *child, gpointer client_data)
{
  struct {
    GtkWidget *container;
    GdkEventExpose *event;
  } *data = client_data;
  
  my_gtk_container_propagate_expose (GTK_CONTAINER (data->container), child, data->event);
}

static gint
my_gtk_container_expose (GtkWidget *widget, GdkEventExpose *event)
{
  struct {
    GtkWidget *container;
    GdkEventExpose *event;
  } data;

  g_return_val_if_fail (GTK_IS_CONTAINER (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  
  if (GTK_WIDGET_DRAWABLE (widget)) 
    {
      data.container = widget;
      data.event = event;
      
      gtk_container_forall (GTK_CONTAINER (widget), my_gtk_container_expose_child, &data);
    }   
  
  return FALSE;
}

