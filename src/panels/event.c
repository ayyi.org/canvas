/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 |  Unlike other windows, Event is always in Link mode, and doesnt have
 |  its own selection data.
 */

#define __event_window_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <model/part_manager.h>
#include <model/time.h>
#include <model/song.h>
#include "windows.h"
#include "support.h"
#include "event.h"

static GObject*    ayyi_event_win_construct       (GType, guint n_construct_properties, GObjectConstructParam*);
static void        ayyi_event_win_dispose         (GObject*);
static void        ayyi_event_win_finalize        (GObject*);

static void        event_win_on_ready             (AyyiPanel*);
static void        event_win_on_change            ();
static GtkWidget*  event_win_add_new_box          (EventWindow*, int type, const char* name, int length, gpointer);

static bool        event_on_edit_focus            (GtkWidget*, gpointer);
static gboolean    event_on_field_changed         (GtkWidget*, gpointer);
static gboolean    event_on_name_changed          (GtkWidget*, GdkEventFocus*, gpointer);
static bool        event_on_bbst_changed          (GtkWidget*, GdkEventFocus*, gpointer);
static bool        event_on_length_changed        (GtkWidget*, GdkEventFocus*, gpointer);
static bool        event_on_level_changed         (GtkWidget*, gpointer);
static void        event_on_part_selection_change (AyyiPanel*);
static void        event_on_part_selection_change2(GObject*, AyyiPanel*, EventWindow*);
static void        event_on_parts_change          (GObject*, gpointer);
static void        event_on_part_change           (GObject*, AMPart*, AMChangeType, AyyiPanel*, gpointer);

static EventField* event_field_lookup             (EventWindow*, GtkWidget*);

G_DEFINE_TYPE (AyyiEventWin, ayyi_event_win, AYYI_TYPE_PANEL)


static void
ayyi_event_win_class_init (AyyiEventWinClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	g_object_class->constructor = ayyi_event_win_construct;
	g_object_class->dispose = ayyi_event_win_dispose;
	g_object_class->finalize = ayyi_event_win_finalize;

	panel->name     = "Event";
	panel->accel    = GDK_F6;
	panel->on_ready = event_win_on_ready;

	g_signal_connect(song->parts, "change", G_CALLBACK(event_on_parts_change), NULL);
	g_signal_connect(song->parts, "item-changed", G_CALLBACK(event_on_part_change), NULL);
}


static GObject*
ayyi_event_win_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_event_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_EVENT_WIN);

	return g_object;
}


static void
ayyi_event_win_init (AyyiEventWin* object)
{
}


static void
ayyi_event_win_dispose (GObject *object)
{
#if 0 // disconnection done automatically by gtk?
	EventWindow* event_win = (EventWindow*)object;

	if(event_win->entry_label){
		if(event_win->handler_id){
			g_signal_handler_disconnect((gpointer)event_win->entry_label, event_win->handler_id);
			event_win->handler_id = 0;
		}
		event_win->entry_label = NULL;
	}
#endif

	#define song_handler_disconnect(FN) if(g_signal_handlers_disconnect_matched (song, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, object) != 1) pwarn("handler disconnection");
	song_handler_disconnect(event_on_part_selection_change2);
	#undef song_handler_disconnect

	G_OBJECT_CLASS (ayyi_event_win_parent_class)->dispose (object);
}


static void
ayyi_event_win_finalize (GObject* g_object)
{
	PF;

	G_OBJECT_CLASS(ayyi_event_win_parent_class)->finalize(g_object);
}


static void
event_win_on_ready (AyyiPanel* panel)
{
	AyyiEventWin* self = (AyyiEventWin*)panel;

	if(self->hbox) return;

	GtkWidget* h = self->hbox = gtk_hbox_new(FALSE, 2);
	panel_pack(h, EXPAND_TRUE);

	//TODO change callbacks to all use event_on_field_changed.
	event_win_add_new_box(self, EF_NAME,     "name",    20, event_on_name_changed);
	event_win_add_new_box(self, EF_TIME,     "pos",     11, event_on_bbst_changed);
	event_win_add_new_box(self, EF_LEN,      "length",  11, event_on_length_changed);
	event_win_add_new_box(self, EF_LEVEL,    "level",    5, event_on_level_changed);
	event_win_add_new_box(self, EF_FADE_IN,  "fade in",  4, event_on_field_changed);
	event_win_add_new_box(self, EF_FADE_OUT, "fade out", 4, event_on_field_changed);

	g_signal_connect(am_parts, "selection-change", G_CALLBACK(event_on_part_selection_change2), self);

	event_win_on_change();
}


static void
event_win_on_change ()
{
	void event_update()
	{
		//refreshes all open event windows to reflect current song selection.

		//note: this gets called whenever the mouse leaves the event window thus
		//      preventing having to 'set' the change to text fields.

		char text[EF_MAX][128] = {{0,}};

		event_foreach {
			g_return_if_fail(event->hbox/* && GTK_IS_ENTRY(event->entry_label)*/);

			//Lets deal with parts first:
			int part_count = g_list_length(am_parts_selection);
			if(part_count > 1){
				sprintf(text[EF_LEN], "%d parts selected.", part_count);
				strcpy(text[EF_NAME], "*");
				gtk_editable_set_editable(GTK_EDITABLE(event->fields[EF_NAME].entry), FALSE); //?? working? no...
			}
			else if(part_count == 0){
				strcpy(text[EF_LEN], " ");
				strcpy(text[EF_NAME], "-");
				strcpy(text[EF_TIME], " ");
				gtk_editable_set_editable(GTK_EDITABLE(event->fields[EF_NAME].entry), FALSE);
			}
			else{
				//1 part is selected.
				AMPart* part = am_parts_selection->data;
				ayyi_pos2bbss(&part->start, text[EF_TIME]);

				g_strlcpy(text[EF_NAME], part->name, AYYI_NAME_MAX);

				am_part__get_length_bbst(part, text[EF_LEN]);

				sprintf(text[EF_LEVEL],  "%.2f", am_part__get_level   (part));
				sprintf(text[EF_FADE_IN],  "%i", am_part__get_fade_in (part));
				sprintf(text[EF_FADE_OUT], "%i", am_part__get_fade_out(part));

				gtk_editable_set_editable(GTK_EDITABLE(event->fields[EF_NAME].entry), TRUE);
			}

			for(int i=0;i<EF_MAX;i++){
				gtk_entry_set_text(GTK_ENTRY(event->fields[i].entry), text[i]);
				g_strlcpy(event->fields[i].ref, text[i], 64);
			}
		} end_event_foreach
	}

	static guint update_id = 0;
	if(update_id) return;

	gboolean
	event_windows_update (AyyiPanel* active_panel)
	{
		event_update();

		update_id = 0; //show update queue has been cleared.
		return false;  //dont run again.
	}

	update_id = g_idle_add((GSourceFunc)event_windows_update, app->active_panel);
}


/*
 *  Make widgets for a single event window item. Vbox, label and text entry.
 */
static GtkWidget*
event_win_add_new_box (EventWindow* event, int type, const char* name, int length, gpointer callback)
{
	GtkWidget* vbox = gtk_vbox_new(FALSE, 2);
	gtk_container_add(GTK_CONTAINER(event->hbox), vbox);

	// alignment box for the label
	GtkWidget* align = gtk_alignment_new(0.0, 0.5, 0.0, 0.0);
	gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, TRUE, 0);

	// label
	GtkWidget* label = gtk_label_new(name);
	gtk_container_add(GTK_CONTAINER(align), label);

	GtkWidget* entry = event->fields[type].entry = gtk_entry_new();
	gtk_entry_set_width_chars(GTK_ENTRY(entry), length);
	g_signal_connect((gpointer)entry, "focus-in-event",  G_CALLBACK(event_on_edit_focus), event);
	g_signal_connect((gpointer)entry, "focus-out-event", G_CALLBACK(callback),            event);
	gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, TRUE, 0);

	return entry;
}


static bool
event_on_edit_focus (GtkWidget* widget, gpointer user_data)
{
	gtk_window_remove_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);
	return false;
}


static gboolean
event_on_name_changed (GtkWidget* widget, GdkEventFocus* event, gpointer user_data)
{
	EventWindow* panel = (EventWindow*)user_data;
	g_return_val_if_fail(AYYI_IS_EVENT_WIN(panel), false);

	const char* text = gtk_entry_get_text(GTK_ENTRY(widget));

	if(strcmp(text, panel->fields[EF_NAME].ref)){
		// update the part data
		if(g_list_length(am_parts_selection) == 1){
			AMPart* part = am_parts_selection->data;
			am_part_rename(part, text, NULL, NULL);
		}

		GtkWindow* window = GTK_WINDOW(gtk_widget_get_toplevel(widget));
		if(window) gtk_window_add_accel_group(window, app->global_accel_group);

		g_strlcpy(panel->fields[EF_NAME].ref, text, 64);
	}
	return false;
}


static bool
event_on_bbst_changed (GtkWidget* widget, GdkEventFocus* event, gpointer user_data)
{
	PF;
	EventWindow* panel = (EventWindow*)user_data;

	const char* text = gtk_entry_get_text(GTK_ENTRY(widget));

	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	if(!strcmp(text, panel->fields[EF_TIME].ref)) return false; // nothing to do.

	AyyiSongPos pos;
	if(bbss2pos(gtk_entry_get_text(GTK_ENTRY(widget)), &pos)){
		if(g_list_length(am_parts_selection) == 1){
			AMPart* part = am_parts_selection->data;
			am_part_move(part, &pos, NULL, NULL, NULL);
		}
	}

	g_strlcpy(panel->fields[EF_TIME].ref, text, 64);

	return false;
}


static bool
event_on_length_changed (GtkWidget* widget, GdkEventFocus* e, gpointer user_data)
{
	PF;
	EventWindow* event = (EventWindow*)user_data;
	if(!is_event_window(event)) return false;

	EventField* field = &event->fields[EF_LEN];
	if(!field) return false;

	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	const char* text = gtk_entry_get_text(GTK_ENTRY(widget));
	char* old_text = field->ref;

	if(!strcmp(text, old_text)) return false; // nothing to do.

	dbg(1, "old_text=%s new=%s", old_text, text);

	GPos len;
	if(bbst2pos(text, &len)){
		if(g_list_length(am_parts_selection) == 1){
			AMPart* part = am_parts_selection->data;
			songpos_gui2ayyi(&part->length, &len);
			am_part_set_length(part);
		}
	}

	g_strlcpy(old_text, text, 64);

	return false;
}


static bool
event_on_level_changed (GtkWidget* widget, gpointer user_data)
{
	PF;
	EventWindow* event = (EventWindow*)windows__get_panel_from_widget(widget);

	EventField* field = event_field_lookup(event, widget);
	if(!field) return false;

	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	const char* text     = gtk_entry_get_text(GTK_ENTRY(widget));
	char*       old_text = field->ref;

	if(!strcmp(text, old_text)) return false; // nothing to do.

	dbg(1, "old_text=%s new=%s", old_text, text);

	float level = atof(text);

	if(g_list_length(am_parts_selection) == 1){
		AMPart* part = am_parts_selection->data;
		am_part_set_level_async(part, level);
	}

	g_strlcpy(field->ref, text, 64); // update the reference text

	return false;
}


static gboolean
event_on_field_changed (GtkWidget* widget, gpointer user_data)
{
	PF;
	EventWindow* event = (EventWindow*)windows__get_panel_from_widget(widget);

	gtk_window_add_accel_group(GTK_WINDOW(gtk_widget_get_toplevel(widget)), app->global_accel_group);

	EventField* field = event_field_lookup(event, widget);
	if(!field) return false;

	const char* text = gtk_entry_get_text(GTK_ENTRY(widget));
	char*   old_text = field->ref;

	if(!strcmp(text, old_text)) return FALSE; // nothing to do.

	dbg(1, "old_text=%s new=%s", field->ref, text);

	int fade_time = atoi(text);

	if(g_list_length(am_parts_selection) == 1){
		AMPart* part = am_parts_selection->data;
		if(field == &event->fields[EF_FADE_IN]){
			am_part_set_fadein_async(part, fade_time);
		} else if (field == &event->fields[EF_FADE_OUT]){
			am_part_set_fadeout_async(part, fade_time);
		}
	}

	g_strlcpy(old_text, text, 64); // update the reference text

	return false;
}


static EventField*
event_field_lookup (EventWindow* event, GtkWidget* widget)
{
	GtkWidget* vbox = gtk_widget_get_parent(widget);
	GList* children = gtk_container_get_children ((GtkContainer*)gtk_widget_get_parent(vbox));
	int t = -1;
	int i = 0;
	for(GList* l=children;l;l=l->next,i++){
		if(((GtkWidget*)l->data) == vbox) t = i;
	}
	g_list_free(children);

	return t > -1 ? &event->fields[t] : NULL;
}


static void
event_on_part_selection_change (AyyiPanel* panel)
{
	event_win_on_change();
}


static void
event_on_part_selection_change2 (GObject* am_song, AyyiPanel* sender, EventWindow* event)
{
	if((AyyiPanel*)event != sender){
		event_on_part_selection_change((AyyiPanel*)event);
	}
}


static void
event_on_parts_change (GObject* object, gpointer user_data)
{
	// this was originally intended for part addition and deletion
	// but it isnt very fine grained.
	// event_on_part_change() is preferred, but cant be used for parts that have been deleted.
	PF;

	// TODO move these to separate signal handlers:
	event_win_on_change();
}


static void
event_on_part_change (GObject* song, AMPart* part, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	PF;

	event_win_on_change();
}

