/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __plugin_window_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>

#include "model/channel.h"
#include "model/am_message.h"
#include "windows.h"
#include "plugin.h"
#include "mixer.h"
#include "song.h"
#include "support.h"

extern SMConfig* config;
extern GHashTable* plugin_icons;

enum
{
  COL_ICON = 0,
  COL_NAME,
  COL_CATEGORY,
  COL_N_IN,
  COL_N_OUT,
  COL_IDX,
  NUM_COLS
};

static GObject*      plugins_construct              (GType, guint n_construct_properties, GObjectConstructParam*);
static void          plugins_on_ready               (AyyiPanel*);
static GtkTreeModel* plugins_create_model           ();
static void          plugins_load_icons             ();
static void          plugin_window_drag_dataget     (GtkWidget*, GdkDragContext*, GtkSelectionData*, guint, guint, AyyiPanel*);
static gboolean      plugin_treeview_on_button_press(GtkWidget*, GdkEventButton*, gpointer);

GtkTreeStore* plugin_treestore;

G_DEFINE_TYPE (AyyiPluginWin, ayyi_plugin_win, AYYI_TYPE_PANEL)


static void
ayyi_plugin_win_class_init (AyyiPluginWinClass *klass)
{
	song->plugins = NULL; //cough!

	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	g_object_class->constructor = plugins_construct;

	panel->name           = "Plugin";
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->min_size.x     = 20;
	panel->min_size.y     = 20;
	panel->accel          = GDK_F10;
	panel->default_size.x = 360;
	panel->default_size.y = 400;
	panel->on_ready       = plugins_on_ready;

	plugin_treestore = GTK_TREE_STORE(plugins_create_model());

	plugins_load_icons();
}


static GObject*
plugins_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	AYYI_DEBUG_1 printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_plugin_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_PLUGIN_WIN);

	return g_object;
}


static void
ayyi_plugin_win_init (AyyiPluginWin* object)
{
}


void
plugins_free ()
{
	GList* l = song->plugins;
	for(;l;l=l->next) free(l->data);
	g_list_free(song->plugins);
	song->plugins = NULL;
}


gboolean
pluginbox_on_activate (GtkComboBox* combo_box, gpointer data)
{
	PF;
	return TRUE;
}


static GtkTreeModel*
plugins_create_model ()
{
	GtkTreeStore* treestore = plugin_treestore = gtk_tree_store_new(NUM_COLS, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	return GTK_TREE_MODEL(treestore);
}


void
plugin_model_update ()
{
	PF;
	GtkTreeIter iter;

	gtk_tree_store_clear (plugin_treestore);

#ifdef DEBUG
	void print_icon(gpointer key, gpointer val, gpointer user_data){ dbg (0, "key=%s val=%p", (char*)key, val); }
	if(_debug_ > 1) g_hash_table_foreach(plugin_icons, print_icon, NULL);
#endif

	GList* l = song->plugins;
	for(;l;l=l->next){
		AMPlugin* plugin = l->data;
		AyyiPlugin* shared = ayyi_plugin_at(plugin->shm_num);
		g_return_if_fail(shared);

		GdkPixbuf* icon = am_plugin__get_icon(plugin);

		gtk_tree_store_append (plugin_treestore, &iter, NULL);
		gtk_tree_store_set (plugin_treestore, &iter,
	                        COL_ICON, icon,
	                        COL_NAME, plugin->name,
	                        COL_CATEGORY, "vst",
	                        COL_N_IN, shared->n_inputs,
	   	                    COL_N_OUT, shared->n_outputs,
	                        -1);
	}
}


static void
plugins_on_ready (AyyiPanel* panel)
{
	PluginWindow* window = (PluginWindow*)panel;

	if(window->treestore) return;

	window->treestore = plugin_treestore;

	GtkWidget* scrollwin = gtk_scrolled_window_new(NULL, NULL); //adjustments created automatically.
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show(scrollwin);

	//----------------------------------------------

	//wrap the model to make the view independently sortable:
	GtkTreeModel* sort_model = gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(plugin_treestore));

	GtkWidget* treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(sort_model));
	gtk_widget_show(treeview);
	gtk_container_add(GTK_CONTAINER(scrollwin), treeview);

	// column 0 - icon
	GtkCellRenderer* cell0 = gtk_cell_renderer_pixbuf_new();
	GtkTreeViewColumn* col0 = gtk_tree_view_column_new_with_attributes("", cell0, "pixbuf", COL_ICON, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col0); //pack treeview-column into the treeview
	gtk_tree_view_column_set_fixed_width(col0, 16);

	// column 1
	GtkTreeViewColumn* col1 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col1, "Plugin");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col1); //pack treeview-column into the treeview
	GtkCellRenderer* renderer1 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_set_resizable(col1, TRUE);
	gtk_tree_view_column_pack_start(col1, renderer1, TRUE);     //pack cellrenderer into treeview-column
	gtk_tree_view_column_set_sort_column_id(col1, COL_NAME);

	gtk_tree_view_column_add_attribute(col1, renderer1, "text", COL_NAME);

	// column2
	GtkTreeViewColumn* col2 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col2, "Category");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col2);
	GtkCellRenderer* renderer2 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col2, renderer2, TRUE);
	gtk_tree_view_column_set_resizable(col2, TRUE);

	gtk_tree_view_column_add_attribute(col2, renderer2, "text", COL_CATEGORY);

	// num inputs
	GtkTreeViewColumn* col3 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col3, "In");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col3);
	GtkCellRenderer* renderer3 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col3, renderer3, TRUE);
	gtk_tree_view_column_add_attribute(col3, renderer3, "text", COL_N_IN);

	// num outputs
	GtkTreeViewColumn* col4 = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col4, "Out");
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), col4);
	GtkCellRenderer* renderer4 = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col4, renderer4, TRUE);
	gtk_tree_view_column_add_attribute(col4, renderer4, "text", COL_N_OUT);

	gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(sort_model), COL_NAME, GTK_SORT_ASCENDING);

	//----------------------------------------------

	// set up treeview as a dnd source
	gtk_drag_source_set(treeview, GDK_BUTTON1_MASK | GDK_BUTTON2_MASK, app->dnd.file_drag_types, app->dnd.file_drag_types_count, GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);
	g_signal_connect (G_OBJECT(treeview), "drag-data-get", G_CALLBACK(plugin_window_drag_dataget), panel);

	//----------------------------------------------

	g_signal_connect(treeview, "button-press-event", (GCallback)plugin_treeview_on_button_press, NULL);

	panel_pack(scrollwin, EXPAND_TRUE);

	plugin_model_update();
}


static void
plugins_load_icons ()
{
	am_plugins.map  = g_hash_table_new(g_str_hash, g_str_equal);

	// hash names should not contain spaces - use underscore instead. Lookup should match any whitescape.

	g_hash_table_insert(am_plugins.map, "2016_ste", "sr2016");
	g_hash_table_insert(am_plugins.map, "b4",       "ni");
	g_hash_table_insert(am_plugins.map, "b4fx",     "ni");
	g_hash_table_insert(am_plugins.map, "bandpass", "grm");
	g_hash_table_insert(am_plugins.map, "bassline", "audiorealism");
	g_hash_table_insert(am_plugins.map, "beast",    "refx");
	g_hash_table_insert(am_plugins.map, "blue_tube","nomad_factory");
	g_hash_table_insert(am_plugins.map, "cube",     "virsyn");
	g_hash_table_insert(am_plugins.map, "redoptor", "d16");
	g_hash_table_insert(am_plugins.map, "dfx_geom", "dfx");
	g_hash_table_insert(am_plugins.map, "dfx_tran", "dfx");
	g_hash_table_insert(am_plugins.map, "crystal",  "green_oak");
	g_hash_table_insert(am_plugins.map, "izotope_", "izotope");
	g_hash_table_insert(am_plugins.map, "kore",     "ni");
	g_hash_table_insert(am_plugins.map, "kore_fx",  "ni");
	g_hash_table_insert(am_plugins.map, "northpol", "prosoniq");
	g_hash_table_insert(am_plugins.map, "ohmboyz_", "ohmboyz");
	g_hash_table_insert(am_plugins.map, "pitchacc", "grm");
	g_hash_table_insert(am_plugins.map, "psp_vint", "psp");
	g_hash_table_insert(am_plugins.map, "quadrasi", "refx");
	g_hash_table_insert(am_plugins.map, "studio_ch","nomad_factory");
	g_hash_table_insert(am_plugins.map, "tc_nativ", "tc");
	g_hash_table_insert(am_plugins.map, "trilogy",  "ni");
	g_hash_table_insert(am_plugins.map, "saffirea", "focusrite");
	g_hash_table_insert(am_plugins.map, "saffirec", "focusrite");
	g_hash_table_insert(am_plugins.map, "saffiree", "focusrite");
	g_hash_table_insert(am_plugins.map, "saffirer", "focusrite");
	g_hash_table_insert(am_plugins.map, "sir_086",  "sir");
	g_hash_table_insert(am_plugins.map, "spectra",  "kjaerhus");
	g_hash_table_insert(am_plugins.map, "urs_1970", "urs");
	g_hash_table_insert(am_plugins.map, "urs_1975", "urs");

	GList* files = NULL;
	GList* dirs  = NULL;
	char* icons_dir = g_build_path("/", config->svgdir, "../plugins", NULL);
	if(!g_file_test(icons_dir, G_FILE_TEST_EXISTS)) pwarn("dir doesnt exist: %s", icons_dir);
	filelist_read(icons_dir, &files, &dirs);

	GList* l = files;
	for (;l;l=l->next) {
		char* path = l->data;
		gchar* basename = g_path_get_basename(path);
		char* stripped = malloc(256); // stored in the hashtable. Never free'd.
		filename_remove_extension(basename, stripped);

		GdkPixbuf* pixbuf = gdk_pixbuf_new_from_file(path, NULL);

		dbg(2, "  icon=%s pixbuf=%p", stripped, pixbuf);
		g_hash_table_insert(plugin_icons, stripped, pixbuf);

		g_free(path);
		g_free(basename);
	}
	g_list_free(files);
	g_list_free(dirs);
	g_free(icons_dir);
}

/*
 *   For _outgoing_ drags.
 */
static void
plugin_window_drag_dataget (GtkWidget* widget, GdkDragContext* drag_context, GtkSelectionData* data, guint info, guint time, AyyiPanel* window)
{
	PF;

	char text[256], row_str[256]="";
	uint32_t plugin_idx = 0;
	GtkTreeIter iter;

	g_return_if_fail(window);
	PluginWindow* plugin_win = (PluginWindow*)window;

	// which rows are selected?
	GtkTreeModel* model = (GtkTreeModel*)plugin_win->treestore;
	GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
	GList* selected_rows = gtk_tree_selection_get_selected_rows(selection, &(model));

	// get the data associated with each of the selected rows:
	GList* row = selected_rows;
	for(; row; row=row->next){
		GtkTreePath* treepath_selection = row->data;
		gtk_tree_model_get_iter(model, &iter, treepath_selection);
		gtk_tree_model_get(model, &iter, COL_IDX, &plugin_idx, -1);
		snprintf(row_str, 255, "plugin=%u", plugin_idx + 1);
	}

	// free the selection list data
	g_list_foreach(selected_rows, (gpointer)gtk_tree_path_free, NULL);
	g_list_free(selected_rows);

	// to send multiple items append them separated by newlines.
	snprintf(text, 255, "plugin:%.245s%c%c", row_str, 13, 10);

	if(!strlen(row_str)){
		// return a dock string instead of a plugin
		char win_id[STRLEN_WIN_ID];
		ayyi_panel_get_id_string(window, win_id);
		snprintf(text, 255, "dock:%s%c%c", win_id, 13, 10);
	}

	gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, _8_BITS_PER_CHAR, (unsigned char*)text, strlen(text));
}


static gboolean
plugin_treeview_on_button_press (GtkWidget* treeview, GdkEventButton* event, gpointer userdata)
{
	PF;
	if(event->type == GDK_BUTTON_PRESS  &&  event->button == 1){
		GtkTreePath* path;
		if(gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview), (gint) event->x, (gint) event->y, &path, NULL, NULL, NULL)){
			gtk_tree_path_free(path);
		}else{
			dbg(0, "no path! FIXME start dnd?");
		}
	}
	return NOT_HANDLED;
}


