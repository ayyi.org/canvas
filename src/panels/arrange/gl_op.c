/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2010-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "arrange/automation_menu.h"

static AutoSelection automation_pick (AGlActor*, TrackDispNum, ArrTrk*, AGliPt);


static AGlActor*
arr_gl_op_actor (GtkWidget* panel)
{
	// Actor for the main canvas area. Acts as a container and viewport, and handles events.

	Arrange* arrange = (Arrange*)panel;

	void arr_gl_op_pick (AGlActor* actor, Arrange* arrange, AGliPt* xy, TrackDispNum* td, AMPart** part)
	{
		// not using canvas->pick because the scene graph has already adjusted for the track control and viewport

		AyyiSongPos pos;
		arr_px2spos(arrange, &pos, xy->x);

		if (td) {
			TrackDispNum d = arr_px2trk(arrange, xy->y);
			dbg(2, "d=%i", d);
			if (d > -1 && d < AM_MAX_TRKX) {
				*td = d;
				ArrTrk* atr = track_list__track_by_display_index(arrange->priv->tracks, d);
				*part = (AMPart*)am_partmanager__find_by_position(atr->track, &pos);
			}
		}
	}

	bool op_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		// x can be negative during drag operations

		Arrange* arrange = actor->root->user_data;
		AyyiArrangeClass* klass = AYYI_ARRANGE_GET_CLASS(arrange);
		CanvasOp* op = arrange->canvas->op;

		if (op->arrange != arrange){
			op->arrange = arrange;
		}

		AGliPt xy_world = {xy.x - actor->scrollable.x1, xy.y - actor->scrollable.y1};

		op->xscrolloffset = -actor->scrollable.x1;
		op->yscrolloffset = -actor->scrollable.y1;
		op->mouse.x = xy_world.x;
		op->mouse.y = xy_world.y;

		static bool pressed = false;
		static int old_cursor = 0;
		int cursor = 0;
		bool shift   = event->button.state & GDK_SHIFT_MASK;
		bool control = event->button.state & GDK_CONTROL_MASK;
		int handled = NOT_HANDLED;

		// warning! sometimes event->button.y=0 when event is type GDK_BUTTON_PRESS
		if (_debug_ && event->button.y == 0) pwarn("button.y not set");

		// get the part we are over, but only if we are not in the middle of an op.
		TrackDispNum track = -1;
		AMPart* part = NULL;
		// TODO this is messy - can we move this into op.new() ?
		if (!op->type)
			arr_gl_op_pick(actor, arrange, &xy_world, &track, &part);
		else
			part = op->part;

		AGlActor* track_actor = NULL;
		if (track > -1) {
			AGlActor* tracks = CGL->actors[ACTOR_TYPE_TRACKS];

			ArrTrk* old = arrange->automation.hover.track;
			arrange->automation.hover = automation_pick(actor, track, arrange->priv->tracks->display[track], xy_world);

			if (arrange->automation.hover.track) {
				track_actor = agl_actor__pick_child (tracks, (AGlfPt){xy_world.x, xy_world.y});
			}

			if (track_actor) {
				agl_actor__invalidate (track_actor);
			}

			if (arrange->automation.hover.track != old) {
				GList* l = tracks->children;
				for (;l;l=l->next) {
					AGlActor* child = l->data;
					TrackActor* ta = (TrackActor*)child;
					if (ta->atr == old) {
						agl_actor__invalidate (child);
						break;
					}
				}
			}
		}

		if (part) {
			if (part != op->part) {
				((GlCanvasOp*)op)->original_track = part->track;
				op->part = part;
			}
			op->p1.x = arr_gl_pos2px_(arrange, &op->part->start);
			op->p2.x = arr_gl_pos2px_(arrange, &op->part->length) + op->p1.x;
			ArrTrk* atr = track_list__lookup_track(arrange->priv->tracks, part->track); // track will be wrong during move op?
			if (atr) {
				op->p1.y = atr->y * vzoom(arrange); // these y values are only valid if the part can be dragged inbetween tracks.
				op->p2.y = (atr->y + atr->height) * vzoom(arrange);
			}
		}
		else op->part = NULL;

		if (arrange->automation.hover.track) {
			AyyiSongPos pos;
			arr_gl_px2songpos(arrange, &pos, op->mouse.x);
			shell__statusbar_print(((AyyiPanel*)arrange)->window, 3,
				"%s %i %.1f",
				arrange->automation.hover.type == VOL ? "VOL" : "PAN",
				arrange->automation.hover.subpath,
				am_automation_get_value (((Curve**)&arrange->automation.hover.track->track->bezier)[arrange->automation.hover.type], arrange->automation.hover.subpath, ayyi_pos2samples(&pos))
			);
		} else if (op->part) {
			shell__statusbar_print(((AyyiPanel*)arrange)->window, 3, "%s", op->part->name);
		} else {
			AyyiSongPos pos;
			char bbst[AYYI_BBST_MAX];
			shell__statusbar_print(((AyyiPanel*)arrange)->window, 3, "%.0f %.0f (%s)", op->mouse.x, op->mouse.y, ayyi_pos2bbss(arr_gl_px2songpos(arrange, &pos, op->mouse.x), bbst));
		}

		switch (event->type) {
			case GDK_ENTER_NOTIFY:
				set_cursor(actor->root->gl.gdk.widget->window, klass->toolbox[arrange->toolbox.current].cursor);
				break;
			case GDK_2BUTTON_PRESS:
				if (!part) {
					uint64_t len = AYYI_MU_PER_BEAT;
					ArrTrk* atr = track_list__track_by_display_index(arrange->priv->tracks, track);
					AyyiSongPos pos;
					arr_px2spos(arrange, &pos, xy.x);
					am_song__add_part(AM_TRK_IS_AUDIO(atr->track) ? AYYI_AUDIO : AYYI_MIDI, -1, (AM_TRK_IS_AUDIO(atr->track) && song->pool->list) ? song->pool->list->data : NULL, atr->track->ident.idx, &pos, len, 0, NULL, 0, NULL, NULL);
				}
				break;
			case GDK_BUTTON_PRESS:
				dbg (1, "BUTTON_PRESS");
				gtk_widget_grab_focus(CGL->image);
				arrange->automation.sel =arrange->automation.hover;
				switch (event->button.button) {
					case 3:
						if (arrange->automation.hover.track)
							automation_menu_popup(arrange, event, op->mouse);
						else if (op->part)
							gtk_menu_popup(GTK_MENU(arrange->partmenu), NULL, NULL, NULL, NULL, event->button.button, (guint32)(event->button.time));
						else
							gtk_menu_popup(GTK_MENU(AYYI_ARRANGE_GET_CLASS(arrange)->rootmenu), NULL, NULL, toolbox_position, event, event->button.button, (guint32)(event->button.time));
						pressed = false;
						break;
					case 1:
						if (arrange->automation.hover.track) {
							gl_canvas_op__new(arrange, OP_AUTO_DRAG);
							op->ft.automation.track = track_actor;
							op->op->press(op, event);
						} else if (op->part) {
							switch (arrange->toolbox.current) {
								case TOOL_DEFAULT:
									if (shift) {
										arr_part_selection_add_range(arrange, op->part, (AMPart*)arrange->part_selection->data); //this is relative to first part selected - should be the last?
									} else if (control) {
										arr_part_selection_add(arrange, op->part);
									} else {
										// the part grabs the selection unless it is part of an existing selection)
										if(!arr_part_is_selected(arrange, op->part)){
											arr_part_selection_reset(arrange, true);
											arr_part_selection_add(arrange, op->part);
										}
										set_cursor_delayed(arrange->canvas->widget->window, GDK_FLEUR);
									}
									break;
								case TOOL_SCISSORS:
									gl_canvas_op__new(arrange, OP_SPLIT);
									break;
							}
						}

						pressed = true;
						break;
				}
				handled = HANDLED;
				break;
			case GDK_MOTION_NOTIFY:
				if (arrange->automation.hover.track) {
					if (pressed) {
					}
				} else if (op->part) {
					if (pressed) {
						// a drag op.
						// the op is not started until the cursor is actually moved.
																							// this is really annoying. would be better to create the op in BUTTON_PRESS
						if (!op->type) {
							gl_canvas_op__new(arrange, get_optype(arrange, pick_feature_type(op), event->button.state));
							dbg(1, "MOTION: optype=%s", canvas_op__print_type(op));
							op->op->press(op, event);
						}
					} else {
						double start = arr_gl_pos2px_(arrange, &op->part->start);
						double length = arr_gl_pos2px_(arrange, &op->part->length);
						double end = start + length;
						if (op->mouse.x - start < 3) {
							cursor = CURSOR_H_DOUBLE_ARROW;
						} else if (end - op->mouse.x < 3){
							cursor = CURSOR_H_DOUBLE_ARROW;
						} else {
							cursor = CURSOR_NORMAL;
						}
					}
				} else {
					// not over a Part
					if (pressed && (op->mouse.y > 0.0)) {
						if (!op->type) {
							gl_canvas_op__new(arrange, optype_from_tooltype(arrange->toolbox.current));
							dbg(1, "non-part op: optype=%s", canvas_op__print_type(op));
							op->op->press(op, event);
						}
					}

					cursor = CURSOR_NORMAL;
				}

				((GlCanvasOp*)op)->event = (Pti){event->button.x, event->button.y};
				op->op->motion(op);
				break;
			case GDK_BUTTON_RELEASE:
				dbg (1, "GDK_BUTTON_RELEASE %s", canvas_op__print_type(op));
				pressed = false;
				op->op->finish(op);
				handled = HANDLED;
				break;
			case GDK_LEAVE_NOTIFY:
				op->mouse.x = -1;
				set_cursor(actor->root->gl.gdk.widget->window, CURSOR_NORMAL);
				break;
			case GDK_SCROLL:
				;GdkEventScroll* ev = (GdkEventScroll*)event;
				if     (ev->direction == GDK_SCROLL_UP)   arrange->canvas->scroll_up  (arrange, 12);
				else if(ev->direction == GDK_SCROLL_DOWN) arrange->canvas->scroll_down(arrange, 12);
				break;
			default:
				break;
		}

		if (cursor != old_cursor) set_cursor(arrange->canvas->widget->window, old_cursor = cursor);

		return handled;
	}

	void op_set_size (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		ArrTrk* last_track = arrange->priv->tracks->track[am_track_list_count(song->tracks) - 1];
		g_return_if_fail(last_track);

		actor->region.x2 = agl_actor__width(actor->parent);
		actor->region.y2 = agl_actor__height(actor->parent);

		actor->scrollable.x2 = arr_gl_pos2px_(arrange, &(AyyiSongPos){am_object_val(&song->loc[AM_LOC_END]).sp.beat,})
			+ ARR_SONG_END_MARGIN
			+ actor->scrollable.x1;
		actor->scrollable.y2 = (last_track->y + last_track->height) * vzoom(arrange);
	}

	return agl_actor__new(AGlActor,
		.region = (AGlfRegion){arrange->tc_width.val.i, arrange->ruler_height, 4096, 2024},
		.on_event = op_on_event,
		.name = g_strdup("Op"),
		.set_size = op_set_size
	);
}


static AGlActor*
arr_gl_foreground (GtkWidget* panel)
{
	return agl_actor__new(AGlActor,
		.region = (AGlfRegion){.x2 = 4096, 2024},
		.name = g_strdup("Foreground")
	);
}


static AutoSelection
automation_pick (AGlActor* actor, TrackDispNum d, ArrTrk* atr, AGliPt xy)
{
	AMTrack* tr = atr->track;
	Arrange* arrange = atr->arrange;

	for (AyyiAutoType c=0;c<AUTO_MAX;c++) {
		Curve* cv = ((Curve**)&tr->bezier)[c];
		if (!cv || !cv->len) continue;

		ArrAutoPath* aap = AUTO_PATH(c);
		if (aap && !aap->hidden) {
			iRange xrange = {0, arr_px2samples(arrange, actor->region.x2 - actor->scrollable.x1)};

			AGlfPt scale = {
				.x = arr_samples2px(atr->arrange, 1),
				.y = vzoom(arrange) * atr->height
			};
			#define Y_TO_PX(Y) (scale.y * (100. - Y) / 100.)

			BPath* path = cv->path;
			BPath* p = (BPath*)&path[0];

			BPath prev_path0 = {CURVETO, .x3 = p->x3 - 20.0, p->y3};
			BPath* pp = &prev_path0;

			for (int s=0; s<cv->len; s++) {
				p = (BPath*)&path[s];

				AGlfPt p1 = (AGlfPt){
					scale.x * pp->x3,
					Y_TO_PX(pp->y3)
				};
				AGlfPt p2 = (AGlfPt){
					scale.x * p->x3,
					Y_TO_PX(p->y3)
				};

				bool is_pt = xy.x > p1.x - 2 && xy.x < p1.x + 2;
				bool is_pt2 = xy.x > p2.x - 2 && xy.x < p2.x + 2;
				if (is_pt2) {
					if (p2.y > xy.y - 2 && p2.y < xy.y + 2) {
						return (AutoSelection){atr, am_track_list_position(song->tracks, tr), c, s, false};
					}
				}
				bool is_line = !is_pt && !is_pt2 && (xy.x > p1.x && xy.x < p2.x - 2);
				if (is_pt || is_line) {
					float dx = (p2.x == p1.x) ? 1 : (xy.x - p1.x) / (p2.x - p1.x);

					float y = p1.y + dx * (p2.y - p1.y);

					if (y > xy.y - 2 && y < xy.y + 2) {
						return (AutoSelection){atr, am_track_list_position(song->tracks, tr), c, s - 1, is_line};
					} else {
						break;
					}
				}
				pp = p;
				if (pp->x3 > xrange.end + 2) {
					break;
				}
			}
		}
	}
	return (AutoSelection){NULL, -1, -1, -1};
}
