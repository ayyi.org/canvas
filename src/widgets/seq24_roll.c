/* GTK - The GIMP Toolkit
 * Copyright (C) 1997 David Mosberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */


#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gtk/gtksignal.h>
#include "debug/debug.h"
#include "gui_types.h"
#include "seq24/sequence.h"

typedef  GtkWidget *sequence;
typedef  GtkWidget *perform;
typedef  GtkWidget *seqdata;
typedef  GtkWidget *seqevent;
typedef  GtkWidget *seqkeys;
typedef  GtkWidget *mainwid;

#include "widgets/seq24_roll.h"
extern void sm_cursor_reset(GdkWindow *window);
extern void set_cursor(GdkWindow *window, GdkCursorType type);


#define c_num_keys 88
#define c_key_x 80     //width of each key
#define c_key_y 8      //height of each key FIXME this must match with the key widget.
#define c_scale_off 0  //???
#define c_scales_policy 0 //????its an array
#define c_ppqn 192
#define c_keyarea_y c_key_y * c_num_keys + 1
#define ROLLAREA_Y c_keyarea_y

static GtkDrawingAreaClass *parent_class = NULL;
//static guint roll_type_changed_signal = 0;


//forward declarations:
static void gtk_roll_class_init   (GtkRollClass  *class);
static void gtk_roll_init         (GtkRoll       *curve);
static void gtk_roll_realize      (GtkWidget     *widget);
static void gtk_roll_get_property (GObject       *object,
				                   guint          param_id,
				                   GValue        *value,
				                   GParamSpec    *pspec);
static void gtk_roll_set_property (GObject       *object,
				                   guint          param_id,
				                   const GValue  *value,
				                   GParamSpec    *pspec);
static void gtk_roll_finalize     (GObject       *object);
//static gint gtk_roll_expose       (GtkWidget*, GdkEvent*, GtkRoll*);
//static gboolean gtk_roll_expose       (GtkWidget *widget, GdkEventExpose *e);
//static void     gtk_roll_expose        (GtkWidget *widget, GdkEventExpose *e);
static gint     gtk_roll_expose        (GtkWidget *widget, GdkEventExpose *e);
static void     gtk_roll_update_pixmap (GtkWidget *widget);

static void     gtk_roll_change_vert   (GtkRoll *r);
static void     gtk_roll_draw_events_on(GtkRoll *r, GdkDrawable *a_draw);
static void     gtk_roll_force_draw    (GtkRoll *r);
static void     gtk_roll_size_allocate (GtkWidget *w, GtkAllocation* a_r);

static gboolean gtk_roll_button_press  (GtkWidget* w, GdkEventButton* a_ev);

static void     convert_xy             (GtkWidget* w, int a_x, int a_y, long *a_tick, int *a_note);
static void     convert_tn_box_to_rect (GtkWidget* w, long a_tick_s, long a_tick_f, int a_note_h, int a_note_l, int *a_x, int *a_y, int *a_w, int *a_h);
static void     snap_x                 (GtkWidget* w, int *a_x);
static void     convert_tn             (GtkWidget* w, long a_ticks, int a_note, int *a_x, int *a_y);
static void     xy_to_rect             (GtkWidget* w, int a_x1,  int a_y1, int a_x2,  int a_y2, int *a_x,  int *a_y, int *a_w,  int *a_h);


GType
gtk_roll_get_type(void)
{
  static GType roll_type = 0;

  if(!roll_type){
    static const GTypeInfo roll_info =
      {
	  sizeof (GtkRollClass),
	  NULL,		/* base_init */
	  NULL,		/* base_finalize */
	  (GClassInitFunc) gtk_roll_class_init,
	  NULL,		/* class_finalize */
	  NULL,		/* class_data */
	  sizeof (GtkRoll),
	  0,		/* n_preallocs */
	  (GInstanceInitFunc)gtk_roll_init,
      };

      roll_type = g_type_register_static(GTK_TYPE_DRAWING_AREA, "GtkRoll", &roll_info, 0);
    }
  return roll_type;
}


static void
gtk_roll_class_init(GtkRollClass *class)
{
  GObjectClass   *gobject_class = G_OBJECT_CLASS(class);
  GtkWidgetClass *widget_class  = (GtkWidgetClass*)class;

  parent_class = g_type_class_peek_parent(class);
  
  gobject_class->finalize     = gtk_roll_finalize;

  gobject_class->set_property = gtk_roll_set_property;
  gobject_class->get_property = gtk_roll_get_property;
 
  widget_class->realize       = gtk_roll_realize;
  widget_class->size_allocate = gtk_roll_size_allocate;
  widget_class->expose_event  = gtk_roll_expose;

  widget_class->button_press_event = gtk_roll_button_press;
 
  /*
  g_object_class_install_property (gobject_class,
				   PROP_MIN_X,
				   g_param_spec_float ("min_x",
						       P_("Minimum X"),
						       P_("Minimum possible value for X"),
						       -G_MAXFLOAT,
						       G_MAXFLOAT,
						       0.0,
						       G_PARAM_READABLE |
						       G_PARAM_WRITABLE));
  g_object_class_install_property (gobject_class,
				   PROP_MAX_X,
				   g_param_spec_float ("max_x",
								P_("Maximum X"),
								P_("Maximum possible X value"),
								-G_MAXFLOAT,
								G_MAXFLOAT,
								1.0,
								G_PARAM_READABLE |
								G_PARAM_WRITABLE));
  */
}


static void
gtk_roll_init(GtkRoll *roll)
{
  //gint old_mask;

  //roll->cursor_type = GDK_TOP_LEFT_ARROW;
  roll->m_pixmap = NULL;
  //roll->height = 0;
  //roll->grab_point = -1;

  //roll->min_x = 0.0;
  //keys->max_x = 1.0;
  //keys->min_y = 0.0;
  //keys->max_y = 1.0;

  roll->m_perform = NULL;//a_perf;
  roll->seq =   NULL;//a_seq;
  roll->m_zoom = 1;//a_zoom;
  roll->m_snap =  0;//a_snap;
  roll->m_seqdata_widget = NULL;//a_seqdata_wid;
  roll->m_seqevent_widget = NULL;//a_seqevent_wid;
  roll->m_mainwid = NULL;//a_mainwid;
  roll->m_seqkeys_widget = NULL;//a_seqkeys_wid;
  roll->m_pos = 0;//a_pos;

  //roll->m_clipboard = new sequence( );

  /*
  roll->  add_events( Gdk::BUTTON_PRESS_MASK |
        Gdk::BUTTON_RELEASE_MASK |
        Gdk::POINTER_MOTION_MASK |
        Gdk::KEY_PRESS_MASK |
        Gdk::KEY_RELEASE_MASK |
        Gdk::FOCUS_CHANGE_MASK |
        Gdk::ENTER_NOTIFY_MASK |
        Gdk::LEAVE_NOTIFY_MASK );
  */

  roll->selecting  = FALSE;
  roll->moving     = FALSE;
  roll->moving_init= FALSE;
  roll->growing    = FALSE;
  roll->adding     = FALSE;
  roll->paste      = FALSE;

  roll->m_old_progress_x = 0;

  roll->m_scale = 0;
  roll->m_key = 0;

  roll->m_vadjust = NULL;//a_vadjust;
  roll->m_hadjust = NULL;//a_hadjust;

  roll->m_window_x = 200;
  roll->m_window_y = 200;

  roll->m_scroll_offset_ticks = 0;
  roll->m_scroll_offset_key = 0;

  roll->m_scroll_offset_x = 0;
  roll->m_scroll_offset_y = 0;

  roll->m_background_sequence = 0;
  roll->m_drawing_background_seq = FALSE;

  //  set_double_buffered( FALSE );
  gtk_widget_set_double_buffered(GTK_WIDGET(roll), FALSE);


  //old_mask = gtk_widget_get_events(GTK_WIDGET(roll));
  //gtk_widget_set_events(GTK_WIDGET(roll), old_mask | GRAPH_MASK);              //?????
  //g_signal_connect(roll, "expose-event", G_CALLBACK(gtk_roll_expose), roll);
  //g_signal_connect(roll, "size-allocate", G_CALLBACK(gtk_roll_size_allocate), NULL);

  roll->m_window = NULL;
  roll->m_gc     = NULL;

  //roll->num_keys = 88; //the number of keys to draw on the keyboard.

  roll->black.red     = 0xaaaa;
  roll->black.green   = 0xaaaa;
  roll->black.blue    = 0xaaaa;
  roll->white.red     = 0x3333;
  roll->white.green   = 0x3333;
  roll->white.blue    = 0x3333;
  roll->grey.red     = 0x7777;
  roll->grey.green   = 0x7777;
  roll->grey.blue    = 0x7777;
  roll->dk_grey.red   = 0x5555;
  roll->dk_grey.green = 0x5555;
  roll->dk_grey.blue  = 0xbbbb;
  roll->red.red      = 0xffff;
  roll->red.green    = 0x0000;
  roll->red.blue     = 0x0000;

  GdkColormap *colormap = gdk_colormap_get_system();
  gdk_colormap_alloc_color(colormap, &(roll->black), TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(roll->white), TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(roll->grey),  TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(roll->dk_grey),  TRUE, TRUE);
  gdk_colormap_alloc_color(colormap, &(roll->red),  TRUE, TRUE);

}


static void
gtk_roll_finalize(GObject *object)
{
  GtkRoll *r;

  g_return_if_fail(GTK_IS_ROLL(object));

  r = GTK_ROLL(object);
  if(r->m_pixmap) g_object_unref(r->m_pixmap);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


GtkWidget*
gtk_roll_new(  perform *a_perf,
               struct _midi_sequence* a_seq, int a_zoom, int a_snap,
               seqdata *a_seqdata_wid,
               seqevent *a_seqevent_wid,
               seqkeys *a_seqkeys_wid,
               mainwid *a_mainwid,
               int a_pos,
               GtkAdjustment *a_hadjust,
               GtkAdjustment *a_vadjust
               )
{
  GtkRoll *r = g_object_new(GTK_TYPE_ROLL, NULL);

  r->m_perform = a_perf;
  r->seq       = a_seq;
  //FIXME and the rest.

  return GTK_WIDGET(r);
}


static void 
gtk_roll_realize(GtkWidget *widget)
{
  //this doesnt chain up to its parents realize fn. Not sure if we should or not.

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_ROLL(widget));

  GdkWindowAttr attributes;
  gint attributes_mask;

  GtkRoll *k = GTK_ROLL(widget);

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED); //set the realized flag

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new(gtk_widget_get_parent_window(widget), &attributes, attributes_mask);
  gdk_window_set_user_data(widget->window, widget);

  widget->style = gtk_style_attach(widget->style, widget->window);

  //gtkdrawingarea.c also calls this - any significance?
  //gtk_drawing_area_send_configure(GTK_DRAWING_AREA(widget));

  //-----------------------------------------------------

  //seq24 stuff:

  //set_flags( Gtk::CAN_FOCUS );

  //k->m_window = get_window();
  k->m_window = widget->window;

  //m_gc = Gdk::GC::create(m_window);
  k->m_gc = gdk_gc_new(widget->window);//FIXME why make a new one?
  gdk_window_clear(k->m_window); //sets the background onto the whole window.

  //m_hadjust->signal_value_changed().connect( slot( *this, &seqroll::change_horz ));
  //m_vadjust->signal_value_changed().connect( slot( *this, &seqroll::change_vert ));

  k->colormap = gdk_colormap_get_system();

  //test only:
  //k->m_pixmap = gdk_pixmap_new(widget->window, 200, 200, -1);

  gtk_roll_update_sizes(k);

  //test only:
  gtk_widget_set_double_buffered(widget, FALSE);
}


static void
gtk_roll_set_property(GObject              *object,
			          guint                 prop_id,
			          const GValue         *value,
			          GParamSpec           *pspec)
{
  //GtkRoll *roll = GTK_ROLL(object);
  
  switch (prop_id)
    {
    //case PROP_CURVE_TYPE:
    //  gtk_curve_set_curve_type (curve, g_value_get_enum (value));
    //  break;
    //case PROP_MIN_X:
    //  gtk_curve_set_range (curve, g_value_get_float (value), curve->max_x,
	//		   curve->min_y, curve->max_y);
    //  break;
    //case PROP_MAX_X:
    //  gtk_curve_set_range (curve, curve->min_x, g_value_get_float (value),
	//		   curve->min_y, curve->max_y);
    //  break;	
    //case PROP_MIN_Y:
    //  gtk_curve_set_range (curve, curve->min_x, curve->max_x,
	//		   g_value_get_float (value), curve->max_y);
    //  break;
    //case PROP_MAX_Y:
    //  gtk_curve_set_range (curve, curve->min_x, curve->max_x,
	//		   curve->min_y, g_value_get_float (value));
    //  break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gtk_roll_get_property (GObject              *object,
			           guint                 prop_id,
			           GValue               *value,
			           GParamSpec           *pspec)
{
  //GtkKeys *keys = GTK_KEYS(object);

  switch (prop_id)
    {
    /*
    case PROP_CURVE_TYPE:
      g_value_set_enum (value, curve->curve_type);
      break;
    case PROP_MIN_X:
      g_value_set_float (value, curve->min_x);
      break;
    case PROP_MAX_X:
      g_value_set_float (value, curve->max_x);
      break;
    case PROP_MIN_Y:
      g_value_set_float (value, curve->min_y);
      break;
    case PROP_MAX_Y:
      g_value_set_float (value, curve->max_y);
      break;
    */
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


void
gtk_roll_update_sizes(GtkRoll *r)
{
  //set default size

  /*
  m_hadjust->set_lower( 0 );
  m_hadjust->set_upper( m_seq->get_length() );
  m_hadjust->set_page_size( m_window_x * m_zoom );
  m_hadjust->set_step_increment( c_ppqn / 4 );
  m_hadjust->set_page_increment( c_ppqn / 4 );
  */

  int h_max_value = 1000;//( m_seq->get_length() - (m_window_x * m_zoom));
  h_max_value++; //temp!!!!!

  /*
  if( m_hadjust->get_value() > h_max_value ){
      m_hadjust->set_value( h_max_value );
  }
  */

  /*
  m_vadjust->set_lower( 0 );
  m_vadjust->set_upper( c_num_keys );
  m_vadjust->set_page_size( m_window_y / c_key_y );
  m_vadjust->set_step_increment( 12 );
  m_vadjust->set_page_increment( 12 );
  */

  int v_max_value = c_num_keys - (r->m_window_y / c_key_y);
  v_max_value++; //temp!!!

  /*
  if(m_vadjust->get_value() > v_max_value ){
      m_vadjust->set_value(v_max_value);
  }
  */

  //create pixmaps with window dimensions:
  //if(is_realized(r)){
  if(GTK_WIDGET_REALIZED(r)){

    if(r->m_pixmap) g_object_unref(r->m_pixmap);
    //r->m_pixmap = gdk_pixmap_new(r->m_window, widget->allocation.width, widget->allocation.height, -1);
    r->m_pixmap = gdk_pixmap_new(r->m_window, r->m_window_x, r->m_window_y, /*depth*/-1);
    dbg (2, "new pixmap:%p  size=%ix%i", r->m_pixmap,r->m_window_x,r->m_window_y);
    gdk_drawable_set_colormap(GDK_DRAWABLE(r->m_pixmap), r->colormap);

    gtk_roll_change_vert(r);
  }
}


static gint
//gtk_roll_expose(GtkWidget *widget, GdkEvent *event, GtkRoll *r)
gtk_roll_expose(GtkWidget *widget, GdkEventExpose *e)
{
  GtkRoll *r = GTK_ROLL(widget);

  dbg (2, "e=%ix%i", e->area.width, e->area.height);

  //GtkStyle *style = widget->style; //temp!!!

  gdk_draw_drawable(r->m_window, 
                            widget->style->fg_gc[GTK_STATE_NORMAL], //r->m_gc,
                            r->m_pixmap,
                            e->area.x,
                            e->area.y,
                            e->area.x,
                            e->area.y,
                            e->area.width,
                            e->area.height );

  //gtk_roll_draw_selection_on_window(r);


  //FIXME tmp!!! the seq24 expose doesnt do this!!!!
  //gtk_roll_force_draw(r);


  return FALSE;
  //return TRUE;
}


static void
gtk_roll_update_pixmap(GtkWidget *widget)
{
  GtkRoll *r = GTK_ROLL(widget);

  gtk_roll_draw_background(r);
  gtk_roll_draw_events_on_pixmap(r);

  gtk_roll_force_draw(GTK_ROLL(widget)); //FIXME tmp!!! only here while xpose_event() is not happening...
}


/* draws background pixmap on main pixmap,
   then puts the events on */
/*
static void
update_pixmap(GtkWidget* widget)
{
    draw_background();
    draw_events_on_pixmap();
}
*/


void
gtk_roll_draw_events_on_pixmap(GtkRoll *r)
{
  gtk_roll_draw_events_on(r, r->m_pixmap);
}


static void 
gtk_roll_draw_events_on(GtkRoll *r, GdkDrawable *a_draw)
{
	long tick_s;
	long tick_f;
	int note;
	/*

	int note_x;
	int note_width;
	int note_y;
	int note_height;

	*/
	gboolean selected;
	int velocity;

	DrawType dt;

	//int start_tick = r->m_scroll_offset_ticks;
	//int end_tick  = (r->m_window_x * r->m_zoom) + r->m_scroll_offset_ticks;

	struct _midi_sequence* seq = NULL;
	int method;
	for(method=0; method<2; ++method){

		if(method == 0 && r->m_drawing_background_seq){

			if(false /*r->m_perform->is_active( m_background_sequence )*/ ){
				//seq = r->m_perform->get_sequence(m_background_sequence);
			}
			else
				method++;
		}
		else if(method == 0) method++;


        if(method==1) seq = r->seq;
		if(!seq) continue;

        //draw boxes from sequence:
        gdk_gc_set_foreground(r->m_gc, &(r->black));
        //seq->reset_draw_marker(); //FIXME

        while((dt = sequence__get_next_note_event(seq, &tick_s, &tick_f, &note, &selected, &velocity )) != DRAW_FIN){
        /*

            if((tick_s >= start_tick && tick_s <= end_tick) ||
                 ((dt == DRAW_NORMAL_LINKED) &&
                  (tick_f >= start_tick && tick_f <= end_tick))){

                //turn into screen corrids:
                note_x = tick_s / m_zoom;
                note_y = c_rollarea_y -(note * c_key_y) - c_key_y - 1 + 2;
                note_height = c_key_y - 3;

                //printf( "drawing note[%d] tick_start[%d] tick_end[%d]\n",
                //      note, tick_start, tick_end );

                int in_shift = 0;
                int length_add = 0;

                if ( dt == DRAW_NORMAL_LINKED ){

                    note_width = (tick_f - tick_s) / m_zoom;
                    if ( note_width < 1 ) note_width = 1;

                }
                else {
                    note_width = 16 / m_zoom;
                }

                if ( dt == DRAW_NOTE_ON ){

                    in_shift = 0;
                    length_add = 2;
                }

                if ( dt == DRAW_NOTE_OFF ){

                    in_shift = -1;
                    length_add = 1;
                }

                note_x -= m_scroll_offset_x;
                note_y -= m_scroll_offset_y;

                m_gc->set_foreground(m_black);
                //draw boxes from sequence:

                if(method==0) m_gc->set_foreground( m_dk_grey );

                a_draw->draw_rectangle(m_gc,TRUE,
                                       note_x,
                                       note_y,
                                       note_width,
                                       note_height);

                //draw inside box if there is room:
                if(note_width > 3){

                    if(selected) m_gc->set_foreground(m_red);
                    else         m_gc->set_foreground(m_white);

                    if(method == 1)
                        a_draw->draw_rectangle(m_gc,TRUE,
                                               note_x + 1 + in_shift,
                                               note_y + 1,
                                               note_width - 3 + length_add,
                                               note_height - 3);
                }
            }
        */
        }
    }

}




void
gtk_roll_draw_selection_on_window(GtkRoll *r)
{
/*
    int x,y,w,h;


    if(m_selecting || m_moving || m_paste || m_growing){

        m_gc->set_line_attributes( 1,
                                   Gdk::LINE_SOLID,
                                   Gdk::CAP_NOT_LAST,
                                   Gdk::JOIN_MITER );

        //replace old:
        m_window->draw_drawable(m_gc,
                                m_pixmap,
                                m_old.x,
                                m_old.y,
                                m_old.x,
                                m_old.y,
                                m_old.width + 1,
                                m_old.height + 1 );
    }

    if(m_selecting){

        xy_to_rect ( m_drop_x,
                     m_drop_y,
                     m_current_x,
                     m_current_y,
                     &x, &y,
                     &w, &h );

        x -= m_scroll_offset_x;
        y -= m_scroll_offset_y;

        m_old.x = x;
        m_old.y = y;
        m_old.width = w;
        m_old.height = h + c_key_y;

        m_gc->set_foreground(m_black);
        m_window->draw_rectangle(m_gc,false,
                                 x,
                                 y,
                                 w,
                                 h + c_key_y );
    }

    if ( m_moving || m_paste ){

        int delta_x = m_current_x - m_drop_x;
        int delta_y = m_current_y - m_drop_y;

        x = m_selected.x + delta_x;
        y = m_selected.y + delta_y;

        x -= m_scroll_offset_x;
        y -= m_scroll_offset_y;

        m_gc->set_foreground(m_black);
        m_window->draw_rectangle(m_gc,false,
                                 x,
                                 y,
                                 m_selected.width,
                                 m_selected.height );
        m_old.x = x;
        m_old.y = y;
        m_old.width = m_selected.width;
        m_old.height = m_selected.height;
    }

    if ( m_growing ){

        int delta_x = m_current_x - m_drop_x;
        int width = delta_x + m_selected.width;

        if ( width < 1 )
            width = 1;

        x = m_selected.x;
        y = m_selected.y;

        x -= m_scroll_offset_x;
        y -= m_scroll_offset_y;

        m_gc->set_foreground(m_black);
        m_window->draw_rectangle(m_gc,false,
                                 x,
                                 y,
                                 width,
                                 m_selected.height );

        m_old.x = x;
        m_old.y = y;
        m_old.width = width;
        m_old.height = m_selected.height;

    }
*/
}


static void 
gtk_roll_change_vert(GtkRoll *r)
{
  //update vars in response to a change in widget height?

  /* FIXME

  m_scroll_offset_key = (int) m_vadjust->get_value();
  */
  r->m_scroll_offset_y = r->m_scroll_offset_key * c_key_y;

  gtk_roll_update_pixmap(GTK_WIDGET(r));
  gtk_roll_force_draw(r);
}


void
gtk_roll_redraw(GtkRoll *r)
{
    //r->m_scroll_offset_ticks = (int) r->m_hadjust->get_value();
    //r->m_scroll_offset_x = r->m_scroll_offset_ticks / r->m_zoom;

    gtk_roll_update_pixmap(GTK_WIDGET(r));
    gtk_widget_queue_draw(GTK_WIDGET(r));
    //r->m_mainwid->update_sequence_on_window( m_pos );
}


void
gtk_roll_draw_background(GtkRoll *r)
{
    //clear background:
    gdk_gc_set_foreground(r->m_gc, &(r->white));
    gdk_draw_rectangle(r->m_pixmap, r->m_gc, TRUE, 0, 0, r->m_window_x, r->m_window_y);

    //draw horz grey lines:
    gdk_gc_set_foreground(r->m_gc, &(r->grey));
    gdk_gc_set_line_attributes(r->m_gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
    gint8 dash = 1;
    gdk_gc_set_dashes(r->m_gc, 0, &dash, 1);

    int i, y;
    for(i=0;i< (r->m_window_y / c_key_y) + 1;i++){
        y = i * c_key_y;
        gdk_draw_line(r->m_pixmap, r->m_gc,
                            0,              y,
                            r->m_window_x,  y);

        if(r->m_scale != c_scale_off){
            /*
            if(!c_scales_policy[r->m_scale][ ((c_num_keys - i)
                                             - r->m_scroll_offset_key
                                             - 1 + ( 12 - m_key )) % 12] )

                gdk_draw_rectangle(r->m_pixmap, r->m_gc, TRUE,
                                         0,
                                         i * c_key_y + 1,
                                         m_window_x,
                                         c_key_y - 1 );
            */

        }
    }

    //(bw is beat width)
    int measure_length_64ths =  120/*m_seq->get_bpm() FIXME*/ * 64 / 40/*m_seq->get_bw() FIXME*/;

    int measures_per_line = (256 / measure_length_64ths) / (32 / r->m_zoom);
    if ( measures_per_line <= 0 )
        measures_per_line = 1;

    int ticks_per_measure =  120/*m_seq->get_bpm() FIXME*/ * (4 * c_ppqn) / 40/*m_seq->get_bw() FIXME*/;
    int ticks_per_step = 12 * r->m_zoom;
    int ticks_per_m_line =  ticks_per_measure * measures_per_line;
    int start_tick = r->m_scroll_offset_ticks - (r->m_scroll_offset_ticks % ticks_per_step);
    int end_tick = (r->m_window_x * r->m_zoom) + r->m_scroll_offset_ticks;

    gdk_gc_set_foreground(r->m_gc, &(r->grey));

    for(i=start_tick; i<end_tick; i += ticks_per_step){
        int base_line = i / r->m_zoom;
        int x;

        if(i % ticks_per_m_line == 0){

            //solid vert line on every beat:
            x = base_line - r->m_scroll_offset_x;
            gdk_gc_set_line_attributes(r->m_gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
            gdk_draw_line(r->m_pixmap, r->m_gc,
                                x,        0,
                                x,        r->m_window_y);

        }
        else {

            gdk_gc_set_line_attributes(r->m_gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
            gint8 dash = 1;
            gdk_gc_set_dashes(r->m_gc, 0, &dash, 1);

            gdk_draw_line(r->m_pixmap, r->m_gc,
                                base_line - r->m_scroll_offset_x,
                                0,
                                base_line - r->m_scroll_offset_x,
                                r->m_window_y);

        }

    }

    //reset line style:
    gdk_gc_set_line_attributes(r->m_gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
}


static void
gtk_roll_force_draw(GtkRoll *r)
{
  GtkWidget *w = GTK_WIDGET(r);

  if(!GTK_WIDGET_REALIZED(w)){ printf("gtk_roll_force_draw(): widget not realized!\n"); return;}
  if(!GTK_WIDGET_MAPPED  (w)){ dbg (2, "widget not mapped!", ""); return;}
  if(!GTK_WIDGET_VISIBLE (w)) printf("gtk_roll_force_draw(): widget not visible!\n");
  if(!GTK_WIDGET_DRAWABLE(w)) printf("gtk_roll_force_draw(): widget not drawable!\n");
  if(!gdk_window_is_visible(r->m_window)) printf("gtk_roll_force_draw(): window not drawable!\n");
  if(!gdk_window_is_viewable(r->m_window)) printf("gtk_roll_force_draw(): window not viewable!\n");
  if(!GTK_WIDGET_RC_STYLE(w)) printf("gtk_roll_force_draw(): widget not rcstyle!\n");
  if(GTK_WIDGET_DOUBLE_BUFFERED(w)) printf("gtk_roll_force_draw(): widget is double buffered!\n");

  int wid,h;

  GdkWindow *parent = gtk_widget_get_parent_window(w);
  gdk_drawable_get_size(parent, &wid, &h);

  gdk_drawable_get_size(r->m_pixmap, &wid, &h);
  dbg (2, "pixmapsize: %ix%i", wid, h);
  //printf("gtk_roll_force_draw(): m_window:   %ix%i gdkwin=%p\n", r->m_window_x, r->m_window_y, r->m_window);

  //parent widget:
  //GtkWidget *parent_widget = gtk_widget_get_parent(w);

  gdk_draw_drawable(r->m_window, r->m_gc,
                            r->m_pixmap,
                            0,
                            0,
                            0,
                            0,
                            r->m_window_x,
                            r->m_window_y);

  //test drawing directly on the window:
  /*
  //GdkColor color;
  //color.red  = 0xffff;
  //color.green= 0x0000;
  //color.blue = 0x0000;
  GdkGC *gc = gdk_gc_new(w->window);
  gdk_gc_set_foreground(gc, &(r->red));
  gdk_draw_rectangle(r->m_window, 
                     //gc,
                     w->style->fg_gc[GTK_STATE_NORMAL],
                     TRUE, //filled
                             0,      0,
                             40,    80);
  */

  //gtk_roll_draw_selection_on_window(r);
}


static void     
gtk_roll_size_allocate(GtkWidget *w, GtkAllocation* allocation)
{ 
  //Gtk::DrawingArea::on_size_allocate( a_r );
  GtkRoll *r = GTK_ROLL(w);

  w->allocation = *allocation;

  r->m_window_x = allocation->width;
  r->m_window_y = allocation->height;
  
  gtk_roll_update_sizes(r);

  if(GTK_WIDGET_REALIZED(w)){
    gdk_window_move_resize(w->window,
                  allocation->x,     allocation->y,
                  allocation->width, allocation->height);

    //????????
    //gtk_drawing_area_send_configure(GTK_DRAWING_AREA(w));
  }
}


static gboolean
gtk_roll_button_press(GtkWidget* widget, GdkEventButton* a_ev)
{
	GtkRoll* r = GTK_ROLL(widget);

    int numsel;
	
    long tick_s;
    long tick_f;
    int note_h;
    int note_l;
	
    int norm_x, norm_y, snapped_x, snapped_y;
	
    gtk_widget_grab_focus(widget);

    gboolean needs_update = FALSE;
	
    snapped_x = norm_x = (int) (a_ev->x + r->m_scroll_offset_x );
    snapped_y = norm_y = (int) (a_ev->y + r->m_scroll_offset_y );
	
    //snap_x( &snapped_x ); //FIXME
    //snap_y( &snapped_y );
	
    // y is always snapped
    r->m_current_y = r->m_drop_y = snapped_y;
	
    // reset box that holds dirty redraw spot
    r->m_old.x = 0;
    r->m_old.y = 0;
    r->m_old.width = 0;
    r->m_old.height = 0;
	
    if ( r->paste ){
		
		convert_xy(widget, snapped_x, snapped_y, &tick_s, &note_h );
		r->paste = FALSE;
		//r->m_seq->push_undo(); //FIXME
		//r->m_seq->paste_selected( tick_s, note_h ); //FIXME

        needs_update = TRUE;
    
	} else { 
		
		// left mouse button
		if ( a_ev->button == 1 ){ 
			
			// selection, normal x
			r->m_current_x = r->m_drop_x = norm_x;
			
			// turn x,y in to ticknote
			convert_xy(widget, r->m_drop_x, r->m_drop_y, &tick_s, &note_h );
			
			// set to playing
			numsel = 0; //r->m_seq->select_note_events( tick_s, note_h, tick_s, note_h ); //events are selected by setting "m_selected = true" in the individual event.
			
			// none selected, start selection box
			if ( numsel == 0 && !r->adding ){ 
				
				//r->m_seq->unselect();	    
				r->selecting = TRUE;
			}
			
			// add a new note if we didnt select anything
			else if ( numsel == 0 && r->adding ){
				
				// adding, snapped x
				r->m_current_x = r->m_drop_x = snapped_x;
				convert_xy(widget, r->m_drop_x, r->m_drop_y, &tick_s, &note_h );
				
				// add note, length = little less than snap
				//r->m_seq->push_undo();
				midi_add_note( tick_s, r->m_note_length - 2, note_h );

				needs_update = TRUE;
			}
			
			// if we clicked on an event, we can start moving all selected notes
			else if ( numsel > 0 ){
                            
				r->moving_init = TRUE;
				needs_update = TRUE;
                                                        
				// get the box that selected elements are in
				midiedit_get_selected_box(r->seq, &tick_s, &note_h, &tick_f, &note_l);
                            
				convert_tn_box_to_rect(widget, tick_s, tick_f, note_h, note_l,
                                                    &r->m_selected.x,
                                                    &r->m_selected.y,
                                                    &r->m_selected.width,
                                                    &r->m_selected.height );

				// save offset that we get from the snap above
				int adjusted_selected_x = r->m_selected.x;
				snap_x(widget, &adjusted_selected_x);
				r->m_move_snap_offset_x = (r->m_selected.x - adjusted_selected_x);

				// align selection for drawing
				snap_x(widget, &r->m_selected.x);

				r->m_current_x = r->m_drop_x = snapped_x;
			} 
		}
    
		// right mouse button
		if ( a_ev->button == 3 ){
			gtk_roll_set_adding(widget, TRUE);
		}
		
		// middle mouse button
		if ( a_ev->button == 2 ){
			
			// moving, normal x
			r->m_current_x = r->m_drop_x = norm_x;
			convert_xy(widget, r->m_drop_x, r->m_drop_y, &tick_s, &note_h );
			
			// set to playing
			numsel = midiedit_select_note_events(r->seq, tick_s, note_h, tick_s+1, note_h);
			if ( numsel > 0 ){
				
				r->growing = TRUE;
				
				// get the box that selected elements are in
				midiedit_get_selected_box(r->seq, &tick_s, &note_h, &tick_f, &note_l );
				
				convert_tn_box_to_rect(widget, tick_s, tick_f, note_h, note_l,
										&r->m_selected.x,
										&r->m_selected.y,
										&r->m_selected.width,
										&r->m_selected.height );	
			}
		}
    }

    // if they clicked, something changed
    if ( needs_update ){
        gtk_roll_update_pixmap(widget);
        gtk_widget_queue_draw(widget);
        //r->m_seqevent_widget->redraw(widget);
        //r->m_seqdata_widget->redraw(widget);
    }
    return TRUE;
    
}

static void
convert_xy(GtkWidget* widget, int a_x, int a_y, long *a_tick, int *a_note)
{
	// take screen corrdinates, give us notes and ticks

	GtkRoll* r = GTK_ROLL(widget);

    *a_tick = a_x * r->m_zoom;
    *a_note = (ROLLAREA_Y - a_y - 2) / c_key_y;
}

static void
convert_tn_box_to_rect(GtkWidget* widget, long a_tick_s, long a_tick_f, int a_note_h, int a_note_l, int *a_x, int *a_y, int *a_w, int *a_h)
{
    int x1, y1, x2, y2;

    // convert box to X,Y values
    convert_tn(widget, a_tick_s, a_note_h, &x1, &y1);
    convert_tn(widget, a_tick_f, a_note_l, &x2, &y2);

    xy_to_rect(widget, x1, y1, x2, y2, a_x, a_y, a_w, a_h);

    *a_h += c_key_y;
}

static void
snap_x(GtkWidget* widget, int *a_x)
{
	/* performs a 'snap' on x */

	GtkRoll* r = GTK_ROLL(widget);

    //snap = number pulses to snap to
    //m_zoom = number of pulses per pixel
    //so snap / m_zoom  = number pixels to snap to
    int mod = (r->m_snap / r->m_zoom);
    if ( mod <= 0 )
        mod = 1;

    *a_x = *a_x - (*a_x % mod );
}


static void
convert_tn(GtkWidget* widget, long a_ticks, int a_note, int *a_x, int *a_y)
{
	// notes and ticks to screen corridinates */

	GtkRoll* r = GTK_ROLL(widget);

    *a_x = a_ticks / r->m_zoom;
    *a_y = ROLLAREA_Y - ((a_note + 1) * c_key_y) - 1;
}


static void
xy_to_rect(GtkWidget* widget, int a_x1,  int a_y1,
             int a_x2,  int a_y2,
             int *a_x,  int *a_y,
             int *a_w,  int *a_h )
{
	// checks mins / maxes..  the fills in x,y and width and height

	if ( a_x1 < a_x2 ){
		*a_x = a_x1;
		*a_w = a_x2 - a_x1;
	} else {
		*a_x = a_x2;
		*a_w = a_x1 - a_x2;
	}

	if ( a_y1 < a_y2 ){
		*a_y = a_y1;
		*a_h = a_y2 - a_y1;
	} else {
		*a_y = a_y2;
		*a_h = a_y1 - a_y2;
	}
}


/* popup menu calls this */
void
gtk_roll_set_adding(GtkWidget* widget, gboolean a_adding)
{
	GtkRoll* r = GTK_ROLL(widget);

	if (a_adding){

		set_cursor(widget->window, GDK_PENCIL);
		r->adding = TRUE;

	} else {

		//arr_cursor_reset(widget->window);
		r->adding = FALSE;
	}
}


//-----------------------------------------------------------------------


int
midiedit_select_note_events(struct _midi_sequence* seq, long a_tick_s, int a_note_h, long a_tick_f, int a_note_l)
{
	// selects events in range..  tick start, note high, tick end note low

    int ret=0;

    //lock();

	GList* list = seq->events;
	for(;list;list=list->next){
		midi_event* i = list->data;

        if(midi_event__is_note_off(i)               &&
           midi_event_get_timestamp(i) >= a_tick_s &&
           midi_event__get_note(i)      <= a_note_h &&
           midi_event__get_note(i)      >= a_note_l   ){


            if (midi_event_is_linked(i)){

                struct _midi_event *ev = midi_event_get_linked(i);

                if ( midi_event_get_timestamp(ev) <= a_tick_f ){

                    midi_event_select(i);
                    midi_event_select(ev);
                    ret++;
                }
            }
        }

        if (!midi_event_is_linked(i) &&
                (midi_event__is_note_on(i) ||
                midi_event_is_note_off(i) ) &&
                midi_event_get_timestamp(i)  >= a_tick_s - 16 &&
                midi_event_get_timestamp(i)  <= a_tick_f &&
                midi_event__get_note(i)       <= a_note_h &&
                midi_event__get_note(i)       >= a_note_l ) {
            midi_event_select(i);
            ret++;
        }
    }

    //unlock();

    return ret;
}


int
midi_add_note()
{
	return 0;
}

void
midiedit_get_selected_box(struct _midi_sequence* seq, long *a_tick_s, int *a_note_h, long *a_tick_f, int *a_note_l)
{
	//returns the 'box' of the selected items

	*a_tick_s = MAXBEATS * PPQN;
	*a_tick_f = 0;

	*a_note_h = 0;
	*a_note_l = 128;

	long time;
	int note;

	//lock();

	GList* list = seq->events;
	for(;list;list=list->next){
		struct _midi_event* i = list->data;

		if(i->selected){

		time = midi_event_get_timestamp(i);

		if ( time < *a_tick_s ) *a_tick_s = time;
		if ( time > *a_tick_f ) *a_tick_f = time;

		note = midi_event__get_note(i);

		if ( note < *a_note_l ) *a_note_l = note;
		if ( note > *a_note_h ) *a_note_h = note;
		}
	}

	//unlock();
}
gboolean
midi_event_is_note_off(midi_event* event)
{
    return (event->status == EVENT_NOTE_OFF);
}


long
midi_event_get_timestamp(midi_event* event)
{
	return event->timestamp;
}


gboolean
midi_event_is_linked(midi_event* event)
{
    return event->has_link;
}


struct _midi_event*
midi_event_get_linked(midi_event* event)
{
    return event->linked;
}


void
midi_event_select(midi_event* event)
{
    event->selected = TRUE;
}


