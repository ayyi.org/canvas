/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __event_h__
#define __event_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_EVENT_WIN            (ayyi_event_win_get_type ())
#define AYYI_EVENT_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_EVENT_WIN, AyyiEventWin))
#define AYYI_EVENT_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_EVENT_WIN, AyyiEventWinClass))
#define AYYI_IS_EVENT_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_EVENT_WIN))
#define AYYI_IS_EVENT_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_EVENT_WIN))
#define AYYI_EVENT_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_EVENT_WIN, AyyiEventWinClass))

typedef struct _AyyiEventWinClass AyyiEventWinClass;

#define event_foreach GList* ewindows = windows__get_by_type(AYYI_TYPE_EVENT_WIN); if(ewindows){ for(;ewindows;ewindows=ewindows->next){ AyyiEventWin* event = (AyyiEventWin*)ewindows->data;
#define end_event_foreach } g_list_free(ewindows); }
#define is_event_window(A) windows__verify_pointer((AyyiPanel*)A, AYYI_TYPE_EVENT_WIN)

typedef enum {
   EF_NAME = 0,
   EF_TIME,
   EF_LEN,
   EF_LEVEL,
   EF_FADE_IN,
   EF_FADE_OUT,
   EF_MAX
} EventFieldType;

struct _EventField
{
   GtkWidget*    entry;
   char          ref[64];
};

struct _AyyiEventWinClass {
   AyyiPanelClass parent_class;
};

struct _AyyiEventWin
{
   AyyiPanel     panel;

   EventField    fields[EF_MAX];

   GtkWidget*    hbox;
};


G_END_DECLS
#endif
