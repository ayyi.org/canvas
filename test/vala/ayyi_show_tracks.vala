/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShowtracksClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new ShowtracksClient();
		return (int)app.run(((ShowtracksClient*)app)->on_shm);
	}

	private void on_shm()
	{
		print("-----------------------------------------------------------------\n");
		print("audio tracks:\n");
		if ((bool)AyyiSong.audio_track_next(null)) {
			print("  idx  flgs clr p a name\n");
			AyyiTrack* track = null;
			while ((bool)(track = AyyiSong.audio_track_next(track))) {
				AyyiChannel* ch = AyyiSong.track__get_channel(track);
				int aux_count = Mixer.aux_count(ch->shm_idx);
				//AM.curve_get_length(track->bezier.vol)
				print("  %3i %5i %3i %i %i %s\n", track->shm_idx, track->flags, track->colour, ch->has_pan, aux_count, (string)track->name);
			}
		}
		else print("\tno audio tracks\n");
		print("-----------------------------------------------------------------\n");

		print("midi tracks:\n");
		if ((bool)AyyiSong.midi_track_next(null)) {
			print("  idx name\n");
			AyyiMidiTrack* midi_track = null;
			while ((bool)(midi_track = AyyiSong.midi_track_next(midi_track))) {
				print("  %3i %s\n", midi_track->shm_idx, (string)midi_track->name);
			}
		}
		else print("\tno midi tracks\n");

		print("-----------------------------------------------------------------\n");

		app.quit();
	}
}


