/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __help_c__
#include <ctype.h>
#include <gdk/gdkkeysyms.h>
#include "global.h"
#include "windows.h"
#include "support.h"

#if defined(USE_WEBKIT) || defined(USE_GTKHTML)
  #include "html/html_plugin.h"
  #include "html/update.h"
  #include "html/ui_htmlview.h"
#endif
#include "help.h"

#define help_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_HELP_WIN) continue; \
		HelpWindow* help = (HelpWindow*)panel;
#define end_help_foreach \
	end_panel_foreach

static GObject* help_window__construct        (GType, guint n_construct_properties, GObjectConstructParam*);
static void     help_window__on_ready         (AyyiPanel*);
static void     help_window__load_from_file   (LifereaHtmlView*, const gchar*);
static gboolean help_window__on_statusbar     (GtkWidget*, const char*, AyyiPanel*);
static void     help_window__on_focus_changed (GObject*, gpointer);
#ifdef NOT_USED
static gboolean help_window__on_load_started  (GtkWidget*, gpointer object, AyyiPanel*);
#endif

G_DEFINE_TYPE (AyyiHelpWin, ayyi_help_win, AYYI_TYPE_PANEL)

static const char* dirs[] = {"../help/html", PACKAGE_DATA_DIR"/"PACKAGE"/help/html", NULL};
static const char* helpdir = NULL;


void
help_init ()
{
}

static void
ayyi_help_win_class_init (AyyiHelpWinClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	g_object_class->constructor = help_window__construct;

	panel->name           = "Help";
	panel->has_statusbar  = true;
	panel->has_link       = true;
	panel->has_follow     = false;
	panel->default_size.x = 640;
	panel->default_size.y = 320;
	panel->accel          = GDK_F1;
	panel->new            = help_win__new;
	panel->on_ready       = help_window__on_ready;

#if defined(USE_WEBKIT) || defined(USE_GTKHTML)
	update_init();
	html_plugin_mgmt_init();
#endif

	helpdir = find_path(dirs);
	dbg(1, "helpdir='%s'.", helpdir);
}


static void
ayyi_help_win_init (AyyiHelpWin* object)
{
}


static GObject*
help_window__construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* object = G_OBJECT_CLASS(ayyi_help_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)object, AYYI_TYPE_HELP_WIN);

	return object;
}


GtkWidget*
help_win__new (AyyiWindow* window, Size* config_size, GtkOrientation orientation)
{
	HelpWindow* self = AYYI_HELP_WIN (g_object_new (AYYI_TYPE_HELP_WIN, "behavior", GDL_DOCK_ITEM_BEH_NORMAL, "orientation", orientation, NULL));
	AyyiPanel* panel = (AyyiPanel*)self;
	ayyi_panel_on_new(panel, config_size, orientation);

	return (GtkWidget*)self;
}


static void
help_window__on_ready (AyyiPanel* panel)
{
	HelpWindow* self = (HelpWindow*)panel;

	if(self->view) return;

	LifereaHtmlView* view = self->view = htmlview_new (panel, FALSE);

#if defined(USE_WEBKIT) || defined(USE_GTKHTML)
	if(view){
		//renderWidget is a scroll panel.
		GtkWidget* renderWidget = htmlview_get_widget(view);
		panel_pack(renderWidget, EXPAND_TRUE);
		gtk_widget_show(renderWidget);

		htmlview_clear(view);

		help_window__load_from_file(view, "index.html");

		g_signal_connect(G_OBJECT(view), "statusbar_changed", G_CALLBACK(help_window__on_statusbar), panel);
		//g_signal_connect(G_OBJECT(view), "load_started",      G_CALLBACK(help_window__on_load_started), sm_window);
	}
#endif

	ayyi_panel_on_open(panel);

	if(g_list_length(panel->shell->panels) == 1){
		GtkWidget* icon = panel->shell->statusbar.widget[STATUSBAR_ICON];
		gtk_widget_set_no_show_all(icon, true);
		gtk_widget_hide(icon);
	}

	g_signal_connect(app, "focus-changed", (GCallback)help_window__on_focus_changed, NULL);
}


static void
help_window__load_from_file (LifereaHtmlView* view, const gchar* filename)
{
	GString* html_str = g_string_new("");

	char html_path[256];
	snprintf(html_path, 255, "%s/%s", helpdir, filename);
	html_path[255] = '\0';

	GError* error = NULL;
	gsize length;
	gchar* buffer;
	if(!g_file_get_contents(html_path, &buffer, &length, &error)){
		gchar* log_msg = g_strdup_printf("%s: %s", __func__, error->message);
		log_print(0, log_msg);
		g_string_printf(html_str, "%s<br><a href=\"index.html\">home</a>", log_msg);
		g_error_free(error);
		g_free(log_msg);
	}else{
		g_string_printf(html_str, "%s", buffer);
	}

#if 0
	const gchar* base = NULL;
	htmlview_write (view, html_str->str, base);
#endif

	void launch_url(LifereaHtmlView* htmlview, const gchar* url)
	{
#ifdef USE_WEBKIT
		char u[256];
		if(url[0] != '/' && !strstr(url, "file://")){
			if(g_path_is_absolute(helpdir))
				snprintf(u, 255, "file://%s/%s", helpdir, url);
			else
				snprintf(u, 255, "file://%s/%s/%s", g_get_current_dir(), helpdir, url);
		}
		else strcpy(u, url);
		dbg(2, "%s", u);
#else
		char* u = (char*)url;
#endif
		htmlview_launch_url(view, u);
	}

	launch_url(view, filename);
}


static void
help_window__on_focus_changed (GObject* _ap, gpointer _)
{
	if(app->disable_help_focus) return;

	AyyiPanel* panel = app->active_panel;

	if(!windows__verify_pointer(panel, 0)) return; //we get invalid pointer after window close.

	if(AYYI_IS_HELP_WIN(panel)) return;

	char file[64];
	snprintf(file, 63, "%s.html", AYYI_PANEL_GET_CLASS(panel)->name);
	file[0] = tolower(file[0]);
	dbg(2, "%s", file);

	help_foreach {
#if defined(USE_WEBKIT) || defined(USE_GTKHTML)
		if(help->view && help->currently_showing != G_OBJECT_TYPE(panel)){
			htmlview_clear(help->view);
			help_window__load_from_file(help->view, file);
			help->currently_showing = G_OBJECT_TYPE(panel);
		}
#endif
	} end_help_foreach;
}


static gboolean
help_window__on_statusbar (GtkWidget* widget, const char* url, AyyiPanel* panel)
{
	windows__verify_pointer(panel, AYYI_TYPE_HELP_WIN);

	shell__statusbar_print(panel->shell, 1, "%s", url ? url : "");

	return false;
}


#ifdef NOT_USED
static gboolean
help_window__on_load_started (GtkWidget* widget, gpointer object, AyyiPanel* panel)
{
	windows__verify_pointer(panel, AYYI_TYPE_HELP_WIN);
	return false;
}
#endif


