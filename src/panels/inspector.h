/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __inspector_h__
#define __inspector_h__

#define inspector_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_INSPECTOR_WIN) continue; \
		InspectorWin* inspector = (InspectorWin*)panel;
#define end_inspector_foreach end_panel_foreach

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_INSPECTOR_WIN            (ayyi_inspector_win_get_type ())
#define AYYI_INSPECTOR_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_INSPECTOR_WIN, AyyiInspectorWin))
#define AYYI_INSPECTOR_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_INSPECTOR_WIN, AyyiInspectorWinClass))
#define AYYI_IS_INSPECTOR_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_INSPECTOR_WIN))
#define AYYI_IS_INSPECTOR_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_INSPECTOR_WIN))
#define AYYI_INSPECTOR_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_INSPECTOR_WIN, AyyiInspectorWinClass))

typedef struct _AyyiInspectorWinClass AyyiInspectorWinClass;

typedef struct _PlaybackParam PlaybackParam;

struct _AyyiInspectorWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiInspectorWin
{
   AyyiPanel       panel;

   Strip*          strip;             // widget references for the inspector window channel strip.
   PlaybackParam*  pb;                // playback params array.

   AMTrack*        trk;               // if in TRACK mode, this is the currently displayed track.

   GtkWidget*      obj_label;         // gtklabel widget showing the track/part name.
   GtkWidget*      obj_label_evbox;   // event box for above.
   gulong          obj_label_changed; // handler id for signal emitted following the modification of the label text.

   Pti             prev_size;
};

GType      ayyi_inspector_win_get_type        ();
void       inspector_track_label_update       (InspectorWin*);

G_END_DECLS

#endif
