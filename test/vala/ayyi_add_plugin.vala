using GLib;
using Gdk;
using Ayyi;

[CCode(cheader_filename = "ayyi_client.h")]

public class Ayyi.AddpluginClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new AddpluginClient();

		if(!app.run(((AddpluginClient*)app)->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		if (!client->got_shm) app.quit();

		string _name = "New Part";
		char* name = AyyiSong.container_next_name(&((SongService*)ayyi.service)->song->audio_regions, _name);
		GLib.List<Model.PoolItem*>* pool = song->pool->list;
		Model.PoolItem* pool_item = pool->first().data;
		SongPos pos = {0, 0, 0};
		uint64 len = 10000;
		AyyiTrack* track = AyyiSong.audio_track_next(null);
		AM.Song.add_part(MediaType.AUDIO, REGION_FULL, pool_item, track->shm_idx, &pos, len, 0, name, 0, on_added);
	}

	public static void on_added(Ident ident, GLib.Error* error)
	{
		print("added\n");
	}

	public static void done(AyyiAction* action)
	{
		log_print(LogType.OK, "done.");
		app.quit();
	}
}


