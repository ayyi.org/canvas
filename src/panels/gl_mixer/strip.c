/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2014-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "arrange/shader.h"
#include "button.h"
#include "icon.h"

#include "fader.c"
#include "aux.c"
#include "pan.c"
#include "meter.c"

extern Icon icons[];

typedef struct {
    AGlActor   actor;
    AMChannel* channel;
    bool       selected;
    AGlActor*  pan;
    AGlActor*  mute;
    AGlActor*  fader;
    AGlActor*  meter;
    AGlActor*  label;

    ChannelActionList actions;
} GlMixerStrip;

#include "label.c"

static void strip_set_size (AGlActor*);
static bool strip_on_event (AGlActor*, GdkEvent*, AGliPt);


AGlActor*
strip_new (AMChannel* channel)
{
	GlMixerStrip* strip = agl_actor__new(GlMixerStrip,
		.actor = {
			.name     = g_strdup("Strip"),
			.region   = (AGlfRegion){0,}, // size is set by parent
			.set_size = strip_set_size,
			.paint    = agl_actor__null_painter,
			.on_event = strip_on_event
		},
		channel = channel
	);
	AGlActor* actor = (AGlActor*)strip;

#if 1
	aux_new(channel); // TODO
#else
	int y = 10;
	AGlActor* a[2];
	int i; for(i=0;i<2;i++){
		agl_actor__add_child(actor, a[i] = aux_new(channel));
		a[i]->region = (AGliRegion){0, y, 30, y + 30};
		y += 30;
	}
#endif

	agl_actor__add_child(actor, strip->pan = pan_new(channel));

	void mute_action (AGlActor* actor, gpointer _c)
	{
		AMChannel* channel = _c;
		am_channel__set_mute(channel, !am_channel__is_muted(channel));
	}

	bool mute_get_state (AGlActor* actor, gpointer _c)
	{
		return am_channel__is_muted((AMChannel*)_c);
	}

	agl_actor__add_child(actor, strip->mute = button(&icons[ICON_MUTE], mute_action, mute_get_state, channel));

	agl_actor__add_child(actor, strip->meter = meter_new(channel));
	((Meter*)strip->meter)->level = 0.25;

	agl_actor__add_child(actor, strip->fader = fader_new(channel));

	agl_actor__add_child(actor, strip->label = label_new(strip));

	return actor;
}


static void
strip_set_size (AGlActor* actor)
{
	g_return_if_fail(actor);
	g_return_if_fail(actor->parent);

	GlMixerStrip* strip = (GlMixerStrip*)actor;

	actor->region.y2 = agl_actor__height(actor->parent);

	int width = agl_actor__width(actor);

	int y = 10;
	strip->pan->region = (AGlfRegion){0, y, width, y + 10};
	y += 15;
	strip->mute->region = (AGlfRegion){0, y, width, y + 15};
	y += 30;
	strip->meter->region = (AGlfRegion){0, y, (width - PADDING) / 2, y + 160};
	strip->fader->region = (AGlfRegion){(width + PADDING) / 2, y, width, y + 160};
	y += 160 + 8;
	strip->label->region = (AGlfRegion){0, y, width, y + 20};
}


static bool
strip_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	GlMixerStrip* strip = (GlMixerStrip*)actor;

	switch (event->type){
		case GDK_BUTTON_PRESS:
			return AGL_HANDLED;
		case GDK_BUTTON_RELEASE:
			// TODO only do this if drag not started?
			strip->actions.select(strip->channel, strip->actions.user_data);
			return AGL_HANDLED;
	}

	return AGL_NOT_HANDLED;
}


static bool
strip_periodic_update (GlMixerStrip* strip)
{
	return G_SOURCE_CONTINUE;
}

