/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include "app.h"
#include "window.h"
#include "window.statusbar.h"


void
shell__statusbar_print (AyyiWindow* window, StatusbarNum n, const char* fmt, ...)
{
	if(n < STATUSBAR_MAIN || n >= STATUSBAR_MAX) n = STATUSBAR_MAIN;

	if(!window){
		if(!(window = (AyyiWindow*)gtk_widget_get_toplevel((GtkWidget*)app->latest_arrange))) return;
	}

	va_list ap;
	char text[256];
	if (fmt == NULL)
		*text = '\0';
	else {
		va_start(ap, fmt);
		vsprintf(text, fmt, ap);
		va_end(ap);
	}

	Statusbar* s = window->statusbar;
	if(!s) return;

	GtkWidget* statusbar = s->widget[n];
	if(!statusbar) return;

	gint cid = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Song");
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), cid, text);

	//if(shell->statusbar_icon) gtk_widget_hide(shell->statusbar_icon);
}


/*
 *  Only print to the statusbar if the shell contains a panel of Type, and it is visible.
 */
void
shell__statusbar_print_if_panel (AyyiWindow* window, int n, PanelType type, const char* fmt, ...)
{
	va_list ap;
	char text[256];
	if (fmt == NULL)
		*text = '\0';
	else {
		va_start(ap, fmt);
		vsnprintf(text, 255, fmt, ap);
		va_end(ap);
	}

	if(window__has_panel_type(window, type)){
		shell__statusbar_print(window, n, text);
	}
}


#ifdef UNUSED
void
shell__statusbar_print_bbs (AyyiWindow* window, int n, GPos* pos)
{
	g_return_if_fail(pos);

	char bbs[64];
	pos2bbs(pos, bbs);
	shell__statusbar_print(window, n, bbs);
}
#endif

