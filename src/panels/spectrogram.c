/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __spectrogram_c__
#include "global.h"
#include <sys/time.h>
#include <gdk/gdkkeysyms.h>
#include "windows.h"
#include "toolbar.h"
#include "spectrogram/client.h"
#include "spectrogram.h"

G_DEFINE_TYPE (AyyiSpectrogramWin, ayyi_spectrogram_win, AYYI_TYPE_PANEL)

static GObject* spectrogram_win__construct    (GType, guint n_construct_properties, GObjectConstructParam*);
static void     spectrogram_win__on_finalize  (GObject*);
static void     spectrogram_win__on_ready     (AyyiPanel*);
static void     spectrogram_win__on_realise   (GtkWidget*, gpointer);
static void     spectrogram_win__on_allocate  (GtkWidget*, gpointer);
static gboolean spectrogram_win__on_expose    (GtkWidget*, GdkEventExpose*, gpointer);
static gboolean spectrogram_win__on_timeout   (gpointer);
static void     spectrogram_win__update       (SpectrogramWin*);


static void
ayyi_spectrogram_win_class_init (AyyiSpectrogramWinClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

	AyyiPluginPtr plugin;
	if ((plugin = ayyi_client_get_plugin("Ayyi Spectrogram Plugin"))) {

		SpectrogramSymbols* spectrogram_symbols = plugin->symbols;
		dbg(2, "spectrogram hello=%s", spectrogram_symbols->get_hello());
		if (strcmp(spectrogram_symbols->get_hello(), "hello")) {
			gerr("plugin test failed!");
			return;
		}
	}

	panel->name           = "Spectrogram";
	panel->has_link       = false;
	panel->has_follow     = false;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F14;
	panel->default_size.x = 280;
	panel->default_size.y = 350;
	panel->new            = spectrogram_win__new;
	panel->on_ready       = spectrogram_win__on_ready;

    g_object_class->constructor = spectrogram_win__construct;
	g_object_class->finalize = spectrogram_win__on_finalize;
}


static GObject*
spectrogram_win__construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if (_debug_ > -1) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_spectrogram_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_SPECTROGRAM_WIN);

	return g_object;
}


static GtkWidget*
spectrogram_win__new (AyyiWindow* _, Size* config_size, GtkOrientation orientation)
{
	SpectrogramWin* self = AYYI_SPECTROGRAM_WIN (g_object_new (AYYI_TYPE_SPECTROGRAM_WIN, "behavior", GDL_DOCK_ITEM_BEH_NORMAL, "orientation", orientation, NULL));

	return (GtkWidget*)self;
}


static void
spectrogram_win__on_finalize (GObject* g_object)
{
	AyyiSpectrogramWinClass* sk = AYYI_SPECTROGRAM_WIN_GET_CLASS(g_object);
	if(sk->timer){ //TODO check n_instances
		g_source_remove(sk->timer);
		sk->timer = 0;
	}

	G_OBJECT_CLASS(ayyi_spectrogram_win_parent_class)->finalize(g_object);
}


static void
spectrogram_win__on_ready (AyyiPanel* panel)
{
	SpectrogramWin* self = (SpectrogramWin*)panel;

	if (self->area) return;

	GtkWidget* win = panel->shell->widget;
	AyyiPanelClass* k = g_type_class_peek(AYYI_TYPE_SPECTROGRAM_WIN);

	self->meterlevel = 0.0;

	g_signal_connect((gpointer)win, "realize", G_CALLBACK(spectrogram_win__on_realise), NULL);
	g_signal_connect((gpointer)win, "size-allocate", G_CALLBACK(spectrogram_win__on_allocate), NULL);

	GtkWidget* area = self->area = gtk_drawing_area_new();
	gtk_widget_set_size_request(area, 100, 200);
	gtk_widget_show(area);
	panel_pack(area, EXPAND_FALSE);
	g_signal_connect((gpointer)area, "expose_event", G_CALLBACK(spectrogram_win__on_expose), self);

	AyyiSpectrogramWinClass* sk = (AyyiSpectrogramWinClass*)k; //AYYI_SPECTROGRAM_WIN_GET_CLASS(self);
	sk->timer = g_timeout_add(50, spectrogram_win__on_timeout, NULL); //should be per class, not per window.
}


static void
spectrogram_win__on_realise (GtkWidget* window, gpointer user_data)
{
}


static void
spectrogram_win__on_allocate (GtkWidget* window, gpointer user_data)
{
	// remove size forcing
	static gboolean first_time = TRUE;
	if(first_time && GTK_WIDGET_REALIZED(window)){
		gtk_widget_set_size_request(window, 20, 20);
		first_time = FALSE;
	}
}


static gboolean
spectrogram_win__on_expose (GtkWidget* widget, GdkEventExpose* event, gpointer data)
{
	SpectrogramWin* panel = (SpectrogramWin*)widget;

	AyyiPluginPtr plugin = ayyi_client_get_plugin("Ayyi Spectrogram Plugin");
	if (!plugin) return TRUE;
	SpectrogramSymbols* spectrogram = plugin->symbols;

	if (spectrogram) {
		struct _spec_shm* spec_shm = plugin->client_data;
		g_return_val_if_fail(spec_shm, TRUE);

		panel->update_time_ms = spec_shm->time_ms;

		return spectrogram->on_expose(widget, event, data, plugin);
	}

	/*
	SpectrogramWin* panel = (SpectrogramWin*)data;
	windows__verify_pointer(&panel->panel, AYYI_TYPE_SPECTROGRAM_WIN);

	//dbg(0, "window=%p", widget->window);
	cairo_t* cr = gdk_cairo_create (widget->window);
	gdk_cairo_set_source_color (cr, &widget->style->fg[widget->state]);

	int x = 10;
	int y = 10;
	int height = panel->meterlevel;
	cairo_rectangle (cr, x, y, 10, height);

	cairo_fill (cr);
	cairo_destroy (cr);
	*/
	return TRUE;
}


static gboolean
spectrogram_win__on_timeout (gpointer data)
{
	bool dont_stop = TRUE;

	spectrogram_foreach {
		spectrogram_win__update(spectrogram);
	} end_spectrogram_foreach;

	return dont_stop;
}


static void
spectrogram_win__update(SpectrogramWin* win)
{
	AyyiPluginPtr plugin = ayyi_client_get_plugin("Ayyi Spectrogram Plugin");
	if (!plugin) return;
	SpectrogramSymbols* spectrogram = plugin->symbols;

	if (spectrogram) {
		win->meterlevel = 100 * spectrogram->get_meterlevel(plugin);

		struct _spec_shm* spec_shm = plugin->client_data;
		g_return_if_fail(spec_shm);

		uint64_t t_prev = win->update_time_ms;
		if (t_prev != spec_shm->time_ms) {
			gtk_widget_queue_draw_area(win->area, 0, 0, win->area->allocation.width, win->area->allocation.height);
			win->update_time_ms = spec_shm->time_ms;
		}
	}
}
