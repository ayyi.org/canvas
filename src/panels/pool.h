/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __pool_win_h__
#define __pool_win_h__

#include <panels/panel.h>
#include "pool_model.h"

G_BEGIN_DECLS

#define AYYI_TYPE_POOL_WIN            (ayyi_pool_win_get_type ())
#define AYYI_POOL_WIN(obj)            (G_YPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_POOL_WIN, AyyiPoolWin))
#define AYYI_POOL_WIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_POOL_WIN, AyyiPoolWinClass))
#define AYYI_IS_POOL_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_POOL_WIN))
#define AYYI_IS_POOL_WIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_POOL_WIN))
#define AYYI_POOL_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_POOL_WIN, AyyiPoolWinClass))

typedef struct _AyyiPoolWinClass AyyiPoolWinClass;

struct _AyyiPoolWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiPoolWin
{
  AyyiPanel            panel;
  GtkWidget*           menu;           // treeview popup menu.
  GtkWidget*           filer_scrollwin;
 
  GtkWidget*           treeview;
  GtkWidget*           scrollwindow;   // scrollwindow for the pool treeview.
  GtkTreeViewColumn*   col2;           // the column displaying peak overviews.
  GList*               expanded_list;  // rowrefs for rows that are expanded in the tree.

  int                  pixbuf_width;
};

GType                ayyi_pool_win_get_type        ();

void                 pool_win__init                ();

void                 pool_win_pixbuf_cell_update   (PoolWindow*, GtkTreeIter*, AMPoolItem*, int palette_num);

void                 pool_win_statusbar_print_all  (int n, char*);
void                 pool_win_statusbar_update     (PoolWindow*, GtkTreeIter*, AMPart*, AyyiIdx);

#define pool_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_POOL_WIN) continue; \
		AyyiPoolWin* pool_window = (AyyiPoolWin*)panel;
#define end_pool_foreach end_panel_foreach

G_END_DECLS

#endif
