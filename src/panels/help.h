/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __help_win_h__
#define __help_win_h__

#ifdef USE_GTKHTML
#  include <gtkhtml/gtkhtml.h>
#  include <gtkhtml/gtkhtml-embedded.h>
#endif
#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_HELP_WIN            (ayyi_help_win_get_type ())
#define AYYI_HELP_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_HELP_WIN, AyyiHelpWin))
#define AYYI_HELP_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_HELP_WIN, AyyiHelpWinClass))
#define AYYI_IS_HELP_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_HELP_WIN))
#define AYYI_IS_HELP_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_HELP_WIN))
#define AYYI_HELP_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_HELP_WIN, AyyiHelpWinClass))

typedef struct _AyyiHelpWinClass AyyiHelpWinClass;

struct _AyyiHelpWinClass {
    AyyiPanelClass   parent_class;
};

struct _AyyiHelpWin
{
    AyyiPanel        panel;
#if defined(USE_WEBKIT) || defined(USE_GTKHTML)
    LifereaHtmlView* view;
#endif
    int              currently_showing;
};


void       help_init     ();
GtkWidget* help_win__new (AyyiWindow*, Size*, GtkOrientation);

G_END_DECLS

#endif
