/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __window_statusbar_h__
#define __window_statusbar_h__

#include "gui_types.h"
#include "statusbar.h"

void shell__statusbar_print          (AyyiWindow*, StatusbarNum, const char* fmt, ...);
void shell__statusbar_print_if_panel (AyyiWindow*, int n, PanelType, const char* fmt, ...);
void shell__statusbar_print_bbs      (AyyiWindow*, int n, GPos*);

#endif
