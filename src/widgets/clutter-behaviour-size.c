/*
 * Clutter.
 *
 * An OpenGL based 'interactive canvas' library.
 *
 * Authored By Matthew Allum  <mallum@openedhand.com>
 *
 * Copyright (C) 2006 OpenedHand
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:clutter-behaviour-size
 * @short_description: A behaviour controlling size
 *
 * A #ClutterBehaviourSize interpolates actors size between two values.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <clutter/clutter.h>
#include "clutter-behaviour-size.h"

//duplicated from clutter-private.h
#define CLUTTER_PARAM_READWRITE \
        G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB

#include <math.h>

G_DEFINE_TYPE (ClutterBehaviourSize, clutter_behaviour_size, CLUTTER_TYPE_BEHAVIOUR);

struct _ClutterBehaviourSizePrivate
{
  gdouble x_size_start;
  gdouble y_size_start;

  gdouble x_size_end;
  gdouble y_size_end;
};

#define CLUTTER_BEHAVIOUR_SIZE_GET_PRIVATE(obj)        (G_TYPE_INSTANCE_GET_PRIVATE ((obj), CLUTTER_TYPE_BEHAVIOUR_SIZE, ClutterBehaviourSizePrivate))

enum
{
  PROP_0,

  PROP_X_SCALE_START,
  PROP_Y_SCALE_START,
  PROP_X_SCALE_END,
  PROP_Y_SCALE_END,
};

typedef struct {
  gdouble size_x;
  gdouble size_y;
} SizeFrameClosure;

static void
size_frame_foreach (ClutterBehaviour* behaviour, ClutterActor* actor, gpointer data)
{
  SizeFrameClosure *closure = data;

  clutter_actor_set_size (actor, closure->size_x, closure->size_y);
}

static void
clutter_behaviour_size_alpha_notify (ClutterBehaviour* behave, gdouble alpha_value)
{
  SizeFrameClosure closure = { 0, };

  ClutterBehaviourSizePrivate* priv = CLUTTER_BEHAVIOUR_SIZE(behave)->priv;

  /* Fix the start/end values, avoids potential rounding errors on large
   * values. 
  */
  if (alpha_value == 1.0)
    {
      closure.size_x = priv->x_size_end;
      closure.size_y = priv->y_size_end;
    }
  else if (alpha_value == 0)
    {
      closure.size_x = priv->x_size_start;
      closure.size_y = priv->y_size_start;
    }
  else
    {
      closure.size_x = (priv->x_size_end - priv->x_size_start)
                      * alpha_value
                      + priv->x_size_start;
      
      closure.size_y = (priv->y_size_end - priv->y_size_start)
                      * alpha_value
                      + priv->y_size_start;
    }

  clutter_behaviour_actors_foreach (behave, size_frame_foreach, &closure);
}

static void
clutter_behaviour_size_set_property (GObject* gobject, guint prop_id, const GValue* value, GParamSpec* pspec)
{
  ClutterBehaviourSizePrivate *priv;

  priv = CLUTTER_BEHAVIOUR_SIZE (gobject)->priv;

  switch (prop_id)
    {
    case PROP_X_SCALE_START:
      priv->x_size_start = g_value_get_double (value);
      break;

    case PROP_X_SCALE_END:
      priv->x_size_end = g_value_get_double (value);
      break;

    case PROP_Y_SCALE_START:
      priv->y_size_start = g_value_get_double (value);
      break;

    case PROP_Y_SCALE_END:
      priv->y_size_end = g_value_get_double (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
clutter_behaviour_size_get_property (GObject    *gobject,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  ClutterBehaviourSizePrivate *priv;

  priv = CLUTTER_BEHAVIOUR_SIZE (gobject)->priv;

  switch (prop_id)
    {
    case PROP_X_SCALE_START:
      g_value_set_double (value, priv->x_size_start);
      break;

    case PROP_X_SCALE_END:
      g_value_set_double (value, priv->x_size_end);
      break;

    case PROP_Y_SCALE_START:
      g_value_set_double (value, priv->y_size_start);
      break;

    case PROP_Y_SCALE_END:
      g_value_set_double (value, priv->y_size_end);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
clutter_behaviour_size_class_init (ClutterBehaviourSizeClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  ClutterBehaviourClass *behave_class = CLUTTER_BEHAVIOUR_CLASS (klass);
  GParamSpec *pspec = NULL;

  g_type_class_add_private (klass, sizeof (ClutterBehaviourSizePrivate));

  gobject_class->set_property = clutter_behaviour_size_set_property;
  gobject_class->get_property = clutter_behaviour_size_get_property;

  /**
   * ClutterBehaviourSize:x-size-start:
   *
   * The initial scaling factor on the X axis for the actors.
   *
   * Since: 0.6
   */
  pspec = g_param_spec_double ("x-size-start",
                               "X Start Size",
                               "Initial size on the X axis",
                               0.0, G_MAXDOUBLE,
                               1.0,
                               CLUTTER_PARAM_READWRITE);
  g_object_class_install_property (gobject_class,
                                   PROP_X_SCALE_START,
                                   pspec);
  /**
   * ClutterBehaviourSize:x-size-end:
   *
   * The final scaling factor on the X axis for the actors.
   *
   * Since: 0.6
   */
  pspec = g_param_spec_double ("x-size-end",
                               "X End Size",
                               "Final size on the X axis",
                               0.0, G_MAXDOUBLE,
                               1.0,
                               CLUTTER_PARAM_READWRITE);
  g_object_class_install_property (gobject_class,
                                   PROP_X_SCALE_END,
                                   pspec);
  /**
   * ClutterBehaviourSize:y-size-start:
   *
   * The initial scaling factor on the Y axis for the actors.
   *
   * Since: 0.6
   */
  pspec = g_param_spec_double ("y-size-start",
                               "Y Start Size",
                               "Initial size on the Y axis",
                               0.0, G_MAXDOUBLE,
                               1.0,
                               CLUTTER_PARAM_READWRITE);
  g_object_class_install_property (gobject_class, PROP_Y_SCALE_START, pspec);
  /**
   * ClutterBehaviourSize:y-size-end:
   *
   * The final scaling factor on the Y axis for the actors.
   *
   * Since: 0.6
   */
  pspec = g_param_spec_double ("y-size-end",
                               "Y End Size",
                               "Final size on the Y axis",
                               0.0, G_MAXDOUBLE,
                               1.0,
                               CLUTTER_PARAM_READWRITE);
  g_object_class_install_property (gobject_class, PROP_Y_SCALE_END, pspec);

  behave_class->alpha_notify = clutter_behaviour_size_alpha_notify;
}

static void
clutter_behaviour_size_init (ClutterBehaviourSize *self)
{
  ClutterBehaviourSizePrivate* priv = self->priv = CLUTTER_BEHAVIOUR_SIZE_GET_PRIVATE (self);

  priv->x_size_start = priv->x_size_end = 1.0;
  priv->y_size_start = priv->y_size_end = 1.0;
}

/**
 * clutter_behaviour_size_new:
 * @alpha: a #ClutterAlpha
 * @x_size_start: initial size factor on the X axis
 * @y_size_start: initial size factor on the Y axis
 * @x_size_end: final size factor on the X axis
 * @y_size_end: final size factor on the Y axis
 *
 * Creates a new  #ClutterBehaviourSize instance.
 *
 * Return value: the newly created #ClutterBehaviourSize
 *
 * Since: 0.2
 */
ClutterBehaviour *
clutter_behaviour_size_new (ClutterAlpha   *alpha,
			     gdouble         x_size_start,
			     gdouble         y_size_start,
			     gdouble         x_size_end,
			     gdouble         y_size_end)
{
	printf("clutter_behaviour_size_new()...\n");
	g_return_val_if_fail (alpha == NULL || CLUTTER_IS_ALPHA (alpha), NULL);

	return g_object_new (CLUTTER_TYPE_BEHAVIOUR_SIZE,
	                     "alpha", alpha,
	                     "x-size-start", x_size_start,
	                     "y-size-start", y_size_start,
	                     "x-size-end", x_size_end,
	                     "y-size-end", y_size_end,
	                     NULL);
}

/**
 * clutter_behaviour_size_set_bounds:
 * @size: a #ClutterBehaviourSize
 * @x_size_start: initial size factor on the X axis
 * @y_size_start: initial size factor on the Y axis
 * @x_size_end: final size factor on the X axis
 * @y_size_end: final size factor on the Y axis
 *
 * Sets the bounds used by size behaviour.
 *
 * Since: 0.6
 */
void
clutter_behaviour_size_set_bounds (ClutterBehaviourSize *size,
                                    gdouble                x_size_start,
                                    gdouble                y_size_start,
                                    gdouble                x_size_end,
                                    gdouble                y_size_end)
{
  ClutterBehaviourSizePrivate *priv;

  g_return_if_fail (CLUTTER_IS_BEHAVIOUR_SIZE (size));

  priv = size->priv;

  g_object_freeze_notify (G_OBJECT (size));

  if (priv->x_size_start != x_size_start)
    {
      priv->x_size_start = x_size_start;
      g_object_notify (G_OBJECT (size), "x-size-start");
    }

  if (priv->y_size_start != y_size_start)
    {
      priv->y_size_start = y_size_start;
      g_object_notify (G_OBJECT (size), "y-size-start");
    }

  if (priv->x_size_end != x_size_end)
    {
      priv->x_size_end = x_size_end;
      g_object_notify (G_OBJECT (size), "x-size-end");
    }

  if (priv->y_size_end != y_size_end)
    {
      priv->y_size_end = y_size_end;
      g_object_notify (G_OBJECT (size), "y-size-end");
    }

  g_object_thaw_notify (G_OBJECT (size));
}

/**
 * clutter_behaviour_size_get_bounds:
 * @size: a #ClutterBehaviourSize
 * @x_size_start: return location for the initial size factor on the X
 *   axis, or %NULL
 * @y_size_start: return location for the initial size factor on the Y
 *   axis, or %NULL
 * @x_size_end: return location for the final size factor on the X axis,
 *   or %NULL
 * @y_size_end: return location for the final size factor on the Y axis,
 *   or %NULL
 *
 * Retrieves the bounds used by size behaviour.
 *
 * Since: 0.4
 */
void
clutter_behaviour_size_get_bounds (ClutterBehaviourSize *size,
                                    gdouble               *x_size_start,
                                    gdouble               *y_size_start,
                                    gdouble               *x_size_end,
                                    gdouble               *y_size_end)
{
  ClutterBehaviourSizePrivate *priv;

  g_return_if_fail (CLUTTER_IS_BEHAVIOUR_SIZE (size));

  priv = size->priv;

  if (x_size_start)
    *x_size_start = priv->x_size_start;

  if (x_size_end)
    *x_size_end = priv->x_size_end;

  if (y_size_start)
    *y_size_start = priv->y_size_start;

  if (y_size_end)
    *y_size_end = priv->y_size_end;
}
