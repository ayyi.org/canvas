/*
  Html rendering plugin using gtkhtml-3
  -------------------------------------

  This file is part of AyyiGtk. http://ayyi.org
  copyright (C) 2004-2009 Tim Orford <tim@orford.org>
  Originally based on code from Devhelp. http://live.gnome.org/devhelp

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#define __dh_html_c__
#include "global.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>

#include <gtkhtml/gtkhtml.h>
#include <gtkhtml/gtkhtml-stream.h>
#include <gtkhtml/gtkhtml-embedded.h>

typedef struct LifereaHtmlView HtmlView;
#include "html/html_plugin.h"
#include <html/ui_htmlview.h>

#include "window.h"
#include "support.h"
#include "sm_html.h"

extern struct _sm_config* config;

static DhHtml*  dh_html_new                 (void);
static void     _ayyi_gtkhtml_load_from_file(GtkWidget* html_widget, const gchar* filename);
GType           dh_html_get_type            (void);
static void     html_init                   (DhHtml*);
static void     html_class_init             (DhHtmlClass*);
static void     ayyi_gtkhtml_on_link_clicked(GtkHTML*, const gchar *url, gpointer);
static void     ayyi_gtkhtml_url_requested  (GtkHTML*, const char *url, GtkHTMLStream*);
static gboolean _uri_is_relative            (const char *uri);
static char*    get_full_uri                (const char* uri);
static gboolean object_requested            (GtkHTML*, GtkHTMLEmbedded*);

enum {
  URI_SELECTED,
  LAST_SIGNAL
};
static gint signals[LAST_SIGNAL] = { 0 };


static void
ayyi_gtkhtml_init (void)
{
}

static void ayyi_gtkhtml_deinit (void) { }


static GtkWidget*
ayyi_gtkhtml_new (HtmlView* htmlview, AyyiPanel* panel, gboolean forceInternalBrowsing) 
{
	DhHtml* html = dh_html_new();

	return html->priv->scrolled_window;
}


static void
ayyi_gtkhtml_write_html (GtkWidget *scrollpane, const gchar *string, guint length, const gchar *base, const gchar *mime_type)
{
	GtkWidget* htmlwidget = gtk_bin_get_child (GTK_BIN (scrollpane));

	mime_type =
		g_ascii_strcasecmp (mime_type, "application/xhtml+xml") == 0
		? "application/xhtml"
		: mime_type;
	gtk_html_load_from_string(GTK_HTML(htmlwidget), string, length);
}


static void
ayyi_gtkhtml_launch_url (GtkWidget *scrollpane, const gchar *url)
{
	GtkWidget* htmlwidget = gtk_bin_get_child (GTK_BIN (scrollpane));
	_ayyi_gtkhtml_load_from_file(htmlwidget, url);
}


static DhHtml*
dh_html_new ()
{
  PF;
  DhHtml* html = g_object_new (DH_TYPE_HTML, NULL);
  GtkWidget* widget = GTK_WIDGET(html);

  gtk_container_add(GTK_CONTAINER(html->priv->scrolled_window), widget);
  gtk_html_construct(GTK_HTML(widget));
  g_signal_connect (G_OBJECT (widget), "link_clicked", G_CALLBACK (ayyi_gtkhtml_on_link_clicked), html->priv);

  g_signal_connect (GTK_OBJECT(html), "object_requested", GTK_SIGNAL_FUNC (object_requested), NULL);
  gtk_signal_connect (GTK_OBJECT(widget), "url_requested", GTK_SIGNAL_FUNC(ayyi_gtkhtml_url_requested), NULL); //images

  _ayyi_gtkhtml_load_from_file(GTK_WIDGET(html), "index.html");

  PF_DONE;
  return html;
}


static void
_ayyi_gtkhtml_load_from_file(GtkWidget* html_widget, const gchar* filename)
{
  //load html file from disk using glib.

  GString* html_str = g_string_new("");

  char* html_path = get_full_uri(filename);

  GError* error = NULL;
  guint length;
  gchar* buffer;
  if(!g_file_get_contents(html_path + 5, &buffer, &length, &error)){
    ayyi_log_print(0, "%s", error->message);
    g_string_printf(html_str, "%s<br><a href=\"index.html\">home</a>", error->message);
    g_error_free(error);
  }else{
    g_string_printf(html_str, "%s", buffer);
  }

  gtk_html_load_from_string(GTK_HTML(html_widget), html_str->str, html_str->len);

  g_free(html_path);
}


static void
html_class_init (DhHtmlClass *klass)
{
    signals[URI_SELECTED] = g_signal_new (
		"uri_selected",
		G_TYPE_FROM_CLASS (klass),
		G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (DhHtmlClass, uri_selected), // function_offset: The offset within the class structure //of a pointer to the default handler:
		NULL, NULL,
		gtk_marshal_NONE__POINTER,
		G_TYPE_NONE,                                 // return type
		1, G_TYPE_POINTER                            // parameters: number of, and type
	);
}


GType
dh_html_get_type (void)
{
  static GType type = 0;

  if (!type)
  {
    static const GTypeInfo info =
    {
      sizeof (DhHtmlClass),
              NULL,
              NULL,
              (GClassInitFunc) html_class_init,
              NULL,
              NULL,
              sizeof (DhHtml),
              0,
              (GInstanceInitFunc) html_init,
    };

    type = g_type_register_static (GTK_TYPE_HTML, "DhHtml", &info, 0);
  }

  return type;
}


/*
 *   Mostly for loading images, not new pages.
 *	 Urls must start with "file:"
 */
static void
ayyi_gtkhtml_url_requested(GtkHTML *html, const char *_url, GtkHTMLStream *stream)
{
    dbg(2, "url=%s rel=%i", _url, _uri_is_relative(_url+5));

	char* url = get_full_uri(_url + 5);

    if (url && !strncmp (url, "file:", 5)) {
        url += 5;
        int fd = open (url, O_RDONLY);

        if (fd != -1) {
            gchar *buf;
            size_t size;
            buf = alloca (8192);
            while ((size = read (fd, buf, 8192)) > 0) {
                gtk_html_stream_write (stream, buf, size);
            }
            gtk_html_stream_close (stream,
                               size == -1
                               ? GTK_HTML_STREAM_ERROR
                               : GTK_HTML_STREAM_OK);
                                close (fd);
                            return;
        }
    }
    gtk_html_stream_close (stream, GTK_HTML_STREAM_ERROR);
	g_free(url);
}


static void
ayyi_gtkhtml_on_link_clicked(GtkHTML* doc, const gchar* url, gpointer data)
{
  //this is called directly following a user clicking on a link.
  //-why does it emit another signal instead of loading the page directly?
  //-doc and data are almost the same, why are they specified twice?

  //gchar      *full_uri;
  //DhHtml* html = DH_HTML(data);
  //DhHtmlPriv* priv = html->priv;

  //FIXME we need to use or reimplement html_get_full_uri().
  //-i didnt use it cos it has dependencies on gnomevfs.
  //full_uri = html_get_full_uri (html, url); //something to do with going rel->full.
  //full_uri = url;

  //d(g_print ("Full URI: %s\n", full_uri));
  dbg(0, "loading url: %s", url);

  //load page via a signal (i havnt gotton this to work yet):
  ////g_signal_emit (html, signals[URI_SELECTED], 0, full_uri);
  //g_signal_emit (html, signals[URI_SELECTED], 0, url);
  
  //alternative: load html file directly:
  _ayyi_gtkhtml_load_from_file(GTK_WIDGET(doc), url);

  //g_free (full_uri);
}


/*
static gchar *
html_get_full_uri (DhHtml *html, const gchar *url)
{
	DhHtmlPriv *priv;

	priv = html->priv;

	if (priv->base_url) {
		if (dh_util_uri_is_relative (url)) {
			return dh_util_uri_relative_new (url, priv->base_url);
		}
	}

	return g_strdup (url);
}




// -----------------------------------------------------------------
//                          From GNOME VFS
// -----------------------------------------------------------------
static void
remove_internal_relative_components (char *uri_current)
{
	char *segment_prev, *segment_cur;
	size_t len_prev, len_cur;

	len_prev = len_cur = 0;
	segment_prev = NULL;

	segment_cur = uri_current;

	while (*segment_cur) {
		len_cur = strcspn (segment_cur, "/");

		if (len_cur == 1 && segment_cur[0] == '.') {
			// Remove "." 's
			if (segment_cur[1] == '\0') {
				segment_cur[0] = '\0';
				break;
			} else {
				memmove (segment_cur, segment_cur + 2, strlen (segment_cur + 2) + 1);
				continue;
			}
		} else if (len_cur == 2 && segment_cur[0] == '.' && segment_cur[1] == '.' ) {
			// Remove ".."'s (and the component to the left of it) that aren't at the
			// beginning or to the right of other ..'s
			//
			if (segment_prev) {
				if (! (len_prev == 2
				       && segment_prev[0] == '.'
				       && segment_prev[1] == '.')) {
				       	if (segment_cur[2] == '\0') {
						segment_prev[0] = '\0';
						break;
				       	} else {
						memmove (segment_prev, segment_cur + 3, strlen (segment_cur + 3) + 1);

						segment_cur = segment_prev;
						len_cur = len_prev;

						// now we find the previous segment_prev
						if (segment_prev == uri_current) {
							segment_prev = NULL;
						} else if (segment_prev - uri_current >= 2) {
							segment_prev -= 2;
							for ( ; segment_prev > uri_current && segment_prev[0] != '/'
							      ; segment_prev-- );
							if (segment_prev[0] == '/') {
								segment_prev++;
							}
						}
						continue;
					}
				}
			}
		}

		//Forward to next segment

		if (segment_cur [len_cur] == '\0') {
			break;
		}

		segment_prev = segment_cur;
		len_prev = len_cur;
		segment_cur += len_cur + 1;
	}

}
*/


static gboolean
_uri_is_relative (const char *uri)
{
	const char *current;

	// RFC 2396 section 3.1
	for (current = uri ;
		*current
		&& 	((*current >= 'a' && *current <= 'z')
			 || (*current >= 'A' && *current <= 'Z')
			 || (*current >= '0' && *current <= '9')
			 || ('-' == *current)
			 || ('+' == *current)
			 || ('.' == *current)) ;
	     current++);

	return  !(':' == *current);
}


static char*
get_full_uri(const char* uri)
{
  //returned uri must be freed.
  //TODO should use proper base_url, not helpdir.

  if(uri[0] == '/'){
    return g_build_path("/", "file:", uri, NULL);
  } else {
    return g_build_path("/", "file:", config->helpdir, uri, NULL);
  }
  return NULL;
}


/*
gchar *
dh_util_uri_relative_new (const gchar *uri, const gchar *base_uri)
{
	char *result = NULL;

	g_return_val_if_fail (base_uri != NULL, g_strdup (uri));
	g_return_val_if_fail (uri != NULL, NULL);

	// See section 5.2 in RFC 2396

	// FIXME bugzilla.eazel.com 4413: This function does not take
	// into account a BASE tag in an HTML document, so its
	// functionality differs from what Mozilla itself would do.
	//

	if (dh_util_uri_is_relative (uri)) {
		char *mutable_base_uri;
		char *mutable_uri;

		char *uri_current;
		size_t base_uri_length;
		char *separator;

		// We may need one extra character
		// to append a "/" to uri's that have no "/"
		// (such as help:)
		//

		mutable_base_uri = g_malloc(strlen(base_uri)+2);
		strcpy (mutable_base_uri, base_uri);

		uri_current = mutable_uri = g_strdup (uri);

		// Chew off Fragment and Query from the base_url

		separator = strrchr (mutable_base_uri, '#');

		if (separator) {
			*separator = '\0';
		}

		separator = strrchr (mutable_base_uri, '?');

		if (separator) {
			*separator = '\0';
		}

		if ('/' == uri_current[0] && '/' == uri_current [1]) {
			// Relative URI's beginning with the authority
			// component inherit only the scheme from their parents


			separator = strchr (mutable_base_uri, ':');

			if (separator) {
				separator[1] = '\0';
			}
		} else if ('/' == uri_current[0]) {
			// Relative URI's beginning with '/' absolute-path based
			// at the root of the base uri

			separator = strchr (mutable_base_uri, ':');

			// g_assert (separator), really
			if (separator) {
				// If we start with //, skip past the authority section
				if ('/' == separator[1] && '/' == separator[2]) {
					separator = strchr (separator + 3, '/');
					if (separator) {
						separator[0] = '\0';
					}
				} else {
				// If there's no //, just assume the scheme is the root
					separator[1] = '\0';
				}
			}
		} else if ('#' != uri_current[0]) {
			// Handle the ".." convention for relative uri's

			// If there's a trailing '/' on base_url, treat base_url
			// as a directory path.
			// Otherwise, treat it as a file path, and chop off the filename

			base_uri_length = strlen (mutable_base_uri);
			if ('/' == mutable_base_uri[base_uri_length-1]) {
				// Trim off '/' for the operation below
				mutable_base_uri[base_uri_length-1] = 0;
			} else {
				separator = strrchr (mutable_base_uri, '/');
				if (separator) {
					*separator = '\0';
				}
			}

			remove_internal_relative_components (uri_current);

			// handle the "../"'s at the beginning of the relative URI
			while (0 == strncmp ("../", uri_current, 3)) {
				uri_current += 3;
				separator = strrchr (mutable_base_uri, '/');
				if (separator) {
					*separator = '\0';
				} else {
					// <shrug>
					break;
				}
			}

			// handle a ".." at the end
			if (uri_current[0] == '.' && uri_current[1] == '.'
			    && uri_current[2] == '\0') {

			    	uri_current += 2;
				separator = strrchr (mutable_base_uri, '/');
				if (separator) {
					*separator = '\0';
				}
			}

			// Re-append the '/'
			mutable_base_uri [strlen(mutable_base_uri)+1] = '\0';
			mutable_base_uri [strlen(mutable_base_uri)] = '/';
		}

		result = g_strconcat (mutable_base_uri, uri_current, NULL);
		g_free (mutable_base_uri);
		g_free (mutable_uri);

	} else {
		result = g_strdup (uri);
	}

	return result;
}
*/


static gboolean
object_requested (GtkHTML* html, GtkHTMLEmbedded* ew)
{
  //note: ew = embedded widget

  //FIXME showing embedded widget does NOT currently work.
  //-this is taken from the v1 tutorial so may need some tweaking..

  PF;

  //this is in gtkhtml-embedded:
  if (!strcmp (ew->classid, "close_button") == 0)
  return FALSE;
  else dbg(0, "class_id OK.\n");

  GtkWidget* button = gtk_button_new_with_label ("Close");
  gtk_widget_show (button);

  return TRUE;
}


static void
html_init (DhHtml *html)
{
  /* 

  +--scrollwindow
     +--widget (gtkhtml)
  
  */
  DhHtmlPriv *priv = g_new0 (DhHtmlPriv, 1);

  priv->scrolled_window = gtk_scrolled_window_new(NULL, NULL);
  priv->base_url        = NULL;

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(priv->scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  //g_signal_connect (G_OBJECT (priv->widget), "request_url", G_CALLBACK (ayyi_gtkhtml_on_url_requested), html);

  html->priv = priv;
}


static struct htmlviewPlugin gtkhtmlInfo = {
	.api_version    = HTMLVIEW_PLUGIN_API_VERSION,
	.name           = "GtkHtml3",
	.priority       = 50,
	.externalCss    = FALSE,
	.plugin_init    = ayyi_gtkhtml_init,
	.plugin_deinit  = ayyi_gtkhtml_deinit,
	.create         = ayyi_gtkhtml_new,
	.write          = ayyi_gtkhtml_write_html,
	.launch         = ayyi_gtkhtml_launch_url,
	.zoomLevelGet   = NULL,//webkit_get_zoom_level,
	.zoomLevelSet   = NULL,//webkit_change_zoom_level,
	.scrollPagedown = NULL,//webkit_scroll_pagedown,
	.setProxy       = NULL,
	.setOffLine     = NULL
};

static struct htmlplugin pi = {
	PLUGIN_API_VERSION,
	"GtkHtml3 Rendering Plugin",
	PLUGIN_TYPE_HTML_RENDERER,
	&gtkhtmlInfo
};

DECLARE_PLUGIN(pi);
DECLARE_HTMLVIEW_PLUGIN(gtkhtmlInfo);

