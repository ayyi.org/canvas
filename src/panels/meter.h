/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __meter_h__
#define __meter_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_METERWIN            (ayyi_meterwin_get_type ())
#define AYYI_METERWIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_METERWIN, AyyiMeterWin))
#define AYYI_METERWIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_METERWIN, AyyiMeterWinClass))
#define AYYI_IS_METERWIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_METERWIN))
#define AYYI_IS_METERWIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_METERWIN))
#define AYYI_METERWIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_METERWIN, AyyiMeterWinClass))

typedef struct _AyyiMeterWinClass AyyiMeterWinClass;

GType      ayyi_meterwin_get_type ();


G_END_DECLS

#endif
