/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public class Ayyi.ShowplaylistsClient : Ayyi.SimpleClient
{
	public static int main(string[] args)
	{
		app = new ShowplaylistsClient();
		return (int)app.run(((ShowplaylistsClient*)app)->on_shm);
	}

	private void on_shm()
	{
		print("-----------------------------------------------------------------\n");
		print("playlists:\n");
		if((bool)AyyiSong.playlist_next(null)){
			print("  idx flg   tr %20s\n", "name");
			Playlist* playlist = null;
			while((bool)(playlist = AyyiSong.playlist_next(playlist))){
				stdout.printf("  %3i %3i %c%3i %20s\n", playlist->shm_idx, playlist->flags, AyyiSong.playlist_is_midi(playlist) ? 'M' : 'A', playlist->track, (string)playlist->name);
			}
		}
		else print("\tno playlists\n");

		print("-----------------------------------------------------------------\n");

		app.quit();
	}
}


