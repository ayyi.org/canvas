/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __mixer_h__
#define __mixer_h__

#include <model/mixer.h>
#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_MIXER_WIN            (ayyi_mixer_win_get_type ())
#define AYYI_MIXER_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_MIXER_WIN, AyyiMixerWin))
#define AYYI_MIXER_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_MIXER_WIN, AyyiListMixerClass))
#define AYYI_IS_MIXER_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_MIXER_WIN))
#define AYYI_IS_MIXER_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_MIXER_WIN))
#define AYYI_MIXER_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_MIXER_WIN, AyyiMixerWinClass))

typedef struct _AyyiMixerWinClass AyyiMixerWinClass;

#define IS_MIXER (AYYI_IS_MIXER_WIN(panel))
#define MIXER_FIRST (MixerWin*)windows__get_first(AYYI_TYPE_MIXER_WIN)
#define mixer_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_MIXER_WIN) continue; \
		MixerWin* mixer = (MixerWin*)panel;
#define end_mixer_foreach end_panel_foreach

#include "mixer/strip.h"

/*
	gui mixer object types:

	Channel   - (model) dynamic array of mixer strips.
	MixerWin  - (view)  struct holding data for each mixer window. Holds pointers to its gui strips.
	Strip     - (view)  a single mixer strip display. Allocated on demand.

*/

typedef struct {
	GtkWidget* menu;

	GtkWidget* use_rotaries;
	GtkWidget* show_aux_values;

	GPtrArray* to_free;
} MixerMenu;

struct _AyyiMixerWinClass {
	AyyiPanelClass  parent_class;
};

struct _AyyiMixerWin
{
    AyyiPanel       panel;
    GtkWidget*      hbox;                 // the main hbox - used at least by resize
    GtkWidget*      scrollwin;
#ifndef USE_MULTICONTAINER
    double          hzoom;                // the hzoom widget *value*. Is also the ch width in pixels (i think).
#else
    double          dummy;
#endif
    GtkObject*      hzoom_adj;            // the horizontal zoom Adjustment.
    GtkWidget*      hzoom_scale;
    double          ch_base_width;        // the width of channels that have not been individually modified.
                                          //   -initially it is equal to hzoom.
                                          //   -should be updated whenever the window is resized.

    AyyiMixerStrip* strips[AM_MAX_CHS+1]; // pointer to the strips's. (+1 for master.) **Cannot contain gaps**.
                                          // The index is unique to this array - it cant be used for any other arrays.

    GtkSizeGroup*   strip_label_sizegroup;

    MixerMenu*      contextmenu;          // is now unique per window.
    GList*          selectionlist;        // list of channel strip numbers.

    MixerOptions    options;

    bool            resize_done;
    int             resize_count;
    int             resize_timeout;
};


GType       ayyi_mixer_win_get_type     ();
void        mixer_init                  ();
AyyiMixerStrip*
            mixer__strip_next           (MixerWin*, AyyiMixerStrip*);
int         mixer__n_visible_strips     (MixerWin*);

gint        mixer__drag_drop            (GtkWidget*, GdkDragContext*, gint x, gint y, guint time, gpointer);

void        mixer__select_channel       (MixerWin*, const AMChannel*);
void        mixer__selection_replace    (MixerWin*, AyyiMixerStrip*);
void        mixer__selection_add        (MixerWin*, Strip*);
void        mixer__selection_subtract   (MixerWin*, Strip*);

void        aux_menu__popup             (GtkWidget*, GdkEventButton*);

void        mixer__print_channels       ();

G_END_DECLS

#endif
