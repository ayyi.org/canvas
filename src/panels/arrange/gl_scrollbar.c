/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 */

#define R 2
#define Rf 2.0
#define SCROLLBAR_WIDTH (2. * Rf + 8.)
#define V_SCROLLBAR_H_PADDING 2
#define V_SCROLLBAR_V_PADDING 3
#define H_SCROLLBAR_H_PADDING 3
#define H_SCROLLBAR_V_PADDING 2

AGlActor*   scrollbar_view                 (AGlActor*, AGlOrientation, AGlObservable*);
static void scrollbar_free                 (AGlActor*);
static void scrollbar_set_size             (AGlActor*);
static bool arr_scrollbar_on_event         (AGlActor*, GdkEvent*, AGliPt);
static void scrollbar_on_scrollable_width  (AGlObservable*, AGlVal, gpointer);
static void scrollbar_on_scrollable_height (AGlObservable*, AGlVal, gpointer);
static void scrollbar_start_activity       (AGlActor*, bool);

static void arr_hscrollbar_bar_position    (AGlActor*, iRange*);
static void arr_vscrollbar_bar_position    (AGlActor*, iRange*);

typedef struct {
    AGlActor       actor;
    AGlOrientation orientation;
    AGlActor*      scrollable;
    AGlObservable* size;
    struct {
      float        opacity;
      WfAnimatable animation;
    }              handle;
} ScrollbarActor;

static struct Press {
    AGliPt     pt;
    AGliPt     offset; // TODO this should probably be part of the actor_context
    AGliRegion viewport;
} press = {{0,}};

static guint activity = 0;

static AGlActorClass actor_class = {0, "Scrollbar", (AGlActorNew*)scrollbar_view, scrollbar_free};

AGlActorClass*
scrollbar_view_get_class ()
{
	return &actor_class;
}

AGlActor*
scrollbar_view (AGlActor* scrollable, AGlOrientation orientation, AGlObservable* scrollable_size)
{
	bool scrollbar_draw_gl1 (AGlActor* actor)
	{
		return true;
	}

	bool scrollbar_draw_h (AGlActor* actor)
	{
		iRange bar;
		arr_hscrollbar_bar_position(actor, &bar);

		if(!actor->disabled){
			if(((ScrollbarActor*)actor)->handle.opacity > 0.61){
				SET_PLAIN_COLOUR (agl->shaders.plain, 0xffffff66);
				agl_use_program (agl->shaders.plain);
				agl_rect (0., 1., agl_actor__width(actor), agl_actor__height(actor) - 1.);
			}

			H_SCROLLBAR_COLOUR() = (get_style_bg_color_rgba(GTK_STATE_SELECTED) & 0xffffff00) + (int)(((ScrollbarActor*)actor)->handle.opacity * 0xff);
			H_SCROLLBAR_BG_COLOUR() = H_SCROLLBAR_COLOUR() & 0xffffff00;
			h_scrollbar_shader.uniform.centre1 = (AGliPt){bar.start + 4, 6};
			h_scrollbar_shader.uniform.centre2 = (AGliPt){bar.end   - 4, 6};
			agl_use_program((AGlShader*)&h_scrollbar_shader);

			agl_rect(H_SCROLLBAR_H_PADDING + bar.start, H_SCROLLBAR_V_PADDING, bar.end - bar.start, 11);
		}
		return true;
	}

	bool scrollbar_draw_v (AGlActor* actor)
	{
		if (!actor->disabled) {
			iRange bar;
			arr_vscrollbar_bar_position(actor, &bar);

			V_SCROLLBAR_COLOUR() = (get_style_bg_color_rgba(GTK_STATE_SELECTED) & 0xffffff00) + (int)(((ScrollbarActor*)actor)->handle.opacity * 0xff);
			V_SCROLLBAR_BG_COLOUR() = V_SCROLLBAR_COLOUR() & 0xffffff00;
			v_scrollbar_shader.uniform.centre1 = (AGliPt){6, bar.start + 4};
			v_scrollbar_shader.uniform.centre2 = (AGliPt){6, bar.end - 4};
			agl_use_program((AGlShader*)&v_scrollbar_shader);

			agl_rect (V_SCROLLBAR_H_PADDING, bar.start, 10, bar.end - bar.start);
		}
		return true;
	}

	/*
	 *  Scrollbar does not have a shader property because the first draw (background) uses the plain shader.
	 */
	void scrollbar_init (AGlActor* actor)
	{
		ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

		if (scrollbar->orientation == AGL_ORIENTATION_VERTICAL) {
			if (!v_scrollbar_shader.shader.program) {
				agl_create_program(&v_scrollbar_shader.shader);
				v_scrollbar_shader.uniform.radius = 3;
			}

			actor->paint = agl->use_shaders ? scrollbar_draw_v : agl_actor__null_painter;

			if (scrollbar->size) agl_observable_subscribe (scrollbar->size, scrollbar_on_scrollable_height, actor);
		} else {
			if(!h_scrollbar_shader.shader.program){
				agl_create_program(&h_scrollbar_shader.shader);
				h_scrollbar_shader.uniform.radius = 3;
			}
			actor->paint = agl->use_shaders ? scrollbar_draw_h : scrollbar_draw_gl1;

			if (scrollbar->size) agl_observable_subscribe (scrollbar->size, scrollbar_on_scrollable_width, actor);
		}
	}

	ScrollbarActor* scrollbar = AGL_NEW (ScrollbarActor,
		.actor = {
			.class = &actor_class,
			.init = scrollbar_init,
			.set_size = scrollbar_set_size,
			.paint = agl_actor__null_painter,
			.on_event = arr_scrollbar_on_event,
			.region = (AGlfRegion){0, 0, 1, 1},
		},
		.orientation = orientation,
		.scrollable = scrollable,
		.size = scrollable_size,
		// TODO fade in and out when leaving/entering window
		.handle = {
			.opacity = 0.5,
			.animation = {
				.start_val.f  = 0.6,
				.target_val.f = 0.6,
				.type         = WF_FLOAT
			}
		}
	);
	scrollbar->handle.animation.val.f = &scrollbar->handle.opacity;

	return (AGlActor*)scrollbar;
}


static void
scrollbar_free (AGlActor* actor)
{
	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	if (scrollbar->size) agl_observable_unsubscribe (scrollbar->size, scrollbar_on_scrollable_height, actor);
	am_source_remove0(activity);
	g_free(actor);
}


static void
scrollbar_set_size (AGlActor* actor)
{
	Arrange* arrange = actor->root->user_data;
	AGlActor* scrollable = ((ScrollbarActor*)actor)->scrollable;

	if (scrollable->region.x2 > 0. && actor->region.x2 > 0) {
		if(((ScrollbarActor*)actor)->orientation == AGL_ORIENTATION_VERTICAL){
			actor->region = (AGlfRegion){
				.x1 = scrollable->region.x2 - V_SCROLLBAR_H_PADDING - 2 * R - 5,
				.x2 = scrollable->region.x2 - V_SCROLLBAR_H_PADDING,
				.y1 = V_SCROLLBAR_V_PADDING,
				.y2 = agl_actor__height(scrollable) - V_SCROLLBAR_V_PADDING
			};
		}else{
			int offset = SHOW_OVERVIEW ? -OVERVIEW_HEIGHT : 0;

			actor->region = (AGlfRegion){
				.x1 = 0,
				.x2 = scrollable->region.x2,
				.y1 = scrollable->region.y2 - H_SCROLLBAR_V_PADDING - 2 * R - 5 + offset,
				.y2 = scrollable->region.y2                                     + offset
			};
		}
	}
}


static void
arr_hscrollbar_bar_position (AGlActor* actor, iRange* pos)
{
	AGlActor* scrollable = ((ScrollbarActor*)actor)->scrollable;
	Arrange* arrange = actor->root->user_data;

	double songlen = arr_gl_pos2px_(arrange, &am_object_val(&song->loc[AM_LOC_END]).sp) + 20;
	double vp_width = agl_actor__width(scrollable);
	double pct = vp_width / songlen;
	int len = MAX(28, pct * agl_actor__width(actor));

	if(pct < 1.0){
		pos->start = 1 - CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 * pct;
		pos->end = pos->start + len;
	}else{
		*pos = (iRange){0,}; // no scrollbar needed
	}
}


static void
arr_vscrollbar_bar_position (AGlActor* actor, iRange* pos)
{
	Arrange* arrange = actor->root->user_data;
	GtkAdjustment* adj = arrange->canvas->v_adj;

	double scaling = agl_actor__height(actor) / adj->upper;
	if(scaling < 1.0){
		pos->start = 1 - CGL->actors[ACTOR_TYPE_MAIN]->scrollable.y1 * scaling;
		pos->end = pos->start + adj->page_size * scaling;
	}else{
		*pos = (iRange){0,}; // no scrollbar needed
	}
}


static bool
arr_scrollbar_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	void animation_done (WfAnimation* animation, gpointer user_data)
	{
	}

	Arrange* arrange = actor->root->user_data;
	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	int x = xy.x;

	bool handled = false;

	switch (event->type){
		case GDK_MOTION_NOTIFY:
			if (actor_context.grabbed == actor) {
				if (scrollbar->orientation == AGL_ORIENTATION_VERTICAL) {
					int dy = xy.y - press.pt.y;
					double scale = arrange->canvas->v_adj->upper / (double)agl_actor__height(actor);
					arrange->canvas->scroll_to(arrange, -1, ((int)-press.viewport.y1) + dy * scale);
				} else {
					// would it simplify to use h_adj here?
					int dx = x - press.pt.x;
					double vp_size = arr_gl_pos2px_(arrange, &am_object_val(&song->loc[AM_LOC_END]).sp) + 20.0;
					double scale = vp_size / (double)agl_actor__width(actor);
					arrange->canvas->scroll_to(arrange, ((int)-press.viewport.x1) + dx * scale, -1);
				}
			}
			break;
		case GDK_ENTER_NOTIFY:
			scrollbar->handle.animation.target_val.f = 1.0;
			agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->handle.animation), animation_done, NULL);
			break;
		case GDK_LEAVE_NOTIFY:
			scrollbar->handle.animation.target_val.f = 0.6;
			agl_actor__start_transition(actor, g_list_append(NULL, &scrollbar->handle.animation), animation_done, NULL);
			break;
		case GDK_BUTTON_PRESS:
			{
				iRange bar;
				if (((ScrollbarActor*)actor)->orientation == AGL_ORIENTATION_VERTICAL) {
					arr_vscrollbar_bar_position (actor, &bar);
					if(xy.y > bar.start && xy.y < bar.end){
						actor_context.grabbed = actor;
						handled = true;
					}
				} else {
					arr_hscrollbar_bar_position (actor, &bar);
					if(x > bar.start && x < bar.end){
						actor_context.grabbed = actor;
						handled = true;
					}
				}

				press = (struct Press){
					.pt = xy,
					.offset = (AGliPt){xy.x - bar.start, xy.y - bar.start},
					.viewport = CGL->actors[ACTOR_TYPE_MAIN]->scrollable
				};
			}
			break;
		case GDK_BUTTON_RELEASE:
			actor_context.grabbed = NULL;
			handled = true;
			break;
		default:
			break;
	}
	return handled;
}


static void
scrollbar_on_scrollable_height (AGlObservable* observable, AGlVal value, gpointer scrollbar)
{
	scrollbar_set_size (scrollbar);
	scrollbar_start_activity (scrollbar, false);
	agl_actor__invalidate (scrollbar);
}


static void
scrollbar_on_scrollable_width (AGlObservable* observable, AGlVal value, gpointer scrollbar)
{
}


static gboolean
go_inactive (gpointer _view)
{
	PF;
	AGlActor* actor = _view;
	ScrollbarActor* scrollbar = _view;

	if (agl_actor__is_hovered(actor)) {
		return G_SOURCE_CONTINUE;
	}

	activity = 0;

	scrollbar->handle.animation.target_val.f = 0.0;
	agl_actor__start_transition (actor, g_list_append(NULL, &scrollbar->handle.animation), NULL, NULL);

	return G_SOURCE_REMOVE;
}


/*
 *  hovered refers to the handle, not the trough
 */
static void
scrollbar_start_activity (AGlActor* actor, bool hovered)
{
	ScrollbarActor* scrollbar = (ScrollbarActor*)actor;

	actor->disabled = false;

	scrollbar->handle.animation.target_val.f = hovered ? 1.0 : 0.7;
	agl_actor__start_transition (actor, g_list_append(NULL, &scrollbar->handle.animation), NULL, NULL);

	if (activity) {
		g_source_remove (activity);
	}
	activity = g_timeout_add (5000, go_inactive, actor);
}
