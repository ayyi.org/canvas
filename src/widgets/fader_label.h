/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __fader_label2_h__
#define __fader_label2_h__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TYPE_FADER_LABEL            (fader_label_get_type ())
#define FADER_LABEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FADER_LABEL, FaderLabel))
#define FADER_LABEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FADER_LABEL, FaderLabelClass))
#define IS_FADER_LABEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FADER_LABEL))
#define IS_FADER_LABEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FADER_LABEL))
#define FADER_LABEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FADER_LABEL, FaderLabelClass))

typedef struct _FaderLabel FaderLabel;
typedef struct _FaderLabelClass FaderLabelClass;
typedef struct _FaderLabelPrivate FaderLabelPrivate;

struct _FaderLabel {
	GtkDrawingArea parent_instance;
	FaderLabelPrivate * priv;
};

struct _FaderLabelClass {
	GtkDrawingAreaClass parent_class;
};


GType       fader_label_get_type  () G_GNUC_CONST;
FaderLabel* fader_label_new       ();
FaderLabel* fader_label_construct (GType);


G_END_DECLS

#endif
