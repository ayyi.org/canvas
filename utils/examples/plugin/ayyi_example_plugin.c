/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

/*
  Example plugin service for Ayyi system
  --------------------------------------

  When run, the app will create a new shm segment to share data, and start an 
  associated dbus service to provide functions to modify the shm data.

  This simple example does not show how to install a dll for service discovery and 
  client usage of our exported shm segment. See the Spectrogram plugin for a more
  complete real-life example.

*/
#include "config.h"
#include <stdio.h>
#include <sys/time.h>
#include <glib.h>

#include <ayyi/ayyi.h>
#include <ayyi/ayyi_server.h>
#include "dbus.h"

#define SEG_SIZE 8

AyyiServer* ayyi_server = NULL;


int
main (int argc, char **argv)
{
	// Initialise Ayyi, and export a single 4k shm segment
	AyyiShmSeg* seg = ayyi_server__shm_seg__new(AYYI_SEG_TYPE_PLUGIN, SEG_SIZE);
	ayyi_server = ayyi_server__new(g_list_append(NULL, seg));

	// Instantiate and start our dbus proxy GObject:
	ExampleDbus* bus = example_dbus_get_instance();
	if (!example_dbus_register_service (bus)) {
		printf("dbus registration failed!\n");
		exit(1);
	}

	// The server is now running
	// We can add shared data, emit events and respond to service requests

	g_main_loop_run (g_main_loop_new (NULL, 0));

	return EXIT_SUCCESS;
}

