/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

GtkWidget* meter_new          (AyyiMixerStrip*, int n);
void       meter_destroy      (AyyiMixerStrip*, int n);

void       meters_start       ();
void       meters_stop        ();
void       meters_demo_toggle ();

