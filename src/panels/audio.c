/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "global.h"
#include <gdk/gdkkeysyms.h>
#ifdef HAVE_GTK_2_22
#include <gdk/gdkkeysyms-compat.h>
#endif
#include "agl/utils.h"
#include "agl/text/text_node.h"
											#undef USE_VIEW
											#ifdef USE_VIEW
#include "waveform/view.h"
											#else
											#include "waveform/view_plus.h"
											#endif
#include "support.h"
#include "song.h"
#include "pool_model.h"
#include "../shortcuts.h"
#include "audio.h"

G_DEFINE_TYPE (AyyiAudioWin, ayyi_audio_win, AYYI_TYPE_PANEL)

static GObject* audio_win_construct  (GType, guint n_construct_properties, GObjectConstructParam*);
static void   audio_win_destroy      (GtkObject*);
static void   audio_win_on_ready     (AyyiPanel*);
static void   audio_win_update       (AyyiAudioWin*);
static bool   audio_win_on_key_press (GtkWidget*, GdkEventKey*, gpointer);
static bool   audio_win_on_focus_in  (GtkWindow*, GdkEventFocus*, AyyiPanel*);
static gint   audio_drag_received    (GtkWidget*, GdkDragContext*, gint x, gint y, GtkSelectionData*, guint info, guint time, gpointer);


static void
ayyi_audio_win_class_init (AyyiAudioWinClass* klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);

    g_object_class->constructor = audio_win_construct;

	object_class->destroy  = audio_win_destroy;

	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	panel->name           = "Audio";
	panel->has_link       = true;
	panel->has_follow     = true;
	panel->accel          = 0;
	panel->default_size.x = 480;
	panel->default_size.y = 200;
	panel->on_ready       = audio_win_on_ready;

	void dummy_callback(AyyiPanel* panel){
		dbg(0, "...");
	}

	AMAccel list_key[] = {
    	{"Dummy",    {{(char)'j',    GDK_CONTROL_MASK}, {0, 0}}, dummy_callback, NULL, NULL},
		// needs to be trapped elsewhere
    	{"Zoom In",  {{GDK_KP_Equal, 0               }, {0, 0}}, dummy_callback, NULL, NULL},
	};
	panel->action_group = shortcuts_add_group(panel->name);
	panel->accel_group = gtk_accel_group_new();
	make_accels(panel->accel_group, panel->action_group, list_key, G_N_ELEMENTS(list_key), NULL);

	waveform_view_plus_set_gl(agl_get_gl_context());
}


static GObject*
audio_win_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_audio_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_AUDIO_WIN);

	return g_object;
}


static void
ayyi_audio_win_init (AyyiAudioWin* object)
{
}


static void
audio_win_destroy (GtkObject* object)
{
	PF;
	AyyiAudioWin* audio = (AyyiAudioWin*)object;
	if(!((AyyiPanel*)object)->destroyed){

#if 0
		#define parts_handler_disconnect() \
			if(g_signal_handlers_disconnect_matched (am_parts, G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, audio) != 1) pwarn("handler disconnection");
		parts_handler_disconnect();
		#undef parts_handler_disconnect
#endif

		observable_unsubscribe(am_parts->selection2, NULL, audio);
		observable_unsubscribe(am_pool->selection2, NULL, audio);
	}

	GTK_OBJECT_CLASS(ayyi_audio_win_parent_class)->destroy(object);

	((AyyiPanel*)object)->destroyed = true;
}


static void
audio_win_on_ready (AyyiPanel* panel)
{
	AyyiAudioWin* audio_win = (AyyiAudioWin*)panel;

	if (audio_win->waveform) return;

#ifdef USE_VIEW
	audio_win->waveform = waveform_view_new(NULL);
#else
	WaveformViewPlus* waveform = audio_win->waveform = waveform_view_plus_new(NULL);

	waveform_view_plus_add_layer(waveform, background_actor(NULL), 0);

	AGlActor* text = text_node(NULL);
	text->region = (AGlfRegion){5, 2, 100, 40};
	AGlActor* text_layer = waveform_view_plus_add_layer(audio_win->waveform, text, 3);
	text_layer->colour = 0x0000aa;
	((TextNode*)text_layer)->font.size = 8;
	((TextNode*)text_layer)->font.name = "Open Sans";
#endif
	gtk_widget_show((GtkWidget*)audio_win->waveform);
	panel_pack((GtkWidget*)audio_win->waveform, EXPAND_TRUE);

	g_signal_connect((gpointer)audio_win->waveform, "key-press-event", G_CALLBACK(audio_win_on_key_press), panel);
	g_signal_connect(G_OBJECT(panel), "focus-in-event", G_CALLBACK(audio_win_on_focus_in), panel);

	gtk_drag_dest_set((GtkWidget*)audio_win->waveform, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction)(GDK_ACTION_MOVE | GDK_ACTION_COPY));
	g_signal_connect(G_OBJECT(audio_win->waveform), "drag-data-received", G_CALLBACK(audio_drag_received), panel);

	void audio_on_selection_change (Observable* o, AMVal val, gpointer audio){ audio_win_update((AyyiAudioWin*)audio); }
	observable_subscribe(am_parts->selection2, audio_on_selection_change, audio_win);
	observable_subscribe(am_pool->selection2, audio_on_selection_change, audio_win);

	void shell_panel_finalize_notify (gpointer _audio_win, GObject* was)
	{
		PF;
		AyyiAudioWin* audio_win = _audio_win;
		audio_win->waveform = NULL;
	}
	g_object_weak_ref((GObject*)audio_win->waveform, shell_panel_finalize_notify, audio_win);

	audio_win_update(audio_win);
}


static void
audio_win_update (AyyiAudioWin* audio)
{
	PF;

	void set_item (AMPoolItem* pi, uint32_t fg_colour, int bg_colour, char* name)
	{
		if (audio->waveform->waveform != (Waveform*)pi) {
			uint32_t fg = (fg_colour & 0xffffff00) + 0xbb;

#ifdef USE_VIEW
			waveform_view_set_waveform(audio->waveform, (Waveform*)pi);
#else
			waveform_view_plus_set_waveform(audio->waveform, (Waveform*)pi);
			waveform_view_plus_set_colour(audio->waveform, fg, song->palette[bg_colour]);

			AGlActor* text = waveform_view_plus_get_layer(audio->waveform, 3);
			text_node_set_text((TextNode*)text, name);
#endif
		} else {
			g_free(name);
		}
#ifdef USE_VIEW
		waveform_view_set_colour(audio->waveform, part->fg_colour, song->palette[part->bg_colour]);
#endif
	}

	GList* p = am_parts->selection;
	if (p) {
		AMPart* part = p->data;
		if (part->pool_item) {
			set_item(part->pool_item, part->fg_colour, part->bg_colour, g_strdup(part->name));
			return;
		}
	}

	if(am_pool->selection){
		AMPoolItem* pi = am_pool->selection->data;
		char* name = g_malloc(256);
		pool_item_get_display_name(pi, name);
		set_item(pi, get_style_fg_color_rgba(GTK_STATE_NORMAL), 0, name);
		return;
	}

	AMPart* part = am_list_first(song->parts);
	if(part){
		AMPoolItem* pi = part->pool_item;
		if(pi){
			set_item(pi, part->fg_colour, part->bg_colour, g_strdup(part->name));
		}
	}
}


static bool
audio_win_on_key_press (GtkWidget* widget, GdkEventKey* event, gpointer panel)
{
	AyyiAudioWin* audio = (AyyiAudioWin*)panel;
#ifdef USE_VIEW
	WaveformView* waveform = audio->waveform;
#else
	WaveformViewPlus* waveform = audio->waveform;
#endif
	int handled = false;
	switch(event->keyval){
		case GDK_equal:
		case GDK_plus:
#ifdef USE_VIEW
			waveform_view_set_zoom(waveform, waveform->zoom * 1.5);
#else
			waveform_view_plus_set_zoom((WaveformViewPlus*)waveform, waveform_view_plus_get_zoom(waveform) * 1.5);
#endif
			break;
		case GDK_minus:
#ifdef USE_VIEW
			waveform_view_set_zoom(waveform, waveform->zoom / 1.5);
#else
			waveform_view_plus_set_zoom(waveform, waveform_view_plus_get_zoom(waveform) / 1.5);
#endif
			break;
		case GDK_Left:
		case GDK_KP_Left:
			waveform_view_plus_set_start(waveform, waveform->start_frame - 8192 / waveform_view_plus_get_zoom(waveform));
			handled = true;
			break;
		case GDK_Right:
		case GDK_KP_Right:
			waveform_view_plus_set_start(waveform, waveform->start_frame + 8192 / waveform_view_plus_get_zoom(waveform));
			handled = true;
			break;
	}
	return handled;
}


static bool
audio_win_on_focus_in (GtkWindow* window, GdkEventFocus* event, AyyiPanel* panel)
{
	AyyiAudioWin* audio_win = (AyyiAudioWin*)panel;
	gtk_widget_grab_focus((GtkWidget*)audio_win->waveform);
	return NOT_HANDLED;
}


static gint
audio_drag_received (GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, GtkSelectionData* data, guint info, guint time, gpointer user_data)
{
	// supported drag types:
	//  - colour     no
	//  - file       yes (from pool window)
	//  - part       no

	AyyiAudioWin* audio = user_data;

	if(data == NULL || data->length < 0){ pwarn ("no data!"); return -1; }

	dbg(1, "%s", data->data);

	switch(info){
		case AYYI_TARGET_URI_LIST:
			;GList* list = uri_list_to_glist((char*)data->data);
			GList* l = list;
			for(;l;l=l->next){
				char* u = l->data;
				gchar* method_string;
				vfs_get_method_string(u, &method_string);
				dbg (1, "method=%s", method_string);

				if(!strcmp(method_string, "pool")){
					PoolDrop data;
					pool_get_dropdata(u, &data);
					if(data.file){
						dbg(1, "item=%s", data.file->leafname);
#ifdef USE_VIEW
						waveform_view_set_waveform(audio->waveform, (Waveform*)data.file);
#else
						waveform_view_plus_set_waveform(audio->waveform, (Waveform*)data.file);
#endif
					}
				}
			}
			uri_list_free(list);
			break;
	}

	return false;
}
