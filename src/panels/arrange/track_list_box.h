/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2014 Tim Orford <tim@orford.org>                       |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __TRACKLISTBOX_H__
#define __TRACKLISTBOX_H__
#include <glib.h>
#include <gtk/gtk.h>
#include "arrange.h"

G_BEGIN_DECLS


#define TYPE_TRACK_LIST_BOX (track_list_box_get_type ())
#define TRACK_LIST_BOX(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TRACK_LIST_BOX, TrackListBox))
#define TRACK_LIST_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TRACK_LIST_BOX, TrackListBoxClass))
#define IS_TRACK_LIST_BOX(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TRACK_LIST_BOX))
#define IS_TRACK_LIST_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TRACK_LIST_BOX))
#define TRACK_LIST_BOX_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TRACK_LIST_BOX, TrackListBoxClass))

typedef struct _TrackListBox TrackListBox;
typedef struct _TrackListBoxClass TrackListBoxClass;
typedef struct _TrackListBoxPrivate TrackListBoxPrivate;

struct _TrackListBox {
   GtkViewport          parent_instance;
   Arrange*             arrange;

   GtkWidget*           ev;
   GtkWidget*           vbox;              // vbox used to hold the track controls and labels.

   int                  drag_begin_y;      // position of mouse cursor when the drag was started. In canvas coords.
   TrackListBoxPrivate* priv;
};

struct _TrackListBoxClass {
	GtkViewportClass parent_class;
};


GType         track_list_box_get_type      () G_GNUC_CONST;
TrackListBox* track_list_box_new           (Arrange*);
TrackListBox* track_list_box_construct     (GType);
void          track_list_box_on_dim_change (Arrange*);

G_END_DECLS

#endif
