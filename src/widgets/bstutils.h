/* BEAST - Bedevilled Audio System
 * Copyright (C) 1998-2006 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * A copy of the GNU Lesser General Public License should ship along
 * with this library; if not, see http://www.gnu.org/copyleft/.
 */
#ifndef __BST_UTILS_H__
#define __BST_UTILS_H__

#include "bstbseutils.h"
#include <gtk/gtk.h>
#include "bstdefs.h"
#include "bstcluehunter.h"

G_BEGIN_DECLS

/* --- GUI utilities --- */
void           bst_status_eprintf             (BseErrorType     error,
                                               const gchar     *message_fmt,
                                               ...) G_GNUC_PRINTF (2, 3);
GtkWidget*     bst_xpm_view_create            (const gchar    **xpm,
                                               GtkWidget       *colormap_widget);

/* --- GUI field mask --- */
typedef struct _BstGMask BstGMask;
GtkWidget*   bst_gmask_container_create (guint          border_width,
                                         gboolean       dislodge_columns);
typedef enum /*< skip >*/
{
  BST_GMASK_FIT,
  BST_GMASK_FILL,
  BST_GMASK_INTERLEAVE, /* stretch */
  BST_GMASK_BIG,
  BST_GMASK_CENTER,
  BST_GMASK_MULTI_SPAN
} BstGMaskPack;
BstGMask*       bst_gmask_form          (GtkWidget     *gmask_container,
                                         GtkWidget     *action,
                                         BstGMaskPack   gpack);
#define         bst_gmask_form_big(c,a) bst_gmask_form ((c), (a), BST_GMASK_BIG)
void            bst_gmask_set_tip       (BstGMask      *mask,
                                         const gchar   *tip_text);
void            bst_gmask_set_prompt    (BstGMask      *mask,
                                         gpointer       widget);
void            bst_gmask_set_aux1      (BstGMask      *mask,
                                         gpointer       widget);
void            bst_gmask_set_aux2      (BstGMask      *mask,
                                         gpointer       widget);
void            bst_gmask_set_aux3      (BstGMask      *mask,
                                         gpointer       widget);
void            bst_gmask_set_column    (BstGMask      *mask,
                                         guint          column);
GtkWidget*      bst_gmask_get_prompt    (BstGMask      *mask);
GtkWidget*      bst_gmask_get_aux1      (BstGMask      *mask);
GtkWidget*      bst_gmask_get_aux2      (BstGMask      *mask);
GtkWidget*      bst_gmask_get_aux3      (BstGMask      *mask);
GtkWidget*      bst_gmask_get_action    (BstGMask      *mask);
void            bst_gmask_foreach       (BstGMask      *mask,
                                         gpointer       func,
                                         gpointer       data);
void            bst_gmask_pack          (BstGMask      *mask);
BstGMask*       bst_gmask_quick         (GtkWidget     *gmask_container,
                                         guint          column,
                                         const gchar   *prompt,
                                         gpointer       action,
                                         const gchar   *tip_text);
#define bst_gmask_set_sensitive(mask, sensitive)        \
    bst_gmask_foreach ((mask), \
                       (sensitive) ? gxk_widget_make_sensitive : gxk_widget_make_insensitive, \
                       NULL)
#define bst_gmask_destroy(mask)                         \
    bst_gmask_foreach ((mask), gtk_widget_destroy, NULL)
#define bst_gmask_ref           g_object_ref
#define bst_gmask_unref         g_object_unref


/* --- object utils --- */
#define bst_object_class_install_property(oclass, group, property_id, pspec) \
  g_object_class_install_property (oclass, property_id, sfi_pspec_set_group (pspec, group))


/* --- generated includes --- */
/* marshallers */
//#include "bstmarshal.h"


/* --- config values --- */
//BstGConfig*     bst_gconfig_get_global (void);
//#define BST_GCONFIG(field) (* bst_gconfig_get_global ()) . field


/* --- internal --- */
void            _bst_init_utils         (void);
void            _bst_init_radgets       (void);

G_END_DECLS

#endif /* __BST_UTILS_H__ */
