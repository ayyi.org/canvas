/*
  Part of the Ayyi project. http://ayyi.org
  copyright (C) 2007-2011 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#define __cl_canvas_c__
#define __arrange_private__
#include "global.h"
#include <sys/time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <clutter/clutter.h>
#include <clutter-gtk/clutter-gtk.h>

#include "model/am_time.h"
#include "model/am_palette.h"
#include "model/part_manager.h"
#include "model/am_utils.h"
#include "window.h"
#include "arrange.h"
#include "support.h"
#include "song.h"
#include "waveform/waveform.h"
#include "canvas_op.h"
#include "gl_canvas_op.h"
#include "widgets/clutter_part.h"
#include "panels/arrange/part.h"

#include "clutter.h"

static void       arr_cl_canvas_free             (Arrange*);
static GtkWidget* arr_cl_canvas_create_widgets   (Arrange*);
static void       arr_cl_canvas_redraw           (Arrange*);
static void       arr_cl_canvas_redraw_fast      (Arrange*);
static void       arr_cl_hscroll_changed         (GtkWidget*, Arrange*);
static void       arr_cl_vscroll_changed         (GtkWidget*, Arrange*);
static gint       arr_cl_canvas_event            (GtkWidget*, GdkEvent*, gpointer _arrange);
static gboolean   arr_cl_part_new                (Arrange*, AMPart*);
static void       arr_cl_part_delete             (Arrange*, AMPart*);
static ClutterPart* arr_cl_part_lookup           (Arrange*, AMPart*);
static void       arr_cl_on_view_change          (Arrange*, AMChangeType);
static void       arr_cl_on_songlength_change    (Arrange*);
static void       arr_cl_on_peak_view_toggle     (Arrange*);
static void       arr_cl_redraw_locators         (Arrange*);
static void       arr_cl_on_part_change          (Arrange*, AMPart*);
static void       arr_cl_do_follow               (Arrange*);
static void       arr_cl_scroll_to               (Arrange*, int x, int y);
static void       arr_cl_scroll_to_pos           (Arrange*, GPos*, int tnum);
static void       arr_cl_scroll_left             (Arrange*, int pix);
static void       arr_cl_scroll_right            (Arrange*, int pix);
static void       arr_cl_get_scroll_offsets      (Arrange*, int* cx, int* cy);
static void       arr_cl_px_to_model             (Arrange*, Pti, AyyiSongPos*, ArrTrk**);
static void       arr_cl_get_view_fraction       (Arrange*, double* fx, double* fy);
static void       arr_cl_draw_curve              (Arrange*);
static uint32_t   arr_cl_get_viewport_left       (Arrange*);
static void       arr_cl_set_spp                 (Arrange*, uint32_t);
#define ENABLE_ANIMATION
//#undef ENABLE_ANIMATION
#ifdef ENABLE_ANIMATION
static guint32    arr_cl_fade_in                 (ClutterAlpha*, gpointer dummy);
#endif
static void       arr_cl_on_allocate             (GtkWidget*, GtkAllocation*, Arrange*);
static void       arr_cl_on_realise              (GtkWidget*, Arrange*);
static gboolean   arr_cl_on_button_press_event   (ClutterStage*, ClutterButtonEvent*, Arrange*);
static gboolean   arr_cl_on_motion_event         (ClutterStage*, ClutterMotionEvent*, Arrange*);
static gboolean   arr_cl_on_button_release_event (ClutterStage*, ClutterButtonEvent*, Arrange*);
static void       arr_cl_on_key_event            (ClutterStage*, ClutterEvent*, Arrange*);
static gboolean   arr_cl_part_on_button_press_event(ClutterActor*, ClutterButtonEvent*, AMPart*);
static void       arr_cl_set_stage_colour        (Arrange*);
static void       arr_cl_set_part_selection      (Arrange*);

static CanvasOp*  cl_canvas_op__new              (Arrange*, ClutterButtonEvent*, Optype type);
static void       cl_canvas_op__free             (Arrange*);
static void       cl_canvas_op__move_press       (CanvasOp*, GdkEvent*);
static void       cl_canvas_op__copy_press       (CanvasOp*, GdkEvent*);
static void       cl_canvas_op__move_motion      (CanvasOp*);
static void       cl_canvas_op__copy_motion      (CanvasOp*);
static void       cl_canvas_op__move_finish      (CanvasOp*);
static void       cl_canvas_op__copy_finish      (CanvasOp*);
static void       cl_canvas_op__box_press        (CanvasOp*, GdkEvent*);
static void       cl_canvas_op__press            (CanvasOp*, GdkEvent*);
static void       arr_cl_pick3                   (Arrange*, double x, double y, int* track, AMPart**);
static void       arr_cl_pick                    (Arrange*, ClutterButtonEvent*);
static void       arr_cl_pick2                   (Arrange*, gint, gint, ClutterPart**, AMPart**, ArrTrk**, gboolean*, gboolean*);
static void       menu_arr_set_cl_canvas         (GtkMenuItem*, Arrange*);

#include "clutter_canvas_op.c"

extern GHashTable* canvas_types;

#define PEAKS_PER_BLOCK 256 //defines the texture size

static CanvasType cl_type = 0;

CanvasType
arr_cl_init()
{
	cl_type = arr_register_canvas("clutter", arr_cl_canvas_new, menu_arr_set_cl_canvas);

	CanvasClass* cc = g_hash_table_lookup(canvas_types, &cl_type);
	cc->provides = ASYNC_CREATION;

	return cl_type;
}


CanvasType
arr_cl_get_type()
{
	return cl_type;
}


GtkWidget*
arr_cl_canvas_new(Arrange* arrange)
{
	PF;

	arrange->canvas                       = g_new0(struct _canvas, 1);
	arrange->canvas->type                 = arr_cl_get_type();
	arrange->canvas->redraw               = arr_cl_canvas_redraw;
	arrange->canvas->free                 = arr_cl_canvas_free;
	arrange->canvas->get_scroll_offsets   = arr_cl_get_scroll_offsets;
	arrange->canvas->px_to_model          = arr_cl_px_to_model;
	arrange->canvas->get_viewport_left    = arr_cl_get_viewport_left;
	arrange->canvas->get_view_fraction    = arr_cl_get_view_fraction;
	arrange->canvas->scroll_to            = arr_cl_scroll_to;
	arrange->canvas->pick                 = arr_cl_pick3;
	arrange->canvas->on_view_change       = arr_cl_on_view_change;
	arrange->canvas->on_songlength_change = arr_cl_on_songlength_change;
	arrange->canvas->on_peak_view_toggle  = arr_cl_on_peak_view_toggle;
	arrange->canvas->on_locator_change    = arr_cl_redraw_locators;
	arrange->canvas->part_new             = arr_cl_part_new;
	arrange->canvas->part_delete          = arr_cl_part_delete;
	arrange->canvas->on_part_change       = arr_cl_on_part_change;
	arrange->canvas->do_follow            = arr_cl_do_follow;
	arrange->canvas->scroll_to_pos        = arr_cl_scroll_to_pos;
	arrange->canvas->scroll_left          = arr_cl_scroll_left;
	arrange->canvas->scroll_right         = arr_cl_scroll_right;
	arrange->canvas->set_part_selection   = arr_cl_set_part_selection;
	arrange->canvas->set_spp              = arr_cl_set_spp;
	arrange->canvas->cl                   = g_new0(struct _cl_canvas, 1);

	ClCanvas* c = arrange->canvas->cl;

	c->animation_zoom.x = arrange->hzoom;
	c->animation_zoom.y = arrange->vzoom;

	if(arrange->canvas_scrollwin){
		pwarn("scrollwin not reset.");
		arrange->canvas_scrollwin = NULL;
	}

	//the adjustments are created before other widgets as they are used to determine canvas viewport.
	c->hscrollbar = gtk_hscrollbar_new(arrange->canvas->h_adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 320.0, 1.0, 20.0, 20.0)));
	c->vscrollbar = gtk_vscrollbar_new(arrange->canvas->v_adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0, 0.0, 240.0, 1.0, 20.0, 20.0)));
	g_signal_connect(gtk_range_get_adjustment(GTK_RANGE(c->hscrollbar)), "value-changed", G_CALLBACK(arr_cl_hscroll_changed), arrange); //merge this with other canvas?
	g_signal_connect(gtk_range_get_adjustment(GTK_RANGE(c->vscrollbar)), "value-changed", G_CALLBACK(arr_cl_vscroll_changed), arrange);

	if(GTK_WIDGET_REALIZED(((AyyiPanel*)&arrange->panel)->dnd_ebox))
		return arr_cl_canvas_create_widgets(arrange);
	else{
		void on_realize(GtkWidget* widget, Arrange* arrange)
		{
			arr_cl_canvas_create_widgets(arrange);
		}
		g_signal_connect(G_OBJECT(((AyyiPanel*)&arrange->panel)->dnd_ebox), "realize", G_CALLBACK(on_realize), arrange);
		return NULL;
	}
}


static GtkWidget*
arr_cl_canvas_create_widgets(Arrange* arrange)
{
	PF;
	ClCanvas* c = arrange->canvas->cl;

	if(arrange->canvas->widget) return NULL;

	GtkWidget* clutter = arrange->canvas->widget = gtk_clutter_embed_new();

	ClutterActor* stage = c->stage = gtk_clutter_embed_get_stage(GTK_CLUTTER_EMBED(clutter));
	clutter_stage_set_user_resizable(CLUTTER_STAGE(stage), FALSE);
	clutter_actor_set_name(CLUTTER_ACTOR(stage), "stage");

	arr_cl_set_stage_colour(arrange);

	clutter_actor_set_size(stage, 320, 240);

	//create a group holding everything else to use as a scrollwindow.
	c->viewport = clutter_group_new();
	ClutterGroup* viewport = CLUTTER_GROUP(c->viewport);
	clutter_actor_set_clip(CLUTTER_ACTOR(viewport), 0, 0, 320, 240);
#ifdef USING_CLUTTER_INTERNAL_RULERBAR
	clutter_actor_set_position(CLUTTER_ACTOR(viewport), 0, arrange->ruler_height);
#else
	clutter_actor_set_position(CLUTTER_ACTOR(viewport), 0, 0);
#endif
	clutter_actor_set_name(CLUTTER_ACTOR(viewport), "viewport");
	clutter_group_add(CLUTTER_GROUP(c->stage), CLUTTER_ACTOR(viewport));
	//clutter_actor_show(CLUTTER_ACTOR(viewport));


#ifdef ENABLE_ANIMATION
	//create and start a timeline to manage animation:
	c->timeline = clutter_timeline_new (6000);  // duration in milliseconds
	g_object_set(c->timeline, "loop", TRUE, NULL);
#endif

#ifdef ENABLE_ANIMATION
	c->stage_zoom_timeline = clutter_timeline_new (250);
	clutter_timeline_stop(c->stage_zoom_timeline); //needed?
	c->stage_scroll_timeline = clutter_timeline_new (250);
	clutter_timeline_stop(c->stage_scroll_timeline); //needed?
#endif

	g_signal_connect(clutter, "size_allocate", G_CALLBACK(arr_cl_on_allocate), arrange);
	g_signal_connect(clutter, "realize",       G_CALLBACK(arr_cl_on_realise), arrange);

	//---------------------------------------------------------

	/*
	ClutterGroup* test_group = CLUTTER_GROUP(clutter_group_new());
	ClutterColor c1 = { 255, 0, 255, 255 };
	ClutterColor c2 = { 0, 0xff, 0, 255 };
	ClutterActor* rect;

	rect = clutter_rectangle_new_with_color(&c2);
	clutter_actor_set_position(rect, 0, 0);
	clutter_actor_set_size(rect, 100, 100);
	clutter_group_add(CLUTTER_GROUP(test_group), CLUTTER_ACTOR(rect));

	*/

	//----------------------------------------------------------------

#if 0
#if 1
	if(!arrange->canvas_scrollwin) arrange->canvas_scrollwin = gtk_scrolled_window_new(NULL, NULL);
#else
	gtk_widget_show(arrange->scrollwin);
	//gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
#endif
#endif

	g_signal_connect_after(G_OBJECT(c->hscrollbar), "button-press-event", G_CALLBACK(stop_propogation), NULL);
	g_signal_connect_after(G_OBJECT(c->hscrollbar), "button-release-event", G_CALLBACK(stop_propogation), NULL);

	GtkWidget* table = c->table = gtk_table_new(2, 2, NON_HOMOGENOUS);
	gtk_table_attach(GTK_TABLE(table), c->hscrollbar, 0, 1, 1, 2, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	gtk_table_attach(GTK_TABLE(table), c->vscrollbar, 1, 2, 0, 1, 0, GTK_FILL | GTK_EXPAND, 0, 0);
	gtk_widget_show_all(table);

#if 1
	gtk_table_attach(GTK_TABLE(table), clutter, 0, 1, 0, 1, GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);
#else
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollwin), clutter);
	gtk_box_pack_start(GTK_BOX(arrange->vbox_rhs), scrollwin, TRUE, TRUE, 0);
#endif

	//-------------------------------------------------------

	//event box to prevent propagation to the dock object:
	c->event_box = gtk_event_box_new();
	gtk_widget_show(c->event_box);
	gtk_container_add(GTK_CONTAINER(c->event_box), table);
	gtk_box_pack_start(GTK_BOX(arrange->priv->vbox_rhs), c->event_box, EXPAND_TRUE, FILL_TRUE, 0);
	g_signal_connect(c->event_box, "button-press-event",   G_CALLBACK(stop_propogation), arrange);
	g_signal_connect(c->event_box, "button-release-event", G_CALLBACK(stop_propogation), arrange);

	#ifdef NEVER
	static gboolean
	show_all_delayed(GtkWidget* widget)
	{
		PF;
		gtk_widget_show_all(widget);
		return TIMER_STOP;
	}

	//just testing...
	//g_timeout_add(200, (gpointer)show_all_delayed, window->shell->widget);
	#endif

	//according to 0.8.1 example, this must be done *after* gtk_widget_show_all()
	//clutter_actor_show_all(CLUTTER_ACTOR(viewport));
	//delaying the showing of actors makes no discernable difference
	gboolean idle_show_all(gpointer data)
	{
		PF;
		Arrange* arrange = (Arrange*)data;
		ClCanvas* c = arrange->canvas->cl;
		clutter_actor_show_all(c->viewport);

		if(SONG_LOADED){
			arr_cl_draw_curve(arrange);
			part_foreach {
				arrange->canvas->part_new(arrange, gpart);
			} end_part_foreach;
		}
		return G_SOURCE_REMOVE;
	}
	g_idle_add(idle_show_all, arrange);

	g_signal_connect(stage, "button-press-event",   G_CALLBACK(arr_cl_on_button_press_event), arrange);
	g_signal_connect(stage, "motion-event",         G_CALLBACK(arr_cl_on_motion_event), arrange);
	g_signal_connect(stage, "button-release-event", G_CALLBACK(arr_cl_on_button_release_event), arrange);
	//if we enable this, the other handlers dont get called
	//g_signal_connect(stage, "event", G_CALLBACK(arr_cl_on_event), (gpointer)arrange);
	g_signal_connect(stage, "key-press-event",      G_CALLBACK(arr_cl_on_key_event), (gpointer)arrange);

//#if 0
	gtk_widget_show(arrange->canvas->widget);
//#endif
	return arrange->canvas->widget;
}


static void
arr_cl_canvas_free(Arrange* arrange)
{
	PF;
	ClCanvas* c = arrange->canvas->cl;
	//if(!c) return;

	if(g_signal_handlers_disconnect_matched (c->stage, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, arr_cl_on_motion_event, arrange) != 1) pwarn("handler disconnection");

	if(c->timeline) g_object_unref(c->timeline);
	cl_canvas_op__free(arrange);
	//gtk_widget_destroy(arrange->canvas->widget);
	gtk_widget_destroy(arrange->canvas->cl->table);
	if(arrange->canvas_scrollwin) gtk_widget_destroy(arrange->canvas_scrollwin);
	arrange->canvas_scrollwin = NULL;

	//destroying this should be enough.. children should be automatically destroyed.
	gtk_widget_destroy(c->event_box);

	g_list_free(arrange->canvas->cl->parts);
	g_free(arrange->canvas->cl);
	g_free(arrange->canvas);
	arrange->canvas = NULL;
}


void
arr_cl_canvas_show(Arrange* arrange)
{
	if(!arrange->canvas->widget){ pwarn("!!"); return; }

	ClCanvas* c = arrange->canvas->cl;
	clutter_actor_show_all (CLUTTER_ACTOR (c->stage));
}


static void
arr_cl_scrollbars_update(Arrange* arrange)
{
	double fx, fy;
	arr_cl_get_view_fraction(arrange, &fx, &fy);
	float visible_x_px = arrange->canvas->widget->allocation.width;
	float total_x_px = arr_beats2px(arrange, song->end);

	float visible_y_px = arrange->canvas->widget->allocation.height;
	float total_y_px = arrange->track_pos[am_track_list_count(song->tracks)] + arrange->ruler_height;

	GtkAdjustment* h_adj = arrange->canvas->h_adj;
	h_adj->lower = 0;
	h_adj->upper = total_x_px;
	h_adj->page_size = visible_x_px;
	gtk_adjustment_set_page_increment(h_adj, visible_x_px * 0.8);
	gtk_adjustment_changed(h_adj);

	GtkAdjustment* v_adj = arrange->canvas->v_adj;
	v_adj->lower = 0;
	v_adj->upper = total_y_px;
	v_adj->page_size = visible_y_px;
	gtk_adjustment_set_page_increment(v_adj, visible_y_px * 0.8);
	gtk_adjustment_changed(v_adj);

	// ...and update the track control pane:
  	GtkAdjustment* adj = gtk_viewport_get_vadjustment(GTK_VIEWPORT(arrange->ctl_viewport));
	adj->lower     = v_adj->lower;
	adj->upper     = v_adj->upper;
	adj->page_size = v_adj->page_size;

	dbg(2, "pagesizex=%.2f totalx=%.2f totaly=%.2f", visible_x_px, total_x_px, total_y_px);

	//dont hide the scrollbar - issues with clutter when reshowing...
#if 0
	if(fx < 1.0){ dbg(0, "showing..."); gtk_widget_show(arrange->canvas->cl->hscrollbar);}
	else         gtk_widget_hide(arrange->canvas->cl->hscrollbar);
#endif
}


static void
arr_cl_canvas_redraw_fast(Arrange* arrange)
{
	arr_cl_canvas_redraw(arrange);
}


static void
arr_cl_canvas_redraw(Arrange* arrange)
{
	if(!arrange->canvas->widget){ pwarn("not yet"); return; }

PF;
	GList* l = arrange->canvas->cl->parts;
	for(;l;l=l->next){
		ClutterPart* clutter_part = l->data;
		clutter_part_redraw(clutter_part, CHANGE_ALL);
	}

	arr_cl_scrollbars_update(arrange);

	//dbg(0, "height=%i parent=%p", arrange->canvas->widget->allocation.height, gtk_widget_get_parent(arrange->canvas->widget));
	//if(arrange->canvas->widget->allocation.height < 10){
		//just testing...
		//gtk_widget_set_size_request(arrange->canvas->widget, 320, 240);
	//}
}


static void
arr_cl_on_allocate(GtkWidget* win, GtkAllocation* allocation, Arrange* arrange)
{
	PF;
	static guint update_id = 0; //temp! wont work with multiple panels
	GtkWidget* clutter = arrange->canvas->widget;
	int x = 0, y = 0;
	clutter_actor_set_clip(arrange->canvas->cl->viewport, x, y, clutter->allocation.width, clutter->allocation.height);
	ClutterActor* stage = gtk_clutter_embed_get_stage(GTK_CLUTTER_EMBED(clutter));

	dbg(1, "%i x %i clip=%s", allocation->width, allocation->height, clutter_actor_has_clip(stage) ? "true" : "false");

	gboolean view_change(gpointer data)
	{
		PF;
		update_id = 0;
		Arrange* arrange = (Arrange*)data;
		arr_cl_on_view_change(arrange, CHANGE_WIDTH | CHANGE_HEIGHT);
		return G_SOURCE_REMOVE;
	}
	if(!update_id){
		update_id = g_idle_add(view_change, arrange);
	}
}


static void
arr_cl_on_realise(GtkWidget* widget, Arrange* arrange)
{
	windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE);
	get_style_font();
	arr_cl_canvas_redraw(arrange);
}


static gint
arr_cl_canvas_event(GtkWidget* widget, GdkEvent* event, gpointer _arrange)
{
	PF;
	Arrange* arrange = _arrange; g_return_val_if_fail(arrange, NOT_HANDLED);
#if 0

	double mouse.x        = event->button.x;
	double mouse.y        = event->button.y - RULERBAR_HEIGHT_PX;
	static int op         = OP_NONE;
	static int op_ready   = OP_NONE;
	static int old_cursor = 0;
    int            cursor = 0;
	gboolean shift        = event->button.state & GDK_SHIFT_MASK;
	gboolean control      = event->button.state & GDK_CONTROL_MASK;
#endif

#if 0
	int track;
	if(op->type == OP_NONE) arr_gl_canvas_pick(arrange, op->mouse.x, op->mouse.y, &track, &op->part);
#endif

	switch (event->type){
		case GDK_ENTER_NOTIFY:
			break;
		case GDK_BUTTON_PRESS:
			dbg (0, "GDK_BUTTON_PRESS");
			switch(event->button.button){
				case 1:
					break;
				default:
					break;
			}
		default:
			break;
	}

	return HANDLED;
}


static void
arr_cl_hscroll_changed(GtkWidget* widget, Arrange* arrange)
{
	static double prev = 0;

	g_return_if_fail(arrange);
	double val = gtk_adjustment_get_value(GTK_ADJUSTMENT(widget));
	dbg (2, "val=%.2f pagesize=%.2f", val, gtk_adjustment_get_page_size((GtkAdjustment*)widget));
	arrange->canvas->cl->visible.x = val;

	if(prev != val){
		arr_cl_scroll_to(arrange, val, -1);
		arr_on_view_changed(arrange, CHANGE_POS_X);
		prev = val;
	}
}


static void
arr_cl_vscroll_changed(GtkWidget* widget, Arrange* arrange)
{
	static double prev = 0;

	g_return_if_fail(arrange);
	double val = gtk_adjustment_get_value(GTK_ADJUSTMENT(widget));
	arrange->canvas->cl->visible.y = val;

	if(prev != val){
  		GtkAdjustment* adj = gtk_viewport_get_vadjustment(GTK_VIEWPORT(arrange->ctl_viewport));
		adj->value = GTK_ADJUSTMENT(widget)->value;
		gtk_adjustment_value_changed(adj);
		dbg(3, "upper=%.2f", adj->upper);

		arr_cl_scroll_to(arrange, -1, val);
		prev = val;
	}
}


double
arr_cl_samples2px(Arrange* arrange, uint32_t samples)
{
	return arr_samples2px(arrange, samples);
}


static void
arr_cl_get_scroll_offsets(Arrange* arrange, int* cx, int* cy)
{
	g_return_if_fail(arrange->canvas->h_adj);

	*cx = arrange->canvas->h_adj->value;//arrange->canvas->gl->visible.x;
	*cy = arrange->canvas->v_adj->value;
}


static void
arr_cl_px_to_model(Arrange* arrange, Pti px, AyyiSongPos* pos, ArrTrk** track)
{
	dbg(0, "TODO");
}


static double
arr_cl_get_scroll_left(Arrange* arrange)
{
	//returns zoomed value in pixels?

	//FIXME clutter takes ints
	return arrange->canvas->h_adj->value;
}


static double
arr_cl_get_scroll_top(Arrange* arrange)
{
	return arrange->canvas->v_adj->value;
}


static void
arr_cl_get_view_fraction(Arrange* arrange, double* fx, double* fy)
{
	if(!arrange->canvas->widget){ pwarn("not yet"); return; }

	float tot_x_beats = song->end + 4;
	float visible_x_px = arrange->canvas->widget->allocation.width;
	float visible_x_beats = visible_x_px / ( PX_PER_BEAT * arrange->hzoom);

	float visible_y_px = arrange->canvas->widget->allocation.height;
	float total_y_px = arrange->track_pos[am_track_list_count(song->tracks)] + arrange->ruler_height;

	*fx = visible_x_beats / tot_x_beats;
	*fy = visible_y_px / total_y_px;

	dbg(2, "cluterreq=%i fraction=%.2f", arrange->canvas->widget->requisition.width, *fx);
}


static uint32_t
arr_cl_get_viewport_left(Arrange* arrange)
{
	if(!arrange->canvas->widget){ if(_debug_ > 1) pwarn("not yet"); return 0; }

	GtkAdjustment* h_adj = arrange->canvas->h_adj;
	return arr_px2samples(arrange, gtk_adjustment_get_value(h_adj));
}


static void
arr_cl_scroll_to(Arrange* arrange, int x, int y)
{
	//scroll the arrange canvas to the given absolute canvas position.
	//-use -1 to keep existing position.

#ifdef ENABLE_ANIMATION
	ClCanvas* c = arrange->canvas->cl;
#endif

	dbg(2, "x=%i y=%i", x, y);
	if(x < 0) x = arr_cl_get_scroll_left(arrange);
	if(y < 0) y = arr_cl_get_scroll_top (arrange);

#ifdef ENABLE_ANIMATION
	if((double)x > arrange->canvas->h_adj->upper) pwarn ("val too high. Upper=%.2f", arrange->canvas->h_adj->upper);

	typedef struct _scroll_closure ScrollClosure;
	struct _scroll_closure
	{
		Arrange* arrange;
		Ptf      start_scroll;
		Ptd      end_scroll;
		void     (*on_completed)(ClutterTimeline*, gpointer);
		void     (*on_frame)(ClutterTimeline*, gint, gpointer);
		void     (*on_paused)(ClutterTimeline*, gpointer);
		void     (*on_stop)(ScrollClosure*);
	};

	void stage_scroll_on_frame(ClutterTimeline* timeline, gint msecs, gpointer user_data)
	{
		ScrollClosure* closure = user_data;
		Arrange* arrange = closure->arrange;
		ClCanvas* c = arrange->canvas->cl;

		gdouble progress = clutter_timeline_get_progress (timeline);

		double x = closure->start_scroll.x * (1 - progress) + closure->end_scroll.x * progress;
		double y = closure->start_scroll.y * (1 - progress) + closure->end_scroll.y * progress;
		#ifdef USING_CLUTTER_INTERNAL_RULERBAR
		clutter_actor_set_position(arrange->canvas->cl->viewport, -x, -y + arrange->ruler_height);
		#else
		clutter_actor_set_position(arrange->canvas->cl->viewport, -x, -y + 0);
		#endif
		clutter_actor_set_clip(arrange->canvas->cl->viewport, x, y/* + arrange->ruler_height*/, arrange->canvas->widget->allocation.width, arrange->canvas->widget->allocation.height);

		c->animation_scroll.x = x;
		c->animation_scroll.y = y;
	}

	void stage_scroll_on_completed(ClutterTimeline* timeline, gpointer _closure)
	{
		PF;
		ScrollClosure* closure = _closure;

		closure->on_stop(closure);
	}

	void stage_scroll_on_paused(ClutterTimeline* timeline, gpointer user_data)
	{
		PF;
		ScrollClosure* closure = user_data;
		closure->on_stop(closure);
	}

	void scroll_timer_on_stop(ScrollClosure* closure)
	{
		PF;
		ClCanvas* c = closure->arrange->canvas->cl;

		if(g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_scroll_timeline), closure->on_frame, closure) != 1) pwarn("not disconnected!");
		if(g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_scroll_timeline), closure->on_completed, closure) != 1) pwarn("2");
		g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_scroll_timeline), closure->on_paused, closure);
		g_free(closure);
	}

	if(c->stage_scroll_timeline && clutter_timeline_is_playing(c->stage_scroll_timeline)){
		dbg(2, "stopping timer...");
		clutter_timeline_pause(c->stage_scroll_timeline);
	}
	ScrollClosure* closure = g_new0(struct _scroll_closure, 1);
	closure->arrange = arrange;
	closure->on_frame = stage_scroll_on_frame;
	closure->on_completed = stage_scroll_on_completed;
	closure->on_paused = stage_scroll_on_paused;
	closure->on_stop = scroll_timer_on_stop;
	closure->end_scroll.x = x;
	closure->end_scroll.y = y;

	//int cx, cy;
	//arr_cl_get_scroll_offsets(arrange, &cx, &cy);
	closure->start_scroll.x = c->animation_scroll.x;
	closure->start_scroll.y = c->animation_scroll.y;
	//clutter_actor_get_position(arrange->canvas->cl->viewport, &closure->start_scroll.x, &closure->start_scroll.y);
	dbg(2, " starting timer... x=%.2f", closure->start_scroll.x);

	g_signal_connect(c->stage_scroll_timeline, "new-frame", (gpointer)stage_scroll_on_frame, closure);
	g_signal_connect(c->stage_scroll_timeline, "completed", (gpointer)stage_scroll_on_completed, closure);
	g_signal_connect(c->stage_scroll_timeline, "paused", (gpointer)stage_scroll_on_paused, closure);
	clutter_timeline_start(c->stage_scroll_timeline);
#endif

	if((int)arrange->canvas->h_adj->value != x){
		gtk_adjustment_set_value(GTK_ADJUSTMENT(arrange->canvas->h_adj), (double)x);
	}
}


static void
arr_cl_on_view_change(Arrange* arrange, AMChangeType change_type)
{
	if(!arrange->canvas->widget) return;
PF;

	ClCanvas* c = arrange->canvas->cl;

	gboolean need_scrollbar_update = false;

	if(change_type & CHANGE_WIDTH || change_type & CHANGE_HEIGHT){
		need_scrollbar_update = true;
	}

#ifdef ENABLE_ANIMATION
	//TODO move this to a zoom_to function?
	typedef struct _closure Closure;
	struct _closure
	{
		Arrange* arrange;
		Ptd      start_zoom;
		void     (*on_completed)(ClutterTimeline*, gpointer);
		void     (*on_frame)(ClutterTimeline*, gint, gpointer);
		void     (*on_paused)(ClutterTimeline*, gpointer);
		void     (*on_stop)(Closure*);
	};

	void stage_resize_on_frame(ClutterTimeline* timeline, gint msecs, gpointer user_data)
	{
		struct _closure* closure = user_data;
		g_return_if_fail(closure->arrange->canvas);
		ClCanvas* c = closure->arrange->canvas->cl;

		gdouble progress = clutter_timeline_get_progress (timeline);
		c->animation_zoom.x = closure->start_zoom.x * (1 - progress) + closure->arrange->hzoom * progress;
		c->animation_zoom.y = closure->start_zoom.y * (1 - progress) + closure->arrange->vzoom * progress;
		arr_cl_canvas_redraw(closure->arrange);
	}

	void stage_resize_on_completed(ClutterTimeline* timeline, gpointer _closure)
	{
		Closure* closure = _closure;

		closure->on_stop(closure);
	}

	void stage_resize_on_paused(ClutterTimeline* timeline, gpointer user_data)
	{
		dbg(0, "NEVER GET HERE? yes we do get here");
		struct _closure* closure = user_data;
		closure->on_stop(closure);
	}

	void zoom_timer_on_stop(struct _closure* closure)
	{
		g_return_if_fail(closure->arrange->canvas);
		ClCanvas* c = closure->arrange->canvas->cl;

		if(g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_zoom_timeline), closure->on_frame, closure) != 1) pwarn("not disconnected!");
		if(g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_zoom_timeline), closure->on_completed, closure) != 1) pwarn("2");
		g_signal_handlers_disconnect_by_func(G_OBJECT(c->stage_zoom_timeline), closure->on_paused, closure);
		g_free(closure);
	}
#endif

	if(change_type & AM_CHANGE_ZOOM_H || change_type & AM_CHANGE_ZOOM_V){
#ifdef ENABLE_ANIMATION
		if(c->stage_zoom_timeline && clutter_timeline_is_playing(c->stage_zoom_timeline)){
			dbg(2, "CHANGE_ZOOM stopping timer...");
			clutter_timeline_pause(c->stage_zoom_timeline);
			//clutter_timeline_advance (c->stage_zoom_timeline, 10000);
		}
		dbg(2, "CHANGE_ZOOM starting timer...");
		struct _closure* closure = g_new0(struct _closure, 1);
		closure->arrange = arrange;
		closure->on_frame = stage_resize_on_frame;
		closure->on_completed = stage_resize_on_completed;
		closure->on_paused = stage_resize_on_paused;
		closure->on_stop = zoom_timer_on_stop;
		closure->start_zoom.x = c->animation_zoom.x;
		closure->start_zoom.y = c->animation_zoom.y;
		g_signal_connect(c->stage_zoom_timeline, "new-frame", (gpointer)stage_resize_on_frame, closure);
		g_signal_connect(c->stage_zoom_timeline, "completed", (gpointer)stage_resize_on_completed, closure);
		g_signal_connect(c->stage_zoom_timeline, "paused", (gpointer)stage_resize_on_paused, closure);
		clutter_timeline_start(c->stage_zoom_timeline);
#else
		c->animation_zoom.x = arrange->hzoom;
		c->animation_zoom.y = arrange->vzoom;
		arr_cl_canvas_redraw(arrange);
#endif
	}

	if(change_type & AM_CHANGE_BACKGROUND_COLOUR){
		arr_cl_set_stage_colour(arrange);
	}

	if(need_scrollbar_update){
		arr_cl_scrollbars_update(arrange);
	}
}


static void
arr_cl_on_songlength_change(Arrange* arrange)
{
	if(!arrange->canvas->h_adj){ pwarn("no canvas h_adj!"); return; }
	arrange->canvas->h_adj->upper = arrange->canvas_width;
}


static void
arr_cl_on_peak_view_toggle(Arrange* arrange)
{
	ViewOption* option = &arrange->view_options[SHOW_PART_CONTENTS];

	GList* l = arrange->canvas->cl->parts;
	for(;l;l=l->next){
		ClutterPart* clutter_part = l->data;
		ClutterActor* waveform = clutter_part->waveform;
		if(option->value) clutter_actor_show(waveform);
		else clutter_actor_hide(waveform);
	}
}


static void
arr_cl_redraw_locators(Arrange* arrange)
{
}


static gboolean
arr_cl_part_new(Arrange* arrange, AMPart* part)
{
	//clutter already tiles the texture, so doing it again here is unneccesary.
	PF;

	ClCanvas* cl = arrange->canvas->cl;

	if(!part->pool_item){ dbg(0, "ignoring midi part..."); return FALSE; }

	ClutterActor* group = clutter_part_new(arrange, part);

	cl->parts = g_list_append(cl->parts, group);

	clutter_part_redraw((ClutterPart*)group, CHANGE_ALL); //testing

	g_object_set_data(G_OBJECT(group), "part", part);

	g_signal_connect(CLUTTER_ACTOR(group), "button-press-event", G_CALLBACK(arr_cl_part_on_button_press_event), part);

/*
	void arr_cl_part_trans_alpha_func(ClutterBehaviour* behave, guint alpha_value, ClutterActor* part_group)
	{
		PF;
	}

	ClutterAlpha* alpha;
	ClutterBehaviour* trans_behave = fluttr_behave_new (alpha, arr_cl_part_trans_alpha_func, (gpointer)group);	
*/
	// Add the group to the stage
	clutter_group_add(CLUTTER_GROUP(cl->viewport), CLUTTER_ACTOR(group));
#ifdef PUT_ME_BACK_MAYBE
	clutter_actor_show_all(CLUTTER_ACTOR(group));
#endif

	return TRUE;
}


static void
arr_cl_part_delete(Arrange* arrange, AMPart* part)
{
	dbg(0, "...");
	ClCanvas* c = arrange->canvas->cl;

	ClutterPart* cpart = arr_cl_part_lookup(arrange, part);
	if(!cpart){ pwarn("!! part not found"); return; }

	clutter_container_remove_actor(CLUTTER_CONTAINER(c->viewport), CLUTTER_ACTOR(cpart));
	//TODO check actor dispose is called.
	//g_object_unref(cpart);

	c->parts = g_list_remove(c->parts, cpart);
	c->selected_parts = g_list_remove(c->selected_parts, cpart);
}


static ClutterPart*
arr_cl_part_lookup(Arrange* arrange, AMPart* part)
{
	ClCanvas* c = arrange->canvas->cl;

	GList* l = c->parts;
	for(;l;l=l->next){
		ClutterPart* cpart = l->data;
		if(cpart->part == part) return cpart;
	}
	return NULL;
}


static void
arr_cl_do_follow(Arrange* arrange)
{
}


static void
arr_cl_on_part_change(Arrange* arrange, AMPart* gpart)
{
	arrange->canvas->redraw(arrange);
}


static void
arr_cl_scroll_to_pos(Arrange* arrange, GPos* pos, int tnum)
{
}


static void
arr_cl_scroll_left(Arrange* arrange, int pix)
{
	int x = arrange->canvas->cl->visible.x;
	dbg(2, "%i --> %i", x, x - pix);
	arr_cl_scroll_to(arrange, x - pix, -1);
}


static void
arr_cl_scroll_right(Arrange* arrange, int pix)
{
	arr_cl_scroll_to(arrange, arrange->canvas->cl->visible.x + pix, -1);
}


static void
arr_cl_set_part_selection(Arrange* arrange)
{
	ClCanvas* c = arrange->canvas->cl;

	int selection_start = 10000000, selection_end = 0;

	// clear the old selection
	dbg (3, "clearing old selection... %i", g_list_length(c->selected_parts));
	GList* l = c->selected_parts;
	for(;l;l=l->next){
		ClutterPart* item = l->data;
		clutter_part_select (item, false);
	}
	dbg (3, "old selection cleared.");

	g_list_clear(c->selected_parts);

	l = arrange->part_selection;
	for(;l;l=l->next){
		AMPart* gpart = l->data;
		ClutterPart* item = arr_cl_part_lookup(arrange, gpart);
		if(item){
			if(!CLUTTER_IS_PART(item)){ gerr ("!CLUTTER_IS_PART %s", gpart->name); continue; }
			clutter_part_select (item, true);
			c->selected_parts = g_list_append(c->selected_parts, item);
		}
else dbg(0, "not found");

		selection_start = MIN(selection_start, arr_pos2px(arrange, &gpart->start));
		selection_end   = MAX(selection_end, arr_part_end_px(arrange, gpart));
	}
}


static void
arr_cl_draw_curve(Arrange* arrange)
{
#ifdef USE_CLUTTER_CAIRO
	ClCanvas* c = arrange->canvas->cl;
	dbg(0, "drawing curve...");

	// testing automation drawing
	ClutterActor* ctex = clutter_cairo_new(32, 32);
	cairo_t* cr = clutter_cairo_create(CLUTTER_CAIRO(ctex));

	// clear
	cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint(cr);
	cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

	double curve_colour[3] = {1.0, 0.0, 0.0};
	cairo_set_source_rgba (cr, curve_colour[0], curve_colour[1], curve_colour[2], 0.5);
	{
		//cairo_save (cr);

		cairo_new_path (cr);
		cairo_move_to (cr, -100, -100);
		#if 0
		cairo_rel_curve_to (cr,
				petal_size, petal_size,
				(pm2+2)*petal_size, petal_size,
				(2*petal_size) + pm1, 0);
		#endif
		cairo_line_to(cr, 100, 100);
		cairo_line_to(cr, -100, 200);
		cairo_close_path (cr);
		cairo_fill (cr);

		cairo_stroke (cr);

		//cairo_restore (cr);
	}
	//clutter_group_add (CLUTTER_GROUP(viewport), ctex);
	clutter_group_add (CLUTTER_GROUP(c->stage), ctex);
	clutter_actor_set_position (ctex, 0, 0);
#endif
}
#ifdef ENABLE_ANIMATION
static guint32
arr_cl_fade_in(ClutterAlpha* alpha, gpointer dummy)
{
	#define fade_speed 32

	ClutterTimeline* timeline = clutter_alpha_get_timeline (alpha);

	//gint ellapsed_milliseconds = clutter_timeline_get_elapsed_time(timeline);
	//gint total_milliseconds = clutter_timeline_get_n_frames (timeline);
	gint progress = clutter_timeline_get_progress (timeline);

	return MIN(1.0, progress * fade_speed);
}
#endif


int
arr_cl_pos2px(Arrange* arrange, GPos* pos)
{
	//return should be int or float?

	ClCanvas* c = arrange->canvas->cl;

	return arr_pos2px(arrange, pos) * c->animation_zoom.x / arrange->hzoom;
}


int
arr_cl_cpos2px(Arrange* arrange, AyyiSongPos* pos)
{
	//return should be int or float?

	ClCanvas* c = arrange->canvas->cl;

	return arr_spos2px(arrange, pos) * c->animation_zoom.x / arrange->hzoom;
}


static void
op_set_part_boundary(CanvasOp* op)
{
	Arrange* arrange = op->arrange;

	op->p1.x = arr_pos2px(arrange, &op->part->start);
	op->p2.x = arr_spos2px(arrange, &op->part->length) + op->p1.x;
	TrackDispNum d = arr_px2trk(arrange, op->bnew.y);
	if(d > -1){
		op->p1.y = arrange->track_pos[d];
		op->p2.y = arrange->track_pos[d + 1];
	}
}


static void
arr_cl_on_button_press(Arrange* arrange, ClutterButtonEvent* event)
{
	PF;
	if(event->type != CLUTTER_BUTTON_PRESS) pwarn("wrong event type!");

	CanvasOp* op = arrange->canvas->op = cl_canvas_op__new(arrange, event, OP_NONE);
	arr_cl_pick(arrange, (ClutterButtonEvent*)event);

	if(op->part){
		dbg(0, "yes, part is selected.");
		if(!arr_part_is_selected(arrange, op->part)){
			arr_part_selection_reset(arrange, true);
			arr_part_selection_add(arrange, op->part);
		}
		else dbg(0, "part was already selected. doing nothing.");

		op_set_part_boundary(op);
	}else{
		dbg(0, "no part selected");
		arr_part_selection_reset(arrange, true);
	}

	if(!op->part) return;

	GdkModifierType mods;
	gdk_window_get_pointer(((AyyiPanel*)arrange)->shell->widget->window, NULL, NULL, &mods);
	bool control = mods & GDK_CONTROL_MASK;

	if(control){
		if((op = cl_canvas_op__new(arrange, event, OP_COPY))){

			//op->origin.x    = op->win_x + op->xscrolloffset; //FIXME xscrolloffset not set?
			//op->origin.y    = op->win_y + op->yscrolloffset;

			op->press(op, (GdkEvent*)event);
		}
	}else{
		if((op = cl_canvas_op__new(arrange, event, OP_MOVE))){

			op->motion = cl_canvas_op__move_motion; //TODO remove line. is already done

			op->press(op, (GdkEvent*)event);
		}
	}
}


static gboolean
arr_cl_on_button_press_event(ClutterStage* stage, ClutterButtonEvent* event, Arrange* arrange)
{
	PF;
	windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE);

	CanvasOp* op = arrange->canvas->op;
	if(op) { op->mouse.x = event->x; op->mouse.y = event->y; }

	if (event->type == CLUTTER_BUTTON_PRESS) {
		arr_cl_on_button_press(arrange, (ClutterButtonEvent*)event);
	}
	else dbg(0, "type=%i", event->type);

	return HANDLED;
}


static gboolean
arr_cl_part_on_button_press_event(ClutterActor* actor, ClutterButtonEvent* event, AMPart* part)
{
	return NOT_HANDLED;
}


static gboolean
arr_cl_on_motion_event(ClutterStage* stage, ClutterMotionEvent* event, Arrange* arrange)
{
	g_return_val_if_fail(AYYI_IS_ARRANGE(arrange), NOT_HANDLED);

	CanvasOp* op = arrange->canvas->op;
	if(!op){
		op = arrange->canvas->op = cl_canvas_op__new(arrange, (ClutterButtonEvent*)event, OP_NONE);
	}
	op->mouse.x = event->x;
	op->mouse.y = event->y;
	op->bnew.x  = event->x;
	op->bnew.y  = event->y;
	op->motion(op);
	op->part && (op_set_part_boundary(op), false);

	return HANDLED;
}


static gboolean
arr_cl_on_button_release_event(ClutterStage* stage, ClutterButtonEvent* event, Arrange* arrange)
{
	PF;
	dbg(0, "CLUTTER_BUTTON_RELEASE");
	CanvasOp* op = arrange->canvas->op;
	if(op) op->finish(op);
	return HANDLED;
}


static void
arr_cl_on_key_event(ClutterStage* stage, ClutterEvent* event, Arrange* arrange)
{
	// If we use key-release-event, we get some strange behaviour. Clutter bug?

	// ClutterModifierType is the same as GdkModifierType.

	PF;

	windows__verify_pointer((AyyiPanel*)arrange, AYYI_TYPE_ARRANGE);

	if (event->type == CLUTTER_KEY_PRESS) {
		ClutterKeyEvent* kev = (ClutterKeyEvent *) event;

		switch (clutter_event_get_key_symbol(event)) {

			case CLUTTER_Escape:
				break;
			default:
				dbg(2, "activating accel group...");
				gtk_accel_groups_activate(G_OBJECT(gtk_widget_get_toplevel(arrange->canvas->widget)), clutter_event_get_key_symbol(event), kev->modifier_state);
				break;
		}
	}
	else dbg(0, "type=%i", event->type);
}


static void
arr_cl_part_bounding(ClutterPart* part, Pti* part_origin)
{
	// Move the part_origin so that its fully inside the canvas

	int part_height = clutter_actor_get_height(CLUTTER_ACTOR(part));
	int max_height = part->arrange->track_pos[am_track_list_count(song->tracks)];

	part_origin->x = MAX(part_origin->x, 0);
	part_origin->y = CLAMP(part_origin->y, 0, max_height - part_height);
}


static void
arr_cl_set_stage_colour(Arrange* arrange)
{
	ClCanvas* c = arrange->canvas->cl;

	/*
	ClutterColor stage_color;
	gtk_clutter_get_bg_color(((AyyiPanel*)arrange)->shell->widget, GTK_STATE_NORMAL, &stage_color); // removed from clutter-gtk
	clutter_stage_set_color(CLUTTER_STAGE(c->stage), &stage_color);
	*/

	//uint32_t rgba = arrange->bg_colour;
	ClutterColor rgba = {(arrange->bg_colour >> 24) % 0x100, (arrange->bg_colour >> 16) % 0x100, (arrange->bg_colour >> 8) % 0x100, 0xff};
	dbg(2, "bgcolour=0x%08x %02x", arrange->bg_colour, rgba.red); // =0 by default (blue) - should be 3 (base) ?
	clutter_stage_set_color(CLUTTER_STAGE(c->stage), &rgba);
}


ClutterTimeline* op_timeline = NULL;

//note: part coords are *viewport* coords!
#define XLATE_PART_TO_MOUSE(A) A.x += idiff.x; A.y += idiff.y
#define XLATE_MOUSE_TO_PART(A) A.x -= idiff.x; A.y -= idiff.y + viewport.y

#define MOUSE_CURRENT op->old_bounded
#define MOUSE_TARGET_X op->mouse.x
#define MOUSE_TARGET_Y op->mouse.y

#define MAX_SPEED (20 << 4)
Pti speed = {0, 0};

#define damp(speed) \
	speed.x = (speed.x * 5) / 10; \
	speed.y = (speed.y * 5) / 10;

static void
canvas_op__on_frame(ClutterTimeline* timeline, gint frame_num, CanvasOp* op)
{
	static int f = 0; f++;

	if(!op_timeline) return;

	g_return_if_fail(op->arrange->canvas);
	g_return_if_fail(op->arrange->canvas->type == arr_cl_get_type());

	//static Pti actor_prev;
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;

	ClutterActor* actor = CLUTTER_ACTOR(((ClutterCanvasOp*)op)->item);
	if(!actor) return;
	ClutterPart* cpart = CLUTTER_PART(actor);

	int acceleration = 2;

	//pt idiff = { op->diff.x, op->diff.y }; //unfortunately op->diff is float.

	/*
	clutter_actor_get_abs_position() - do not use! Reported position can be off by several pixels! You must get position relative to parent and chain up all the way to the stage.
	*/

	//arr_px2pos(op->arrange, &op->pos, op->mouse.x);

	//calculate using cursor coords:

	Pti part_current;
	gfloat x, y;
	clutter_actor_get_position(CLUTTER_ACTOR(cop->item), &x, &y);
	part_current.x = x;
	part_current.y = y;
	Ptf viewport;
	clutter_actor_get_position(CLUTTER_ACTOR(op->arrange->canvas->cl->viewport), &viewport.x, &viewport.y);
	part_current.y -= viewport.y;

	//XLATE_PART_TO_MOUSE(old_part);
	//dbg(0, "oldpart=%i x %i => %i x %i", test.x, test.y, old_part.x, old_part.y);

#if 0
	pt mouse_part = old_part;
	XLATE_PART_TO_MOUSE(mouse_part);
	pt distance_from_target = {mouse_part.x - MOUSE_TARGET_X, mouse_part.y - MOUSE_TARGET_Y};
	//if distance.x is + then we need to move left
	//if distance.x is - then we need to move right

	pt new_pos = {
		MOUSE_CURRENT.x - MIN(distance_from_target.x, MAX_SPEED),
		MOUSE_CURRENT.y + MIN(distance_from_target.y, MAX_SPEED),
	};

	//translate to part coords
	pt actor_new = new_pos;
	//actor_new.x = new_pos.x - idiff.x;
	//actor_new.y = new_pos.y - idiff.y;
	XLATE_MOUSE_TO_PART(actor_new);
#endif

	//translate mouse position to Part coords.
	/*
	pt mouse_pc = {op->mouse.x, op->mouse.y};
	XLATE_MOUSE_TO_PART(mouse_pc);
	*/

	//pt distance_from_target = {mouse_pc.x - part_current.x, mouse_pc.y - part_current.y};
	Pti distance_from_target = {cpart->target_position.x - part_current.x, cpart->target_position.y - part_current.y};
	speed.x = speed.x + distance_from_target.x * acceleration;
	speed.y = speed.y + distance_from_target.y * acceleration;
	damp(speed);

	speed.x = CLAMP(speed.x, -MAX_SPEED, MAX_SPEED);
	speed.y = CLAMP(speed.y, -MAX_SPEED, MAX_SPEED);

	Pti new_pos = {
		part_current.x + (speed.x >> 4),
		part_current.y + (speed.y >> 4),
	};
	//if we are close, just go straight to the target position:
	if(speed.x < 2 && distance_from_target.x < 3){
		//new_pos.x = mouse_pc.x;
		//new_pos.y = mouse_pc.y;
	}
	Pti actor_new = new_pos;

	arr_cl_part_bounding(CLUTTER_PART(actor), &actor_new);

	if(f < 10) dbg(0, "target=%.1f x %.1f distance=%02i x %02i speed=%i x %i current=%i x %i actor_new=%i x %i",
		cpart->target_position.x, cpart->target_position.y,
		distance_from_target.x, distance_from_target.y,
		speed.x, speed.y,
		part_current.x, part_current.y,
		actor_new.x, actor_new.y
		);
	if(distance_from_target.x || distance_from_target.y){
		clutter_actor_set_position(actor, actor_new.x, actor_new.y);

	/*
		pt test;
		clutter_actor_get_position(CLUTTER_ACTOR(op->item), &test.x, &test.y);
		dbg(0, "       requested=%i x %i got=%i x %i", actor_new.x, actor_new.y, test.x, test.y - viewport.y);
	*/
	}

	//note that old_bounded is the cursor position, not the part position.
	op->old_bounded.x = op->mouse.x;
	op->old_bounded.y = op->mouse.y;
	//actor_prev = actor_new;
}


static void
arr_cl_pick3(Arrange* arrange, double x, double y, int* track, AMPart** gpart)
{
	//implements arrange->canvas->pick
	PF;
}


static void
arr_cl_pick(Arrange* arrange, ClutterButtonEvent* event)
{
	CanvasOp* op = arrange->canvas->op;
	g_return_if_fail(op);

	ClCanvas* c = arrange->canvas->cl;
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	op->part = NULL;
	cop->item = NULL;

	gint x = event->x;
	gint y = event->y;
	ClutterActor* actor = clutter_stage_get_actor_at_pos(CLUTTER_STAGE(c->stage), /*CLUTTER_PICK_REACTIVE*/CLUTTER_PICK_ALL, x, y);
	if(!actor) return;
	if(CLUTTER_IS_STAGE(actor)) return;
	if(!CLUTTER_IS_GROUP(actor)) actor = clutter_actor_get_parent(actor);
	if(!CLUTTER_IS_PART(actor)) return;
	dbg(0, "actor=%s", clutter_actor_get_name(actor));
	op->part = g_object_get_data(G_OBJECT(actor), "part");
	cop->item = actor;
}


static void
arr_cl_pick2(Arrange* arrange, gint x, gint y, ClutterPart** cpart, AMPart** part, ArrTrk** trk, gboolean* at_start, gboolean* at_end)
{
	//does _not_ set values into the CanvasOp.

	//TODO merge this with the other pick fn once we know the best signature...

	ClCanvas* c = arrange->canvas->cl;

	*part = NULL;
	*cpart = NULL;
	*trk = NULL;
	*at_start = false;
	*at_end = false;

	ClutterActor* actor = clutter_stage_get_actor_at_pos(CLUTTER_STAGE(c->stage), /*CLUTTER_PICK_REACTIVE*/CLUTTER_PICK_ALL, x, y);
	if(!actor) return;
	//dbg(0, "actor=%s", clutter_actor_get_name(actor));
	if(CLUTTER_IS_STAGE(actor)) return;
	if(!CLUTTER_IS_GROUP(actor)) actor = clutter_actor_get_parent(actor);
	if(!CLUTTER_IS_PART(actor)) return;
	*part = g_object_get_data(G_OBJECT(actor), "part");
	*cpart = (ClutterPart*)actor;
	*trk = track_list__track_by_index(arrange->priv->tracks, (*part)->track->track_num);

	x += arr_cl_get_scroll_left(arrange);
	int start = arr_pos2px(arrange, &(*part)->start);
	int end = start + arr_spos2px(arrange, &(*part)->length);
	//dbg(0, "  actor=%s x=%i %i", clutter_actor_get_name(actor), x, start);
	if(x >= start && x < start + 4){
		*at_start = true;
	}
	else if(x <= end && x > end - 4){
		*at_start = true;
	}
}


static CanvasOp*
cl_canvas_op__new(Arrange* arrange, ClutterButtonEvent* event, Optype type)
{
	PF;
	CanvasOp* op = arrange->canvas->op;
	if(!op) op = arrange->canvas->op = (CanvasOp*)g_new0(ClutterCanvasOp, 1);
	ClutterCanvasOp* cop = (ClutterCanvasOp*)op;
	op->arrange = arrange;

	dbg(2, "name=%s part=%s", cop->item ? clutter_actor_get_name(cop->item) : NULL, op->part ? op->part->name : NULL);

	if(!op_timeline){
		op_timeline = clutter_timeline_new (6000);  // duration ms
		g_object_set(op_timeline, "loop", TRUE, NULL); // make it loop
		g_signal_connect(op_timeline, "new-frame", G_CALLBACK(canvas_op__on_frame), op);
	}
	speed.x = 0;
	speed.y = 0;
	clutter_timeline_start(op_timeline);

	if(cop->item){
		Ptf actor_pos;
		clutter_actor_get_transformed_position(cop->item, &actor_pos.x, &actor_pos.y);

		op->diff.x      = op->mouse.x - actor_pos.x;
		op->diff.y      = op->mouse.y - actor_pos.y;
		dbg(2, "actor_pos=%.1fx%.1f diff=%.1fx%.1f", actor_pos.x, actor_pos.y, op->diff.x, op->diff.y);
	}

	op->start.x     = event->x;
	op->start.y     = event->y;
	op->mouse.x     = event->x;
	op->mouse.y     = event->y;
	op->old_bounded.x = op->mouse.x;
	op->old_bounded.y = op->mouse.y;
	dbg(2, "diff=%.1f x %.1f event=%.1f x %.1f", op->diff.x, op->diff.y, event->x, event->y);
	//op->part       = gpart;
	op->type        = type;
	dbg(2, "type=%s", canvas_op__print_type(op));
	switch(type){
		case OP_NONE:
			op->press  = cl_canvas_op__press;
			op->motion = cl_canvas_op__none_motion;
			op->finish = cl_canvas_op__finish;
			break;
		case OP_MOVE:
			op->press  = cl_canvas_op__move_press;
			op->motion = cl_canvas_op__move_motion;
			op->finish = cl_canvas_op__move_finish;
			break;
		case OP_COPY:
			op->press  = cl_canvas_op__copy_press;
			op->motion = cl_canvas_op__copy_motion;
			op->finish = cl_canvas_op__copy_finish;
			break;
		case OP_RESIZE_RIGHT:
			//op->press  = canvas_op__press;
			//op->motion = canvas_op__resize_right_motion;
			//op->finish = canvas_op__resize_right_finish;
			break;
		case OP_BOX:
			op->press  = cl_canvas_op__box_press;
			//op->motion = canvas_op__box_motion;
			//op->finish = canvas_op__box_finish;
			break;
		default:
			op->press  = cl_canvas_op__press;
			op->motion = cl_canvas_op__motion;
			op->finish = cl_canvas_op__finish;
			break;
	}

	op->origin.x = op->win.x + op->xscrolloffset; //FIXME xscrolloffset not set?
	op->origin.y = op->win.y + op->yscrolloffset;

	return op;
}


static void cl_canvas__op_timeline_free(Arrange* arrange)
{
	if(op_timeline){
		dbg(2, "timeline=%p - clearing...", op_timeline);
		if(g_signal_handlers_disconnect_matched(op_timeline, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, canvas_op__on_frame, NULL) != 1) pwarn("disconnect failed!");
		if(op_timeline) g_object_unref(op_timeline);
		op_timeline = NULL;
	}
	else dbg(2, "already done - nothing to do.");
}


static void
cl_canvas_op__free(Arrange* arrange)
{
	PF;
	if(!arrange->canvas->op) pwarn("no op to free");

	cl_canvas__op_timeline_free(arrange);

	g_free(arrange->canvas->op);
	arrange->canvas->op = NULL;
}


static void
cl_canvas_op__press(CanvasOp* op, GdkEvent* event)
{
	PF;
}


static void
cl_canvas_op__box_press(CanvasOp* op, GdkEvent* event)
{
	// start a selection box
}


static void
cl_canvas_op__none_motion(CanvasOp* op)
{
	ClutterPart* part;
	AMPart* gpart;
	ArrTrk* trk;
	gboolean at_start, at_end;
	//maybe a pick_feature() fn is more appropriate...
	arr_cl_pick2(op->arrange, op->mouse.x, op->mouse.y, &part, &gpart, &trk, &at_start, &at_end);
	if(at_start || at_end){
		set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
		op->ready = READY_RESIZE_LEFT;
	}else{
		arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
	}

	cl_canvas_op__motion(op);
}


static void
cl_canvas_op__finish(CanvasOp* op)
{
	PF;
	cl_canvas__op_timeline_free(op->arrange); //TODO dont we have to wait until the animation is finished?

	op->type   = OP_NONE;
	op->motion = cl_canvas_op__motion;
	op->finish = cl_canvas_op__finish;
}


static void
menu_arr_set_cl_canvas(GtkMenuItem* menuitem, Arrange* arrange)
{
	CanvasType arr_cl_get_type();
	arr_set_canvas(arrange, arr_cl_get_type());
}


static void
arr_cl_set_spp(Arrange* arrange, uint32_t samples)
{
	arr_cl_canvas_redraw_fast(arrange);
}


void
arr_k_clutter(GtkAccelGroup* accel_group, gpointer data)
{
	arr_set_canvas((Arrange*)app->active_panel, arr_cl_get_type());
	menu_update_options();
}


void
clutter_never()
{
	char c = type_strings[0][0]; c++;
#ifdef ENABLE_ANIMATION
	arr_cl_fade_in(NULL, NULL);
#endif
}


