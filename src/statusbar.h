/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __statusbar_h__
#define __statusbar_h__

#include "src/window.h"

G_BEGIN_DECLS

#define TYPE_STATUSBAR (statusbar_get_type ())
#define STATUSBAR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_STATUSBAR, Statusbar))
#define STATUSBAR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_STATUSBAR, StatusbarClass))
#define IS_STATUSBAR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_STATUSBAR))
#define IS_STATUSBAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_STATUSBAR))
#define STATUSBAR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_STATUSBAR, StatusbarClass))

typedef struct _Statusbar        Statusbar;
typedef struct _StatusbarClass   StatusbarClass;
typedef struct _StatusbarPrivate StatusbarPrivate;
typedef struct _statusbar_id     StatusbarId;

struct _statusbar_id
{
	guint cid; // the statusbar context_id.
	guint mid; // the statusbar message_id.
};

typedef enum {
	STATUSBAR_ICON = 0,
	STATUSBAR_MAIN,
	STATUSBAR_SELECTION,
	STATUSBAR_HOVER,
	STATUSBAR_MAX
} StatusbarNum;

struct _Statusbar {
	GtkHBox           parent_instance;
	StatusbarPrivate* priv;

	GtkWidget*        widget[STATUSBAR_MAX];
	GtkWidget*        progress;
	StatusbarId       id_partsel;           // statusbar id for part selections
	StatusbarId       drag_id;              // statusbar id for drag operations
};

struct _StatusbarClass {
	GtkHBoxClass      parent_class;
};

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Statusbar, g_object_unref)

GType      statusbar_get_type        () G_GNUC_CONST;
GtkWidget* statusbar__new            (AyyiWindow*);
void       statusbar__set_icon       (Statusbar*, const gchar* stock_id);

void       statusbar__enable_logging ();

G_END_DECLS

#endif
