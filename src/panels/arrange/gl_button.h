/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2016 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __gl_button_h__
#define __gl_button_h__

typedef void      (*ButtonAction)   (AGlActor*, gpointer);
typedef bool      (*ButtonGetState) (AGlActor*, gpointer);

typedef struct {
    AGlActor       actor;
    guint          icon;
    ButtonAction   action;
    ButtonGetState get_state;
    uint32_t       bg_colour;
    gboolean       disabled;
    float          bg_opacity;
    float          ripple_radius;
    WfAnimatable*  animatables[2];
    gpointer       user_data;
} ButtonActor;

#endif
