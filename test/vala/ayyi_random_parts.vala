/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
using GLib;
using Gdk;
using Ayyi;

public class Ayyi.RandomParts : Ayyi.SimpleClient
{
	public static int n_max = 100;
	public static int min_parts = 10;

	public static RandomParts* instance;
	public static int n_done = 0;
	Rand rand;

	public static int main(string[] args)
	{
		app = new RandomParts();
		instance = (RandomParts*)app;

		if(!app.run(instance->on_shm)) return 1;

		return 0;
	}

	private void on_shm()
	{
		rand = new Rand();
		add_part();
	}

	public void add_part()
	{
		print("add_part\n");

		string _name = "Audio";
		char* name = AyyiSong.container_next_name(&((SongService*)ayyi.service)->song->audio_regions, _name);
		GLib.List<Model.PoolItem*>* pool = song->pool->list;
		Model.PoolItem* pool_item = pool->first().data;

		Rand rand = new Rand();
		SongPos pos = {rand.int_range(0, 4 * 20), 0, 0};

		GPos len = {1, 0, 0};

		AyyiTrack* trk = AyyiSong.audio_track_next(null);
		if(AyyiSong.track_is_master(trk)){
			trk = AyyiSong.audio_track_next(trk);
		}

		Track* tr = instance->find_track();
		if(!(bool)tr){ print("no track\n"); app.quit(); }

		AM.Song.add_part(MediaType.AUDIO, REGION_FULL, pool_item, tr->ident.idx, &pos, AM.pos2mu(&len), 0, name, 0, (obj, error) => next());
	}

	void delete_part()
	{
		print("delete_part\n");

		uint n_parts = song->parts->length();
		AM.Part* part = (AM.Part*)song->parts->list.nth_data(rand.int_range(0, (int32)n_parts));
		AM.Song.remove_part(part, (ident, error) => { next(); });
	}

	private void wait(ClientDelegateType wait_done)
	{
		Timeout.add (4000, on_timeout);
	}

	private bool on_timeout()
	{
		if(song->parts->length() < min_parts || rand.boolean()){
			instance->add_part();
		}else{
			instance->delete_part();
		}
		return false;
	}

	private Track* find_track()
	{
		int i = 0;
		while(i<100){
			uint t = rand.int_range(0, (int)song->tracks->length() - 1);
			if(song->tracks->at((int)t)->visible) return song->tracks->at((int)t);
			i++;
		}
		return null;
	}

	public static void next()
	{
		n_done++;
		if(n_done < n_max){
			instance->wait(instance->add_part);
		}else{
			print("finished\n");
			log_print(LogType.OK, "part added");
			app.quit();
		}
	}
}


