/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __target_actor_h__
#define __target_actor_h__

typedef struct {
    AGlActor actor;
    ArrTrk*  track;
    AMPart*  part;
    int      width;
} Target;


AGlActor* target_actor (gpointer);

#endif
