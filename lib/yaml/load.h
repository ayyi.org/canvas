/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2007-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <stdbool.h>
#include <yaml.h>

typedef void  (*YamlMappingCallback) (yaml_parser_t*, const char* key, gpointer);
typedef void  (YamlValueCallback)   (const yaml_event_t*, const char* key, gpointer);

typedef struct
{
	char*               key;
	YamlMappingCallback callback;
	gpointer            data;
} YamlHandler;

typedef struct
{
	char*             key;
	YamlValueCallback* callback;
	gpointer          data;
} YamlValueHandler;


bool yaml_load            (FILE*, YamlHandler[]);
bool yaml_load_string     (const char*, YamlHandler[]);
bool yaml_load_mapping    (yaml_parser_t*, YamlHandler[], YamlValueHandler[], gpointer);
bool yaml_load_section2   (yaml_parser_t*, YamlHandler**, YamlValueHandler**);

YamlValueCallback yaml_set_string;
YamlValueCallback yaml_set_int;
YamlValueCallback yaml_set_uint64;
YamlValueCallback yaml_set_position;

#define get_expected_event(parser, event, EVENT_TYPE) \
	if (!yaml_parser_parse(parser, event)) return false; \
	if ((event)->type != EVENT_TYPE) return false;

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC(yaml_event_t, yaml_event_delete)
G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC(yaml_parser_t, yaml_parser_delete)
