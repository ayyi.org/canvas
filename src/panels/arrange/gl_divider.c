/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define DIVIDER_WIDTH 4

typedef struct {
	AGlActor  actor;
} DividerActor;

static void arr_gl_divider_set_size (AGlActor*);
static bool arr_gl_divider_on_event (AGlActor*, GdkEvent*, AGliPt);


AGlActor*
arr_gl_divider(GtkWidget* panel)
{
	// This actor has no draw fn, it only captures events.

	Arrange* arrange = (Arrange*)panel;

	return agl_actor__new (AGlActor,
		.name = g_strdup("Divider"),
		.region   = (AGlfRegion){arrange->tc_width.val.i - DIVIDER_WIDTH, -arrange->ruler_height, arrange->tc_width.val.i, 1280},
		.set_size = arr_gl_divider_set_size,
		.paint    = agl_actor__null_painter,
		.on_event = arr_gl_divider_on_event
	);
}


static void
arr_gl_divider_set_size (AGlActor* actor)
{
}


static bool
arr_gl_divider_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	Arrange* arrange = actor->root->user_data;

	switch (event->type) {
		case GDK_BUTTON_PRESS:
			agl_actor__grab(actor);
			set_cursor(arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			break;
		case GDK_MOTION_NOTIFY:
			if (actor_context.grabbed == actor) {
				arr_gl_set_tc_width(arrange, event->button.x);
			}
			else set_cursor(arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
			break;
		case GDK_2BUTTON_PRESS:
			dbg(1, "double click");
			break;
		case GDK_BUTTON_RELEASE:
			actor->region.x1 = arrange->tc_width.val.i - DIVIDER_WIDTH;
			actor->region.x2 = arrange->tc_width.val.i;
			break;
		default:
			break;
	}
	return HANDLED;
}
