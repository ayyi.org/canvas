/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
/*
 *  svgbutton.h - multi-state button with svg images.
 *
 */
#ifndef __svgbutton_h__
#define __svgbutton_h__

#include <gdk/gdk.h>
#include <gtk/gtkvbox.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

G_BEGIN_DECLS

#define TYPE_SVGBUTTON            (svgbutton_get_type())
#define SVGBUTTON(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SVGBUTTON, SvgButton))
#define SVGBUTTON_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SVGBUTTON, SvgButtonClass))
#define IS_SVGBUTTON(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SVGBUTTON))
#define IS_SVGBUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SVGBUTTON))
#define SVGBUTTON_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SVGBUTTON, SvgButtonClass))

typedef struct _SvgButton       SvgButton;
typedef struct _SvgButtonClass  SvgButtonClass;
typedef struct _SvgButtonState  SvgButtonState;

struct _SvgButton
{
  GtkButton           button;
  int                 size;
  
  GList*              state_list;
  SvgButtonState*     current_state;

  char*               mouseover_text;
};

struct _SvgButtonClass
{
  GtkButtonClass parent_class;

  void (* svgbutton) (SvgButton*);
};

GType          svgbutton_get_type        (void);
GtkWidget*     svgbutton_new             (void);
void           svgbutton_add_state       (SvgButton*, const char* state_name, const char* icon);
char*          svgbutton_get_state       (SvgButton*);
void           svgbutton_set_state_n     (SvgButton*, int n);
void           svgbutton_set_size        (SvgButton*, int);

G_END_DECLS

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
