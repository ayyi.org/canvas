/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2004-2013 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

using GLib;

public class Ayyi.App : GLib.Object
{
	public int state = 0;

	public int  font_size;
	public char font_family[64];
	public int  line_height;

	public signal void app_stop_meters     ();
	public signal void app_start_meters    ();
	public signal void styles_available    ();
	public signal void shortcuts_changed   ();
	public signal void focus_changed       ();

	private void* glconfig;

	construct
	{
	}

	public void
	set_state(int _state)
	{
		state = _state;
	}

	public void*
	get_glconig ()
	{
		return glconfig;
	}

	public void quit()
	{
	}
}

