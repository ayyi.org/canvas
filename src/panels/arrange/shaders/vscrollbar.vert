uniform vec2 modelview;
uniform vec2 translate;

attribute vec2 vertex;

varying vec2 position;

void main () {
	position = vertex;
	gl_Position = vec4(vec2(1., -1.) * (vertex + translate) / modelview - vec2(1.0, -1.0), 1.0, 1.0);
}
