/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

static void     rotation_start             (Arrange*);
static gboolean rotation_frame             (Arrange*);

static float rotate[3] = {0,};
static float isometric_rotation[3] = {35.264f, 45.0f, 0.0f};

#ifdef NOT_USED
static void     gl_on_rotate    (GtkWidget*, Arrange*);
static void     gl_on_rotate2   (GtkWidget*, Arrange*);
static void     gl_on_rotate3   (GtkWidget*, Arrange*);
#endif


static AGlActor*
arr_gl_iso_frame (GtkWidget* panel)
{
	bool arr_gl_draw_iso_frame(AGlActor* actor)
	{
		Arrange* arrange = (Arrange*)actor->root->gl.gdk.widget;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

		double vp_left = c->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
		double beat = arr_px2beat(arrange, vp_left);
		int first_beat = beat;

		#define ISO_FRAME_DEPTH 400
		agl_enable(0 /* !AGL_ENABL_ETEXTURE_2D */);
		glColor4f(1.0, 1.0, 1.0, 0.1);
		float h = ISOMETRIC_HEIGHT * vzoom(arrange);
		GPos pos = {0,0,0};
		if (c->isometric) {
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, -ISO_FRAME_DEPTH);
			for (int i=first_beat; i<first_beat+40; i++) {
				pos.beat = i;
				double x = arr_gl_pos2px(arrange, &pos) - vp_left;
				if(x > arrange->canvas_scrollwin->allocation.width) break;

				glVertex3f(x, h, -10.0);
				glVertex3f(x, h, -ISO_FRAME_DEPTH);
			}
			glEnd();
		}
		return true;
	}

	return agl_actor__new(AGlActor,
		.paint = arr_gl_draw_iso_frame
	);
}


static AGlActor*
arr_gl_iso_tc (GtkWidget* panel)
{
	bool arr_gl_draw_tc (AGlActor* actor)
	{
		//only shown in isometric mode

		Arrange* arrange = (Arrange*)actor->root->gl.gdk.widget;

		double height = (ISOMETRIC_HEIGHT * vzoom(arrange)) / 15;
		double spacing = 10;

		glMatrixMode(GL_MODELVIEW);
												// TODO push not needed. is done by the actor
		glPushMatrix();
		glLoadIdentity();
		glRotatef(isometric_rotation[0], 1.0f, 0.0f, 0.0f);
		glRotatef(-45, 0.0f, 1.0f, 0.0f);

		AMTrack* track = NULL;
		AMIter i;
		am_collection_iter_init (am_tracks, &i);
		while((track = am_collection_iter_next(am_tracks, &i))){ // should use Arrange iterator instead: track_list__next_visible_
			TrackNum t = am_track_list_position(song->tracks, track);
			int colour = MAX(0, am_track__get_colour(track));

			double top = (t * (height + spacing));
			double bot = top + height;
			dbg(10, "%i: bot=%.2f", t, bot);
			double x1 = -ISOMETRIC_X0 + 30 * t;
			double x2 =             0 + 30 * t;

			gl_set_palette_colour(colour);

	#if 1
			agl_enable(AGL_ENABLE_BLEND);
			glBindTexture(GL_TEXTURE_2D, bg_textures[TEXTURE_BG]);
	#endif
			glBegin(GL_QUADS);
			//glVertex3d(x1, top, 0);
			//glVertex3d(x2, top, 0);
			//glVertex3d(x2, bot, 0);
			//glVertex3d(x1, bot, 0);
			glTexCoord2d(1.0, 1.0); glVertex2d(x1, top);
			glTexCoord2d(0.0, 1.0); glVertex2d(x2, top);
			glTexCoord2d(0.0, 0.0); glVertex2d(x2, bot);
			glTexCoord2d(1.0, 0.0); glVertex2d(x1, bot);
			glEnd();

			glBegin(GL_LINES);
			glVertex3d(x2, bot, 0);
			glVertex3d(-t * TRACK_SPACING_Z, bot, 0);
			glVertex3d(-t * TRACK_SPACING_Z, bot, 0);
			glVertex3d(-t * TRACK_SPACING_Z, (ISOMETRIC_HEIGHT * vzoom(arrange)) / 2, 0);
			glVertex3d(-t * TRACK_SPACING_Z, (ISOMETRIC_HEIGHT * vzoom(arrange)) / 2, 0);
			glVertex3d(-t * TRACK_SPACING_Z, (ISOMETRIC_HEIGHT * vzoom(arrange)) / 2, -1500);
			glEnd();

			agl_print(0, bot - 10, 0, 0xffffffff, "%s", song->tracks->track[t]->name);
		}

		glPopMatrix();
		return true;
	}

	return agl_actor__new(AGlActor,
		.paint = arr_gl_draw_tc
	);
}


gint rotation_timer = 0;
gint rotation_step = 0; //debugging

static void
rotation_start (Arrange* arrange)
{
	rotation_step = 0;
	rotation_timer = g_timeout_add(50/*ms*/, (gpointer)rotation_frame, arrange);
}


static gboolean
rotation_frame (Arrange* arrange)
{
	GlCanvas* c = arrange->canvas->gl;

	// Xserver will die if rotation continues!
	if(rotation_step++ > 100) return FALSE;

	if(c->isometric) c->iso_factor += 0.01 + ((1-c->iso_factor) * 0.08);
	else             c->iso_factor -= 0.01 + ((1-c->iso_factor) * 0.08);
	gtk_widget_queue_draw(arrange->canvas->gl->image);

	rotate[0] = isometric_rotation[0] * c->iso_factor;
	rotate[1] = isometric_rotation[1] * c->iso_factor;
	rotate[2] = isometric_rotation[2] * c->iso_factor;

	arr_gl_setup_view(arrange);

	float target_factor = c->isometric ? 1.0 : 0.0;
	gboolean stop = fabs(c->iso_factor - target_factor) < 0.1;
	if(stop){
		c->iso_factor = target_factor;
		c->part_background_alpha = c->isometric ? HAS_ALPHA_TRUE : HAS_ALPHA_FALSE;
	}
	return !stop;
}


#ifdef NOT_USED
static void
gl_on_rotate (GtkWidget* widget, Arrange* arrange)
{
	GtkAdjustment* adj = gtk_range_get_adjustment(GTK_RANGE(widget));
	rotate[0] = adj->value / 10;

	arr_gl_setup_view(arrange);

	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


static void
gl_on_rotate2 (GtkWidget* widget, Arrange* arrange)
{
	GtkAdjustment* adj = gtk_range_get_adjustment(GTK_RANGE(widget));
	rotate[1] = adj->value / 5;

	arr_gl_setup_view(arrange);

	gtk_widget_queue_draw(arrange->canvas->gl->image);
}


static void
gl_on_rotate3 (GtkWidget* widget, Arrange* arrange)
{
	GtkAdjustment* adj = gtk_range_get_adjustment(GTK_RANGE(widget));
	rotate[2] = adj->value / 5;

	arr_gl_setup_view(arrange);

	gtk_widget_queue_draw(arrange->canvas->gl->image);
}
#endif


