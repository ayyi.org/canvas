/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __lv2_h__
#define __lv2_h__

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_LV2_WIN            (ayyi_lv2_win_get_type ())
#define AYYI_LV2_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_LV2_WIN, AyyiLv2Win))
#define AYYI_LV2_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_LV2_WIN, AyyiLv2WinClass))
#define AYYI_IS_LV2_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_LV2_WIN))
#define AYYI_IS_LV2_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_LV2_WIN))
#define AYYI_LV2_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_LV2_WIN, AyyiLv2WinClass))

typedef struct _AyyiLv2WinClass AyyiLv2WinClass;

struct _AyyiLv2WinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiLv2Win {
	AyyiPanel          panel;
	GtkWidget*         area;
	void*              plugin_props;
};

G_END_DECLS

#endif
