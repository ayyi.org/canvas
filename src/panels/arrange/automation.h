/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#ifdef USE_GNOMECANVAS
#include "gnome/automation.h"
#endif

void         automation_on_song_load (Arrange*);
void         automation_setup        (ArrTrk*);
void         automation_free         (ArrTrk*);

void         automation_view_vol     (ArrTrk*);
void         automation_view_pan     (ArrTrk*);

ArrAutoPath* automation_current_path (ArrTrk*);
