/*
 * This file is part of AyyiGtk.
 * Copyright (C) 2008 Tim Orford <tim@orford.org>
 */
#include <config.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtkmain.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <gtk/gtkbutton.h>

typedef struct _glyph Glyph;
#include <image/text_render.h>
#include "text_fx.h"

#define SCROLL_DELAY_LENGTH  300
#define TEXT_FX_DEFAULT_WIDTH 540
#define TEXT_FX_DEFAULT_HEIGHT 50

#define HAS_ALPHA_FALSE 0
#define BITS_PER_PIXEL 8

struct _TextFxPriv
{
  char*         prev_str; //the string currently rendered onto the pixbuf. Used to determine how much re-rendering is needed.
};

static void     text_fx_class_init           (TextFxClass*);
static void     text_fx_init                 (TextFx*);
static void     text_fx_destroy              (GtkObject*);
static void     text_fx_realize              (GtkWidget*);
static void     text_fx_size_request         (GtkWidget*, GtkRequisition *requisition);
static void     text_fx_size_allocate        (GtkWidget*, GtkAllocation *allocation);
static gint     text_fx_expose               (GtkWidget*, GdkEventExpose *event);
static gint     text_fx_button_press         (GtkWidget*, GdkEventButton *event);
static gint     text_fx_button_release       (GtkWidget*, GdkEventButton *event);
static gint     text_fx_motion_notify        (GtkWidget*, GdkEventMotion *event);
static gboolean text_fx_enter_notify         (GtkWidget*, GdkEventCrossing *event);
static gboolean text_fx_leave_notify         (GtkWidget*, GdkEventCrossing *event);
static guint    text_fx_get_natural_width    (TextFx*);
//static gboolean text_fx_timer                (TextFx* textfx);

static void     text_fx_update_mouse         (TextFx* textfx, gint x, gint y);
//static void     text_fx_update               (TextFx* textfx);
static void     text_fx_render_text          (GtkWidget*, int digit_from);

static char font_string[256] = "San-Serif 9";

//--------------------------------------------------------

typedef struct _PixopsFilter PixopsFilter;
typedef struct _PixopsFilterDimension PixopsFilterDimension;

typedef guchar *(*PixopsLineFunc) (int *weights, int n_x, int n_y,
				   guchar *dest, int dest_x, guchar *dest_end, int dest_channels, int dest_has_alpha,
				   guchar **src, int src_channels, gboolean src_has_alpha,
				   int x_init, int x_step, int src_width,
				   int check_size, guint32 color1, guint32 color2);

typedef void (*PixopsPixelFunc) (guchar *dest, int dest_x, int dest_channels, int dest_has_alpha,
				 int src_has_alpha, int check_size, guint32 color1,
				 guint32 color2,
				 guint r, guint g, guint b, guint a);

/* Interpolation modes; must match GdkInterpType */
typedef enum {
    PIXOPS_INTERP_NEAREST,
    PIXOPS_INTERP_TILES,
    PIXOPS_INTERP_BILINEAR,
    PIXOPS_INTERP_HYPER
} PixopsInterpType;

void _pixops_composite (guchar* dest_buf, int render_x0, int render_y0, int render_x1, int render_y1, int dest_rowstride, int dest_channels, gboolean dest_has_alpha, const guchar  *src_buf, int src_width, int src_height, int src_rowstride, int src_channels, gboolean src_has_alpha, guchar* debug_bound);
#if 0
static void pixops_process (guchar *dest_buf,
		int             render_x0,
		int             render_y0,
		int             render_x1,
		int             render_y1,
		int             dest_rowstride,
		int             dest_channels,
		gboolean        dest_has_alpha,
		const guchar   *src_buf,
		int             src_width,
		int             src_height,
		int             src_rowstride,
		int             src_channels,
		gboolean        src_has_alpha,
		int             check_x,
		int             check_y,
		int             check_size,
		guint32         color1,
		guint32         color2,
		PixopsFilter   *filter,
		PixopsLineFunc  line_func,
		PixopsPixelFunc pixel_func);
#endif


//local data:
static GtkWidgetClass *parent_class = NULL;

void colour_lighter_gdk(GdkColor* colour, int amount);
void colour_get_frame  (GdkColor* colour);
void pixbuf_clear_rect (GdkPixbuf*, GdkRectangle*);


GType
text_fx_get_type ()
{
  static GType textfx_type = 0;

  if (!textfx_type)
    {
      static const GTypeInfo textfx_info =
      {
        sizeof (TextFxClass),
        NULL, NULL,
        (GClassInitFunc) text_fx_class_init,
        NULL, NULL,
        sizeof (TextFx),
        0,
        (GInstanceInitFunc) text_fx_init,
      };

      textfx_type = g_type_register_static (GTK_TYPE_WIDGET, "TextFx", &textfx_info, 0);
    }

  return textfx_type;
}


static void
text_fx_class_init (TextFxClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  object_class->destroy = text_fx_destroy;

  widget_class->realize = text_fx_realize;
  widget_class->expose_event = text_fx_expose;
  widget_class->size_request = text_fx_size_request;
  widget_class->size_allocate = text_fx_size_allocate;
  widget_class->button_press_event = text_fx_button_press;
  widget_class->button_release_event = text_fx_button_release;
  widget_class->motion_notify_event = text_fx_motion_notify;

  widget_class->enter_notify_event = text_fx_enter_notify;
  widget_class->leave_notify_event = text_fx_leave_notify;
}


static void
text_fx_init (TextFx *textfx)
{
  GTK_WIDGET_SET_FLAGS (textfx, GTK_CAN_FOCUS);

  textfx->button = 0;
  //textfx->policy = GTK_UPDATE_CONTINUOUS;
  textfx->timer = 0;
  textfx->text_width  = 0;
  textfx->text_height = 0;
  textfx->font_size = 9; //FIXME this should be overridden later using a theme value.
  textfx->padding_top = 3;
  //textfx->padding_bottom = 3;
  textfx->pixbuf = NULL;

  //textfx->priv = G_TYPE_INSTANCE_GET_PRIVATE (textfx, text_fx_get_type(), TextFxPriv);
  textfx->priv = g_new0(TextFxPriv, 1);
}


GtkWidget*
text_fx_new (Glyph** buffers)
{
  TextFx *textfx = g_object_new (text_fx_get_type (), NULL);

  textfx->src_bufs = buffers;

  gtk_widget_add_events(GTK_WIDGET(textfx), GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

  return GTK_WIDGET (textfx);
}


static void
text_fx_destroy (GtkObject *object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_TEXT_FX (object));

	TextFx* text_fx = TEXT_FX(object);

	if (text_fx->pixbuf) { g_object_unref(text_fx->pixbuf); text_fx->pixbuf = NULL; }
	if (GTK_OBJECT_CLASS (parent_class)->destroy) (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
#if 0 //double free! looks like this is being freed elsewhere.
	if (text_fx->priv) g_free(text_fx->priv);
#endif
}


#if 0
void
text_fx_set_update_policy (TextFx *textfx, GtkUpdateType policy)
{
  g_return_if_fail (textfx != NULL);
  g_return_if_fail (IS_TEXT_FX (textfx));

  textfx->policy = policy;
}
#endif


void
text_fx_set_font (TextFx* textfx, const char* font)
{
	//this is a static function. The font string only needs to be set once per application.

	strncpy(font_string, font, 255);
}


#if 0
void
text_fx_set_value (GtkWidget *widget, int val)
{
	TextFx *textfx = TEXT_FX(widget);

	if(val != textfx->val){
		textfx->val = val;
		text_fx_render_text(widget, 0);
	}
}
#endif


void
text_fx_set_string (GtkWidget *widget, const char* str)
{
	TextFx *textfx = TEXT_FX(widget);

	if(!textfx->str || strcmp(str, textfx->str)){
		if(textfx->str) g_free(textfx->str);
		textfx->str = g_strdup(str);
		text_fx_render_text(widget, 0);
	}
}


void
text_fx_set_padding (GtkWidget *widget, int top, int bottom)
{
  if(top > -1){
    //d->padding_top = top;
  }

  if(bottom > -1){
    //d->padding_bottom = bottom;
  }
}


static void
text_fx_realize (GtkWidget *widget)
{
  g_return_if_fail (widget);
  g_return_if_fail (IS_TEXT_FX (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  GdkWindowAttr attributes;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) |
                          GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK |
                          GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
                          GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  gint attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);

  //change widget background color:
  GtkStyle *style = gtk_style_copy(gtk_widget_get_style(widget));
  GdkColor color1 = style->bg[GTK_STATE_NORMAL];
  //colour_lighter_gdk(&color1, 2);
  colour_get_frame(&color1);
  style->bg[GTK_STATE_NORMAL] = color1;
  gtk_widget_set_style (widget, style);

  gdk_window_set_user_data (widget->window, widget);

  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  //------------------------------------------

  //create the text object:
  text_fx_render_text(widget, 0);
}


static int
get_render_start_char(TextFx *textfx)
{
	TextFxPriv* priv = textfx->priv;
	if(!priv->prev_str) return 0;
	int end = MIN(strlen(textfx->str), strlen(priv->prev_str));
	int i; for(i=0;i<end;i++){
		if(textfx->str[i] != priv->prev_str[i]) return i;
	}
	return 0;
}


static void
text_fx_render_text (GtkWidget *widget, int digit_from)
{
	//render the text onto the backbuf, following widget creation or a change in value.
	//chars positioned before @digit_from are not redrawn.

	//printf("%s()...\n", __func__);
	if(!GTK_WIDGET_REALIZED(widget)) return;

	TextFx *textfx = TEXT_FX(widget);
	TextFxPriv* priv = textfx->priv;

	//GtkStyle *style = gtk_style_copy(gtk_widget_get_style(widget)); //FREE!!!
	//GdkColor color_bg = textfx->in_button ? style->bg[GTK_STATE_PRELIGHT] : style->bg[GTK_STATE_NORMAL];
	//GdkColor color_fg = style->fg[GTK_STATE_NORMAL];

	//char format[8];   sprintf(format, "%%0%ii:", textfx->n_chars);
	//printf("%s(): format=%s\n", __func__, format);
	//char charlist[16]; sprintf(charlist, format, textfx->val);
	int n_chars = textfx->str ? strlen(textfx->str) : 0;
	if(!n_chars) return;

	//at which char should we start rendering?
	int start = get_render_start_char(textfx);

    #define FILTER_WIDTH 4 //FIXME use the real value
    int glyph_width = textfx->src_bufs[0]->rect->width - FILTER_WIDTH * 2;

	char* charlist = textfx->str;
	int height = textfx->src_bufs[0]->rect->height;
	guint pixbuf_width = text_fx_get_natural_width(textfx);
	pixbuf_width += 256; //FIXME crashes if we remove this.

	if(!textfx->pixbuf) printf("%s(): making pixbuf... width=%i height=%i\n", __func__, pixbuf_width, height);
	if(!textfx->pixbuf) textfx->pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE, BITS_PER_PIXEL, pixbuf_width, height);
	//printf("%s(): pixbuf=%p pix=%p\n", __func__, textfx->pixbuf, gdk_pixbuf_get_pixels(textfx->pixbuf));
	int start_px = start * glyph_width;
	GdkRectangle clear = {start_px, 0, pixbuf_width - start_px, height};
	pixbuf_clear_rect(textfx->pixbuf, &clear);

    int src_width = gdk_pixbuf_get_width(textfx->src_bufs[0]->pixbuf);
    //if(g->rect) printf("%s(): src_width=%i glyph: %i\n", __func__, src_width, g->rect->width);
    int src_height = gdk_pixbuf_get_height(textfx->src_bufs[0]->pixbuf);
	int src_chans = gdk_pixbuf_get_n_channels(textfx->src_bufs[0]->pixbuf);
	if(src_chans != 3) printf("%s(): !3\n", __func__);

	int x = 0;
	int i; for(i=start;i<n_chars;i++){
      //Glyph* g = textfx->src_bufs[charlist[i]-48];
      GdkPixbuf* src = textfx->src_bufs[charlist[i]-48]->pixbuf;
#if 0
      gdk_pixbuf_composite(src,
                   textfx->pixbuf,            //dest
                   x, 0,                      //int dest x,y
                   src_width, src_height,     //dest w,h
                   0.0, 0.0,                  //double src offset (translation)
                   1.0, 1.0,                  //double src scale
                   GDK_INTERP_NEAREST,
                   255);
#else
      #ifndef NEW_SUPER_COMPOSITOR

      guchar* dest_buf = gdk_pixbuf_get_pixels(textfx->pixbuf);
      int render_x0=0, render_y0=FILTER_WIDTH, render_x1=src_width, render_y1=src_height - 0*FILTER_WIDTH;

      //printf("%s(): i=%i dest_buf=%p\n", __func__, i, dest_buf);
      if((i+1)*glyph_width > gdk_pixbuf_get_width(textfx->pixbuf)) printf("%s(): **** address outside of buffer.\n", __func__);

      //dest coords are not explicity specified. we have to offset the buffer address.
      _pixops_composite(dest_buf + i*src_chans*glyph_width,
                        //define the source rect:
                        render_x0, render_y0, render_x1, render_y1,

                        //specify props of dest buffer:
                        gdk_pixbuf_get_rowstride(textfx->pixbuf), gdk_pixbuf_get_n_channels(textfx->pixbuf), gdk_pixbuf_get_has_alpha(textfx->pixbuf),

                        //specify props of source buffer:
                        gdk_pixbuf_get_pixels(src), src_width, src_height, gdk_pixbuf_get_rowstride(src), gdk_pixbuf_get_n_channels(src), gdk_pixbuf_get_has_alpha(src),

						//temp debugging buffer bound:
						dest_buf + gdk_pixbuf_get_rowstride(textfx->pixbuf) * gdk_pixbuf_get_height(textfx->pixbuf));
      #else
      gdk_pixbuf_copy_area(src,
                       0, 0,                  //int src_x,src_y
                       src_width, src_height, //int width,height
                       textfx->pixbuf,
                       x,                     //int dest_x
                       0);                    //int dest_y
      #endif
#endif
      //printf("%s(): x=%i width=%i\n", __func__, x, src_width);
      x += src_width;
	}

	gtk_widget_queue_draw (widget);

	if(priv->prev_str) g_free(priv->prev_str);
	priv->prev_str = g_strdup(textfx->str);
}


static guint
text_fx_get_natural_width(TextFx *textfx)
{
	int n_chars = textfx->str ? strlen(textfx->str) : 0;
	if(!n_chars) return TEXT_FX_DEFAULT_WIDTH;

	guint width = 0;
	int i; for(i=0;i<n_chars;i++){
		width += textfx->src_bufs[textfx->str[i]-48]->rect->width - FILTER_WIDTH * 2; //it actually can be slightly smaller
	}
	return width;
}


static void
text_fx_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	//note: we dont have to override the default size_request() with this function...

	TextFx *textfx = TEXT_FX (widget);

	requisition->width  = text_fx_get_natural_width(textfx);
	requisition->height = textfx->src_bufs[0]->rect->height;

	printf("%s(): %i x %i\n", __func__, requisition->width, requisition->height);
}


static void
text_fx_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
  g_return_if_fail (widget);
  g_return_if_fail (IS_TEXT_FX(widget));
  g_return_if_fail (allocation);

  TextFx *textfx = TEXT_FX (widget);

  widget->allocation = *allocation;

  if(GTK_WIDGET_REALIZED(widget)){
    gdk_window_move_resize(widget->window,
			      allocation->x, allocation->y, //x,y pos relative to windows parent. Normally zero.
			      allocation->width,
			      allocation->height
                  );

    GdkPixbuf* pixbuf = textfx->pixbuf;
    if(0 && pixbuf){
      if(allocation->width != gdk_pixbuf_get_width(pixbuf) || allocation->height != gdk_pixbuf_get_height(pixbuf)){
        g_object_unref(pixbuf);
        textfx->pixbuf = NULL;
      }
    }
  }
}


static gint
text_fx_expose (GtkWidget *widget, GdkEventExpose *event)
{
	g_return_val_if_fail (widget, FALSE);
	g_return_val_if_fail (IS_TEXT_FX(widget), FALSE);
	g_return_val_if_fail (event, FALSE);

	if (event->count > 0) return FALSE;

	TextFx *textfx = TEXT_FX (widget);
	if(!textfx->pixbuf) return FALSE;

	//blit the pixbuf onto the window:

	guchar* pixels = (guchar*)gdk_pixbuf_get_pixels(textfx->pixbuf);

	gdk_draw_rgb_image (widget->window,
	                    widget->style->black_gc,                   // according to api docs, this is ignored.
	                    0,                                         // dest x
	                    0,                                         // dest y
	                    gdk_pixbuf_get_width(textfx->pixbuf), gdk_pixbuf_get_height(textfx->pixbuf),
	                    GDK_RGB_DITHER_NONE,
	                    pixels,                                    // src - must be 24bit.
	                    gdk_pixbuf_get_rowstride(textfx->pixbuf)); // the number of bytes in each row.

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		gint border_width = 1;//GTK_CONTAINER (widget)->border_width;
		gint x = widget->allocation.x + border_width;
		//gint y = widget->allocation.y + border_width;
		//gint width  = widget->allocation.width  - border_width * 2;
		//gint height = widget->allocation.height - border_width * 2;
		//printf("%s(): painting focus... x=%i y=%i width=%i height=%i\n", __func__, x, y, width, height);
		GdkRectangle *area = &event->area;

		gtk_paint_focus (widget->style, widget->window, GTK_WIDGET_STATE (widget),
		                 area, widget, "button",
		                 x, 0, widget->allocation.width - 2, widget->allocation.height);
	}

	return FALSE;
}


static gint
text_fx_button_press(GtkWidget* widget, GdkEventButton *event)
{
  TextFx *textfx;

  if(event->button != 1) return FALSE; //propogate

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_TEXT_FX (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  textfx = TEXT_FX(widget);

  gtk_widget_grab_focus (widget);

  /* Determine if button press was within pointer region - we
     do this by computing the parallel and perpendicular distance of
     the point where the mouse was pressed from the line passing through
     the pointer */

  //gint dx = event->x - widget->allocation.width / 2;
  //gint dy = widget->allocation.height / 2 - event->y;
  
  gtk_grab_add (widget);

  textfx->button = event->button;

  text_fx_update_mouse (textfx, event->x, event->y);

  return TRUE; //handled. Dont propagate.
}


static gint
text_fx_button_release (GtkWidget *widget, GdkEventButton *event)
{
  TextFx *textfx;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_TEXT_FX (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  textfx = TEXT_FX (widget);

  if (textfx->button == event->button){
    gtk_grab_remove (widget);

    textfx->button = 0;
  }

  return FALSE;
}


static gint
text_fx_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
  GdkModifierType mods;
  gint mask;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_TEXT_FX (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  TextFx *textfx = TEXT_FX (widget);

  if (textfx->button != 0){
    gint x = event->x;
    gint y = event->y;

    if (event->is_hint || (event->window != widget->window)) gdk_window_get_pointer (widget->window, &x, &y, &mods);

    switch (textfx->button)
	{
	case 1:
	  mask = GDK_BUTTON1_MASK;
	  break;
	case 2:
	  mask = GDK_BUTTON2_MASK;
	  break;
	case 3:
	  mask = GDK_BUTTON3_MASK;
	  break;
	default:
	  mask = 0;
	  break;
	}

    if (mods & mask) text_fx_update_mouse (textfx, x,y);
  }

  return FALSE;
}


static gboolean
text_fx_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  TextFx* d = TEXT_FX (widget);
  d->in_button = TRUE;
  gtk_widget_set_state(widget, GTK_STATE_PRELIGHT);

  //gtk_style_set_background (widget->style, widget->window, GTK_STATE_PRELIGHT);

  return FALSE;
}


static gboolean
text_fx_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
  TextFx* d = TEXT_FX(widget);
  d->in_button = FALSE;
  return FALSE;
}


#if 0
static gboolean
text_fx_timer (TextFx *textfx)
{
  g_return_val_if_fail (textfx != NULL, FALSE);
  g_return_val_if_fail (IS_TEXT_FX (textfx), FALSE);

  return FALSE;
}
#endif


static void
text_fx_update_mouse (TextFx *textfx, gint x, gint y)
{
  g_return_if_fail (textfx != NULL);
  g_return_if_fail (IS_TEXT_FX (textfx));
}


#if 0
static void
text_fx_update (TextFx *textfx)
{
  g_return_if_fail (textfx != NULL);
  g_return_if_fail (IS_TEXT_FX (textfx));

  gtk_widget_queue_draw (GTK_WIDGET (textfx));
}
#endif

//------------------------------------------------------------------------

// below are functions copied from gdkpixbuf/pixops (to be modified) - should ultimately be moved out of the widget.

#define SUBSAMPLE_BITS 4
#define SUBSAMPLE (1 << SUBSAMPLE_BITS)
#define SUBSAMPLE_MASK ((1 << SUBSAMPLE_BITS)-1)
#define SCALE_SHIFT 16

struct _PixopsFilterDimension
{
  int n;
  double offset;
  double *weights;
};

struct _PixopsFilter
{
  PixopsFilterDimension x;
  PixopsFilterDimension y;
  double overall_alpha;
}; 

#if 0
static void process_pixel (int *weights, int n_x, int n_y,
	       guchar *dest, int dest_x, int dest_channels, int dest_has_alpha,
	       guchar **src, int src_channels, gboolean src_has_alpha,
	       int x_start, int src_width,
	       int check_size, guint32 color1, guint32 color2,
	       PixopsPixelFunc pixel_func);
static int* make_filter_table (PixopsFilter *filter);
static void tile_make_weights (PixopsFilterDimension *dim, double scale);
static void make_weights (PixopsFilter *filter, PixopsInterpType  interp_type,double scale_x, double scale_y);
static guchar *composite_line_22_4a4 (int *weights, int n_x, int n_y,
		       guchar *dest, int dest_x, guchar *dest_end, int dest_channels, int dest_has_alpha,
		       guchar **src, int src_channels, gboolean src_has_alpha,
		       int x_init, int x_step, int src_width,
		       int check_size, guint32 color1, guint32 color2);
static guchar *composite_line (int *weights, int n_x, int n_y,
		guchar *dest, int dest_x, guchar *dest_end, int dest_channels, int dest_has_alpha,
		guchar **src, int src_channels, gboolean src_has_alpha,
		int x_init, int x_step, int src_width,
		int check_size, guint32 color1, guint32 color2);
static void composite_pixel (guchar *dest, int dest_x, int dest_channels, int dest_has_alpha,
		 int src_has_alpha, int check_size, guint32 color1, guint32 color2,
		 guint r, guint g, guint b, guint a);
static void bilinear_magnify_make_weights (PixopsFilterDimension *dim, double scale);
static void bilinear_box_make_weights (PixopsFilterDimension *dim, double scale);
static double linear_box_half (double b0, double b1);
static void pixops_composite_nearest (guchar        *dest_buf,
			  int            render_x0,
			  int            render_y0,
			  int            render_x1,
			  int            render_y1,
			  int            dest_rowstride,
			  int            dest_channels,
			  gboolean       dest_has_alpha,
			  const guchar  *src_buf,
			  int            src_width,
			  int            src_height,
			  int            src_rowstride,
			  int            src_channels,
			  gboolean       src_has_alpha,
			  double         scale_x,
			  double         scale_y,
			  int            overall_alpha);
#endif



//copy without macros.
static void
pixops_composite_nearest_debug (guchar        *dest_buf,
			  int            render_x0,
			  int            render_y0,
			  int            render_x1,
			  int            render_y1,
			  int            dest_rowstride,
			  int            dest_channels,
			  gboolean       dest_has_alpha,
			  const guchar  *src_buf,
			  int            src_width,
			  int            src_height,
			  int            src_rowstride,
			  int            src_channels,
			  gboolean       src_has_alpha,
			  double         scale_x,
			  double         scale_y,
			  int            overall_alpha,
			  guchar*        debug_buf_upper_bound)
{
	gboolean dbg_first = TRUE;

	int i;
	int x;
	int x_step = (1 << SCALE_SHIFT) / scale_x;
	int y_step = (1 << SCALE_SHIFT) / scale_y;
	int xmax, xstart, xstop, x_pos, y_pos;
	const guchar *p;
	unsigned int  a0;

	for (i = 0; i < (render_y1 - render_y0); i++) {
		const guchar *src;
		y_pos = ((i + render_y0) * y_step + y_step / 2) >> SCALE_SHIFT;
		y_pos = CLAMP (y_pos, 0, src_height - 1);
		src  = src_buf + y_pos * src_rowstride;
		guchar* dest = dest_buf + i * dest_rowstride;

		x = render_x0 * x_step + x_step / 2;

		//-----------

//#define INNER_LOOP(ASSIGN_PIXEL)
      xmax = x + (render_x1 - render_x0) * x_step;
      xstart = MIN (0, xmax);
      xstop = MIN (src_width << SCALE_SHIFT, xmax);
      p = src + (CLAMP (x, xstart, xstop) >> SCALE_SHIFT) * src_channels;
      while (x < xstart)
        {
		    //being ASSIGN_PIXEL:
			if (src_has_alpha) a0 = (p[3] * overall_alpha) / 0xff;
			else               a0 = overall_alpha;

			switch (a0) {
				case 0:
					break;
				case 255:   //   <----- we are here
					//dest[0] = p[0];
					dest[0] = MIN(dest[0] + p[0], 0xff);
					dest[1] = MIN(dest[1] + p[1], 0xff);
					dest[2] = MIN(dest[2] + p[2], 0xff);
					if (dest_has_alpha) dest[3] = 0xff;
					break;
				default:
					if (dest_has_alpha) {
						unsigned int w0 = 0xff * a0;
						unsigned int w1 = (0xff - a0) * dest[3];
						unsigned int w = w0 + w1;

						dest[0] = (w0 * p[0] + w1 * dest[0]) / w;
						dest[1] = (w0 * p[1] + w1 * dest[1]) / w;
						dest[2] = (w0 * p[2] + w1 * dest[2]) / w;
						dest[3] = w / 0xff;
					} else {
						unsigned int a1 = 0xff - a0;
						unsigned int tmp;

						tmp = a0 * p[0] + a1 * dest[0] + 0x80;
						dest[0] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[1] + a1 * dest[1] + 0x80;
						dest[1] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[2] + a1 * dest[2] + 0x80;
						dest[2] = (tmp + (tmp >> 8)) >> 8;
					}
					break;
			}
		    //end ASSIGN_PIXEL:
          dest += dest_channels;
          x += x_step;
        }
      while (x < xstop)
        {
          p = src + (x >> SCALE_SHIFT) * src_channels;
		    //being ASSIGN_PIXEL:
			if (src_has_alpha) a0 = (p[3] * overall_alpha) / 0xff;
			else               a0 = overall_alpha;

			switch (a0) {
				case 0:
					break;
				case 255:   //   <----- we are here
					//dest[0] = p[0];
					if(dbg_first) printf("  dest=%p p=%p\n", dest, p);
if(dest > debug_buf_upper_bound){ printf("***** buffer overrun?\n"); return; }
					dest[0] = MIN(dest[0] + p[0], 0xff);
					dest[1] = MIN(dest[1] + p[1], 0xff);
					dest[2] = MIN(dest[2] + p[2], 0xff);
					if (dest_has_alpha) dest[3] = 0xff;

					dbg_first = FALSE;
					break;
				default:
					if (dest_has_alpha) {
						unsigned int w0 = 0xff * a0;
						unsigned int w1 = (0xff - a0) * dest[3];
						unsigned int w = w0 + w1;

						dest[0] = (w0 * p[0] + w1 * dest[0]) / w;
						dest[1] = (w0 * p[1] + w1 * dest[1]) / w;
						dest[2] = (w0 * p[2] + w1 * dest[2]) / w;
						dest[3] = w / 0xff;
					} else {
						unsigned int a1 = 0xff - a0;
						unsigned int tmp;

						tmp = a0 * p[0] + a1 * dest[0] + 0x80;
						dest[0] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[1] + a1 * dest[1] + 0x80;
						dest[1] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[2] + a1 * dest[2] + 0x80;
						dest[2] = (tmp + (tmp >> 8)) >> 8;
					}
					break;
			}
		    //end ASSIGN_PIXEL:
          dest += dest_channels;
          x += x_step;
        }
      x_pos = x >> SCALE_SHIFT;
      p = src + CLAMP (x_pos, 0, src_width - 1) * src_channels;
      while (x < xmax)
        {
		    //being ASSIGN_PIXEL:
			if (src_has_alpha) a0 = (p[3] * overall_alpha) / 0xff;
			else               a0 = overall_alpha;

			switch (a0) {
				case 0:
					break;
				case 255:   //   <----- we are here
					//dest[0] = p[0];
					dest[0] = MIN(dest[0] + p[0], 0xff);
					dest[1] = MIN(dest[1] + p[1], 0xff);
					dest[2] = MIN(dest[2] + p[2], 0xff);
					if (dest_has_alpha) dest[3] = 0xff;
					break;
				default:
					if (dest_has_alpha) {
						unsigned int w0 = 0xff * a0;
						unsigned int w1 = (0xff - a0) * dest[3];
						unsigned int w = w0 + w1;

						dest[0] = (w0 * p[0] + w1 * dest[0]) / w;
						dest[1] = (w0 * p[1] + w1 * dest[1]) / w;
						dest[2] = (w0 * p[2] + w1 * dest[2]) / w;
						dest[3] = w / 0xff;
					} else {
						unsigned int a1 = 0xff - a0;
						unsigned int tmp;

						tmp = a0 * p[0] + a1 * dest[0] + 0x80;
						dest[0] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[1] + a1 * dest[1] + 0x80;
						dest[1] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[2] + a1 * dest[2] + 0x80;
						dest[2] = (tmp + (tmp >> 8)) >> 8;
					}
					break;
			}
		    //end ASSIGN_PIXEL:
          dest += dest_channels;
          x += x_step;
        }

		//-----------
#if 0
		    //being ASSIGN_PIXEL:
			if (src_has_alpha) a0 = (p[3] * overall_alpha) / 0xff;
			else               a0 = overall_alpha;

			switch (a0) {
				case 0:
					break;
				case 255:   //   <----- we are here
					//dest[0] = p[0];
					dest[0] = MIN(dest[0] + p[0], 0xff);
					dest[1] = MIN(dest[1] + p[1], 0xff);
					dest[2] = MIN(dest[2] + p[2], 0xff);
					if (dest_has_alpha) dest[3] = 0xff;
					break;
				default:
					if (dest_has_alpha) {
						unsigned int w0 = 0xff * a0;
						unsigned int w1 = (0xff - a0) * dest[3];
						unsigned int w = w0 + w1;

						dest[0] = (w0 * p[0] + w1 * dest[0]) / w;
						dest[1] = (w0 * p[1] + w1 * dest[1]) / w;
						dest[2] = (w0 * p[2] + w1 * dest[2]) / w;
						dest[3] = w / 0xff;
					} else {
						unsigned int a1 = 0xff - a0;
						unsigned int tmp;

						tmp = a0 * p[0] + a1 * dest[0] + 0x80;
						dest[0] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[1] + a1 * dest[1] + 0x80;
						dest[1] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[2] + a1 * dest[2] + 0x80;
						dest[2] = (tmp + (tmp >> 8)) >> 8;
					}
					break;
			}
		    //end ASSIGN_PIXEL:
//		);	      
#endif
	}
}

/**
 * _pixops_composite:
 * @dest_buf: pointer to location to store result
 * @render_x0: x0 of region of scaled source to store into @dest_buf
 * @render_y0: y0 of region of scaled source to store into @dest_buf
 * @render_x1: x1 of region of scaled source to store into @dest_buf
 * @render_y1: y1 of region of scaled source to store into @dest_buf
 * @dest_rowstride: rowstride of @dest_buf
 * @dest_channels: number of channels in @dest_buf
 * @dest_has_alpha: whether @dest_buf has alpha
 * @src_buf: pointer to source pixels
 * @src_width: width of source (used for clipping)
 * @src_height: height of source (used for clipping)
 * @src_rowstride: rowstride of source
 * @src_channels: number of channels in @src_buf
 * @src_has_alpha: whether @src_buf has alpha
 * @scale_x: amount to scale source by in X direction
 * @scale_y: amount to scale source by in Y direction
 * @interp_type: type of enumeration
 * @overall_alpha: overall alpha factor to multiply source by
 * 
 * Scale source buffer by scale_x / scale_y, then composite a given rectangle
 * of the result into the destination buffer.
 **/
void
_pixops_composite (guchar* dest_buf,
		   int            render_x0,
		   int            render_y0,
		   int            render_x1,
		   int            render_y1,
		   int            dest_rowstride,
		   int            dest_channels,
		   gboolean       dest_has_alpha,
		   const guchar  *src_buf,
		   int            src_width,
		   int            src_height,
		   int            src_rowstride,
		   int            src_channels,
		   gboolean       src_has_alpha,
		   guchar*        debug_buf_upper_boound)
{
  double scale_x=1.0, scale_y=1.0;
  int    overall_alpha = 0xff;
  PixopsInterpType interp_type = PIXOPS_INTERP_BILINEAR;

  //PixopsFilter filter;
  //PixopsLineFunc line_func;
  
#ifdef USE_MMX
  gboolean found_mmx = _pixops_have_mmx ();
#endif

  g_return_if_fail (!(dest_channels == 3 && dest_has_alpha));
  g_return_if_fail (!(src_channels == 3 && src_has_alpha));

  if (scale_x == 0 || scale_y == 0)
    return;

  // TODO do we need _pixops_scale ?
  /*
  if (!src_has_alpha && overall_alpha == 255)
    {
      _pixops_scale (dest_buf, render_x0, render_y0, render_x1, render_y1,
		     dest_rowstride, dest_channels, dest_has_alpha,
		     src_buf, src_width, src_height, src_rowstride, src_channels,
		     src_has_alpha, scale_x, scale_y, interp_type);
      return;
    }
  */

  if (1 || interp_type == PIXOPS_INTERP_NEAREST)
    {
      printf("%s(): src=%i dest=%i\n", __func__, src_width, render_x1 - render_x0);
#ifdef REAL
      pixops_composite_nearest (dest_buf, render_x0, render_y0, render_x1, render_y1,
				dest_rowstride, dest_channels, dest_has_alpha,
				src_buf, src_width, src_height, src_rowstride, src_channels,
				src_has_alpha, scale_x, scale_y, overall_alpha);
#else
      pixops_composite_nearest_debug (dest_buf, render_x0, render_y0, render_x1, render_y1,
				dest_rowstride, dest_channels, dest_has_alpha,
				src_buf, src_width, src_height, src_rowstride, src_channels,
				src_has_alpha, scale_x, scale_y, overall_alpha,
		        debug_buf_upper_boound);
#endif
      return;
    }
  #if 0
  filter.overall_alpha = overall_alpha / 255.;
  make_weights (&filter, interp_type, scale_x, scale_y);

  if (filter.x.n == 2 && filter.y.n == 2 &&
      dest_channels == 4 && src_channels == 4 && src_has_alpha && !dest_has_alpha)
    {
#ifdef USE_MMX
      if (found_mmx)
	line_func = composite_line_22_4a4_mmx_stub;
      else
#endif	
	line_func = composite_line_22_4a4;
    }
  else
    line_func = composite_line;
  
  pixops_process (dest_buf, render_x0, render_y0, render_x1, render_y1,
		  dest_rowstride, dest_channels, dest_has_alpha,
		  src_buf, src_width, src_height, src_rowstride, src_channels,
		  src_has_alpha, 0, 0, 0, 0, 0, 
		  &filter, line_func, composite_pixel);

  g_free (filter.x.weights);
  g_free (filter.y.weights);
  #endif
}


#if 0
static int
get_check_shift (int check_size)
{
  int check_shift = 0;
  g_return_val_if_fail (check_size >= 0, 4);

  while (!(check_size & 1))
    {
      check_shift++;
      check_size >>= 1;
    }

  return check_shift;
}


static void
pixops_scale_nearest (guchar        *dest_buf,
		      int            render_x0,
		      int            render_y0,
		      int            render_x1,
		      int            render_y1,
		      int            dest_rowstride,
		      int            dest_channels,
		      gboolean       dest_has_alpha,
		      const guchar  *src_buf,
		      int            src_width,
		      int            src_height,
		      int            src_rowstride,
		      int            src_channels,
		      gboolean       src_has_alpha,
		      double         scale_x,
		      double         scale_y)
{
  int i;
  int x;
  int x_step = (1 << SCALE_SHIFT) / scale_x;
  int y_step = (1 << SCALE_SHIFT) / scale_y;
  int xmax, xstart, xstop, x_pos, y_pos;
  const guchar *p;
#endif

#define INNER_LOOP(SRC_CHANNELS,DEST_CHANNELS,ASSIGN_PIXEL)     \
      xmax = x + (render_x1 - render_x0) * x_step;              \
      xstart = MIN (0, xmax);                                   \
      xstop = MIN (src_width << SCALE_SHIFT, xmax);             \
      p = src + (CLAMP (x, xstart, xstop) >> SCALE_SHIFT) * SRC_CHANNELS; \
      while (x < xstart)                                        \
        {                                                       \
          ASSIGN_PIXEL;                                         \
          dest += DEST_CHANNELS;                                \
          x += x_step;                                          \
        }                                                       \
      while (x < xstop)                                         \
        {                                                       \
          p = src + (x >> SCALE_SHIFT) * SRC_CHANNELS;          \
          ASSIGN_PIXEL;                                         \
          dest += DEST_CHANNELS;                                \
          x += x_step;                                          \
        }                                                       \
      x_pos = x >> SCALE_SHIFT;                                 \
      p = src + CLAMP (x_pos, 0, src_width - 1) * SRC_CHANNELS; \
      while (x < xmax)                                          \
        {                                                       \
          ASSIGN_PIXEL;                                         \
          dest += DEST_CHANNELS;                                \
          x += x_step;                                          \
        }

#if 0
  for (i = 0; i < (render_y1 - render_y0); i++)
    {
      const guchar *src;
      guchar       *dest;
      y_pos = ((i + render_y0) * y_step + y_step / 2) >> SCALE_SHIFT;
      y_pos = CLAMP (y_pos, 0, src_height - 1);
      src  = src_buf + y_pos * src_rowstride;
      dest = dest_buf + i * dest_rowstride;

      x = render_x0 * x_step + x_step / 2;

      if (src_channels == 3)
	{
	  if (dest_channels == 3)
	    {
	      INNER_LOOP (3, 3, dest[0]=p[0];dest[1]=p[1];dest[2]=p[2]);
	    }
	  else
	    {
	      INNER_LOOP (3, 4, dest[0]=p[0];dest[1]=p[1];dest[2]=p[2];dest[3]=0xff);
	    }
	}
      else if (src_channels == 4)
	{
	  if (dest_channels == 3)
	    {
	      INNER_LOOP (4, 3, dest[0]=p[0];dest[1]=p[1];dest[2]=p[2]);
	    }
	  else
	    {
	      guint32 *p32;
	      INNER_LOOP(4, 4, p32=(guint32*)dest;*p32=*((guint32*)p));
	    }
	}
    }
}

static void 
correct_total (int    *weights, 
               int    n_x, 
               int    n_y,
               int    total, 
               double overall_alpha)
{
  int correction = (int)(0.5 + 65536 * overall_alpha) - total;
  int remaining, c, d, i;
  
  if (correction != 0)
    {
      remaining = correction;
      for (d = 1, c = correction; c != 0 && remaining != 0; d++, c = correction / d) 
	for (i = n_x * n_y - 1; i >= 0 && c != 0 && remaining != 0; i--) 
	  if (*(weights + i) + c >= 0) 
	    {
	      *(weights + i) += c;
	      remaining -= c;
	      if ((0 < remaining && remaining < c) ||
		  (0 > remaining && remaining > c))
		c = remaining;
	    }
    }
}

static void
pixops_process (guchar        *dest_buf,
		int             render_x0,
		int             render_y0,
		int             render_x1,
		int             render_y1,
		int             dest_rowstride,
		int             dest_channels,
		gboolean        dest_has_alpha,
		const guchar   *src_buf,
		int             src_width,
		int             src_height,
		int             src_rowstride,
		int             src_channels,
		gboolean        src_has_alpha,
		//double          scale_x,
		//double          scale_y,
		int             check_x,
		int             check_y,
		int             check_size,
		guint32         color1,
		guint32         color2,
		PixopsFilter   *filter,
		PixopsLineFunc  line_func,
		PixopsPixelFunc pixel_func)
{
  double scale_x = 1.0, scale_y = 1.0;
  int i, j;
  int x, y;			/* X and Y position in source (fixed_point) */
  
  guchar **line_bufs = g_new (guchar *, filter->y.n);
  int *filter_weights = make_filter_table (filter);

  int x_step = (1 << SCALE_SHIFT) / scale_x; /* X step in source (fixed point) */
  int y_step = (1 << SCALE_SHIFT) / scale_y; /* Y step in source (fixed point) */

  int check_shift = check_size ? get_check_shift (check_size) : 0;

  int scaled_x_offset = floor (filter->x.offset * (1 << SCALE_SHIFT));

  /* Compute the index where we run off the end of the source buffer. The furthest
   * source pixel we access at index i is:
   *
   *  ((render_x0 + i) * x_step + scaled_x_offset) >> SCALE_SHIFT + filter->x.n - 1
   *
   * So, run_end_index is the smallest i for which this pixel is src_width, i.e, for which:
   *
   *  (i + render_x0) * x_step >= ((src_width - filter->x.n + 1) << SCALE_SHIFT) - scaled_x_offset
   *
   */
#define MYDIV(a,b) ((a) > 0 ? (a) / (b) : ((a) - (b) + 1) / (b))    /* Division so that -1/5 = -1 */
  
  int run_end_x = (((src_width - filter->x.n + 1) << SCALE_SHIFT) - scaled_x_offset);
  int run_end_index = MYDIV (run_end_x + x_step - 1, x_step) - render_x0;
  run_end_index = MIN (run_end_index, render_x1 - render_x0);

  y = render_y0 * y_step + floor (filter->y.offset * (1 << SCALE_SHIFT));
  for (i = 0; i < (render_y1 - render_y0); i++)
    {
      int dest_x;
      int y_start = y >> SCALE_SHIFT;
      int x_start;
      int *run_weights = filter_weights +
                         ((y >> (SCALE_SHIFT - SUBSAMPLE_BITS)) & SUBSAMPLE_MASK) *
                         filter->x.n * filter->y.n * SUBSAMPLE;
      guchar *new_outbuf;
      guint32 tcolor1, tcolor2;
      
      guchar *outbuf = dest_buf + dest_rowstride * i;
      guchar *outbuf_end = outbuf + dest_channels * (render_x1 - render_x0);

      if (((i + check_y) >> check_shift) & 1)
	{
	  tcolor1 = color2;
	  tcolor2 = color1;
	}
      else
	{
	  tcolor1 = color1;
	  tcolor2 = color2;
	}

      for (j=0; j<filter->y.n; j++)
	{
	  if (y_start <  0)
	    line_bufs[j] = (guchar *)src_buf;
	  else if (y_start < src_height)
	    line_bufs[j] = (guchar *)src_buf + src_rowstride * y_start;
	  else
	    line_bufs[j] = (guchar *)src_buf + src_rowstride * (src_height - 1);

	  y_start++;
	}

      dest_x = check_x;
      x = render_x0 * x_step + scaled_x_offset;
      x_start = x >> SCALE_SHIFT;

      while (x_start < 0 && outbuf < outbuf_end)
	{
	  process_pixel (run_weights + ((x >> (SCALE_SHIFT - SUBSAMPLE_BITS)) & SUBSAMPLE_MASK) * (filter->x.n * filter->y.n), filter->x.n, filter->y.n,
			 outbuf, dest_x, dest_channels, dest_has_alpha,
			 line_bufs, src_channels, src_has_alpha,
			 x >> SCALE_SHIFT, src_width,
			 check_size, tcolor1, tcolor2, pixel_func);
	  
	  x += x_step;
	  x_start = x >> SCALE_SHIFT;
	  dest_x++;
	  outbuf += dest_channels;
	}

      new_outbuf = (*line_func) (run_weights, filter->x.n, filter->y.n,
				 outbuf, dest_x,
				 dest_buf + dest_rowstride * i + run_end_index * dest_channels,
				 dest_channels, dest_has_alpha,
				 line_bufs, src_channels, src_has_alpha,
				 x, x_step, src_width, check_size, tcolor1, tcolor2);

      dest_x += (new_outbuf - outbuf) / dest_channels;

      x = (dest_x - check_x + render_x0) * x_step + scaled_x_offset;
      outbuf = new_outbuf;

      while (outbuf < outbuf_end)
	{
	  process_pixel (run_weights + ((x >> (SCALE_SHIFT - SUBSAMPLE_BITS)) & SUBSAMPLE_MASK) * (filter->x.n * filter->y.n), filter->x.n, filter->y.n,
			 outbuf, dest_x, dest_channels, dest_has_alpha,
			 line_bufs, src_channels, src_has_alpha,
			 x >> SCALE_SHIFT, src_width,
			 check_size, tcolor1, tcolor2, pixel_func);
	  
	  x += x_step;
	  dest_x++;
	  outbuf += dest_channels;
	}

      y += y_step;
    }

  g_free (line_bufs);
  g_free (filter_weights);
}


static void
process_pixel (int *weights, int n_x, int n_y,
	       guchar *dest, int dest_x, int dest_channels, int dest_has_alpha,
	       guchar **src, int src_channels, gboolean src_has_alpha,
	       int x_start, int src_width,
	       int check_size, guint32 color1, guint32 color2,
	       PixopsPixelFunc pixel_func)
{
  unsigned int r = 0, g = 0, b = 0, a = 0;
  int i, j;
  
  for (i=0; i<n_y; i++)
    {
      int *line_weights  = weights + n_x * i;

      for (j=0; j<n_x; j++)
	{
	  unsigned int ta;
	  guchar *q;

	  if (x_start + j < 0)
	    q = src[i];
	  else if (x_start + j < src_width)
	    q = src[i] + (x_start + j) * src_channels;
	  else
	    q = src[i] + (src_width - 1) * src_channels;

	  if (src_has_alpha)
	    ta = q[3] * line_weights[j];
	  else
	    ta = 0xff * line_weights[j];

	  r += ta * q[0];
	  g += ta * q[1];
	  b += ta * q[2];
	  a += ta;
	}
    }

  (*pixel_func) (dest, dest_x, dest_channels, dest_has_alpha, src_has_alpha, check_size, color1, color2, r, g, b, a);
}


static int *
make_filter_table (PixopsFilter *filter)
{
  int i_offset, j_offset;
  int n_x = filter->x.n;
  int n_y = filter->y.n;
  int *weights = g_new (int, SUBSAMPLE * SUBSAMPLE * n_x * n_y);

  for (i_offset=0; i_offset < SUBSAMPLE; i_offset++)
    for (j_offset=0; j_offset < SUBSAMPLE; j_offset++)
      {
        double weight;
        int *pixel_weights = weights + ((i_offset*SUBSAMPLE) + j_offset) * n_x * n_y;
        int total = 0;
        int i, j;

        for (i=0; i < n_y; i++)
          for (j=0; j < n_x; j++)
            {
              weight = filter->x.weights[(j_offset * n_x) + j] *
                       filter->y.weights[(i_offset * n_y) + i] *
                       filter->overall_alpha * 65536 + 0.5;

              total += (int)weight;

              *(pixel_weights + n_x * i + j) = weight;
            }

        correct_total (pixel_weights, n_x, n_y, total, filter->overall_alpha);
      }

  return weights;
}


/* Compute weights for reconstruction by replication followed by
 * sampling with a box filter
 */
static void
tile_make_weights (PixopsFilterDimension *dim, double scale)
{
  int n = ceil (1 / scale + 1);
  double *pixel_weights = g_new (double, SUBSAMPLE * n);
  int offset;
  int i;

  dim->n = n;
  dim->offset = 0;
  dim->weights = pixel_weights;

  for (offset = 0; offset < SUBSAMPLE; offset++)
    {
      double x = (double)offset / SUBSAMPLE;
      double a = x + 1 / scale;

      for (i = 0; i < n; i++)
        {
          if (i < x)
            {
              if (i + 1 > x)
                *(pixel_weights++)  = (MIN (i + 1, a) - x) * scale;
              else
                *(pixel_weights++) = 0;
            }
          else
            {
              if (a > i)
                *(pixel_weights++)  = (MIN (i + 1, a) - i) * scale;
              else
                *(pixel_weights++) = 0;
            }
       }
    }
}

static void
make_weights (PixopsFilter     *filter,
	      PixopsInterpType  interp_type,	      
	      double            scale_x,
	      double            scale_y)
{
  switch (interp_type)
    {
    case PIXOPS_INTERP_NEAREST:
      g_assert_not_reached ();
      break;

    case PIXOPS_INTERP_TILES:
      tile_make_weights (&filter->x, scale_x);
      tile_make_weights (&filter->y, scale_y);
      break;
      
    case PIXOPS_INTERP_BILINEAR:
      bilinear_magnify_make_weights (&filter->x, scale_x);
      bilinear_magnify_make_weights (&filter->y, scale_y);
      break;
      
    case PIXOPS_INTERP_HYPER:
      bilinear_box_make_weights (&filter->x, scale_x);
      bilinear_box_make_weights (&filter->y, scale_y);
      break;
    }
}

static guchar *
composite_line_22_4a4 (int *weights, int n_x, int n_y,
		       guchar *dest, int dest_x, guchar *dest_end, int dest_channels, int dest_has_alpha,
		       guchar **src, int src_channels, gboolean src_has_alpha,
		       int x_init, int x_step, int src_width,
		       int check_size, guint32 color1, guint32 color2)
{
  int x = x_init;
  guchar *src0 = src[0];
  guchar *src1 = src[1];

  g_return_val_if_fail (src_channels != 3, dest);
  g_return_val_if_fail (src_has_alpha, dest);
  
  while (dest < dest_end)
    {
      int x_scaled = x >> SCALE_SHIFT;
      unsigned int r, g, b, a, ta;
      int *pixel_weights;
      guchar *q0, *q1;
      int w1, w2, w3, w4;
      
      q0 = src0 + x_scaled * 4;
      q1 = src1 + x_scaled * 4;
      
      pixel_weights = (int *)((char *)weights + ((x >> (SCALE_SHIFT - SUBSAMPLE_BITS - 4)) & (SUBSAMPLE_MASK << 4)));
      
      w1 = pixel_weights[0];
      w2 = pixel_weights[1];
      w3 = pixel_weights[2];
      w4 = pixel_weights[3];

      a = w1 * q0[3];
      r = a * q0[0];
      g = a * q0[1];
      b = a * q0[2];

      ta = w2 * q0[7];
      r += ta * q0[4];
      g += ta * q0[5];
      b += ta * q0[6];
      a += ta;

      ta = w3 * q1[3];
      r += ta * q1[0];
      g += ta * q1[1];
      b += ta * q1[2];
      a += ta;

      ta = w4 * q1[7];
      r += ta * q1[4];
      g += ta * q1[5];
      b += ta * q1[6];
      a += ta;

      dest[0] = ((0xff0000 - a) * dest[0] + r) >> 24;
      dest[1] = ((0xff0000 - a) * dest[1] + g) >> 24;
      dest[2] = ((0xff0000 - a) * dest[2] + b) >> 24;
      dest[3] = a >> 16;
      
      dest += 4;
      x += x_step;
    }

  return dest;
}


static guchar *
composite_line (int *weights, int n_x, int n_y,
		guchar *dest, int dest_x, guchar *dest_end, int dest_channels, int dest_has_alpha,
		guchar **src, int src_channels, gboolean src_has_alpha,
		int x_init, int x_step, int src_width,
		int check_size, guint32 color1, guint32 color2)
{
  int x = x_init;
  int i, j;

  while (dest < dest_end)
    {
      int x_scaled = x >> SCALE_SHIFT;
      unsigned int r = 0, g = 0, b = 0, a = 0;
      int *pixel_weights;
      
      pixel_weights = weights + ((x >> (SCALE_SHIFT - SUBSAMPLE_BITS)) & SUBSAMPLE_MASK) * n_x * n_y;
      
      for (i=0; i<n_y; i++)
	{
	  guchar *q = src[i] + x_scaled * src_channels;
	  int *line_weights = pixel_weights + n_x * i;
	  
	  for (j=0; j<n_x; j++)
	    {
	      unsigned int ta;

	      if (src_has_alpha)
		ta = q[3] * line_weights[j];
	      else
		ta = 0xff * line_weights[j];
	      
	      r += ta * q[0];
	      g += ta * q[1];
	      b += ta * q[2];
	      a += ta;

	      q += src_channels;
	    }
	}

      if (dest_has_alpha)
	{
	  unsigned int w0 = a - (a >> 8);
	  unsigned int w1 = ((0xff0000 - a) >> 8) * dest[3];
	  unsigned int w = w0 + w1;

	  if (w != 0)
	    {
	      dest[0] = (r - (r >> 8) + w1 * dest[0]) / w;
	      dest[1] = (g - (g >> 8) + w1 * dest[1]) / w;
	      dest[2] = (b - (b >> 8) + w1 * dest[2]) / w;
	      dest[3] = w / 0xff00;
	    }
	  else
	    {
	      dest[0] = 0;
	      dest[1] = 0;
	      dest[2] = 0;
	      dest[3] = 0;
	    }
	}
      else
	{
	  dest[0] = (r + (0xff0000 - a) * dest[0]) / 0xff0000;
	  dest[1] = (g + (0xff0000 - a) * dest[1]) / 0xff0000;
	  dest[2] = (b + (0xff0000 - a) * dest[2]) / 0xff0000;
	}
      
      dest += dest_channels;
      x += x_step;
    }

  return dest;
}


static void
composite_pixel (guchar *dest, int dest_x, int dest_channels, int dest_has_alpha,
		 int src_has_alpha, int check_size, guint32 color1, guint32 color2,
		 guint r, guint g, guint b, guint a)
{
  if (dest_has_alpha)
    {
      unsigned int w0 = a - (a >> 8);
      unsigned int w1 = ((0xff0000 - a) >> 8) * dest[3];
      unsigned int w = w0 + w1;
      
      if (w != 0)
	{
	  dest[0] = (r - (r >> 8) + w1 * dest[0]) / w;
	  dest[1] = (g - (g >> 8) + w1 * dest[1]) / w;
	  dest[2] = (b - (b >> 8) + w1 * dest[2]) / w;
	  dest[3] = w / 0xff00;
	}
      else
	{
	  dest[0] = 0;
	  dest[1] = 0;
	  dest[2] = 0;
	  dest[3] = 0;
	}
    }
  else
    {
      dest[0] = (r + (0xff0000 - a) * dest[0]) / 0xff0000;
      dest[1] = (g + (0xff0000 - a) * dest[1]) / 0xff0000;
      dest[2] = (b + (0xff0000 - a) * dest[2]) / 0xff0000;
    }
}

/* Compute weights for a filter that, for minification
 * is the same as 'tiles', and for magnification, is bilinear
 * reconstruction followed by a sampling with a delta function.
 */
static void
bilinear_magnify_make_weights (PixopsFilterDimension *dim, double scale)
{
  double *pixel_weights;
  int n;
  int offset;
  int i;

  if (scale > 1.0)            /* Linear */
    {
      n = 2;
      dim->offset = 0.5 * (1 / scale - 1);
    }
  else                          /* Tile */
    {
      n = ceil (1.0 + 1.0 / scale);
      dim->offset = 0.0;
    }

  dim->n = n;
  dim->weights = g_new (double, SUBSAMPLE * n);

  pixel_weights = dim->weights;

  for (offset=0; offset < SUBSAMPLE; offset++)
    {
      double x = (double)offset / SUBSAMPLE;

      if (scale > 1.0)      /* Linear */
        {
          for (i = 0; i < n; i++)
            *(pixel_weights++) = (((i == 0) ? (1 - x) : x) / scale) * scale;
        }
      else                  /* Tile */
        {
          double a = x + 1 / scale;

          /*           x
           * ---------|--.-|----|--.-|-------  SRC
           * ------------|---------|---------  DEST
           */
          for (i = 0; i < n; i++)
            {
              if (i < x)
                {
                  if (i + 1 > x)
                    *(pixel_weights++) = (MIN (i + 1, a) - x) * scale;
                  else
                    *(pixel_weights++) = 0;
                }
              else
                {
                  if (a > i)
                    *(pixel_weights++) = (MIN (i + 1, a) - i) * scale;
                  else
                    *(pixel_weights++) = 0;
                }
            }
        }
    }
}

/* Compute weights for reconstructing with bilinear
 * interpolation, then sampling with a box filter
 */
static void
bilinear_box_make_weights (PixopsFilterDimension *dim, double scale)
{
  int n = ceil (1/scale + 3.0);
  double *pixel_weights = g_new (double, SUBSAMPLE * n);
  double w;
  int offset, i;

  dim->offset = -1.0;
  dim->n = n;
  dim->weights = pixel_weights;

  for (offset = 0; offset < SUBSAMPLE; offset++)
    {
      double x = (double)offset / SUBSAMPLE;
      double a = x + 1 / scale;

      for (i = 0; i < n; i++)
        {
          w  = linear_box_half (0.5 + i - a, 0.5 + i - x);
          w += linear_box_half (1.5 + x - i, 1.5 + a - i);
      
          *(pixel_weights++) = w * scale;
        }
    }
}
#endif //0

#if 0
static void
pixops_composite_nearest (guchar        *dest_buf,
			  int            render_x0,
			  int            render_y0,
			  int            render_x1,
			  int            render_y1,
			  int            dest_rowstride,
			  int            dest_channels,
			  gboolean       dest_has_alpha,
			  const guchar  *src_buf,
			  int            src_width,
			  int            src_height,
			  int            src_rowstride,
			  int            src_channels,
			  gboolean       src_has_alpha,
			  double         scale_x,
			  double         scale_y,
			  int            overall_alpha)
{
	int i;
	int x;
	int x_step = (1 << SCALE_SHIFT) / scale_x;
	int y_step = (1 << SCALE_SHIFT) / scale_y;
	int xmax, xstart, xstop, x_pos, y_pos;
	const guchar *p;
	unsigned int  a0;

	for (i = 0; i < (render_y1 - render_y0); i++) {
		const guchar *src;
		guchar       *dest;
		y_pos = ((i + render_y0) * y_step + y_step / 2) >> SCALE_SHIFT;
		y_pos = CLAMP (y_pos, 0, src_height - 1);
		src  = src_buf + y_pos * src_rowstride;
		dest = dest_buf + i * dest_rowstride;

		x = render_x0 * x_step + x_step / 2;

		INNER_LOOP(src_channels, dest_channels,
			if (src_has_alpha) a0 = (p[3] * overall_alpha) / 0xff;
			else               a0 = overall_alpha;

			switch (a0) {
				case 0:
					break;
				case 255:   //   <----- we are here
					//dest[0] = p[0];
					dest[0] = MIN(dest[0] + p[0], 0xff);
					dest[1] = MIN(dest[1] + p[1], 0xff);
					dest[2] = MIN(dest[2] + p[2], 0xff);
					if (dest_has_alpha) dest[3] = 0xff;
					break;
				default:
					if (dest_has_alpha) {
						unsigned int w0 = 0xff * a0;
						unsigned int w1 = (0xff - a0) * dest[3];
						unsigned int w = w0 + w1;

						dest[0] = (w0 * p[0] + w1 * dest[0]) / w;
						dest[1] = (w0 * p[1] + w1 * dest[1]) / w;
						dest[2] = (w0 * p[2] + w1 * dest[2]) / w;
						dest[3] = w / 0xff;
					} else {
						unsigned int a1 = 0xff - a0;
						unsigned int tmp;

						tmp = a0 * p[0] + a1 * dest[0] + 0x80;
						dest[0] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[1] + a1 * dest[1] + 0x80;
						dest[1] = (tmp + (tmp >> 8)) >> 8;
						tmp = a0 * p[2] + a1 * dest[2] + 0x80;
						dest[2] = (tmp + (tmp >> 8)) >> 8;
					}
					break;
			}
		);	      
	}
}
#endif

#if 0
/* Computes the integral from b0 to b1 of
 *
 * f(x) = x; 0 <= x < 1
 * f(x) = 0; otherwise
 *
 * We combine two of these to compute the convolution of
 * a box filter with a triangular spike.
 */
static double
linear_box_half (double b0, double b1)
{
  double a0, a1;
  double x0, x1;

  a0 = 0.;
  a1 = 1.;

  if (a0 < b0)
    {
      if (a1 > b0)
        {
          x0 = b0;
          x1 = MIN (a1, b1);
        }
      else
        return 0;
    }
  else
    {
      if (b1 > a0)
        {
          x0 = a0;
          x1 = MIN (a1, b1);
        }
      else
        return 0;
    }

  return 0.5 * (x1*x1 - x0*x0);
}
#endif //0

