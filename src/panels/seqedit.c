//----------------------------------------------------------------------------
//
//  This file is part of seq24.
//
//  seq24 is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  seq24 is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with seq24; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//-----------------------------------------------------------------------------

#define __seqedit_c__
#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <stdint.h>

//typedef void *sequence;

#include "seqedit.h" 
//#include "seqkeys.h" 
//#include "sequence.h"
//#include "midibus.h"
//#include "controllers.h"
//#include "options.h"

typedef  GtkWidget *seqdata;
typedef  GtkWidget *seqevent;
typedef  GtkWidget *seqkeys;

#include "pixmaps/seq24/play.xpm"
#include "pixmaps/seq24/rec.xpm"
#include "pixmaps/seq24/thru.xpm"
//#include "/root/2/src/seq24-0.6.0/src/bus.xpm"
//#include "/root/2/src/seq24-0.6.0/src/midi.xpm"
//#include "/root/2/src/seq24-0.6.0/src/snap.xpm"
//#include "/root/2/src/seq24-0.6.0/src/zoom.xpm"
//#include "/root/2/src/seq24-0.6.0/src/length.xpm"
//#include "/root/2/src/seq24-0.6.0/src/scale.xpm"
//#include "/root/2/src/seq24-0.6.0/src/key.xpm"   
//#include "/root/2/src/seq24-0.6.0/src/down.xpm"
//#include "/root/2/src/seq24-0.6.0/src/note_length.xpm"
//#include "/root/2/src/seq24-0.6.0/src/undo.xpm"
//#include "/root/2/src/seq24-0.6.0/src/menu_empty.xpm"
//#include "/root/2/src/seq24-0.6.0/src/menu_full.xpm"
//#include "/root/2/src/seq24-0.6.0/src/sequences.xpm"

//static void seqedit_destroy(AyyiPanel*);

//#include "widgets/seq24_keys.h"
           extern GtkWidget*  gtk_keys_new(Sequence* m_seq, GtkAdjustment *v_adj);
#include "widgets/seq24_roll.h"
extern GtkWidget* gtk_roll_new (
               perform *a_perf,
               struct _midi_sequence* a_seq, int a_zoom, int a_snap,
               seqdata *a_seqdata_wid,
               seqevent *a_seqevent_wid,
               seqkeys *a_seqkeys_wid,
               mainwid *a_mainwid,
               int a_pos,
               GtkAdjustment *a_hadjust,
               GtkAdjustment *a_vadjust
               );


#define c_num_keys  88
#define c_keyarea_x 40
#define c_ppqn 192 //FIXME

static GObject* ayyi_midi_win_construct (GType, guint n_construct_properties, GObjectConstructParam*);
static void     ayyi_midi_win_on_ready  (AyyiPanel*);

G_DEFINE_TYPE (AyyiMidiWin, ayyi_midi_win, AYYI_TYPE_PANEL)


void 
seqedit_menu_action_quantise( void )
{
  // connects to a menu item, tells the performance to launch the timer thread
}


static void
ayyi_midi_win_class_init (AyyiMidiWinClass *klass)
{
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);

    g_object_class->constructor = ayyi_midi_win_construct;

	panel->name         = "Midi";
	panel->has_link     = true;
	panel->has_follow   = true;
	panel->has_toolbar  = true;
	panel->accel        = GDK_F11;
	panel->default_size = (Pti){500, 500};
	panel->on_ready     = ayyi_midi_win_on_ready;
}


static GObject*
ayyi_midi_win_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_ > -1) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_midi_win_parent_class)->constructor(type, n_construct_properties, construct_param);

	MidiWindow* seqedit = (MidiWindow*)g_object;

	/*

	sequence:    -a 'sequence' object (list of events), from sequence.c
	performance: -a collection of several 'sequences'.
	mainwid:     -the 'piano roll' - a gtkdrawingarea.

	*/
	//sequence* a_seq     = NULL;
	//perform*  a_perf    = NULL;
	//mainwid*  a_mainwid = NULL;

	int a_pos = 0;

	seqedit->m_initial_zoom = 2;
	seqedit->m_initial_snap = c_ppqn / 4;
	seqedit->m_initial_note_length = c_ppqn / 4;
	seqedit->m_initial_scale = 0;
	seqedit->m_initial_key = 0;
	seqedit->m_initial_sequence = -1;

	//set the performance:
	//m_seq = a_seq;

	seqedit->m_zoom        = seqedit->m_initial_zoom;
	seqedit->m_snap        = seqedit->m_initial_snap;
	seqedit->m_note_length = seqedit->m_initial_note_length;
	seqedit->m_scale       = seqedit->m_initial_scale;
	seqedit->m_key         = seqedit->m_initial_key;
	seqedit->m_sequence    = seqedit->m_initial_sequence;

	//m_mainperf = a_perf;
	//m_mainwid = a_mainwid;
	seqedit->m_pos = a_pos;

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_MIDI_WIN);

	return g_object;
}


static void
ayyi_midi_win_init(AyyiMidiWin* object)
{
}


static void
ayyi_midi_win_on_ready (AyyiPanel* panel)
{
	MidiWindow* seqedit = (MidiWindow*)panel;

	if(seqedit->m_vadjust) return;

	//main vbox:
	//seqedit->m_vbox = gtk_vbox_new(FALSE, 2);     
	//gtk_widget_show(seqedit->m_vbox);
	//gtk_container_add(GTK_CONTAINER(win), seqedit->m_vbox);

	//m_seq->set_editing(true);

	//scroll bars:
	seqedit->m_vadjust = gtk_adjustment_new(55,0, c_num_keys,  1,1,1);
	seqedit->m_hadjust = gtk_adjustment_new(0, 0, 1,  1,1,1);
	seqedit->m_vscroll_new   =  gtk_vscrollbar_new(GTK_ADJUSTMENT(seqedit->m_vadjust));
	seqedit->m_hscroll_new   =  gtk_hscrollbar_new(GTK_ADJUSTMENT(seqedit->m_hadjust));
	gtk_widget_show(seqedit->m_vscroll_new);
	gtk_widget_show(seqedit->m_hscroll_new);

  //-----------------------------------------------

  //make some new objects:
  //----------------------
  
  seqedit->m_seqkeys_wid = (tseqkeys*)gtk_keys_new(seqedit->m_seq, GTK_ADJUSTMENT(seqedit->m_vadjust));
  gtk_widget_set_size_request(GTK_WIDGET(seqedit->m_seqkeys_wid), c_keyarea_x + 2, 20); //!!!!temp!!!!
  gtk_widget_show(GTK_WIDGET(seqedit->m_seqkeys_wid));
  

  /*
  m_seqtime_wid  = manage( new seqtime(  m_seq,
                                           m_zoom,
                                           m_hadjust ));
    
  m_seqdata_wid  = manage( new seqdata(  m_seq,
                                           m_zoom,
                                           m_hadjust));
    
  m_seqevent_wid = manage( new seqevent( m_seq,
                                           m_zoom,
                                           m_snap,
                                           m_seqdata_wid,
                                           m_hadjust));
  */
#define USE_SEQROLL
#ifdef USE_SEQROLL
  GtkWidget *m_seqroll_wid = gtk_roll_new(
                                NULL, //m_mainperf,
                                NULL, //m_seq,
                                1,    //         m_zoom,
                                0,    //         m_snap,
                                NULL, //         m_seqdata_wid,
                                NULL, //         m_seqevent_wid,
                                NULL, //         m_seqkeys_wid,
                                NULL, //         m_mainwid,
                                0,    //         m_pos,
                                GTK_ADJUSTMENT(seqedit->m_hadjust),
                                GTK_ADJUSTMENT(seqedit->m_vadjust)
                                );
  //gtk_widget_set_size_request(m_seqroll_wid, 200, 100); //temp!!!!!!!!
  //gtk_box_pack_start(GTK_BOX(seqedit->m_vbox), m_seqroll_wid, /*expand*/TRUE, TRUE, /*padding*/0);
  gtk_widget_show(m_seqroll_wid);
#endif  

    
  //menus:
  /*
  m_menubar   =  manage( new MenuBar());
  m_menu_tools = manage( new Menu() );
  m_menu_zoom =  manage( new Menu());
  m_menu_snap =   manage( new Menu());
  m_menu_note_length = manage( new Menu());
  m_menu_length = manage( new Menu());
  m_menu_bpm = manage( new Menu() );
  m_menu_bw = manage( new Menu() );

  m_menu_midich = manage( new Menu());
  m_menu_midibus = NULL;
  m_menu_sequences = NULL;


  m_menu_key = manage( new Menu());
  m_menu_scale = manage( new Menu());
  m_menu_data = NULL;
  */
   

  //create_menus(); 

  //tooltips
  //m_tooltips = manage( new Tooltips( ) );

 
  //init table, viewports and scroll bars:
  seqedit->m_table     = gtk_table_new(7, 4, FALSE); gtk_widget_show(seqedit->m_table);
  seqedit->m_hbox      = gtk_hbox_new(FALSE, 2);     gtk_widget_show(seqedit->m_hbox);
  //seqedit->m_hbox2     = gtk_hbox_new(FALSE, 2);     gtk_widget_show(seqedit->m_hbox2);
  //seqedit->m_hbox3     = gtk_hbox_new(FALSE, 2);     gtk_widget_show(seqedit->m_hbox3);
  GtkWidget *dhbox     = gtk_hbox_new(FALSE, 2);     gtk_widget_show(dhbox);

  //gtk_box_pack_start(GTK_BOX(seqedit->m_vbox), new, TRUE, TRUE, 0);

  //m_vbox->set_border_width( 2 );

  //seqedit->m_vscroll   =  gtk_vscrollbar_new(adj);
  //seqedit->m_hscroll   =  gtk_hscrollbar_new();

 
  //Gtk::Adjustment NullAdj(1,1,1);
    
  //m_keysview   =  manage( new Viewport( NullAdj,                      *m_vscroll->get_adjustment() ));
  //m_rollview   =  gtk_viewport_new(seqedit->m_hscroll->get_adjustment(), seqedit->m_vscroll->get_adjustment());
  //m_dataview   =  manage( new Viewport( *m_hscroll->get_adjustment(), NullAdj ));
  //m_timeview   =  manage( new Viewport( *m_hscroll->get_adjustment(), NullAdj ));
  //m_eventview  =  manage( new Viewport( *m_hscroll->get_adjustment(), NullAdj ));

    /* init keys view */
    //m_keysview->set_shadow_type( Gtk::SHADOW_NONE );
    //m_keysview->add( *m_seqkeys_wid );
    //gtk_widget_set_size_request(seqedit->m_keysview, c_keyarea_x + 2, 10 );
    //m_keysview->set_vadjustment( *(m_vscroll->get_adjustment()));

    //init the piano roll view:
    //m_rollview->set_shadow_type( Gtk::SHADOW_NONE );
    //m_rollview->add( *m_seqroll_wid );
    //gtk_widget_set_size_request(m_rollview, 30, 30 );
    //m_rollview->set_hadjustment( *(m_hscroll->get_adjustment()));
    //m_rollview->set_vadjustment( *(m_vscroll->get_adjustment()));

    /* init event view */
    //m_eventview->set_shadow_type( Gtk::SHADOW_NONE );
    //m_eventview->add( *m_seqevent_wid );
    //m_eventview->set_size_request( 10, c_eventarea_y + 1 );
    //m_eventview->set_hadjustment( *(m_hscroll->get_adjustment()));
  
    /* data */
    //m_dataview->set_shadow_type( Gtk::SHADOW_NONE );
    //m_dataview->add( *m_seqdata_wid );
    //m_dataview->set_size_request( 10, c_dataarea_y + 2 );
    //m_dataview->set_hadjustment( *(m_hscroll->get_adjustment()));

    /* time */
    //m_timeview->set_shadow_type( Gtk::SHADOW_NONE );
    //m_timeview->add( *m_seqtime_wid );
    //m_timeview->set_size_request( 10, c_timearea_y + 1 );
    //m_timeview->set_hadjustment( *(m_hscroll->get_adjustment()));

  // x y
  //fill table:
  gtk_table_attach(GTK_TABLE(seqedit->m_table), GTK_WIDGET(seqedit->m_seqkeys_wid),    0, 1, 1, 2, GTK_SHRINK, GTK_FILL, /*xpadding*/0, 2);

  //gtk_table_attach(seqedit->m_table, *m_seqtime_wid, 1, 2, 0, 1, GTK_FILL, GTK_SHRINK );
  gtk_table_attach(GTK_TABLE(seqedit->m_table), m_seqroll_wid , 1, 2, 1, 2,
   	                     GTK_FILL | GTK_SHRINK,  
    	                 GTK_FILL | GTK_SHRINK, 0,0);

  //m_table->attach( *m_seqevent_wid, 1, 2, 2, 3, Gtk::FILL, Gtk::SHRINK );
  //m_table->attach( *m_seqdata_wid, 1, 2, 3, 4, Gtk::FILL, Gtk::SHRINK );
  
  gtk_table_attach(GTK_TABLE(seqedit->m_table), dhbox,      
                                             1, 2, 4, 5, GTK_FILL | GTK_EXPAND, GTK_SHRINK, 0, 2 );
  
  gtk_table_attach(GTK_TABLE(seqedit->m_table), seqedit->m_vscroll_new, 2, 3, 1, 2, 
                                                GTK_SHRINK, GTK_FILL | GTK_EXPAND, 0,0);
  gtk_table_attach(GTK_TABLE(seqedit->m_table), seqedit->m_hscroll_new, 1, 2, 5, 6, 
                                                GTK_FILL | GTK_EXPAND, GTK_SHRINK, 0,0);
  
  // no expand, just fit the widgets
  // m_vbox->pack_start(*m_menubar, false, false, 0);
  //gtk_box_pack_start(GTK_BOX(seqedit->m_vbox), seqedit->m_hbox,  FALSE, FALSE, 0);
  /*
  m_vbox->pack_start(*m_hbox2, false, false, 0);
  m_vbox->pack_start(*m_hbox3, false, false, 0);

  // exapand, cause rollview expands
  */
  //gtk_box_pack_start(GTK_BOX(seqedit->m_vbox), seqedit->m_table, TRUE, TRUE, 0);
  panel_pack(seqedit->m_table, EXPAND_TRUE);

  //--------------------------------------------------------------------

  //buttons:

  //tools button
  //seqedit->m_button_tools = gtk_button_new_with_label(" T ");    //where is this packed?
  //gtk_widget_show(seqedit->m_button_tools);
  //m_button_tools->signal_clicked().connect( bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_tools ));

  //data button:
  seqedit->m_button_data = gtk_button_new_with_label(" Event ");
  gtk_widget_show(seqedit->m_button_data);
  //m_button_data->signal_clicked().connect( slot( *this, &seqedit::popup_event_menu));

  seqedit->m_entry_data = gtk_entry_new();
  gtk_widget_show(seqedit->m_entry_data);
  gtk_widget_set_size_request(seqedit->m_entry_data, 40, -1);
  gtk_entry_set_editable(GTK_ENTRY(seqedit->m_entry_data), FALSE);

  gtk_box_pack_start(GTK_BOX(dhbox), seqedit->m_button_data, FALSE, FALSE, 0); //4
  gtk_box_pack_start(GTK_BOX(dhbox), seqedit->m_entry_data, TRUE, TRUE, 0);

  //play, rec, thru:
  seqedit->m_toggle_play = gtk_toggle_button_new();
  gtk_widget_show(seqedit->m_toggle_play);
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_xpm_data((const char **)play_xpm);
  GtkWidget *image = gtk_image_new_from_pixbuf(pixbuf);
  gtk_container_add(GTK_CONTAINER(seqedit->m_toggle_play), image);
  gtk_box_pack_end(GTK_BOX(dhbox), seqedit->m_toggle_play,   FALSE, FALSE, 4);
  //m_toggle_play->set_active( m_seq->get_playing());
  //m_toggle_play->signal_clicked().connect( slot( *this, &seqedit::play_change_callback));
  //m_tooltips->set_tip( *m_toggle_play, "Sequence dumps data to midi bus." );
    
  seqedit->m_toggle_record = gtk_toggle_button_new();
  gtk_widget_show(seqedit->m_toggle_record);
  pixbuf = gdk_pixbuf_new_from_xpm_data((const char **)rec_xpm);
  image  = gtk_image_new_from_pixbuf(pixbuf);
  gtk_container_add(GTK_CONTAINER(seqedit->m_toggle_record), image);
  //m_toggle_record->set_active( m_seq->get_recording());
  //m_toggle_record->signal_clicked().connect( slot( *this, &seqedit::record_change_callback));
  //m_tooltips->set_tip( *m_toggle_record, "Records incoming midi data." );
  gtk_box_pack_end(GTK_BOX(dhbox), seqedit->m_toggle_record, FALSE, FALSE, 4);

  seqedit->m_toggle_thru = gtk_toggle_button_new();
  gtk_widget_show(seqedit->m_toggle_thru);
  pixbuf = gdk_pixbuf_new_from_xpm_data((const char **)thru_xpm);
  image  = gtk_image_new_from_pixbuf(pixbuf);
  gtk_container_add(GTK_CONTAINER(seqedit->m_toggle_thru), image);
  gtk_box_pack_end(GTK_BOX(dhbox), seqedit->m_toggle_thru,   FALSE, FALSE, 4);
  //m_toggle_thru->set_active( m_seq->get_thru());
  //m_toggle_thru->signal_clicked().connect( slot( *this, &seqedit::thru_change_callback));
  //m_tooltips->set_tip(*m_toggle_thru,"Incoming midi data passes thru to sequences midi bus and channel." );
  //--------------------------------------------------------------

  /*
  dhbox->pack_end( *(manage(new VSeparator( ))), false, false, 4);

  fill_top_bar();
  */


  gtk_widget_show_all((GtkWidget*)panel);

  //sets scroll bar to the middle:
  gfloat middle = ((GtkAdjustment*)(seqedit->m_vadjust))->upper / 3;
  gtk_adjustment_set_value(GTK_ADJUSTMENT(seqedit->m_vadjust), middle);

  //seqedit_set_zoom(seqedit->m_zoom);
  //seqedit_set_snap(seqedit->m_snap);
  //seqedit_set_note_length(seqedit->m_note_length);
  //seqedit_set_background_sequence(seqedit->m_sequence);


  //set_bpm(m_seq->get_bpm());
  //set_bw(m_seq->get_bw());    //bar width
  //set_measures(get_measures());

  //set_midi_channel(m_seq->get_midi_channel());
  //set_midi_bus(m_seq->get_midi_bus());
  //set_data_type(EVENT_NOTE_ON);

  //set_scale(m_scale);
  //set_key(m_key);
  //set_key(m_key);
}


void
seqedit_set_part(MidiWindow* panel, AMPart* part)
{
	//FIXME create a sequence from the GPart
	pwarn("FIXME");
}


void 
seqedit_create_menus(void)
{
  /*
    using namespace Menu_Helpers;

    char b[20];
    
    //zoom
    m_menu_zoom->items().push_back(MenuElem("1:1",  bind(slot(*this,&seqedit::set_zoom), 1 )));
    m_menu_zoom->items().push_back(MenuElem("1:2",  bind(slot(*this,&seqedit::set_zoom), 2 )));
    m_menu_zoom->items().push_back(MenuElem("1:4",  bind(slot(*this,&seqedit::set_zoom), 4 )));
    m_menu_zoom->items().push_back(MenuElem("1:8",  bind(slot(*this,&seqedit::set_zoom), 8 )));
    m_menu_zoom->items().push_back(MenuElem("1:16", bind(slot(*this,&seqedit::set_zoom), 16 )));
    m_menu_zoom->items().push_back(MenuElem("1:32", bind(slot(*this,&seqedit::set_zoom), 32 )));
      
    //note snap
    m_menu_snap->items().push_back(MenuElem("1",     bind(slot(*this,&seqedit::set_snap), c_ppqn * 4  )));
    m_menu_snap->items().push_back(MenuElem("1/2",   bind(slot(*this,&seqedit::set_snap), c_ppqn * 2  )));
    m_menu_snap->items().push_back(MenuElem("1/4",   bind(slot(*this,&seqedit::set_snap), c_ppqn * 1  )));
    m_menu_snap->items().push_back(MenuElem("1/8",   bind(slot(*this,&seqedit::set_snap), c_ppqn / 2  )));
    m_menu_snap->items().push_back(MenuElem("1/16",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 4  )));
    m_menu_snap->items().push_back(MenuElem("1/32",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 8  )));
    m_menu_snap->items().push_back(MenuElem("1/64",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 16 )));
    m_menu_snap->items().push_back(MenuElem("1/128", bind(slot(*this,&seqedit::set_snap), c_ppqn / 32 )));
    m_menu_snap->items().push_back(SeparatorElem());
    m_menu_snap->items().push_back(MenuElem("1/3",   bind(slot(*this,&seqedit::set_snap), c_ppqn * 4  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/6",   bind(slot(*this,&seqedit::set_snap), c_ppqn * 2  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/12",  bind(slot(*this,&seqedit::set_snap), c_ppqn * 1  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/24",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 2  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/48",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 4  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/96",  bind(slot(*this,&seqedit::set_snap), c_ppqn / 8  / 3 )));
    m_menu_snap->items().push_back(MenuElem("1/192", bind(slot(*this,&seqedit::set_snap), c_ppqn / 16 / 3 )));
    
    //note note_length
    m_menu_note_length->items().push_back(MenuElem("1",     bind(slot(*this,&seqedit::set_note_length), c_ppqn * 4  )));
    m_menu_note_length->items().push_back(MenuElem("1/2",   bind(slot(*this,&seqedit::set_note_length), c_ppqn * 2  )));
    m_menu_note_length->items().push_back(MenuElem("1/4",   bind(slot(*this,&seqedit::set_note_length), c_ppqn * 1  )));
    m_menu_note_length->items().push_back(MenuElem("1/8",   bind(slot(*this,&seqedit::set_note_length), c_ppqn / 2  )));
    m_menu_note_length->items().push_back(MenuElem("1/16",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 4  )));
    m_menu_note_length->items().push_back(MenuElem("1/32",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 8  )));
    m_menu_note_length->items().push_back(MenuElem("1/64",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 16 )));
    m_menu_note_length->items().push_back(MenuElem("1/128", bind(slot(*this,&seqedit::set_note_length), c_ppqn / 32 )));
    m_menu_note_length->items().push_back(SeparatorElem());
    m_menu_note_length->items().push_back(MenuElem("1/3",   bind(slot(*this,&seqedit::set_note_length), c_ppqn * 4  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/6",   bind(slot(*this,&seqedit::set_note_length), c_ppqn * 2  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/12",  bind(slot(*this,&seqedit::set_note_length), c_ppqn * 1  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/24",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 2  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/48",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 4  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/96",  bind(slot(*this,&seqedit::set_note_length), c_ppqn / 8  / 3 )));
    m_menu_note_length->items().push_back(MenuElem("1/192", bind(slot(*this,&seqedit::set_note_length), c_ppqn / 16 / 3 )));
    
    //Key
    m_menu_key->items().push_back(MenuElem( c_key_text[0],  bind(slot(*this,&seqedit::set_key), 0 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[1],  bind(slot(*this,&seqedit::set_key), 1 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[2],  bind(slot(*this,&seqedit::set_key), 2 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[3],  bind(slot(*this,&seqedit::set_key), 3 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[4],  bind(slot(*this,&seqedit::set_key), 4 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[5],  bind(slot(*this,&seqedit::set_key), 5 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[6],  bind(slot(*this,&seqedit::set_key), 6 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[7],  bind(slot(*this,&seqedit::set_key), 7 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[8],  bind(slot(*this,&seqedit::set_key), 8 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[9],  bind(slot(*this,&seqedit::set_key), 9 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[10], bind(slot(*this,&seqedit::set_key), 10 )));
    m_menu_key->items().push_back(MenuElem( c_key_text[11], bind(slot(*this,&seqedit::set_key), 11 )));
    
    //bw
    m_menu_bw->items().push_back(MenuElem("1", bind(slot(*this,&seqedit::set_bw), 1  )));
    m_menu_bw->items().push_back(MenuElem("2", bind(slot(*this,&seqedit::set_bw), 2  )));
    m_menu_bw->items().push_back(MenuElem("4", bind(slot(*this,&seqedit::set_bw), 4  )));
    m_menu_bw->items().push_back(MenuElem("8", bind(slot(*this,&seqedit::set_bw), 8  )));
    m_menu_bw->items().push_back(MenuElem("16", bind(slot(*this,&seqedit::set_bw), 16 )));
    
    
    //music scale
    m_menu_scale->items().push_back(MenuElem(c_scales_text[0], bind(slot(*this,&seqedit::set_scale), c_scale_off )));
    m_menu_scale->items().push_back(MenuElem(c_scales_text[1], bind(slot(*this,&seqedit::set_scale), c_scale_major )));
    m_menu_scale->items().push_back(MenuElem(c_scales_text[2], bind(slot(*this,&seqedit::set_scale), c_scale_minor )));
    
    //midi channel menu
    for( int i=0; i<16; i++ ){
        
        sprintf( b, "%d", i+1 );
        
        m_menu_midich->items().push_back(MenuElem(b, 
                                                  bind(slot(*this,&seqedit::set_midi_channel), 
                                                       i )));
        //length
        m_menu_length->items().push_back(MenuElem(b, 
                                                  bind(slot(*this,&seqedit::set_measures),   
                                                       i+1 )));
        //length
        m_menu_bpm->items().push_back(MenuElem(b, 
                                               bind(slot(*this,&seqedit::set_bpm),   
                                                    i+1 )));
    }

    m_menu_length->items().push_back(MenuElem("32", bind(slot(*this,&seqedit::set_measures), 32 )));
    m_menu_length->items().push_back(MenuElem("64", bind(slot(*this,&seqedit::set_measures), 64 )));





	//tools
	Menu *holder;

	holder = manage( new Menu());
	holder->items().push_back( MenuElem( "All Notes",       bind(slot(*this,&seqedit::do_action),  0 )));
	holder->items().push_back( MenuElem( "All Events",      bind(slot(*this,&seqedit::do_action),  0 )));
	holder->items().push_back( MenuElem( "Inverse Notes",   bind(slot(*this,&seqedit::do_action),  0 )));
  	holder->items().push_back( MenuElem( "Inverse Events",  bind(slot(*this,&seqedit::do_action), 0 )));
	m_menu_tools->items().push_back( MenuElem( "Select", *holder ));

	holder = manage( new Menu());
	holder->items().push_back( MenuElem( "Notes",       bind(slot(*this,&seqedit::do_action), 0 )));
  	holder->items().push_back( MenuElem( "Events",      bind(slot(*this,&seqedit::do_action), 0 )));
 	holder->items().push_back( MenuElem( "All",         bind(slot(*this,&seqedit::do_action), 0 )));
 	holder->items().push_back( MenuElem( "Selected",    bind(slot(*this,&seqedit::do_action), 0 )));
	m_menu_tools->items().push_back( MenuElem( "Quantize", *holder ));
 
	holder = manage( new Menu());
	holder->items().push_back( MenuElem( "Up",       bind(slot(*this,&seqedit::do_action), 0 )));
  	holder->items().push_back( MenuElem( "Down",      bind(slot(*this,&seqedit::do_action), 0 )));

	m_menu_tools->items().push_back( MenuElem( "Transpose", *holder ));

    */

  //m_menu_tools->items().push_back( SeparatorElem( )); 
 
}


void
seqedit_do_action(int a_action)
{
}


void  
seqedit_fill_top_bar(void)
{
  /*
    //name:
    seqedit->m_entry_name = manage( new Entry(  ));
    seqedit->m_entry_name->set_max_length(14);
    seqedit->m_entry_name->set_width_chars(14);
    seqedit->m_entry_name->set_text( m_seq->get_name());
    seqedit->m_entry_name->select_region(0,0);
    seqedit->m_entry_name->set_position(0);
    seqedit->m_entry_name->signal_changed().connect( slot( *this, &seqedit::name_change_callback));

    m_hbox->pack_start( *m_entry_name, true, true );
    //m_hbox->pack_start( *m_button_tools, false, false );

    m_hbox->pack_start( *(manage(new VSeparator( ))), false, false, 4);
    
    //undo
    m_button_undo = manage( new Button());
    m_button_undo->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( undo_xpm  ))));
    m_button_undo->signal_clicked().connect(  slot( *this, &seqedit::undo_callback));
    m_tooltips->set_tip( *m_button_undo, "Undo." );
 
    m_hbox3->pack_start( *m_button_undo , false, false );
    m_hbox3->pack_start( *(manage(new VSeparator( ))), false, false, 4);

    
    

              
    //key
    m_button_key = manage( new Button());
    m_button_key->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( key_xpm  ))));
    m_button_key->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_key  ));
    m_tooltips->set_tip( *m_button_key, "Key of Sequence" );
    m_entry_key = manage( new Entry());
    m_entry_key->set_width_chars(5);
    m_entry_key->set_editable( false );

    m_hbox3->pack_start( *m_button_key , false, false );
    m_hbox3->pack_start( *m_entry_key , false, false );

    //music scale
    m_button_scale = manage( new Button());
    m_button_scale->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( scale_xpm  ))));
    m_button_scale->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_scale  ));
    m_tooltips->set_tip( *m_button_scale, "Musical Scale" );
    m_entry_scale = manage( new Entry());
    m_entry_scale->set_width_chars(5);
    m_entry_scale->set_editable( false );

    m_hbox3->pack_start( *m_button_scale , false, false );
    m_hbox3->pack_start( *m_entry_scale , false, false );

    m_hbox3->pack_start( *(manage(new VSeparator( ))), false, false, 4);

    //background sequence
    m_button_sequence = manage( new Button());
    m_button_sequence->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( sequences_xpm  ))));
    m_button_sequence->signal_clicked().connect(  slot( *this, &seqedit::popup_sequence_menu));
    m_tooltips->set_tip( *m_button_sequence, "Background Sequence" );
    m_entry_sequence = manage( new Entry());
    m_entry_sequence->set_width_chars(14);
    m_entry_sequence->set_editable( false );

    m_hbox3->pack_start( *m_button_sequence , false, false );
    m_hbox3->pack_start( *m_entry_sequence , true, true );
 
    //beats per measure
    m_button_bpm = manage( new Button());
    m_button_bpm->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( down_xpm  ))));
    m_button_bpm->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_bpm  ));
    m_tooltips->set_tip( *m_button_bpm, "Time Signature. Beats per Measure" );
    m_entry_bpm = manage( new Entry());
    m_entry_bpm->set_width_chars(2);
    m_entry_bpm->set_editable( false );

    m_hbox->pack_start( *m_button_bpm , false, false );
    m_hbox->pack_start( *m_entry_bpm , false, false );
 
    m_hbox->pack_start( *(manage(new Label( "/" ))), false, false, 4);

    //beat width
    m_button_bw = manage( new Button());
    m_button_bw->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( down_xpm  ))));
    m_button_bw->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_bw  ));
    m_tooltips->set_tip( *m_button_bw, "Time Signature.  Length of Beat" );
    m_entry_bw = manage( new Entry());
    m_entry_bw->set_width_chars(2);
    m_entry_bw->set_editable( false );

    m_hbox->pack_start( *m_button_bw , false, false );
    m_hbox->pack_start( *m_entry_bw , false, false );


    //length
    m_button_length = manage( new Button());
    m_button_length->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( length_xpm  ))));
    m_button_length->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_length  ));
    m_tooltips->set_tip( *m_button_length, "Sequence length in Bars." );
    m_entry_length = manage( new Entry());
    m_entry_length->set_width_chars(2);
    m_entry_length->set_editable( false );

    m_hbox->pack_start( *m_button_length , false, false );
    m_hbox->pack_start( *m_entry_length , false, false );



  



    //snap
    m_button_snap = manage( new Button());
    m_button_snap->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( snap_xpm  ))));
    m_button_snap->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_snap  ));
    m_tooltips->set_tip( *m_button_snap, "Grid snap." );
    m_entry_snap = manage( new Entry());
    m_entry_snap->set_width_chars(5);
    m_entry_snap->set_editable( false );
    
    m_hbox2->pack_start( *m_button_snap , false, false );
    m_hbox2->pack_start( *m_entry_snap , false, false );


    //note_length
    m_button_note_length = manage( new Button());
    m_button_note_length->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( note_length_xpm  ))));
    m_button_note_length->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_note_length  ));
    m_tooltips->set_tip( *m_button_note_length, "Note Length." );
    m_entry_note_length = manage( new Entry());
    m_entry_note_length->set_width_chars(5);
    m_entry_note_length->set_editable( false );
    
    m_hbox2->pack_start( *m_button_note_length , false, false );
    m_hbox2->pack_start( *m_entry_note_length , false, false );


    //zoom
    m_button_zoom = manage( new Button());
    m_button_zoom->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( zoom_xpm  ))));
    m_button_zoom->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_zoom  ));
    m_tooltips->set_tip( *m_button_zoom, "Zoom. Pixels to Ticks" );
    m_entry_zoom = manage( new Entry());
    m_entry_zoom->set_width_chars(4);
    m_entry_zoom->set_editable( false );

    m_hbox2->pack_start( *m_button_zoom , false, false );
    m_hbox2->pack_start( *m_entry_zoom , false, false );

  
    m_hbox2->pack_start( *(manage(new VSeparator( ))), false, false, 4);


    //midi bus
    m_button_bus = manage( new Button());
    m_button_bus->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( bus_xpm  ))));
    m_button_bus->signal_clicked().connect( slot( *this, &seqedit::popup_midibus_menu));
    m_tooltips->set_tip( *m_button_bus, "Select Output Bus." );

    m_entry_bus = manage( new Entry());
    m_entry_bus->set_max_length(30);
    m_entry_bus->set_width_chars(30);
    m_entry_bus->set_editable( false );

    m_hbox2->pack_start( *m_button_bus , false, false );
    m_hbox2->pack_start( *m_entry_bus , true, true );

    //midi channel
    m_button_channel = manage( new Button());
    m_button_channel->add( *manage( new Image(Gdk::Pixbuf::create_from_xpm_data( midi_xpm  ))));
    m_button_channel->signal_clicked().connect(  bind<Menu *>( slot( *this, &seqedit::popup_menu), m_menu_midich  ));
    m_tooltips->set_tip( *m_button_channel, "Select Midi channel." );
    m_entry_channel = manage( new Entry());
    m_entry_channel->set_width_chars(2);
    m_entry_channel->set_editable( false );

    m_hbox2->pack_start( *m_button_channel , false, false );
    m_hbox2->pack_start( *m_entry_channel , false, false );
    */
}


void
seqedit_popup_menu(GtkWidget *a_menu)
{
  //a_menu->popup(0,0);
}


void
seqedit_popup_midibus_menu(void)
{
  /*
    using namespace Menu_Helpers;

    m_menu_midibus = manage( new Menu());

    //midi buses
    mastermidibus *masterbus = m_mainperf->get_master_midi_bus();
    for ( int i=0; i< masterbus->get_num_out_buses(); i++ ){
        m_menu_midibus->items().push_back(MenuElem(masterbus->get_midi_bus_name(i),
                                                   bind(slot(*this,&seqedit::set_midi_bus), i)));
    }

    m_menu_midibus->popup(0,0);

  */
}


void
seqedit_popup_sequence_menu(void)
{
  /*
    using namespace Menu_Helpers;

    if ( m_menu_sequences != NULL )
        delete m_menu_sequences;

    m_menu_sequences = manage( new Menu());

    m_menu_sequences->items().push_back(MenuElem("Off",
                                                 bind(slot
                                                      (*this,
                                                       &seqedit::set_background_sequence), -1)));
    m_menu_sequences->items().push_back( SeparatorElem( ));

    for ( int ss=0; ss<c_max_sets; ++ss ){

        Menu *menu_ss = NULL;  
        bool inserted = false;
        
        for ( int seq=0; seq<  c_seqs_in_set; seq++ ){

            int i = ss * c_seqs_in_set + seq;

            char name[30];
            
            if ( m_mainperf->is_active( i )){

                if ( !inserted ){
                    inserted = true;
                    sprintf( name, "[%d]", ss );
                    menu_ss = manage( new Menu());
                    m_menu_sequences->items().push_back(MenuElem(name,*menu_ss));
                    
                }
                
                sequence *seq = m_mainperf->get_sequence( i );                
                sprintf( name, "[%d] %.13s", i, seq->get_name() );

                menu_ss->items().push_back(MenuElem(name,
                                                    bind(slot
                                                         (*this,
                                                          &seqedit::set_background_sequence), i)));
                
            }
        }
    }
    
    m_menu_sequences->popup(0,0);
  */
}


void
seqedit_set_background_sequence(int a_seq)
{
    /*
    char name[30];

    m_initial_sequence = m_sequence = a_seq;

    if ( a_seq == -1 || !m_mainperf->is_active( a_seq )){
        m_entry_sequence->set_text("Off");
         m_seqroll_wid->set_background_sequence( false, NULL );
    }
    
    if ( m_mainperf->is_active( a_seq )){

        sequence *seq = m_mainperf->get_sequence( a_seq );                
        sprintf( name, "[%d] %.13s", a_seq, seq->get_name() );
        m_entry_sequence->set_text(name);

        m_seqroll_wid->set_background_sequence( true, a_seq );

    }
  */
}


GtkImage*
seqedit_create_menu_image(gboolean a_state)
{
  /*
    if (a_state)
        return manage( new Image(Gdk::Pixbuf::create_from_xpm_data( menu_full_xpm  )));
    else
        return manage( new Image(Gdk::Pixbuf::create_from_xpm_data( menu_empty_xpm  )));
  */
  return NULL; //temp!!!
}


void
seqedit_popup_event_menu(void)
{
  /*
    using namespace Menu_Helpers;

    //temp
    char b[20];

    bool note_on = false;
    bool note_off = false;
    bool aftertouch = false;
    bool program_change = false;
    bool channel_pressure = false;
    bool pitch_wheel = false;
    bool ccs[128]; memset( ccs, false, sizeof(bool) * 128 );

    unsigned char status, cc;
    m_seq->reset_draw_marker();
    while ( m_seq->get_next_event( &status, &cc ) == true ){

        switch( status ){
            
        	case EVENT_NOTE_OFF: note_off = true; break;	  
            case EVENT_NOTE_ON: note_on = true; break;
            case EVENT_AFTERTOUCH: aftertouch = true; break;
            case EVENT_CONTROL_CHANGE: ccs[cc] = true; break;
            case EVENT_PITCH_WHEEL: pitch_wheel = true; break;
		    
            //one data item
            case EVENT_PROGRAM_CHANGE: program_change = true; break;
            case EVENT_CHANNEL_PRESSURE: channel_pressure = true; break;
        }
    }


    m_menu_data = manage( new Menu());

    m_menu_data->items().push_back( ImageMenuElem( "Note On Velocity",
                                                   *create_menu_image( note_on ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_NOTE_ON, 0 )));

    m_menu_data->items().push_back( SeparatorElem( )); 

    m_menu_data->items().push_back( ImageMenuElem( "Note Off Velocity",
                                                   *create_menu_image( note_off ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_NOTE_OFF, 0 )));

    m_menu_data->items().push_back( ImageMenuElem( "AfterTouch",
                                                   *create_menu_image( aftertouch ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_AFTERTOUCH, 0 )));

    m_menu_data->items().push_back( ImageMenuElem( "Program Change",
                                                   *create_menu_image( program_change ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_PROGRAM_CHANGE, 0 )));

    m_menu_data->items().push_back( ImageMenuElem( "Channel Pressure",
                                                   *create_menu_image( channel_pressure ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_CHANNEL_PRESSURE, 0 )));

    m_menu_data->items().push_back( ImageMenuElem( "Pitch Wheel",
                                                   *create_menu_image( pitch_wheel ),
                                                   bind(slot(*this,&seqedit::set_data_type), (unsigned char) EVENT_PITCH_WHEEL , 0 )));

    m_menu_data->items().push_back( SeparatorElem( )); 

    //create control change
    for ( int i=0; i<8; i++ ){
        
        sprintf( b, "Controls %d-%d", (i*16), (i*16)+15 );
        Menu *menu_cc = manage( new Menu() ); 
        
        for( int j=0; j<16; j++ ){
            menu_cc->items().push_back( ImageMenuElem( c_controller_names[i*16+j],
                                                       *create_menu_image( ccs[i*16+j]),
                                                       bind(slot(*this,&seqedit::set_data_type), 
                                                       (unsigned char) EVENT_CONTROL_CHANGE, i*16+j)));
        }
        m_menu_data->items().push_back( MenuElem( string(b), *menu_cc ));
    }

    m_menu_data->popup(0,0);
  */
}


    //m_option_midich->set_history( m_seq->getMidiChannel() );
    //m_option_midibus->set_history( m_seq->getMidiBus()->getID() );


void 
seqedit_set_midi_channel(int a_midichannel)
{
  /*
    char b[10];
    sprintf( b, "%d", a_midichannel+1 );
    m_entry_channel->set_text(b);
    m_seq->set_midi_channel( a_midichannel );
    m_mainwid->update_sequence_on_window( m_pos );
  */
}


void 
seqedit_set_midi_bus(int a_midibus)
{
  /*
    if ( m_menu_midibus != NULL )
        delete m_menu_midibus;
    
    m_seq->set_midi_bus( a_midibus );
	mastermidibus *mmb =  m_mainperf->get_master_midi_bus();
    m_entry_bus->set_text( mmb->get_midi_bus_name( a_midibus ));
    m_mainwid->update_sequence_on_window( m_pos );
  */
}


void 
seqedit_set_zoom(int a_zoom)
{
  /*
    char b[10];
    sprintf( b, "1:%d", a_zoom );
    m_entry_zoom->set_text(b);

    m_zoom = a_zoom;
    m_initial_zoom = a_zoom;
    m_seqroll_wid->set_zoom( m_zoom );
    m_seqtime_wid->set_zoom( m_zoom );
    m_seqdata_wid->set_zoom( m_zoom ); 
    m_seqevent_wid->set_zoom( m_zoom );
  */
}


void 
seqedit_set_snap(int a_snap)
{
  /*
    char b[10];
    sprintf( b, "1/%d",   c_ppqn * 4 / a_snap );
    m_entry_snap->set_text(b);
    
    m_snap = a_snap;
    m_initial_snap = a_snap;
    m_seqroll_wid->set_snap( m_snap );
    m_seqevent_wid->set_snap( m_snap );
  */
}


void 
seqedit_set_note_length(int a_note_length)
{
  /*
    char b[10];
    sprintf( b, "1/%d",   c_ppqn * 4 / a_note_length );
    m_entry_note_length->set_text(b);
    
    m_note_length = a_note_length;
    m_initial_note_length = a_note_length;
    m_seqroll_wid->set_note_length( m_note_length );
  */
}


void 
seqedit_set_scale(int a_scale)
{
  /*
  m_entry_scale->set_text( c_scales_text[a_scale] );

  m_scale = m_initial_scale = a_scale;

  m_seqroll_wid->set_scale( m_scale );

  */
}


void 
seqedit_set_key(int a_note)
{ 
  /*
  m_entry_key->set_text( c_key_text[a_note] );

  m_key = m_initial_key = a_note;
  
  m_seqroll_wid->set_key( m_key );
  */
}
 

void
seqedit_apply_length(int a_bpm, int a_bw, int a_measures)
{
  /*
  m_seq->set_length( a_measures * a_bpm * ((c_ppqn * 4) / a_bw) );

  m_seqroll_wid->reset();
  m_seqtime_wid->reset();
  m_seqdata_wid->reset();  
  m_seqevent_wid->reset();
  */
}


long
seqedit_get_measures(void)
{
  /*
  long measures = ( m_seq->get_length() / ((m_seq->get_bpm() * (c_ppqn * 4)) /  m_seq->get_bw() ));
 
  return measures;
  */
  return 0; //TEMP!!!!!!!!!
}


void 
seqedit_set_measures(int a_length_measures)
{
  /*
    char b[10];
    sprintf( b, "%d", a_length_measures );
    m_entry_length->set_text(b);

    m_measures = a_length_measures;
    apply_length( m_seq->get_bpm(), m_seq->get_bw(), a_length_measures );
  */
}


void 
seqedit_set_bpm(int a_beats_per_measure)
{
  /*
  char b[4];
  sprintf( b, "%d", a_beats_per_measure );
  m_entry_bpm->set_text(b);

  if ( a_beats_per_measure != m_seq->get_bpm() ){
    
    long length = get_measures();
    m_seq->set_bpm( a_beats_per_measure );
    apply_length( a_beats_per_measure, m_seq->get_bw(), length );
  }
  */
}


void 
seqedit_set_bw(int a_beat_width)
{
  /*
  char b[4];
  sprintf( b, "%d",   a_beat_width );
  m_entry_bw->set_text(b);
  
  if ( a_beat_width != m_seq->get_bw()){
    
    long length = get_measures();
    m_seq->set_bw( a_beat_width );
    apply_length( m_seq->get_bpm(), a_beat_width, length );
  }
  */
}




void 
seqedit_name_change_callback(void)
{
    /*
    m_seq->set_name( m_entry_name->get_text());
    m_mainwid->update_sequence_on_window( m_pos );
    */
}


void 
seqedit_play_change_callback(void)
{
    /*
    m_seq->set_playing( m_toggle_play->get_active() );
    m_mainwid->update_sequence_on_window( m_pos );
    */
}


void 
seqedit_record_change_callback(void)
{
    /*
    m_mainperf->get_master_midi_bus()->set_sequence_input( true, m_seq );
    m_seq->set_recording( m_toggle_record->get_active() );
    */
}


void 
seqedit_undo_callback(void)
{
    /*
	m_seq->pop_undo( );
 	
	m_seqroll_wid->redraw();
	m_seqtime_wid->redraw();
	m_seqdata_wid->redraw();  
	m_seqevent_wid->redraw();
    */
}


void 
seqedit_thru_change_callback(void)
{
    /*
    m_mainperf->get_master_midi_bus()->set_sequence_input( true, m_seq );
    m_seq->set_thru( m_toggle_thru->get_active() );
    */
}

void 
seqedit_set_data_type(unsigned char a_status, unsigned char a_control)
{
    /*
    if ( m_menu_data != NULL )
        delete m_menu_data;
           
    m_editing_status = a_status;
    m_editing_cc = a_control;

    m_seqevent_wid->set_data_type( a_status, a_control );
    m_seqdata_wid->set_data_type( a_status, a_control );
	m_seqroll_wid->set_data_type( a_status, a_control );

    char text[100];
    char hex[20];
    char type[80];

    sprintf( hex, "[0x%02X]", a_status );  
    
    if ( a_status ==  EVENT_NOTE_OFF )         
	sprintf( type, "Note Off" );
    if ( a_status ==  EVENT_NOTE_ON )          
	sprintf( type, "Note On" );  
    if ( a_status ==  EVENT_AFTERTOUCH )       
	sprintf( type, "Aftertouch" );           
    if ( a_status ==  EVENT_CONTROL_CHANGE )   
	sprintf( type, "Control Change - %s", c_controller_names[a_control].c_str() );  
    if ( a_status ==    EVENT_PROGRAM_CHANGE )   
	sprintf( type, "Program Change" );       
    if ( a_status ==    EVENT_CHANNEL_PRESSURE ) 
	sprintf( type, "Channel Pressure" );     
    if ( a_status ==  EVENT_PITCH_WHEEL )      
	sprintf( type, "Pitch Wheel" );          

    sprintf( text, "%s %s", hex, type );

    m_entry_data->set_text(text);
    */
}


/*
void 
seqedit_on_realize()
{
    // we need to do the default realize
    //Gtk::Window::on_realize();

    //Glib::signal_timeout().connect(slot(*this,&seqedit::timeout ), c_redraw_ms);
}
*/


gboolean
seqedit_timeout(void)
{
    /*
    if (m_seq->is_dirty_edit() ){

	m_seqroll_wid->idle_redraw();
	m_seqevent_wid->idle_redraw();
	m_seqdata_wid->idle_redraw();
    }

    m_seqroll_wid->draw_progress_on_window();
    */
    return TRUE;
}


/*
static void
seqedit_destroy(AyyiPanel* window)
{
  PF;
  //GtkWidget* widget = window->shell->widget;

  //m_seq->set_editing( false );

  //struct _seqedit* seqedit = (struct _seqedit*)window;//win_widget_get_data(widget);
}
*/


gboolean
seqedit_on_delete_event(GdkEventAny* a_event)
{
    //printf( "seqedit::on_delete_event()\n" );
    /*
    m_seq->set_recording( false );
    m_mainperf->get_master_midi_bus()->set_sequence_input( false, NULL );
    m_seq->set_editing( false );

    delete this;
    */
    return FALSE;
}
