/*
  This file is part of AyyiGtk.
  copyright (C) 2004-2008 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
	TODO try GEGL http://gegl.org/
*/
#include "config.h"
#include <pango/pangoft2.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#ifdef USE_GANDALF
#  include <gandalf/image/image_defs.h>
#  include <gandalf/image/image_gl_uchar.h>
#  include <gandalf/image/image_gl_uint.h>
#  include <gandalf/image/image_gl_int.h>
#  include <gandalf/image/image_rgb_uchar.h>
#  include <gandalf/vision/mask1D.h>
#  include <gandalf/vision/convolve1D.h>
#  include <gandalf/common/misc_error.h>
#endif

#define USE_CONVOLVE_C
#ifdef USE_CONVOLVE_C
#  include "convolve.h"
#endif

#include "image.h"

#define HAS_ALPHA_FALSE 0
#define _8_BITS_PER_CHAR 8
#define dbg(X, A, ...) printf(A, ##__VA_ARGS__)

static void blend        (FT_Bitmap*, guchar* buf2, guchar alpha);
static void blend_pixbuf (GdkPixbuf*, guchar* buf2, guchar alpha);


void
bloom(FT_Bitmap* bitmap, int* fx_width)
{
	//@param width - input is the requested width of the effect. The return is the actual width used (subject to convolver limitations).

	//FIXME the image we end up with needs to be bigger than the src.
#ifdef USE_GANDALF
	int width     = bitmap->width;
	int height    = bitmap->rows;
	int rowstride = bitmap->pitch;
	//printf("%s(): width=%i height=%i pitch=%i\n", __func__, width, height, bitmap->pitch);

	//Gan_Image* img_orig = gan_image_alloc_rgb_uc(width, height);
	//Gan_Image* img_orig = gan_image_alloc_gl_uc(width, height);

	//copy the buffer:
	guchar* src_copy = g_malloc (bitmap->pitch * bitmap->rows);
	memcpy(src_copy, bitmap->buffer, bitmap->pitch * bitmap->rows);

	//make new gan_image using the bitmap buffer. The buffer is not copied.
	Gan_Image* img_orig = gan_image_alloc_data_gl_uc (width, height, rowstride, src_copy, rowstride*height, NULL, 0);

	// image smoothed in x-direction
	Gan_Image* img_out_x = gan_image_form_gen_gl_uc(NULL, width, height, rowstride, GAN_TRUE, NULL, 0, NULL, 0);
	// image smoothed in x & y directions
	//Gan_Image* img_out_xy = gan_image_alloc_gl_uc(width, height);

	// create symmetric 1D convolution mask
	#define GAUSSIAN_STANDARD_DEVIATION 4096.0
	#define MASK_SIZE 25                      //can be any odd size.
	#define MASK_SCALING 1.0                  //multiplies each mask value. Leave it at 1.0
	Gan_Mask1D* mask = gan_gauss_mask_new (GAN_FLOAT, GAUSSIAN_STANDARD_DEVIATION, MASK_SIZE, MASK_SCALING, NULL);
	*fx_width = MASK_SIZE;

	if(0){
		static gboolean done = FALSE;
		if(!done){
			int i; for(i=0;i<mask->size;i++){
				printf("%.3f ", mask->data.f[i]);
			}
			printf("type=%s\n", gan_type_string(mask->type));
		}
		done = TRUE;
	}

	#define EDGE_BEHAVIOUR GAN_EDGE_BEHAVIOUR_REPEAT
	//#define EDGE_BEHAVIOUR GAN_EDGE_BEHAVIOUR_EXPAND //this requires bigger images.
	// apply smoothing in the x direction
	gan_image_convolve1Dx_q (img_orig, GAN_ALL_CHANNELS, EDGE_BEHAVIOUR, mask, img_out_x);

	// use the original buffer to draw back onto directly
	Gan_Image* img_out_xy = gan_image_alloc_data_gl_uc (width, height, rowstride, src_copy, rowstride*height, NULL, 0);
	// apply smoothing in the y direction
	gan_image_convolve1Dy_q (img_out_x, GAN_ALL_CHANNELS, EDGE_BEHAVIOUR, mask, img_out_xy);

	blend(bitmap, src_copy, 0xff/*a0*/);

	gan_image_free(img_orig);
	gan_image_free(img_out_x);
	gan_image_free(img_out_xy);
	gan_mask1D_free(mask);
	g_free(src_copy);

#else
  #ifdef USE_CONVOLVE_C
	//this uses the statically linked convolver. Limited to small kernel sizes only.

	//copy the buffer:
	int n_bytes = bitmap->rows * bitmap->pitch;
	guchar* copy = g_new(guchar, n_bytes);
	memcpy(copy, bitmap->buffer, n_bytes);

	convolve_main(bitmap);

	//composite the fx bitmap with the original
	blend(bitmap, copy, 0xff);

	#ifdef LATER
	FT_EXPORT( FT_Error )
	FT_Bitmap_Embolden(FT_Library library, FT_Bitmap*  bitmap, FT_Pos xStrength, FT_Pos yStrength );
	#endif
  #endif
#endif

  //--------------------------------------------------------

#ifdef USE_MAGICK
  MagickCoreGenesis("path", MagickTrue);
  ExceptionInfo* exception = AcquireExceptionInfo();
  int width = 100, height = 100;
  GdkPixbuf* pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE, BITS_PER_PIXEL, width, height);
  Image* image = ConstituteImage(width, height, "RGB", CharPixel, gdk_pixbuf_get_pixels(pixbuf), exception);
  if (exception->severity != UndefinedException) CatchException(exception);

  const double radius = 4.0;
  const double sigma = 4.0;
  Image* blurred_image = BlurImage(image, radius, sigma, exception);

  //now how to 'return' the new image?

  //copy it back to the original?
  //MagickBooleanType CompositeImage(Image *image, const CompositeOperator compose,Image *composite_image, const long x_offset,const long y_offset);

  //ImageInfo * ?

  //access directly in the Image object? see /usr/include/magick/image.h
#endif
}


//probably a steeper curve than this is needed.
#define MASK_FN(A) ((A) * (A)) >> 8


#ifdef USE_GANDALF
static void
print_kernel(Gan_Mask1D* mask)
{
	//dbg(0, "size=%i", mask->size);
	int i;
	for(i=0;i<mask->size_alloc;i++){
		printf("%.6f ", mask->data.f[i]);
	}
	printf("\n");
}
#endif


void
bloom_pixbuf(GdkPixbuf* pixbuf, guint x1, guint x2)
{
	g_return_if_fail(GDK_IS_PIXBUF(pixbuf));
	g_return_if_fail(x2 > x1);

	guint width = x2 - x1;
	guint height = gdk_pixbuf_get_height(pixbuf);
	guint nc = gdk_pixbuf_get_n_channels(pixbuf);
	guint rs1 = gdk_pixbuf_get_rowstride(pixbuf);

	//create a 'mask' with only the bright areas:
	GdkPixbuf* mask = gdk_pixbuf_new(GDK_COLORSPACE_RGB, HAS_ALPHA_FALSE, _8_BITS_PER_CHAR, x2-x1, height);
	guchar* src = gdk_pixbuf_get_pixels(pixbuf);
	guchar* m   = gdk_pixbuf_get_pixels(mask);
	guint rs2 = gdk_pixbuf_get_rowstride(mask);
	int y; for(y=0;y<height;y++){
		//memcpy(m + y * rs2, src + (y * rs1) + x1, width * nc);
		int x; for(x=0;x<width*nc;x++){
			guint val = *(src + (y * rs1) + x1 + x);
			*(m + (y * rs2) + x) = MASK_FN(val);
		}
	}

#ifdef USE_GANDALF
	//blur the masked pixbuf, and composite it onto the original image.

	//printf("%s(): width=%i height=%i pitch=%i\n", __func__, width, height, bitmap->pitch);

	//Gan_Image* img_orig = gan_image_alloc_rgb_uc(width, height);

	// create symmetric 1D convolution mask
	#define GAUSSIAN_STANDARD_DEVIATION2 4.0  //if this is increased, masksize must also be increased.
	#define MASK_SIZE2 19                     //can be any odd size.
	#define MASK_SCALING 1.0                  //multiplies each mask value. Leave it at 1.0
	Gan_Mask1D* cmask = gan_gauss_mask_new (GAN_FLOAT, GAUSSIAN_STANDARD_DEVIATION2, MASK_SIZE2, MASK_SCALING, NULL);

	//copy the masked buffer:
	int buf_size = rs2 * height/* * nc*/;
	guchar* src_copy = g_malloc (buf_size);
	memcpy(src_copy, m, buf_size);

	//make new gan_image using the masked buffer. The buffer is not copied.
	//FIXME gandalf only accepts certain sizes?
	width = 256;
	height = 256;
	rs2 = width * 3; //no!!! tmp
	//buf_size = rs2 * height; 
	buf_size = width * height; 
	//dbg(2, "making ganimage from new copy buffer... width=%i height=%i rs=%i size=%i", width, height, rs2, buf_size);
	Gan_Image* img_orig = gan_image_alloc_data_rgb_uc (width, height, rs2, (void*)src_copy, buf_size, NULL, 0);
	//Gan_Image* img_orig = gan_image_alloc_data_rgb_uc (256, 256, 256*3, (void*)src_copy, 65536, NULL, 0);
	if(img_orig){

		// image smoothed in x-direction
		Gan_Image* img_out_x = gan_image_form_gen_gl_uc(NULL, width, height, rs2, GAN_TRUE, NULL, 0, NULL, 0);
		if(img_out_x){

#if 0 //segfaults :-(
			dbg(0, "applying 1st... mask=%p", cmask);
			//#define EDGE_BEHAVIOUR GAN_EDGE_BEHAVIOUR_CLIP
			#define EDGE_BEHAVIOUR2 GAN_EDGE_BEHAVIOUR_REPEAT
			//#define EDGE_BEHAVIOUR GAN_EDGE_BEHAVIOUR_EXPAND //this requires bigger images.
			// apply smoothing in the x direction
			gan_image_convolve1Dx_q (img_orig, GAN_ALL_CHANNELS, EDGE_BEHAVIOUR2, cmask, img_out_x);

			dbg(0, "applying 2nd...");
			// use the original buffer to draw back onto directly
			Gan_Image* img_out_xy = gan_image_alloc_data_rgb_uc (width, height, rs2, (void*)src_copy, buf_size, NULL, 0);
			// apply smoothing in the y direction
			gan_image_convolve1Dy_q (img_out_x, GAN_ALL_CHANNELS, EDGE_BEHAVIOUR2, cmask, img_out_xy);

			//----------------------------------------------------

			//composite the blurred image back onto the original pixbuf:

			blend_pixbuf(pixbuf, src_copy, 0xff/*a0*/);
#endif

			gan_image_free(img_out_x);
		}
		gan_image_free(img_orig);
	}
	g_free(src_copy);
	gan_mask1D_free(cmask);

#endif
	g_object_unref(mask);
}


static void
blend(FT_Bitmap* bitmap, guchar* buf2, guchar alpha)
{
	//composite the @buf2 buffer with the bitmap, leaving the result in the bitmap.
	//the @buf2 buffer must be the same format as the bitmap buffer.

	//dbg(2, "nrows=%i", bitmap->rows);
	int buffer_size = bitmap->pitch * bitmap->rows;
	guchar* b = bitmap->buffer;
	int i; for(i=0;i<buffer_size;i++){
		b[i] = MIN(b[i] + (buf2[i] * alpha) / 0xff, 255);
	}
}


static void
blend_pixbuf(GdkPixbuf* pixbuf, guchar* buf2, guchar alpha)
{
	//composite the @buf2 buffer with the pixbuf, leaving the result in the pixbuf.

	guint height = gdk_pixbuf_get_height(pixbuf);
	//guint nc = gdk_pixbuf_get_n_channels(pixbuf);
	guint rs = gdk_pixbuf_get_rowstride(pixbuf);
	guchar* b = gdk_pixbuf_get_pixels(pixbuf);

	//dbg(0, "nrows=%i", height);

	int buffer_size = rs * height;
	int i; for(i=0;i<buffer_size;i++){
		b[i] = MIN(b[i] + (buf2[i] * alpha) / 0xff, 255);
	}
}


void
copy_bitmap_to_pixbuf(guchar* bitmap, GdkPixbuf* pixbuf, int pitch)
{
	int x, y;
	int width = gdk_pixbuf_get_width(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	guchar* dest = gdk_pixbuf_get_pixels(pixbuf);
	//printf("%s(): rowstride=%i pitch=%i\n", __func__, rowstride, bitmap.pitch);
	int n = 0;
	for(x=0;x<width;x++){
		for(y=0;y<height;y++){
			n++;
			int p = y*rowstride + 3*x;
			dest[p  ] = *(bitmap + y * pitch + x);
			dest[p+1] = *(bitmap + y * pitch + x);
			dest[p+2] = *(bitmap + y * pitch + x);
			//set_pixel(pixbuf, p,   *(bitmap + y * pitch + x));
			//set_pixel(pixbuf, p+1, *(bitmap + y * pitch + x));
			//set_pixel(pixbuf, p+2, *(bitmap + y * pitch + x));
		}
	}
}


void
image_never()
{
	blend_pixbuf(NULL, NULL, 0);
#ifdef USE_GANDALF
	print_kernel(NULL);
#endif
}

