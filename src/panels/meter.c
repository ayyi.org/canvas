/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+

 This panel shows a window from an external process using xembed.
 A peer to peer dbus connection is established with the other process.

 */

#define __meter_win_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "model/time.h"
#if 0
#include "xembed/xembed_socket.h"
#else
#define XembedSocket GtkSocket
#define xembed_socket_new gtk_socket_new
#define xembed_socket_get_id gtk_socket_get_id
#endif
#include "support.h"
#include "song.h"
#include "toolbar.h"
#include "panels/meter.h"

typedef GObject P2PObject;
typedef GObjectClass P2PObjectClass;

struct _AyyiMeterWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiMeterWin {
	AyyiPanel          panel;
	GtkWidget*         socket;
	GdkNativeWindow    socket_id;
	struct _p2p {
		DBusServer*    server;
		//DBusConnection* connection;
		GObject*       object;
		guint16        port;
	}                  p2p;
};

static GType    p2p_object_get_type   (); /* exported type */
static gboolean p2p_object_echo       (P2PObject*, gchar* text_in, gchar** text_out, GError**);
static gboolean p2p_object_set_xid    (P2PObject*, gchar* text_in, gchar** text_out, GError**);
#include "p2p.h"

static GObject* meter_win_construct   (GType, guint n_construct_properties, GObjectConstructParam*);
static void     meter_win_on_ready    (AyyiPanel*);

static void meter_win_on_realise      (GtkWidget*);
static void meter_win_on_unrealize    (GtkWidget*);
static void meter_win_on_destroy      (GtkObject*);
static void meter_win_on_plug_added   (XembedSocket*, gpointer);
static bool meter_win_on_plug_removed (XembedSocket*, gpointer);
static void meter_win_on_parent_set   (GtkWidget*, GtkObject*, gpointer);
static void meter_win_setup_p2p       (GtkWidget*);
static void meter_win_stop_p2p        (GtkWidget*);

G_DEFINE_TYPE (AyyiMeterWin, ayyi_meterwin, AYYI_TYPE_PANEL)


static void
ayyi_meterwin_class_init (AyyiMeterWinClass* klass)
{
	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	GtkObjectClass* object_class = GTK_OBJECT_CLASS (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	panel->name           = "Meter";
	panel->has_toolbar    = true;
	panel->default_size.x = 360;
	panel->default_size.y = 400;
	panel->on_ready       = meter_win_on_ready;

	widget_class->realize = meter_win_on_realise;
	widget_class->unrealize = meter_win_on_unrealize;
	object_class->destroy = meter_win_on_destroy;
	g_object_class->constructor = meter_win_construct;
}


static GObject*
meter_win_construct (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	AYYI_DEBUG_1 printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* g_object = G_OBJECT_CLASS(ayyi_meterwin_parent_class)->constructor(type, n_construct_properties, construct_param);

	ayyi_panel_set_name((AyyiPanel*)g_object, AYYI_TYPE_METERWIN);

	return g_object;
}


static void
ayyi_meterwin_init (AyyiMeterWin* object)
{
}


static void
meter_win_on_ready (AyyiPanel* panel)
{
	AyyiMeterWin* meter = (AyyiMeterWin*)panel;

	if(meter->socket) return;

	meter->socket = xembed_socket_new ();

	//gtk_widget_set_can_focus(meter->socket, true);

	g_signal_connect(meter->socket, "plug-added", (GCallback)meter_win_on_plug_added, NULL);
	g_signal_connect(meter->socket, "plug-removed", (GCallback)meter_win_on_plug_removed, NULL);
	g_signal_connect(meter->socket, "parent-set", (GCallback)meter_win_on_parent_set, NULL);

	gtk_widget_show (meter->socket);
	gtk_box_pack_start(GTK_BOX(panel->vbox), meter->socket, EXPAND_TRUE, FILL_TRUE, 0);

	meter_win_setup_p2p((GtkWidget*)panel);
	if(!meter->socket_id){
		if(meter->socket)
			meter->socket_id = xembed_socket_get_id((XembedSocket*)meter->socket);
		dbg(1, "socket window id: %x", meter->socket_id);

		char id[32]; snprintf(id, 31, "-x %i", meter->socket_id);
		char port[32]; snprintf(port, 31, "-p %i", meter->p2p.port);
		gchar* argv[3] = {id, port, NULL};
		ayyi_launch_by_name("Ayyi Meter", argv);
	}else{
		dbg(1, "____ socket window id: %x", xembed_socket_get_id((XembedSocket*)meter->socket));
	}
}


static void
meter_win_on_realise (GtkWidget* widget)
{
	((GtkWidgetClass*)ayyi_meterwin_parent_class)->realize(widget);
}


static void
meter_win_on_unrealize (GtkWidget *widget)
{
	PF0;
	AyyiMeterWin* meter = (AyyiMeterWin*)widget;

	dbus_server_disconnect(meter->p2p.server);
	GTK_WIDGET_CLASS(ayyi_meterwin_parent_class)->unrealize(widget);
}


static void
meter_win_on_destroy (GtkObject* object)
{
	PF;

	meter_win_stop_p2p((GtkWidget*)object);
	GTK_OBJECT_CLASS(ayyi_meterwin_parent_class)->destroy(object);
}


static void
meter_win_on_plug_added (XembedSocket* socket, gpointer user_data)
{
	dbg(1, "socket window id: %x", xembed_socket_get_id(socket));
}


static bool
meter_win_on_plug_removed (XembedSocket* socket, gpointer user_data)
{
	#define dont_delete TRUE
	return dont_delete;
}


static void
meter_win_on_parent_set (GtkWidget* widget, GtkObject* old_parent, gpointer user_data)
{
	PF;
}


// to generate the dbus header file:
// dbus-binding-tool --mode=glib-server --prefix=p2p_object --output=p2p.h p2p-object.xml

/* implementation of the exported type */
G_DEFINE_TYPE (P2PObject, p2p_object, G_TYPE_OBJECT);


static void
p2p_object_init (P2PObject* self)
{
}


static void
p2p_object_class_init (P2PObjectClass* self_class)
{
	dbus_g_object_type_install_info (G_TYPE_FROM_CLASS (self_class), &dbus_glib_p2p_object_object_info);
}


static gboolean
p2p_object_echo (P2PObject* self, gchar* text_in, gchar** text_out, GError** error)
{
	if (!G_TYPE_CHECK_INSTANCE_TYPE (self, p2p_object_get_type ())) {
		g_set_error (error, 0, 0, "invalid object type");
		return false;
	}

	*text_out = g_strdup_printf ("%1$s %1$s", text_in);
	return true;
}


/*
 *  Called by out-of-process plugs to obtain the window id.
 */
static gboolean
p2p_object_set_xid (P2PObject* self, gchar* text_in, gchar** text_out, GError** error)
{
	if (!G_TYPE_CHECK_INSTANCE_TYPE (self, p2p_object_get_type ())) {
		g_set_error (error, 0, 0, "invalid object type");
		return FALSE;
	}

	AyyiMeterWin* meter = g_object_get_data(self, "panel");
	dbg(1, "meter=%p server=%p", meter, meter->p2p.server);
	g_return_val_if_fail(meter, FALSE);
	g_return_val_if_fail(meter->p2p.server, FALSE);

	dbg(1, "socket window id: %x", xembed_socket_get_id((XembedSocket*)meter->socket));

	*text_out = g_strdup_printf ("%i", xembed_socket_get_id((XembedSocket*)meter->socket));

	g_object_unref(meter);
	return TRUE;
}


static void
new_connection_cb (DBusServer* server, DBusConnection* connection, gpointer _panel)
{
	AyyiPanel* panel = _panel;
	AyyiMeterWin* meter = (AyyiMeterWin*)panel;

	dbus_int32_t slot = -1;
	if (!dbus_connection_allocate_data_slot (&slot)) {
		g_warning ("error allocating data slot for DBusConnection");
		dbus_connection_close (connection);
		return;
	}

	dbus_connection_ref (connection);

	dbus_connection_set_allow_anonymous (connection, TRUE);
	dbus_connection_setup_with_g_main (connection, NULL);

	GObject* object = meter->p2p.object = g_object_new (p2p_object_get_type (), NULL);
	dbus_g_connection_register_g_object (dbus_connection_get_g_connection (connection), "/", object);
	dbus_connection_set_data (connection, slot, object, g_object_unref);

	g_object_set_data(object, "panel", panel);
}


guint16
my_dbus_server_get_port (DBusServer* server)
{
	char* address = dbus_server_get_address (server);

	DBusAddressEntry** entries;
	DBusError error;
	int n_entries;
	dbus_error_init (&error);
	if (!dbus_parse_address (address, &entries, &n_entries, &error)) {
		g_warning ("error parsing server address: %s", error.message);
		dbus_error_free (&error);
		free (address);
		return 0;
	}

	guint16 port = 0;
	for (int i = 0; i < n_entries; i++) {
		if (!strcmp ("tcp", dbus_address_entry_get_method (entries[i]))) {
			port = atoi (dbus_address_entry_get_value (entries[i], "port"));
			break;
		}
	}

	dbus_address_entries_free (entries);
	free (address);
	return port;
}


static void
meter_win_setup_p2p (GtkWidget* widget)
{
	PF;
	AyyiMeterWin* meter = (AyyiMeterWin*)widget;
	DBusError derror;

	dbus_error_init (&derror);
	DBusServer* server = meter->p2p.server = dbus_server_listen("tcp:bind=*", &derror);
	if (dbus_error_is_set (&derror)) {
		g_warning ("error setting up peer-to-peer server: %s", derror.message);
		dbus_error_free (&derror);
		return;
	}

	dbus_server_set_new_connection_function(server, &new_connection_cb, meter, NULL);
	meter->p2p.port = my_dbus_server_get_port (server);

	dbus_server_setup_with_g_main (server, NULL);
}


static void
meter_win_stop_p2p (GtkWidget* widget)
{
	PF0;
	AyyiMeterWin* meter = (AyyiMeterWin*)widget;

	dbg(0, "meter=%p connected=%i", meter, meter->p2p.server ? dbus_server_get_is_connected(meter->p2p.server) : -1);
	if(meter->p2p.server/* && dbus_server_get_is_connected(meter->p2p.server)*/){
		dbg(0, "disconnecting...");
		//dbus_server_disconnect(meter->p2p.server);
		dbus_server_unref(meter->p2p.server);
		meter->p2p.server = NULL;
		g_clear_pointer(&meter->p2p.object, g_object_unref);
	}
}
