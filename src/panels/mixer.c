/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*

mixer related objects:
	-window   - each window (AyyiMixerWin) is an independent view of the mixer model.
	-channel  - one garray per song, containing channels and virtual (group) channels.
	-strips   - gui widgets. one fixed size array per window.
                TODO strips object creation and deletion could be tidied. Its error-prone having data and widgets allocated separately.

what is a mixer Strip?
	-a Strip is a view object backed by an AMChannel model object.
	-a Channel can have any number of associated strips, including zero.
	-with engines that dont support channels as distinct from tracks, a strip roughtly represents a song track.
	-strip types:
		-input channels
		-virtual (group) channel strips
		-bus channels
		-master out (bus 1,2)
	-beware that the display order of strips is not the same as the order in the mixer window strip array. Currently only the hbox widget knows about display order.

what is a Channel?
	-a Channel corresponds to an audio path.
	-a Channel is not a Track. A Track has zero or more Channels assigned to it. In some cases the model is simplified so that the Track/Channel correlation is fixed at 1:1.

TODO how to deal with busses and master channels?
	-what are the differences between channel types?
		-master channels are very cut-down.
		-buss channels? similar to Input chans. different input routing options?
		-virtual channels? are they just fader/mute or is everything available?

	fader logarithmic adjustments
	-----------------------------

	scale transformations can be implemented as adjustments, see for instance
	the logarithmic adjustments used in beast:
	   https://github.com/tim-janik/beast/blob/master/beast-gtk/gxk/gxklogadjustment.hh
	   https://github.com/tim-janik/beast/blob/master/beast-gtk/gxk/gxklogadjustment.cc
	that code also contains a more generic GxkAdapterAdjustment which allows
	arbitrary adjustment value mappings - the beast implementation supports arbitrary
	spline curves (lookup tables) for the value mapping of it's mixer meters.

	see: beast-gtk/bstparam-scale.c (rotary?)
	     beast-gtk/gxk/gxkparam-scale.c (linear?)

*/

#define __mixer_panel_c__
#define USE_MULTICONTAINER
#include "global.h"
#include <math.h>
#include <gdk/gdkkeysyms.h>

#include "model/am_message.h"
#include "model/am_collection.h"
#include "model/channel.h"
#include "model/am_palette.h"
#include "model/plugin.h"
#include "model/track_list.h"
#include "windows.h"
#include "io.h"
#include "widgets/fader_label.h"
#include "widgets/rotary.h"
#include "widgets/mixer_hbox.h"
#include "widgets/ayyi_hscale.h"
#include "icon.h"
#include "support.h"
#include "toolbar.h"
#include "../meter.h"
#include "gtkmeter.h"
#ifdef USE_CUSTOM_COMBOBOX
  #include "widget-comboboxentry.h"
#endif
#include "song.h"
#include "menu.h"
#include "mixer/master_strip.h"
#include "mixer/insert.h"
#include "mixer/fader.h"
#include "panels/inspector.h"
#include "mixer.h"

#define PAN_RANGE 20.0
#define MIN_CH_WIDTH 10
#define CONNECTION_OFF 0

GtkWidget* pressed_widget; // the widget that is currently pressed with the mouse (or NULL).

extern bool      offline;
extern SMConfig* config;
extern TreePopup input;
extern TreePopup output;

static void       mixer_add_load_handlers            (AyyiPanelClass*);

static void       init_panning                       (double _top, double _mid, double _bot);
static GObject*   mixer_constructor                  (GType, guint, GObjectConstructParam*);
static void       mixer__destroy                     (GtkObject*);
static void       mixer__redraw                      (AyyiPanel*);
static void       mixer_on_sizereq                   (GtkWidget*, GtkRequisition*, gpointer);
static void       mixer_on_resize                    (GtkWidget*, GtkAllocation*);
static void       mixer_on_realise                   (GtkWidget*);
static void       mixer_on_ready                     (AyyiPanel*);
static void**     mixer_get_config                   (AyyiPanel*);
static void       mixer__select_all                  (AyyiPanel*);
static void       mixer__zoom_in                     (AyyiPanel*, ZoomType);
static void       mixer__zoom_out                    (AyyiPanel*, ZoomType);
static double     mixer__get_zoom                    (MixerWin*);

static void       mixer__set_hzoom                   (MixerWin*, float);
static void       mixer__set_send_count              (MixerWin*, int);
static void       mixer__set_rotary_aux              (MixerWin*, bool);

static void       mixer__reallocate_chs              (MixerWin*);
static void       mixer__periodic_update             (MixerWin*);
static void       mixer__set_meter_type              (AyyiPanel*, gpointer type);

static Strip*     mixer__insert_new_strip            (MixerWin*, AMChannel*, int pos);
static void       mixer__remove_strip                (MixerWin*, AMChannel*);
static bool       mixer__insert_strip                (MixerWin*, AyyiMixerStrip*, int pos);
static void       mixer__update_insert_size          (MixerWin*, AMChannel*);
static int        mixer__get_n_strips                (MixerWin*);
static GtkWidget* mixer__add_strip_to_hbox           (MixerWin*, AyyiMixerStrip*);
static GtkWidget* mixer__add_masterstrip_to_hbox     (MixerWin*, AyyiMixerStrip*);
static void       mixer__show_all_strips             (MixerWin*);
#ifndef USE_MULTICONTAINER
static void       strip_on_resize                    (GtkWidget*, GdkEvent*, gpointer);
#endif
static void       mixer_zoombar_on_value_changed     (GtkWidget*, MixerWin*);
//static void     mixer_set_strip_width(AyyiPanel*, double strip_width);
static void       k_zoom_in_hor                      (GtkAccelGroup*, gpointer);
static void       k_zoom_out_hor                     (GtkAccelGroup*, gpointer);
static void       mixer__selection_clear             (MixerWin*);

static GList*     mixer__get_strips_for_channel      (MixerWin*, const AMChannel*);
static void       mixer__move_strip                  (MixerWin*, int from, int to);
#ifdef NEVER
static void       mixer__move_master_strip           ();
#endif
static Strip*     mixer__insert_new_master_strip     (MixerWin*, AMChannel*);
static bool       mixer__insert_master_strip         (MixerWin*, AyyiMixerStrip*);

static void       mixer__on_song_load                (GObject*, gpointer);
static void       mixer__on_song_unload              (GObject*, gpointer);
static void       mixer__on_tracks_change            (GObject*, TrackNum, TrackNum/*, AyyiPanel* sender*/, int change_type, gpointer);
static void       mixer__on_channel_selection_change (GObject*, AyyiPanel* sender, gpointer);
static void       mixer__on_channels_change          (GObject*, gpointer);
static void       mixer__on_channel_change           (GObject*, AMChannel*, AyyiPanel* sender, gpointer);
static void       mixer__on_plugin_changed           (GObject*, AMChannel*, gpointer);
static void       mixer__on_tracks_change_by_list    (GObject*, GList*, AMChangeType, AyyiPanel*, gpointer);

#if 0
static void       mixer__link_selection_changed      (int ch_num);
#endif

static void       mixer__update_mute                 (MixerWin*, const AMChannel*);
static void       mixer__update_outputs              (MixerWin*, const AMChannel*);
static void       mixer__update_solo                 (MixerWin*, const AMChannel*);
static void       mixer__update_armed                (MixerWin*, const AMChannel*);
static void       mixer__pluginboxes_update          (MixerWin*, AMChannel*);

static MixerMenu* mixer_menu__new                    (AyyiPanel*);
/*static */void   mixer_menu__update                 (MixerWin*);
static void       mixer_menu__sends_show_values      (GtkWidget*, gpointer);
static void       mixer_menu__sends_use_rotaries     (GtkWidget*, gpointer);
static void       mixer_menu__hide_strip             (GtkWidget*, MixerWin*);
static void       mixer_menu__unhide_all             (GtkWidget*, gpointer);

static GtkWidget* aux_menu__new                      ();
static void       aux_menu__update                   (Strip*, int);
void              aux_menu__popup                    (GtkWidget*, GdkEventButton*);
static void       aux_menu__on_mute                  (GtkMenuItem*, gpointer);
static void       aux_menu__prepost                  (GtkMenuItem*, gpointer);
static void       aux_menu__on_bus_select            (GtkMenuItem*, gpointer);
static void       aux_menu__on_add_bus               (GtkMenuItem*, gpointer);

static void       mixer__on_channel_add              (GObject*, AMChannel*, MixerWin*);
static void       mixer__on_channel_delete           (GObject*, AMChannel*, gpointer);
static void       mixer__on_track_selection_change   (Observable*, AMVal, gpointer);

static void       mixer__update_zoombar              (MixerWin*);
static void       mixer__strip_widths_update         (MixerWin*);

static void       mixer__print_strips                (MixerWin*);

static ConfigParam* params[] = {
	&(ConfigParam){"strip_width", G_TYPE_FLOAT, {.f=(gpointer)mixer__set_hzoom}, {.f=0.1}, {.f=100.0}, {.f=1.0}},
	&(ConfigParam){"rotary_aux", G_TYPE_BOOLEAN, {.f=(gpointer)mixer__set_rotary_aux}, {.i=0}, {.i=1}, {.i=1}},
	&(ConfigParam){"sendcount", G_TYPE_INT, {.f=(gpointer)mixer__set_send_count}, {.i=0}, {.i=5}, {.i=2}},
	NULL
};

// should we just use mixer__strip_next() instead?
#define mixer__foreach_strip \
	int __i; for(__i=0;mixer->strips[__i];__i++) { \
		Strip* strip = mixer->strips[__i];

typedef enum {
  CROSS,
  MAX_ICON,
} IconId;

struct _aux {
	char*           name;
	int             icon_id;
	void            (*callback)(GtkMenuItem*, gpointer);
	GtkWidget*      widget;
} aux_items[] = {
	{"Mute",   CROSS, aux_menu__on_mute, NULL},
	{"Unmute", CROSS, aux_menu__on_mute, NULL},
	{"Pre",    CROSS, aux_menu__prepost, NULL},
	{"Post",   CROSS, aux_menu__prepost, NULL/*GINT_TO_POINTER(1)*/},
};

typedef struct {
	Strip*             strip;
	int                aux_num;
	AyyiConnection*    connection;
	GtkWidget*         menuitem;
} AuxMenuBusItem;

static struct {
    GtkWidget*         menu;
    struct _aux*       items;
    GList*             busses;  // list of AuxMenuBusItem*
    AuxMenuBusItem* ami;        // callback data for all 'static' menu items
} aux_menu = {
	NULL, aux_items, NULL
};

G_DEFINE_TYPE (AyyiMixerWin, ayyi_mixer_win, AYYI_TYPE_PANEL)


static void
ayyi_mixer_win_class_init (AyyiMixerWinClass* klass)
{
	init_panning(0, -4.0, 96.0);

	AyyiPanelClass* panel = AYYI_PANEL_CLASS(klass);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS (klass);
	GtkObjectClass* gtk_class = GTK_OBJECT_CLASS (klass);
	GObjectClass* g_object_class = G_OBJECT_CLASS (klass);

	gtk_class->destroy = mixer__destroy;

	// keyboard shortcuts
	AMAccel keys[] = {
		{"Zoom In Hor",  {{(char)'d', 0,}, {0, 0}}, k_zoom_in_hor,  NULL},
		{"Zoom Out Hor", {{(char)'a', 0,}, {0, 0}}, k_zoom_out_hor, NULL},
	};

	panel->accel_group = gtk_accel_group_new();
	make_accels(panel->accel_group, panel->action_group, keys, G_N_ELEMENTS(keys), NULL);

	panel->name           = "Mixer";
	panel->has_link       = true;
	panel->has_follow     = true;
	panel->has_toolbar    = true;
	panel->accel          = GDK_F2;
	panel->default_size.x = 300;
	panel->default_size.y = 640;
	panel->config         = (ConfigParam**)params;
	panel->select_all     = mixer__select_all;
	panel->on_ready       = mixer_on_ready;
	panel->get_config     = mixer_get_config;

	widget_class->size_allocate = mixer_on_resize;
	widget_class->realize       = mixer_on_realise;

	g_object_class->constructor = mixer_constructor;

	mixer_add_load_handlers(panel);

	g_signal_connect(song->channels, "change", G_CALLBACK(mixer__on_channel_change), NULL);
	g_signal_connect(song->channels, "delete", G_CALLBACK(mixer__on_channel_delete), NULL);
	am_song__connect("plugin-change", G_CALLBACK(mixer__on_plugin_changed), NULL);
	am_song__connect("tracks-change-by-list", G_CALLBACK(mixer__on_tracks_change_by_list), NULL);

	void _mixer__periodic_update(gpointer _){ mixer_foreach { mixer__periodic_update(mixer); } end_mixer_foreach }
	am_song__connect("periodic-update", (GCallback)_mixer__periodic_update, NULL);
  
	aux_menu.menu = aux_menu__new();
}


static void
ayyi_mixer_win_init (AyyiMixerWin* mixer)
{
	mixer->selectionlist = NULL;
	mixer->contextmenu   = NULL;

	((AyyiPanel*)mixer)->meter_type = METER_BST;  //METER_JAMIN; //METER_GTK;
}


static GObject*
mixer_constructor (GType type, guint n_construct_properties, GObjectConstructParam* construct_param)
{
	if(_debug_) printf("%s%s%s\n", constructor_colour, __func__, white);

	GObject* object = G_OBJECT_CLASS(ayyi_mixer_win_parent_class)->constructor(type, n_construct_properties, construct_param);
	AyyiPanel* panel = (AyyiPanel*)object;
	MixerWin* m = (MixerWin*)object;

	ayyi_panel_set_name(panel, AYYI_TYPE_MIXER_WIN);

	#define MIXER_MIN_HZOOM 12.0
	#define MIXER_MAX_HZOOM 201.0
	m->hzoom_adj = gtk_adjustment_new (40.0, MIXER_MIN_HZOOM, MIXER_MAX_HZOOM, 1.0, 5.0, 1.0);

	m->scrollwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(m->scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(m->scrollwin), GTK_SHADOW_NONE);

#ifdef USE_MULTICONTAINER
	#include "widgets/mixer_hbox.h"
	m->hbox = mixer_hbox_new(8);
	MIXER_HBOX(m->hbox)->scrollwin = m->scrollwin;
#endif

	void mixer_on_zoom (Observable* zoom, AMVal val, gpointer _mixer)
	{
		MixerWin* mixer = _mixer;

		mixer_hbox_set_zoom (MIXER_HBOX(mixer->hbox), val.pt.x);
	}
	panel->zoom->value.pt = (Ptf){40.0, 1.0};
	panel->zoom->min.pt = (Ptf){MIXER_MIN_HZOOM, 1.0};
	panel->zoom->max.pt = (Ptf){MIXER_MAX_HZOOM, 1.0};

	observable_subscribe(panel->zoom, mixer_on_zoom, m);
	observable_subscribe_with_state(am_tracks->selection2, mixer__on_track_selection_change, m);

	return object;
}


static void
mixer__destroy (GtkObject* object)
{
	AyyiPanel* panel = (AyyiPanel*)object;
	MixerWin* mixer = (MixerWin*)panel;
	g_return_if_fail(mixer);
	g_return_if_fail(AYYI_IS_MIXER_WIN(panel));

	if(!panel->destroyed){
		am_song__disconnect(mixer, mixer__on_tracks_change);
		am_song__disconnect(mixer, mixer__on_song_load);
		am_song__disconnect(mixer, mixer__on_song_unload);
		int n = 0;
		#define channels_handler_disconnect(FN) if((n = g_signal_handlers_disconnect_matched (song->channels, G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA, 0, 0, NULL, FN, mixer)) != 1) pwarn("channel handler disconnection: %s n=%i", #FN, n);
		channels_handler_disconnect(mixer__on_channel_add);
		channels_handler_disconnect(mixer__on_channels_change);
		channels_handler_disconnect(mixer__on_channel_selection_change);
		#undef channels_handler_disconnect

		observable_unsubscribe(am_tracks->selection2, NULL, mixer);

		g_ptr_array_free(mixer->contextmenu->to_free, true);
		g_clear_pointer(&mixer->contextmenu, g_free);
		g_clear_pointer(&mixer->strip_label_sizegroup, g_object_unref);

		if(mixer->resize_timeout) g_source_remove(mixer->resize_timeout);
	}

	GTK_OBJECT_CLASS(ayyi_mixer_win_parent_class)->destroy(object);

	panel->destroyed = true;
}


static void
mixer__move_strip (MixerWin* mixer, int from, int to)
{
	if(mixer->strips[to]){ perr ("destination slot must be empty"); return; }

	mixer->strips[to] = mixer->strips[from];
	mixer->strips[from] = NULL;

	AyyiMixerStrip* s = mixer->strips[to];
	s->strip_num = to;
}


#ifdef NEVER
static void
mixer__move_master_strip ()
{
	// Temporary
	// Make sure the master channel is the last entry in the mixer->strips[] array.
	// -this must be done *before* channel list is fully updated :-(
	// -we cannot move the widgets yet, as any new strips may not have been made.

	mixer_foreach {

		// find the old master
		int old_master_position = 0;
		for(int j=0;j<AM_MAX_CHS;j++){
			if(!mixer->strips[j]) continue;
			AyyiMixerStrip* s = mixer->strips[j];
			if(!s->channel) continue;

			int type = s->channel->type;
			if(type == AM_CHAN_MASTER){
				old_master_position = j;
				break;
			}
		}

		int new_master_position = song->channels->len - 1;
		if(new_master_position != old_master_position){
			dbg (0, "moving master: %i -> %i", old_master_position, new_master_position);
			mixer->strips[new_master_position] = mixer->strips[old_master_position];
			mixer->strips[old_master_position] = NULL;
		}
		else dbg (0, "nothing to move");
	} end_mixer_foreach;
}


static Strip*
mixer_win_master_strip_get (MixerWin* mixer)
{
	dbg (0, "len=%i master=%p", song->channels->len, mixer->strips[song->channels->len-1]);
	return mixer->strips[song->channels->len-1];
}
#endif


/*
 *  Re-allocate existing strips following song load, and create any new strips as neccesary.
 */
static void
mixer__reallocate_chs (MixerWin* mixer)
{
	// TODO remove unused strips.
	// TODO existing strips are not re-allocated

	PF;

	pwarn("resetting strip->chanel allocation - FIXME!! n=%i", song->channels->array->len);
	mixer__print_strips(mixer);

	int c = 0;
	AMIter i;
	channel_iter_init(&i);
	AMChannel* channel;
	while((channel = channel_next(&i))){
		AyyiMixerStrip* strip = mixer->strips[c];

		dbg (1, "%i: type=%i", c, channel->type);

		switch(channel->type){
			case AM_CHAN_INPUT:
			case AM_CHAN_BUS:
			case AM_CHAN_MASTER:
				if (!strip) {
					if(!(strip = strip__new(channel))) { perr ("strip new fail. Out of memory."); break; }
					mixer->strips[c] = strip;

					if(!mixer__add_strip_to_hbox(mixer, strip)) { pwarn("mixer__add_strip_to_hbox() failed."); return; }
				}
				strip->strip_num = c;

				if((_debug_ > -1) && channel->type == AM_CHAN_MASTER && strip->box){
					//dbg(0, "children=%i", g_list_length(gtk_container_get_children(mixer->hbox)));
					//mixer_hbox_get_strip_pos(mixer->hbox, strip);
				}
				//move master to the window rhs:
#ifdef USE_AYYI_CONTAINER
				if(channel->type == AM_CHAN_MASTER && strip->box) ayyi_box_reorder_child(NS_BOX(mixer->hbox), strip, -1);
#else
				if(channel->type == AM_CHAN_MASTER && strip->box){
					dbg(0, "reordering master...");
					//print_widget_tree(mixer->hbox);
				}
				if(channel->type == AM_CHAN_MASTER && strip->box) gtk_box_reorder_child(NS_BOX(mixer->hbox), (GtkWidget*)strip, -1);
#endif
				break;

			default:
				perr ("unknown channel type: %i", channel->type);
				continue;
				break;
		}
		c++;
	}
	PF_DONE;
}


static void
mixer__print_strips (MixerWin* mixer)
{
	g_return_if_fail(mixer);

	UNDERLINE;
	PF;
	int empty = 0;
	printf("       ch type s     meter name\n");
	int i; for(i=0;i<16 && empty<3;i++){
		AyyiMixerStrip* strip = mixer->strips[i];
		if(!strip){ printf("  %2i not assigned\n", i); empty++; continue; }
		AMChannel* channel = strip->channel;
		if(!channel){ printf("  %2i no channel!\n", i); continue; }
		if(strip->strip_num != i) pwarn("index!");

		printf("  %2i    %i %4i %i %s\n", i, channel->ident.idx, channel->type, strip->strip_num, strip__get_ch_name(strip));
	}
	UNDERLINE;
}


/*
 *  Follow automation changes
 */
static void
mixer__periodic_update (MixerWin* mixer)
{
	static int n = 0;
	if(n++ % 2) return;

#if 0
	//Strip* strip0 = ((MixerWin*)window)->strips[0];
	//AyyiChannel* ch0 = ayyi_mixer_track_get(strip0->channel->ident.idx);
	AyyiChannel* ch0 = ayyi_mixer_track_get(1);
	AyyiChannel* ch1 = ayyi_mixer_track_get(2);
	dbg(0, "%p %i %.2f %s",
		ch0,
		ch0->shm_idx,
		ch0->level,
		ch0->name
	);
	am__mixer_print();
#endif

	int c; for(c=0;c<mixer__get_n_strips(mixer);c++){
		AyyiMixerStrip* s = mixer->strips[c];
		AMChannel* channel = s->channel;
		if(channel && channel->type == AM_CHAN_INPUT){
			AyyiChannel* ch = ayyi_mixer__channel_at_quiet(channel->ident.idx);
			if(!ch) continue;
#if 0
			if(!s->pan_fdr) dbg(0, "no pan: strip=%s", strip__get_ch_name(strip));
#endif
			//if((channel->ident.idx == 1) && !(n%9)) dbg(0, "%i: ch1: lvl=%.2f --> %.2f dB", ayyi_panel_get_instance_num((AyyiPanel*)mixer), ch->level, gain2db_linear(ch->level));

			fader__update(s->fdr, s->channel);

			//TODO remove the pan_fdr test - ch->has_pan needs updating after strip deleting?
			if(ch->has_pan && s->pan_fdr) strip__set_pan(s, am_channel__pan_value(channel, NULL));
		}
	}
}


static GtkWidget*
mixer__add_strip_to_hbox (MixerWin* mixer, AyyiMixerStrip* strip)
{
	// The strip must be pre-assigned to the correct slot in mixer->strips[].

	int s = strip->strip_num;
	g_return_val_if_fail(s < AM_MAX_CHS, NULL);
	g_return_val_if_fail(mixer->strips[s], NULL);

	GtkWidget* strip_vbox;
	if ((strip_vbox = strip__add_widgets(&mixer->panel, strip, &mixer->options))){
#ifdef USE_MULTICONTAINER
		/*
		paned_multi_add(PANED_MULTI(mixer->hbox), strip_vbox);
		*/
		mixer_hbox_add_strip(MIXER_HBOX(mixer->hbox), strip);
#else
		gtk_box_pack_start(GTK_BOX(mixer->hbox), strip_vbox, TRUE, TRUE, 0);
#endif
	}
	else { perr ("create_strip failure."); return NULL; }

#ifndef USE_MULTICONTAINER
	//strip resize event box:
	#ifdef TODO__DISABLED_AS_PROP_IS_NOW_PRIVATE
	GtkWidget* resize_handle = mixer->strips[s]->rsiz_handle = gtk_event_box_new();
	gtk_box_pack_start(GTK_BOX(mixer->hbox), resize_handle, FALSE, FALSE, 0);
	gtk_widget_set_size_request(resize_handle, 4, -1); //not having any effect ?
	gtk_widget_show(resize_handle);

	g_signal_connect((gpointer)resize_handle, "event", G_CALLBACK(strip_on_resize), mixer);
	gtk_drag_dest_set(resize_handle, GTK_DEST_DEFAULT_ALL, app->dnd.file_drag_types, app->dnd.file_drag_types_count, (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY));
	g_signal_connect(G_OBJECT(resize_handle), "drag-drop", G_CALLBACK(mixer__drag_drop), NULL);
	g_signal_connect(G_OBJECT(resize_handle), "drag-data-received", G_CALLBACK(strip_drag_received), strip);
	#endif
#endif
	return strip_vbox;
}


static GtkWidget*
mixer__add_masterstrip_to_hbox (MixerWin* mixer, AyyiMixerStrip* strip)
{
	strip__add_widgets((AyyiPanel*)mixer, strip, &mixer->options);

#ifdef USE_MULTICONTAINER
	mixer_hbox_add_strip(MIXER_HBOX(mixer->hbox), strip);
#else
	gtk_widget_set_size_request((GtkWidget*)strip, 20, -1); //set minimum channel width.
	gtk_box_pack_start(GTK_BOX(mixer->hbox), (GtkWidget*)strip, TRUE, TRUE, 0);
#endif

	return NULL;
}


static Strip*
mixer__insert_new_strip (MixerWin* mixer, AMChannel* channel, int pos)
{
	// create a new Strip widget, and add it to the mixer window.

	// @param pos - set to -1 to insert at the default position.

	if(pos == -1){
		//default position is at the end, but before the master.
		pos = MAX(0, mixer__get_n_strips(mixer));
		if(mixer__get_n_strips(mixer)){
			Strip* a = mixer->strips[pos - 1];
			if(a && CH_IS_MASTER(a->channel)){ dbg(1, "  adjusting for master"); pos = MAX(0, pos - 1); }
		}
	}
	dbg(1, "inserting new strip at pos=%i. n_strips=%i", pos, mixer__get_n_strips(mixer));

	Strip* strip = strip__new(channel);

	mixer__insert_strip(mixer, strip, pos);
	return strip;
}


static bool
mixer__insert_strip (MixerWin* mixer, AyyiMixerStrip* strip, int pos)
{
	// Add an existing strip to a mixer window at a certain position.
	// -existing strips are moved out of the way to accomodate this.

	// move all subsequent strips right by one slot
	int s; for(s = mixer__get_n_strips(mixer) - 1; s >= pos; s--){
		dbg(2, "moving strip: %i --> %i", s, s + 1);
		mixer__move_strip(mixer, s, s + 1);
	}

	if(mixer->strips[pos]){ perr ("failed to clear slot for use. %i", pos); return false; }

	dbg(2, "%s: insert pos=%i", strip__get_ch_name(strip), pos);
	strip->strip_num = pos;
	mixer->strips[pos] = strip;
	mixer__add_strip_to_hbox(mixer, strip);

	mixer__strip_widths_update(mixer);
	return true;
}


static Strip*
mixer__insert_new_master_strip (MixerWin* mixer, AMChannel* channel)
{
	Strip* bus = (Strip*)master_strip_new(channel, (AyyiPanel*)mixer);
	g_return_val_if_fail(bus, NULL);
	mixer__insert_master_strip(mixer, bus);
	return bus;
}


static bool
mixer__insert_master_strip (MixerWin* mixer, AyyiMixerStrip* strip)
{
	int n = mixer__get_n_strips(mixer);
	g_return_val_if_fail(!mixer->strips[n], false);

	strip->strip_num = n;
	mixer->strips[n] = strip;
	mixer__add_masterstrip_to_hbox(mixer, strip);

	mixer__strip_widths_update(mixer);

	return true;
}


/*
 *  Return a new list of Strip*. Must be freed by caller.
 *
 *	See also mixer__foreach_strip macro
 */
static GList*
mixer__get_strips_for_channel (MixerWin* mixer, const AMChannel* channel)
{
	GList* strips = NULL;
	int c; for(c=0;c<AM_MAX_CHS;c++){
		if(mixer->strips[c]){
			AyyiMixerStrip* s = mixer->strips[c];
			if(s->channel == channel) strips = g_list_append(strips, mixer->strips[c]);
		}
	}
	return strips;
}


static int
mixer__get_n_strips (MixerWin* mixer)
{
	// TODO check that mixer->strips is non-sparse.

	int s; for(s=0;s<AM_MAX_CHS;s++){
		if(!mixer->strips[s]) break;
	}
	return s;
}


static int
mixer__get_strip_index (MixerWin* mixer, AMChannel* channel)
{
	int i; for(i=0;i<AM_MAX_CHS;i++){
		AyyiMixerStrip* strip = mixer->strips[i];
		if(!strip) break;
		if(strip->channel == channel) return i;
	}
	return 0;
}


static void
mixer_on_ready (AyyiPanel* panel)
{
	AyyiMixerWin* m = (AyyiMixerWin*)panel;
	GtkWidget* window = (GtkWidget*)panel->window;

	if(m->hzoom_scale) return;

	//prevent gtk from making the window too wide:
	int tot_strips = song->channels->array->len;
	gtk_window_set_default_size (GTK_WINDOW(window), MAX(tot_strips*60, 800), 520); //gtk overrides this if it is too small.

	//------------------------------toolbar---------------------------------------

	GtkObject* hadj_ctl = gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0);
	GtkObject* vadj_ctl = gtk_adjustment_new(100.0, 50.00, 200.0, 1.0, 10.0, 10.0);
	GtkWidget* viewport = gtk_viewport_new(GTK_ADJUSTMENT(hadj_ctl), GTK_ADJUSTMENT(vadj_ctl));
	gtk_widget_set_size_request(viewport, 10, -1);
	panel_pack(viewport, EXPAND_FALSE);

	//horizontal zoom:
	//m->hzoom_scale = gtk_hscale_new (GTK_ADJUSTMENT (m->hzoom_adj));
	m->hzoom_scale = (GtkWidget*)ayyi_hscale_new (GTK_ADJUSTMENT (m->hzoom_adj));
	gtk_scale_set_value_pos(GTK_SCALE(m->hzoom_scale), GTK_POS_RIGHT);
	gtk_scale_set_digits(GTK_SCALE(m->hzoom_scale), 0);
	gtk_box_pack_start (GTK_BOX(panel->toolbar_box[0]), m->hzoom_scale, EXPAND_TRUE, FILL_TRUE, 0);
	g_signal_connect ((gpointer)m->hzoom_scale, "value-changed", G_CALLBACK(mixer_zoombar_on_value_changed), m);

	void mixer_zoombar_on_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer user)
	{
		dbg(1, "TODO set max size. w=%i", allocation->width);
		allocation->width = MIN(200, allocation->width);
	}
	g_signal_connect((gpointer)m->hzoom_scale, "size-allocate", G_CALLBACK(mixer_zoombar_on_allocate), m);

	//a resize eventbox underneath the toolbar:
	//not yet hooked up, but used for padding.
	GtkWidget* tb_size_ev = gtk_event_box_new();
	panel_pack(tb_size_ev, EXPAND_FALSE);
	gtk_widget_set_size_request(tb_size_ev, -1, 4);
	//g_signal_connect(tb_size_ev, "event", G_CALLBACK(toolbar_on_resize), NULL);

	//----------------------------------------------------------------------------

#ifdef USE_MULTICONTAINER
	void mixer_on_zoom_changed (GtkWidget* widget, double zoom, gpointer mixer){
		mixer__update_zoombar((MixerWin*)mixer);
	}
	g_signal_connect(MIXER_HBOX(m->hbox), "zoom-changed", G_CALLBACK(mixer_on_zoom_changed), m);
#else

	//box to hold mixer strips:
	m->hbox = gtk_hbox_new(FALSE, 0);
#endif

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(m->scrollwin), m->hbox);
	panel_pack(m->scrollwin, EXPAND_TRUE);

	m->strip_label_sizegroup = gtk_size_group_new(GTK_SIZE_GROUP_VERTICAL);

	void create_strips ()
	{
		AMIter iter;
		channel_iter_init(&iter);
		AMChannel* channel;
		while((channel = channel_next(&iter))){
			switch(channel->type){
				case AM_CHAN_INPUT:
					mixer__insert_new_strip(m, channel, -1);
					break;
				default:
					break;
			}
		}

		//add bus and master channels to the end:
		channel_iter_init(&iter);
		while((channel = channel_next(&iter))){
			switch(channel->type){
				case AM_CHAN_INPUT:
					break;
				case AM_CHAN_BUS:
				case AM_CHAN_MASTER:
					mixer__insert_new_master_strip(m, channel);
				break;
				default:
					perr ("unknown channel type: %i", channel->type);
					break;
			}
		}
	}

	create_strips();

	am__mixer_show_routing();

	g_signal_connect((gpointer)window, "size-request", G_CALLBACK(mixer_on_sizereq), m);

	m->contextmenu = mixer_menu__new(panel); // popup menu

	ayyi_panel_on_open(panel);

	mixer__redraw(panel);
	if(_debug_ > 1) mixer__print_strips(m);

	am_song__connect("tracks-change", G_CALLBACK(mixer__on_tracks_change), m);
	am_song__connect("song-load", G_CALLBACK(mixer__on_song_load), m);
	am_song__connect("song-unload", G_CALLBACK(mixer__on_song_unload), m);
	g_signal_connect(song->channels, "add", G_CALLBACK(mixer__on_channel_add), m);
	g_signal_connect(song->channels, "change", G_CALLBACK(mixer__on_channels_change), m);
	g_signal_connect(song->channels, "selection-change", G_CALLBACK(mixer__on_channel_selection_change), m);
}


static void**
mixer_get_config (AyyiPanel* panel)
{
	void** config = g_malloc0(sizeof(void*) * G_N_ELEMENTS(params));

	static float z;
	config[0] = (z = mixer__get_zoom((MixerWin*)panel), &z);

	return config;
}


static void
mixer_add_load_handlers (AyyiPanelClass* klass)
{
	void config_load_strips (yaml_parser_t* parser, const char* key, gpointer data)
	{
		AyyiPanelClass* panel_class = g_type_class_peek(AYYI_TYPE_MIXER_WIN);
		panel_class->loader.instances++;

		void config_load_strip (yaml_parser_t* parser, const char* key, gpointer data)
		{
			int* sp = (int*)data;
			dbg(2, "%i key=%s", *sp, key);

			typedef struct {
				AyyiPanel* mixer;
				int sp;
				int colour;
				int hidden;
				int sends_hidden;
			} C;

			C* c = AYYI_NEW(C,
				.mixer = windows__get_from_instance(AYYI_TYPE_MIXER_WIN, ((AyyiPanelClass*)g_type_class_peek(AYYI_TYPE_MIXER_WIN))->loader.instances),
				.sp = *sp,
				.colour = -1
			);

			YamlValueHandler vhandlers[] = {
				{"hidden", yaml_set_int, &c->hidden},
				{"sends_hidden", yaml_set_int, &c->sends_hidden},
				{"colour", yaml_set_int, &c->colour},
				{NULL}
			};
			yaml_load_mapping(parser, NULL, vhandlers, NULL);

			gboolean apply (gpointer _c)
			{
				C* c = _c;

				if(c->mixer){
					Strip* strip = ((MixerWin*)c->mixer)->strips[c->sp];
					if(strip){
						if(c->hidden      ) strip__hide      (strip);
						if(c->sends_hidden) strip__show_sends(strip, FALSE);
						if(c->colour > -1 ) strip__set_colour(strip, c->colour);
					}
				}
				g_free(c);

				return G_SOURCE_REMOVE;
			}
			(*sp)++;
			g_idle_add(apply, c);
		}

		int s = 0;
		YamlHandler handlers[] = {
			{"", config_load_strip, &s},
			{NULL}
		};

		yaml_load_mapping(parser, handlers, NULL, NULL);
	}

	YamlHandler _handlers[] = {
		{"", config_load_strips, NULL},
		{NULL}
	};

	// Convert to pointer array
	YamlHandler** handlers = (YamlHandler**)g_malloc0(sizeof(YamlHandler) * 2);
	for(int i=0;i<2;i++){
		handlers[i] = g_new0(YamlHandler, 1);
		memcpy((YamlHandler*)handlers[i], &_handlers[i], sizeof(YamlHandler));
	}

#if 0
	YamlValueHandler vhandlers[] = {
		{NULL}
	};
#endif
	YamlValueHandler* vhandlers = g_malloc0(1 * sizeof(YamlValueHandler));

	klass->loader.handlers = handlers;
	klass->loader.vhandlers = (YamlValueHandler**)vhandlers;
}


static void
mixer_on_realise (GtkWidget* widget)
{
	((GtkWidgetClass*)ayyi_mixer_win_parent_class)->realize(widget);

	request_toolbar_update();
}


static void
mixer_on_sizereq (GtkWidget* widget, GtkRequisition* requisition, gpointer user_data)
{
	// note: currently its only the toolbar that is forcing the size.

	MixerWin* mixer = user_data;
	if(GTK_WIDGET_REALIZED(widget) && !mixer->resize_done){
		gtk_widget_set_size_request(widget, 50, -1);
		mixer->resize_done = true;
	}
}


static void
mixer_on_resize (GtkWidget* widget, GtkAllocation* allocation)
{
	MixerWin* mixer = AYYI_MIXER_WIN(widget);
	g_return_if_fail(AYYI_IS_MIXER_WIN(mixer));

	((GtkWidgetClass*)ayyi_mixer_win_parent_class)->size_allocate(widget, allocation);

	// there is currently a bug in the panel resizing when docked.
	// Below is a trap to prevent endless resize attempts:
	gboolean mixer_resize__on_timeout (gpointer _mixer)
	{
		MixerWin* mixer = _mixer;

		mixer->resize_count--;
		if(mixer->resize_count < -20){
			mixer->resize_count = 0;
			mixer->resize_timeout = 0;
			return G_SOURCE_REMOVE;
		}
		return G_SOURCE_CONTINUE;
	}
	if(!mixer->resize_count) mixer->resize_timeout = g_timeout_add(100, mixer_resize__on_timeout, mixer);
	if(mixer->resize_count++ > 50/* && count < 60*/){ pwarn("too many resize attempts!"); return; }

	if(!song->channels->array->len) return;

	Strip* rch = mixer->strips[0]; //get a strip for this window
	if(!rch) return; //it is valid for a mixer to have no strips.

#if 0
#ifndef USE_MULTICONTAINER
  GtkObject* hzoom_adj = mixer_win->hzoom_adj;
  if(mixer_win->hzoom < 1.0){ //only do this once.
    GtkWidget* box = rch->box;
    if(box){
      int width = box->allocation.width;
      gtk_adjustment_set_value (GTK_ADJUSTMENT(hzoom_adj), (double)width);
      mixer_win->hzoom = width;
      mixer_win->ch_base_width = width;
    }
  }

  //-----------------------------------------------------------------

  //set the approx minimum zoom level:
  int min_width = win->allocation.width / (AM_MAX_CHS + 1); //the 1 is for the master channel.
  /*struct _*/GtkAdjustment* a = GTK_ADJUSTMENT(hzoom_adj);
  a->lower = (double)min_width;
#endif

  show_widget_if(mixer->hzoom_scale, allocation->width > 350);

  request_toolbar_update();

  //show allocations:
  //dbg(0, "requisition widths: window=%i scrollwin=%i toolbar=%i", win->requisition.width, mixer_win->scrollwin->requisition.width, ((AyyiPanel*)mixer_win)->toolbar_box[0]->requisition.width);
#endif
}


static void
mixer__redraw (AyyiPanel* window)
{
	// ensure each strip is in sync with the model. Should be called after new window created, or song loaded.

	g_return_if_fail(song->channels);
	MixerWin* mixer = (MixerWin*)window;

	AyyiMixerStrip* s;
	for(int i=0;i<AM_MAX_CHS && (s = mixer->strips[i]);i++){
		dbg(2, "  i=%i", i);
		AMChannel* channel = s->channel;
		if(channel){
			dbg (2, "i=%i channel=%p type=%i", i, channel, channel->type);
			if(channel->type == AM_CHAN_INPUT){
				strip__update(s);
			}
		}
		else master_strip__update(s);
	}
}


static void
mixer__remove_strip (MixerWin* mixer, AMChannel* channel)
{
	int strip_idx = mixer__get_strip_index(mixer, channel);
	dbg (1, "deleting strip %i...", strip_idx);

	AyyiMixerStrip* deleted_strip = mixer->strips[strip_idx];
	g_return_if_fail(deleted_strip);

	// re-index the strips array for each window
	int i; for(i=strip_idx;i<AM_MAX_CHS;i++){
		Strip* s = mixer->strips[i];
		if(!s || !mixer->strips[i + 1]) break;
		mixer->strips[i] = mixer->strips[i + 1];
		s->strip_num = i;
	}
	mixer->strips[i] = NULL;

	// testing: reset all strip_num indexes (has some bad side effects?)
	AyyiMixerStrip* ss;
	for(i=0;(ss = mixer->strips[i]);i++){
		ss->strip_num = i;
	}

#ifdef DEBUG
	if(_debug_) mixer__print_strips(mixer);
#endif

	//check this:
#if 0
	int c; for(c=0;c<song->channels->len-1;c++){
		strip__set_channel(mixer->strips[c], g_ptr_array_index(song->channels, c));
	}
#endif

	// delete the widgets
	strip__destroy(deleted_strip);
}


#ifdef UNUSED
void
mixer__delete_strip__post (MixerWin* mixer, AMChannel* channel)
{
	void test_update_channel_mapping(MixerWin* mixer)
	{
		Strip* s;
		int c; for(c=0;(c<AM_MAX_CHS)&&((s = mixer->strips[c]));c++){
			Strip* strip = mixer->strips[c];
			AMChannel* ch = s->channel;
			strip__set_channel(strip, ch); //does this do anything?
			switch(strip->channel->type){
				case AM_CHAN_INPUT:
					strip__update(strip);
					break;
				default:
					master_strip__update(strip);
					break;
			}
			dbg(0, "s=%i ch=%i %s", c, ch->ident.idx, s->cha_name ? s->cha_name->str : "<>");
		}
	}
	test_update_channel_mapping(mixer);

	mixer__print_strips(mixer);
}
#endif


AyyiMixerStrip*
mixer__strip_next (MixerWin* mixer, AyyiMixerStrip* strip)
{
	// for iterating over the strip list.

	if(!strip) return mixer->strips[0];

	int next_idx = strip->strip_num + 1;
	if(next_idx >= AM_MAX_CHS) return NULL;
	return mixer->strips[next_idx];
}


static void
mixer__pluginboxes_update (MixerWin* mixer, AMChannel* ch)
{
	// currently doesnt deal with creating empty slots as needed.

	AyyiIdx ch_num = ch->ident.idx;

	Strip* s = mixer->strips[ch_num];
	if(!s->inserts) return;
	g_return_if_fail(s->inserts->len < 10);

	for(int slot=0;slot<s->inserts->len;slot++){
		strip__update_pluginbox(s, slot);
	}
}


static void
mixer__set_send_count (MixerWin* mixer, int val)
{
	g_return_if_fail(mixer);

	mixer->options.sendcount = val;
}


static void
mixer__set_rotary_aux (MixerWin* mixer, bool val)
{
	g_return_if_fail(mixer);

	mixer->options.rotary_aux = val;
}


static void
mixer__set_hzoom (MixerWin* mixer, float hzoom)
{
	g_return_if_fail(mixer);

	observable_point_set(((AyyiPanel*)mixer)->zoom, &hzoom, NULL);
}


static void
mixer__strip_widths_update (MixerWin* mixer)
{
	//sets the correct size_request for each mixer strip, following a change in size, or addition of new strips.

#ifndef USE_MULTICONTAINER
	int new_width = mixer->hzoom;

	int i;
	AMChannel* channel = NULL;
	for(i=0;i<song->channels->len;i++){
		channel = g_ptr_array_index(song->channels, i);
		if(channel->type != AM_CHAN_INPUT && channel->type != AM_CHAN_BUS) continue;

		Strip* strip = mixer->strips[i];
		if(strip){
			gtk_widget_set_size_request(strip->box, new_width * strip->hmag, -1);
		}
	}
	//gtk_range_set_value (hzoom_scale, (double)wid);

	//gtk_range_set_adjustment(GTK_RANGE(hzoom_scale), hzoom_adj);
#endif
}


/*
 *  Horizontal resizing of a complete mixer window from an adjustment.
 *
 *  @param - widget must be the mixer resize GtkScale.
 */
static void
mixer_zoombar_on_value_changed (GtkWidget* widget, MixerWin* mixer)
{
	g_return_if_fail(mixer);

	double val = gtk_range_get_value(GTK_RANGE(widget));
#ifdef USE_MULTICONTAINER
	mixer_hbox_set_zoom (MIXER_HBOX(mixer->hbox), val);

#else
	//printf("val: %f\n", val);
	//dbg(0, "old zoom=%f\n", mixer_win->hzoom);
	int new_width = val;
	int old_width = mixer->hzoom;

	mixer->hzoom = val;
	mixer__strip_widths_update(mixer);

	mixer->ch_base_width = mixer->ch_base_width * new_width / old_width;
#endif
}


static void
mixer__update_zoombar (MixerWin* mixer)
{
	g_return_if_fail(mixer);

	dbg(5, "adj=%p", mixer->hzoom_adj);
	GtkAdjustment* adj = GTK_ADJUSTMENT(mixer->hzoom_adj);
	MixerHBox* mixer_hbox = MIXER_HBOX(mixer->hbox);

	adj->value = mixer_hbox->zoom;
	adj->lower = mixer_hbox->min_zoom * (mixer_hbox)->strip_width;
	gtk_adjustment_changed(adj);

	dbg(2, "val=%.2f lower=%.2f", adj->value, adj->lower);
}


#ifndef USE_MULTICONTAINER
static void
strip_on_resize (GtkWidget* widget, GdkEvent* event, gpointer user_data)
{
  // callback for resize handles on individual mixer channel strips.

  g_return_if_fail(user_data); 
  //struct _MixerWin* mixer_win = user_data;

  static int        initial_width;
  //static int        initial_win_width;
  //static GtkWidget* vport;          //the (equivalent of) the overall mixer window viewport
  int               new_width;
  //static double     initial_hzoom;
  static double     initialx;
  //static double new_ch_width_nz;  //width (non zoomed), as stored in chA
  double            dx_z;           //the total amount the mouse has moved.
  double            old_dx_z=0;     //the amount the mouse had moved on the previous call.
  //static int        strip_num;
  double            mouse_x;
  static int        dragging;
  //int               i;
  GdkCursor*        curs;
  static GtkWidget* box;            //the channel strip vbox.

  mouse_x = event->button.x;

  switch (event->type)
  {
    case GDK_BUTTON_PRESS:
      switch(event->button.button)
      {
        case 1:
          if (event->button.state & GDK_SHIFT_MASK)
          {
            //gtk_object_destroy(GTK_OBJECT(item));
          }
          else
          {
#ifdef TEMP
            //get existing strip properties:
            strip_num = -1;
            for(i=0;i<song->channels->len;i++){
              if(mixer_win->strips[i]->rsiz_handle == widget) strip_num = i;
            }
            if(strip_num == -1){
              perr ("strip_num not set.");
              strip_num = 0;
            }

            box = mixer_win->strips[strip_num]->box;
            //printf("box: %i\n", box);
            initial_width = box->allocation.width;
            initial_hzoom = mixer_win->hzoom;

            //get current mixer window width:
            //not sure how to access the viewport, so using the child hbox instead:
            vport = mixer_win->hbox;
            initial_win_width = vport->allocation.width;

            printf("channel resize init. ch=%i, width=%i\n", strip_num, initial_width);

#endif
           dragging = TRUE;
         }
         break;

         default:
           break;
           }
         break;


       case GDK_MOTION_NOTIFY:
         if (dragging && (event->motion.state & GDK_BUTTON1_MASK))
         {
           dx_z = mouse_x - initialx;
           if(dx_z==old_dx_z) break; //mouse hasnt moved. nothing to do.

           new_width = initial_width + dx_z;
           if(new_width < MIN_CH_WIDTH) new_width = MIN_CH_WIDTH;

		   //int new_win_width = initial_win_width + dx_z;
		  
	       //printf("motion! x=%f dx_z=%f new_width=%i\n", mouse_x, dx_z, new_width);

           //resize the strip:
		   //FIXME this works, but is not smooth, as gtk is fighting to autosize it.
		   // -modifying the window allocation width by the same amount doesnt do it.
		   // -next idea: the mixer resize function is being called. We should prevent that.
		   
           //gtk_widget_set_size_request(vport, new_win_width, -1);
           gtk_widget_set_size_request(box, new_width, -1);

		   //what is the new hmag?
		   //   total mixer width = ch1_width + ch2_width ...etc
		   //   where chN->width = base_width * chN->hmag
		   //
		   //   initially: chN->width = adj_val
		   //   and:       chN->hmag = 1.0
		   //   so:        base_width = chN->width
		   //   -we can use this value until the mixer window scrollwin is resized
		   //   -when it is resized we can adjust the base_width accordingly.
		   //
#ifdef TEMP
		   mixer_win->strips[strip_num]->hmag = new_width / mixer_win->ch_base_width;
#endif

           /* 
		   printf("mixer mixer_hzoom=%2.2f newzoom=%.2f ch->hmag=%.2f window.w=%i\n", 
		   			mixer_win->hzoom, initial_hzoom * new_width / initial_width, 
					mixer_win->strips[strip_num]->hmag,
					new_win_width);
           */

           //--------------------------------------------
		   //override default v size of rotary sends.
		   //-this shouldnt be necc. Its a bug in the rotary widget.
		   //printf("strip_resize(): requesting rotary height: %i\n", MIN(new_width,30));
           //FXIME only do for rotaries!
           /*
           for(i=0;i<mixer_win->sendcount;i++){
             gtk_widget_set_size_request(mixer_win->strips[strip_num]->wsend[i], -1, MIN(new_width,30));
           //gtk_widget_set_size_request(mixer_win->strips[strip_num]->wsend[1], -1, MIN(new_width,30));
           //gtk_widget_set_size_request(mixer_win->strips[strip_num]->wsend[2], -1, MIN(new_width,30));
           }
           */
   
           //--------------------------------------------
   
           old_dx_z = dx_z;
         }
       break;


     case GDK_BUTTON_RELEASE:
       curs = gdk_cursor_new(GDK_LEFT_PTR);
       gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
       gdk_cursor_destroy(curs);
       dragging = FALSE;

       // resize the canvas so scrollbars are correct:
       // arrange_scrollregion_update();
       break;

     case GDK_ENTER_NOTIFY:
       curs = gdk_cursor_new(GDK_SB_H_DOUBLE_ARROW);
       gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
       gdk_cursor_destroy(curs);
   
       break;
   
     case GDK_LEAVE_NOTIFY:
       if(dragging == FALSE){
         curs = gdk_cursor_new(GDK_LEFT_PTR);
         gdk_window_set_cursor(gtk_widget_get_parent_window(widget), curs);
         gdk_cursor_destroy(curs);
       }
   
       break;
   
     default:
       break;
   }
}
#endif


gint
mixer__drag_drop(GtkWidget* widget, GdkDragContext* drag_context, gint x, gint y, guint time, gpointer user_data)
{
	// Something has been dropped on the mixer.

	printf("drop!\n");

	return false;
}


static void
mixer_menu__hide_strip(GtkWidget* widget, MixerWin* mixer)
{
	// Hide the selected mixer strip(s).
	// Called by the context menu.

	PF;

	g_return_if_fail(mixer);

	GList* l = mixer->selectionlist;
	for(;l;l=l->next){
		int cnum = GPOINTER_TO_INT(l->data);
		strip__hide(mixer->strips[cnum]);
	}

	g_list_clear(mixer->selectionlist);
}


int
mixer__n_visible_strips(MixerWin* mixer)
{
	int n = 0;
	Strip* strip = NULL;
	while((strip = mixer__strip_next(mixer, strip))){
		if(!strip__is_hidden(strip)) n++;
	}
	return n;
}


static void
mixer__show_all_strips(MixerWin* mixer)
{
	Strip* strip = NULL;
	while((strip = mixer__strip_next(mixer, strip))){
		strip__show(strip);
	}
}


static void
mixer__on_song_load (GObject* _song, gpointer user_data)
{
	MixerWin* mixer = user_data;
#if 0
	mixer__reallocate_chs(mixer);
#endif
    mixer__redraw((AyyiPanel*)mixer);
}


static void
mixer__on_song_unload (GObject* _song, gpointer _mixer)
{
	PF;
	MixerWin* mixer = _mixer;

	AMIter i;
	AMChannel* channel; channel_iter_init(&i);
	while((channel = channel_next(&i))){
		mixer__remove_strip(mixer, channel);
	}
}


static void
mixer__on_tracks_change (GObject* _song, TrackNum t_num1, TrackNum t_num2/*, AyyiPanel* sender*/, int change_type, gpointer _mixer_win)
{
	PF;
	MixerWin* mixer = _mixer_win;

	g_return_if_fail(t_num1 > -1);
	g_return_if_fail(t_num2 < AM_MAX_CHS + 1);

 	if((change_type & AM_CHANGE_MUTE)){
		TrackNum t; for(t=t_num1;t<=t_num2;t++){
			const AMChannel* channel = am_track__lookup_channel(song->tracks->track[t]);
			mixer__update_mute(mixer, channel);
		}
	}

	if((change_type & AM_CHANGE_OUTPUT)){
		for(TrackNum t=t_num1;t<=t_num2;t++){
			if(!mixer->strips[t]){ perr("bad strip index: %i", t); continue; }
			const AMChannel* channel = am_track__lookup_channel(song->tracks->track[t]);
			mixer__update_outputs(mixer, channel);
		}
	}

	if((change_type & AM_CHANGE_SOLO)){
		TrackNum t; for(t=t_num1;t<=t_num2;t++){
			const AMChannel* channel = am_track__lookup_channel(song->tracks->track[t]);
			mixer__update_solo(mixer, channel);
		}
	}

	if((change_type & AM_CHANGE_ARMED)){
		TrackNum t; for(t=t_num1;t<=t_num2;t++){
			const AMChannel* channel = am_track__lookup_channel(song->tracks->track[t]);
			mixer__update_armed(mixer, channel);
		}
	}
}


static double
mixer__get_zoom (MixerWin* mixer)
{
#ifdef USE_MULTICONTAINER
	dbg(2, "zoom=%.2f", MIXER_HBOX(mixer->hbox)->zoom);
	return MIXER_HBOX(mixer->hbox)->zoom;
#else
	pwarn("FIXME for non MIXER_HBOX");
	return 1.0;
#endif
}


void
mixer__set_meter_type(AyyiPanel* panel, gpointer _type)
{
	int type = GPOINTER_TO_INT(_type);
	dbg (1, "meter_type=%i", type);

	// currently, meter type is stored per window, not per channel.

	//GList* list = NULL; //TODO
	if(AYYI_IS_MIXER_WIN(panel)){
		//list = ((MixerWin*)(panel))->selectionlist;
		panel->meter_type = type;
	}else if (AYYI_IS_INSPECTOR_WIN(panel)){
		//list = NULL; //FIXME
	}else{
		perr ("unexpected window type: %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel))); return;
	}

	int l;
	mixer__print_channels();
	switch(type){
		case METER_BST:
			//bst is different in that it can be multichannel
			for(l=0;l<song->channels->array->len;l++){
				Strip* s = ((MixerWin*)(panel))->strips[l];
				g_return_if_fail(s);
				dbg (0, "strip=%p\n", s);
				meter_destroy(s, -1);
				GtkWidget* meter = meter_new(s, 0);
				gtk_box_pack_start(GTK_BOX(s->fdr_bay), meter, TRUE, TRUE, 0); 
				//gtk_box_reorder_child(GTK_BOX(ch->fdr_bay), meter, 0); //move to left.
			}
			break;
    	default:
			for(l=0;l<song->channels->array->len;l++){
				Strip* strip = ((MixerWin*)(panel))->strips[l];
				g_return_if_fail(strip);
				dbg (2, "ch=%p\n", strip);
				meter_destroy(strip, -1);
				int n; for(n=strip->channel->nchans-1; n>=0; n--){
					dbg (0, "ch%i:%i new meter... channel=%p striptype=%i", strip->strip_num, n, strip->channel, strip->channel->type);
					GtkWidget* meter = meter_new(strip, n);
					dbg(0, "packing box:%p child:%p", strip->fdr_bay, meter);
					//FIXME crashes here when newtype==GTK - is it because jamin meters dont destroy properly? - crashes in gtk_vumeter_setup_colors() ?
					gtk_box_pack_start   (GTK_BOX(strip->fdr_bay), meter, TRUE, TRUE, 0); 
					gtk_box_reorder_child(GTK_BOX(strip->fdr_bay), meter, 0); //move to left.
					dbg (0, "pack done.\n");
				}
			}
			break;
	}
}


typedef struct _SelectionListContext SelectionListContext;

struct _SelectionListContext {
    MixerWin* mixer;
    GtkWidget* menu_item;
    void (*fn) (AyyiMixerStrip*, SelectionListContext*);
};

static void
mixer__selection_foreach (GtkWidget* widget, gpointer user_data)
{
	SelectionListContext* c = user_data;

	for(GList* l=c->mixer->selectionlist;l;l=l->next){
		AyyiMixerStrip* s = c->mixer->strips[GPOINTER_TO_INT(l->data)];
		c->fn(s, c);
	}
}


static MixerMenu*
mixer_menu__new (AyyiPanel* panel)
{
	//each mixer window (or inspector window) has its own contextmenu.

	//in some ways it would be nice to make this menu global.
	//      The menu changes slightly depending on the selection, so having one menu per strip
	//      or per window doesnt help anyway.
	//      It would help with inspector windows.
	//      The problem is embedding the parent widget in the callbacks to get around the problem of
	//      knowing who called the callback.

	//some menu items have the panel pointer embedded in them to get round the problem
	// of not being able to use get_toplevel() on them.
	// TODO change all the instance where we embed it into the menuitem to use the menu embed instead. see mixer_menu_sends_use_rotaries()

	int meter_type = 0;

	if(AYYI_IS_MIXER_WIN(panel)){
		meter_type = panel->meter_type;
		dbg(2, "meter_type=%i", meter_type);
	}else if (AYYI_IS_INSPECTOR_WIN(panel)){
      dbg(0, "inspector! FIXME?");
	}else{
		perr ("unexpected window type: %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));
		return NULL;
	}

	MixerMenu* m = AYYI_NEW(MixerMenu,
		.to_free = g_ptr_array_new_full(4, g_free)
	);

	GtkWidget* menu = m->menu = gtk_menu_new();
	g_object_set_data(G_OBJECT(menu), "sm_window", panel);

	// item 1
	GtkWidget* menu_item = gtk_menu_item_new_with_label("Hide Strip");
	gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
	g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(mixer_menu__hide_strip), panel);

	GtkWidget* add_check_item (GtkWidget* menu, const char* name, GCallback activate, gpointer user_data)
	{
		GtkWidget* menu_item = gtk_check_menu_item_new_with_label(name);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item), true);
		gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
		g_signal_connect(G_OBJECT(menu_item), "activate", activate, user_data);
		return menu_item;
	}

	void add_toggle_item (MixerWin* mixer, GtkWidget* menu, const char* name, void (*fn) (AyyiMixerStrip*, SelectionListContext*))
	{
		SelectionListContext* c = AYYI_NEW(SelectionListContext,
			.mixer = mixer,
			.fn = fn
		);
		menu_item = add_check_item(menu, name, G_CALLBACK(mixer__selection_foreach), c);
		c->menu_item = menu_item;
		g_ptr_array_add(m->to_free, c);
	}

	void toggle_insel (AyyiMixerStrip* strip, SelectionListContext* c)
	{
		g_return_if_fail(GTK_IS_CHECK_MENU_ITEM(c->menu_item));

		void (*toggle) (GtkWidget*) = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(c->menu_item)) ? gtk_widget_show : gtk_widget_hide;

		toggle(strip->insel);
	}
	add_toggle_item((MixerWin*)panel, menu, "Show InputSel", toggle_insel);

	void toggle_outsel (AyyiMixerStrip* strip, SelectionListContext* c)
	{
		g_return_if_fail(GTK_IS_CHECK_MENU_ITEM(c->menu_item));

		void (*toggle) (GtkWidget*) = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(c->menu_item)) ? gtk_widget_show : gtk_widget_hide;

		toggle(strip->outsel);
	}
	add_toggle_item((MixerWin*)panel, menu, "Show OutputSel", toggle_outsel);

	void toggle_insert (AyyiMixerStrip* strip, SelectionListContext* c)
	{
		strip__show_insert(strip, 0, gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(c->menu_item)));
	}
	add_toggle_item((MixerWin*)panel, menu, "Show Inserts", toggle_insert);

	void toggle_send (AyyiMixerStrip* strip, SelectionListContext* c)
	{
		strip__show_sends(strip, gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(c->menu_item)));
	}
	add_toggle_item((MixerWin*)panel, menu, "Show Sends", toggle_send);

	//-----------------------------

	// 6: metering
	menu_item = gtk_menu_item_new_with_label("Metering");
	gtk_container_add(GTK_CONTAINER(menu), menu_item);

	//sub-menu:
	GtkWidget* sub_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), sub_menu);

	GSList* group = NULL;

	GtkWidget* item1 = gtk_radio_menu_item_new_with_label (group, "Jamin");
	group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (item1));
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item1), meter_type == METER_JAMIN);
	gtk_menu_shell_append(GTK_MENU_SHELL(sub_menu), item1);
	g_object_set_data(G_OBJECT(item1), "panel", panel);

	GtkWidget* item2 = gtk_radio_menu_item_new_with_label (group, "GtkMeter");
	if(meter_type == METER_GTK) gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item2), TRUE);
	gtk_menu_shell_append(GTK_MENU_SHELL(sub_menu), item2);
	g_object_set_data(G_OBJECT(item2), "panel", panel);

	GtkWidget* item3 = gtk_radio_menu_item_new_with_label (group, "None");
	if(!meter_type) gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item3), TRUE);
	gtk_menu_shell_append(GTK_MENU_SHELL(sub_menu), item3);
	g_object_set_data(G_OBJECT(item3), "panel", panel);

	void _set_meter_type (GtkMenuItem* item, gpointer type)
	{
		// note: windows__get_panel_from_widget() works for context menus by using user_data embedded in the widget.
		AyyiPanel* panel = windows__get_panel_from_widget((GtkWidget*)item);
		g_return_if_fail(panel);
		mixer__set_meter_type(panel, type);
	}
	// because of the way radio menu items get activated, we have to do this after all items are added.
	g_signal_connect(G_OBJECT(item1), "activate", G_CALLBACK(_set_meter_type), (gpointer)METER_JAMIN);
	g_signal_connect(G_OBJECT(item2), "activate", G_CALLBACK(_set_meter_type), (gpointer)METER_GTK);
	g_signal_connect(G_OBJECT(item3), "activate", G_CALLBACK(_set_meter_type), (gpointer)METER_NONE);

	//-----------------------------

	// send options
	menu_item = gtk_menu_item_new_with_label("Sends");
	gtk_container_add(GTK_CONTAINER(menu), menu_item);
	// sub-menu
	sub_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), sub_menu);
	g_object_set_data(G_OBJECT(sub_menu), "sm_window", panel);

	GtkWidget* item = m->show_aux_values = gtk_check_menu_item_new_with_label("Show Values");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item), TRUE);
	gtk_menu_shell_append(GTK_MENU_SHELL(sub_menu), item);
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(mixer_menu__sends_show_values), NULL);

	item = m->use_rotaries = gtk_check_menu_item_new_with_label("Use Rotary Knobs");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item), TRUE);
	gtk_menu_shell_append(GTK_MENU_SHELL(sub_menu), item);
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(mixer_menu__sends_use_rotaries), NULL);

	//-----------------------------

	menu_item = gtk_menu_item_new_with_label("Unhide All");
	gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
	g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(mixer_menu__unhide_all), panel);

	gtk_widget_show_all(menu);
	return m;
}


void
mixer_menu__update (MixerWin* mixer)
{
	PF;
	MixerMenu* m = mixer->contextmenu;
	if(mixer->selectionlist){
		Strip* s = mixer->strips[GPOINTER_TO_INT(mixer->selectionlist->data)];
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(m->use_rotaries), s->use_rotaries_for_auxs);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(m->show_aux_values), s->show_aux_values);
	}
}


static AyyiPanel*
get_window_for_menuitem(GtkWidget* widget)
{
	GtkWidget* parent = widget;
	while((parent = gtk_widget_get_parent(parent))){
		AyyiPanel* window = g_object_get_data(G_OBJECT(parent), "sm_window");
		if(window) return window;
	}
	return NULL;
}


/*
 *  Toggle the visibility of text value for auxilliary sends.
 */
static void
mixer_menu__sends_show_values(GtkWidget* widget, gpointer user_data)
{
	//AyyiPanel* panel = app.active_panel;
	AyyiPanel* panel = get_window_for_menuitem(widget);
	g_return_if_fail(panel);

	GList* list = NULL;
	if(AYYI_IS_MIXER_WIN(panel)){
		list = ((MixerWin*)(panel))->selectionlist;
	}
	else if(AYYI_IS_INSPECTOR_WIN(panel)){
		list = NULL; //FIXME
	}
	else{
		perr ("unexpected window type: %s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel))); return;
	}

	int i = 0;
	for(;list;list=list->next){
		int chnum = GPOINTER_TO_INT(list->data);
		Strip* s = ((MixerWin*)(panel))->strips[chnum];
		s->show_aux_values = !s->show_aux_values;
		int send;
		for(send=0;send<MAX_SENDS;send++){
			if(s->wsend[send]){
				if(s->use_rotaries_for_auxs){
					gtk_knob_set_showvalue(s->wsend[send], !gtk_knob_get_showvalue(s->wsend[send]));
				}
				else gtk_scale_set_draw_value(GTK_SCALE(s->wsend[send]), s->show_aux_values);
			}
		}
		i++;
	}
}


static void
mixer_menu__sends_use_rotaries(GtkWidget* widget, gpointer user_data)
{
	GtkWidget* parent = widget;
	while((parent = gtk_widget_get_parent(parent))){
		AyyiPanel* panel = g_object_get_data(G_OBJECT(parent), "sm_window");
		if(panel){
			if(AYYI_IS_MIXER_WIN(panel)){
				MixerWin* mixer = (MixerWin*)panel;
				int send_count = mixer->options.sendcount;
				GList* l = mixer->selectionlist;
				for(;l;l=l->next){
					Strip* strip = mixer->strips[GPOINTER_TO_INT(l->data)];
					AMChannel* channel = strip->channel;
					dbg(0, "%i: strip=%p.", GPOINTER_TO_INT(l->data), strip);

					if (strip->use_rotaries_for_auxs) {
						dbg(0, "changing to linear...");
#if 0
						int i; for (i=0;i<send_count;i++) {
							GtkDial* dial = GTK_DIAL(strip->wsend[i]);
							observable_unsubscribe(dial->value, NULL, dial);

							GtkAdjustment* adj = GTK_DIAL(strip->wsend[i])->adjustment;
							GtkWidget* scale = gtk_hscale_new(adj);
							gtk_box_replace(strip->box, strip->wsend[i], scale);
							strip->wsend[i] = scale;
						}
#endif
					} else {

						AyyiChannel* ayyi_chan = strip->channel ? ayyi_mixer__channel_at(strip->channel->ident.idx) : NULL;

						int i; for (i=0;i<send_count;i++) {
							AyyiAux* aux = ayyi_chan ? ayyi_mixer__aux_get(ayyi_chan, i) : NULL;
							GtkWidget* knob = gtk_knob_new(channel->aux_level[i], (boolp)aux);
							gtk_box_replace(strip->box, strip->wsend[i], knob);
							strip->wsend[i] = knob;
							GTK_DIAL(knob)->on_right_click = aux_menu__popup;
						}
					}
					strip->use_rotaries_for_auxs = !strip->use_rotaries_for_auxs;
				}
			}
			break;
		}
	}
}


static void
mixer_menu__unhide_all(GtkWidget* menu, gpointer user_data)
{
	mixer__show_all_strips((MixerWin*)user_data);
}


static void
mixer__update_insert_size(MixerWin* mixer, AMChannel* channel)
{
	// following a plugin selection change, we may need to either add or remove an insert row.
	// @param ch_num is the channel that changed.

	PF;
	//inserts_print(ch_num);

	// if the last slot is not empty, we need to add another.
	GList* strips = mixer__get_strips_for_channel(mixer, channel);
	if(strips){
		GList* l = strips;
		for(;l;l=l->next){
			strip__update_insert_size(l->data);
		}
		g_list_free(strips);
	}
}


static void
mixer__update_outputs(MixerWin* mixer, const AMChannel* ch)
{
	GList* strips = mixer__get_strips_for_channel(mixer, ch);
	GList* l = strips;
	for(;l;l=l->next){
		strip__output_selector_update ((Strip*)l->data);
	}
	g_list_free(strips);
}


static void
mixer__update_mute(MixerWin* mixer, const AMChannel* ch)
{
	PF;
	GList* strips = mixer__get_strips_for_channel(mixer, ch);
	GList* l = strips;
	for(;l;l=l->next){
		strip__set_mute((Strip*)l->data, am_channel__is_muted(ch));
	}
	g_list_free(strips);
}


static void
mixer__update_solo(MixerWin* mixer, const AMChannel* ch)
{
	GList* strips = mixer__get_strips_for_channel(mixer, ch);
	GList* l = strips;
	for(;l;l=l->next){
		strip__set_solo((Strip*)l->data, am_channel__is_solod(ch));
	}
	g_list_free(strips);
}


static void
mixer__update_armed (MixerWin* mixer, const AMChannel* ch)
{
	GList* strips = mixer__get_strips_for_channel(mixer, ch);
	GList* l = strips;
	for(;l;l=l->next){
		strip__set_armed((Strip*)l->data, am_channel__is_armed(ch));
	}
	g_list_free(strips);
}


static void
mixer__selection_clear (MixerWin* mixer)
{
	GList* list = mixer->selectionlist;

	int len = g_list_length(list);
	int i; for(i=0;i<len;i++){
		int cnum = GPOINTER_TO_INT(list->data); // removing first entry.
		AyyiMixerStrip* s = mixer->strips[cnum];
		if(s) strip__unselect(s);
		list = g_list_remove(list, GINT_TO_POINTER(cnum));
	}
	if(list) perr ("selection list not cleared.");
	mixer->selectionlist = list;
}


void
mixer__select_channel (MixerWin* mixer, const AMChannel* ch)
{
	g_return_if_fail(ch);

	GList* strips = mixer__get_strips_for_channel (mixer, ch);

	if(!strips)
		pwarn("no strips for channel: %i", ch->ident.idx);
	else
		if(g_list_length(strips) != 1){ pwarn("%i strips found", g_list_length(strips)); }

	GList* l = strips;
	for(;l;l=l->next){
		Strip* s = l->data;
		mixer__selection_replace(mixer, s);
		break;
	}
	g_list_free(strips);
}


static void
mixer__on_channel_selection_change (GObject* _song, AyyiPanel* sender, gpointer user_data)
{
	MixerWin* mixer = user_data;
	AyyiPanel* panel = (AyyiPanel*)mixer;
	g_return_if_fail(AYYI_IS_MIXER_WIN(mixer));

	if((panel == sender) || panel->link){

		if(mixer->selectionlist){
  			Strip* strip = mixer->strips[GPOINTER_TO_INT(mixer->selectionlist->data)];
			if(strip){
				strip__set_selected(strip);
			}
		}
		mixer_hbox_on_selection_change(mixer->hbox);
	}
}


static void
mixer__on_channels_change(GObject* _song, gpointer user_data)
{
	//called in response to song->channels->change signal

	MixerWin* mixer = user_data;

	mixer__reallocate_chs(mixer);
}


/*
 *	Mark a single channel strip as being 'selected'
 *
 *	(this is not applicable in an inspector window, so dont attach it)
 */
void
mixer__selection_replace(MixerWin* mixer, AyyiMixerStrip* strip)
{
	g_return_if_fail(mixer);

	mixer__selection_clear(mixer);

	//strip__set_selected(strip);

	mixer->selectionlist = g_list_append(NULL, GINT_TO_POINTER(strip->strip_num));

	GList* channels = g_list_append(NULL, strip->channel);
	am_collection_selection_replace((AMCollection*)song->channels, channels, &mixer->panel);
}


void
mixer__selection_add(MixerWin* mixer, Strip* strip)
{
	//add a single strip, which is *not* already in it, to the selectionlist.

	g_return_if_fail(strip);

	GList* list = mixer->selectionlist;
	list = g_list_append(list, GINT_TO_POINTER(strip->strip_num));

	strip__set_selected(strip);

	mixer->selectionlist = list;

	am_collection_selection_replace((AMCollection*)song->channels, g_list_append(NULL, strip->channel), &mixer->panel);
}


void
mixer__selection_subtract(MixerWin* mixer, Strip* strip)
{
	//remove a channel from the selectionlist and give visual feedback.
	PF;

	mixer->selectionlist = g_list_remove(mixer->selectionlist, GINT_TO_POINTER(strip->strip_num));
	strip__unselect(strip);
  
	am_collection_selection_replace((AMCollection*)song->channels, g_list_append(NULL, strip->channel), &mixer->panel);
}


static void
mixer__select_all(AyyiPanel* panel)
{
	// primarily called from the CTL-A keyboard command.
	// see arr_select_all() for more comments.
	PF;

	g_return_if_fail(panel);

	if(AYYI_IS_MIXER_WIN(panel)){
		MixerWin* mixer = (MixerWin*)panel;
		mixer__selection_clear(mixer);
		Strip* strip = NULL;
		while((strip = mixer__strip_next(mixer, strip))){
			mixer__selection_add(mixer, strip);
		}
	}
	else perr ("wrong window type (%s).", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));
}


static double mid_mul = -20.0;
static double range = 96.0;

static void 
init_panning(double top, double mid, double bot)
{
	// use top,mid and bot to convert +-32768 int scale to |1| log() scale
	// use flag to invert scaler

	mid_mul = mid / log10(0.5);
	range = bot - top;
}


static void
mixer__zoom_in (AyyiPanel* panel, ZoomType zoom_type)
{
	MixerWin* mixer = (MixerWin*)panel;
	mixer_hbox_set_zoom (MIXER_HBOX(mixer->hbox), ((MixerHBox*)mixer->hbox)->strip_width * 1.1);
}


static void
mixer__zoom_out (AyyiPanel* panel, ZoomType zoom_type)
{
	MixerWin* mixer = (MixerWin*)panel;
	mixer_hbox_set_zoom (MIXER_HBOX(mixer->hbox), ((MixerHBox*)mixer->hbox)->strip_width / 1.1);
}


static void
k_zoom_in_hor (GtkAccelGroup* accel_group, gpointer user_data)
{
	AyyiPanel* panel = app->active_panel;

	if(panel) mixer__zoom_in(panel, ZOOM_H);
}


static void
k_zoom_out_hor (GtkAccelGroup* accel_group, gpointer user_data)
{
	PF;
	AyyiPanel* panel = app->active_panel;

	if(panel) mixer__zoom_out(panel, ZOOM_H);
}


static GtkWidget*
aux_menu__new ()
{
	// create the context menu for the arrange window track-control panel.

	GtkWidget* menu = gtk_menu_new ();

	gtk_widget_show_all(menu);

	return menu;
}


static int
get_aux_num_from_widget (GtkWidget* knob, Strip* s)
{
	for(int i=0;i<AYYI_AUX_PER_CHANNEL;i++){
		if(knob == s->wsend[i]) return i;
	}
	pwarn("not found.");
	return -1;
}


#define aux_num_valid(A) (A >= 0 && A < AYYI_AUX_PER_CHANNEL)

void
aux_menu__popup (GtkWidget* knob, GdkEventButton* event)
{
	PF;
	AyyiPanel* panel = app->active_panel;
	if(!panel || !IS_MIXER){ pwarn("!! type=%s", G_OBJECT_TYPE_NAME(panel)); return; }

	MixerWin* mixer = (MixerWin*)panel;
	if(!mixer->selectionlist) return;
	Strip* strip = mixer->strips[GPOINTER_TO_INT(mixer->selectionlist->data)];
	if(!strip) return;

	int aux_num = get_aux_num_from_widget(knob, strip);
	if(aux_num_valid(aux_num)){
		aux_menu__update(strip, aux_num);
		gtk_menu_popup(GTK_MENU(aux_menu.menu), NULL, NULL, NULL, NULL, event->button, (guint32)(event->time));
	}
}


static void
aux_menu__update (Strip* s, int aux_num)
{
	int channel_idx = s->channel->ident.idx;
	AyyiAux* aux = ayyi_mixer__aux_get_(channel_idx, aux_num);
	dbg(1, "aux_num=%i", aux_num);

	AuxMenuBusItem* aux_menu_bus_item_new(Strip* strip, int aux_num, AyyiConnection* connection)
	{
#if 1
		return AYYI_NEW(AuxMenuBusItem,
			.strip = strip,
			.aux_num = aux_num,
			.connection = connection
		);
#else
		AuxMenuBusItem* i = g_new0(AuxMenuBusItem, 1);
		i->strip = strip;
		i->aux_num = aux_num;
		i->connection = connection;
		return i;
#endif
	};

	void aux_menu_bus_item_free(AuxMenuBusItem** ami)
	{
		g_clear_pointer(ami, g_free);
	}

	for(GList* l=aux_menu.busses;l;l=l->next) g_free(l->data);
	g_clear_pointer(&aux_menu.busses, g_list_free);
	gtk_widget_destroy(aux_menu.menu);

	aux_menu.menu = gtk_menu_new();

	aux_menu_bus_item_free(&aux_menu.ami);
	aux_menu.ami = aux_menu_bus_item_new(s, aux_num, NULL);

	for(int i=0;i<G_N_ELEMENTS(aux_items);i++){
		struct _aux* item = &aux_items[i];
		item->widget = gtk_image_menu_item_new_with_label(item->name);
		GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, "cross", 16, 0, NULL);
		GtkWidget* image = gtk_image_new_from_pixbuf(pixbuf);
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item->widget), image);
		gtk_menu_shell_append (GTK_MENU_SHELL(aux_menu.menu), item->widget);

		g_signal_connect(G_OBJECT(item->widget), "activate", G_CALLBACK(aux_items[i].callback), aux_menu.ami);
	}

	gtk_widget_set_sensitive(aux_items[0].widget, (gboolean)!(aux && aux->flags & muted));
	gtk_widget_set_sensitive(aux_items[1].widget, (gboolean)(aux && aux->flags & muted));
	gtk_widget_set_sensitive(aux_items[2].widget, (boolp)aux);
	gtk_widget_set_sensitive(aux_items[3].widget, FALSE);

	menu_separator_new(aux_menu.menu);

	GSList* group = NULL;
	char label[32]; sprintf(label, "Off");
	int i = 0;
	AyyiConnection* c = NULL;
	do {
		if(i && !strcasestr(c->name, "send")) continue;
		if(c) snprintf(label, 31, "%.24s %02i", c->name, (char)i);

		AuxMenuBusItem* bus = aux_menu_bus_item_new(s, aux_num, c);
		aux_menu.busses = g_list_append(aux_menu.busses, bus);

		bus->menuitem = gtk_radio_menu_item_new_with_label (group, label);
		group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM(bus->menuitem));
		gtk_menu_shell_append (GTK_MENU_SHELL(aux_menu.menu), bus->menuitem);

		if(c && aux && aux->bus_num == c->shm_idx) gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(bus->menuitem), TRUE);
		else /*if(c)*/ g_signal_connect(G_OBJECT(bus->menuitem), "activate", G_CALLBACK(aux_menu__on_bus_select), bus);

		i++;
	} while((c = ayyi_song__audio_connection_next(c)));

	menu_separator_new(aux_menu.menu);

	GtkWidget* item = gtk_image_menu_item_new_with_label("Add Aux Bus");
	GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, "zoom_in", 16, 0, NULL);
	GtkWidget* image = gtk_image_new_from_pixbuf(pixbuf);
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL(aux_menu.menu), item);
	AuxMenuBusItem* mbi = aux_menu_bus_item_new(s, aux_num, NULL);
	g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(aux_menu__on_add_bus), mbi);

	gtk_widget_show_all(aux_menu.menu);
}


static void
aux_menu__on_mute (GtkMenuItem* menuitem, gpointer user_data)
{
	pwarn("not implemented?");
	AuxMenuBusItem* item = (AuxMenuBusItem*)user_data;
	g_return_if_fail(item);
	Strip* strip = item->strip;
	int aux_num = item->aux_num;
	//AyyiConnection* connection = item->connection;
	uint32_t obj_idx = strip->channel->ident.idx | (aux_num << 16);
	gboolean mute = TRUE; //FIXME
	ayyi_object_set_bool(AYYI_OBJECT_AUX, obj_idx, AYYI_MUTE, mute, NULL);
}


static void
aux_menu__prepost (GtkMenuItem* menuitem, gpointer user_data)
{
	pwarn("not implemented?");
	AuxMenuBusItem* item = (AuxMenuBusItem*)user_data;
	AyyiMixerStrip* strip = item->strip;
	uint32_t obj_idx = strip->channel->ident.idx | (item->aux_num << 16);
	gboolean post = 1;//GPOINTER_TO_INT(user_data);
	ayyi_object_set_bool(AYYI_OBJECT_AUX, obj_idx, AYYI_PREPOST, post, NULL);
}


static void
aux_menu__on_bus_select (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;

	AuxMenuBusItem* item = (AuxMenuBusItem*)user_data;

	if(!(GTK_CHECK_MENU_ITEM(menuitem))->active) return;

	MixerWin* mixer = (MixerWin*)item->strip->panel;
	if(!mixer->selectionlist) return;

	Strip* strip = mixer->strips[GPOINTER_TO_INT(mixer->selectionlist->data)];
	if(!strip) return;

	int connection = item->connection ? item->connection->shm_idx : CONNECTION_OFF;

	am_channel__set_aux_output(strip->channel, item->aux_num, connection, NULL, NULL);
}


static void
aux_menu__on_add_bus (GtkMenuItem* menuitem, gpointer user_data)
{
	PF;
	AuxMenuBusItem* i = (AuxMenuBusItem*)user_data;
	g_return_if_fail(i);
	am_aux__add(i->strip->channel, i->aux_num, NULL, NULL);
}


extern bool strip_check (Strip*);

static void
mixer__on_channel_add (GObject* _song, AMChannel* new_channel, MixerWin* mixer)
{
	PF;
	int pos = -1;
	Strip* strip = new_channel->type == AM_CHAN_MASTER
		? mixer__insert_new_master_strip (mixer, new_channel)
		: mixer__insert_new_strip(mixer, new_channel, pos);
	strip_check(strip);
}


static void
mixer__on_channel_delete (GObject* _song, AMChannel* channel, gpointer user_data)
{
	PF;
	mixer_foreach {
		mixer__remove_strip(mixer, channel);
	} end_mixer_foreach
}


static void
mixer__on_track_selection_change (Observable* o, AMVal val, gpointer _mixer)
{
	MixerWin* mixer = _mixer;
	AyyiPanel* panel = _mixer;

	if(panel->link){
		if(am_tracks->selection){
			AMTrack* track = am_collection_selection_first(am_tracks);
			if(track){
				const AMChannel* ch = am_track__lookup_channel(track);
				if(ch) mixer__select_channel(mixer, ch);
			}
		}
	}
}


static void
mixer__on_channel_change (GObject* _song, AMChannel* channel, AyyiPanel* sender, gpointer user_data)
{
	PF;
	mixer_foreach {
		int s; for(s=0;s<AM_MAX_CHS;s++){
			Strip* strip = mixer->strips[s];
			if(strip && strip->channel == channel){
				strip__auxs_update(strip);
			}
		}
	} end_mixer_foreach
}


static void
mixer__on_plugin_changed (GObject* _song, AMChannel* ch, gpointer user_data)
{
	mixer_foreach {
		mixer__pluginboxes_update(mixer, ch);
		mixer__update_insert_size(mixer, ch);
	} end_mixer_foreach;
}


static void
mixer__on_tracks_change_by_list (GObject* _song, GList* tracks, AMChangeType change_type, AyyiPanel* sender_win, gpointer user_data)
{
	// @param tracks - list of AMTrack*.

	if((change_type & AM_CHANGE_OUTPUT)){
		dbg(2, "output changed...");
		mixer_foreach {
			for(GList* l=tracks;l;l=l->next){
				AMTrack* trk = l->data;
				TrackNum t = am_track_list_position(song->tracks, trk);
				strip__output_selector_update (mixer->strips[t]); //TODO check this is the correct strip index.
			}
		} end_mixer_foreach
	}
}


void
mixer__print_channels ()
{
	printf("%s():\n", __func__);

	// we also show corresponding information for the first mixer window.
	MixerWin* mixer = NULL;
	mixer = MIXER_FIRST;

	printf("               type nchans core nplg     strip                  has_pan\n");

	for(int i=0;i<song->channels->array->len;i++){
		AMChannel* channel = g_ptr_array_index(song->channels->array, i);
		AyyiChannel* ac = ayyi_mixer__channel_at(channel->ident.idx);
		AyyiTrack* shared = ayyi_song__audio_track_at(channel->ident.idx);

		char name[AYYI_NAME_MAX];
		g_strlcpy(name, shared ? shared->name : "[no track]", AYYI_NAME_MAX);

		printf(" %2i %p %5i %6i %4i %4i %9p %16s %6i\n", i, channel, channel->type, channel->nchans, channel->ident.idx, ayyi_channel__count_plugins(ac), mixer ? mixer->strips[i] : NULL, name, ac ? ac->has_pan: 0);
	}
}


