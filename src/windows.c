/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define __window_c__

#include "global.h"
#include <gdk/gdkkeysyms.h>

#include "model/model_types.h"
#include "window.h"
#include "support.h"
#include "windows.h"
#include "panels/arrange.h"
#include "toolbar.h"
#include "settings.h"
#include "statusbar.h"
#include "icon.h"
#include "style.h"

extern SMConfig* config;

extern void        register_panels                     ();

static GHashTable* windows__new_count_table_from_shell (AyyiWindow*) __attribute__((unused));
static void        windows__count_table_print          (GHashTable*);
static void        windows__on_dock_change             (GdlDockMaster*, gpointer);
static void        on_style_change                     (GtkWidget*);


typedef struct
{
	GType   gtype;
	void    (*init)();
} DefaultPanelClass;

#include "register.c"

GList*         windows;
AyyiPanelClass window_classes[WINDOW_TYPE_MAX];


void
windows_init ()
{
	windows = NULL; // initialise the list of open windows

	// list of panel types hashed by GType.
	app->panel_classes = g_hash_table_new(g_int64_hash, g_int_equal);

	register_panels();

	for (int i=1;i<G_N_ELEMENTS(default_panel_class_types);i++) {
		DefaultPanelClass* dp = &default_panel_class_types[i];
#ifdef DEBUG
		if (!dp->gtype) perr("gtype not set! %i", i);
#endif
		g_type_class_unref (g_type_class_ref (dp->gtype));
		if (dp->init) dp->init();
		g_type_class_unref (g_type_class_ref (dp->gtype));

		g_hash_table_insert(app->panel_classes, &dp->gtype, &window_classes[i]);
	}

#if 0
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		AyyiPanelClass* window_class = value;
		PanelType panel_type = *((gint64*)key);
		dbg(0, "  %zu=%p", *((gint64*)key), value);
	}
#endif
#if 0
	void each(gpointer key, gpointer value, gpointer user_data)
	{
		PanelType type = *((gsize*)key);
		dbg(0, "  %s", g_type_name(*((gsize*)key)));
	}
	g_hash_table_foreach(app->panel_classes, each, NULL);
#endif

	windows__add_type(default_panel_class_types[TYPE_METER].gtype);

	toolbar_init();

	void on_new_window (Observable* o, AMVal val, gpointer _)
	{
		ArrayObservable* array = (ArrayObservable*)o;
		if(array->change != OBSERVABLE_ADD) return;

		AyyiWindow* window = val.p;
		dbg(1, "%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(window)));

		windows = g_list_append(windows, window);

		gtk_window_add_accel_group(GTK_WINDOW(window), app->global_accel_group); // set global keyboard shortcuts

		void window__on_style_set (GtkWidget* widget, GtkStyle* previous_style, gpointer user_data)
		{
			if(!app->styles_are_set){
				get_style_font();
				on_style_change(widget);
			}

			GdkPixbuf* pixbuf = gtk_icon_theme_load_icon(icon_theme, "arrange", 96, 0, NULL);
			gtk_window_set_icon((GtkWindow*)widget, pixbuf);
			g_object_unref(pixbuf);
		}
		g_signal_connect(G_OBJECT(window), "style-set", G_CALLBACK(window__on_style_set), NULL);

		void window__on_realise (GtkWidget* widget, AyyiWindow* window)
		{
			static bool first = true;

			if(first){
				first = false;

				int get_layout_height (GtkWidget* widget)
				{
					PangoLayout* layout = gtk_widget_create_pango_layout(widget, "bygA1");
					char font_string[64];
					get_font_string(font_string, 0);

					PangoFontDescription* font_desc = pango_font_description_from_string(font_string);
					pango_layout_set_font_description(layout, font_desc);

					PangoRectangle ink_rect;
					PangoRectangle logical_rect;
					pango_layout_get_pixel_extents(layout, &ink_rect, &logical_rect);
					dbg(2, "line_height=%i %i", ink_rect.height, logical_rect.height);
					return MAX(ink_rect.height, logical_rect.height);
				}

				app->line_height = get_layout_height(widget);
				get_style_font();
				g_signal_emit_by_name (app, "styles-available", NULL);
			}
		}

		g_signal_connect(G_OBJECT(window), "realize", G_CALLBACK(window__on_realise), window);

		void windows_on_panel_finalize (gpointer _window, GObject* was_panel)
		{
			PF;
			AyyiWindow* window = _window;

			if(was_panel == (GObject*)app->active_panel){
				app->active_panel = NULL;
			}

			if(g_list_find(windows, window)){
				g_signal_emit_by_name(app, "panel-list-changed");
			}
		}

		/*
		 *  This will only be called _once_ for each panel
		 */
		void windows_on_panel_added (GObject* window, AyyiPanel* panel, gpointer user_data)
		{
			dbg(1, "%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));

			g_object_weak_ref((GObject*)panel, windows_on_panel_finalize, window);

			void on_parented (gpointer _, gpointer panel)
			{
				GtkWidget* top = gtk_widget_get_toplevel(panel);
				g_return_if_fail(AYYI_IS_WINDOW(top));
				((AyyiPanel*)panel)->window = (AyyiWindow*)top;
				config_load_window_instance(panel);
			}
#ifdef DEBUG
			g_return_if_fail(!GDL_DOCK_ITEM(panel)->parented->children);
#endif
			am_promise_add_callback(GDL_DOCK_ITEM(panel)->parented, on_parented, panel);
		}
		g_signal_connect(window, "panel-added", (GCallback)windows_on_panel_added, NULL);

		void windows_on_panel_parented (GObject* window, AyyiPanel* panel, gpointer user_data)
		{
			dbg(1, "%s", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(panel)));

			panel->window = (AyyiWindow*)window;

			g_signal_emit_by_name(app, "panel-list-changed");
		}
		g_signal_connect(window, "panel-parented", (GCallback)windows_on_panel_parented, NULL);

		void window_finalize_notify (gpointer user_data, GObject* was_window)
		{
			windows = g_list_remove(windows, was_window);
		}
		g_object_weak_ref((GObject*)window, window_finalize_notify, NULL);
	}
	g_type_class_unref (g_type_class_ref (AYYI_TYPE_WINDOW));
	observable_subscribe(windows_, on_new_window, NULL);
}


/*
 *  Register a new panel type
 *  Only needed for dynamic panel types (eg plugins)
 */
void
windows__add_type (PanelType type)
{
	dbg(2, "%zu", type);
	g_type_class_unref (g_type_class_ref (type));

	PanelType* dtype = g_malloc(sizeof(PanelType*));
	*dtype = type;
	g_hash_table_insert(app->panel_classes, dtype, NULL);
}


static char*
dock_object_get_depth ()
{
	static char str[][3] = {" * "};
	return str[0];
}


#define DOCK_NAME(A) (A ? (A->name ? A->name : G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(A))) : "null")
struct _done
{
    ConfigWindow*  cw;
    GdlDockObject* obj;
};

#if 0
static int** docktree_lookup (GdlDockObject* dock_object, GList* done_list)
{
    GList* l = done_list;
    for(;l;l=l->next){
      struct _done* d = l->data;
      //dbg(0, "  d=%p", d);
      if(d->obj == dock_object) return (int**)d->cw->docktree;
    }
    return NULL;
}
#endif

	static gboolean dockpath_is_same(int** path1, int** path2, int n)
	{
		gboolean same = true;
		int i=0; for(;i<16;i++){
			if(i == n) break;
			if(path1[i] != path2[i]) return false;
		}
		return same;
	}

	struct _aa {
		int*           depth;
		int            idx;
		ConfigWindow*  panel;
		gboolean*      finished;
		GdlDockObject* target;
		GList*         done_list;
		int**          pathinfo;
	};
	static void get_children (GdlDockObject* object, ConfigWindow* panel, gboolean* finished, int** pathinfo, int* depth, int idx, GdlDockObject* target, GList* done_list);

	static void children_foreach (GdlDockObject* object, struct _aa* a)
	{
		a->pathinfo[*(a->depth)]++;
		gboolean path_match = dockpath_is_same((int**)a->panel->docktree, (int**)a->pathinfo, a->idx + 1);
		if(path_match){
			*(a->finished) = true;
			a->target = object;
			dbg(0, "found!");
			if(*(a->depth) > a->idx - 1){ //cough!
				pwarn("found item is at wrong depth. using parent...");
				a->target = gdl_dock_object_get_parent_object(object);
				g_return_if_fail(GDL_IS_DOCK_OBJECT(a->target));
			}
		}
		else if(1 /*|| pathinfo[*depth] == panel->docktree[*depth]*/) get_children(object, a->panel, a->finished, a->pathinfo, a->depth, a->idx, a->target, a->done_list);
	}

    static void get_children (GdlDockObject* object, ConfigWindow* panel, gboolean* finished, int** pathinfo, int* depth, int idx, GdlDockObject* target, GList* done_list)
    {
      g_return_if_fail(!*finished);

      (*depth)++;
      if(*depth == idx){
        //if target is GdlDockPaned, it won't be in the done_list, and won't have a docktree.
        //-using the parent doesnt always work because there isnt always one with a docktree.
#if 0
        int** dt = docktree_lookup(target, done_list);
        if(!dt){
        }
#endif
        //dbg(0, "using target=%s %s", DOCK_NAME(object), dock_info_to_string((int**)pathinfo));
      }

      if(!*finished){
        if(gdl_dock_object_is_compound(object)){
          struct _aa aa = {depth, idx, panel, finished, target, done_list, pathinfo};
          gtk_container_foreach(GTK_CONTAINER (object), (GtkCallback)children_foreach, &aa);
        }
        else dbg(2, "leaf. depth=%i.", *depth);
      }
      (*depth)--;
    }

/*
 *  Return the AyyiPanel for the window containing the given widget.
 *
 *  Note that this doesnt work for context menus.
 */
AyyiPanel*
windows__get_panel_from_widget (GtkWidget* widget)
{
	g_return_val_if_fail(widget, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);

	GtkWidget* _window = gtk_widget_get_toplevel(widget);

	/*
	GtkWindow* t_parent = gtk_window_get_transient_for((GtkWindow*)window);
	if(t_parent) dbg(0, "has transient parent!"); else dbg(0, "transient not set"); //should it be set?
	*/
	GtkWindowType type;
	g_object_get(_window, "type", &type, NULL);
	if(type == GTK_WINDOW_POPUP) return NULL;

	// new code that works for windows with multiple panels
	// TODO better to use gtk_widget_get_ancestor() ?
	{
		GtkWidget* parent = widget;
		do {
			dbg(3, "  widget=%s %i", G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(parent)), AYYI_IS_PANEL(parent));
			if(AYYI_IS_PANEL(parent)){
				AyyiPanel* panel = (AyyiPanel*)parent;
				char type[AYYI_SHORT_NAME_MAX]; ayyi_panel_print_type(panel, type);
				dbg(3, "found panel '%s %i' %s", type, ayyi_panel_get_instance_num(panel), GDL_DOCK_OBJECT(parent)->name);
				return panel;
			}
		}
	    while((parent = gtk_widget_get_parent(parent)));
	}
	dbg(0, "falling back to (unreliable) old method...");

//#ifdef USE_OLD_CODE_THAT_DOESNT_WORK_WITH_DOCKED_PANELS
	g_return_val_if_fail(_window, NULL);

	panel_foreach {
		if(window == (AyyiWindow*)_window){
			if(_debug_ > 1 && g_list_length(window->dock->panels) > 1){
				pwarn("FIXME doesnt make sense for shells with multiple panels.");
			}
			return panel;
		}
	} end_panel_foreach;
//#endif

	windows__print();
	pwarn ("window not found. widget=%p gtk_window=%p", widget, _window);
	return NULL;
}


/*
 *  Return the AyyiPanel for the currently active toplevel window.
 */
AyyiPanel*
windows__get_active ()
{
	// under certain circumstance, no window may be active:
	//  -a dnd operation from another application.
	//  -a popup menu is open.

	int active = 0;

	// how many active windows are there?
	int count = 0;
	AyyiWindow* active_shell = NULL;
	bool realised = true;
	for(GList* l = windows;l;l=l->next){
		AyyiWindow* window = l->data;
		if(!GTK_WIDGET_REALIZED(window)){ realised = false; continue; }
		g_object_get(window, "has-toplevel-focus", &active, NULL);
		if(active){
			active_shell = window;
			count++;
		}
	}
	g_return_val_if_fail(count == 1, NULL);
	if(!realised) return pwarn ("not realized!"), NULL;
	g_return_val_if_fail(active_shell, NULL);

	AyyiWindow* window = active_shell;
	GtkWidget* focussed = gtk_window_get_focus(GTK_WINDOW(window));
	if(!focussed){
		pwarn ("no widget focussed. active_shell='%s'", GDL_DOCK_OBJECT(window->dock)->name);

		if(active_shell->dock->panels && g_list_length(active_shell->dock->panels) == 1){
			// return the window's only panel.
			return active_shell->dock->panels->data;
		}

		return app->active_panel;
	}

#ifdef DEBUG
	if(!active) pwarn ("active window not found");
#endif

	return windows__get_panel_from_widget(focussed);
}


/*
 *  Find a panel instance of the specified type. If there is more than one, the focused panel takes precedence.
 */
AyyiPanel*
windows__find_panel (AyyiWindow* window, PanelType dt)
{
	g_return_val_if_fail(window, NULL);

	AyyiPanel* primary = NULL;

	GList* p = window->dock->panels;
	for(;p;p=p->next){
		if(G_OBJECT_TYPE(p->data) == dt){
			AyyiPanel* panel = p->data;
			if(panel == app->active_panel){
				primary = panel; // active panel takes precedence
				break;
			}else{
				if(!primary) primary = panel;
			}
		}
	}

	return primary;
}


AyyiPanel*
windows__get_first (PanelType type)
{
	// return the first open windows of the given type

	panel_foreach {
		if(G_OBJECT_TYPE(panel) == type) return panel;
	} end_panel_foreach;

	dbg (2, "window not found. type=%i", type);
	return NULL;
}


/*
 *  Return a newly allocated list of open windows of the given type.
 */
GList*
windows__get_by_type (PanelType dpt)
{
	GList* ret = NULL;

	panel_foreach {
		if(G_OBJECT_TYPE(panel) == dpt) ret = g_list_append(ret, panel);
	} end_panel_foreach;

	return ret;
}


AyyiWindow*
windows__open_new_window ()
{
	PF;

	char win_id[32];
	static int i = 0;
	sprintf(win_id, "Shell %i", i++); // this is an empty shell. Name is set later when first child is added.

	AyyiWindowClass* W = g_type_class_peek(AYYI_TYPE_WINDOW);

	GdlDock* dock = NULL;
	if(!W->master_dock){
		GtkWidget* _dock = gdl_dock_new();
		W->master_dock = dock = GDL_DOCK(_dock);
		g_signal_connect(GDL_DOCK_MASTER(GDL_DOCK_OBJECT(W->master_dock)->master), "layout-changed", G_CALLBACK(windows__on_dock_change), W->master_dock);
	} else {
		dock = GDL_DOCK(gdl_dock_new_from(W->master_dock, FALSE));
	}

	AyyiWindow* window = ayyi_window_new(GTK_WINDOW_TOPLEVEL);
	window->dock = dock;
	dbg(2, "adding shell to shell_list: dock=%s", window->dock ? GDL_DOCK_OBJECT(window->dock)->name : "<unnamed>");

	char dock_name[32]; snprintf(dock_name, 31, "%.24s Shell", win_id);
	GDL_DOCK_OBJECT(window->dock)->long_name = g_strdup(dock_name);

	// these can possibly be removed
	char place_holder_name[32];
	snprintf(place_holder_name, 31, "%.26s ph1", win_id);
	gdl_dock_placeholder_new (place_holder_name, GDL_DOCK_OBJECT(window->dock), GDL_DOCK_TOP, FALSE);
	snprintf(place_holder_name, 31, "%.26s ph2", win_id);
	gdl_dock_placeholder_new (place_holder_name, GDL_DOCK_OBJECT(window->dock), GDL_DOCK_BOTTOM, FALSE);
	snprintf(place_holder_name, 31, "%.26s ph3", win_id);
	gdl_dock_placeholder_new (place_holder_name, GDL_DOCK_OBJECT(window->dock), GDL_DOCK_LEFT, FALSE);
	snprintf(place_holder_name, 31, "%.26s ph4", win_id);
	gdl_dock_placeholder_new (place_holder_name, GDL_DOCK_OBJECT(window->dock), GDL_DOCK_RIGHT, FALSE);

#if 0
	void shell_on_allocate (GtkWidget* widget, GdkRectangle* allocation, gpointer user_data)
	{
		PF2;
	}
	g_signal_connect(G_OBJECT(shell->widget), "size-allocate", G_CALLBACK(shell_on_allocate), shell);
#endif

	gtk_box_pack_start(GTK_BOX(window->vbox), (GtkWidget*)window->dock, EXPAND_TRUE, FILL_TRUE, 0);

	if(W->instances->array->len == 1) gtk_window_set_title(GTK_WINDOW(window), "Ayyi");

	return window;
}


int
windows__get_n_instances (PanelType type)
{
	// this is only useful for debugging.
	// -normally you would use class->instances intead.

	int n = 0;
	panel_foreach {
		PanelType dtype = G_OBJECT_TYPE(panel);
		if(dtype == type) n++;
	} end_panel_foreach;

	return n;
}


/*
 *  Return the panel class type for the given string.
 *  The string can contain multiple words, but only the first word is matched.
 */
PanelType
windows__panel_type_from_str (const char* str)
{
	g_return_val_if_fail(str, 0);

	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, &value)) {
		PanelType panel_type = *((gint64*)key);
		AyyiPanelClass* k = g_type_class_peek(panel_type);
		dbg(3, "  type=%s %s", g_type_name(panel_type), k->name);
		if (!k) { pwarn("no class for type=%s", g_type_name(panel_type)); continue; }

		char* found = g_strstr_len(str, 32, k->name);
		if (found == str) {
			return G_TYPE_FROM_CLASS(k);
		}
	}
	AYYI_DEBUG pwarn("not found! %s", str);

	return 0;
}


/*
 *  @param type - if this is non-zero, we also check the window type matches.
 *
 *  Note that this will fail during docking operations because panels are temporarily removed
 */
bool
windows__verify_pointer (AyyiPanel* panel, PanelType type)
{
	g_return_val_if_fail(panel, false);

	AyyiPanelClass* panel_class = g_type_class_peek(type);

	bool found = false;
	AyyiPanel* target = panel;
	panel_foreach {
		if (panel == target) { found = true; break; }
	} end_panel_foreach;

	if (!found) {
		// do _not_ try and introspect the panel pointer
		perr("window not found");
		return false;
	}

	if (!type) return true;

	if (G_OBJECT_TYPE(panel) == type) return true;

	perr ("wrong type. type=%s expected=%s", AYYI_PANEL_GET_CLASS(panel)->name, panel_class->name);
	return false;
}


/*
 *  @param str should be of form: "$window_class_name-$integer"
 */
AyyiPanel*
windows__get_from_instance_string (const char* str)
{
	char* separator = strstr(str, " ");
	if(separator){
		char* instance_str = separator + 1;
		int instance_num = atoi(instance_str);
		if(instance_num){
			char window_type[32];
			int len = separator - str;
			strncpy(window_type, str, len);
			window_type[len] = '\0';
			dbg(3, "type=%s", window_type);

			panel_foreach {
				int i = ayyi_panel_get_instance_num(panel);
				dbg(3, "  (%s) %i==%i %s", str, i, instance_num, AYYI_PANEL_GET_CLASS(panel)->name);
				if(i == instance_num && !strcmp(AYYI_PANEL_GET_CLASS(panel)->name, window_type)) return panel;
			} end_panel_foreach;
		}
		else dbg(0, "failed to get instance num.");
	}
	else dbg(0, "failed to find separator. str='%s'", str);
	dbg(0, "failed.");
	return NULL;
}


AyyiPanel*
windows__get_from_instance (GType type, int instance_num)
{
	panel_foreach {
		if(G_OBJECT_TYPE(panel) != type) continue;
		int i = ayyi_panel_get_instance_num(panel);
		if(i == instance_num) return panel;
	} end_panel_foreach;

	return NULL;
}


GHashTable*
windows__new_count_table ()
{
	GHashTable* instances = g_hash_table_new(g_int64_hash, g_int_equal);

	// Add an instances entry for each window type
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		g_hash_table_insert(instances, key, 0); // key is PanelType
	}
	return instances;
}


GHashTable*
windows__new_count_table_from_shell (AyyiWindow* window)
{
	GHashTable* instances = windows__new_count_table();

	GList* existing_panels = window__get_dock_items(window);
	GList* p = existing_panels;
	for(;p;p=p->next){
		GdlDockObject* object = p->data;
		windows__count_table_add(instances, G_OBJECT_TYPE(object));
		//windows__print_dock_object(object, "  ");
	}
	g_list_free(existing_panels);
	return instances;
}


int
windows__count_table_add (GHashTable* table, PanelType type)
{
	int n_instances = GPOINTER_TO_INT(g_hash_table_lookup(table, &type));
	n_instances++;
	g_hash_table_insert(table, &type, GINT_TO_POINTER(n_instances));
	return n_instances;
}


void
windows__count_table_remove_instance (GHashTable* instances, PanelType type)
{
	PF;
	int n_instances = GPOINTER_TO_INT(g_hash_table_lookup(instances, &type));
	if(!n_instances) return;
	n_instances--;
	g_hash_table_insert(instances, &type, &n_instances);
}


static void
windows__count_table_print (GHashTable* table)
{
#ifdef DEBUG
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, table);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		int instances = GPOINTER_TO_INT(value);
		PanelType panel_type = *((int*)key);
		dbg(0, "  %i=%i %s", panel_type, instances, g_type_name(panel_type));
	}
#endif
}


void
windows__ensure (PanelType dtype)
{
	// make sure there is at least one window open of the given type.

	if(!windows__get_n_instances(dtype))
		window__new_floating(g_type_class_peek(dtype));
}


/*
AyyiPanelClass*
window__lookup_class_by_static_type (StaticPanelType w)
{
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		WindowClass* window_class = value;
		PanelType panel_type = *((int*)key);
		if(window_class){
			//dbg(0, "  key=%i", *((int*)key));
			dbg(0, "  val=%p", value);
		}
	}
}
*/

AyyiPanelClass*
panel_class_lookup_by_accel (int accel)
{
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, app->panel_classes);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		PanelType panel_type = *((gint64*)key);
		AyyiPanelClass* class = g_type_class_peek(panel_type);
		if(class->accel == accel) return class;
	}
	pwarn("not found!");
	return NULL;
}


/*
 *  Gets called following a "layout-changed" signal. This doesnt tell us what has changed.
 *  The potential advantage of this signal is that it occurs _after_ an operation has _completed_.
 *  Change can be new dock item added, or re-arrange
 */
static void
windows__on_dock_change (GdlDockMaster* master, gpointer data)
{
	windows__print_dock();
}


void
windows__print ()
{
	PF;
	printf("     type                  panel shellwdgt\n");
	int w = 0;
	char type[AYYI_SHORT_NAME_MAX];

	panel_foreach {
		ayyi_panel_print_type(panel, type);
		printf("  %i: %02i %11s %p %p\n", w, (int)G_OBJECT_TYPE(panel), type, panel, panel->window);
		w++;
	} end_panel_foreach;
}


void
windows__print_dock ()
{
	AyyiWindowClass* W = g_type_class_peek(AYYI_TYPE_WINDOW);

	GList* i = gdl_dock_get_named_items(W->master_dock);
	if(i){
		UNDERLINE;
		PF;
		printf("\n");
		printf("           type container           name       longname b    orientation     ph    parent\n");
		char shell_name[64] = "";
		window_foreach {
			GdlDock* dock = window->dock;
			if(!GDL_IS_DOCK_OBJECT(dock)){ printf("    !! window list corrupted. window=%p\n", window); continue; }

			sprintf(shell_name, "%s", GDL_DOCK_OBJECT(dock)->name);
			GList* items = window__get_dock_items(window);
			if(items){
				GList* i = items;
				printf(" window='%s' n_items=%i\n", shell_name, g_list_length(i));
				for(;i;i=i->next){
					if(GDL_IS_DOCK_OBJECT(i->data)){
						GdlDockObject* object = GDL_DOCK_OBJECT(i->data);
						windows__print_dock_object(object, "  ");
					}
					else printf("    not dock object! shell__get_dock_items() returned bad item\n");
				}
				g_list_free(items);
			}
		} end_window_foreach;

		UNDERLINE;
		g_list_free(i);
	}
	else dbg(0, "empty.");
}


void
windows__print_dock_item (GdlDockItem* item, const char* text)
{
	// note: see also gdl_dock_item_print()

	g_return_if_fail(item);
	GdlDockObject* object = &item->object;
	g_return_if_fail(object);
	dbg(0, "%s: %s %s", text, object->name, object->long_name);
}


void
windows__print_dock_object (GdlDockObject* object, const char* text)
{
	g_return_if_fail(object);

	char* depth = dock_object_get_depth();

	char type[32] = "<unknown type>";
	char behaviour[32] = "";
	char orientation_str[64] = "";
	char placeholder_name[64] = "";
	if(GDL_IS_DOCK_ITEM(object)){
		GdlDockItem* item  = (GdlDockItem*)object;

		strcpy(type, "DockItem");
		sprintf(behaviour, "%i", item->behavior);

		GtkOrientation orientation;
		g_object_get(item, "orientation", &orientation, NULL);
		strcpy(orientation_str, get_orientation_string(orientation));

		GList* ph = gdl_dock_item_get_placeholders(item);
		if(ph){
			if(ph->data){
				GdlDockObject* placeholder_object = GDL_DOCK_OBJECT(ph->data);
				if(placeholder_object) sprintf(placeholder_name, "%s", placeholder_object->name);
				else sprintf(placeholder_name, "<none>");
			}
			g_list_free(ph);
		}
	}

	GtkContainer* container = &object->container;

	printf("%s %s %8s %p %14s %14s %s %14s %s %p\n", text, depth, type, container, object->name ? object->name : "<Unnamed>", object->long_name, behaviour, orientation_str, placeholder_name, gdl_dock_object_get_parent_object(object));
}


#ifdef DEBUG
void
windows__print_items ()
{
	if(!windows){ dbg(0, "window list empty"); return; }

	dbg(0, "");

	window_foreach {
		GList* i = window->dock->panels;
		printf("  shell %s%s%s:\n", bold, GDL_DOCK_OBJECT(window->dock)->long_name, white);
		if(!i) printf("    <no items>\n");
		for(;i;i=i->next){
			GdlDockItem* di = i->data;

#if 0
			GValue name = {0,};
			g_value_init(&name, G_TYPE_STRING);
			g_object_get_property(G_OBJECT(di), "long-name", &name);
#endif

			printf("    %s     %s\n", ((GdlDockObject*)di)->long_name, get_orientation_string(di->orientation));
		}
	} end_window_foreach;
}
#endif


void
shell_never ()
{
	windows__count_table_print(NULL);
}


static void
on_style_change (GtkWidget* widget)
{
	bool
	now_set_colours (gpointer _widget)
	{
		GtkWidget* widget = GTK_WIDGET(_widget);
		if(widget->style){
			GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));
			am_palette_set_style(song->palette, style);
			g_object_unref(style);
			app->styles_are_set = true;
		}
		else pwarn("widget still not realised!");

		return false;
	}

	if(widget->style){
		now_set_colours(widget);
	}else{
		// this path never used.
		g_idle_add((GSourceFunc)now_set_colours, widget);
	}
}

