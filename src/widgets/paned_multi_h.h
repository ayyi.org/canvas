/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __HPANED_MULTI_H__
#define __HPANED_MULTI_H__

#include <paned_multi.h>

G_BEGIN_DECLS

#define TYPE_HPANED_MULTI            (hpaned_multi_get_type ())
#define HPANED_MULTI(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_HPANED_MULTI, HPanedMulti))
#define HPANED_MULTI_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  TYPE_HPANED_MULTI, HPanedMultiClass))
#define IS_HPANED_MULTI(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_HPANED_MULTI))
#define IS_HPANED_MULTI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  TYPE_HPANED_MULTI))
#define HPANED_MULTI_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_HPANED_MULTI, HPanedMultiClass))


typedef struct _HPanedMulti      HPanedMulti;
typedef struct _HPanedMultiClass HPanedMultiClass;

struct _HPanedMulti
{
  PanedMulti paned;
};

struct _HPanedMultiClass
{
  PanedMultiClass parent_class;
};

GType      hpaned_multi_get_type (void) G_GNUC_CONST;
GtkWidget *hpaned_multi_new      (void);

G_END_DECLS

#endif /* __HPANED_MULTI_H__ */
