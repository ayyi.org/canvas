/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2007 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __PANED_MULTI_H__
#define __PANED_MULTI_H__

#include <gtk/gtkcontainer.h>

G_BEGIN_DECLS

#define TYPE_PANED_MULTI                (paned_multi_get_type ())
#define PANED_MULTI(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PANED_MULTI, PanedMulti))
#define PANED_MULTI_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  TYPE_PANED_MULTI, PanedMultiClass))
#define IS_PANED_MULTI(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PANED_MULTI))
#define IS_PANED_MULTI_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  TYPE_PANED_MULTI))
#define PANED_MULTI_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_PANED_MULTI, PanedMultiClass))

//#define paned_multi_get_child(A)        ((paned_child*)g_ptr_array_index(paned->children, A))
#define paned_multi_get_child_widget(A) ((paned_child*)g_ptr_array_index(paned->children, A))->widget
#define paned_multi_get_child_size(A)   ((paned_child*)g_ptr_array_index(paned->children, A))->size
#define paned_multi_get_handle(A)       ((paned_child*)g_ptr_array_index(paned->children, A))->handle


typedef struct _PanedMulti        PanedMulti;
typedef struct _PanedMultiClass   PanedMultiClass;
typedef struct _PanedMultiPrivate PanedMultiPrivate;

typedef struct _paned_child
{
  GtkWidget   *widget;
  gint         size;
  GdkWindow   *handle;
  GdkRectangle handle_pos;

} paned_child;


struct _PanedMulti
{
  GtkContainer container;

  GPtrArray* children;  
  
  //GdkWindow *handle;
  GdkGC *xor_gc;
  GdkCursorType cursor_type;
  
  /*< private >*/
  //GdkRectangle handle_pos;

  //gint child1_size;
  gint last_allocation;
  gint min_position;
  gint max_position;

  //guint position_set : 1;
  guint in_drag : 1;
  guint children_shrink : 1;
  //guint children_resize : 1;
  //guint child2_shrink : 1;
  //guint child2_resize : 1;
  guint orientation : 1;
  guint in_recursion : 1;
  guint handle_prelit : 1;

  //GtkWidget *last_child1_focus;
  //GtkWidget *last_child2_focus;
  PanedMultiPrivate *priv;

  gint drag_pos;
  gint original_position;
};
 
struct _PanedMultiClass
{
  GtkContainerClass parent_class;
  
  gboolean (* cycle_child_focus)   (PanedMulti*, gboolean reverse); 
  gboolean (* toggle_handle_focus) (PanedMulti*);
  gboolean (* move_handle)         (PanedMulti*, GtkScrollType);
  gboolean (* cycle_handle_focus)  (PanedMulti*, gboolean reverse);
  gboolean (* accept_position)     (PanedMulti*);
  gboolean (* cancel_position)     (PanedMulti*);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GType   paned_multi_get_type        (void) G_GNUC_CONST;
void    paned_multi_add             (PanedMulti*, GtkWidget *child);
void    paned_multi_pack            (PanedMulti*, GtkWidget *child, gboolean shrink);
gint    paned_multi_get_position    (PanedMulti*);
void    paned_multi_set_position    (PanedMulti*, gint position);

GtkWidget *paned_multi_get_child    (PanedMulti*, int n);
//GtkWidget *paned_multi_get_child2   (PanedMulti*);

/* Internal function */
#if !defined (GTK_DISABLE_DEPRECATED) || defined (GTK_COMPILATION)
void    paned_multi_compute_position (PanedMulti*, gint allocation, gint child1_req, gint child2_req);
#endif /* !GTK_DISABLE_DEPRECATED || GTK_COMPILATION */
#ifndef GTK_DISABLE_DEPRECATED
#define	paned_multi_gutter_size(p,s)     (void) 0
#define	paned_multi_set_gutter_size(p,s) (void) 0
#endif /* GTK_DISABLE_DEPRECATED */

G_END_DECLS

#endif /* __PANED_MULTI_H__ */
