/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "math.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <glib-object.h>
#include <gdk/gdk.h>
#include "debug/debug.h"
#include "ayyi/ayyi_mixer.h"
#include "ayyi/ayyi_utils.h"
#include "gui_types.h"
#include "fader.h"

extern GtkWidget* pressed_widget;

static gpointer fader_parent_class = NULL;

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Fader, g_object_unref)

static GObject* fader_constructor       (GType, guint, GObjectConstructParam*);
static GType    fader_get_type_once     ();
static void     fader_size_allocate     (GtkWidget*, GdkRectangle*);
static gboolean fader_real_expose_event (GtkWidget*, GdkEventExpose*);


Fader*
fader_construct (GType object_type)
{
	return g_object_new (object_type, NULL);
}


GtkWidget*
fader_new (GtkAdjustment* adjustment)
{
	GtkWidget* widget = (GtkWidget*)fader_construct (TYPE_FADER);

	if(adjustment)
		gtk_range_set_adjustment(GTK_RANGE(widget), adjustment);

	return widget;
}


static GObject*
fader_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties)
{
	GObjectClass* parent_class = G_OBJECT_CLASS (fader_parent_class);
	return parent_class->constructor (type, n_construct_properties, construct_properties);
}


static void
fader_class_init (FaderClass* klass, gpointer klass_data)
{
	fader_parent_class = g_type_class_peek_parent (klass);

	((GtkWidgetClass *) klass)->expose_event = (gboolean (*) (GtkWidget*, GdkEventExpose*)) fader_real_expose_event;
	((GtkWidgetClass *) klass)->size_allocate = (void (*) (GtkWidget*, GdkRectangle*)) fader_size_allocate;

	G_OBJECT_CLASS (klass)->constructor = fader_constructor;
}


static void
fader_instance_init (Fader* self, gpointer klass)
{
	gtk_orientable_set_orientation (GTK_ORIENTABLE (self), GTK_ORIENTATION_VERTICAL);
}


static GType
fader_get_type_once (void)
{
	static const GTypeInfo g_define_type_info = { sizeof (FaderClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) fader_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Fader), 0, (GInstanceInitFunc) fader_instance_init, NULL };
	GType fader_type_id;
	fader_type_id = g_type_register_static (GTK_TYPE_SCALE, "Fader", &g_define_type_info, 0);
	return fader_type_id;
}


GType
fader_get_type (void)
{
	static volatile gsize fader_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&fader_type_id__volatile)) {
		GType fader_type_id;
		fader_type_id = fader_get_type_once ();
		g_once_init_leave (&fader_type_id__volatile, fader_type_id);
	}
	return fader_type_id__volatile;
}


static void
fader_size_allocate (GtkWidget* widget, GdkRectangle* allocation)
{
	allocation->width = 14;
	widget->allocation = *allocation;
	GTK_WIDGET_CLASS(fader_parent_class)->size_allocate(widget, allocation);
}


static gboolean
fader_real_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	GtkRange* range = GTK_RANGE(widget);
	GtkAdjustment* a = range->adjustment;

	float top = (a->upper - a->value) * (widget->allocation.height - 4.) / (a->upper - a->lower);
	float height = (widget->allocation.height - 3.) - top;

	cairo_t* cr = gdk_cairo_create (widget->window);
	gdk_cairo_set_source_color (cr, &widget->style->bg[widget->state]);

	// set clip rect
	cairo_rectangle (cr, event->area.x, event->area.y, event->area.width, event->area.height);
	cairo_clip (cr);

	int x = 2 + widget->allocation.x;
	int y = 1 + widget->allocation.y; // align with meter
	int width = 10;

	cairo_rectangle (cr, x, y, width, widget->allocation.height - 2);
	cairo_fill (cr);

	gdk_cairo_set_source_color (cr, &widget->style->bg[GTK_STATE_SELECTED]);
	cairo_rectangle (cr, x + 1, y + top, width - 2, height);
	cairo_fill (cr);

	cairo_destroy (cr);

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		gtk_paint_focus (widget->style, widget->window, GTK_WIDGET_STATE (widget),
			&event->area,
			widget,
			"button",
			widget->allocation.x,
			widget->allocation.y - 1,
			width + 4,
			widget->allocation.height + 2
		);
	}

	return TRUE;
}


static bool
fader_on_change_value (GtkRange* range, GtkScrollType scroll, gdouble value, gpointer _channel)
{
	AMChannel* ch = (AMChannel*)_channel;

	switch(scroll){
		case GTK_SCROLL_STEP_BACKWARD:
			dbg(0, "GTK_SCROLL_STEP_BACKWARD");
			ayyi_channel__set_float(ch->ident.idx, AYYI_LEVEL, value); // testing...
			break;
		default:
			dbg(2, "GtkScrollType=%i", scroll);
	}
	return NOT_HANDLED;
}


static void
fader_on_value_changed (GtkWidget* widget, gpointer _channel)
{
	// Called when a fader value is changed by the user.
	// -faders can have NULL channel.

	double val = am_db2gain(gtk_range_get_value(GTK_RANGE(widget)));

	AMChannel* ch = (AMChannel*)_channel;
	if(ch){
		dbg(2, "ch=%i %s%s%s gain=%.2f", ch->ident.idx, bold, am_channel__get_name(ch), white, val);

		// only send a msg if mouse button is down
		if(widget == pressed_widget){
			ayyi_channel__set_float(ch->ident.idx, AYYI_LEVEL, val);
		}
	}
}


void
fader_set_channel (GtkWidget* fader, AMChannel* channel, AMChannel* prev)
{
	g_signal_handlers_disconnect_matched (fader, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, fader_on_value_changed, NULL);
	g_signal_handlers_disconnect_matched (fader, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, fader_on_change_value, NULL);

	void fader_on_level(Observable* level, AMVal value, gpointer fader)
	{
		gtk_range_set_value(GTK_RANGE(fader), am_gain2db(value.f));
	}

	if(prev)
		observable_unsubscribe(prev->level, NULL, fader);

	if(channel){
		g_signal_connect((gpointer)fader, "value-changed", G_CALLBACK(fader_on_value_changed), channel);
		g_signal_connect((gpointer)fader, "change-value", G_CALLBACK(fader_on_change_value), channel);

		observable_subscribe(channel->level, fader_on_level, fader);
	}
}


static int
fader__block_fader_handler (GtkWidget* fader)
{
	return g_signal_handlers_block_matched(fader, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, fader_on_value_changed, NULL);
}


static int
fader__unblock_fader_handler (GtkWidget* fader)
{
	return g_signal_handlers_unblock_matched(fader, G_SIGNAL_MATCH_FUNC, 0, 0, NULL, fader_on_value_changed, NULL);
}


void
fader__update (GtkWidget* fader, AMChannel* channel)
{
	if(channel && channel->type == AM_CHAN_INPUT){
		AyyiChannel* ch = ayyi_mixer__channel_at_quiet(channel->ident.idx);
		if(ch){
			int n = fader__block_fader_handler(fader);
			if(!n) pwarn("fader handler not found");
			fader__set_value(fader, ch->level);
			fader__unblock_fader_handler(fader);
		}
	}
}


void
fader__set_value (GtkWidget* fader, double val)
{
	g_return_if_fail(fader);

	val = am_gain2db(val);

	if(fader != pressed_widget) {
		GtkAdjustment* adj = GTK_RANGE(fader)->adjustment;
		if(fabs(val - adj->value) > 0.01){
			gtk_adjustment_set_value(adj, val);
		}
	}
}

