/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi Project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __gl_canvas_priv__
#define __arrange_private__

#include "global.h"
#include "waveform/actor.h"
#include "model/time.h"
#include "window.statusbar.h"
#include "support.h"
#include "part_manager.h"
#include "song.h"
#include "window.h"
#include "panels/arrange.h"
#include "arrange/track_control_midi.h"
#include "arrange/utils.h"
#include "arrange/part.h"
#include "arrange/shader.h"
#include "arrange/gl_canvas.h"
#include "arrange/gl_target.h"
#include "arrange/gl_canvas_op.h"

extern void gl_part_move                     (AGlActor*);
extern void gl_part_change_track             (AGlActor*);

static void gl_canvas_op__press              (CanvasOp*, GdkEvent*);
static void gl_canvas_op__motion             (CanvasOp*);
static void gl_canvas_op__finish             (CanvasOp*);
//static void gl_canvas_op__note_right_press   (CanvasOp*, GdkEvent*);
static Op*  gl_canvas_op__none               ();
static Op*  gl_canvas_op__move               ();
static Op*  gl_canvas_op__copy               ();
static Op*  gl_canvas_op__resize_left        ();
static Op*  gl_canvas_op__resize_right       ();
static Op*  gl_canvas_op__repeat             ();
static Op*  gl_canvas_op__box                ();
static Op*  gl_canvas_op__scissors           ();
static Op*  gl_canvas_op__scroll             ();
static Op*  gl_canvas_op__draw               ();
static Op*  gl_canvas_op__auto_drag          ();


static CanvasOp* instance = NULL;

static Op* ops    [N_OP_TYPES]      = {0,};
Optype ftr2op     [N_FEATURE_TYPES] = {0,};  // map feature --> optype
Optype ftr2op_ctl [N_FEATURE_TYPES];         // map feature --> optype when CTL key pressed

static void update_target (Arrange*, ArrTrk*, AMPart*);
static void remove_target (Arrange*);

static void canvas_op__handle_scrolling (CanvasOp*);

static struct {
    int to_move;
    int did_move;
    int timer;
} scroller = {0,};

static void scroll_motion (CanvasOp*);
static void scroll_finish (CanvasOp*);

#include "gl_ghost.c"


Optype
get_optype (Arrange* arrange, FeatureType type, guint button_state)
{
	dbg(1, "featureType=%s tool=%i", print_feature_type(type), arrange->toolbox.current);

	switch (arrange->toolbox.current) {
		case TOOL_SCROLL:
			dbg(0, "OP_SCROLL!");
			return OP_SCROLL;
		case TOOL_SCISSORS: 
			return OP_SPLIT;
		case TOOL_PENCIL: 
			if(type == FEATURE_NOTE_AREA || type == FEATURE_NONE){ // TODO why not getting note area?
				return OP_DRAW;
			}
			break;
		default:
			break;
	}

	return (button_state & GDK_CONTROL_MASK)
		? ftr2op_ctl[type]
		: ftr2op[type]
			? ftr2op[type]
			: OP_MOVE;
}


CanvasOp*
gl_canvas_op__new (Arrange* arrange, Optype type)
{
	void gl_canvas_op_init()
	{
		ops[OP_NONE]         = gl_canvas_op__none();
		ops[OP_MOVE]         = gl_canvas_op__move();
		ops[OP_COPY]         = gl_canvas_op__copy();
		ops[OP_RESIZE_LEFT]  = gl_canvas_op__resize_left();
		ops[OP_RESIZE_RIGHT] = gl_canvas_op__resize_right();
		ops[OP_REPEAT]       = gl_canvas_op__repeat();
		ops[OP_BOX]          = gl_canvas_op__box();
		ops[OP_SPLIT]        = gl_canvas_op__scissors();
		ops[OP_SCROLL]       = gl_canvas_op__scroll();
		ops[OP_DRAW]         = gl_canvas_op__draw();
		ops[OP_AUTO_DRAG]    = gl_canvas_op__auto_drag();

		ftr2op[FEATURE_NONE]       = OP_MOVE;
		ftr2op[FEATURE_PART_LEFT]  = OP_RESIZE_LEFT;
		ftr2op[FEATURE_PART_RIGHT] = OP_RESIZE_RIGHT;

		ftr2op_ctl[FEATURE_NONE]       = OP_COPY;
		ftr2op_ctl[FEATURE_PART_RIGHT] = OP_REPEAT;
	}

	CanvasOp* op = instance
		? instance
		: (gl_canvas_op_init(), instance = (CanvasOp*)g_new0(GlCanvasOp, 1)); // singleton

	op->type = type;
	dbg(1, "type=%i %s", type, canvas_op__print_type(op));

	op->arrange = arrange;

	switch(type){
		case OP_NONE:
		case OP_MOVE:
		case OP_COPY:
		case OP_RESIZE_LEFT:
		case OP_RESIZE_RIGHT:
		case OP_REPEAT:
		case OP_SPLIT:
		case OP_BOX:
		case OP_SCROLL:
		case OP_DRAW:
		case OP_AUTO_DRAG:
			op->op = ops[type];
			break;
		case OP_EDIT:
		case OP_VELOCITY:
			break;
		default:
			*op->op = (Op){gl_canvas_op__press, gl_canvas_op__motion, gl_canvas_op__finish};
			break;
	}
	return op;
}


static void
gl_canvas_op__press (CanvasOp* op, GdkEvent* event)
{
	op->start = op->mouse;
	op->diff = (Ptd){op->mouse.x - op->p1.x, op->diff.y = op->mouse.y - op->p1.y};
	op->old_bounded = op->mouse;

	op->event.state = event->button.state;
}


#if 0
static void
gl_canvas_op__note_right_press (CanvasOp* op, GdkEvent* event)
{
	PF;
	canvas_op__press(op, event);

	//if no notes are selected, we need to select one
	if(!((part_midi*)op->part)->note_selection){
		const MidiNote* note = gl_part_pick_note(part, op->diff, NULL, NULL);
		if(note){
			//gnome_canvas_part_set_edit_selection(GNOME_CANVAS_PART(op->item), g_list_append(NULL, (void*)note));
		}
	}
}
#endif


static void
gl_canvas_op__motion (CanvasOp* op)
{
	g_return_if_fail(op->arrange);

	op->bnew.x = op->mouse.x;

	// vertical bounds checking
	ArrTrk* last = track_list__last_visible(op->arrange->priv->tracks);
	op->bnew.y = MAX(op->diff.y + PART_OUTLINE_WIDTH, op->mouse.y);
	op->bnew.y = MIN(op->bnew.y, (last->y + last->height) * vzoom(op->arrange) - (op->p2.y - op->p1.y) + op->diff.y + PART_OUTLINE_WIDTH);

	int max_movement = MAX(ABS(op->start.x - op->mouse.x), ABS(op->start.y - op->mouse.y));
	op->max_movement = MAX(max_movement, op->max_movement);
}


static Op*
gl_canvas_op__none ()
{
	void motion (CanvasOp* op)
	{
		// No operation, but we need to show mouse-over effects.
		// TODO why is this separate to canvas_op__motion?
		gl_canvas_op__motion(op);

		Arrange* arrange = op->arrange;

		op->diff = (Ptd){op->mouse.x - op->p1.x, op->mouse.y - op->p1.y};
		op->ready = 0;

		if (op->mouse.y < 0) return;

		TrackDispNum d = arr_px2trk(arrange, op->mouse.y);
		if (d < 0) return;
#ifndef DEBUG_TRACKCONTROL
		ArrTrk* trk = track_list__track_by_display_index(arrange->priv->tracks, d);
#endif

		// this fn picks the feature for a *particular part*
		if (op->part) {
			FeatureType feature = pick_feature_type(op);
			dbg(3, "feature=%s mouse=%.2f x=%.2f", print_feature_type(feature), op->mouse.x, op->p1.x);
			switch (feature) {
				case FEATURE_NONE:
					arr_cursor_reset(arrange, arrange->canvas->widget->window);
					break;
				case FEATURE_PART_LEFT:
					set_cursor(arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
					op->ready = READY_RESIZE_LEFT;
					break;
				case FEATURE_PART_RIGHT:
					set_cursor(arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
					op->ready = READY_RESIZE_RIGHT;
					break;
				case FEATURE_NOTE:
					arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
					break;
				case FEATURE_VEL_DIVIDER:
					set_cursor(op->arrange->canvas->widget->window, CURSOR_V_DOUBLE_ARROW);
					break;
				case FEATURE_VEL_AREA:
					arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
					int velocity = arr_track_get_velocity_from_y((ArrTrackMidi*)trk, op->bnew.y);
					shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 3, "%i", velocity);
					break;
				case FEATURE_VEL_BAR_TOP:
					set_cursor(op->arrange->canvas->widget->window, CURSOR_V_DOUBLE_ARROW);
					break;
				case FEATURE_NOTE_AREA:
					arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);
					int note = arr_track_get_note_num_from_y((ArrTrackMidi*)trk, op->diff.y);
					if(arr_track_is_selected(arrange, trk->track)){
						if(note){
							((ArrTrackMidi*)trk)->hover_note = note;
							agl_actor__invalidate(CGL->actors[ACTOR_TYPE_TC]);
						}else{
							if(((ArrTrackMidi*)trk)->hover_note){
								((ArrTrackMidi*)trk)->hover_note = 0;
								agl_actor__invalidate(CGL->actors[ACTOR_TYPE_TC]);
							}
						}
					}
					shell__statusbar_print(((AyyiPanel*)op->arrange)->window, 3, "%s%i", note_format(note), note_get_octave_num(note));
					break;
				case FEATURE_NOTE_LEFT:
					set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
					break;
				case FEATURE_NOTE_RIGHT:
					set_cursor(op->arrange->canvas->widget->window, CURSOR_H_DOUBLE_ARROW);
					break;
				default:
					break;
			}
		} else {
			if(arrange->canvas && op->arrange->canvas->widget) arr_cursor_reset(op->arrange, op->arrange->canvas->widget->window);

			if(AM_TRK_IS_MIDI(trk->track)){
				if(((ArrTrackMidi*)trk)->hover_note){
					((ArrTrackMidi*)trk)->hover_note = 0;
					agl_actor__invalidate(CGL->actors[ACTOR_TYPE_TC]);
				}
			}
		}
	}

	static Op op;
	op = (Op){gl_canvas_op__press, motion, gl_canvas_op__finish};
	return &op;
}


static Op*
gl_canvas_op__move ()
{
	void press (CanvasOp* op, GdkEvent* event)
	{
		gl_canvas_op__press(op, event);

		arr_statusbar_printf(op->arrange, 2, "%i parts selected", 1);
	}

	void motion (CanvasOp* op)
	{
		Arrange* arrange = op->arrange;
		GlCanvasOp* gop = (GlCanvasOp*)op;

		gl_canvas_op__motion(op);

		ArrTrk* at = canvas_op__get_track(op, op->bnew.y);

		if (at && !gop->ghost) {
			// Create a ghost part to show the original position.
			// Note that it is not shown until after movement has started.
			// Note that unlike the gnome canvas, op->part_local is not being used.
			agl_actor__add_child(CGL->actors[ACTOR_TYPE_MAIN], gop->ghost = ghost_actor((GtkWidget*)arrange));
			int x = op->start.x - op->diff.x;
			gop->ghost->region = (AGlfRegion){x, op->start.y - op->diff.y, x + arr_gl_pos2px_(arrange, &op->part->length), op->start.y - op->diff.y + at->height * vzoom(arrange)};
		}

		// warning, setting AMPart start! make sure it's re-set if op fails.
		double new_pos_px = op->bnew.x - op->diff.x;
		if (new_pos_px < 0.0) new_pos_px = 0.0;
		arr_px2spos(arrange, &op->part->start, new_pos_px);
		dbg(2, "new=%.2f diff=%.2f --> %.2f", op->bnew.x, op->diff.x, op->bnew.x - op->diff.x);

		AGlActor* pa = g_hash_table_lookup(CGL->parts, op->part);

		// warning! setting AMPart track! make sure its re-set if op fails.
		if (at) {
			g_return_if_fail(at->track->visible);
			if(at->track != op->part->track){
				dbg(1, "track changed: %s", at->track->name);
				arr_gl_canvas_redraw_part(arrange, op->part); // invalidate the draw cache for the _old_ track.
				op->part->track = at->track;
				gl_part_change_track(pa);
			}
		}

		op->old_bounded = op->bnew;

		update_target(arrange, at, op->part);

		// clear AMTrack cache
		g_signal_emit_by_name (am_parts, "item-changed", op->part, AM_CHANGE_POS_X, NULL);

		gl_part_move(pa);

		scroll_motion(op);

		arr_bbst2statusbar(arrange, op->p1.x, NULL);
	}

	void move_finish (CanvasOp* op)
	{
		g_return_if_fail(op->part);
		Arrange* arrange = op->arrange;
		GlCanvasOp* gop = (GlCanvasOp*)op;

		// vertical snapping
		ArrTrk* at = canvas_op__get_track(op, op->bnew.y);
		AMTrack* new_track = (at && at->track != ((GlCanvasOp*)op)->original_track) ? at->track : NULL;
		op->part->track = ((GlCanvasOp*)op)->original_track; //need to restore the original state so the model can detect if it has changed.
		dbg (1, "track=%s orig_track=%s new_track=%s", at ? at->track->name: NULL, ((GlCanvasOp*)op)->original_track->name, new_track ? new_track->name : NULL);

		// detect false moves
		//  -this appears to be ok. If we need more control maybe use a timer (events are timestamped!),
		//  -we do the check in the Release phase to avoid the 'sticky' intertia feeling.
		if (op->max_movement > 2.0) {
			op->p1.x = MAX(op->p1.x, 0);
			dbg (2, " dropping... updating part data...");

			DRect rect;
			arrange->canvas->get_part_rect(arrange, op->part, &rect);
			AyyiSongPos difference;
			arr_snap_px2spos(arrange, &difference, rect.x1);
			char b2[32]; ayyi_pos2bbss(&difference, b2);

			// the first part should be snapped, the rest maintain their relative position to the first.
			AyyiSongPos orig;
			arr_px2spos(arrange, &orig, op->start.x - op->diff.x);
			ayyi_pos_sub(&difference, &orig);

			char b1[32]; ayyi_pos2bbss(&orig, b1);
			char bb[32]; ayyi_pos2bbss(&difference, bb);
			dbg(1, "%s --> %s difference=%s", b1, b2, bb);

			GList* l = op->arrange->part_selection;
			for(;l;l=l->next){
				AMPart* part = l->data;
				AyyiSongPos p = (part == op->part) ? orig : part->start;
				ayyi_pos_add(&p, &difference);
				am_part_move(part, &p, new_track, NULL, NULL);
			}

			AyyiWindow* window = ((AyyiPanel*)op->arrange)->window;
			if(window->statusbar->drag_id.cid)
				gtk_statusbar_remove(GTK_STATUSBAR(window->statusbar->widget[STATUSBAR_MAIN]), window->statusbar->drag_id.cid, window->statusbar->drag_id.mid);
		}

		remove_target(arrange);

		if (gop->ghost) {
			agl_actor__remove_child(CGL->actors[ACTOR_TYPE_MAIN], gop->ghost);
			gop->ghost = NULL;
		}

		gl_canvas_op__finish(op);
	}

	static Op op;
	op = (Op){press, motion, move_finish};
	return &op;
}


static Op*
gl_canvas_op__copy ()
{
	bool copy_paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

		GList* l = c->local_parts;
		for(;l;l=l->next){
			LocalPart* lpart = l->data;
			arr_gl_canvas_paint_local_part(arrange, lpart);
		}
		return true;
	}

	void press (CanvasOp* op, GdkEvent* event)
	{
		PF;
		GlCanvas* c = op->arrange->canvas->gl;

		gl_canvas_op__press(op, event);

		c->actors[ACTOR_TYPE_FG]->paint = copy_paint;

		ArrTrk* atr = track_list__lookup_track(op->arrange->priv->tracks, op->part->track);
		op->part_local = part_local_new_from_part(op->arrange, op->p1.x, atr->y * vzoom(op->arrange), op->part, FALSE);
		g_return_if_fail(!c->local_parts);
		c->local_parts = g_list_append(c->local_parts, op->part_local);
	}

	void motion (CanvasOp* op)
	{
		GlCanvas* c = op->arrange->canvas->gl;

		gl_canvas_op__motion(op);

		// move the new part to follow the mouse
		op->bnew.x = op->mouse.x;
		op->bnew.y = op->mouse.y;
		if (op->bnew.y < op->diff.y) op->bnew.y = op->diff.y; // FIXME use same bounds checking as DRAGGING

		if(c->local_parts){
			LocalPart* part = c->local_parts->data;

			double new_pos_px = op->bnew.x - op->diff.x;
			if(new_pos_px < 0.0) new_pos_px = 0.0;
			arr_px2spos(op->arrange, &((AMPart*)part)->start, new_pos_px);

			ArrTrk* at = canvas_op__get_track(op, op->bnew.y);
			if(at){
				g_return_if_fail(at->track->visible);
				if(at->track != ((AMPart*)part)->track){
					dbg(1, "track changed: %s", at->track->name);
					arr_gl_canvas_redraw_track(op->arrange, track_list__lookup_track(op->arrange->priv->tracks, ((AMPart*)part)->track)); //invalidate cache on the old track;
					((AMPart*)part)->track = at->track;
				}

				update_target(op->arrange, at, (AMPart*)part);
			}
		}
		op->old_bounded.x = op->bnew.x;
		op->old_bounded.y = op->bnew.y;

		scroll_motion(op);

		arr_bbst2statusbar(op->arrange, op->mouse.x, NULL);
	}

	void finish (CanvasOp* op)
	{
		PF;
		GlCanvas* c = op->arrange->canvas->gl;

		if(op->max_movement > 2.0){
			arr_snap_px2spos(op->arrange, &op->part_local->part.start, op->mouse.x - op->diff.x);

			if(g_list_length(op->arrange->part_selection) > 1) pwarn ("FIXME - unique name may fail when copying multiple parts.");
			int track_offset = 0;
			if(c->local_parts){
				LocalPart* part = c->local_parts->data;
				TrackDispNum d_old  = track_list__get_display_num(op->arrange->priv->tracks, op->part->track);
				TrackDispNum d_new  = track_list__get_display_num(op->arrange->priv->tracks, ((AMPart*)part)->track);
				track_offset = d_new - d_old;
			}
			song_add_parts_from_selection((AyyiPanel*)op->arrange, &op->part_local->part.start, track_offset);
		}

		gl_canvas_op__finish(op);

		if(c->local_parts){
			g_list_clear(c->local_parts);
		}

		remove_target(op->arrange);

		scroll_finish(op);

		c->actors[ACTOR_TYPE_FG]->paint = agl_actor__null_painter;
	}

	static Op op;
	op = (Op){press, motion, finish};
	return &op;
}


/*
 *  The part start is modified. The region start is also modified by the inverse amount.
 *
 *  TODO Add support for trimming of MIDI parts
 */
static Op*
gl_canvas_op__resize_left ()
{
	void left_motion (CanvasOp* op)
	{
		GlCanvas* c = op->arrange->canvas->gl;

		canvas_op__motion(op);

		AyyiRegionBase* base = am_part_get_shared(op->part);
		if(base){
			uint32_t orig_part_start = base->position;
			uint32_t orig_part_start_px = arr_samples2px(op->arrange, orig_part_start);

			double new_pos_px = MAX(0.0, op->bnew.x - op->diff.x);
			uint32_t orig_inset_samples = ((AyyiAudioRegion*)base)->start;
			double orig_inset_px = arr_samples2px(op->arrange, orig_inset_samples);
			arr_px2spos(op->arrange, &op->part->start, MAX(orig_part_start_px - orig_inset_px, new_pos_px));
			dbg(2, "new=%.2f diff=%.2f --> %.2f", op->bnew.x, op->diff.x, op->bnew.x - op->diff.x);

			uint32_t orig_sample_start_samples = orig_part_start - orig_inset_samples;
			double orig_sample_start_px = arr_samples2px(op->arrange, orig_sample_start_samples);
			double new_inset_px = MAX(0, new_pos_px - orig_sample_start_px);
			double new_inset_samples = arr_px2samples(op->arrange, new_inset_px);
			int delta_samples = new_inset_samples - orig_inset_samples;
			op->part->region_start = new_inset_samples;
			int orig_length_samples = op->part->ayyi->length;
			int new_length_samples = orig_length_samples - delta_samples;
			ayyi_samples2pos(new_length_samples, &op->part->length);
			dbg(1, "samples: orig=%u new_inset=%.2f delta=%i len=%i", orig_inset_samples, new_inset_samples, delta_samples, new_length_samples);

			if(PART_IS_AUDIO(op->part)){
				PartActor* pa = g_hash_table_lookup(c->parts, op->part);
				if(pa){
					dbg(1, "len=%u", ayyi_pos2samples(&op->part->length));
					int max_len = pa->wf_actor->waveform->n_frames - op->part->region_start; // needed due to rounding errors from low-res GPos* part->length.
					wf_actor_set_region(pa->wf_actor, &(WfSampleRegion){op->part->region_start, MIN(ayyi_pos2samples(&op->part->length), max_len)});
				}
			}

			arr_gl_canvas_redraw_part(op->arrange, op->part);

			scroll_motion(op);

			arr_bbst2statusbar(op->arrange, op->mouse.x, NULL);
		}
	}

	void left_finish (CanvasOp* op)
	{
		g_return_if_fail(op->part);

		if(PART_IS_AUDIO(op->part)){
			AyyiAudioRegion* region = ayyi_song__audio_region_at(op->part->ident.idx);
			if(region){
				uint32_t orig_inset_samples = region->start;
				if(op->part->region_start != orig_inset_samples){
					if(!(op->event.state & GDK_CONTROL_MASK)){
						am_snap_(&op->part->start);
					}

					uint32_t new_part_start = ayyi_pos2samples(&op->part->start);
					uint32_t delta = new_part_start - ((AyyiRegionBase*)region)->position;
					op->part->region_start = orig_inset_samples - delta;

					am_part_trim_left(op->part, NULL, NULL);
				}
			}
		}

		scroll_finish(op);
		gl_canvas_op__finish(op);
	}

	static Op op;
	op = (Op){gl_canvas_op__press, left_motion, left_finish};
	return &op;
}


static Op*
gl_canvas_op__resize_right ()
{
	void right_motion (CanvasOp* op)
	{
		GlCanvas* c = op->arrange->canvas->gl;

		canvas_op__motion(op);

		double max_length_px = am_part__max_length_px_nz(op->part) * hzoom(op->arrange); // TODO inset
		double new_length_px = MIN(max_length_px, op->mouse.x - op->p1.x);
		arr_px2spos(op->arrange, &op->part->length, new_length_px);

		// FIXME this is better than nothing, but stops the autoscrolling from working.
		//       It moves the cursor away from the part end, and scrolls to end.
		canvas_op__check_song_length(op, op->mouse.x);

		PartActor* pa = g_hash_table_lookup(c->parts, op->part);
		if(pa && op->part->pool_item){
			dbg(1, "len=%u", ayyi_pos2samples(&op->part->length));
			wf_actor_set_region(pa->wf_actor, &(WfSampleRegion){op->part->region_start, ayyi_pos2samples(&op->part->length)});
		}

		arr_gl_canvas_redraw_part(op->arrange, op->part);

		scroll_motion(op);

		char lbl[AYYI_BBST_MAX];
		arr_px2bbs(op->arrange, new_length_px, lbl);
		arr_statusbar_printf(op->arrange, 1, "part length: %s", lbl);
	}

	void right_finish (CanvasOp* op)
	{
		g_return_if_fail(op->part);
		AMPart* part = op->part;
		PF;

#if 0 //snapping disabled - needs a lot more work
		arr_snap_px2pos(op->arrange, &op->part->length, op->mouse.x - op->p1.x);
		if(gpos_is_empty(&part->length)){
			am_snap_to_next(&part->length);
		}
#else
		arr_px2spos(op->arrange, &op->part->length, op->mouse.x - op->p1.x);
#endif

		char bbst[128]; ayyi_pos2bbst(&op->part->start, bbst);
		dbg (0, "length=%dpx (%u frames) start=%s", (int)arr_spos2px(op->arrange, &op->part->length), (unsigned)ayyi_pos2samples(&op->part->length), bbst);

		AyyiNFrames orig = part->ayyi->length;
		if(ayyi_pos2samples(&part->length) != orig){
			am_part_set_length(part);
		}else{
			log_print(0, "length unchanged.");
			ayyi_samples2pos(orig, &part->length);
		}

		scroll_finish(op);

		gl_canvas_op__finish(op);
	}

	static Op op;
	op = (Op){gl_canvas_op__press, right_motion, right_finish};
	return &op;
}


static Op*
gl_canvas_op__repeat ()
{
	bool repeat_paint (AGlActor* actor)
	{
		Arrange* arrange = (Arrange*)actor->root->gl.gdk.widget;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;
		CanvasOp* op = arrange->canvas->op;

		GPos selection_end;
		am_partmanager__get_pos(arrange->part_selection, NULL, &selection_end);

		// start on the next beat
		GPos repeat_start = selection_end;
		repeat_start.beat += 1;
		repeat_start.sub = 0;
		repeat_start.tick = 0;

		float x = arr_pos2px(arrange, &repeat_start) - c->actors[ACTOR_TYPE_MAIN]->scrollable.x1;
		float y = op->p1.y - c->actors[ACTOR_TYPE_MAIN]->scrollable.y1;
		float w = MAX(1, op->mouse.x - x);
		float h = op->p2.y - op->p1.y;
		agl_enable(0 /* !AGL_ENABLE_TEXTURE_2D */);
		glColor4f(1.0, 1.0, 1.0, 0.4);
		glBegin(GL_LINES);
		glVertex3f(x,   y,   0); glVertex3f(x+w, y,   0);
		glVertex3f(x+w, y,   0); glVertex3f(x+w, y+h, 0);
		glVertex3f(x+w, y+h, 0); glVertex3f(x,   y+h, 0);
		glVertex3f(x,   y+h, 0); glVertex3f(x,   y,   0);
		glEnd();

		return true;
	}

	void repeat_press (CanvasOp* op, GdkEvent* event)
	{
		op->arrange->canvas->gl->actors[ACTOR_TYPE_MAIN]->paint = repeat_paint;
		gl_canvas_op__press(op, event);
	}

	void repeat_motion (CanvasOp* op)
	{
		gl_canvas_op__motion(op);
	}

	void repeat_finish (CanvasOp* op)
	{
		PF;

		GPos selection_end;
		am_partmanager__get_pos(op->arrange->part_selection, NULL, &selection_end);

		GPos repeat_start = selection_end;
		repeat_start.beat += 1;
		repeat_start.sub = 0;
		repeat_start.tick = 0;

		GPos repeat_end; arr_px2pos(op->arrange, &repeat_end, op->mouse.x);

		if(pos_is_after(&repeat_end, &repeat_start)){
			song_part_selection_repeat(op->arrange->part_selection, &repeat_end); //create new parts
		}

		gl_canvas_op__finish(op);

		op->arrange->canvas->gl->actors[ACTOR_TYPE_MAIN]->paint = agl_actor__null_painter;
	}

	static Op op;
	op = (Op){repeat_press, repeat_motion, repeat_finish};
	return &op;
}


static Op*
gl_canvas_op__box ()
{
	static bool first; first = true;

	bool arr_gl_draw_selection_box (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		GlCanvasOp* g = (GlCanvasOp*)arrange->canvas->op;
		AGl* agl = agl_get_instance();
		Rectangle* box = &g->selection_box;

		SET_PLAIN_COLOUR (agl->shaders.plain, 0xff000080);
		agl_use_program((AGlShader*)agl->shaders.plain);

		agl_box(1, box->x, box->y, box->width, box->height);

		return true;
	}

	void box_motion (CanvasOp* op)
	{
		Arrange* arrange = op->arrange;

		canvas_op__motion(op);
		canvas_op__handle_scrolling(op);

		dbg(1, "x=%.1f-->%.1f", op->start.x, op->mouse.x);
		Rectangle* box = &((GlCanvasOp*)op)->selection_box;
		if (first) {
			*box = (Rectangle){
				.x = op->mouse.x,
				.y = op->mouse.y,
				.width = 1,
				.height = 1
			};
			CGL->actors[ACTOR_TYPE_FG]->paint = arr_gl_draw_selection_box;
			first = false;
		} else {
			((GlCanvasOp*)op)->selection_box.width = op->mouse.x - box->x;
			((GlCanvasOp*)op)->selection_box.height = op->mouse.y - box->y;
		}

		agl_actor__grab(CGL->actors[ACTOR_TYPE_MAIN]);
		agl_actor__invalidate(CGL->actors[ACTOR_TYPE_MAIN]);
	}

	void box_finish (CanvasOp* op)
	{
		PF;
		Rectangle* box = &((GlCanvasOp*)op)->selection_box;

		op->arrange->canvas->gl->actors[ACTOR_TYPE_FG]->paint = agl_actor__null_painter;

		gl_canvas_op__finish(op);

		if (op->arrange->toolbox.current == TOOL_MAG) {
			// zoom mode
			dbg(1, "x=%.1f-->%.1f", op->start.x, op->mouse.x);
			arr_zoom_to(op->arrange, op->start.x, op->start.y, op->mouse.x, op->mouse.y);
		} else {

			typedef struct {
				CanvasOp*  op;
				Rectangle* box;
			} C;
			C* c = g_new(C, 1);
			c->op = op;

			// invert if box width is negative
			Rectangle* __box = &((GlCanvasOp*)op)->selection_box;
			Rectangle box = *__box;
			if(box.width < 0){
				box.x += __box->width;
				box.width = -__box->width;
			}
			c->box = &box;

			bool part_is_inside_box (AMPart* part, gpointer _c)
			{
				C* c = _c;
				CanvasOp* op = c->op;
				Rectangle* box = c->box;
				GPos box_start, part_end;
				AyyiSongPos _part_end;
				Arrange* arrange = op->arrange;

				arr_px2pos(arrange, &box_start, box->x);
				AyyiSongPos box_end; arr_px2spos(arrange, &box_end, box->x + box->width);
				am_part__end_pos(part, &_part_end);
				songpos_ayyi2gui(&part_end, &_part_end);
				bool inside = pos_is_after(&part_end, &box_start) && ayyi_pos_is_after(&box_end, &part->start);
				if(inside){
					TrackDispNum _t0 = arr_px2trk(arrange, MAX(0, box->y));
					TrackDispNum _t1 = arr_px2trk(arrange, CLAMP(box->y + box->height, 0, CGL->actors[ACTOR_TYPE_TRACKS]->region.y2 -1));
					TrackDispNum d = track_list__get_display_num(arrange->priv->tracks, part->track);
					TrackDispNum t0, t1;
					(_t1 > _t0) ? (t0 = _t0, t1 = _t1) : (t0 = _t1, t1 = _t0);

					if(!(d >= t0 && d <= t1)) inside = false;
				}
				dbg(3, "  %s", inside ? "inside" : "outside");
				return inside;
			}

			if (box.width > 1.0) {
				GList* selection = NULL;
				FilterIterator* i = am_part_iterator_new ((Filter)part_is_inside_box, c);
				AMPart* part = NULL;
				while ((part = (AMPart*)filter_iterator_next(i))) {
					selection = g_list_prepend(selection, part);
				}
				filter_iterator_unref(i);
				dbg(1, "n_selected=%i", g_list_length(selection));

				if (list_cmp(selection, op->arrange->part_selection))
					arr_part_selection_replace(op->arrange, selection, true);
			}

			g_free(c);
		}

		*box = (Rectangle){0,};
		first = true;
	}

	static Op op;
	op = (Op){gl_canvas_op__press, box_motion, box_finish};
	return &op;
}


static Op*
gl_canvas_op__scissors ()
{
	void split_motion (CanvasOp* op)
	{
		canvas_op__motion(op);
	}

	void split_finish (CanvasOp* op)
	{
		PF;

		if(op->mouse.x > op->p1.x + 1 && op->mouse.x < op->p2.x - 1){ //if mouse is outside the part, we consider the operation aborted.
			pwarn("TODO result is snapped, but is not shown...");
			GPos pos;
			arr_snap_px2pos(op->arrange, &pos, op->mouse.x);

			AyyiSongPos p;
			songpos_gui2ayyi(&p, &pos);

			am_part_split(op->part, &p, NULL, NULL);
		}

		gl_canvas_op__finish(op);
	}

	static Op op;
	op.press = gl_canvas_op__press;
	op.motion = split_motion;
	op.finish = split_finish;
	return &op;
}


static Op*
gl_canvas_op__scroll ()
{
	static WfViewPort viewport = {0,};

	void scroll_press (CanvasOp* op, GdkEvent* event)
	{
		viewport = (WfViewPort){
			op->arrange->canvas->h_adj->value,
			op->arrange->canvas->v_adj->value
		};

		gl_canvas_op__press(op, event);
	}

	void scroll_motion (CanvasOp* op)
	{
		canvas_op__motion(op);

		canvas_op__handle_scrolling(op);

		int x = op->arrange->canvas->h_adj->value;
		int y = op->arrange->canvas->v_adj->value;
		Ptd vp_diff = {x - viewport.left, y - viewport.top};
		op->arrange->canvas->scroll_to(op->arrange,
			viewport.left + op->bnew.x - op->diff.x - vp_diff.x,
			viewport.top + op->bnew.y - op->diff.y - vp_diff.y
		);
	}

	void scroll_finish (CanvasOp* op)
	{
		PF;
		viewport = (WfViewPort){0,};

		gl_canvas_op__finish(op);
	}

	static Op op;
	op = (Op){scroll_press, scroll_motion, scroll_finish};
	return &op;
}


static Op*
gl_canvas_op__draw ()
{
	bool draw_paint (AGlActor* actor)
	{
		Arrange* arrange = actor->root->user_data;
		GlCanvas* c = (GlCanvas*)arrange->canvas->gl;

		GList* l = c->local_parts;
		for(;l;l=l->next){
			LocalPart* lpart = l->data;
			arr_gl_canvas_paint_local_part(arrange, lpart);
		}
		return true;
	}

	void draw_press (CanvasOp* op, GdkEvent* event)
	{
		GlCanvas* c = op->arrange->canvas->gl;

		gl_canvas_op__press(op, event);

		if(!op->part){
			c->actors[ACTOR_TYPE_FG]->paint = draw_paint;
			ArrTrk* atr = canvas_op__get_track(op, op->bnew.y);

			op->part_local = part_local_new_from_part(op->arrange, op->p1.x, atr->y * vzoom(op->arrange), op->part, FALSE);
			AyyiSongPos pos;
			arr_snap_px2spos(op->arrange, &pos, op->mouse.x);
			op->part_local = part_local_new_at_position(op->arrange, &pos, atr);
			g_return_if_fail(!c->local_parts);
			c->local_parts = g_list_append(c->local_parts, op->part_local);
		}
	}

	void draw_motion (CanvasOp* op)
	{
		GlCanvas* c = op->arrange->canvas->gl;

		canvas_op__motion(op);

		if(c->local_parts){
			LocalPart* lpart = c->local_parts->data;
			AMPart* part = (AMPart*)lpart;

			double start_px = arr_gl_pos2px_(op->arrange, &part->start);
			double max_length_px = am_part__max_length_px_nz(part) * hzoom(op->arrange);
			double new_length_px = MIN(max_length_px, op->mouse.x - start_px);
			arr_px2spos(op->arrange, &part->length, new_length_px);
		}

		if(op->part){
		}
	}

	void draw_finish (CanvasOp* op)
	{
		PF;

		GlCanvas* c = op->arrange->canvas->gl;

		if (op->part && PART_IS_MIDI(op->part)) {
			TrackDispNum d = arr_px2trk(op->arrange, op->mouse.y);
			ArrTrk* at = track_list__track_by_display_index(op->arrange->priv->tracks, d);

			int start = arr_px2samples(op->arrange, op->diff.x);
			int end = arr_px2samples(op->arrange, op->mouse.x - op->p1.x);
			//int end = arr_px2samples(op->arrange, op->bnew.x);
			if (end > start) {
				MidiNote* note = AYYI_NEW(MidiNote,
					.note = arr_track_get_note_num_from_y((ArrTrackMidi*)at, op->diff.y),
					.start = start,
					.length = end - start,
					.velocity = 0x40
				);
				am_song__midi_note_add(op->part, note, NULL, NULL);
			}
		} else if (c->local_parts) {
			ArrTrk* atr = canvas_op__get_track(op, op->mouse.y);
			LocalPart* lpart = c->local_parts->data;
			AMPart* part = (AMPart*)lpart;
			uint64_t len = 1000;
			am_song__add_part(am_track__get_media_type(atr->track), 0, NULL, atr->track->ident.idx, &part->start, len, 0, NULL, 0, NULL, NULL);
		}

		g_list_free0(c->local_parts); // TODO clean up properly

		c->actors[ACTOR_TYPE_FG]->paint = agl_actor__null_painter;

		gl_canvas_op__finish(op);
	}

	static Op op;
	op = (Op){draw_press, draw_motion, draw_finish};
	return &op;
}


static Op*
gl_canvas_op__auto_drag ()
{
	void auto_press (CanvasOp* op, GdkEvent* event)
	{
		AutoSelection* sel = &op->arrange->automation.hover;

		gl_canvas_op__press(op, event);

		op->ft.automation.orig[0] = ((Curve**)&sel->track->track->bezier)[sel->type]->path[sel->subpath];
		if (sel->is_line) {
			op->ft.automation.orig[1] = ((Curve**)&sel->track->track->bezier)[sel->type]->path[sel->subpath + 1];
		}
	}

	void auto_motion (CanvasOp* op)
	{
		#define PX_TO_Y(P) (100. * (height - (P)) / height)
		#define Y_TO_PX(Y) (height * (100. - Y) / 100.)

		Arrange* arrange = op->arrange;
		AutoSelection* sel = &arrange->automation.hover;
		Curve* curve = ((Curve**)&sel->track->track->bezier)[sel->type];
		BPath* path = curve->path;

		gl_canvas_op__motion(op);

		float height = sel->track->height * vzoom(arrange);

		int n_pts = sel->is_line ? 2 : 1;
		for (int i=0;i<n_pts;i++) {
			int subpath = sel->subpath + i;
			BPath* p = (BPath*)&path[subpath];

			p->x3 = CLAMP(
				op->ft.automation.orig[i].x3 + arr_px2samples(arrange, op->mouse.x - op->start.x),
				subpath > 0 ? ((BPath*)&path[subpath - 1])->x3 + 1. : 0.,
				subpath + 1 < curve->len ? ((BPath*)&path[subpath + 1])->x3 - 1. : FLT_MAX
			);

			float orig = Y_TO_PX(op->ft.automation.orig[i].y3);
			float dy = op->mouse.y - op->start.y;
			p->y3 = CLAMP(
				PX_TO_Y(orig + dy),
				0.,
				100.
			);
		}

		if (op->ft.automation.track) {
			agl_actor__set_size(op->ft.automation.track);
			agl_actor__invalidate(op->ft.automation.track);
		}
	}

	void auto_finish (CanvasOp* op)
	{
		Arrange* arrange = op->arrange;
		AutoSelection* sel = &arrange->automation.hover;

		gl_canvas_op__finish(op);

		Curve* curve = ((Curve**)&sel->track->track->bezier)[arrange->automation.hover.type];
		int n_pts = sel->is_line ? 2 : 1;
		for (int i=0;i<n_pts;i++) {
			am_automation_point_change(curve, arrange->automation.hover.subpath + i, NULL, NULL);
		}
	}

	static Op op;
	op = (Op){auto_press, auto_motion, auto_finish};
	return &op;
}


static void
gl_canvas_op__finish (CanvasOp* op)
{
	if (!op->part && (op->arrange->toolbox.current == TOOL_DEFAULT)) {
		// TODO should we make this part of the box op? currently box is not started until drag. we would have to start the op on PRESS.
		dbg(1, "part=%p %s", op->part, canvas_op__print_type(op));
		if (op->arrange->part_selection) arr_part_selection_replace(op->arrange, NULL, true);
	}

	if (op->part_local) part_local_destroy(op->part_local);
	op->part_local = NULL;

	op->op = ops[op->type = OP_NONE];

	op->max_movement = 0;

	g_source_remove0(op->timer);
}


static void
update_target (Arrange* arrange, ArrTrk* track, AMPart* part)
{
	AGlActor* target = CGL->actors[ACTOR_TYPE_TARGET];
	if (!target) {
		agl_actor__add_child(CGL->actors[ACTOR_TYPE_MAIN], target = CGL->actors[ACTOR_TYPE_TARGET] = target_actor((GtkWidget*)arrange));
		((Target*)target)->part = part;

		int n_tracks = 1;
		((Target*)target)->width = arr_get_selection_size(arrange, arrange->part_selection, &n_tracks, NULL, NULL);
	}
	((Target*)target)->track = track;

	DRect rect;
	arrange->canvas->get_part_rect(arrange, part, &rect);
	AyyiSongPos pos;
	arr_snap_px2spos(arrange, &pos, rect.x1);

	target->region.x1 = arr_gl_pos2px_(arrange, &pos);
	target->region.x2 = target->region.x1 + 1;
}


static void
remove_target (Arrange* arrange)
{
	agl_actor__remove_child(CGL->actors[ACTOR_TYPE_MAIN], CGL->actors[ACTOR_TYPE_TARGET]);
	CGL->actors[ACTOR_TYPE_TARGET] = NULL;
}


static void
scroll_motion (CanvasOp* op)
{
	// Should work for copy, move, resize_left and resize_right operations

	gboolean on_timeout (gpointer user_data)
	{
		CanvasOp* op = user_data;
		Arrange* arrange = op->arrange;

		arrange->canvas->scroll_to (arrange, -CGL->actors[ACTOR_TYPE_MAIN]->scrollable.x1 + scroller.to_move, -1);

		scroller.did_move += scroller.to_move;
		scroller.timer = 0;

		return G_SOURCE_REMOVE;
	}

	GlCanvasOp* gop = (GlCanvasOp*)op;
	GlCanvas* c = op->arrange->canvas->gl;

	agl_actor__grab(c->actors[ACTOR_TYPE_MAIN]);

	int to_move = 0;
	int _to_move = (gop->event.x - c->actors[ACTOR_TYPE_MAIN]->region.x1) - SCROLL_EDGE_WIDTH;
	if (_to_move < 0) {
		if(_to_move < scroller.did_move){
			to_move = _to_move;
		}
	} else {
		int _to_move = gop->event.x - (c->actor->region.x2 - SCROLL_EDGE_WIDTH);
		if(_to_move > scroller.did_move){
			to_move = _to_move;
		}
	}

	if (to_move) {
		scroller.to_move = to_move - scroller.did_move;
		if (!scroller.timer) scroller.timer = g_timeout_add(20, on_timeout, op);
	}
}


static void
scroll_finish (CanvasOp* op)
{
	scroller.did_move = 0;
	g_source_remove0(scroller.timer);
}


// These 2 fns used to be shared by all canvases but needed to be updated
// The difference is:
//  - 'origin' is not used in this version
//  - the offset of the panel from the window is calculated
static bool
drag_mouse_timeout (CanvasOp* op)
{
	GlCanvasOp* gop = (GlCanvasOp*)op;
	Arrange* arrange = op->arrange;
	g_return_val_if_fail(arrange, false);

	static int scroll_dx = SCROLL_MULTIPLIER;

	bool stop = true;

	GtkWidget* widget = arrange->canvas->widget;
	GdkWindow* window = arrange->canvas->widget->window;
	g_return_val_if_fail(window, false);

	gint x = 0, y = 0, topwinx = 0, topwiny = 0, rootx, rooty;
	gdk_window_get_pointer(window, &x, &y, NULL);
	gdk_window_get_pointer(gtk_widget_get_toplevel(arrange->canvas->widget)->window, &topwinx, &topwiny, NULL);
	gdk_window_get_origin (gtk_widget_get_toplevel(arrange->canvas->widget)->window, &rootx, &rooty);

	gint wx, wy;
	gtk_widget_translate_coordinates(widget, gtk_widget_get_toplevel(widget), 0, 0, &wx, &wy);
	gop->event.x = x - wx - op->arrange->tc_width.val.i;

	// how big is the screen?
	GdkScreen* screen = gdk_screen_get_default(); // this may not be the right screen in a multiscreen setup!!
	int screen_h = gdk_screen_get_height(screen);

	// do the bounds checking again in case the mouse has moved
	if (canvas_op__is_right_edge(op)) {
		arrange->canvas->scroll_right(arrange, scroll_dx / SCROLL_MULTIPLIER);
		stop = false;
	}
	else if (canvas_op__is_left_edge(op)) {
		call(arrange->canvas->scroll_left, arrange, scroll_dx / SCROLL_MULTIPLIER);
		stop = false;
	}
	else if (rooty + topwiny < SCROLL_EDGE_WIDTH) {
		// we are at the top edge of the screen.
		call(arrange->canvas->scroll_down, arrange, scroll_dx / SCROLL_MULTIPLIER);
		stop = false;
	}
	else if (rooty + topwiny > screen_h - SCROLL_EDGE_WIDTH) {
		// we are at the bottom edge of the screen.
		call(arrange->canvas->scroll_up, arrange, scroll_dx / SCROLL_MULTIPLIER);
		stop = false;
	}
	else scroll_dx = 1;

	// update the scroll speed
	if (!stop) scroll_dx = MIN((int)(scroll_dx * 1.0 + 1), SCROLL_MAX_SPEED);

	if (stop) op->timer = 0;
	return !stop;
}


static void
canvas_op__handle_scrolling (CanvasOp* op)
{
	// The edge detection is done here to determine whether the timer needs to be started.
	// It is done again in the timeout as the mouse may have moved.
	if (canvas_op__is_right_edge(op) || canvas_op__is_left_edge(op)) {
		op->timer = g_timeout_add(50, (gpointer)drag_mouse_timeout, op);
	}
}
