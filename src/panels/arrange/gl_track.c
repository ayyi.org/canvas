/*
 +------------------------------------------------------------
 | This file is part of the Ayyi project. https://www.ayyi.org
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>
 +---------------------------------------------------------------------
 | This program is free software; you can redistribute it and/or modify
 | it under the terms of the GNU General Public License version 3
 | as published by the Free Software Foundation.
 +----------------------------------------------
 |
 */

#include "agl/behaviours/partial_cache.h"

#define TCTRACKS ((TrackControlActor*)c->actors[ACTOR_TYPE_TC])->tracks
#define PARTIAL_CACHE() ((PartialCacheBehaviour*)((AGlActor*)ta)->behaviours[0])

AGlActor*   gl_track      (ArrTrk*);
static void gl_track_free (AGlActor*);

static AGlActorClass track_class = {0, "Track", (AGlActorNew*)gl_track, gl_track_free};

static PartActor* gl_track_find_part     (AGlActor*, AMPart*);
static AGliRange  gl_track_content_range (AGlActor*);

#ifdef USE_FBO

static bool
gl_track_paint (AGlActor* actor)
{
#if 0
#ifdef DEBUG
	// paint fbo background
	if (actor->cache.enabled) {
		SET_PLAIN_COLOUR(agl->shaders.plain, 0x6666ff55);
		agl_use_program((AGlShader*)agl->shaders.plain);
		AGlRect r = {
			.w = 1000000,
			.h = agl_actor__height(actor)
		};
		agl_rect_(r);
	}
#endif
#endif

	return true;
}
#endif


AGlActor*
gl_track (ArrTrk* atr)
{
	void gl_track_init (AGlActor* actor)
	{
		TrackActor* ta = (TrackActor*)actor;
		Arrange* arrange = actor->root->user_data;

		PARTIAL_CACHE()->scrollable = CGL->actors[ACTOR_TYPE_MAIN];

#ifdef AGL_ACTOR_RENDER_CACHE
		actor->fbo = agl_fbo_new(2., agl_actor__height(actor), 0, AGL_FBO_HAS_STENCIL);
		actor->cache.enabled = true;
#endif
	}

	/*
	 *  Because there are child actors that are positioned relative to their parent, GlTrack start position is always zero.
	 *  The cache geometry is determined by the track contents. At high zoom the actor will be large and the cache will only cover a part of the actor.
	 */
	void gl_track_set_size (AGlActor* actor)
	{
		TrackActor* ta = (TrackActor*)actor;
		Arrange* arrange = actor->root->user_data;

		AGliRange pxrange = PARTIAL_CACHE()->content;

		float vzoom = CGL->zoom->value.pt.y;
		float y = ta->atr->y * vzoom;
#ifdef AGL_ACTOR_RENDER_CACHE
		AGlfRegion region = {
			.x1 = 0.,
			.x2 = pxrange.end,
			.y1 = y,
			.y2 = y + ta->atr->height * vzoom
		};

		if (memcmp(&region, &actor->region, sizeof(AGlfRegion))) {
			actor->region = region;
		}
#else
		actor->region = (AGlfRegion){
			.x1 = 0.,
			.x2 = pxrange.end,
			.y1 = y,
			.y2 = y + ta->atr->height * vzoom
		};
#endif
	}

	AGlActor* ta = (AGlActor*)agl_actor__new(TrackActor,
		.actor = {
			.class = &track_class,
			.name = g_strdup(atr->track->name),
			.program = (AGlShader*)agl->shaders.plain,
			.init = gl_track_init,
			.paint = gl_track_paint,
			.set_size = gl_track_set_size,
		},
		.atr = atr
	);

	ta->behaviours[0] = partial_cache();
	PARTIAL_CACHE()->get_range = gl_track_content_range;

	if (atr->track->bezier.vol->path) {
		AGlActor* automation = agl_actor__add_child(ta, arr_gl_track_automation(atr));
		automation->z = 100;
	}

	return ta;
}


static void
gl_track_free (AGlActor* actor)
{
	g_free(actor->name);
	g_free(actor);
}


static PartActor*
gl_track_find_part (AGlActor* actor, AMPart* part)
{
	GList* l = actor->children;
	for (;l;l=l->next) {
		PartActor* pa = l->data;
		if (pa->part == part) {
			return pa;
		}
	}
	return NULL;
}


static AGliRange
gl_track_content_range (AGlActor* actor)
{
	TrackActor* ta = (TrackActor*)actor;
	Arrange* arrange = actor->root->user_data;

	AyyiSongPos track_end, track_start;
	am_track__get_start_end(ta->atr->track, &track_start, &track_end);

	AMiRange auto_range = arr_track_get_automation_range(ta->atr);

	return (AGliRange){
		.start = (int)MIN(arr_gl_pos2px_(arrange, &track_start), arr_gl_samples2px(arrange, auto_range.start)),
		.end = (int)MAX(arr_gl_pos2px_(arrange, &track_end), arr_gl_samples2px(arrange, auto_range.end))
	};
}


#ifdef DEBUG
/*
 *  Check internal consistency
 */
static bool
gl_track_verify (TrackActor* ta)
{
	AGlActor* actor = (AGlActor*)ta;
	Arrange* arrange = actor->root->user_data;
	g_return_val_if_fail(arrange->canvas, false);
	GlCanvas* c = arrange->canvas->gl;

	int n1 = 0;
	FilterIterator* i = am_part_iterator_new ((Filter)am_part__has_track, ta->atr->track);
	AMPart* part;
	while ((part = (AMPart*)filter_iterator_next(i))) {
		n1++;
	}
	filter_iterator_unref(i);

	int n2 = g_list_length(actor->children);
	if (n2 != n1) {
		pwarn("%s: n_parts=%i (expected %i)", ta->atr->track->name, n2, n1);
		return false;
	}

	guint a1 = am_collection_length(am_parts);
	guint a2 = g_hash_table_size(c->parts);
	if (a2 != a1) {
		pwarn("parts: %i %i", a1, a2);
		return false;
	}

	return true;
}
#endif
