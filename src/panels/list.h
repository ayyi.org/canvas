/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_LISTWIN            (ayyi_listwin_get_type ())
#define AYYI_LISTWIN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), AYYI_TYPE_LISTWIN, AyyiListWin))
#define AYYI_LISTWIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), AYYI_TYPE_LISTWIN, AyyiListWinClass))
#define AYYI_IS_LISTWIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), AYYI_TYPE_LISTWIN))
#define AYYI_IS_LISTWIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_LISTWIN))
#define AYYI_LISTWIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), AYYI_TYPE_LISTWIN, AyyiListWinClass))

typedef struct _AyyiListWinClass AyyiListWinClass;

struct _AyyiListWinClass {
	AyyiPanelClass parent_class;
};

struct _AyyiListWin {
  AyyiPanel          panel;
  
  GtkListStore*      list_store;        // the model. List windows cannot always share the model.
  GtkTreeModel*      sort_model;
  GtkTreeView*       treeview;

  int                mode;
  GList*             src;               // objects that we are showing content of. For LIST_MODE_MIDI, this is a list of AMPart*.

  GtkTreeViewColumn* column_val1;
  GtkTreeViewColumn* column_val2;
  GtkTreeViewColumn* column_label;

  GtkWidget*         menu;

  GList*             selection;         // AMPart* - private. used only to check whether the selection has changed.
  bool               selection_valid;   // should only be set for private (non-linked) selections
                                        // -indicates that the list is owned by self.
                                        // -list->selection cannot be set if valid is not set.
                                        // new - implemenation incomplete
  
  guint              idle;
  gulong             cursor_handler_id; // row select signal handler id.

  guint              update_id;
  bool               debug_freeze_selection;
};

GType ayyi_listwin_get_type ();

#define list_foreach \
	panel_foreach \
		if(G_OBJECT_TYPE(panel) != AYYI_TYPE_LISTWIN) continue; \
		AyyiListWin* list_win = (AyyiListWin*)panel;
#define end_list_foreach end_panel_foreach


G_END_DECLS
