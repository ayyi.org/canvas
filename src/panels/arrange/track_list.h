/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | TrackList provides an array datastructure with the following         |
 | features:                                                            |
 |   * fixed size (minimal number of heap allocations)                  |
 |   * items are reorderable                                            |
 |   * display order can be different to internal order                 |
 +----------------------------------------------------------------------+
 |
*/

#ifndef __track_list_h__
#define __track_list_h__

#define AM_MAX_TRKX (AM_MAX_TRK + 48)

typedef int ArrTrackNum;

typedef struct
{
   ArrTrk**           pool;

   ArrTrk*            track   [AM_MAX_TRKX]; // pointer array. Currently not in the same order as the model track array, so use ArrTrackNum as index, not AMTrackNum.
   ArrTrk*            display [AM_MAX_TRKX]; // tracks by display order.
   TrackDispNum       order   [AM_MAX_TRKX]; // maps from data --> display order. Accessed via the arr_t_to_d() function.
   int                display_max;           // the current active length of the display array.

} TrackList;

TrackList*   track_list__new                        (Arrange*);
void         track_list__free                       (TrackList*);

ArrTrk*      track_list__add                        (TrackList*, AMTrack*);
void         track_list__remove                     (TrackList*, ArrTrk*);
void         track_list__move_to_pos                (TrackList*, TrackDispNum, TrackDispNum);

ArrTrk*      track_list__lookup_track               (TrackList*, AMTrack*);
ArrTrk*      track_list__track_by_index             (TrackList*, ArrTrackNum);
ArrTrk*      track_list__track_by_display_index     (TrackList*, TrackDispNum);
TrackDispNum track_list__get_display_num            (TrackList*, const AMTrack*);
AMTrack*     track_list__get_trk_from_display_index (TrackList*, TrackDispNum);
TrackDispNum arr_t_to_d                             (TrackList*, TrackNum);
TrackNum     track_list__track_num_from_display_num (TrackList*, TrackDispNum);

int          track_list__count_disp_items           (TrackList*);

AMTrack*     track_list__prev_visible               (TrackList*, AMTrack*);
ArrTrk*      track_list__next_visible               (TrackList*, AMTrack*);
ArrTrk*      track_list__last_visible               (TrackList*);

void         track_list__update_all_pointers        (TrackList*);

#endif
