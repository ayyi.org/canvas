/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2005-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
* | A single "track control" area containing buttons and labels for      |
* | an arrange-window track.                                             |
* | Class is based on gtk_hbox                                           |
* +----------------------------------------------------------------------+
*
*/
#include "global.h"
#ifndef DEBUG_TRACKCONTROL
#include "windows.h"
#include "arrange.h"
#include "widgets/trackbutton.h"
#include "support.h"
#include "gtkmeter.h"
#include "gtkvumeter.h"
#include "seqedit.h"
#include "widgets/keys.h"

#include "track_control_midi.h"

extern int       debug;
extern SMConfig* config;
extern void      arr_observer__midi_key_scroll(TrackControlMidi*);

static void track_control_midi_on_key_scroll         (GtkAdjustment*, TrackControlMidi*);
static void track_control_midi_on_note_height_change (GtkWidget*, int, TrackControlMidi*);


G_DEFINE_TYPE (TrackControlMidi, track_control_midi, TYPE_TRACK_CONTROL)

static void
track_control_midi_class_init (TrackControlMidiClass *class)
{
  //GtkWidgetClass *widget_class = (GtkWidgetClass*) class;

  //widget_class->size_request = track_control_midi_size_request;
  //widget_class->size_allocate = track_control_midi_size_allocate;

  /*
  TrackControlClass *track_control_class = (TrackControlClass*) class;
  track_control_class->redraw = track_control_midi_redraw;
  */
}

static void
track_control_midi_init(TrackControlMidi *tc)
{
}


GtkWidget*
track_control_midi_new(ArrTrk* atr, gint spacing)
{
	// Create widgets for a track that already exists in the songcore.

	TrackControlMidi* tcm = g_object_new(TYPE_TRACK_CONTROL_MIDI, NULL);

	TrackControl* tc      = TRACK_CONTROL(tcm);
	tc->redraw            = track_control_midi_redraw;
	tc->arrange           = atr->arrange;
	tc->atr               = atr;
	GTK_BOX(tc)->spacing  = spacing;

	track_control_make_spacer(tc);
	track_control_make_widgets(tc);

	int bottom_note = 60 - 48; // C
	tcm->vadjust = GTK_ADJUSTMENT(gtk_adjustment_new(60.0, bottom_note, bottom_note + 88, 1, 10, 10));
	tcm->keys = gtk_keys_new(NULL, tcm->vadjust);
	gtk_box_pack_start(GTK_BOX(tc), tcm->keys, FALSE, FALSE, 0);
	gtk_widget_set_size_request(tcm->keys, 20, -1);
	gtk_widget_show(tcm->keys);
	g_signal_connect(tcm->vadjust, "value-changed", G_CALLBACK(track_control_midi_on_key_scroll), tcm);
	g_signal_connect(tcm->keys, "change_note_height", G_CALLBACK(track_control_midi_on_note_height_change), tcm);

	return GTK_WIDGET(tcm);
}


void
track_control_midi_redraw (TrackControl* tc)
{
	track_control_redraw(TRACK_CONTROL(tc));
}


void
track_control_midi_set_height (TrackControlMidi* tcm, int height)
{
	// @height is the _zoomed_ track height.

	g_return_if_fail(tcm);
	TrackControl* tc = TRACK_CONTROL(tcm);
	ArrTrk* at = tc->atr;

#ifdef DEBUG
	ArrTrackMidi* atm = (ArrTrackMidi*)at;

	int centre_note = atm->centre_note;
	int new_height_z = height;
	int new_n_notes = new_height_z / atm->note_height;
	int n_notes = arr_track_midi_get_n_notes(atm);
	int top_note = centre_note + new_n_notes / 2;
	dbg(2, "n_notes=%i centre_note=%i new_height_z=%i new_n_notes=%i top_note=%i", n_notes, centre_note, new_height_z, new_n_notes, top_note);
#endif

	((ArrTrackMidi*)at)->velocity_chart_height = CLAMP((height - 2 * PART_OUTLINE_WIDTH) / 2, 0, 30);

	track_control_set_height(tc, height);
}


void
track_control_midi_highlight_note(TrackControlMidi* tcm, int note)
{
	//@param note - set to zero to unselect.

	gtk_keys_set_highlighted_note(GTK_KEYS(tcm->keys), note);
}


static void
track_control_midi_on_key_scroll(GtkAdjustment* widget, TrackControlMidi* tcm)
{
	arr_observer__midi_key_scroll(tcm);
}


static void
track_control_midi_on_note_height_change(GtkWidget* keys, int height, TrackControlMidi* tcm)
{
	//as this change only affects the current track, not sure if an Observer fn is appropriate...
	//... i guess we should move it to Observer in order to reduce redepencies. TODO
	dbg(0, "height=%i", height);
	g_return_if_fail(IS_TRACK_CONTROL_MIDI(tcm));
	TrackControl* tc = TRACK_CONTROL(tcm);

	//ArrTrk* at = tc->arrange->track[tc->track->track_num];
	ArrTrk* at = tc->atr;
	((ArrTrackMidi*)at)->note_height = height;

#ifdef USE_GNOMECANVAS
	void parts_redraw_by_track (Arrange*, AMTrack*);
	parts_redraw_by_track(tc->arrange, at->track);
#endif
}


#endif //DEBUG_TRACKCONTROL

#define __TRACK_CONTROL_MIDI_C__
