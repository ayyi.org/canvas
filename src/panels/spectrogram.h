/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2017 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __spectrogram_h__
#define __spectrogram_h__

#define spectrogram_foreach GList* swindows = windows__get_by_type(AYYI_TYPE_SPECTROGRAM_WIN); if(swindows){ for(;swindows;swindows=swindows->next){ SpectrogramWin* spectrogram = (SpectrogramWin*)swindows->data;
#define end_spectrogram_foreach } g_list_free(swindows); }

#include <panels/panel.h>

G_BEGIN_DECLS

#define AYYI_TYPE_SPECTROGRAM_WIN            (ayyi_spectrogram_win_get_type ())
#define AYYI_SPECTROGRAM_WIN(obj)            (GTK_CHECK_CAST ((obj), AYYI_TYPE_SPECTROGRAM_WIN, AyyiSpectrogramWin))
#define AYYI_SPECTROGRAM_WIN_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), AYYI_TYPE_SPECTROGRAM_WIN, AyyiSpectrogramWinClass))
#define AYYI_IS_SPECTROGRAM_WIN(obj)         (GTK_CHECK_TYPE ((obj), AYYI_TYPE_SPECTROGRAM_WIN))
#define AYYI_IS_SPECTROGRAM_WIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), AYYI_TYPE_SPECTROGRAM_WIN))
#define AYYI_SPECTROGRAM_WIN_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), AYYI_TYPE_SPECTROGRAM_WIN, AyyiSpectrogramWinClass))

typedef struct _AyyiSpectrogramWinClass AyyiSpectrogramWinClass;

struct _AyyiSpectrogramWinClass {
	AyyiPanelClass parent_class;
	guint          timer;
};

struct _AyyiSpectrogramWin {
	AyyiPanel      panel;
	GtkWidget*     area;
	float          meterlevel;
	void*          plugin_props;
	uint64_t       update_time_ms;
};

G_END_DECLS
#endif //__spectrogram_h__
