/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2004-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

GHashTable* canvas_types = NULL;

int max_canvas_type = 0;

CanvasType
arr_register_canvas (const char* name, NewCanvas canvas_new, void (*menu_callback)(Arrange*))
{
	CanvasType* T = g_new(int, 1);
	*T = ++max_canvas_type;

	dbg(2, "%s type=%i", name, *T);

	CanvasClass* cc = AYYI_NEW(CanvasClass,
		.name = name,
		.menu_callback = menu_callback,
		.canvas_new = canvas_new
	);

	g_hash_table_insert(canvas_types, T, cc);

	return *T;
}


CanvasType
arr_lookup_canvas_type(const char* name)
{
	g_return_val_if_fail(name, 0);
	if(name){
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init (&iter, canvas_types);
		while (g_hash_table_iter_next (&iter, &key, &value)){
			CanvasClass* canvas_class = value;
			CanvasType canvas_type = *((int*)key);
			dbg(3, "  %i=%p %s", canvas_type, canvas_class, canvas_class->name);
			if(!strcmp(canvas_class->name, name)){
				return canvas_type;
			}
		}
	}
	pwarn("not found: %s", name);
	return 0;
}


void
arr_canvas_foreach(void (*fn)(CanvasType, CanvasClass*, gpointer), gpointer user_data)
{
	int i = 0;
	GHashTableIter iter;
	gpointer key, value;
	g_hash_table_iter_init (&iter, canvas_types);
	while (g_hash_table_iter_next (&iter, &key, &value)){
		CanvasClass* canvas_class = value;
		CanvasType canvas_type = *((int*)key);
		dbg(3, "  %i=%p %s", canvas_type, canvas_class, canvas_class->name);
		fn(canvas_type, canvas_class, user_data);
		i++;
	}
}


