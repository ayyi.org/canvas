/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2019-2019 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

using Gdk;
using Gtk;

public class GlArea : Gtk.DrawingArea
{
	public GlArea()
	{
		print("hello");
	}

	public override void realize ()
	{
		print("realize");
	}

	public override void size_allocate (Gdk.Rectangle allocation)
	{
		print("allocate");
	}

	public override bool expose_event (Gdk.EventExpose event)
	{
		print("expose");
		return true;
	}

	public override bool event (Gdk.Event event)
	{
		print("event");
		return false;
	}

	/*
	private override void finalize ()
	{
		print("finalize");
	}
	*/
}

